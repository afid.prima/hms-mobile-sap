package id.co.ams_plantation.harvestsap.presenter.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseAndJSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import id.co.ams_plantation.harvestsap.BuildConfig;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.connection.Logger;
import id.co.ams_plantation.harvestsap.connection.Method;
import id.co.ams_plantation.harvestsap.connection.ServiceRequest;
import id.co.ams_plantation.harvestsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestsap.connection.Tag;
import id.co.ams_plantation.harvestsap.util.ActivityLoggerHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.SetUpDataSyncHelper;
import id.co.ams_plantation.harvestsap.view.ApiView;
import okhttp3.OkHttpClient;
import okhttp3.Response;



public class ApiPresenter<V extends ApiView> extends BasePresenter<ApiView> {


    @Override
    protected void attachView(ApiView view) {
        super.attachView(view);
    }

    protected void get(String path, HashMap<String, String> queryParameter, Tag tag) {
        ServiceRequest serviceRequest = new ServiceRequest();
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
        boolean isUsingPublicNetwork = preferences.getBoolean(HarvestApp.CONNECTION_PREF,false);
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECT_BUFFER_SERVER, Context.MODE_PRIVATE);
        boolean isConnectBufferServer = prefer.getBoolean(HarvestApp.CONNECT_BUFFER_SERVER,false);
        String host = isUsingPublicNetwork ? BuildConfig.HOST_PUBLIC : isConnectBufferServer ? BuildConfig.HOST_BUFFER : BuildConfig.HOST;
        serviceRequest.setHost(host + BuildConfig.API_URL);
        serviceRequest.setPath(path);
        serviceRequest.setTag(tag);
        serviceRequest.setQueryParameter(queryParameter);
        serviceRequest.setMethod(Method.GET);
        if(SetUpDataSyncHelper.cekGetDataTag(tag)){
            skipExecute(serviceRequest);
        }else{
            execute(serviceRequest);
        }
    }

    protected void get2(String path, HashMap<String, String> queryParameter, Tag tag) {
        ServiceRequest serviceRequest = new ServiceRequest();
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
        boolean isUsingPublicNetwork = preferences.getBoolean(HarvestApp.CONNECTION_PREF,false);
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECT_BUFFER_SERVER, Context.MODE_PRIVATE);
        boolean isConnectBufferServer = prefer.getBoolean(HarvestApp.CONNECT_BUFFER_SERVER,false);
        String host = isUsingPublicNetwork ? BuildConfig.HOST_PUBLIC : isConnectBufferServer ? BuildConfig.HOST_BUFFER : BuildConfig.HOST;
        serviceRequest.setHost(host + BuildConfig.API_URL);
        serviceRequest.setPath(path);
        serviceRequest.setTag(tag);
        serviceRequest.setQueryParameter(queryParameter);
        serviceRequest.setMethod(Method.GET);
        execute(serviceRequest);
    }

    protected void post(String path, HashMap<String, String> bodyParameter, Tag tag) {
        ServiceRequest serviceRequest = new ServiceRequest();
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
        boolean isUsingPublicNetwork = preferences.getBoolean(HarvestApp.CONNECTION_PREF,false);
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECT_BUFFER_SERVER, Context.MODE_PRIVATE);
        boolean isConnectBufferServer = prefer.getBoolean(HarvestApp.CONNECT_BUFFER_SERVER,false);
        String host = isUsingPublicNetwork ? BuildConfig.HOST_PUBLIC : isConnectBufferServer ? BuildConfig.HOST_BUFFER : BuildConfig.HOST;
        serviceRequest.setHost(host + BuildConfig.API_URL);
        serviceRequest.setPath(path);
        serviceRequest.setBodyParameter(bodyParameter);
        serviceRequest.setTag(tag);
        serviceRequest.setMethod(Method.POST);
        execute(serviceRequest);
    }

    protected void postBodyRaw(String path, JSONObject bodyParameter, Tag tag) {
        ServiceRequest serviceRequest = new ServiceRequest();
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
        boolean isUsingPublicNetwork = preferences.getBoolean(HarvestApp.CONNECTION_PREF,false);
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECT_BUFFER_SERVER, Context.MODE_PRIVATE);
        boolean isConnectBufferServer = prefer.getBoolean(HarvestApp.CONNECT_BUFFER_SERVER,false);
        String host = isUsingPublicNetwork ? BuildConfig.HOST_PUBLIC : isConnectBufferServer ? BuildConfig.HOST_BUFFER : BuildConfig.HOST;
        serviceRequest.setHost(host + BuildConfig.API_URL);
        serviceRequest.setPath(path);
        serviceRequest.setBodyRaw(bodyParameter);
        serviceRequest.setTag(tag);
        serviceRequest.setMethod(Method.POST);
        execute(serviceRequest);
    }

    protected void postBodyRawArray(String path, JSONArray bodyParameter, Tag tag) {
        ServiceRequest serviceRequest = new ServiceRequest();
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
        boolean isUsingPublicNetwork = preferences.getBoolean(HarvestApp.CONNECTION_PREF,false);
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECT_BUFFER_SERVER, Context.MODE_PRIVATE);
        boolean isConnectBufferServer = prefer.getBoolean(HarvestApp.CONNECT_BUFFER_SERVER,false);
        String host = isUsingPublicNetwork ? BuildConfig.HOST_PUBLIC : isConnectBufferServer ? BuildConfig.HOST_BUFFER : BuildConfig.HOST;
        serviceRequest.setHost(host + BuildConfig.API_URL);
        serviceRequest.setPath(path);
        serviceRequest.setBodyRawArray(bodyParameter);
        serviceRequest.setTag(tag);
        serviceRequest.setMethod(Method.POST);
        execute(serviceRequest);
    }

    protected void postBodyRawArrayAdmin(String path, JSONArray bodyParameter, Tag tag) {
        ServiceRequest serviceRequest = new ServiceRequest();
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
        boolean isUsingPublicNetwork = preferences.getBoolean(HarvestApp.CONNECTION_PREF,false);
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECT_BUFFER_SERVER, Context.MODE_PRIVATE);
        boolean isConnectBufferServer = prefer.getBoolean(HarvestApp.CONNECT_BUFFER_SERVER,false);
        String host = isUsingPublicNetwork ? BuildConfig.HOST_PUBLIC : isConnectBufferServer ? BuildConfig.HOST_BUFFER : BuildConfig.HOST;
        serviceRequest.setHost(BuildConfig.HOST_ADMIN);
        serviceRequest.setPath(path);
        serviceRequest.setBodyRawArray(bodyParameter);
        serviceRequest.setTag(tag);
        serviceRequest.setMethod(Method.POST);
        execute(serviceRequest);
    }

    public void upload(String path, HashMap<String, String> bodyParameter, HashMap<String, File> files, Tag tag) {
        ServiceRequest serviceRequest = new ServiceRequest();
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
        boolean isUsingPublicNetwork = preferences.getBoolean(HarvestApp.CONNECTION_PREF,false);
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECT_BUFFER_SERVER, Context.MODE_PRIVATE);
        boolean isConnectBufferServer = prefer.getBoolean(HarvestApp.CONNECT_BUFFER_SERVER,false);
        String host = isUsingPublicNetwork ? BuildConfig.HOST_PUBLIC : isConnectBufferServer ? BuildConfig.HOST_BUFFER : BuildConfig.HOST;
        serviceRequest.setHost(host + BuildConfig.API_URL);
        serviceRequest.setPath(path);
        serviceRequest.setBodyParameter(bodyParameter);
        serviceRequest.setFiles(files);
        serviceRequest.setTag(tag);
        serviceRequest.setMethod(Method.UPLOAD);
        execute(serviceRequest);
    }

    protected void execute(final ServiceRequest serviceRequest) {
        ANRequest request = null;
        Gson gson = new Gson();
        Log.d(this.toString()+" execute",gson.toJson(serviceRequest));
        String url = serviceRequest.getHost() + serviceRequest.getPath();

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(GlobalHelper.TIMES_OUT_NETWORK, TimeUnit.SECONDS)
                .readTimeout(GlobalHelper.TIMES_OUT_NETWORK, TimeUnit.SECONDS)
                . writeTimeout(GlobalHelper.TIMES_OUT_NETWORK, TimeUnit.SECONDS)
                .build();

        Date startHit = new Date();

        switch (serviceRequest.getMethod()) {
            case GET:
                request = AndroidNetworking.get(url)
                        .addHeaders(serviceRequest.getHeaders())
                        .addQueryParameter(serviceRequest.getQueryParameter())
                        .setTag(String.valueOf(serviceRequest.getTag()))
                        .setOkHttpClient(okHttpClient)
                        .build();
                break;
            case POST:
                if(serviceRequest.getBodyRaw() != null){
                    request = AndroidNetworking.post(url)
                            .addHeaders(serviceRequest.getHeaders())
                            .addQueryParameter(serviceRequest.getQueryParameter())
                            .addJSONObjectBody(serviceRequest.getBodyRaw())
                            .setTag(String.valueOf(serviceRequest.getTag()))
                            .setOkHttpClient(okHttpClient)
                            .build();
                }else if (serviceRequest.getBodyRawArray() != null){
                    request = AndroidNetworking.post(url)
                            .addHeaders(serviceRequest.getHeaders())
                            .addQueryParameter(serviceRequest.getQueryParameter())
                            .addJSONArrayBody(serviceRequest.getBodyRawArray())
                            .setTag(String.valueOf(serviceRequest.getTag()))
                            .setOkHttpClient(okHttpClient)
                            .build();
                }else{
                    request = AndroidNetworking.post(url)
                            .addHeaders(serviceRequest.getHeaders())
                            .addQueryParameter(serviceRequest.getQueryParameter())
                            .addBodyParameter(serviceRequest.getBodyParameter())
                            .setTag(String.valueOf(serviceRequest.getTag()))
                            .setOkHttpClient(okHttpClient)
                            .build();
                }

                break;
            case UPLOAD:
                request = AndroidNetworking.upload(url)
                        .addHeaders(serviceRequest.getHeaders())
                        .addQueryParameter(serviceRequest.getQueryParameter())
                        .addMultipartParameter(serviceRequest.getBodyParameter())
                        .addMultipartFile(serviceRequest.getFiles())
                        .setTag(String.valueOf(serviceRequest.getTag()))
                        .setOkHttpClient(okHttpClient)
                        .build();
                break;
            case PATCH:
                request = AndroidNetworking.patch(url)
                        .addHeaders(serviceRequest.getHeaders())
                        .addQueryParameter(serviceRequest.getQueryParameter())
                        .addBodyParameter(serviceRequest.getBodyParameter())
                        .setTag(String.valueOf(serviceRequest.getTag()))
                        .setOkHttpClient(okHttpClient)
                        .build();
                break;
            case DELETE:
                request = AndroidNetworking.delete(url)
                        .addHeaders(serviceRequest.getHeaders())
                        .addQueryParameter(serviceRequest.getQueryParameter())
                        .addBodyParameter(serviceRequest.getBodyParameter())
                        .setTag(String.valueOf(serviceRequest.getTag()))
                        .setOkHttpClient(okHttpClient)
                        .build();
                break;
        }

        Logger.request(serviceRequest);

        if (request != null) {
            request.setAnalyticsListener((timeTakenInMillis, bytesSent, bytesReceived, isFromCache) ->
                    Logger.analytic(serviceRequest.getTag(), timeTakenInMillis, bytesSent, bytesReceived, isFromCache))
                    .getAsOkHttpResponseAndJSONObject(new OkHttpResponseAndJSONObjectRequestListener() {
                        @Override
                        public void onResponse(Response okHttpResponse, JSONObject response) {
                            ServiceResponse serviceResponse = JSON.parseObject(response.toString(), ServiceResponse.class);

                            serviceResponse.setTag(serviceRequest.getTag());
                            serviceResponse.setHost(serviceRequest.getHost());
                            serviceResponse.setPath(serviceRequest.getPath());
                            serviceResponse.setMethod(serviceRequest.getMethod());
                            serviceResponse.setHttpCode(okHttpResponse.code());
                            if (serviceResponse.getMessage() == null)
                                serviceResponse.setMessage(okHttpResponse.message());
                            Logger.response(serviceResponse, response.toString());

                            view.successResponse(serviceResponse);

                            Date endTimeHit = new Date();
                            if(GlobalHelper.getUser()!=null){
                                ActivityLoggerHelper.recordRequestData(ApiPresenter.class.getSimpleName(),serviceRequest.getPath(),startHit, endTimeHit);
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            try {
                                ServiceResponse serviceResponse = JSON.parseObject(anError.getErrorBody(), ServiceResponse.class);
                                Log.d(this.toString()+" execute onError", anError.getErrorBody());
                                serviceResponse.setTag(serviceRequest.getTag());
                                serviceResponse.setHost(serviceRequest.getHost());
                                serviceResponse.setPath(serviceRequest.getPath());
                                serviceResponse.setMethod(serviceRequest.getMethod());
                                serviceResponse.setHttpCode(anError.getErrorCode());
                                if (anError.getErrorCode() == 0 || serviceResponse.getHttpCode() == 404)
                                    serviceResponse.setMessage("Can not connect to the server");
                                else if (anError.getErrorCode() == 500)
                                    serviceResponse.setMessage("Internal Server Error");
                                else {
                                    try {
                                        JSONObject jsonMain = new JSONObject(anError.getErrorBody());
                                        Object obj = jsonMain.get("data");
                                        if (obj == null || obj instanceof JSONObject || obj instanceof JSONArray || obj.equals("null"))
                                            serviceResponse.setMessage(jsonMain.getString("error_message"));
                                        else serviceResponse.setMessage("Internal Server Error");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        serviceResponse.setMessage("Internal Server Error");
                                    }
                                }
                                Logger.response(serviceResponse, anError.getErrorBody());
                                view.badResponse(serviceResponse);
                            } catch (Exception e) {
                                e.printStackTrace();
                                ServiceResponse serviceResponse = new ServiceResponse();
                                serviceResponse.setHttpCode(500);
                                if (serviceResponse.getHttpCode() == 404 || anError.getErrorCode() == 0)
                                    serviceResponse.setMessage("Can not connect to the server");
                                else serviceResponse.setMessage("Internal Server Error");
                                serviceResponse.setTag(serviceRequest.getTag());
                                serviceResponse.setHost(serviceRequest.getHost());
                                serviceResponse.setPath(serviceRequest.getPath());
                                serviceResponse.setMethod(serviceRequest.getMethod());
                                Logger.response(serviceResponse, anError.getErrorBody());
                                view.badResponse(serviceResponse);
                            }
                            if(!serviceRequest.getTag().toString().toUpperCase().contains("POST")) {
                                SetUpDataSyncHelper.addPointCounterBadResponse();
                            }
                        }
                    });
        }
    }

    protected void skipExecute(final ServiceRequest serviceRequest) {
        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.setCode(304);
        serviceResponse.setData(new JSONArray());
        serviceResponse.setTag(serviceRequest.getTag());
        serviceResponse.setHost(serviceRequest.getHost());
        serviceResponse.setPath(serviceRequest.getPath());
        serviceResponse.setMethod(serviceRequest.getMethod());
        serviceResponse.setHttpCode(304);
        view.successResponse(serviceResponse);
    }
}
