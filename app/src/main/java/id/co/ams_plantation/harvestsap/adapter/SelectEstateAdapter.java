package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.Estate;

public class SelectEstateAdapter extends ArrayAdapter<Estate> {

    public ArrayList<Estate> items;
    public ArrayList<Estate> itemsAll;
    public ArrayList<Estate> suggestions;
    private final int viewResourceId;

    public SelectEstateAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Estate> data) {
        super(context, resource, data);
        this.items = new ArrayList<>();
        this.items.addAll(data);
        this.itemsAll = (ArrayList<Estate>)this.items.clone();
        this.suggestions = new ArrayList<>();
        this.viewResourceId = resource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        if(items.size()>position){
            Estate estate = items.get(position);
            if (estate != null) {
                ImageView iv = (ImageView) v.findViewById(R.id.iv);
                TextView tv = (TextView) v.findViewById(R.id.tv);
                TextView tv1 = (TextView) v.findViewById(R.id.tv1);
                iv.setVisibility(View.GONE);
                tv.setText(estate.getEstCode());
                tv1.setText(estate.getEstName());
            }
        }
        return v;
    }

    public Estate getItemAt(int position){
        return items.get(position);
    }

    @Override
    public int getCount(){
        return items!=null?items.size():0;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((Estate)(resultValue)).getEstCode();
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (Estate estate : itemsAll) {
                    if(estate.getEstCode().toLowerCase().contains(constraint.toString().toLowerCase()) || estate.getEstName().toLowerCase().contains(constraint.toString().toLowerCase())){
                        suggestions.add(estate);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Estate> filteredList = (ArrayList<Estate>) results.values;
//            clear();
            items.clear();
//            itemsAll.clear();
            if(results != null && results.count > 0) {
//                addAll(filteredList);
//                itemsAll.addAll(filteredList);
//                for (Anggota anggota : filteredList){
//                    items.add(anggota);
//                }
                items.addAll(filteredList);
            }else{
//                clear();
                items.clear();
//                itemsAll.clear();
            }
            notifyDataSetChanged();
        }
    };
}