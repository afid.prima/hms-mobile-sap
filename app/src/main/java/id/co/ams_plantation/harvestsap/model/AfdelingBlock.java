package id.co.ams_plantation.harvestsap.model;

import java.util.ArrayList;

public class AfdelingBlock {
    String afdeling;
    ArrayList<Block> block;

    public AfdelingBlock() {}

    public AfdelingBlock(String afdeling, ArrayList<Block> block) {
        this.afdeling = afdeling;
        this.block = block;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }

    public ArrayList<Block> getBlock() {
        return block;
    }

    public void setBlock(ArrayList<Block> block) {
        this.block = block;
    }
}
