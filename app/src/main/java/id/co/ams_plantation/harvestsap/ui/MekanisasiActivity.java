package id.co.ams_plantation.harvestsap.ui;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.io.File;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import id.co.ams_plantation.harvestsap.Fragment.MekanisasiInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.MekanisasiPanenInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.BJRInformation;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.Estate;
import id.co.ams_plantation.harvestsap.model.MekanisasiPanen;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.presenter.MekanisasiPresenter;
import id.co.ams_plantation.harvestsap.service.BluetoothService;
import id.co.ams_plantation.harvestsap.util.ActivityLoggerHelper;
import id.co.ams_plantation.harvestsap.util.BjrHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.NfcHelper;
import id.co.ams_plantation.harvestsap.util.QrHelper;
import id.co.ams_plantation.harvestsap.util.StateUtils;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiSpbHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import id.co.ams_plantation.harvestsap.view.MekanisasiView;

public class MekanisasiActivity extends BaseActivity implements MekanisasiView {

    public MekanisasiPresenter presenter;
    public Estate estate;
    public MekanisasiPanen mekanisasiPanenSelected;
    public TransaksiSPB selectedTransaksiSPB;
    public QrHelper qrHelper;
    public HashMap<String,Angkut> originAngkut;
    public HashMap<String,BJRInformation> bjrInformationHashMap;
    Bundle savedInstanceState;
    Activity activity;

    public int TYPE_NFC;
    public String valueNFC;

    @BindView(R.id.myCoordinatorLayout)
    public CoordinatorLayout myCoordinatorLayout;
    public Snackbar searchingGPS;

    /****************************************** NFC ******************************************************************/
    public Tag myTag;
    public NfcAdapter nfcAdapter;
    public PendingIntent pendingIntent;
    public IntentFilter[] writeTagFilters;

    @Override
    protected void initPresenter() {
        presenter = new MekanisasiPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        setContentView(R.layout.new_layout);
        getSupportActionBar().hide();
        startTimeScreen = new Date();

        Intent intent = getIntent();
        if(intent.getExtras() != null) {
            String transaksi = intent.getExtras().getString("transaksiSpb");
            if (transaksi != null) {
                File file = new File(GlobalHelper.TRANSFER_TRANSAKSI_SPB);
                if(file.exists()){
                    transaksi = GlobalHelper.readFileContent(GlobalHelper.TRANSFER_TRANSAKSI_SPB);
                    file.delete();
                }
                Log.d("transaksiSpb",transaksi);
                if(!TransaksiSpbHelper.cekFileSpbSelected()) {
                    TransaksiSpbHelper.hapusFileAngkut(false);

                    Gson gson = new Gson();
                    selectedTransaksiSPB = gson.fromJson(transaksi, TransaksiSPB.class);

                    if (selectedTransaksiSPB != null) {

                        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI);
                        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

                        if (selectedTransaksiSPB.getTransaksiAngkut().getMekanisasiPanens() != null) {
                            for (int i = 0; i < selectedTransaksiSPB.getTransaksiAngkut().getMekanisasiPanens().size(); i++) {
                                if(selectedTransaksiSPB.getTransaksiAngkut().getMekanisasiPanens().get(i).getIdMekanisasi() != null) {
                                    DataNitrit dataNitrit = new DataNitrit(selectedTransaksiSPB.getTransaksiAngkut().getMekanisasiPanens().get(i).getIdMekanisasi(),
                                            gson.toJson(selectedTransaksiSPB.getTransaksiAngkut().getMekanisasiPanens().get(i)));
                                    repository.insert(dataNitrit);
                                }
                            }
                        }

                        if (selectedTransaksiSPB.getTransaksiAngkut().getAngkuts() != null) {
                            for(int i = 0 ; i < selectedTransaksiSPB.getTransaksiAngkut().getAngkuts().size(); i++){
                                if(selectedTransaksiSPB.getTransaksiAngkut().getAngkuts().get(i).getIdAngkut() != null) {
                                    DataNitrit dataNitrit = new DataNitrit(selectedTransaksiSPB.getTransaksiAngkut().getAngkuts().get(i).getIdAngkut(),
                                            gson.toJson(selectedTransaksiSPB.getTransaksiAngkut().getAngkuts().get(i)));
                                    repository.insert(dataNitrit);
                                }
                            }
                        }

                        db.close();
                        TransaksiSpbHelper.createFileSpb(transaksi);
                    }
                }else{
                    Gson gson = new Gson();
                    selectedTransaksiSPB = gson.fromJson(TransaksiSpbHelper.readFileSpb(), TransaksiSPB.class);
                }
            }
        }

        qrHelper = new QrHelper(this);
        activity = this;

        String strEst =  Encrypts.decrypt(GlobalHelper.getFileContent(Encrypts.encrypt(GlobalHelper.SELECTED_ESTATE)));
        Gson gson = new Gson();
        estate = (Estate) gson.fromJson(strEst, Estate.class);

        toolBarSetup();

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
        }
        NfcHelper.readFromIntent(getIntent());
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[] { tagDetected };

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        bjrInformationHashMap = BjrHelper.getAllBJR();
//        main();
        new LongOperation().execute();
    }

    private void main() {
        if (!StateUtils.isStateSaved(savedInstanceState)) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_container, MekanisasiInputFragment.getInstance())
                    .commit();
        }
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        TextView textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backProses();
            }
        });
        textToolbar.setText("Mekanisasi SPB");
    }

    public void backProses() {
        if (getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof  MekanisasiInputFragment){
            MekanisasiInputFragment fragment = (MekanisasiInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
            fragment.closeAngkut();
        }else if (getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof MekanisasiPanenInputFragment){
            MekanisasiPanenInputFragment fragment = (MekanisasiPanenInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);

            if(fragment.cekBackProses()){
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_container, MekanisasiInputFragment.getInstance())
                        .commit();
            }else if (fragment.mLayout.getPanelState() != SlidingUpPanelLayout.PanelState.HIDDEN){
                Toast.makeText(HarvestApp.getContext(), "Mohon Selesaikan Inputan Dahulu", Toast.LENGTH_SHORT).show();
            }else {
                AlertDialog alertDialog = new AlertDialog.Builder(MekanisasiActivity.this, R.style.MyAlertDialogStyle)
                        .setTitle("Perhatian")
                        .setMessage("Apakah Anda Yakin Untuk Buang Perubahan Ini")
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_container, MekanisasiInputFragment.getInstance())
                                        .commit();
                            }
                        })
                        .create();
                alertDialog.show();
            }
        }else if (getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof TphInputFragment) {
            getSupportFragmentManager().popBackStack();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_container, MekanisasiInputFragment.getInstance())
                    .commit();
            Log.d(this.getClass() + " backProses", "Dari TphInputFragment");
        }
    }

    public void closeActivity(){
        TransaksiSpbHelper.hapusFileAngkut(true);
        setResult(GlobalHelper.RESULT_MEKANISASIACTIVITY);
        finish();
    }

    private void createLocationListener() {
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

//        searchingGPS = Snackbar.make(myCoordinatorLayout,getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
//        searchingGPS.show();
        WidgetHelper.warningFindGps(this,myCoordinatorLayout);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                1f, mLocationListener);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if(GlobalHelper.isDoubleClick()){
            return;
        }

        setIntent(intent);

        if(selectedTransaksiSPB != null) {
            if (selectedTransaksiSPB.getStatus() == TransaksiSPB.UPLOAD || selectedTransaksiSPB.getStatus() == TransaksiSPB.BELUM_UPLOAD) {
                Toast.makeText(HarvestApp.getContext(), "Spb Ini Sudah Di Upload Atau Close Maka Tidak Bisa Tambah Item Lagi", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        }
        valueNFC = NfcHelper.readFromIntent(intent);
        int formatNFC = NfcHelper.cekFormatNFC(valueNFC);
        if (valueNFC != null) {
            if (formatNFC != GlobalHelper.TYPE_NFC_SALAH) {
                if(valueNFC.length() == 5) {
                    Toast.makeText(this, getResources().getString(R.string.nfc_empty) + " " + GlobalHelper.LIST_NFC[formatNFC], Toast.LENGTH_SHORT).show();
                }else{
//                    if (!closeTransaksi) {
                    cekDataPassing(valueNFC, this);
//                    } else {
//                        Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_not_empty), Toast.LENGTH_SHORT).show();
//                    }
                }
                TYPE_NFC = formatNFC;
            } else {
                Toast.makeText(this,R.string.format_not_valid,Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this,R.string.format_not_valid,Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onStart() {
        super.onStart();

        // If Bluetooth is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!bluetoothHelper.mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, GlobalHelper.REQUEST_ENABLE_BT);
            // Otherwise, setup the session
        } else {
            if (bluetoothHelper.mService == null)
                bluetoothHelper.mService = new BluetoothService(this, bluetoothHelper.mHandler);
        }
    }

    @Override
    public void onBackPressed() {
        backProses();
    }

    @Override
    protected void onResume() {
        super.onResume();

        startTimeScreen = new Date();
        NfcHelper.WriteModeOn(nfcAdapter,this,pendingIntent,writeTagFilters);
        if(bluetoothHelper != null) {
            if (bluetoothHelper.mService != null) {

                if (bluetoothHelper.mService.getState() == BluetoothService.STATE_NONE) {
                    // Start the Bluetooth services
                    bluetoothHelper.mService.start();
                }
            }
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        NfcHelper.WriteModeOff(nfcAdapter,this);
        endTimeScreen = new Date();
        if(GlobalHelper.getUser()!=null){
            ActivityLoggerHelper.recordScreenTime(MekanisasiActivity.class.getSimpleName(),startTimeScreen, endTimeScreen);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth services
        if (bluetoothHelper != null) {
            if (bluetoothHelper.mService != null)
                bluetoothHelper.mService.stop();
        }
        endTimeScreen = new Date();
        if(GlobalHelper.getUser()!=null){
            ActivityLoggerHelper.recordScreenTime(MekanisasiActivity.class.getSimpleName(),startTimeScreen, endTimeScreen);
        }
    }

    public class LongOperation extends AsyncTask<String, String, String> {
        private AlertDialog alertDialogAllpoin ;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(MekanisasiActivity.this, getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                bjrInformationHashMap = BjrHelper.getAllBJR();
                return strings[0];
            } catch (Exception e) {
                return "Exception";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (alertDialogAllpoin != null) {
                alertDialogAllpoin.cancel();
            }
            main();
        }
    }
}
