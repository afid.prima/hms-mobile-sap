package id.co.ams_plantation.harvestsap.model;

public class SubTotalAngkutMekanisasi {
    String idKey;
    TransaksiPanen transaksiPanen;
    Angkut angkut;

    public SubTotalAngkutMekanisasi(String idKey, TransaksiPanen transaksiPanen, Angkut angkut) {
        this.idKey = idKey;
        this.transaksiPanen = transaksiPanen;
        this.angkut = angkut;
    }

    public String getIdKey() {
        return idKey;
    }

    public void setIdKey(String idKey) {
        this.idKey = idKey;
    }

    public TransaksiPanen getTransaksiPanen() {
        return transaksiPanen;
    }

    public void setTransaksiPanen(TransaksiPanen transaksiPanen) {
        this.transaksiPanen = transaksiPanen;
    }

    public Angkut getAngkut() {
        return angkut;
    }

    public void setAngkut(Angkut angkut) {
        this.angkut = angkut;
    }
}


