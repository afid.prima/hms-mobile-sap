package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.Operator;

public class SelectOperatorAdapter extends ArrayAdapter<Operator> {
    public ArrayList<Operator> items;
    public ArrayList<Operator> itemsAll;
    public ArrayList<Operator> suggestions;
    private final int viewResourceId;

    public SelectOperatorAdapter(Context context, int viewResourceId, ArrayList<Operator> data){
        super(context, viewResourceId, data);
        this.items = new ArrayList<>();
        this.items.addAll(data);
        this.itemsAll = (ArrayList<Operator>)this.items.clone();
        this.suggestions = new ArrayList<>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        if(items.size()>position){
            Operator Operator = items.get(position);
            if (Operator != null) {
                ImageView iv = v.findViewById(R.id.iv);
                TextView tv = v.findViewById(R.id.tv);
                TextView tv1 = v.findViewById(R.id.tv1);
                iv.setVisibility(View.GONE);
                tv.setText(Operator.getNik() + "\n" + Operator.getEstCodeSAP());
                if(Operator.getGank() != null){
                    tv1.setText(Operator.getGank()+ "\n" + Operator.getNama());
                }else{
                    tv1.setText(Operator.getNama());
                }
            }
        }
        return v;
    }

    public Operator getItemAt(int position){
        return items.get(position);
    }

    @Override
    public int getCount(){
        return items!=null?items.size():0;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((Operator)(resultValue)).getNama();
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (Operator Operator : itemsAll) {
                    if(Operator.getNama().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                            Operator.getNik().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                            Operator.getEstCodeSAP().toLowerCase().contains(constraint.toString().toLowerCase())){
                        suggestions.add(Operator);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Operator> filteredList = (ArrayList<Operator>) results.values;
//            clear();
            items.clear();
//            itemsAll.clear();
            if(results != null && results.count > 0) {
//                addAll(filteredList);
//                itemsAll.addAll(filteredList);
//                for (Anggota anggota : filteredList){
//                    items.add(anggota);
//                }
                items.addAll(filteredList);
            }else{
//                clear();
                items.clear();
//                itemsAll.clear();
            }
            notifyDataSetChanged();
        }
    };
}
