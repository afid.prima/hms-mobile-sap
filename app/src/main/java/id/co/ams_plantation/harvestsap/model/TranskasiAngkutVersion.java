package id.co.ams_plantation.harvestsap.model;

public class TranskasiAngkutVersion {
    int version;
    TransaksiAngkut transaksiAngkut;
    String nextIdTransaksi;

    public TranskasiAngkutVersion(int version, TransaksiAngkut transaksiAngkut) {
        this.version = version;
        this.transaksiAngkut = transaksiAngkut;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public TransaksiAngkut getTransaksiAngkut() {
        return transaksiAngkut;
    }

    public void setTransaksiAngkut(TransaksiAngkut transaksiAngkut) {
        this.transaksiAngkut = transaksiAngkut;
    }

    public String getNextIdTransaksi() {
        return nextIdTransaksi;
    }

    public void setNextIdTransaksi(String nextIdTransaksi) {
        this.nextIdTransaksi = nextIdTransaksi;
    }
}
