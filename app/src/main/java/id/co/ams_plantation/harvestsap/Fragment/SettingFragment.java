package id.co.ams_plantation.harvestsap.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import cat.ereza.customactivityoncrash.util.CrashUtil;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestsap.connection.Tag;
import id.co.ams_plantation.harvestsap.model.DynamicParameterPenghasilan;
import id.co.ams_plantation.harvestsap.model.GangList;
import id.co.ams_plantation.harvestsap.model.LastSyncTime;
import id.co.ams_plantation.harvestsap.model.ModelRecyclerView;
import id.co.ams_plantation.harvestsap.model.ReasonUnharvestTPH;
import id.co.ams_plantation.harvestsap.model.SuperVision;
import id.co.ams_plantation.harvestsap.model.SupervisionList;
import id.co.ams_plantation.harvestsap.model.SyncTime;
import id.co.ams_plantation.harvestsap.presenter.ConnectionPresenter;
import id.co.ams_plantation.harvestsap.ui.AssistensiActivity;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.CagesActivity;
import id.co.ams_plantation.harvestsap.ui.CekBJRActivity;
import id.co.ams_plantation.harvestsap.ui.CountNfcActivity;
import id.co.ams_plantation.harvestsap.ui.FormatNfcActivity;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestsap.ui.MapActivity;
import id.co.ams_plantation.harvestsap.ui.MekanisasiBlockActivity;
import id.co.ams_plantation.harvestsap.ui.OpnameNfcActivity;
import id.co.ams_plantation.harvestsap.ui.PemanenActivity;
import id.co.ams_plantation.harvestsap.ui.QRScan;
import id.co.ams_plantation.harvestsap.ui.ReportActivity;
import id.co.ams_plantation.harvestsap.ui.SPKActivity;
import id.co.ams_plantation.harvestsap.util.ActivityLoggerHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.MekanisasiHelper;
import id.co.ams_plantation.harvestsap.util.QrDoubleSPBHelpper;
import id.co.ams_plantation.harvestsap.util.RestoreHelper;
import id.co.ams_plantation.harvestsap.util.RestoreMasterHelper;
import id.co.ams_plantation.harvestsap.util.SetUpDataSyncHelper;
import id.co.ams_plantation.harvestsap.util.SyncHistoryHelper;
import id.co.ams_plantation.harvestsap.util.SyncTimeHelper;
import id.co.ams_plantation.harvestsap.util.TphHelper;
import id.co.ams_plantation.harvestsap.util.UploadHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import id.co.ams_plantation.harvestsap.view.ApiView;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

import static id.co.ams_plantation.harvestsap.util.GlobalHelper.SETTING_ASSISTENSI;

/**
 * Created by user on 12/4/2018.
 */

public class SettingFragment extends Fragment implements ApiView {
    int LongOperation;
    public static final int LongOperation_Upload_TPH = 0;
    public static final int LongOperation_Upload_Langsiran = 1;
    public static final int LongOperation_Upload_TransaksiPanen = 2;
    public static final int LongOperation_Upload_TransaksiAngkut = 3;
    public static final int LongOperation_Upload_TransaksiSPB = 4;
    public static final int LongOperation_Download_MasterUser = 5;
    public static final int LongOperation_Download_Pemanen = 6;
    public static final int LongOperation_Download_TPH = 7;
    public static final int LongOperation_Download_Langsiran = 8;
    public static final int LongOperation_Download_TransaksiPanen = 9;
    public static final int LongOperation_Download_TransaksiAngkut = 10;
    public static final int LongOperation_Download_TransaksiSPB = 11;
    public static final int LongOperation_Download_PKS = 12;
    public static final int LongOperation_BackUp_HMS = 13;
    public static final int LongOperation_Get_TPH = 14;
    public static final int LongOperation_Get_Langsiran = 15;
    public static final int LongOperation_Get_TransaksiPanen = 16;
    public static final int LongOperation_Get_TransaksiAngkut = 17;
    public static final int LongOperation_Get_TransaksiSpb = 18;
    public static final int LongOperation_Get_MasterUser = 19;
    public static final int LongOperation_Get_Pemanen = 20;
    public static final int LongOperation_Get_PKS = 21;
    public static final int LongOperation_Count_TPH = 22;
    public static final int LongOperation_Count_Langsiran = 23;
    public static final int LongOperation_Count_TransaiskPanen = 24;
    public static final int LongOperation_Count_TransaiskAngut = 25;
    public static final int LongOperation_Count_TransaiskSpb = 26;
    public static final int LongOperation_Restore_HMS = 27;
    public static final int LongOperation_Map = 28;
    public static final int LongOperation_My_Pemanen = 29;
    public static final int LongOperation_Upload_Pemanen = 30;
    public static final int LongOperation_Download_Operator = 31;
    public static final int LongOperation_Get_Operator = 32;
    public static final int LongOperation_Download_Vehicle = 33;
    public static final int LongOperation_Get_Vehicle  = 34;
    public static final int LongOperation_Download_AfdelingAssistantByEstate = 35;
    public static final int LongOperation_Get_AfdelingAssistantByEstate  = 36;
    public static final int LongOperation_Download_ApplicationConfiguration = 37;
    public static final int LongOperation_Get_ApplicationConfiguration  = 38;
    public static final int LongOperation_Download_SupervisionListByEstate = 39;
    public static final int LongOperation_Get_SupervisionListByEstate  = 40;
    public static final int LongOperation_Count_TPanenSupervision = 41;
    public static final int LongOperation_SetUpSupervision = 42;
    public static final int LongOperation_Upload_Supervision = 43;
    public static final int LongOperation_Get_BJRInformation = 44;
    public static final int LongOperation_Download_BJRInformation = 45;
    public static final int LongOperation_Get_ISCCInformation = 46;
    public static final int LongOperation_Download_ISCCInformation = 47;
    public static final int LongOperation_Format_NFC = 48;
    public static final int LongOperation_Upload_QcBuah = 49;
    public static final int LongOperation_Upload_SensusBjr = 50;
    public static final int LongOperation_Upload_QcAncak = 51;
    public static final int LongOperation_Get_QcBuah = 52;
    public static final int LongOperation_Get_SensusBjr = 53;
    public static final int LongOperation_Get_QcAncak = 54;
    public static final int LongOperation_Download_QcBuah = 55;
    public static final int LongOperation_Download_SensusBjr = 56;
    public static final int LongOperation_Download_QcAncak = 57;
    public static final int LongOperation_Count_QcAncak = 58;
    public static final int LongOperation_Upload_TransaksiPanenNfc = 59;
    public static final int LongOperation_Upload_TransaksiAngkutNfc = 60;
    public static final int LongOperation_Upload_CrashReport = 61;
    public static final int LongOperation_Cek_DataUpload = 62;
    public static final int LongOperation_Get_SPK = 63;
    public static final int LongOperation_Download_SPK = 64;
    public static final int LongOperation_UpdateMasterPemanen = 65;
    public static final int LongOperation_UpdateMasterOperator = 66;
    public static final int LongOperation_CountNfc = 67;
    public static final int LongOperation_RestoreMaster = 68;
    public static final int LongOperation_Opname_NFC = 69;
    public static final int LongOperation_Upload_OpnameNFC = 70;
    public static final int LongOperation_Get_OpnameNFC = 71;
    public static final int LongOperation_Download_OpnameNFC = 72;
    public static final int LongOperation_BJR = 73;
    public static final int LongOperation_Get_KonfigurasiGerdang = 74;
    public static final int LongOperation_Download_KonfigurasiGerdang = 75;
    public static final int LongOperation_Upload_GerdangDetail = 79;
    public static final int LongOperation_Upload_LogActivity = 80;
    public static final int LongOperation_Get_TPHReasonUnharvestMaster = 81;
    public static final int LongOperation_Download_TPHReasonUnharvestMaster = 82;
    public static final int LongOperation_Get_GetBlockMekanisasi = 83;
    public static final int LongOperation_Download_GetBlockMekanisasi = 84;
    public static final int LongOperation_My_SPK= 85;
    public static final int LongOperation_My_BlockMekanisasi= 86;
    public static final int LongOperation_Upload_LOG_SPB = 87;
    public static final int LongOperation_Download_Master_cages = 88;
    public static final int LongOperation_Get_Cages = 89;
    public static final int LongOperation_My_Cages= 90;

    public static final int ButtonGroup_Lokal = 0;
    public static final int ButtonGroup_Public = 1;

    int flagCekData;
    public ConnectionPresenter presenter;
    MainMenuActivity mainMenuActivity;
    public SetUpDataSyncHelper setUpDataSyncHelper;
    RestoreHelper restoreHelper;
    RestoreMasterHelper restoreMasterHelper;
    AlertDialog progressDialog;
    JSONObject objSetUp;
    ServiceResponse responseApi;
    UploadHelper uploadHelper;
    SyncHistoryHelper syncHistoryHelper;
    QrDoubleSPBHelpper qrDoubleSPBHelpper;
    MekanisasiHelper mekanisasiHelper;
    public CoordinatorLayout myCoordinatorLayout;
    RecyclerView rv;
    public SegmentedButtonGroup segmentedButtonGroup;
    boolean isVisibleToUser;

    public boolean adaDataTransaksi;
    public boolean adaDataMasterBaru;
    public ArrayList<GangList> gangLists;
    public ArrayList<SupervisionList> supervisionListsAll;
    public SuperVision superVision;
    public DynamicParameterPenghasilan dynamicParameterPenghasilan;

    public static SettingFragment getInstance() {
        SettingFragment fragment = new SettingFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.setting_layout, null, false);
        mainMenuActivity = (MainMenuActivity) getActivity();
        mainMenuActivity.connectionPresenter = new ConnectionPresenter(this);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        main();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        segmentedButtonGroup = view.findViewById(R.id.segmentedButtonGroup);
        rv = view.findViewById(R.id.rv);
        myCoordinatorLayout = view.findViewById(R.id.myCoordinatorLayout);
        setUpDataSyncHelper = new SetUpDataSyncHelper(getActivity());
        restoreHelper = new RestoreHelper(getActivity());
        restoreMasterHelper = new RestoreMasterHelper(getActivity());
        uploadHelper = new UploadHelper(getActivity());
        syncHistoryHelper = new SyncHistoryHelper(getActivity());
        qrDoubleSPBHelpper = new QrDoubleSPBHelpper(getActivity());
        mekanisasiHelper = new MekanisasiHelper(getActivity());
        main();
    }

    private void main(){
        if (isVisibleToUser && getActivity() != null) {
            segmentedButtonGroup.setVisibility(View.GONE);
//            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
//            if(preferences.getBoolean(HarvestApp.CONNECTION_PREF,false)){
//                segmentedButtonGroup.setPosition(ButtonGroup_Public);
//            }else{
//                segmentedButtonGroup.setPosition(ButtonGroup_Lokal);
//            }
//
//            segmentedButtonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
//                @Override
//                public void onClickedButton(int position) {
//                    switch (position){
//                        case 0: {
//                            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
//                            SharedPreferences.Editor editor = preferences.edit();
//                            editor.putBoolean(HarvestApp.CONNECTION_PREF, false);
//                            editor.apply();
//                            break;
//                        }
//                        case 1: {
//                            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
//                            SharedPreferences.Editor editor = preferences.edit();
//                            editor.putBoolean(HarvestApp.CONNECTION_PREF, true);
//                            editor.apply();
//                            break;
//                        }
//                    }
//                }
//            });

            mainMenuActivity.ivLogo.setVisibility(View.VISIBLE);
            mainMenuActivity.btnFilter.setVisibility(View.GONE);

            GlobalHelper.hideKeyboard(getActivity());
            ArrayList<ModelRecyclerView> arrayList = new ArrayList<>();
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_scan_qr),
                    getString(R.string.double_qr_spb), getString(R.string.double_qr_spb_info), String.valueOf(GlobalHelper.SETTING_DOUBLE_QR)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_nfc),
                    getString(R.string.format_nfc), getString(R.string.format_nfc_info), String.valueOf(GlobalHelper.SETTING_FORMATNFC)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_nfc_card),
                    getString(R.string.format_nfc_opname), getString(R.string.format_nfc_opname_info), String.valueOf(GlobalHelper.SETTING_OPNAMENFC)));
//            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_camera),
//                    getString(R.string.scan_qr), getString(R.string.scan_qr_info), String.valueOf(GlobalHelper.SETTING_SCANQR)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_sync),
                    getString(R.string.sync), getString(R.string.sync_info), String.valueOf(GlobalHelper.SETTING_SYNC)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_history),
                    getString(R.string.sync_history), getString(R.string.sync_history_info), String.valueOf(GlobalHelper.SETTING_HISTORY_SYNC)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_map),
                    getString(R.string.map), getString(R.string.map_info), String.valueOf(GlobalHelper.SETTING_MAP)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_backup),
                    getString(R.string.restore), getString(R.string.restore_info), String.valueOf(GlobalHelper.SETTING_BACKUP)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_master_data_bw),
                    getString(R.string.restoremaster), getString(R.string.restoremaster_info), String.valueOf(GlobalHelper.SETTING_RESTOREMASTER)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_calculator),
                    getString(R.string.count_nfc), getString(R.string.count_nfc_info), String.valueOf(GlobalHelper.SETTING_COUNT_NFC)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_pemanen),
                    getString(R.string.my_pemanen), getString(R.string.my_pemanen_info), String.valueOf(GlobalHelper.SETTING_PEMANEN)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_sensus_bjr_bw),
                    getString(R.string.cek_bjr), getString(R.string.cek_bjr_info), String.valueOf(GlobalHelper.SETTING_BJR)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_contractor),
                    getString(R.string.cek_spk), getString(R.string.cek_spk_info), String.valueOf(GlobalHelper.SETTING_SPK)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_cages),getString(R.string.cek_cages), getString(R.string.cek_cages_info), String.valueOf(GlobalHelper.SETTING_CAGES)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_trucking_bw),
                    getString(R.string.cek_block_mekanisasi), getString(R.string.cek_block_mekanisasi_info), String.valueOf(GlobalHelper.SETTING_BLOCK_MEKANISAIS)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_spb),
                    getString(R.string.report), getString(R.string.report_info), String.valueOf(GlobalHelper.SETTING_REPORT)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_handshake),
                    getString(R.string.assistensi), getString(R.string.assistensi_info), String.valueOf(SETTING_ASSISTENSI)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_about_application),
                    getString(R.string.about_application), getString(R.string.about_application_info), String.valueOf(GlobalHelper.SETTING_ABOUT_APPLICATION)));

            SettingItemAdapter adapter = new SettingItemAdapter(getActivity(), arrayList);
            rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
            rv.setAdapter(scaleInAnimationAdapter);

            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            dynamicParameterPenghasilan = GlobalHelper.getAppConfig().get(GlobalHelper.getEstate().getEstCode());
        }
    }

    public void questionLanjutDownload(){

        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, Context.MODE_PRIVATE);
        String sync_time = prefer.getString(HarvestApp.SYNC_TIME,null);
        Gson gson = new Gson();
        LastSyncTime lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
        HashMap<String,SyncTime> syncTimeHashMap = lastSyncTime.getSyncTimeHashMap();
        SyncTime syncTimeTransaksi = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
        SyncTime syncTimeMaster = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);

        if(!syncTimeTransaksi.isSelected() && !syncTimeMaster.isSelected()){
            Toast.makeText(getActivity(), "Upload Berhasil", Toast.LENGTH_LONG).show();
            return;
        }

        String message = "Upload Berhasil \nApakah Anda Ingin Memulai Download Data ?";
        AlertDialog alertDialog = new AlertDialog.Builder(mainMenuActivity, R.style.MyAlertDialogStyle)
                .setTitle("Perhatian")
                .setMessage(message)
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LongOperation = LongOperation_BackUp_HMS;
                        startLongOperation();
                        dialog.dismiss();
                    }
                })
                .create();
        alertDialog.show();
    }

    class SettingItemAdapter extends RecyclerView.Adapter<SettingItemAdapter.Holder>{
        Context context;
        ArrayList<ModelRecyclerView> originLists;
        public SettingItemAdapter(Context context, ArrayList<ModelRecyclerView> modelRecyclerViews){
            this.context = context;
            originLists = modelRecyclerViews;
        }

        @Override
        public SettingItemAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.row_setting,parent,false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(SettingItemAdapter.Holder holder, int position) {
            holder.setValue(originLists.get(position));
        }

        @Override
        public int getItemCount() {
            return originLists!=null ? originLists.size() : 0;
        }

        public class Holder extends RecyclerView.ViewHolder {

            CardView cv;
            ImageView iv;
            TextView tv;
            TextView tv1;

            public Holder(View itemView) {
                super(itemView);
                cv = (CardView) itemView.findViewById(R.id.cv);
                iv = (ImageView) itemView.findViewById(R.id.iv);
                tv = (TextView) itemView.findViewById(R.id.tv);
                tv1 = (TextView) itemView.findViewById(R.id.tv1);
            }

            public void setValue(ModelRecyclerView value) {
                iv.setImageResource(Integer.parseInt(value.getLayout()));
                tv.setText(value.getNama());
                tv1.setText(value.getNama1());
                cv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ActivityLoggerHelper.recordHitTime(SettingFragment.class.getSimpleName(),value.getNama1(),new Date(),new Date());

                        switch (Integer.parseInt(value.getNama2())){
                            case GlobalHelper.SETTING_SCANQR:
                                Intent intent = new Intent(getActivity(), QRScan.class);
                                startActivityForResult(intent,GlobalHelper.RESULT_SCAN_QR);
                                break;
                            case GlobalHelper.SETTING_SYNC:
                                LongOperation = LongOperation_Cek_DataUpload;
                                flagCekData = GlobalHelper.SETTING_SYNC;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_BACKUP:
                                LongOperation = LongOperation_Cek_DataUpload;
                                flagCekData = GlobalHelper.SETTING_BACKUP;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_RESTOREMASTER:
                                LongOperation = LongOperation_Cek_DataUpload;
                                flagCekData = GlobalHelper.SETTING_RESTOREMASTER;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_MAP:
                                LongOperation = LongOperation_Map;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_PEMANEN:
                                LongOperation = LongOperation_My_Pemanen;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_SPK:
                                LongOperation = LongOperation_My_SPK;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_BLOCK_MEKANISAIS:
                                LongOperation = LongOperation_My_BlockMekanisasi;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_COUNT_NFC:
                                LongOperation = LongOperation_CountNfc;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_FORMATNFC:
                                LongOperation = LongOperation_Format_NFC;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_OPNAMENFC:
                                LongOperation = LongOperation_Opname_NFC;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_BJR:
                                LongOperation = LongOperation_BJR;
                                startLongOperation();
                                break;
                            case GlobalHelper.SETTING_DOUBLE_QR:
                                qrDoubleSPBHelpper.choseQrSPB();
                                break;
                            case GlobalHelper.SETTING_HISTORY_SYNC:
                                syncHistoryHelper.showAllSyncHistroy();
                                break;
                            case GlobalHelper.SETTING_REPORT:
//                                Toast.makeText(getActivity(), "Coming soon", Toast.LENGTH_SHORT).show();
                                Intent intentReport = new Intent(getActivity(), ReportActivity.class);
                                startActivity(intentReport);
                                break;
                            case SETTING_ASSISTENSI:
                                Intent intentAssistensi = new Intent(getActivity(), AssistensiActivity.class);
                                startActivity(intentAssistensi);
                                break;
                            case GlobalHelper.SETTING_ABOUT_APPLICATION:
                                GlobalHelper.menuAboutApplication(getActivity());
                                break;
                            case GlobalHelper.SETTING_CAGES:
                                LongOperation = LongOperation_My_Cages;
                                startLongOperation();
                                break;
                        }
                    }
                });

            }
        }
    }

    private void startLongOperation(){
        new LongOperation().execute(String.valueOf(LongOperation));
    }

    public void startLongOperation(int longOperation){
        LongOperation = longOperation;
        new LongOperation().execute(String.valueOf(LongOperation));
    }

    public class LongOperation extends AsyncTask<String, String, String> {
        boolean skip = false;
        boolean error = false;
        Snackbar snackbar;
        int statLongOperation;

        public LongOperation(){}

        public LongOperation(int statLongOperation) {
            this.statLongOperation = statLongOperation;
            LongOperation = statLongOperation;
        }

        @Override
        protected void onPreExecute() {

            mainMenuActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(progressDialog != null){
                        progressDialog.dismiss();
                    }


                    String text ="";
                    switch (LongOperation){
                        case LongOperation_Upload_TPH:
                            text = getActivity().getResources().getString(R.string.setup_data_tph);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_TPH:
                            text = getActivity().getResources().getString(R.string.update_data_tph);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_TPH:
                            text = getActivity().getResources().getString(R.string.get_data_tph);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Count_TPH:
                            text = getActivity().getResources().getString(R.string.setup_data_Counter_tph);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_Langsiran:
                            text = getActivity().getResources().getString(R.string.setup_data_langsiran);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_Langsiran:
                            text = getActivity().getResources().getString(R.string.update_data_langsiran);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_Langsiran:
                            text = getActivity().getResources().getString(R.string.get_data_langsiran);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Count_Langsiran:
                            text = getActivity().getResources().getString(R.string.setup_data_Counter_langsiran);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_TransaksiPanen:
                            text =getActivity().getResources().getString(R.string.setup_data_TransaksiPanen);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_UpdateMasterPemanen:
                            text =getActivity().getResources().getString(R.string.get_master_pemanen);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_TransaksiPanen:
                            text =getActivity().getResources().getString(R.string.update_data_transaksi_panen);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Upload_TransaksiPanenNfc:
                            text =getActivity().getResources().getString(R.string.setup_data_TransaksiPanen_NFC);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_TransaksiPanen:
                            text = getActivity().getResources().getString(R.string.get_data_transaksi_panen);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Count_TransaiskPanen:
                            text = getActivity().getResources().getString(R.string.setup_data_Counter_transaksi_panen);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_TransaksiAngkut:
                            text = getActivity().getResources().getString(R.string.setup_data_TransaksiAngkut);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_TransaksiAngkut:
                            text =getActivity().getResources().getString(R.string.update_data_transaksi_angkut);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Upload_TransaksiAngkutNfc:
                            text = getActivity().getResources().getString(R.string.setup_data_TransaksiAngkut_NFC);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_TransaksiAngkut:
                            text = getActivity().getResources().getString(R.string.get_data_transaksi_angkut);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Count_TransaiskAngut:
                            text = getActivity().getResources().getString(R.string.setup_data_Counter_transaksi_angkut);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_TransaksiSPB:
                            text = getActivity().getResources().getString(R.string.setup_data_TransaksiSPB);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_TransaksiSPB:
                            text =getActivity().getResources().getString(R.string.update_data_transaksi_spb);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_TransaksiSpb:
                            text = getActivity().getResources().getString(R.string.get_data_transaksi_spb);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Count_TransaiskSpb:
                            text = getActivity().getResources().getString(R.string.setup_data_Counter_transaksi_spb);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_UpdateMasterOperator:
                            text = getActivity().getResources().getString(R.string.get_master_operator);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_MasterUser:
                            text = getActivity().getResources().getString(R.string.get_master_user);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_MasterUser:
                            text = getActivity().getResources().getString(R.string.download_master_user);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_Pemanen:
                            text = getActivity().getResources().getString(R.string.setup_data_pemanen);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_Pemanen:
                            text = getActivity().getResources().getString(R.string.get_master_pemanen);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_Pemanen:
                            text = getActivity().getResources().getString(R.string.download_master_pemanen);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_QcBuah:
                            text = getActivity().getResources().getString(R.string.setup_data_Qc_Buah);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_QcBuah:
                            text = getActivity().getResources().getString(R.string.get_qc_buah_list);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_QcBuah:
                            text = getActivity().getResources().getString(R.string.download_qc_buah_list);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_QcAncak:
                            text = getActivity().getResources().getString(R.string.setup_data_Qc_Buah);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_QcAncak:
                            text = getActivity().getResources().getString(R.string.get_qc_ancak_list);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_QcAncak:
                            text = getActivity().getResources().getString(R.string.download_qc_ancak_list);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Count_QcAncak:
                            text = getActivity().getResources().getString(R.string.setup_data_Counter_mutu_ancak);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_SensusBjr:
                            text = getActivity().getResources().getString(R.string.setup_data_Sensus_Bjr);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_SensusBjr:
                            text = getActivity().getResources().getString(R.string.get_sensus_bjr_list);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_SensusBjr:
                            text = getActivity().getResources().getString(R.string.download_sensus_bjr_list);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_Operator:
                            text = getActivity().getResources().getString(R.string.get_master_operator);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_Operator:
                            text = getActivity().getResources().getString(R.string.download_master_operator);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_Vehicle:
                            text = getActivity().getResources().getString(R.string.get_master_vehicle);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_Vehicle:
                            text = getActivity().getResources().getString(R.string.get_master_vehicle);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_AfdelingAssistantByEstate:
                            text = getActivity().getResources().getString(R.string.get_master_assistent_afd);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_AfdelingAssistantByEstate:
                            text = getActivity().getResources().getString(R.string.download_master_assistent_afd);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_ApplicationConfiguration:
                            text = getActivity().getResources().getString(R.string.get_master_app_config);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_ApplicationConfiguration:
                            text = getActivity().getResources().getString(R.string.download_master_app_config);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_SupervisionListByEstate:
                            text = getActivity().getResources().getString(R.string.get_master_supervision_list);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_SupervisionListByEstate:
                            text = getActivity().getResources().getString(R.string.download_master_supervision_list);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Count_TPanenSupervision:
                            text = getActivity().getResources().getString(R.string.setup_data_Counter_tpanenSupervision);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_SetUpSupervision:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Upload_Supervision:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_BJRInformation:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_BJRInformation:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_TPHReasonUnharvestMaster:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_GetBlockMekanisasi:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Download_TPHReasonUnharvestMaster:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_GetBlockMekanisasi:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_KonfigurasiGerdang:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_KonfigurasiGerdang:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_ISCCInformation:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_ISCCInformation:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Get_SPK:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_SPK:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_OpnameNFC:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_GerdangDetail:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_LOG_SPB:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_CrashReport:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Upload_LogActivity:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_BackUp_HMS:
                            text = getActivity().getResources().getString(R.string.backup_data_hms);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Restore_HMS:
                            text = getActivity().getResources().getString(R.string.restore);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_RestoreMaster:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;

                        case LongOperation_Map:
                            text = getActivity().getResources().getString(R.string.map);
                            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_My_Pemanen:
                            text = getActivity().getResources().getString(R.string.wait);
                            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_My_SPK:
                            text = getActivity().getResources().getString(R.string.wait);
                            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_My_Cages:
                            text = getActivity().getResources().getString(R.string.wait);
                            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_My_BlockMekanisasi:
                            text = getActivity().getResources().getString(R.string.wait);
                            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_CountNfc:
                            text = getActivity().getResources().getString(R.string.wait);
                            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Format_NFC:
                            text = getActivity().getResources().getString(R.string.wait);
                            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Opname_NFC:
                            text = getActivity().getResources().getString(R.string.wait);
                            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_BJR:
                            text = getActivity().getResources().getString(R.string.wait);
                            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Cek_DataUpload:
                            text = getActivity().getResources().getString(R.string.wait);
                            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Get_Cages:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                        case LongOperation_Download_Master_cages:
                            text = getActivity().getResources().getString(R.string.wait);
                            progressDialog = WidgetHelper.showWaitingDialog(getActivity(),text);
                            break;
                    }

                    Log.d("SettingFragment","onPreExecute "+text);
                }
            });
            objSetUp = new JSONObject();
            skip = false;
            error = false;

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                switch (Integer.parseInt(strings[0])) {
                    case LongOperation_Cek_DataUpload:
                        adaDataTransaksi = false;
                        adaDataMasterBaru = false;

                        if(!adaDataTransaksi) {
                            objSetUp = setUpDataSyncHelper.UploadTransaksiPanen(true, this);
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                adaDataTransaksi = true;
                            }
                        }
                        if(!adaDataTransaksi) {
                            objSetUp = setUpDataSyncHelper.UploadTransaksiAngkut(true, this);
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                adaDataTransaksi = true;
                            }
                        }
                        if(!adaDataTransaksi) {
                            objSetUp = setUpDataSyncHelper.UploadTransaksiSpb(true, this);
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                adaDataTransaksi = true;
                            }
                        }
                        if(!adaDataTransaksi) {
                            objSetUp = setUpDataSyncHelper.UploadTph(true, this);
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                adaDataTransaksi = true;
                                adaDataMasterBaru = true;
                            }
                        }
                        if(!adaDataTransaksi) {
                            objSetUp = setUpDataSyncHelper.UploadLangsiran(true, this);
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                adaDataTransaksi = true;
                                adaDataMasterBaru = true;
                            }
                        }
                        if(!adaDataTransaksi) {
                            objSetUp = setUpDataSyncHelper.UploadQcAncak(true, this);
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                adaDataTransaksi = true;
                            }
                        }
                        if(!adaDataTransaksi) {
                            objSetUp = setUpDataSyncHelper.UploadQcBuah(true, this);
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                adaDataTransaksi = true;
                            }
                        }
                        if(!adaDataTransaksi) {
                            objSetUp = setUpDataSyncHelper.UploadSensusBjr(true, this);
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                adaDataTransaksi = true;
                            }
                        }
                        if(!adaDataTransaksi) {
                            objSetUp = setUpDataSyncHelper.UploadOpnameNFC(true, this);
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                adaDataTransaksi = true;
                            }
                        }
                        if(!adaDataTransaksi) {
                            objSetUp = setUpDataSyncHelper.UploadGerdangDetail(true, this);
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                adaDataTransaksi = true;
                            }
                        }
                        break;
//                    supervision
                    case LongOperation_SetUpSupervision:
                        objSetUp = setUpDataSyncHelper.UploadTransaksiPanen(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {

                                gangLists = setUpDataSyncHelper.getGangSupervision(true, this);
                                supervisionListsAll = setUpDataSyncHelper.getSupervision(true, this);

                                Long userId = Long.parseLong(GlobalHelper.getUser().getUserID());
                                SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.SUPERVISION, Context.MODE_PRIVATE);
                                Gson gson = new Gson();
                                String stringSuperVision = preferences.getString(HarvestApp.SUPERVISION, null);
                                SuperVision shareSuperVision = null;
                                if (stringSuperVision != null) {
                                    shareSuperVision = gson.fromJson(stringSuperVision, SuperVision.class);
                                }

                                if(shareSuperVision != null) {
                                    superVision = new SuperVision();
                                    for (int i = 0; i < gangLists.size(); i++) {
                                        GangList gangItem = gangLists.get(i);
                                        if (gangItem.getAfdeling().equals(shareSuperVision.getGangList().getAfdeling())) {
                                            superVision.setGangList(gangItem);
                                            break;
                                        }
                                    }

                                    for (int i = 0; i < supervisionListsAll.size(); i++) {
                                        SupervisionList item = supervisionListsAll.get(i);
                                        if(shareSuperVision.getS1() != null) {
                                            if(shareSuperVision.getS1().getNik() != null) {
                                                if (item.getNik().equalsIgnoreCase(shareSuperVision.getS1().getNik())) {
                                                    superVision.setS1(item);
                                                }
                                            }
                                        }
                                        if(shareSuperVision.getS2() != null) {
                                            if(shareSuperVision.getS2().getNik() != null) {
                                                if (item.getNik().equalsIgnoreCase(shareSuperVision.getS2().getNik())) {
                                                    superVision.setS2(item);
                                                }
                                            }
                                        }
                                        if(shareSuperVision.getS3() != null) {
                                            if(shareSuperVision.getS3().getNik() != null) {
                                                if (item.getUserID().equals(userId)) {
                                                    superVision.setS3(item);
                                                }else if (item.getNik().equalsIgnoreCase(shareSuperVision.getS3().getNik())) {
                                                    superVision.setS3(item);
                                                }
                                            }
                                        }
                                        if(shareSuperVision.getS4() != null) {
                                            if(shareSuperVision.getS4().getNik() != null) {
                                                if (item.getNik().equalsIgnoreCase(shareSuperVision.getS4().getNik())) {
                                                    superVision.setS4(item);
                                                }
                                            }
                                        }

                                        JSONObject objProgres = new JSONObject();
                                        objProgres.put("ket", "Get User Gang");
                                        objProgres.put("persen", (i * 100 / supervisionListsAll.size()));
                                        objProgres.put("count", i);
                                        objProgres.put("total", supervisionListsAll.size());
                                        publishProgress(objProgres);
                                    }
                                }else {
                                    for (int i = 0; i < supervisionListsAll.size(); i++) {
                                        SupervisionList item = supervisionListsAll.get(i);
                                        if (item.getUserID().equals(userId)) {
                                            superVision = new SuperVision();
                                            for (int j = 0; j < gangLists.size(); j++) {
                                                GangList gangItem = gangLists.get(j);
                                                if (gangItem.getAfdeling().equals(item.getAfdeling())) {
                                                    if (shareSuperVision != null) {
                                                        if(shareSuperVision.getS3()  != null) {
                                                            if(shareSuperVision.getS3().getNik() != null) {
                                                                if (shareSuperVision.getS3().getNik().equals(item.getNik())) {
                                                                    superVision = shareSuperVision;
                                                                } else {
                                                                    superVision.setGangList(gangItem);
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        superVision.setGangList(gangItem);
                                                    }
                                                    break;
                                                }
                                            }
                                            superVision.setS3(item);
                                            break;
                                        }

                                        JSONObject objProgres = new JSONObject();
                                        objProgres.put("ket", "Get User Gang");
                                        objProgres.put("persen", (i * 100 / supervisionListsAll.size()));
                                        objProgres.put("count", i);
                                        objProgres.put("total", supervisionListsAll.size());
                                        publishProgress(objProgres);
                                    }
                                }
                                if (gangLists == null || supervisionListsAll == null) {
                                    skip = true;
                                }
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Upload_Supervision:
                        Gson gson = new Gson();
                        if(superVision != null) {
                            try {
                                PackageInfo pi = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(),0);
                                String userId = GlobalHelper.getUser().getUserID();
                                objSetUp = new JSONObject(gson.toJson(superVision));
                                objSetUp.put("userId",userId);
                                objSetUp.put("versionApp",pi.versionName);
                                mainMenuActivity.connectionPresenter.Upload_Supervision(objSetUp);
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                        }else{
                            skip = true;
                        }
                        break;

                    // TPH
                    case LongOperation_Upload_TPH:
                        // periapan data untuk upload tph jika tidak ada data yang akan di upload maka skip
                        objSetUp = setUpDataSyncHelper.UploadTph(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadData(this,Tag.PostTph,objSetUp.getJSONArray("data"),0);
//                                uploadHelper.uploadImages(this,Tag.PostTphImage,objSetUp.getJSONArray("data"),0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_TPH:
                        //akses api doang TPH
                        mainMenuActivity.connectionPresenter.GetTphListByEstate();
                        break;
                    case LongOperation_Download_TPH:
                        //update db tph
                        skip = !setUpDataSyncHelper.downLoadTph(responseApi,this);
                        break;
                    case LongOperation_Count_TPH:
                        mainMenuActivity.connectionPresenter.GetTPHCounterByUserID();
                        break;

                    //Langsiran
                    case LongOperation_Upload_Langsiran:
                        // periapan data untuk upload langsiran jika tidak ada data yang akan di upload maka skip
                        objSetUp = setUpDataSyncHelper.UploadLangsiran(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadImages(this,Tag.PostLangsiranImage,objSetUp.getJSONArray("data"),0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_Langsiran:
                        //akses api doang Langsiran
                        mainMenuActivity.connectionPresenter.GetLangsiranListByEstate();
                        break;
                    case LongOperation_Download_Langsiran:
                        //update db Langsiran
                        skip = !setUpDataSyncHelper.downLoadLangsiran(responseApi,this);
                        break;
                    case LongOperation_Count_Langsiran:
                        mainMenuActivity.connectionPresenter.GetLangsiranCounterByUserID();
                        break;

                    //Transaksi Panen
                    case LongOperation_Upload_TransaksiPanen:
                        // periapan data untuk upload Transaksi Panen jika tidak ada data yang akan di upload maka skip
                        objSetUp = setUpDataSyncHelper.UploadTransaksiPanen(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                try{
                                    if (dynamicParameterPenghasilan.getUploadFotoTransaksi().equals("1")) {
                                        uploadHelper.uploadImages(this, Tag.PostTransaksiPanenImage, objSetUp.getJSONArray("data"), 0);
                                    } else {
                                        uploadHelper.uploadData(this, Tag.PostTransaksiPanen, objSetUp.getJSONArray("data"), 0);
                                    }
                                }catch (Exception e){
                                    uploadHelper.uploadData(this, Tag.PostTransaksiPanen, objSetUp.getJSONArray("data"), 0);
                                }
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Upload_TransaksiPanenNfc:
                        objSetUp = setUpDataSyncHelper.UploadTransaksiPanenNfc(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadData(this,Tag.PostTransaksiPanenNFC,objSetUp.getJSONArray("data"),0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_TransaksiPanen:
                        //akses api doang Transaksi Panen
                        mainMenuActivity.connectionPresenter.GetTransaksiPanenListByEstate();
                        break;
                    case LongOperation_Download_TransaksiPanen:
                        //update db Transaksi Panen
                        skip = !setUpDataSyncHelper.downLoadTransaksiPanen(responseApi,this);
                        break;
                    case LongOperation_Count_TransaiskPanen:
                        mainMenuActivity.connectionPresenter.GetTPanenCounterByUserID();
                        break;
                    case LongOperation_UpdateMasterPemanen:
                        //update db Transaksi Panen
                        skip = !setUpDataSyncHelper.SetPemanenListByEstateFromTransaksiPanen(responseApi,this);
                        break;

                    //Transaksi Angkut
                    case LongOperation_Upload_TransaksiAngkut:
                        // periapan data untuk upload Transaksi Panen jika tidak ada data yang akan di upload maka skip
                        objSetUp = setUpDataSyncHelper.UploadTransaksiAngkut(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                try {
                                    if (dynamicParameterPenghasilan.getUploadFotoTransaksi().equals("1")) {
                                        uploadHelper.uploadImages(this, Tag.PostTransaksiAngkutImage, objSetUp.getJSONArray("data"), 0);
                                    } else {
                                        uploadHelper.uploadData(this, Tag.PostTransaksiAngkut, objSetUp.getJSONArray("data"), 0);
                                    }
                                }catch (Exception e){
                                    uploadHelper.uploadData(this, Tag.PostTransaksiAngkut, objSetUp.getJSONArray("data"), 0);
                                }
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Upload_TransaksiAngkutNfc:
                        // periapan data untuk upload Transaksi Panen jika tidak ada data yang akan di upload maka skip
                        objSetUp = setUpDataSyncHelper.UploadTransaksiAngkutNfc(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadData(this,Tag.PostTransaksiAngkutNFC,objSetUp.getJSONArray("data"),0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_TransaksiAngkut:
                        //akses api doang Transaksi Angkut
                        mainMenuActivity.connectionPresenter.GetTransaksiAngkutListByEstate();
                        break;
                    case LongOperation_Download_TransaksiAngkut:
                        //update db Transaksi Angkut
                        skip = !setUpDataSyncHelper.downLoadTransaksiAngkut(responseApi,this);
                        break;
                    case LongOperation_Count_TransaiskAngut:
                        mainMenuActivity.connectionPresenter.GetTAngkutCounterByUserID();
                        break;
                    case LongOperation_UpdateMasterOperator:
                        //update db Transaksi Panen
                        skip = !setUpDataSyncHelper.SetOperatorListByEstateFromTransaksiSpb(responseApi,this);
                        break;


                    //Transaksi SPB
                    case LongOperation_Upload_TransaksiSPB:
                        // periapan data untuk upload Transaksi Panen jika tidak ada data yang akan di upload maka skip
                        objSetUp = setUpDataSyncHelper.UploadTransaksiSpb(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                try {
                                    if (dynamicParameterPenghasilan.getUploadFotoTransaksi().equals("1")) {
                                        uploadHelper.uploadImages(this, Tag.PostTransaksiSPBImage, objSetUp.getJSONArray("data"), 0);
                                    } else {
                                        uploadHelper.uploadData(this, Tag.PostTransaksiSPB, objSetUp.getJSONArray("data"), 0);
                                    }
                                }catch (Exception e){
                                    uploadHelper.uploadData(this, Tag.PostTransaksiSPB, objSetUp.getJSONArray("data"), 0);
                                }
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_TransaksiSpb:
                        //akses api doang Transaksi Angkut
                        mainMenuActivity.connectionPresenter.GetTransaksiSpbListByEstate();
                        break;
                    case LongOperation_Download_TransaksiSPB:
                        //update db Transaksi Angkut
                        skip = !setUpDataSyncHelper.downLoadTransaksiSpb(responseApi,this);
                        break;
                    case LongOperation_Count_TransaiskSpb:
                        mainMenuActivity.connectionPresenter.GetSPBCounterByUserID();
                        break;

                    // qc buah
                    case LongOperation_Upload_QcBuah:
                        objSetUp = setUpDataSyncHelper.UploadQcBuah(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadImages(this,Tag.PostQcBuahImages,objSetUp.getJSONArray("data"),0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_QcBuah:
                        //aksesapi doang Qc Buah
                        mainMenuActivity.connectionPresenter.GetQcBuahByEstate();
                        break;
                    case LongOperation_Download_QcBuah:
                        //update db Qc Buah
                        skip = !setUpDataSyncHelper.downLoadQcBuah(responseApi,this);
                        break;

                    //Sensus BJR
                    case LongOperation_Upload_SensusBjr:
                        objSetUp = setUpDataSyncHelper.UploadSensusBjr(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadImages(this,Tag.PostSensusBjrImages,objSetUp.getJSONArray("data"),0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_SensusBjr:
                        //aksesapi doang Sensus BJR
                        mainMenuActivity.connectionPresenter.GetSensusBJRByEstate();
                        break;
                    case LongOperation_Download_SensusBjr:
                        //update db Sensus
                        skip = !setUpDataSyncHelper.downLoadSensusBJR(responseApi,this);
                        break;

                    //Qc Ancak
                    case LongOperation_Upload_QcAncak:
                        objSetUp = setUpDataSyncHelper.UploadQcAncak(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                //qc ancak tidak pakai foto
                                uploadHelper.uploadData(this,Tag.PostQcAncak,objSetUp.getJSONArray("data"),0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_QcAncak:
                        mainMenuActivity.connectionPresenter.GetQCMutuAncakByEstate();
                        break;
                    case LongOperation_Download_QcAncak:
                        //update db Qc Ancak
                        skip = !setUpDataSyncHelper.downLoadQcAncak(responseApi,this);
                        break;
                    case LongOperation_Count_QcAncak:
                        mainMenuActivity.connectionPresenter.GetQCMutuAncakHeaderCounterByUserID();
                        break;

                    //Master Pemanen
                    case LongOperation_Upload_Pemanen:
                        objSetUp = setUpDataSyncHelper.UploadPemanenListByEstate(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                mainMenuActivity.connectionPresenter.UploadPemanen(objSetUp.getJSONArray("data"));
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_Pemanen:
                        //akses api doang Langsiran
                        mainMenuActivity.connectionPresenter.GetPemanenListByEstate();
                        break;
                    case LongOperation_Download_Pemanen:
                        //update db Langsiran
                        skip = !setUpDataSyncHelper.SetPemanenListByEstate(responseApi,this);
                        break;

                    //Opname NFC
                    case LongOperation_Upload_OpnameNFC:
                        objSetUp = setUpDataSyncHelper.UploadOpnameNFC(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadData(this,Tag.InsertOpnameNFC,objSetUp.getJSONArray("data"),0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_OpnameNFC:
                        //akses api doang OpnameNFC
                        mainMenuActivity.connectionPresenter.GetOpnameNFCByEstate();
                        break;
                    case LongOperation_Download_OpnameNFC:
                        //update db OpnameNFC
                        skip = !setUpDataSyncHelper.SetOpnameNFCByEstate(responseApi,this);
                        break;

                    case LongOperation_Upload_GerdangDetail:
                        objSetUp = setUpDataSyncHelper.UploadGerdangDetail(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadData(this,Tag.InsertGerdangDetail,objSetUp.getJSONArray("data"),0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;

                    case LongOperation_Upload_LOG_SPB:
                        objSetUp = setUpDataSyncHelper.UploadLogSPB(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                mainMenuActivity.connectionPresenter.UploadLogSPB(objSetUp.getJSONArray("data"));
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;

                    case LongOperation_Upload_CrashReport:
                        objSetUp = setUpDataSyncHelper.UploadCrashReport(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                mainMenuActivity.connectionPresenter.UploadCrashReport(objSetUp.getJSONArray("data"));
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;

                    case LongOperation_Upload_LogActivity:
                        objSetUp = setUpDataSyncHelper.UploadLogActivity(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadData(this,Tag.LogActivity,objSetUp.getJSONArray("data"),0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;

                    //Backup
                    case LongOperation_BackUp_HMS:
//                        if(!GlobalHelper.harusBackup()){
                            setUpDataSyncHelper.backupDataHMS(this);
//                        }
                        break;
                    //Restore
                    case LongOperation_Restore_HMS:
                        skip =!restoreHelper.choseRestore(this);
                        break;
                    case LongOperation_RestoreMaster:
                        skip =!restoreMasterHelper.choseRestoreMaster();
                        break;
                    //Master User
                    case LongOperation_Get_MasterUser:
                        //akses api doang Langsiran
                        mainMenuActivity.connectionPresenter.GetMasterUserByEstate();
                        break;
                    case LongOperation_Download_MasterUser:
                        //update db Langsiran
                        skip = !setUpDataSyncHelper.SetMasterUserByEstate(responseApi,this);
                        break;

                    //Master Operator
                    case LongOperation_Get_Operator:
                        //akses api doang
                        mainMenuActivity.connectionPresenter.GetOperatorListByEstate();
                        break;
                    case LongOperation_Download_Operator:
                        //update db
                        skip = !setUpDataSyncHelper.SetOperatorListByEstate(responseApi,this);
                        break;

                    //Master Vehicle
                    case LongOperation_Get_Vehicle:
                        //akses api doang
                        mainMenuActivity.connectionPresenter.GetVehicleListByEstate();
                        break;
                    case LongOperation_Download_Vehicle:
                        //update db
                        skip = !setUpDataSyncHelper.setVehicleListByEstate(responseApi,this);
                        break;

                    //Master AfdelingAssistantByEstate
                    case LongOperation_Get_AfdelingAssistantByEstate:
                        //akses api doang
                        mainMenuActivity.connectionPresenter.GetAfdelingAssistantByEstate();
                        break;
                    case LongOperation_Download_AfdelingAssistantByEstate:
                        //update db
                        skip = !setUpDataSyncHelper.downLoadAfdelingAssistant(responseApi,this);
                        break;

                    //Master AfdelingAssistantByEstate
                    case LongOperation_Get_ApplicationConfiguration:
                        //akses api doang
                        mainMenuActivity.connectionPresenter.GetApplicationConfiguration();
                        break;
                    case LongOperation_Download_ApplicationConfiguration:
                        //update db
                        skip = !setUpDataSyncHelper.downLoadApplicationConfiguration(responseApi,this);
                        break;

                    //Master AfdelingAssistantByEstate
                    case LongOperation_Get_SupervisionListByEstate:
                        //akses api doang
                        mainMenuActivity.connectionPresenter.GetSupervisionListByEstate();
                        break;
                    case LongOperation_Download_SupervisionListByEstate:
                        //update db
                        skip = !setUpDataSyncHelper.downLoadSupervisionListByEstate(responseApi,this);
                        break;
                    case LongOperation_Count_TPanenSupervision:
                        mainMenuActivity.connectionPresenter.GetTPanenSupervisionCounterByUserID();
                        break;

                    //Master PKS
                    case LongOperation_Get_PKS:
                        //akses api doang Langsiran
                        mainMenuActivity.connectionPresenter.GetPksListByEstate();
                        break;
                    case LongOperation_Download_PKS:
                        //update db Langsiran
                        skip = !setUpDataSyncHelper.SetPksListByEstate(responseApi,this);
                        break;


                    //KonfigurasiGerdang
                    case LongOperation_Get_KonfigurasiGerdang:
                        mainMenuActivity.connectionPresenter.GetConfigGerdangByEstate();
                        break;
                    case LongOperation_Download_KonfigurasiGerdang:
                        skip= !setUpDataSyncHelper.SetConfigGerdangByEstate(responseApi,this);
                        break;

                    //TphUnharvestReason
                    case LongOperation_Get_TPHReasonUnharvestMaster:
                        mainMenuActivity.connectionPresenter.GetTPHReasonUnharvestMaster();
                        break;
                    case LongOperation_Download_TPHReasonUnharvestMaster:
                        skip= !setUpDataSyncHelper.SetTPHReasonUnharvestMaster(responseApi,this);
                        break;

                    case LongOperation_Get_GetBlockMekanisasi:
                        mainMenuActivity.connectionPresenter.GetBlockMekanisasi();
                        break;
                    case LongOperation_Download_GetBlockMekanisasi:
                        skip= !setUpDataSyncHelper.SetBlockMekanisasiMaster(responseApi,this);
                        break;

                    //BJR Informastion
                    case LongOperation_Get_BJRInformation:
                        mainMenuActivity.connectionPresenter.GetBJRInformation();
                        break;
                    case LongOperation_Download_BJRInformation:
                        skip= !setUpDataSyncHelper.downLoadBJRInformation(responseApi,this);
                        break;

                    //ISCC Information
                    case LongOperation_Get_ISCCInformation:
                        mainMenuActivity.connectionPresenter.GetISCCInformation();
                        break;
                    case LongOperation_Download_ISCCInformation:
                        skip=!setUpDataSyncHelper.downLoadISCCInformation(responseApi,this);
                        break;

                    //SPK
                    case LongOperation_Get_SPK:
                        mainMenuActivity.connectionPresenter.GetMasterSPKByEstate();
                        break;
                    case LongOperation_Download_SPK:
                        skip=!setUpDataSyncHelper.downLoadMasterSpk(responseApi,this);
                        break;

                    //CAGES
                    case LongOperation_Get_Cages:
                        mainMenuActivity.connectionPresenter.GetMasterCagesByEstate();
                        break;
                    case LongOperation_Download_Master_cages:
                        skip=!setUpDataSyncHelper.downloadMasterCages(responseApi, this);
                        break;
                }

                Log.d("SettingFragment ","doInBackground "+strings[0]);
                return strings[0];
            } catch (JSONException e) {
                e.printStackTrace();
                return "JSONException";
            }
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            mainMenuActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject objProgres = new JSONObject(String.valueOf(values[0]));

                        if(snackbar== null){
                            snackbar = Snackbar.make(myCoordinatorLayout,objProgres.getString("ket"),Snackbar.LENGTH_INDEFINITE);
                        }
                        snackbar.setText(String.format(objProgres.getString("ket") + " %d %s [%d/%d] ",
                                objProgres.getInt("persen"), "%", objProgres.getInt("count"), objProgres.getInt("total")));
                        snackbar.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.d("SettingFragment","onPostExecute "+result);
            if(snackbar != null){
                snackbar.dismiss();
            }
            if(result.equals("JSONException")){
                Toast.makeText(HarvestApp.getContext(),"Gagal Prepare Data Long Operation",Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }else {
                switch (Integer.parseInt(result)) {
                    case LongOperation_Cek_DataUpload:
                        ((BaseActivity) getActivity()).alertDialogBase.dismiss();
                        if(flagCekData == GlobalHelper.SETTING_SYNC){
                            uploadHelper.SelectUploadSync();
                        }else if(flagCekData == GlobalHelper.SETTING_BACKUP){
                            if(adaDataTransaksi){
                                Toast.makeText(HarvestApp.getContext(),"Mohon Upload Data Dahulu",Toast.LENGTH_LONG).show();
                            }else {
                                restoreHelper.showAllRestore();
                            }
                        }else if (flagCekData == GlobalHelper.SETTING_RESTOREMASTER){
                            if(adaDataMasterBaru) {
                                Toast.makeText(HarvestApp.getContext(),"Mohon Upload Data Dahulu",Toast.LENGTH_LONG).show();
                            }else{
                                restoreMasterHelper.showRestoreMaster();
                            }
                        }
                        break;
                    //supervision
                    case LongOperation_SetUpSupervision:
                        if(!skip) {
                            try {
                                progressDialog.dismiss();
                                uploadHelper.setSupervision(objSetUp.getJSONArray("data"));
                            } catch (JSONException e) {
                                progressDialog.dismiss();
                                Toast.makeText(HarvestApp.getContext(), "Data Panen Supervision Tidak Bisa Di Buka", Toast.LENGTH_SHORT).show();
                            }
                        }else if(skip){
                            LongOperation = LongOperation_Upload_TPH;
                            startLongOperation();
                        }else if (error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Supervision Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Upload_Supervision:
                        if(skip){
                            LongOperation = LongOperation_Upload_TPH;
                            startLongOperation();
                        }else if (error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Supervision Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    //TPH
                    case LongOperation_Upload_TPH:
                        if (skip) {
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Upload_Langsiran;
                            startLongOperation();
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_TPH:
                        if (skip) {
                            progressDialog.dismiss();
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update TPH", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Get_Langsiran;
                            startLongOperation();
                        }
                        break;

                    //Langsiran
                    case LongOperation_Upload_Langsiran:
                        if (skip) {
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Upload_TransaksiPanen;
                            startLongOperation();
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_Langsiran:
                        if (skip) {
                            progressDialog.dismiss();
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update Langsiran", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Get_TransaksiPanen;
                            startLongOperation();
                        }
                        break;

                    //Transaksi Panen
                    case LongOperation_Upload_TransaksiPanen:
                        if (skip) {
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Upload_TransaksiAngkut;
                            startLongOperation();
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Upload_TransaksiPanenNfc:
                        if (skip) {
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Upload_TransaksiAngkutNfc;
                            startLongOperation();
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_TransaksiPanen:
                        if (skip) {
                            progressDialog.dismiss();
                            WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Download Transaksi Panen");
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update Transaksi Panen", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Get_TransaksiAngkut;
                            startLongOperation();
                        }
                        break;
                    case LongOperation_UpdateMasterPemanen:
                        if (skip) {
                            progressDialog.dismiss();
                            WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Update Master Pemanen");
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update Master Pemanen", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Upload_TransaksiAngkut;
                            startLongOperation();
                        }
                        break;

                    //TransaksiAngkut
                    case LongOperation_Upload_TransaksiAngkut:
                        if (skip) {
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Upload_TransaksiSPB;
                            startLongOperation();
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Upload_TransaksiAngkutNfc:
                        if (skip) {
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Upload_QcBuah;
                            startLongOperation();
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_TransaksiAngkut:
                        if (skip) {
                            progressDialog.dismiss();
                            WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Download Transaksi Angkut");
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update Transaksi Angkut", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Get_TransaksiSpb;
                            startLongOperation();
                        }
                        break;

                        //TransaksiSPB
                    case LongOperation_Upload_TransaksiSPB:
                        if (skip) {
                            progressDialog.dismiss();
                            if(responseApi != null) {
                                LongOperation = LongOperation_UpdateMasterOperator;
                            }else {
                                LongOperation = LongOperation_Upload_TransaksiPanenNfc;
                            }
                            startLongOperation();
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_TransaksiSPB:
                        if (skip) {
                            progressDialog.dismiss();
                            WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Download Transaksi SPB");
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update Transaksi SPB", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Count_TPH;
                            startLongOperation();
                        }
                        break;

                    case LongOperation_UpdateMasterOperator:
                        if (skip) {
                            progressDialog.dismiss();
                            WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Update Master Operator");
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update Master Operator", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Upload_TransaksiPanenNfc;
                            startLongOperation();
                        }

                    //Qc MutuBuah
                    case LongOperation_Upload_QcBuah:
                        if (skip) {
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Upload_SensusBjr;
                            startLongOperation();
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_QcBuah:
                        if (skip) {
                            progressDialog.dismiss();
                            WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Download Qc Buah");
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update Qc Bauh", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Get_SensusBjr;
                            startLongOperation();
                        }
                        break;

                    //Qc Sensus BJR
                    case LongOperation_Upload_SensusBjr:
                        if (skip) {
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Upload_QcAncak;
                            startLongOperation();
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_SensusBjr:
                        if (skip) {
                            progressDialog.dismiss();
                            WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Download Sensus BJR");
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update Sensus BJR", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Get_QcAncak;
                            startLongOperation();
                        }
                        break;

                    //Qc Ancak
                    case LongOperation_Upload_QcAncak:
                        if (skip) {
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Upload_Pemanen;
                            startLongOperation();
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_QcAncak:
                        if (skip) {
                            progressDialog.dismiss();
                            WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Download Qc Ancak");
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update Qc Ancak", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            LongOperation = LongOperation_Count_TPH;
                            startLongOperation();
                        }
                        break;

                    //Pemanen
                    case LongOperation_Upload_Pemanen:
                        if (skip) {
                            progressDialog.dismiss();
                            SyncTimeHelper.updateFinishSyncTime(SyncTime.UPLOAD_TRANSAKSI);
                            new LongOperation(LongOperation_Upload_OpnameNFC).execute(String.valueOf(LongOperation_Upload_OpnameNFC));
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_Pemanen:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetPemanenByEstate",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_Operator).execute(String.valueOf(LongOperation_Get_Operator));
                        }
                        break;

                    //Opname NFC
                    case LongOperation_Upload_OpnameNFC:
                        if (skip) {
                            progressDialog.dismiss();
                            SyncTimeHelper.updateFinishSyncTime(SyncTime.UPLOAD_TRANSAKSI);
                            new LongOperation(LongOperation_Upload_GerdangDetail).execute(String.valueOf(LongOperation_Upload_GerdangDetail));
                        } else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Opname NFC Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_OpnameNFC:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetOpnameByEstate",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_Pemanen).execute(String.valueOf(LongOperation_Get_Pemanen));
                        }
                        break;

                    case LongOperation_Upload_GerdangDetail:
                        if (skip) {
                            progressDialog.dismiss();
                            SyncTimeHelper.updateFinishSyncTime(SyncTime.UPLOAD_TRANSAKSI);
                            new LongOperation(LongOperation_Upload_LOG_SPB).execute(String.valueOf(LongOperation_Upload_LOG_SPB));
                        }else if(error){
                            progressDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Gerdang Detail Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case LongOperation_Upload_LOG_SPB:
                        if (skip || error) {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Upload_CrashReport).execute(String.valueOf(LongOperation_Upload_CrashReport));
                        }
                        break;

                    case LongOperation_Upload_CrashReport:
                        if (skip || error) {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Upload_LogActivity).execute(String.valueOf(LongOperation_Upload_LogActivity));
                        }
                        break;

                    case LongOperation_Upload_LogActivity:
                        if (skip || error) {
                            progressDialog.dismiss();
                            questionLanjutDownload();
                        }
                        break;

                    //Backup
                    case LongOperation_BackUp_HMS:
                        progressDialog.dismiss();
                        LongOperation = LongOperation_Get_MasterUser;
                        startLongOperation();
                        break;
                    //restore
                    case LongOperation_Restore_HMS:
                        restoreHelper.closeRestore();
                        progressDialog.dismiss();
                        break;
                    case LongOperation_RestoreMaster:
                        restoreMasterHelper.closeRestoreMaster();
                        progressDialog.dismiss();
                        break;

                    //
                    //master user
                    case LongOperation_Download_MasterUser:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetMasterUserByEstate",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_OpnameNFC).execute(String.valueOf(LongOperation_Get_OpnameNFC));
                        }
                        break;

                    //Master Operator
                    case LongOperation_Download_Operator:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetOperatorByEstate",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_Vehicle).execute(String.valueOf(LongOperation_Get_Vehicle));
                        }
                        break;

                    //Master Vehicle
                    case LongOperation_Download_Vehicle:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetVehicleByEstate",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_AfdelingAssistantByEstate).execute(String.valueOf(LongOperation_Get_AfdelingAssistantByEstate));
                        }
                        break;

                    //Master AssistentAfdeling
                    case LongOperation_Download_AfdelingAssistantByEstate:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert AfdelingAssistantByEstate",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_ApplicationConfiguration).execute(String.valueOf(LongOperation_Get_ApplicationConfiguration));
                        }
                        break;

                    //Master Aplication Configuration
                    case LongOperation_Download_ApplicationConfiguration:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert AppConfigutarion",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_SupervisionListByEstate).execute(String.valueOf(LongOperation_Get_SupervisionListByEstate));
                        }
                        break;

                    //Master SupervisionList
                    case LongOperation_Download_SupervisionListByEstate:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert SupervisionList",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_TPHReasonUnharvestMaster).execute(String.valueOf(LongOperation_Get_TPHReasonUnharvestMaster));
                        }
                        break;

                    //Master TPH Reason
                    case LongOperation_Download_TPHReasonUnharvestMaster:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert TPHReasonUnharvestMaster",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_GetBlockMekanisasi).execute(String.valueOf(LongOperation_Get_GetBlockMekanisasi));
                        }
                        break;

                    //Master Block Mekanisasi
                    case LongOperation_Download_GetBlockMekanisasi:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetBlockMekanisasi",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_KonfigurasiGerdang).execute(String.valueOf(LongOperation_Get_KonfigurasiGerdang));
                        }
                        break;

                    //KonfigurasiGerdang
                    case LongOperation_Download_KonfigurasiGerdang:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert KonfigurasiGerdang",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_BJRInformation).execute(String.valueOf(LongOperation_Get_BJRInformation));
                        }
                        break;

                    //BJR information
                    case LongOperation_Download_BJRInformation:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert BJRInformation",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_ISCCInformation).execute(String.valueOf(LongOperation_Get_ISCCInformation));
                        }
                        break;

                    case LongOperation_Download_ISCCInformation:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert ISCCInformation",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_SPK).execute(String.valueOf(LongOperation_Get_SPK));
                        }
                        break;

                    //BJR information
                    case LongOperation_Download_SPK:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert ISCCInformation",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_PKS).execute(String.valueOf(LongOperation_Get_PKS));
                        }
                        break;

                    //PKS
                    case LongOperation_Download_PKS:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert PKS",Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        } else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_Cages).execute(String.valueOf(LongOperation_Get_Cages));
                        }
                        break;

                    //Cages
                    case LongOperation_Download_Master_cages:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(), "Gagal Insert GetCagesByEstate", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        } else {
                            progressDialog.dismiss();
                            new LongOperation(LongOperation_Get_TPH).execute(String.valueOf(LongOperation_Get_TPH));
                        }
                        break;

                    case LongOperation_Map:
                        HashMap<String,ArrayList<ReasonUnharvestTPH>> hashMapReasonUnharvestTPH = TphHelper.listReasonUnharvestTPH();
                        if(hashMapReasonUnharvestTPH.size() > 0) {
                            Intent i = new Intent(getActivity(), MapActivity.class);
                            startActivityForResult(i, GlobalHelper.RESULT_MAPACTIVITY);
                        }else{
                            Toast.makeText(getActivity(),"alasan tidak muncul mohon sync master sampai berhasil",Toast.LENGTH_SHORT).show();
                            ((BaseActivity) getActivity()).alertDialogBase.dismiss();
                        }
                        break;
                    case LongOperation_My_Pemanen:
                        Intent intent = new Intent(getActivity(), PemanenActivity.class);
                        startActivityForResult(intent,GlobalHelper.RESULT_PEMANENACTIVITY);
                        break;
                    case LongOperation_My_BlockMekanisasi:
                        Intent i = new Intent(getActivity(), MekanisasiBlockActivity.class);
                        startActivityForResult(i,GlobalHelper.RESULT_MEKANISASIBLOCKACTIVITY);
                        break;
                    case LongOperation_My_SPK:
                        Intent i1 = new Intent(getActivity(), SPKActivity.class);
                        startActivityForResult(i1,GlobalHelper.RESULT_SPKACTIVITY);
                        break;
                    case LongOperation_My_Cages:
                        Intent i6 = new Intent(getActivity(), CagesActivity.class);
                        startActivityForResult(i6,GlobalHelper.RESULT_CAGESACTIVITY);
                        break;
                    case LongOperation_CountNfc:
                        Intent i3 = new Intent(getActivity(), CountNfcActivity.class);
                        startActivityForResult(i3,GlobalHelper.RESULT_COUNTNFCACTIVITY);
                        break;
                    case LongOperation_Format_NFC:
                        Intent i2 = new Intent(getActivity(), FormatNfcActivity.class);
                        startActivityForResult(i2,GlobalHelper.RESULT_FORMATNFCACTIVITY);
                        break;
                    case LongOperation_Opname_NFC:
                        Intent i4 = new Intent(getActivity(), OpnameNfcActivity.class);
                        startActivityForResult(i4,GlobalHelper.RESULT_OPNAMENFCACTIVITY);
                        break;
                    case LongOperation_BJR:
                        Intent i5 = new Intent(getActivity(), CekBJRActivity.class);
                        startActivityForResult(i5,GlobalHelper.RESULT_CEKBJRACTIVITY);
                        break;
                }
            }
        }

        public void publishProgress(JSONObject objProgres){
            onProgressUpdate(String.valueOf(objProgres));
        }

        private TextView getDialogText() {
            if (progressDialog.isShowing()) {
                View v = progressDialog.getWindow().getDecorView();
                return (TextView) v.findViewById(R.id.cust_tv_progress);
            }
            return null;
        }
    }


    @Override
    public void successResponse(ServiceResponse serviceResponse) {
        try {
            switch (serviceResponse.getTag()) {
                // TPH
                case PostTphImage:
                    uploadHelper.uploadData(this,Tag.PostTph,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    break;
                case PostTph:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        uploadHelper.uploadImages(this,Tag.PostTphImage,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    }else{
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai TPH");
                        progressDialog.dismiss();
                        LongOperation = LongOperation_Upload_Langsiran;
                        startLongOperation();
                    }
                    break;
                case GetTphListByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai TPH");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    LongOperation = LongOperation_Download_TPH;
                    startLongOperation();
                    break;
                case GetTPHCounterByUserID:
                    if(setUpDataSyncHelper.UpdateTPHCounterByUserID(serviceResponse)){
                        mainMenuActivity.connectionPresenter.GetLangsiranCounterByUserID();
                    }else{
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal UpdateTPHCounterByUserID");
                        progressDialog.dismiss();
                    }
                    break;

                // Langsiran
                case PostLangsiranImage:
                    uploadHelper.uploadData(this,Tag.PostLangsiran,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    break;
                case PostLangsiran:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        uploadHelper.uploadImages(this,Tag.PostLangsiranImage,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    }else{
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai Langsiran");
                        progressDialog.dismiss();
                        responseApi = serviceResponse;
                        LongOperation = LongOperation_Upload_TransaksiPanen;
                        startLongOperation();
                    }
                    break;
                case GetLangsiranListByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Langsiran");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    progressDialog.dismiss();
                    LongOperation = LongOperation_Download_Langsiran;
                    startLongOperation();
                    break;
                case GetLangsiranCounterByUserID:
                    if(setUpDataSyncHelper.UpdateLangsiranCounterByUserID(serviceResponse)){
                        SyncTimeHelper.updateFinishSyncTime(SyncTime.SYNC_DATA_MASTER);
                        mainMenuActivity.connectionPresenter.GetTPanenCounterByUserID();
                    }else{
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal UpdateLangsiranCounterByUserID");
                        progressDialog.dismiss();
                    }
                    break;

                //Transaksi Panen
                case PostTransaksiPanenImage:
                    uploadHelper.uploadData(this,Tag.PostTransaksiPanen,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    break;
                case PostTransaksiPanen:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        try{
                            if (dynamicParameterPenghasilan.getUploadFotoTransaksi().equals("1")) {
                                uploadHelper.uploadImages(this,Tag.PostTransaksiPanenImage,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                            } else {
                                uploadHelper.uploadData(this, Tag.PostTransaksiPanen, objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                            }
                        }catch (Exception e){
                            uploadHelper.uploadData(this, Tag.PostTransaksiPanen, objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                        }
                    }else {
                        GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_TPanenSupervision);
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai");
                        progressDialog.dismiss();
                        //nanti ini di ganti dengan update master pemanen dahulu
                        responseApi = serviceResponse ;
                        LongOperation = LongOperation_UpdateMasterPemanen;
                        startLongOperation();
                    }
                    break;
                case PostTransaksiPanenNFC:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        uploadHelper.uploadData(this,Tag.PostTransaksiPanenNFC,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    }else {
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai");
                        progressDialog.dismiss();
//                    responseApi = serviceResponse ;
                        LongOperation = LongOperation_Upload_TransaksiAngkutNfc;
                        startLongOperation();
                    }
                    break;
                case GetTransaksiPanenListByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Transaksi Panen");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    LongOperation = LongOperation_Download_TransaksiPanen;
                    startLongOperation();
                    break;
                case GetTPanenCounterByUserID:
                    if(setUpDataSyncHelper.UpdateTPanenCounterByUserID(serviceResponse)){
                        mainMenuActivity.connectionPresenter.GetTAngkutCounterByUserID();
                    }else{
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal GetTPanenCounterByUserID");
                        progressDialog.dismiss();
                    }
                    break;

                //TransaksiAngkut
                case PostTransaksiAngkutImage:
                    uploadHelper.uploadData(this,Tag.PostTransaksiAngkut,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    break;
                case PostTransaksiAngkut:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        try {
                            if (dynamicParameterPenghasilan.getUploadFotoTransaksi().equals("1")) {
                                uploadHelper.uploadImages(this,Tag.PostTransaksiAngkutImage,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                            } else {
                                uploadHelper.uploadData(this, Tag.PostTransaksiAngkut, objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                            }
                        }catch (Exception e){
                            uploadHelper.uploadData(this, Tag.PostTransaksiAngkut, objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                        }
                    }else {
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai");
                        progressDialog.dismiss();
//                    responseApi = serviceResponse ;
                        LongOperation = LongOperation_Upload_TransaksiSPB;
                        startLongOperation();
                    }
                    break;
                case PostTransaksiAngkutNFC:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        uploadHelper.uploadData(this,Tag.PostTransaksiAngkutNFC,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    }else {
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai");
                        progressDialog.dismiss();
//                    responseApi = serviceResponse ;
                        LongOperation = LongOperation_Upload_QcBuah;
                        startLongOperation();
                    }
                    break;
                case GetTransaksiAngkutListByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Transaksi Angkut");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    LongOperation = LongOperation_Download_TransaksiAngkut;
                    startLongOperation();
                    break;
                case GetTAngkutCounterByUserID:
                    if(setUpDataSyncHelper.UpdateTAngkutCounterByUserID(serviceResponse)){
                        mainMenuActivity.connectionPresenter.GetSPBCounterByUserID();
                    }else{
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal GetTAngkutCounterByUserID");
                        progressDialog.dismiss();
                    }
                    break;

                //TransaksiSPB
                case PostTransaksiSPBImage:
                    uploadHelper.uploadData(this,Tag.PostTransaksiSPB,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    break;
                case PostTransaksiSPB:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        try {
                            if (dynamicParameterPenghasilan.getUploadFotoTransaksi().equals("1")) {
                                uploadHelper.uploadImages(this,Tag.PostTransaksiSPBImage,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                            } else {
                                uploadHelper.uploadData(this, Tag.PostTransaksiSPB, objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                            }
                        }catch (Exception e){
                            uploadHelper.uploadData(this, Tag.PostTransaksiSPB, objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                        }
                    }else {

                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai");
                        progressDialog.dismiss();
                        //nanti ini di ganti dengan update master operator dahulu
                        responseApi = serviceResponse;
                        LongOperation = LongOperation_Upload_TransaksiSPB;
                        startLongOperation();
                        progressDialog.dismiss();

//                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai");
//                        progressDialog.dismiss();
//                        //nanti ini di ganti dengan update master operator dahulu
//                        responseApi = serviceResponse ;
//                        LongOperation = LongOperation_UpdateMasterOperator;
//                        startLongOperation();
//                        progressDialog.dismiss();
                    }
                    break;
                case GetTransaksiSpbListByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Transaksi SPB");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    LongOperation = LongOperation_Download_TransaksiSPB;
                    startLongOperation();
                    break;
                case GetSPBCounterByUserID:
                    if(setUpDataSyncHelper.UpdateSPBCounterByUserID(serviceResponse)){
                        mainMenuActivity.connectionPresenter.GetQCMutuAncakHeaderCounterByUserID();
                    }else{
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal GetSPBCounterByUserID");
                        progressDialog.dismiss();
                    }
                    break;

                //QcBuah
                case PostQcBuahImages:
                    uploadHelper.uploadData(this,Tag.PostQcBuah,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    break;
                case PostQcBuah:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        uploadHelper.uploadImages(this,Tag.PostQcBuahImages,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    }else {
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai");
                        progressDialog.dismiss();
//                    responseApi = serviceResponse ;
                        LongOperation = LongOperation_Upload_SensusBjr;
                        startLongOperation();
                        progressDialog.dismiss();
                    }
                    break;
                case GetQcBuahByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Qc Buah");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    LongOperation = LongOperation_Download_QcBuah;
                    startLongOperation();
                    break;

                //Sensus BJR
                case PostSensusBjrImages:
                    uploadHelper.uploadData(this,Tag.PostSensusBjr,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    break;
                case PostSensusBjr:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        uploadHelper.uploadImages(this,Tag.PostSensusBjrImages,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    }else {
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai");
                        progressDialog.dismiss();
//                    responseApi = serviceResponse ;
                        LongOperation = LongOperation_Upload_QcAncak;
                        startLongOperation();
                        progressDialog.dismiss();
                    }
                    break;
                case GetSensusBJRByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Sensus BJR");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    LongOperation = LongOperation_Download_SensusBjr;
                    startLongOperation();
                    break;

                // Qc Ancak
                case PostQcAncak:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        uploadHelper.uploadData(this,Tag.PostQcAncak,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    }else {
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai");
                        progressDialog.dismiss();
//                    responseApi = serviceResponse ;
                        LongOperation = LongOperation_Upload_Pemanen;
                        startLongOperation();
                        progressDialog.dismiss();
                    }
                    break;
                case GetQCMutuAncakByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Qc Mutu Ancak");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    LongOperation = LongOperation_Download_QcAncak;
                    startLongOperation();
                    break;
                case GetQCMutuAncakHeaderCounterByUserID:
                    if(setUpDataSyncHelper.UpdateQCMutuAncakCounterByUserID(serviceResponse)){
                        mainMenuActivity.connectionPresenter.GetTPanenSupervisionCounterByUserID();
                    }else{
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal GetQCMutuAncakHeaderCounterByUserID");
                        progressDialog.dismiss();
                    }
                    break;

                // Master User
                case GetMasterUserByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Master User");
                    progressDialog.dismiss();
                    responseApi = serviceResponse;
                    new LongOperation(LongOperation_Download_MasterUser).execute(String.valueOf(LongOperation_Download_MasterUser));
                    break;

                // Opname NFC
                case InsertOpnameNFC:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        try {
                            uploadHelper.uploadData(this, Tag.InsertOpnameNFC, objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                        }catch (Exception e){
                            uploadHelper.uploadData(this, Tag.InsertOpnameNFC, objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                        }
                    }else {

                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai Opname NFC");
                        progressDialog.dismiss();
                        //nanti ini di ganti dengan update master operator dahulu
                        responseApi = serviceResponse;
                        LongOperation = LongOperation_Upload_GerdangDetail;
                        startLongOperation();
                        progressDialog.dismiss();
                    }
                    break;
                case GetOpnameNFCByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Opname NFC");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    new LongOperation(LongOperation_Download_OpnameNFC).execute(String.valueOf(LongOperation_Download_OpnameNFC));
                    break;

                //GetBlockMekanisasi
                case GetBlockMekanisasi:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai GetBlockMekanisasi");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    new LongOperation(LongOperation_Download_GetBlockMekanisasi).execute(String.valueOf(LongOperation_Download_GetBlockMekanisasi));
                    break;
                //GetTPHReasonUnharvestMaster
                case GetTPHReasonUnharvestMaster:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai TPHReasonUnharvestMaster");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    new LongOperation(LongOperation_Download_TPHReasonUnharvestMaster).execute(String.valueOf(LongOperation_Download_TPHReasonUnharvestMaster));
                    break;
                //GetKonfigurasiGerdang
                case GetKonfigurasiGerdang:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai KonfigurasiGerdang");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    new LongOperation(LongOperation_Download_KonfigurasiGerdang).execute(String.valueOf(LongOperation_Download_KonfigurasiGerdang));
                    break;
                //InsertGerdangDetail
                case InsertGerdangDetail:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        try {
                            uploadHelper.uploadData(this, Tag.InsertGerdangDetail, objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                        }catch (Exception e){
                            uploadHelper.uploadData(this, Tag.InsertGerdangDetail, objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                        }
                    }else {
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai Insert Gerdang Detail");
                        progressDialog.dismiss();
                        //nanti ini di ganti dengan update master operator dahulu
                        responseApi = serviceResponse;
                        LongOperation = LongOperation_Upload_LOG_SPB;
                        startLongOperation();
                        progressDialog.dismiss();

                    }
                    break;

                // Pemanen
                case PostPemanen:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai Pemanen");
                    progressDialog.dismiss();
                    new LongOperation(LongOperation_Upload_OpnameNFC).execute(String.valueOf(LongOperation_Upload_OpnameNFC));
                    break;
                case GetPemanenListByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Pemanen");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    new LongOperation(LongOperation_Download_Pemanen).execute(String.valueOf(LongOperation_Download_Pemanen));
                    break;

                //Operator
                case GetOperatorListByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Operator");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    new LongOperation(LongOperation_Download_Operator).execute(String.valueOf(LongOperation_Download_Operator));
                    break;

                 //Vehicle
                case GetVehicleListByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Vehicle");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    new LongOperation(LongOperation_Download_Vehicle).execute(String.valueOf(LongOperation_Download_Vehicle));
                    break;

                //AssistantAfd
                case GetAfdelingAssistantByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Assistant Afd");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    new LongOperation(LongOperation_Download_AfdelingAssistantByEstate).execute(String.valueOf(LongOperation_Download_AfdelingAssistantByEstate));
                    break;

                //ApplicationConfiguration
                case GetApplicationConfiguration:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Assistant Afd");
                    progressDialog.dismiss();
                    responseApi = serviceResponse;
//                    hapus
//                    dataApi = new JSONArray();
//                    dataApi.put(obj);
                    new LongOperation(LongOperation_Download_ApplicationConfiguration).execute(String.valueOf(LongOperation_Download_ApplicationConfiguration));
                    break;

                //SupervisionList
                case GetSupervisionListByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Supervision List By Estate");
                    progressDialog.dismiss();
                    responseApi = serviceResponse;
//                    hapus
//                    dataApi = new JSONArray();
//                    dataApi.put(obj2);
                    new LongOperation(LongOperation_Download_SupervisionListByEstate).execute(String.valueOf(LongOperation_Download_SupervisionListByEstate));
                    break;

                //BJRInformation
                case GetBJRInformation:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai BJRInformation");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    new LongOperation(LongOperation_Download_BJRInformation).execute(String.valueOf(LongOperation_Download_BJRInformation));
                    break;

                //ISCCInformation
                case GetISCCInformation:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai ISCCInformation");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    new LongOperation(LongOperation_Download_ISCCInformation).execute(String.valueOf(LongOperation_Download_ISCCInformation));
                    break;

                //SPK
                case GetMasterSPKByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai Master SPK");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    new LongOperation(LongOperation_Download_SPK).execute(String.valueOf(LongOperation_Download_SPK));
                    break;

                case GetMasterCagesByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout, "Get Selesai Master Cages");
                    progressDialog.dismiss();
                    responseApi = serviceResponse;
                    new LongOperation(LongOperation_Download_Master_cages).execute(String.valueOf(LongOperation_Download_Master_cages));
                    break;

                case GetTPanenSupervisionCounterByUserID:
                    if(setUpDataSyncHelper.UpdateTPanenSupevisionCounterByUserID(serviceResponse)){
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(HarvestApp.getContext(),"SYNC Berhasil",Toast.LENGTH_SHORT).show();
                            }
                        });
                        SyncTimeHelper.updateFinishSyncTime(SyncTime.SYNC_DATA_TRANSAKSI);
                        setUpDataSyncHelper.setUpLastSync(myCoordinatorLayout);
                        progressDialog.dismiss();
                    }else{
                        Toast.makeText(HarvestApp.getContext(),"Gagal GetTPanenSupervisionCounterByUserID",Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                    break;
                case PostSupervisionPanen:
                    Toast.makeText(HarvestApp.getContext(), "Upload Supervision Selesai", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    LongOperation = LongOperation_Upload_TPH;
                    startLongOperation();
                    break;

                // PKS
                case GetListPKSByEstate:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Selesai PKS");
                    progressDialog.dismiss();
                    responseApi = serviceResponse ;
                    new LongOperation(LongOperation_Download_PKS).execute(String.valueOf(LongOperation_Download_PKS));
                    break;

                case UploadLogSPB:
                    JSONArray jsonArray = objSetUp.getJSONArray("data");
                    for(int i = 0 ; i < jsonArray.length();i++){
                        JSONObject obj = jsonArray.getJSONObject(i);
                        File file = new File(obj.getString("fullPath"));
                        GlobalHelper.moveFile(file,
                                new File(GlobalHelper.getPathJSONBackupUpload(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SPB])),
                                file.getName().toString()
                        );
                    }
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai Upload Log SPB");
                    progressDialog.dismiss();
                    new LongOperation(LongOperation_Upload_CrashReport).execute(String.valueOf(LongOperation_Upload_CrashReport));
                    break;

                case CrashReportList:
                    WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai CrashReport");
                    progressDialog.dismiss();
                    CrashUtil.flushCrashReport();
                    new LongOperation(LongOperation_Upload_LogActivity).execute(String.valueOf(LongOperation_Upload_LogActivity));
                    break;
                case LogActivity:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        try {
                            uploadHelper.uploadData(this, Tag.LogActivity, objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                        }catch (Exception e){
                            uploadHelper.uploadData(this, Tag.LogActivity, objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                        }
                    }else {
                        progressDialog.dismiss();
                        ActivityLoggerHelper.clearDB();
                        questionLanjutDownload();
                    }
                    break;

            }
            Log.d("SettingFragment","successResponse " +serviceResponse.getTag());
        } catch (JSONException e) {
            e.printStackTrace();
            objSetUp = new JSONObject();
            WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Upload JSONException successResponse");
//            Toast.makeText(HarvestApp.getContext(), "Gagal Upload JSONException successResponse", Toast.LENGTH_LONG).show();
            progressDialog.dismiss();
        } catch (IOException e) {
            e.printStackTrace();
            objSetUp = new JSONObject();
            WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Upload IOException successResponse");
//            Toast.makeText(HarvestApp.getContext(), "Gagal Upload JSONException successResponse", Toast.LENGTH_LONG).show();
            progressDialog.dismiss();
        }
    }

    @Override
    public void badResponse(ServiceResponse serviceResponse) {
        Log.d("SettingFragment","badResponse " +serviceResponse.getTag());
        Log.d("SettingFragment","badResponse " +serviceResponse.getMessage());
        switch (serviceResponse.getTag()){
            //TPH
            case PostTphImage:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Images TPH");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal Images TPH",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case PostTph:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal TPH");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal TPH",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetTphListByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal TPH");
//                Toast.makeText(HarvestApp.getContext(),"GET Gagal TPH",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetTPHCounterByUserID:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Update Count Gagal TPH");
//                Toast.makeText(HarvestApp.getContext(), "Update Count Gagal TPH", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            //Langsiran
            case PostLangsiranImage:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Images Langsiran");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal Images Langsiran",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case PostLangsiran:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Langsiran");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal Langsiran",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetLangsiranListByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal Langsiran");
//                Toast.makeText(HarvestApp.getContext(),"GET Gagal Langsiran",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetLangsiranCounterByUserID:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Update Count Gagal Langsiran");
//                Toast.makeText(HarvestApp.getContext(), "Update Count Gagal Langsiran", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            //Transaksi Panen
            case PostTransaksiPanenImage:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Images Gagal Transaksi Panen");
//                Toast.makeText(HarvestApp.getContext(),"Upload Images Gagal Transaksi Panen",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case PostTransaksiPanen:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal  Transaksi Panen");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal  Transaksi Panen",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case PostTransaksiPanenNFC:
                try {
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),mainMenuActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        uploadHelper.uploadData(this,Tag.PostTransaksiPanenNFC,objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                    }else {
                        WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Selesai");
                        progressDialog.dismiss();
    //                    responseApi = serviceResponse ;
                        LongOperation = LongOperation_Upload_TransaksiAngkutNfc;
                        startLongOperation();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case GetTransaksiPanenListByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Gagal Transaksi Panen");
//                Toast.makeText(HarvestApp.getContext(),"Get Gagal Transaksi Panen",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetTPanenCounterByUserID:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Update Count Transaksi Panen");
//                Toast.makeText(HarvestApp.getContext(),"Update Count Transaksi Panen",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            //Transaksi Angkut
            case PostTransaksiAngkutImage:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Images Gagal Transaksi Angkut");
//                Toast.makeText(HarvestApp.getContext(),"Upload Images Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case PostTransaksiAngkut:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Transaksi Angkut");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case PostTransaksiAngkutNFC:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Transaksi Angkut NFC");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetTransaksiAngkutListByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Gagal  Transaksi Angkut");
//                Toast.makeText(HarvestApp.getContext(),"Get Gagal  Transaksi Angkut",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetTAngkutCounterByUserID:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Update Count Transaksi Angkut");
//                Toast.makeText(HarvestApp.getContext(),"Update Count Transaksi Angkut",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            //Transaksi SPB
            case PostTransaksiSPBImage:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Images Gagal Transaksi SPB");
//                Toast.makeText(HarvestApp.getContext(),"Upload Images Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case PostTransaksiSPB:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Transaksi SPB");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetTransaksiSpbListByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Gagal  Transaksi Spb");
//                Toast.makeText(HarvestApp.getContext(),"Get Gagal  Transaksi Spb",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetSPBCounterByUserID:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Update Count Transaksi SPB");
//                Toast.makeText(HarvestApp.getContext(),"Update Count Transaksi SPB",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            //QcBuah
            case PostQcBuahImages:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Images Gagal Qc Buah");
//                Toast.makeText(HarvestApp.getContext(),"Upload Images Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case PostQcBuah:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Qc Buah");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetQcBuahByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Gagal Qc Buah");
                progressDialog.dismiss();
                break;

            //Sensus BJR
            case PostSensusBjrImages:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Images Gagal Sensus BJR");
//                Toast.makeText(HarvestApp.getContext(),"Upload Images Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case PostSensusBjr:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Sensus BJR");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetSensusBJRByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Gagal Sensus BJR");
                progressDialog.dismiss();
                break;

            //Qc Ancak
            case PostQcAncak:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Qc Ancak");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetQCMutuAncakByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Get Gagal Qc Ancak");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetQCMutuAncakHeaderCounterByUserID:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Update Count Mutu Ancak");
//                Toast.makeText(HarvestApp.getContext(),"Update Count Transaksi SPB",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;


            case InsertGerdangDetail:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Gerdang Detail");
//                Toast.makeText(HarvestApp.getContext(),"Upload Gagal",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetKonfigurasiGerdang:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Uplaod Gagal KonfigurasiGerdang ");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetTPHReasonUnharvestMaster:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Uplaod Gagal TPHReasonUnharvestMaster ");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetBlockMekanisasi:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Uplaod Gagal GetBlockMekanisasi ");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            case UploadLogSPB:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Uplaod Gagal Log SPB ");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case CrashReportList:
                progressDialog.dismiss();
                CrashUtil.flushCrashReport();
                new LongOperation(LongOperation_Upload_LogActivity).execute(String.valueOf(LongOperation_Upload_LogActivity));
                break;
            case LogActivity:
                try {
                    if(objSetUp.getJSONArray("data").length() -1 != mainMenuActivity.idxUpload) {
                        mainMenuActivity.idxUpload ++;
                        try {
                            uploadHelper.uploadData(this, Tag.LogActivity, objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                        }catch (Exception e){
                            uploadHelper.uploadData(this, Tag.LogActivity, objSetUp.getJSONArray("data"), mainMenuActivity.idxUpload);
                        }
                    }else {
                        progressDialog.dismiss();
                        ActivityLoggerHelper.clearDB();
                        questionLanjutDownload();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                    ActivityLoggerHelper.clearDB();
                    questionLanjutDownload();
                }
                break;


            // Master User
            case GetMasterUserByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal Master User");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Master User", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            // Opname NFC
            case InsertOpnameNFC:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Opname NFC");
//                Toast.makeText(HarvestApp.getContext(), "Upload Gagal Pemanen", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetOpnameNFCByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal Opname NFC");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Pemanen", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            // Pemanen
            case PostPemanen:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal Pemanen");
//                Toast.makeText(HarvestApp.getContext(), "Upload Gagal Pemanen", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetPemanenListByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal Pemanen");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Pemanen", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            // Operator
            case GetOperatorListByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal Operator");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            //Vehicle
            case GetVehicleListByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal Vehicle");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetAfdelingAssistantByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal AfdelingAssistantByEstate");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetApplicationConfiguration:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal ApplicationConfiguration");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetSupervisionListByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal SupervisionList");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetTPanenSupervisionCounterByUserID:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal TPanen Supervision CounterByUserID ");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case PostSupervisionPanen:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Uplaod Gagal Supervision ");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetBJRInformation:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Uplaod Gagal BJRInformation ");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;

            case GetISCCInformation:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Uplaod Gagal ISCCInformation ");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            //SPK
            case GetMasterSPKByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"Upload Gagal ISCCInformation ");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show()'
                progressDialog.dismiss();
                break;

            case GetMasterCagesByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout, "Upload Gagal CagesInformation");
                progressDialog.dismiss();
                break;
            // PKS
            case GetListPKSByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout,"GET Gagal PKS");
//                Toast.makeText(HarvestApp.getContext(), "GET Gagal PKS", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
        }
    }
}
