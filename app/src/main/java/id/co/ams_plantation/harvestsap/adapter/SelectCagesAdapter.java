package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.Cages;

public  class SelectCagesAdapter extends ArrayAdapter<Cages> {

    public ArrayList<Cages> items;
    public ArrayList<Cages> itemsAll;
    public ArrayList<Cages> suggestions;
    private final int viewResourceId;

  public SelectCagesAdapter(Context context, int viewResourceId, ArrayList<Cages> data){
      super(context,viewResourceId,data);
      this.items = new ArrayList<>();
      this.items.addAll(data);
      this.itemsAll = (ArrayList<Cages>)this.items.clone();
      this.suggestions = new ArrayList<>();
      this.viewResourceId = viewResourceId;
  }

  public  View getView(int position, View convertView, ViewGroup parent) {
      View v = convertView;
      if (v == null) {
          LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
          v = vi.inflate(viewResourceId, null);
      }
      if (items.size() > position) {
          Cages cages = items.get(position);
          if (cages != null) {
              ImageView iv = v.findViewById(R.id.iv);
              TextView tv = v.findViewById(R.id.tv);
              TextView tv1 = v.findViewById(R.id.tv1);
              iv.setVisibility(View.GONE);
              tv.setText(cages.getAssetNo());
              tv1.setText(cages.getAssetName());
          }
      }
      return v;
  }
  public Cages getItemAt(int position) {
      return items.get(position);
  }
    @Override
    public int getCount(){
        return items!=null?items.size():0;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                suggestions.clear();

                if (constraint != null && !constraint.toString().isEmpty()) {
                    for (Cages cage : itemsAll) {
                        if (cage.getAssetNo().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                                cage.getAssetName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                            suggestions.add(cage);
                        }
                    }
                } else {
                    suggestions.addAll(itemsAll);
                }

                results.values = suggestions;
                results.count = suggestions.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                items.clear();
                if (results != null && results.count > 0) {
                    items.addAll((ArrayList<Cages>) results.values);
                }
                notifyDataSetChanged();
            }
        };
    }

}