package id.co.ams_plantation.harvestsap.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Date;
import java.util.HashMap;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.connection.ServiceRequest;
import id.co.ams_plantation.harvestsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.CekAktifValue;
import id.co.ams_plantation.harvestsap.model.LastSyncTime;
import id.co.ams_plantation.harvestsap.model.SyncHistroy;
import id.co.ams_plantation.harvestsap.model.SyncTime;
import id.co.ams_plantation.harvestsap.presenter.ConnectionPresenter;
import id.co.ams_plantation.harvestsap.util.ActivityLoggerHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.NetworkHelper;
import id.co.ams_plantation.harvestsap.util.SetUpDataSyncHelper;
import id.co.ams_plantation.harvestsap.util.SyncHistoryHelper;
import id.co.ams_plantation.harvestsap.util.SyncTimeHelper;
import id.co.ams_plantation.harvestsap.util.UploadHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import id.co.ams_plantation.harvestsap.util.ZipManager;
import id.co.ams_plantation.harvestsap.view.ApiView;
//import ir.mahdi.mzip.zip.ZipArchive;
import ng.max.slideview.SlideView;
import pl.droidsonroids.gif.GifImageView;

public class ActiveActivity extends BaseActivity implements ApiView {

    int LongOperation;
    final int LongOperation_GetMasterUserByEstate = 0;
    final int LongOperation_GetPemanenListByEstate = 1;
    final int LongOperation_GetTphListByEstate = 2;
    final int LongOperation_GetLangsiranListByEstate = 3;
    final int LongOperation_GetListPKSByEstate = 4;
    final int LongOperation_GetTransaksiPanenListByEstate = 5;
    final int LongOperation_GetTransaksiAngkutListByEstate = 6;
    final int LongOperation_GetTransaksiSpbListByEstate = 7;
    public final int LongOperation_BackUpHMS = 8;
    final int LongOperation_GetOperatorListByEstate = 9;
    final int LongOperation_GetVehicleListByEstate = 10;
    final int LongOperation_GetAfdelingAssistantByEstate = 11;
    final int LongOperation_GetApplicationConfiguration = 12;
    final int LongOperation_GetSupervisionListByEstate = 13;
    final int LongOperation_GetBJRInformation = 14;
    final int LongOperation_GetISCCInformation = 15;
    final int LongOperation_RestoreHMSTemp = 16;
    final int LongOperation_GetQcBuahByEstate = 17;
    final int LongOperation_GetSensusBJRByEstate = 18;
    final int LongOperation_GetQcAncakByEstate = 19;
    final int LongOperation_GetMasterSPKByEstate= 20;
    final int LongOperation_GetOpnameNFCByEstate = 21;
    final int LongOperation_GetConfigGerdangByEstate = 22;
    final int LongOperation_GetTPHReasonUnharvestMaster = 23;
    final int LongOperation_GetBlockMekanisasi = 24;
    final int LongOperation_CekAktif = 25;
    final int LongOperation_GetMasterCagesByEstate = 26;

    private ConnectionPresenter presenter;
    Button btnSynchronize;
    ImageView ivUserEntry;
    ImageView ivCompanyEntry;
    TextView tvUserEntry;
    TextView tvCompanyEntry;
    TextView keterangan_entry;
    GifImageView giv_loader;
    CoordinatorLayout myCoordinatorLayout;
    SetUpDataSyncHelper setUpDataSyncHelper;
    AlertDialog alertDialogPilihJaringan;
    ServiceResponse responseApi;

    public AlertDialog progressDialog;
    UploadHelper uploadHelper;

    boolean isOnFetchingProcess = false;
    @Override
    protected void initPresenter() {
        presenter = new ConnectionPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active);
        getSupportActionBar().hide();

        setUpDataSyncHelper = new SetUpDataSyncHelper(this);
        uploadHelper = new UploadHelper(this);

        btnSynchronize = (Button) findViewById(R.id.btnSynchronize);
        ivUserEntry = (ImageView) findViewById(R.id.iv_user_entry);
        ivCompanyEntry = (ImageView) findViewById(R.id.iv_company_entry);
        tvUserEntry = (TextView) findViewById(R.id.user_entry);
        tvCompanyEntry = (TextView) findViewById(R.id.company_entry);
        keterangan_entry = (TextView) findViewById(R.id.keterangan_entry);
        giv_loader = (GifImageView) findViewById(R.id.giv_loader);
        myCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.myCoordinatorLayout);

        ivUserEntry.setImageDrawable(
                new IconicsDrawable(this)
                        .icon(MaterialDesignIconic.Icon.gmi_account)
                        .sizeDp(24)
                        .colorRes(R.color.Gray)
        );

        ivCompanyEntry.setImageDrawable(
                new IconicsDrawable(this)
                        .icon(MaterialDesignIconic.Icon.gmi_home)
                        .sizeDp(24)
                        .colorRes(R.color.Gray)
        );

        tvUserEntry.setText(GlobalHelper.getUser().getUserFullName());
        tvCompanyEntry.setText(GlobalHelper.getEstate().getCompanyShortName() + " - "
                + GlobalHelper.getEstate().getEstCode() + " : "
                + GlobalHelper.getEstate().getEstName());

        btnSynchronize.setText(getResources().getString(R.string.active));
        btnSynchronize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityLoggerHelper.recordHitTime(ActiveActivity.class.getSimpleName(),"btnSynchronize",new Date(),new Date());

                String isi = GlobalHelper.readFileContent(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getCountBadResponseName());
                int idx = 0;
                if(!isi.isEmpty()){
                    idx = Integer.parseInt(isi);
                }
                if (idx > GlobalHelper.MAX_BAD_RESPONSE) {
                    showRestoreOrActive();
                } else {
                    showActive();
                }
            }
        });
    }

    public void showRestoreOrActive(){
        String message = "Silahkan pilih Restore Atau Sync Kembali";

        View view = LayoutInflater.from(ActiveActivity.this).inflate(R.layout.public_network_sync_dialog,null);
        TextView tvKet1PilihJaringan = (TextView) view.findViewById(R.id.ket1);
        TextView tvKet2PilihJaringan = (TextView) view.findViewById(R.id.ket2);
        SlideView btnJaringanPublik= (SlideView) view.findViewById(R.id.btnJaringanPublik);
        SlideView btnJaringanLokal = (SlideView) view.findViewById(R.id.btnJaringanKantor);
        tvKet1PilihJaringan.setText(message);
        tvKet2PilihJaringan.setText("Geser Untuk Restore Data Dari HMS Temp Atau Sync Kembali");
        btnJaringanPublik.setText("Sync");
        btnJaringanLokal.setText("Restore");

        isOnFetchingProcess = false;
        btnJaringanLokal.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                isOnFetchingProcess = true;
                startLongOperation(LongOperation_RestoreHMSTemp);
                SyncHistoryHelper.insertHistorySync(SyncHistroy.Sync_Gagal);
                alertDialogPilihJaringan.dismiss();
            }
        });
        btnJaringanPublik.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                isOnFetchingProcess = true;
                alertDialogPilihJaringan.dismiss();
                showActive();
            }
        });

        alertDialogPilihJaringan = WidgetHelper.showFormDialog(alertDialogPilihJaringan,view,ActiveActivity.this);
        alertDialogPilihJaringan.setCancelable(false);
        alertDialogPilihJaringan.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if(!isOnFetchingProcess){
                    if (progressDialog != null) {
                        if(progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    }
                }
            }
        });
    }

    public void showActive(){

        String messagePilihJaringan = "Silahkan pilih jaringan untuk aktivasi!";
        ServiceRequest serviceRequest = new ServiceRequest();

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECT_BUFFER_SERVER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        Boolean connectBufferServer;
        if(NetworkHelper.getIPAddress(true).startsWith("192.168.0")){
            editor.putBoolean(HarvestApp.CONNECT_BUFFER_SERVER,true);
            connectBufferServer = true;
        }else{
            editor.putBoolean(HarvestApp.CONNECT_BUFFER_SERVER,false);
            connectBufferServer =false;
        }
        editor.apply();

        View view = LayoutInflater.from(ActiveActivity.this).inflate(R.layout.public_network_sync_dialog,null);
        TextView tvKet1PilihJaringan = (TextView) view.findViewById(R.id.ket1);
        SlideView btnJaringanPublik= (SlideView) view.findViewById(R.id.btnJaringanPublik);
        SlideView btnJaringanLokal = (SlideView) view.findViewById(R.id.btnJaringanKantor);
        tvKet1PilihJaringan.setText(messagePilihJaringan);

        if(connectBufferServer){
            btnJaringanLokal.setText(getResources().getString(R.string.wifi_hms));
        }else{
            btnJaringanLokal.setText(getResources().getString(R.string.wifi_kebun));
        }

        isOnFetchingProcess = false;
        btnJaringanLokal.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                isOnFetchingProcess = true;

                SyncHistroy syncHistroy = SyncHistoryHelper.getSyncHistory();
                if(syncHistroy != null) {
                    if (!syncHistroy.getSyncFrom().equalsIgnoreCase(SyncHistroy.Sync_From_Active)) {
                        SyncHistoryHelper.insertHistorySync(SyncHistroy.Sync_Gagal);
                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Active, SyncHistroy.Action_Sync);
                    }
                }else{
                    syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Active, SyncHistroy.Action_Sync);
                }

                SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(HarvestApp.CONNECTION_PREF,false);
                editor.apply();

                SharedPreferences pref = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME,Context.MODE_PRIVATE);
                String sync_time = pref.getString(HarvestApp.SYNC_TIME,null);
                HashMap<String, SyncTime> syncTimeHashMap = new HashMap<>();
                if(sync_time != null) {
                    Gson gson = new Gson();
                    LastSyncTime lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
                    if (lastSyncTime.getEstate().getEstCode().equals(GlobalHelper.getEstate().getEstCode())){
                        uploadHelper.ceklistUploadnDownload(false);
                        alertDialogPilihJaringan.dismiss();
                        return;
                    }else{
                        syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,new SyncTime(SyncTime.UPLOAD_TRANSAKSI,System.currentTimeMillis(),false,true));
                        syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,new SyncTime(SyncTime.SYNC_DATA_MASTER,System.currentTimeMillis(),true,false));
                        syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,new SyncTime(SyncTime.SYNC_DATA_TRANSAKSI,System.currentTimeMillis(),true,false));
                    }
                }else{
                    syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,new SyncTime(SyncTime.UPLOAD_TRANSAKSI,System.currentTimeMillis(),false,true));
                    syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,new SyncTime(SyncTime.SYNC_DATA_MASTER,System.currentTimeMillis(),true,false));
                    syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,new SyncTime(SyncTime.SYNC_DATA_TRANSAKSI,System.currentTimeMillis(),true,false));
                }

                LastSyncTime lastSyncTime = new LastSyncTime(GlobalHelper.getEstate(),syncTimeHashMap);
                Gson gson = new Gson();
                SharedPreferences.Editor edit = pref.edit();
                edit.putString(HarvestApp.SYNC_TIME,gson.toJson(lastSyncTime));
                edit.apply();

                startLongOperation(LongOperation_BackUpHMS);
                alertDialogPilihJaringan.dismiss();
            }
        });
        btnJaringanPublik.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                isOnFetchingProcess = true;

                SyncHistroy syncHistroy = SyncHistoryHelper.getSyncHistory();
                if(syncHistroy != null) {
                    if (!syncHistroy.getSyncFrom().equalsIgnoreCase(SyncHistroy.Sync_From_Active)) {
                        SyncHistoryHelper.insertHistorySync(SyncHistroy.Sync_Gagal);
                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Active, SyncHistroy.Action_Sync);
                    }
                }else{
                    syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Active, SyncHistroy.Action_Sync);
                }

                SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(HarvestApp.CONNECTION_PREF,true);
                editor.apply();

                SharedPreferences pref = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME,Context.MODE_PRIVATE);
                String sync_time = pref.getString(HarvestApp.SYNC_TIME,null);
                HashMap<String, SyncTime> syncTimeHashMap = new HashMap<>();
                if(sync_time != null) {
                    uploadHelper.ceklistUploadnDownload(false);
                    alertDialogPilihJaringan.dismiss();
                    return;
                }else{
                    syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,new SyncTime(SyncTime.UPLOAD_TRANSAKSI,System.currentTimeMillis(),false,true));
                    syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,new SyncTime(SyncTime.SYNC_DATA_MASTER,System.currentTimeMillis(),true,false));
                    syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,new SyncTime(SyncTime.SYNC_DATA_TRANSAKSI,System.currentTimeMillis(),true,false));
                }

                LastSyncTime lastSyncTime = new LastSyncTime(GlobalHelper.getEstate(),syncTimeHashMap);
                Gson gson = new Gson();
                SharedPreferences.Editor edit = pref.edit();
                edit.putString(HarvestApp.SYNC_TIME,gson.toJson(lastSyncTime));
                edit.apply();

                startLongOperation(LongOperation_BackUpHMS);
                alertDialogPilihJaringan.dismiss();
            }
        });

        alertDialogPilihJaringan = WidgetHelper.showFormDialog(alertDialogPilihJaringan,view,ActiveActivity.this);
        alertDialogPilihJaringan.setCancelable(false);
        alertDialogPilihJaringan.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if(!isOnFetchingProcess){
                    if (progressDialog != null) {
                        if(progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    }
                }
            }
        });
    }

    public void nextIntent(){
        Log.d("ACTIVE SYNC","nextIntent");
        try {
            JSONObject jModule = GlobalHelper.getModule();
            if(jModule == null){
                Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.cannot_acses),Toast.LENGTH_LONG).show();
                Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
//                if(BuildConfig.BUILD_VARIANT.equals("dev")){
//                    intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps.dev");
//                }
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return;
            }
            if ((jModule.getString("mdlAccCode").equals("HMS3")) && (jModule.getString("subMdlAccCode").equals("P5"))){
                setIntent(ActiveActivity.this,MapActivity.class);
            }else {
                GlobalHelper.setUpAllData();
                setIntent(ActiveActivity.this, MainMenuActivity.class);
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void successResponse(ServiceResponse serviceResponse) {
        switch (serviceResponse.getTag()){
            case GetMasterUserByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetMasterUserByEstate);
                break;
            case GetPemanenListByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetPemanenListByEstate);
                break;
            case GetOperatorListByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetOperatorListByEstate);
                break;
            case GetVehicleListByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetVehicleListByEstate);
                break;
            case GetTphListByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetTphListByEstate);
                Log.d("ACTIVE SYNC","10 a GetTphListByEstate "+ responseApi.getData() );
                break;
            case GetLangsiranListByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetLangsiranListByEstate);
                break;
            case GetTransaksiPanenListByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetTransaksiPanenListByEstate);
                break;
            case GetTransaksiAngkutListByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetTransaksiAngkutListByEstate);
                break;
            case GetTransaksiSpbListByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetTransaksiSpbListByEstate);
                break;
            case GetQcBuahByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetQcBuahByEstate);
                break;
            case GetSensusBJRByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetSensusBJRByEstate);
                break;
            case GetQCMutuAncakByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetQcAncakByEstate);
                break;
            case GetAfdelingAssistantByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetAfdelingAssistantByEstate);
                break;
            case GetApplicationConfiguration:
                responseApi = serviceResponse;
                startLongOperation(LongOperation_GetApplicationConfiguration);
                break;
            case GetBJRInformation:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetBJRInformation);
                break;
            case GetISCCInformation:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetISCCInformation);
                break;
            case GetMasterSPKByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetMasterSPKByEstate);
                break;
            case GetMasterCagesByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetMasterCagesByEstate);
                break;
            case GetOpnameNFCByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetOpnameNFCByEstate);
                break;
            case GetKonfigurasiGerdang:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetConfigGerdangByEstate);
                break;
            case GetTPHReasonUnharvestMaster:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetTPHReasonUnharvestMaster);
                break;
            case GetBlockMekanisasi:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetBlockMekanisasi);
                break;
            case GetSupervisionListByEstate:
                responseApi = serviceResponse;
                startLongOperation(LongOperation_GetSupervisionListByEstate);
                break;
            case GetTPanenCounterByUserID:
                if(setUpDataSyncHelper.UpdateTPanenCounterByUserID(serviceResponse)){
                    Log.d("ACTIVE SYNC","16 GetSPBCounterByUserID");
                    presenter.GetSPBCounterByUserID();
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Gagal UpdateTPanenCounterByUserID",Toast.LENGTH_SHORT).show();
                    //btnSynchronize.setProgress(false);
                    progressDialog.dismiss();
                }
                break;
            case GetSPBCounterByUserID:
                if(setUpDataSyncHelper.UpdateSPBCounterByUserID(serviceResponse)){
                    Log.d("ACTIVE SYNC","17 GetTAngkutCounterByUserID");
                    presenter.GetTAngkutCounterByUserID();
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Gagal UpdateSPBCounterByUserID",Toast.LENGTH_SHORT).show();
                    //btnSynchronize.setProgress(false);
                    progressDialog.dismiss();
                }
                break;
            case GetTAngkutCounterByUserID:
                if(setUpDataSyncHelper.UpdateTAngkutCounterByUserID(serviceResponse)){
                    Log.d("ACTIVE SYNC","18 GetTPHCounterByUserID");
                    presenter.GetTPHCounterByUserID();
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Gagal UpdateTAngkutCounterByUserID",Toast.LENGTH_SHORT).show();
                    //btnSynchronize.setProgress(false);
                    progressDialog.dismiss();
                }
                break;
            case GetTPHCounterByUserID:
                if(setUpDataSyncHelper.UpdateTPHCounterByUserID(serviceResponse)){
                    Log.d("ACTIVE SYNC","19 GetLangsiranCounterByUserID");
                    presenter.GetLangsiranCounterByUserID();
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Gagal UpdateTPHCounterByUserID",Toast.LENGTH_SHORT).show();
                    //btnSynchronize.setProgress(false);
                    progressDialog.dismiss();
                }
                break;
            case GetLangsiranCounterByUserID:
                if(setUpDataSyncHelper.UpdateLangsiranCounterByUserID(serviceResponse)){
                    Log.d("ACTIVE SYNC","20 GetTPanenSupervisionCounterByUserID");
                    presenter.GetTPanenSupervisionCounterByUserID();
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Gagal UpdateLangsiranCounterByUserID",Toast.LENGTH_SHORT).show();
                    //btnSynchronize.setProgress(false);
                    progressDialog.dismiss();
                }
                break;
            case GetTPanenSupervisionCounterByUserID:
                if(setUpDataSyncHelper.UpdateTPanenSupevisionCounterByUserID(serviceResponse)){
                    Log.d("ACTIVE SYNC","21 GetQCMutuAncak");
                    presenter.GetQCMutuAncakHeaderCounterByUserID();
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Gagal UpdateTPanenSupevisionCounterByUserID",Toast.LENGTH_SHORT).show();
                    //btnSynchronize.setProgress(false);
                    progressDialog.dismiss();
                }
                break;
            case GetQCMutuAncakHeaderCounterByUserID:
                if(setUpDataSyncHelper.UpdateQCMutuAncakCounterByUserID(serviceResponse)){
                    SyncTimeHelper.updateFinishSyncTime(SyncTime.SYNC_DATA_TRANSAKSI);
                    Log.d("ACTIVE SYNC","22 GetQCMutuAncak");
                    presenter.GetPksListByEstate();
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Gagal UpdateQCMutuAncakCounterByUserID",Toast.LENGTH_SHORT).show();
                    //btnSynchronize.setProgress(false);
                    progressDialog.dismiss();
                }
                break;
            case GetListPKSByEstate:
                responseApi = serviceResponse ;
                startLongOperation(LongOperation_GetListPKSByEstate);
                break;
        }

        Log.d("ActiveActivity ","succes " +serviceResponse.getTag().toString());
    }

    @Override
    public void badResponse(ServiceResponse serviceResponse) {
        switch (serviceResponse.getTag()){
            case GetMasterUserByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetMasterUserByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetPemanenListByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetPemanenListByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetOperatorListByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetOperatorListByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetVehicleListByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetVehicleListByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetTphListByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetTphListByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                break;
            case GetLangsiranListByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetLangsiranListByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                break;
            case GetTransaksiPanenListByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetTransaksiPanenListByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                break;
            case GetTransaksiAngkutListByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetTransaksiAngkutListByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                break;
            case GetTransaksiSpbListByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetTransaksiSpbListByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                break;
            case GetQcBuahByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetQcBuahByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetSensusBJRByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetSensusBJRByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetQCMutuAncakByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetQCMutuAncakByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetAfdelingAssistantByEstate:
                Toast.makeText(HarvestApp.getContext(),serviceResponse.getHost() + serviceResponse.getPath()+" badResponse GetAfdelingAssistantByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                break;
            case GetApplicationConfiguration:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetApplicationConfiguration",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                break;
            case GetSupervisionListByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetSupervisionListByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                break;
            case GetListPKSByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetListPKSByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                break;
            case GetBJRInformation:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetBJRInformation",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                break;
            case GetISCCInformation:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetISCCInformation",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                break;

            case GetMasterSPKByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetMasterSPKByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;

            case GetMasterCagesByEstate:
                Toast.makeText(HarvestApp.getContext(), "badResponse GetMasterCagesByEstate", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                break;
            case GetOpnameNFCByEstate:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetOpnameNFCByEstate",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetKonfigurasiGerdang:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetKonfigurasiGerdang",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetTPHReasonUnharvestMaster:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetTPHReasonUnharvestMaster",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetBlockMekanisasi:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetBlockMekanisasi",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetTPanenCounterByUserID:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetTPanenCounterByUserID",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                break;
            case GetSPBCounterByUserID:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetSPBCounterByUserID",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                break;
            case GetTAngkutCounterByUserID:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetTAngkutCounterByUserID",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                break;
            case GetTPHCounterByUserID:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetTPHCounterByUserID",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetLangsiranCounterByUserID:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetLangsiranCounterByUserID",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetTPanenSupervisionCounterByUserID:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetTPanenSupervisionCounterByUserID",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
            case GetQCMutuAncakHeaderCounterByUserID:
                Toast.makeText(HarvestApp.getContext(),"badResponse GetQCMutuAncakHeaderCounterByUserID",Toast.LENGTH_SHORT).show();
                //btnSynchronize.setProgress(false);
                progressDialog.dismiss();
                break;
        }
        Log.d("ActiveActivity ","badRespon " +serviceResponse.getTag().toString());
    }

    public void startLongOperation(int paramLongOperation){
        LongOperation = paramLongOperation;
        new LongOperation().execute(String.valueOf(LongOperation));
    }

    public class LongOperation extends AsyncTask<String, String, String> {
        boolean skip = false;
        CekAktifValue cekAktifValue;
        JSONObject objSetUp;
        Snackbar snackbar;
        String text ="";

        public void publishProgress(JSONObject objProgres){
            onProgressUpdate(String.valueOf(objProgres));
        }

        @Override
        protected void onPreExecute() {
            switch (LongOperation){
                case LongOperation_BackUpHMS:
                    text = "Back Up HMS... ";
                    break;
                case LongOperation_GetMasterUserByEstate:
                    text = "Master User By Estate... ";
                    break;
                case LongOperation_GetOperatorListByEstate:
                    text = "Master Operator By Estate... ";
                    break;
                case LongOperation_GetPemanenListByEstate:
                    text = "Master Pemanen By Estate... ";
                    break;
                case LongOperation_GetVehicleListByEstate:
                    text = "Master Kendaraan By Estate... ";
                    break;
                case LongOperation_GetTphListByEstate:
                    text = "Tph By Estate... ";
                    break;
                case LongOperation_GetLangsiranListByEstate:
                    text = "Langsiran By Estate... ";
                    break;
                case LongOperation_GetListPKSByEstate:
                    text = "PKS By Estate... ";
                    break;
                case LongOperation_GetTransaksiPanenListByEstate:
                    text = "Transaksi Panen By Estate... ";
                    break;
                case LongOperation_GetTransaksiAngkutListByEstate:
                    text = "Transaksi Angkut By Estate... ";
                    break;
                case LongOperation_GetTransaksiSpbListByEstate:
                    text = "Transaksi Spb By Estate... ";
                    break;
                case LongOperation_GetQcBuahByEstate:
                    text = "Qc Buah By Estate... ";
                    break;
                case LongOperation_GetSensusBJRByEstate:
                    text = "Sensus BJR By Estate... ";
                    break;
                case LongOperation_GetQcAncakByEstate:
                    text = "Qc Ancak By Estate... ";
                    break;
                case LongOperation_GetAfdelingAssistantByEstate:
                    text = "Assistant Afdeling By Estate... ";
                    break;
                case LongOperation_GetApplicationConfiguration:
                    text = "Aplication Configuration... ";
                    break;
                case LongOperation_GetSupervisionListByEstate:
                    text = "Supervision List By Estate...";
                    break;
                case LongOperation_GetBJRInformation:
                    text = "Get BJR Information...";
                    break;
                case LongOperation_GetISCCInformation:
                    text = "Get ISCC Information";
                    break;
                case LongOperation_GetMasterSPKByEstate:
                    text= "Get Master SPK By Estate...";
                    break;
                case LongOperation_GetMasterCagesByEstate:
                    text= "Get Master Cages By Estate...";
                    break;
                case LongOperation_GetOpnameNFCByEstate:
                    text= "Get Opname NFC By Estate...";
                    break;
                case LongOperation_GetConfigGerdangByEstate:
                    text= "Get Configurasi Gerdang By Estate...";
                    break;
                case LongOperation_GetTPHReasonUnharvestMaster:
                    text= "Get Reason Unharvest By Estate...";
                    break;
                case LongOperation_GetBlockMekanisasi:
                    text= "Get Block Mekanisasi...";
                    break;
                case LongOperation_RestoreHMSTemp:
                    text = "Restore Data HMS TEMP";
                    break;
                case LongOperation_CekAktif:
                    text = "Harap Tunggu...";
                    break;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog != null) {
                         if(progressDialog.isShowing()) {
                             progressDialog.dismiss();
                         }
                    }
                    progressDialog = WidgetHelper.showWaitingDialog(ActiveActivity.this, text);
                }
            });
            Log.d("ActiveActivity ","onPreExecute "+text);
            snackbar = Snackbar.make(myCoordinatorLayout,text,Snackbar.LENGTH_INDEFINITE);
            objSetUp = new JSONObject();
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])){
                case LongOperation_RestoreHMSTemp:
//                    ZipArchive zipArchive = new ZipArchive();
//                    ZipArchive.unzip(GlobalHelper.getDatabasePathHMSTemp() + "HMSTemp.zip",
//                            Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES , "");
                    ZipManager.unzipFolder(GlobalHelper.getDatabasePathHMSTemp() + "HMSTemp.zip",Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES,"");
                    File cekFileLastSync = new File(GlobalHelper.getDatabasePathHMS(), Encrypts.encrypt(GlobalHelper.LAST_SYNC));
                    if (cekFileLastSync.exists()) {
                        setUpDataSyncHelper.clearHmsTemp();
                    }

                    break;
                case LongOperation_BackUpHMS:
                    Log.d("ACTIVE SYNC","0 BackUpHMS");
                    setUpDataSyncHelper.backupDataHMS(this);
                    break;
                case LongOperation_GetMasterUserByEstate:
                    skip = !setUpDataSyncHelper.SetMasterUserByEstate(responseApi,this);
                    break;
                case LongOperation_GetPemanenListByEstate:
                    skip = !setUpDataSyncHelper.SetPemanenListByEstate(responseApi,this);
                    break;
                case LongOperation_GetOperatorListByEstate:
                    skip = !setUpDataSyncHelper.SetOperatorListByEstate(responseApi,this);
                    break;
                case LongOperation_GetVehicleListByEstate:
                    skip = !setUpDataSyncHelper.setVehicleListByEstate(responseApi,this);
                    Log.d("ACTIVE SYNC","9 a GetTphListByEstate "+ skip);
                    break;
                case LongOperation_GetTphListByEstate:
                    skip = !setUpDataSyncHelper.downLoadTph(responseApi,this);
                    Log.d("ACTIVE SYNC","10 b GetTphListByEstate "+ skip );
                    break;
                case LongOperation_GetLangsiranListByEstate:
                    skip = !setUpDataSyncHelper.downLoadLangsiran(responseApi,this);
                    Log.d("ACTIVE SYNC","11 a GetTphListByEstate "+ skip );
                    break;
                case LongOperation_GetListPKSByEstate:
                    skip = !setUpDataSyncHelper.SetPksListByEstate(responseApi,this);
                    break;
                case LongOperation_GetTransaksiPanenListByEstate:
                    skip = !setUpDataSyncHelper.downLoadTransaksiPanen(responseApi,this);
                    Log.d("ACTIVE SYNC","12 a GetTransaksiPanenListByEstate "+ skip );
                    break;
                case LongOperation_GetTransaksiAngkutListByEstate:
                    skip = !setUpDataSyncHelper.downLoadTransaksiAngkut(responseApi,this);
                    Log.d("ACTIVE SYNC","13 a GetTransaksiAngkutListByEstate "+ skip );
                    break;
                case LongOperation_GetTransaksiSpbListByEstate:
                    skip = !setUpDataSyncHelper.downLoadTransaksiSpb(responseApi,this);
                    Log.d("ACTIVE SYNC","14 a GetTransaksiSpbListByEstate "+ skip );
                    break;
                case LongOperation_GetQcBuahByEstate:
                    skip = !setUpDataSyncHelper.downLoadQcBuah(responseApi,this);
                    Log.d("ACTIVE SYNC","15 a LongOperation_GetQcBuahByEstate "+ skip );
                    break;
                case LongOperation_GetSensusBJRByEstate:
                    skip = !setUpDataSyncHelper.downLoadSensusBJR(responseApi,this);
                    Log.d("ACTIVE SYNC","16 a LongOperation_GetSensusBJRByEstate "+ skip );
                    break;
                case LongOperation_GetQcAncakByEstate:
                    skip = !setUpDataSyncHelper.downLoadQcAncak(responseApi,this);
                    Log.d("ACTIVE SYNC","17 a GetQcAncakByEstate "+ skip );
                    break;
                case LongOperation_GetAfdelingAssistantByEstate:
                    skip = !setUpDataSyncHelper.downLoadAfdelingAssistant(responseApi,this);
                    break;
                case LongOperation_GetApplicationConfiguration:
                    skip = !setUpDataSyncHelper.downLoadApplicationConfiguration(responseApi,this);
                    break;
                case LongOperation_GetBJRInformation:
                    skip = !setUpDataSyncHelper.downLoadBJRInformation(responseApi,this);
                    break;
                case LongOperation_GetISCCInformation:
                    skip = !setUpDataSyncHelper.downLoadISCCInformation(responseApi,this);
                    break;
                case LongOperation_GetMasterSPKByEstate:
                    skip = !setUpDataSyncHelper.downLoadMasterSpk(responseApi,this);
                    break;
                case LongOperation_GetMasterCagesByEstate:
                    skip = !setUpDataSyncHelper.downloadMasterCages(responseApi,this);
                    break;
                case LongOperation_GetOpnameNFCByEstate:
                    skip = !setUpDataSyncHelper.SetOpnameNFCByEstate(responseApi,this);
                    break;
                case LongOperation_GetConfigGerdangByEstate:
                    skip = !setUpDataSyncHelper.SetConfigGerdangByEstate(responseApi,this);
                    break;
                case LongOperation_GetTPHReasonUnharvestMaster:
                    skip = !setUpDataSyncHelper.SetTPHReasonUnharvestMaster(responseApi,this);
                    break;
                case LongOperation_GetBlockMekanisasi:
                    skip = !setUpDataSyncHelper.SetBlockMekanisasiMaster(responseApi,this);
                    break;
                case LongOperation_GetSupervisionListByEstate:
                    skip = !setUpDataSyncHelper.downLoadSupervisionListByEstate(responseApi,this);
                    break;
                case LongOperation_CekAktif:
                    cekAktifValue = GlobalHelper.aktifKembali(GlobalHelper.getModule());
                    skip = cekAktifValue.isAktifKembali();
                    break;
            }
            String Flg = skip == true ? "T" : "F";
            Log.d("ActiveActivity ", "doInBackground "+text + "skip " + Flg);
//            Log.d("ActiveActivity ", "data "+responseApi);
            return strings[0];
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject objProgres = new JSONObject(String.valueOf(values[0]));
                        snackbar.setText(String.format(objProgres.getString("ket") + " %d %s [%d/%d] ",
                                objProgres.getInt("persen"), "%", objProgres.getInt("count"), objProgres.getInt("total")));
                        snackbar.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            snackbar.dismiss();
            switch (Integer.parseInt(result)){
                case LongOperation_RestoreHMSTemp:
                    if(GlobalHelper.harusBackup()){
                        progressDialog.dismiss();
                        Toast.makeText(HarvestApp.getContext(),"Gagal Restore HMS Temp",Toast.LENGTH_LONG).show();
                        showActive();
                    }else{
                        nextIntent();
                    }
                    break;
                case LongOperation_BackUpHMS:
                    Log.d("ACTIVE SYNC","1 GetAfdelingAssistantByEstate");
                    presenter.GetAfdelingAssistantByEstate();
                    break;
                case LongOperation_GetAfdelingAssistantByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetAfdelingAssistantByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","2 GetApplicationConfiguration");
                        presenter.GetApplicationConfiguration();
                    }
                    break;
                case LongOperation_GetApplicationConfiguration:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetApplicationConfiguration",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","3 GetBJRInformation");
                        presenter.GetBJRInformation();
                    }
                    break;

                case LongOperation_GetBJRInformation:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetBJRInformation",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","4 GetMasterSPKByEstate");
                        presenter.GetMasterSPKByEstate();
                    }
                    break;
                case LongOperation_GetMasterSPKByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetMasterSPKByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","4-a GetISCCInformation");
                        presenter.GetOpnameNFCByEstate();
                    }
                    break;
                case LongOperation_GetOpnameNFCByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetOpnameNFCByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","4a GetISCCInformation");
                        presenter.GetConfigGerdangByEstate();
                    }
                    break;
                case LongOperation_GetConfigGerdangByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetConfigGerdangByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","4b GetISCCInformation");
                        presenter.GetTPHReasonUnharvestMaster();
                    }
                    break;
                case LongOperation_GetTPHReasonUnharvestMaster:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert LongOperation_Get_TPHReasonUnharvestMaster",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","4b GetISCCInformation");
                        presenter.GetBlockMekanisasi();
                    }
                    break;
                case LongOperation_GetBlockMekanisasi:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert LongOperation_Get_TPHReasonUnharvestMaster",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","4c BlockMekanisasi");
                        presenter.GetISCCInformation();
                    }
                    break;
                case LongOperation_GetISCCInformation:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetISCCInformation",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","5 GetSupervisionListByEstate");
                        presenter.GetSupervisionListByEstate();
                    }
                    break;
                case LongOperation_GetSupervisionListByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetSupervisionListByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","6 GetMasterUserByEstate");
                        presenter.GetMasterUserByEstate();
                    }
                    break;
                case LongOperation_GetMasterUserByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetMasterUserByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else {
                        Log.d("ACTIVE SYNC","7 GetPemanenListByEstate");
                        presenter.GetPemanenListByEstate();
                    }
                    break;
                case LongOperation_GetPemanenListByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetPemanenListByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","8 GetOperatorListByEstate");
                        presenter.GetOperatorListByEstate();
                    }
                    break;
                case LongOperation_GetOperatorListByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetOperatorListByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","9 GetVehicleListByEstate");
                        presenter.GetVehicleListByEstate();
                    }
                    break;
                case LongOperation_GetVehicleListByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetVehicleListByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","10 GetTphListByEstate");
                        presenter.GetMasterCagesByEstate();
                    }
                    break;
                case LongOperation_GetMasterCagesByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetMasterCagesByEstate",Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    } else {
                        Log.d("ACTIVE SYNC","10a GetTPanenCounterByUserID");
                        presenter.GetTphListByEstate();
                    }
                    break;
                case LongOperation_GetTphListByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetTphListByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","11 GetLangsiranListByEstate");
                        presenter.GetLangsiranListByEstate();
                    }
                    break;
                case LongOperation_GetLangsiranListByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetLangsiranListByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","12 GetTransaksiPanenListByEstate");
                        presenter.GetTransaksiPanenListByEstate();
                    }
                    break;
                case LongOperation_GetTransaksiPanenListByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetTransaksiPanenListByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","13 GetTransaksiAngkutListByEstate");
                        presenter.GetTransaksiAngkutListByEstate();
                    }
                    break;
                case LongOperation_GetTransaksiAngkutListByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetTransaksiAngkutListByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","14 GetTransaksiSpbListByEstate");
                        presenter.GetTransaksiSpbListByEstate();
                    }
                    break;
                case LongOperation_GetTransaksiSpbListByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetTransaksiSpbListByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","15 GetTPanenCounterByUserID");
                        presenter.GetTPanenCounterByUserID();
                    }
                    break;
                case LongOperation_GetQcBuahByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetQcBuahByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","16 GetSensusBJRByEstate");
                        presenter.GetSensusBJRByEstate();
                    }
                    break;
                case LongOperation_GetSensusBJRByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetSensusBJRByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","17 GetQcAncakByEstate");
                        presenter.GetQCMutuAncakByEstate();
                    }
                    break;
                case LongOperation_GetQcAncakByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetQcAncakByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        Log.d("ACTIVE SYNC","18 GetTPanenCounterByUserID");
                        presenter.GetTPanenCounterByUserID();
                    }
                    break;

                case LongOperation_GetListPKSByEstate:
                    if(skip){
                        Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetListPKSByEstate",Toast.LENGTH_SHORT).show();
                        //btnSynchronize.setProgress(false);
                        progressDialog.dismiss();
                    }else{
                        if(setUpDataSyncHelper.setUpLastSync(myCoordinatorLayout)) {
                            SyncTimeHelper.updateFinishSyncTime(SyncTime.SYNC_DATA_MASTER);
                            progressDialog.dismiss();
                            new LongOperation().execute(String.valueOf(LongOperation_CekAktif));
                        }else{
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert setUpLastSync",Toast.LENGTH_SHORT).show();
                            //btnSynchronize.setProgress(false);
                            progressDialog.dismiss();
                        }
                    }
                    break;
                case LongOperation_CekAktif:
                    if (!skip){
                        nextIntent();
                    }else{
                        Toast.makeText(HarvestApp.getContext(),cekAktifValue.getMessage(),Toast.LENGTH_LONG).show();
                    }
                    progressDialog.dismiss();
            }
        }
    }
}
