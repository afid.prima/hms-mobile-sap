package id.co.ams_plantation.harvestsap.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.text.SimpleDateFormat;
import java.util.Date;

import id.co.ams_plantation.harvestsap.Fragment.RekonsilasiListFragment;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.presenter.RekonsiliasiListPresenter;
import id.co.ams_plantation.harvestsap.view.RekonsiliasiListView;

public class RekonsiliasiListActivity extends BaseActivity implements RekonsiliasiListView {
    private RekonsiliasiListPresenter presenter;
    private long selectedDate;
    private Date dateSelect;
    private SimpleDateFormat sdf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_layout);
        main();
        toolBarSetup();
    }

    @Override
    protected void initPresenter() {
        presenter= new RekonsiliasiListPresenter(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void toolBarSetup() {
        sdf = new SimpleDateFormat("dd MM yyyy");
//        Log.i("RekonsiliasiActivity", "toolBarSetup: "+sdf.format(dateSelect));
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        TextView textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        textToolbar.setText(getResources().getString(R.string.rekonsiliasi));
    }

    private void main() {
        selectedDate = getIntent().getLongExtra("selectedDate",0);
        dateSelect = new Date(selectedDate);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_container, RekonsilasiListFragment.getInstance(selectedDate))
                .commit();
    }
}
