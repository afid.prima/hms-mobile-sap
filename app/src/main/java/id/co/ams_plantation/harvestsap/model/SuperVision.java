package id.co.ams_plantation.harvestsap.model;

import android.support.annotation.Nullable;

public class SuperVision {
    String idSupervision;
    String estCode;
    GangList gangList;
    SupervisionList s1;
    SupervisionList s2;
    SupervisionList s3;
    SupervisionList s4;
    String createBy;
    Long createDate;

    public SuperVision() {
    }

    public String getIdSupervision() {
        return idSupervision;
    }

    public void setIdSupervision(String idSupervision) {
        this.idSupervision = idSupervision;
    }

    public GangList getGangList() {
        return gangList;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public void setGangList(GangList gangList) {
        this.gangList = gangList;
    }

    @Nullable
    public SupervisionList getS1() {
        return s1;
    }

    public void setS1(SupervisionList s1) {
        this.s1 = s1;
    }

    public SupervisionList getS2() {
        return s2;
    }

    public void setS2(SupervisionList s2) {
        this.s2 = s2;
    }

    public SupervisionList getS3() {
        return s3;
    }

    public void setS3(SupervisionList s3) {
        this.s3 = s3;
    }

    public SupervisionList getS4() {
        return s4;
    }

    public void setS4(SupervisionList s4) {
        this.s4 = s4;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }
}
