package id.co.ams_plantation.harvestsap.model;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;

public class AssistensiRequest {
    public static final int REMOVE = -1;
    public static final int SAVE = 0;
    public static final int PENDING = 1;
    public static final int APPROVED = 2;
    public static final int DECLINED = 3;

    public static final String [] statusName = {
        "SAVE","PENDING","APPROVED","DECLINED"
    };

    public static final int [] statusColor = {
            HarvestApp.getContext().getResources().getColor(R.color.Orange),
            HarvestApp.getContext().getResources().getColor(R.color.Yellow),
            HarvestApp.getContext().getResources().getColor(R.color.Green),
            HarvestApp.getContext().getResources().getColor(R.color.Red)
    };

    String idAssistensi;
    Estate estateRequest;
    Estate estateReceive;
    User assistenReceive;
    User createBy;
    Long startRequest;
    Long endRequest;
    Long createDate;
    int requestPemanen;
    int requestVehicle;
    int status;

    public AssistensiRequest(String idAssistensi, Estate estateRequest, Estate estateReceive, User assistenReceive, User createBy, Long startRequest, Long endRequest, Long createDate, int requestPemanen, int requestVehicle, int status) {
        this.idAssistensi = idAssistensi;
        this.estateRequest = estateRequest;
        this.estateReceive = estateReceive;
        this.assistenReceive = assistenReceive;
        this.createBy = createBy;
        this.startRequest = startRequest;
        this.endRequest = endRequest;
        this.createDate = createDate;
        this.requestPemanen = requestPemanen;
        this.requestVehicle = requestVehicle;
        this.status = status;
    }

    public String getIdAssistensi() {
        return idAssistensi;
    }

    public void setIdAssistensi(String idAssistensi) {
        this.idAssistensi = idAssistensi;
    }

    public Estate getEstateRequest() {
        return estateRequest;
    }

    public void setEstateRequest(Estate estateRequest) {
        this.estateRequest = estateRequest;
    }

    public Estate getEstateReceive() {
        return estateReceive;
    }

    public void setEstateReceive(Estate estateReceive) {
        this.estateReceive = estateReceive;
    }

    public User getAssistenRecive() {
        return assistenReceive;
    }

    public void setAssistenRecive(User assistenReceive) {
        this.assistenReceive = assistenReceive;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public Long getStartRequest() {
        return startRequest;
    }

    public void setStartRequest(Long startRequest) {
        this.startRequest = startRequest;
    }

    public Long getEndRequest() {
        return endRequest;
    }

    public void setEndRequest(Long endRequest) {
        this.endRequest = endRequest;
    }

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }

    public int getRequestPemanen() {
        return requestPemanen;
    }

    public void setRequestPemanen(int requestPemanen) {
        this.requestPemanen = requestPemanen;
    }

    public int getRequestVehicle() {
        return requestVehicle;
    }

    public void setRequestVehicle(int requestVehicle) {
        this.requestVehicle = requestVehicle;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
