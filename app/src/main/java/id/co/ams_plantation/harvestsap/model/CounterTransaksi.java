package id.co.ams_plantation.harvestsap.model;

public class CounterTransaksi {
    String listFolder;
    String tglCounter;
    String userId;
    int counter;

    public CounterTransaksi(String listFolder, String tglCounter, String userId, int counter) {
        this.listFolder = listFolder;
        this.tglCounter = tglCounter;
        this.userId = userId;
        this.counter = counter;
    }

    public String getListFolder() {
        return listFolder;
    }

    public void setListFolder(String listFolder) {
        this.listFolder = listFolder;
    }

    public String getTglCounter() {
        return tglCounter;
    }

    public void setTglCounter(String tglCounter) {
        this.tglCounter = tglCounter;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
