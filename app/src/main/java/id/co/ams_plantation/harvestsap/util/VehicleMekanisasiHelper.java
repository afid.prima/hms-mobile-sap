package id.co.ams_plantation.harvestsap.util;

import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import id.co.ams_plantation.harvestsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.SelectVehicleAdapter;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.DynamicParameterPenghasilan;
import id.co.ams_plantation.harvestsap.model.MekanisasiOperator;
import id.co.ams_plantation.harvestsap.model.Pemanen;
import id.co.ams_plantation.harvestsap.model.SubPemanen;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.model.Vehicle;
import id.co.ams_plantation.harvestsap.ui.TphActivity;

public class VehicleMekanisasiHelper {

    public final String master = "Master Vehicle";
    public final String freeText = "Free Text";

    public final int masterIdx = 0;
    public final int freeTextIdx = 1;

    FragmentActivity activity;
    TphActivity tphActivity;
    TphInputFragment tphFragment;
    SelectVehicleAdapter adapterVehicle;

    boolean isDeletingEtVehicle = false;

    public VehicleMekanisasiHelper(FragmentActivity activity) {
        this.activity = activity;
        if(activity instanceof TphActivity) {
            tphActivity = ((TphActivity) activity);
            if (tphActivity.getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof TphInputFragment) {
                tphFragment = (TphInputFragment) tphActivity.getSupportFragmentManager().findFragmentById(R.id.content_container);
            }
        }
    }

    public boolean cekBawaanInputTph(){
        if(tphActivity == null){
            return false;
        }
        if(tphFragment == null){
            return false;
        }
        return  true;
    }

    public void setDropdownVehicleMekanisasi(DynamicParameterPenghasilan dynamicParameterPenghasilan){
        if(!cekBawaanInputTph()){
            return;
        }

        List<String> listVehicleMekanisasi = new ArrayList<>();

        if(dynamicParameterPenghasilan.getTypeVehicleMekanisasi() == 0){
            listVehicleMekanisasi.add(master);
            listVehicleMekanisasi.add(freeText);
        }else if(dynamicParameterPenghasilan.getTypeVehicleMekanisasi() == 1){
            listVehicleMekanisasi.add(master);
        }else if(dynamicParameterPenghasilan.getTypeVehicleMekanisasi() == 2){
            listVehicleMekanisasi.add(freeText);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(tphActivity,
                android.R.layout.simple_dropdown_item_1line, listVehicleMekanisasi);
        tphFragment.lsKendaraanMekanisasi.setAdapter(adapter);
        tphFragment.lsKendaraanMekanisasi.setText(listVehicleMekanisasi.get(0));

        Collection<Vehicle> vehicles = GlobalHelper.dataVehicle.values();
        if(vehicles.size() > 0){
            adapterVehicle= new SelectVehicleAdapter(tphActivity,R.layout.row_setting, new ArrayList<Vehicle>(vehicles));
            tphFragment.etVehicleMekanisasi.setThreshold(1);
            tphFragment.etVehicleMekanisasi.setAdapter(adapterVehicle);
            adapterVehicle.notifyDataSetChanged();
        }

        tphFragment.lsKendaraanMekanisasi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position == masterIdx){
                    tphFragment.etVehicleMekanisasi.setText("");
                    if(vehicles.size() > 0){
                        adapterVehicle= new SelectVehicleAdapter(tphActivity,R.layout.row_setting, new ArrayList<Vehicle>(vehicles));
                        tphFragment.etVehicleMekanisasi.setThreshold(1);
                        tphFragment.etVehicleMekanisasi.setAdapter(adapterVehicle);
                        adapterVehicle.notifyDataSetChanged();
                    }
                }else if (position == freeTextIdx){
                    tphFragment.etVehicleMekanisasi.setText("");
                    tphFragment.etVehicleMekanisasi.setAdapter(null);
                    tphFragment.etVehicleMekanisasi.setThreshold(0);
                }
            }
        });

//        tphFragment.etVehicleMekanisasi.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if(b)
//                    tphFragment.etVehicleMekanisasi.showDropDown();
//            }
//        });

        tphFragment.etVehicleMekanisasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tphFragment.etVehicleMekanisasi.setText("");
                tphFragment.etVehicleMekanisasi.showDropDown();
            }
        });

        tphFragment.etVehicleMekanisasi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setVehicle(((SelectVehicleAdapter) parent.getAdapter()).getItemAt(position));
            }
        });

        tphFragment.etVehicleMekanisasi.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                isDeletingEtVehicle = count > after;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(tphFragment.lsKendaraanMekanisasi.getText().toString().equalsIgnoreCase(freeText)){
                    tphFragment.etVehicleMekanisasi.setHint("Kendaraan");

                    Vehicle vehicle = new Vehicle();
                    vehicle.setVehicleCode(tphFragment.etVehicleMekanisasi.getText().toString().toUpperCase());
                    setVehicle(vehicle);
                }else{
                    tphFragment.etVehicleMekanisasi.setHint("Kendaraan");
                }
            }

        });
    }

    public void setVehicle(Vehicle vehicle){
        if(vehicle !=null) {
            Gson gson = new Gson();
            Log.d("VehicleMekanisasiHelper", "setVehicle: "+ gson.toJson(vehicle));
            if(tphFragment.lsKendaraanMekanisasi.getText().toString().equalsIgnoreCase(master)) {
                tphFragment.typeVehicleMekanisasi = masterIdx;
                if (vehicle.getVehicleName() != null) {
                    tphFragment.etVehicleMekanisasi.setText(vehicle.getVehicleName());
                }
            }else{
                tphFragment.typeVehicleMekanisasi = freeTextIdx;
            }
            tphFragment.vehicleMekanisasi = vehicle;
        }else{
            tphFragment.etVehicleMekanisasi.setText("");
            tphFragment.vehicleMekanisasi = new Vehicle();
        }
    }

    public void setVehicleOperator(MekanisasiOperator mekanisasiOperator){
        if(mekanisasiOperator == null) {
            return;
        }
        if(mekanisasiOperator.getVehicle() == null){
            return;
        }

        tphFragment.typeVehicleMekanisasi = mekanisasiOperator.getTypeVehicleMekanisasi();
        tphFragment.vehicleMekanisasi = mekanisasiOperator.getVehicle();
        if(mekanisasiOperator.getTypeVehicleMekanisasi() == masterIdx){
            tphFragment.lsKendaraanMekanisasi.setText(master);
            tphFragment.etVehicleMekanisasi.setText(mekanisasiOperator.getVehicle().getVehicleName());
        }else if(mekanisasiOperator.getTypeVehicleMekanisasi() == freeTextIdx){
            tphFragment.lsKendaraanMekanisasi.setText(freeText);
            tphFragment.etVehicleMekanisasi.setText(mekanisasiOperator.getVehicle().getVehicleCode());
        }
    }

    public HashMap<String, MekanisasiOperator> getCurrentMekanisasiOperator(Long currentDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String tgl = sdf.format(currentDate);
        HashMap<String,MekanisasiOperator> currentMekanisasiOperator = new HashMap<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_OPERATOR_MEKANISASI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("param1", tgl));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                MekanisasiOperator mekanisasiOperator  = gson.fromJson(dataNitrit.getValueDataNitrit(),MekanisasiOperator.class);
                currentMekanisasiOperator.put(mekanisasiOperator.getPemanen().getNik(),mekanisasiOperator);
                Log.d("VehicleMekanisasiHelper", "getCurrentMekanisasiOperator: "+gson.toJson(mekanisasiOperator));
            }
        }
        db.close();
        return  currentMekanisasiOperator;
    }
    public void saveMekanisasiOperator(TransaksiPanen transaksiPanen){
        if(transaksiPanen.getVehicleMekanisasi() == null){
            return;
        }

        if(transaksiPanen.getVehicleMekanisasi().getVehicleCode() == null){
            return;
        }

        Vehicle vehicle = transaksiPanen.getVehicleMekanisasi();

        if(transaksiPanen.getKongsi() == null){
            return;
        }

        Pemanen pemanen = null;
        if(transaksiPanen.getKongsi().getSubPemanens().size() > 1){
            for (int i = 0 ; i < transaksiPanen.getKongsi().getSubPemanens().size(); i++){
                SubPemanen subPemanen = transaksiPanen.getKongsi().getSubPemanens().get(i);
                if(subPemanen.getTipePemanen() == SubPemanen.idOperator || subPemanen.getTipePemanen() == SubPemanen.idOperatorTonase){
                    pemanen = subPemanen.getPemanen();
                    break;
                }
            }
        }

        if(pemanen == null){
            return;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdfId = new SimpleDateFormat("yyMMdd");
        String tglPanen = sdf.format(transaksiPanen.getCreateDate());
        String tglPanenId = sdfId.format(transaksiPanen.getCreateDate());
        String estCode = GlobalHelper.getEstate().getEstCode();
        if(pemanen.getEstate() != null){
            estCode = pemanen.getEstate();
        }

        MekanisasiOperator mekanisasiOperator = new MekanisasiOperator(
                estCode + "-"+tglPanenId+ "-"+pemanen.getNik(),
                estCode,
                tglPanen,
                vehicle,
                pemanen,
                transaksiPanen.getTypeVehicleMekanisasi(),
                MekanisasiOperator.MekanisasiOperator_NotUplaod,
                transaksiPanen.getCreateDate(),
                transaksiPanen.getCreateBy()
        );

        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(mekanisasiOperator.getIdMekanisasiOperator(),gson.toJson(mekanisasiOperator),tglPanen);

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_OPERATOR_MEKANISASI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", mekanisasiOperator.getIdMekanisasiOperator()));
        if(cursor.size() > 0) {
            repository.update(dataNitrit);
        }else{
            repository.insert(dataNitrit);
        }
        db.close();
    }
}
