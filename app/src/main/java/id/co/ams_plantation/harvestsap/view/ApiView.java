package id.co.ams_plantation.harvestsap.view;

import id.co.ams_plantation.harvestsap.connection.ServiceResponse;

/**
 * Created by user on 11/21/2018.
 */

public interface ApiView extends BaseView {
    void successResponse(ServiceResponse serviceResponse);
    void badResponse(ServiceResponse serviceResponse);
}
