package id.co.ams_plantation.harvestsap.Fragment;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.mikepenz.iconics.utils.Utils;
import com.roughike.swipeselector.OnSwipeItemSelectedListener;
import com.roughike.swipeselector.SwipeItem;
import com.roughike.swipeselector.SwipeSelector;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.adapter.TRekonsilasiAdapter;
import id.co.ams_plantation.harvestsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.holder.TRekonsilasiRowHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestsap.TableView.view_model.TRekonsiliasiTableViewModel;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.RekonsilasiActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.RekonsiliasiHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;

public class RekonsilasiListFragment extends Fragment {
    public static final int STATUS_LONG_OPERATION_NONE = 0;
    public static final int STATUS_LONG_OPERATION_SEARCH = 1;
    public static final int STATUS_LONG_OPERATION_SELECT_ITEM = 2;
    public static final int STATUS_LONG_OPERATION_NEW_DATA = 3;
    public static final int STATUS_LONG_OPERATION_REKONSILIASI = 4;

    private long selectedDate;
    private TableView rekonsiliasiTable;
    private CardView footerKet;
    private TextView tvStartTime;
    private TextView tvFinishTime;
    private TextView tvTrip;
    private TextView tvUpload;
    private TextView tvTotalKartuHilang;
    private TextView tvTotalKartuRusak;
    private SwipeSelector menuSwipeSelector;
    private LinearLayout llSearch;
    private LinearLayout llContent;
    private CompleteTextViewHelper etSearch;
    private Button btnSearch;

    private TRekonsiliasiTableViewModel mTableViewModel;
    private TRekonsilasiAdapter adapter;
    private Date dateSelect;
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    private TransaksiAngkut selectedTransaksiAngkut;
    private HashMap<String,TransaksiAngkut> origin;
    Set<String> originalSearch;
    Set<String> listSearch;
    private ArrayList<TransaksiAngkut> transaksiAngkuts;
    private ArrayList<TransaksiAngkut> searchTAngkut;
//    private ArrayList<TransaksiAngkut> rekonsilasiAngkuts;
    Long mulai = Long.valueOf(0);
    Long sampai = Long.valueOf(0);
    private int tTotalTrip = 0;
    private int tTotalKartuRusak = 0;
    private int tTotalKartuHilang = 0;
    private int tTotalUpload = 0;


    public static RekonsilasiListFragment getInstance(){
        RekonsilasiListFragment fragment = new RekonsilasiListFragment();
        return fragment;
    }


    public static RekonsilasiListFragment getInstance(long selectedDate){
        RekonsilasiListFragment fragment = new RekonsilasiListFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("selectedDate", selectedDate);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            this.selectedDate = getArguments().getLong("selectedDate");
            dateSelect = new Date(selectedDate);
        }
        origin = new HashMap<>();
        originalSearch = new android.support.v4.util.ArraySet<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.rekonsiliasilist_layout, null,false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getActivity() != null) {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rekonsiliasiTable = view.findViewById(R.id.tableView);
        tvStartTime = view.findViewById(R.id.tvStartTime);
        tvFinishTime = view.findViewById(R.id.tvFinishTime);
        tvTrip = view.findViewById(R.id.tvTrip);
        tvUpload = view.findViewById(R.id.tvUpload);
        tvTotalKartuHilang = view.findViewById(R.id.tvTotalKartuHilang);
        tvTotalKartuRusak = view.findViewById(R.id.tvTotalKartuRusak);
        footerKet = view.findViewById(R.id.llket);
        menuSwipeSelector = view.findViewById(R.id.menuSwipeSelector);
        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        llSearch = view.findViewById(R.id.llSearch);
        llContent = view.findViewById(R.id.llContent);


        ArrayList<SwipeItem> swipeItems= new ArrayList<>();
        Long now = System.currentTimeMillis();
        for(int i = 0 ; i < GlobalHelper.MAX_LAST_DAY_DATA; i++){
            Long l = now - ( i * 24 * 60 * 60 * 1000);
            swipeItems.add(new SwipeItem(String.valueOf(i),sdf.format(l),""));
        }
        Collections.reverse(swipeItems);

        SwipeItem[] tmpStrSwipe = new SwipeItem[swipeItems.size()];
        tmpStrSwipe = swipeItems.toArray(tmpStrSwipe);
        menuSwipeSelector.setItems(tmpStrSwipe);
        menuSwipeSelector.setOnItemSelectedListener(new OnSwipeItemSelectedListener() {
            @Override
            public void onItemSelected(SwipeItem item) {
                try {
                    if(originalSearch != null) originalSearch.clear();
                    Date date = sdf.parse(item.getTitle());
                    dateSelect = date;
                    startLongOperation(STATUS_LONG_OPERATION_NONE);
                    Log.d("item Date ", date.getTime() + " "+ sdf.format(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        menuSwipeSelector.selectItemAt(swipeItems.size() - 1);


        rekonsiliasiTable.setTableViewListener(new ITableViewListener() {
            @Override
            public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

            }

            @Override
            public void onCellDoubleClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

            }

            @Override
            public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

            }

            @Override
            public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {
                    // Create Long Press Popup
                    ColumnHeaderPopup popup = new ColumnHeaderPopup(
                            (ColumnHeaderViewHolder) columnHeaderView, rekonsiliasiTable);
                    // Show
                    popup.show();
                }
            }

            @Override
            public void onColumnHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

            }

            @Override
            public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

            }

            @Override
            public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                if (rowHeaderView != null && rowHeaderView instanceof TRekonsilasiRowHeaderViewHolder) {
                    String [] sid = ((TRekonsilasiRowHeaderViewHolder) rowHeaderView).getCellId().split("-");
                    selectedTransaksiAngkut = origin.get(sid[1]);
                    Log.i("SelectTransaksiAngkut", "SelectTransaksiAngkut: "+selectedTransaksiAngkut.getAngkuts().size());
//                    ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),getResources().getString(R.string.wait));
                    startLongOperation(STATUS_LONG_OPERATION_SELECT_ITEM);
                }

            }

            @Override
            public void onRowHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

            }

            @Override
            public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLongOperation(STATUS_LONG_OPERATION_SEARCH);
            }
        });
    }



    private void addRowHeader(TransaksiAngkut transaksiAngkut){
        origin.put(transaksiAngkut.getIdTAngkut(), transaksiAngkut);
    }

    private void addRowTable(TransaksiAngkut transaksi, int status){
        if (mulai == 0) {
            mulai = transaksi.getStartDate();
        } else if (mulai > transaksi.getStartDate()) {
            mulai = transaksi.getStartDate();
        }

        if (sampai < transaksi.getEndDate()) {
            sampai = transaksi.getEndDate();
        }

        if(transaksi.getStatus() == TransaksiAngkut.UPLOAD){
            tTotalUpload++;
        }

//        for(Angkut angkut : transaksi.getAngkuts()){
//            if(angkut.getManualSPBData().getTipeKongsi().equals("Kartu Rusak")){
//                tTotalKartuRusak++;
//            }else if(angkut.getManualSPBData().getTipeKongsi().equals("Kartu Hilang")){
//                tTotalKartuHilang++;
//            }
//        }

//        tJanjang += transaksi.getTotalJanjang();
//        tBrondolan += transaksi.getTotalBrondolan();
//
//        if (transaksi.getStatus() == TransaksiAngkut.UPLOAD) {
//            upload++;
//        }
//
//        trip++;
//        origin.put(transaksi.getIdTAngkut(), transaksi);
//
        if(status == STATUS_LONG_OPERATION_NONE){
            originalSearch.add(transaksi.getIdTAngkut());
            originalSearch.add(transaksi.getVehicle().getVehicleCode());
            originalSearch.add(transaksi.getTujuan() == TransaksiAngkut.TUJUAN_PKS ? transaksi.getPks().getNamaPKS() :
                    transaksi.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR ? transaksi.getLangsiran().getNamaTujuan():
                            transaksi.getTujuan() == TransaksiAngkut.TUJUAN_KELUAR ? TransaksiAngkut.KELAUR : ""
            );
            originalSearch.add(timeFormat.format(transaksi.getStartDate()));
            originalSearch.add(timeFormat.format(transaksi.getEndDate()));
            tTotalTrip = transaksiAngkuts.size();
        }else{
            if(searchTAngkut!=null){
                if(!searchTAngkut.contains(transaksi)){
                    searchTAngkut.add(transaksi);
                }

            }

            if(searchTAngkut.size()>0){
                tTotalTrip = searchTAngkut.size();
            }else{
                tTotalTrip = transaksiAngkuts.size();
            }
        }


    }

    public void startLongOperation(int status){
        new LongOperation().execute(String.valueOf(status));
    }

    private void updateUIRV(int status){
        if(transaksiAngkuts.size()==0){
            llContent.setVisibility(View.GONE);
        }else{
            llContent.setVisibility(View.VISIBLE);
        }

        List<String> lSearch = new ArrayList<>();
        lSearch.addAll(originalSearch);
        lSearch = GlobalHelper.removeNullUsingIterator(lSearch);
        ArrayAdapter<String> adapterSearch = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, lSearch);
        etSearch.setAdapter(adapterSearch);
        etSearch.setThreshold(1);
        adapterSearch.notifyDataSetChanged();

        if(status == STATUS_LONG_OPERATION_NONE){
//            etSearch.setText("");
        }else if(status == STATUS_LONG_OPERATION_SEARCH){
            if(searchTAngkut.size()==0){
                Toast.makeText(getActivity(), "Data rekonsiliasi tidak ditemukan", Toast.LENGTH_SHORT).show();
            }
        }

//        if (arrayLog.length() > 0) {
//            GlobalHelper.writeFileContent(GlobalHelper.getLogTransaksi(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT], sdf.format(dateSelected)), arrayLog.toString());
//        }

        rekonsiliasiTable.setAdapter(adapter);
        rekonsiliasiTable.setRowHeaderWidth(Utils.convertDpToPx(getActivity(),100));

        adapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel
                .getRowHeaderList(), mTableViewModel.getCellList());
        tvTrip.setText(String.valueOf(tTotalTrip));
        tvTotalKartuHilang.setText(String.valueOf(tTotalKartuHilang));
        tvTotalKartuRusak.setText(String.valueOf(tTotalKartuRusak));
        tvUpload.setText(String.valueOf(tTotalUpload)); // test purpose
        tvStartTime.setText(mulai == 0 ? "" : timeFormat.format(mulai));
        tvFinishTime.setText(sampai == 0 ? "" : timeFormat.format(sampai));

    }

    private void updateDataRV(int status){
        Log.i("updateDataRV", "updateDataRV: heres");
        listSearch = new android.support.v4.util.ArraySet<>();
        mulai = Long.valueOf(0);
        sampai = Long.valueOf(0);

        tTotalTrip = 0;
        tTotalKartuRusak = 0;
        tTotalKartuHilang = 0;
        tTotalUpload = 0;

        RekonsiliasiHelper rekonsiliasiHelper = new RekonsiliasiHelper();
        transaksiAngkuts = rekonsiliasiHelper.getAllRekonsiliasi(dateSelect,origin);
        searchTAngkut = new ArrayList<>();
        Collections.sort(transaksiAngkuts, new Comparator<TransaksiAngkut>() {
            @Override
            public int compare(TransaksiAngkut o1, TransaksiAngkut o2) {
                return o1.getEndDate() > o2.getEndDate() ? -1 : (o1.getEndDate() < o2.getEndDate()) ? 1 : 0;
            }
        });

        for(TransaksiAngkut transaksiAngkut : transaksiAngkuts){
            for(Angkut angkut : transaksiAngkut.getAngkuts()){
                if(angkut.getManualSPBData()!=null){
                    if(status == STATUS_LONG_OPERATION_SEARCH){
                        if ((transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR) && (transaksiAngkut.getLangsiran().getNamaTujuan().toLowerCase().contains(etSearch.getText().toString().toLowerCase())) ){
                            addRowTable(transaksiAngkut, status);

                        }else if ((transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_PKS) && (transaksiAngkut.getPks().getNamaPKS().toLowerCase().contains(etSearch.getText().toString().toLowerCase()))){
                            addRowTable(transaksiAngkut, status);
                        }else if ((transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_KELUAR) && TransaksiAngkut.KELAUR.contains(etSearch.getText().toString().toUpperCase())){
                            addRowTable(transaksiAngkut, status);
                        }else if (transaksiAngkut.getVehicle().getVehicleCode().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                                transaksiAngkut.getIdTAngkut().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                                timeFormat.format(transaksiAngkut.getStartDate()).contains(etSearch.getText().toString().toLowerCase()) ||
                                String.valueOf(transaksiAngkut.getAngkuts().size()).contains(etSearch.getText().toString().toLowerCase())) {
                            addRowTable(transaksiAngkut, status);
                        }

                    }else {
                        addRowTable(transaksiAngkut, status);
                    }

                    if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                        tTotalKartuRusak++;
                    }else if(angkut.getManualSPBData().getTipe().equals("Kartu Hilang")){
                        tTotalKartuHilang++;
                    }


                }
            }
        }


        if(status == STATUS_LONG_OPERATION_SEARCH){
            if(searchTAngkut.size()==0){
                mTableViewModel = new TRekonsiliasiTableViewModel(getContext(), transaksiAngkuts);
            }else{


                mTableViewModel = new TRekonsiliasiTableViewModel(getContext(), searchTAngkut);
            }

        }else{
            mTableViewModel = new TRekonsiliasiTableViewModel(getContext(), transaksiAngkuts);

        }

        adapter = new TRekonsilasiAdapter(getContext(), mTableViewModel);
        Log.i("RekonsiliasiData", "transaksiAngkuts: "+transaksiAngkuts.size()+" searchTAngkut: "+searchTAngkut.size());
    }

    private class LongOperation extends AsyncTask<String, Void, String>{
        String dataString;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (((BaseActivity) getActivity()).alertDialogBase != null) {
                ((BaseActivity) getActivity()).alertDialogBase.cancel();
            }

            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])){
                case STATUS_LONG_OPERATION_NONE: {
                    if(dateSelect!=null){
                        updateDataRV(Integer.parseInt(strings[0]));
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateDataRV(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM: {
                    Gson gson = new Gson();
                    dataString = gson.toJson(selectedTransaksiAngkut);
                    break;
                }
            }
            return strings[0];
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (((BaseActivity) getActivity()).alertDialogBase != null) {
                ((BaseActivity) getActivity()).alertDialogBase.cancel();
            }
            switch (Integer.parseInt(s)){
                case STATUS_LONG_OPERATION_NONE:{
                    updateUIRV(Integer.parseInt(s));
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateUIRV(Integer.parseInt(s));
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM: {
                    Intent intent = new Intent(getActivity(), RekonsilasiActivity.class);
                    intent.putExtra("transaksiAngkut",dataString);
                    intent.putExtra("rekonsiliasiOnly", true);
                    startActivity(intent);
                    break;
                }
            }
        }
    }
}
