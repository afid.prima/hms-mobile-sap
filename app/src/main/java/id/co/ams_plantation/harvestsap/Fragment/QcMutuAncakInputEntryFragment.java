package id.co.ams_plantation.harvestsap.Fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.text.SimpleDateFormat;
import java.util.Set;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.QcAncak;
import id.co.ams_plantation.harvestsap.model.QcAncakPohon;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.QcMutuAncakActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;

public class QcMutuAncakInputEntryFragment extends Fragment {

    static final int LongOperation_Save = 0;
    static final int LongOperation_Deleted = 1;

    CompleteTextViewHelper etBaris;
    AppCompatEditText etPohon;
    AppCompatEditText etJanjang;
    AppCompatEditText etBT;
    AppCompatEditText etBRD;
    AppCompatEditText etOP;
    AppCompatEditText etPS;
    AppCompatEditText etSP;
    AppCompatEditText etBM;
    TextView tvTotalPokok;
    TextView tvTotalJanjang;
    TextView tvTotalBuahTinggal;
    TextView tvTotalBrondolan;
    LinearLayout lsave;
    LinearLayout lldeleted;
    LinearLayout lcancel;
    Set<String> allBaris;

    QcMutuAncakActivity qcMutuAncakActivity;
    QcAncakPohon selectedAncakPohon;

    public static QcMutuAncakInputEntryFragment getInstance(){
        QcMutuAncakInputEntryFragment fragment = new QcMutuAncakInputEntryFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qc_ancak_input_entry_layout,null,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etBaris = view.findViewById(R.id.etBaris);
        etPohon = view.findViewById(R.id.etPohon);
        etJanjang = view.findViewById(R.id.etJanjang);
        etBT = view.findViewById(R.id.etBT);
        etBRD = view.findViewById(R.id.etBRD);
        etOP = view.findViewById(R.id.etOP);
        etPS = view.findViewById(R.id.etPS);
        etSP = view.findViewById(R.id.etSP);
        etBM = view.findViewById(R.id.etBM);
        lsave = view.findViewById(R.id.lsave);
        tvTotalPokok = view.findViewById(R.id.tvTotalPokok);
        tvTotalJanjang = view.findViewById(R.id.tvTotalJanjang);
        tvTotalBuahTinggal = view.findViewById(R.id.tvTotalBuahTinggal);
        tvTotalBrondolan = view.findViewById(R.id.tvTotalBrondolan);
        lldeleted = view.findViewById(R.id.lldeleted);
        lcancel = view.findViewById(R.id.lcancel);

        if(getActivity() instanceof  QcMutuAncakActivity){
            qcMutuAncakActivity = (QcMutuAncakActivity) getActivity();
        }

        allBaris = new android.support.v4.util.ArraySet<>();

        for(Integer integer : qcMutuAncakActivity.seletedQcAncak.getTph().getBaris()){
            allBaris.add(String.valueOf(integer));
        }

        ArrayAdapter<String> adapterBaris = new ArrayAdapter<String>
                (getActivity(),android.R.layout.select_dialog_item, allBaris.toArray(new String[allBaris.size()]));

        etBaris.setThreshold(1);
        etBaris.setAdapter(adapterBaris);
        etBaris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etBaris.showDropDown();
            }
        });
        etBaris.setText(String.valueOf(qcMutuAncakActivity.seletedQcAncak.getTph().getBaris().get(0)));

        if(qcMutuAncakActivity.seletedQcAncakPohon != null){
            selectedAncakPohon = qcMutuAncakActivity.seletedQcAncakPohon;
            qcMutuAncakActivity.seletedQcAncakPohon = null;
            setValue();
            lldeleted.setVisibility(View.VISIBLE);
        }else{
            lldeleted.setVisibility(View.GONE);
        }

        if(qcMutuAncakActivity.seletedQcAncak.getStatus() == QcAncak.Upload){
            lldeleted.setVisibility(View.GONE);
            lsave.setVisibility(View.GONE);
        }

        setSummaryQcAncak();

        lsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LongOperation().execute(String.valueOf(LongOperation_Save));
            }
        });

        lcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qcMutuAncakActivity.backProses();
            }
        });

        lldeleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LongOperation().execute(String.valueOf(LongOperation_Deleted));
            }
        });
    }

    private void setValue(){
        etBaris.setText(String.valueOf(selectedAncakPohon.getNoBaris()));
        etPohon.setText(String.valueOf(selectedAncakPohon.getIdPohon()));
        etJanjang.setText(String.valueOf(selectedAncakPohon.getJanjangPanen()));
        etBT.setText(String.valueOf(selectedAncakPohon.getBuahTinggal()));
        etBRD.setText(String.valueOf(selectedAncakPohon.getBrondolan()));
        etOP.setText(String.valueOf(selectedAncakPohon.getOverPrun()));
        etPS.setText(String.valueOf(selectedAncakPohon.getPelepahSengkelan()));
        etSP.setText(String.valueOf(selectedAncakPohon.getSusunanPelepah()));
        etBM.setText(String.valueOf(selectedAncakPohon.getBuahMatahari()));
    }

    private void setSummaryQcAncak(){
        int TotalJanjang =0;
        int TotalBuahTinggal =0;
        int TotalBrondolan =0;
        for(int i =0;i<qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().size();i++){
            QcAncakPohon qcAncakPohon = qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().get(i);
            TotalJanjang += qcAncakPohon.getJanjangPanen();
            TotalBuahTinggal += qcAncakPohon.getBuahTinggal();
            TotalBrondolan += qcAncakPohon.getBrondolan();
        }
        tvTotalPokok.setText(String.valueOf(qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().size()));
        tvTotalJanjang.setText(String.valueOf(TotalJanjang));
        tvTotalBuahTinggal.setText(String.valueOf(TotalBuahTinggal));
        tvTotalBrondolan.setText(String.valueOf(TotalBrondolan));
    }

    private void clearFormInput(){
        etPohon.requestFocus();
        etPohon.setText("");
        etJanjang.setText("");
        etBT.setText("");
        etBRD.setText("");
        etOP.setText("");
        etPS.setText("");
        etSP.setText("");
        etBM.setText("");
        GlobalHelper.showKeyboard(getActivity());
    }

    private boolean validasiQcAncakBaris(){
        if(etBaris.getText().toString().isEmpty()){
            Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Baris",Toast.LENGTH_SHORT).show();
            etBaris.requestFocus();
            return false;
        }

        if(!allBaris.contains(etBaris.getText().toString())){
            Toast.makeText(HarvestApp.getContext(),"Baris Yang Anda Input Tidak Ada",Toast.LENGTH_SHORT).show();
            etBaris.requestFocus();
            return false;
        }

        if(etPohon.getText().toString().isEmpty()){
            Toast.makeText(HarvestApp.getContext(),"Mohon Input No Pohon",Toast.LENGTH_SHORT).show();
            etPohon.requestFocus();
            return false;
        }
        return true;
    }

    private void saveQcAncakBaris(){
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmm");
        String idTransaksi ;
        if(selectedAncakPohon != null){
            idTransaksi = selectedAncakPohon.getIdQcAncakPohon();
        }else {
            idTransaksi = "P" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK_POHON))
                    + sdf.format(System.currentTimeMillis()) + GlobalHelper.getUser().getUserID();
        }

        QcAncakPohon qcAncakPohon = new QcAncakPohon(idTransaksi,
                Integer.parseInt(etBaris.getText().toString().isEmpty() ? "0" : etBaris.getText().toString()),
                etPohon.getText().toString(),
                Integer.parseInt(etJanjang.getText().toString().isEmpty() ? "0" : etJanjang.getText().toString()),
                Integer.parseInt(etBT.getText().toString().isEmpty() ? "0" : etBT.getText().toString()),
                Integer.parseInt(etBRD.getText().toString().isEmpty() ? "0" : etBRD.getText().toString()),
                Integer.parseInt(etOP.getText().toString().isEmpty() ? "0" : etOP.getText().toString()),
                Integer.parseInt(etPS.getText().toString().isEmpty() ? "0" : etPS.getText().toString()),
                Integer.parseInt(etSP.getText().toString().isEmpty() ? "0" : etSP.getText().toString()),
                Integer.parseInt(etBM.getText().toString().isEmpty() ? "0" : etBM.getText().toString()),
                System.currentTimeMillis(),
                ((BaseActivity) getActivity()).currentlocation.getLatitude(),
                ((BaseActivity) getActivity()).currentlocation.getLongitude()
                );

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_ANCAK_POHON);
        Log.d("lokasi pohon",db.getContext().getFilePath());
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(qcAncakPohon.getIdQcAncakPohon(),gson.toJson(qcAncakPohon));
        if(selectedAncakPohon == null) {
            repository.insert(dataNitrit);
            Log.d("input pohon",gson.toJson(dataNitrit));
            GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK_POHON);
        }else{
            repository.update(dataNitrit);
            for(int i = 0 ; i < qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().size();i++){
                QcAncakPohon qcAncakPohonX = qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().get(i);
                if(qcAncakPohonX.getIdQcAncakPohon().equalsIgnoreCase(qcAncakPohon.getIdQcAncakPohon())){
                    qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().remove(i);
                    break;
                }
            }
        }

        qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().add(qcAncakPohon);
        db.close();
    }

    private void deletedQcAncakBaris(){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_ANCAK_POHON);
        Log.d("lokasi pohon",db.getContext().getFilePath());
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(selectedAncakPohon.getIdQcAncakPohon(),gson.toJson(selectedAncakPohon));
        if(selectedAncakPohon != null) {
            repository.remove(dataNitrit);
            for(int i = 0 ; i < qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().size();i++){
                QcAncakPohon qcAncakPohonX = qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().get(i);
                if(qcAncakPohonX.getIdQcAncakPohon().equalsIgnoreCase(selectedAncakPohon.getIdQcAncakPohon())){
                    qcMutuAncakActivity.seletedQcAncak.getQcAncakPohons().remove(i);
                    break;
                }
            }
        }
        db.close();
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        private AlertDialog alertDialog;
        private Boolean validasi;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialog = WidgetHelper.showWaitingDialog(getActivity(), getResources().getString(R.string.wait));
            validasi = validasiQcAncakBaris();
        }

        @Override
        protected String doInBackground(String... params) {
            if (validasi) {
                switch (Integer.parseInt(params[0])) {
                    case LongOperation_Save:
                        saveQcAncakBaris();
                        break;
                    case LongOperation_Deleted:
                        deletedQcAncakBaris();
                        break;
                }
            }
            return String.valueOf(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(validasi) {
                switch (Integer.parseInt(result)) {
                    case LongOperation_Save:
                        Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.save_done),Toast.LENGTH_SHORT).show();
                        break;
                    case LongOperation_Deleted:
                        Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.dg_message_delete_success_inst2),Toast.LENGTH_SHORT).show();
                        break;
                }
                setSummaryQcAncak();
                clearFormInput();
//                mainMenuActivity.backProses();
            }
            if (alertDialog != null) {
                alertDialog.cancel();
            }
        }
    }
}
