package id.co.ams_plantation.harvestsap.model;

public class QcAncakPohon {
    String idQcAncakPohon;
    int noBaris;
    String idPohon;
    int janjangPanen;
    int buahTinggal;
    int brondolan;
    int overPrun;
    int pelepahSengkelan;
    int susunanPelepah;
    int buahMatahari;
    long waktuQcAncakPohon;
    double latitude;
    double longitude;

    public QcAncakPohon(String idQcAncakPohon, int noBaris, String idPohon, int janjangPanen, int buahTinggal, int brondolan, int overPrun, int pelepahSengkelan, int susunanPelepah, int buahMatahari, long waktuQcAncakPohon, double latitude, double longitude) {
        this.idQcAncakPohon = idQcAncakPohon;
        this.noBaris = noBaris;
        this.idPohon = idPohon;
        this.janjangPanen = janjangPanen;
        this.buahTinggal = buahTinggal;
        this.brondolan = brondolan;
        this.overPrun = overPrun;
        this.pelepahSengkelan = pelepahSengkelan;
        this.susunanPelepah = susunanPelepah;
        this.buahMatahari = buahMatahari;
        this.waktuQcAncakPohon = waktuQcAncakPohon;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getIdQcAncakPohon() {
        return idQcAncakPohon;
    }

    public void setIdQcAncakPohon(String idQcAncakPohon) {
        this.idQcAncakPohon = idQcAncakPohon;
    }

    public int getNoBaris() {
        return noBaris;
    }

    public void setNoBaris(int noBaris) {
        this.noBaris = noBaris;
    }

    public String getIdPohon() {
        return idPohon;
    }

    public void setIdPohon(String idPohon) {
        this.idPohon = idPohon;
    }

    public int getJanjangPanen() {
        return janjangPanen;
    }

    public void setJanjangPanen(int janjangPanen) {
        this.janjangPanen = janjangPanen;
    }

    public int getBuahTinggal() {
        return buahTinggal;
    }

    public void setBuahTinggal(int buahTinggal) {
        this.buahTinggal = buahTinggal;
    }

    public int getBrondolan() {
        return brondolan;
    }

    public void setBrondolan(int brondolan) {
        this.brondolan = brondolan;
    }

    public int getOverPrun() {
        return overPrun;
    }

    public void setOverPrun(int overPrun) {
        this.overPrun = overPrun;
    }

    public int getPelepahSengkelan() {
        return pelepahSengkelan;
    }

    public void setPelepahSengkelan(int pelepahSengkelan) {
        this.pelepahSengkelan = pelepahSengkelan;
    }

    public int getSusunanPelepah() {
        return susunanPelepah;
    }

    public void setSusunanPelepah(int susunanPelepah) {
        this.susunanPelepah = susunanPelepah;
    }

    public long getWaktuQcAncakPohon() {
        return waktuQcAncakPohon;
    }

    public void setWaktuQcAncakPohon(long waktuQcAncakPohon) {
        this.waktuQcAncakPohon = waktuQcAncakPohon;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getBuahMatahari() {
        return buahMatahari;
    }

    public void setBuahMatahari(int buahMatahari) {
        this.buahMatahari = buahMatahari;
    }
}
