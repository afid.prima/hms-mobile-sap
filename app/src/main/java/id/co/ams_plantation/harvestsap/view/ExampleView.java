package id.co.ams_plantation.harvestsap.view;

// Create all method blueprint here

import id.co.ams_plantation.harvestsap.model.ExampleModel;

public interface ExampleView extends BaseView {
    void setExampleScreen(ExampleModel exampleModel);
}
