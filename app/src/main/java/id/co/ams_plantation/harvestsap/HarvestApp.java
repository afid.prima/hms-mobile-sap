package id.co.ams_plantation.harvestsap;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;

/**
 * REPLACE THIS WITH YOUR Application Context
 */

public class HarvestApp extends Application {
    private static Context context;
    public static final String CONNECTION_PREF = "CONNECTION_PREF";
    public static final String SUPERVISION = "SUPERVISION";
    public static final String LATLON_PENDAFTARAN = "LATLON_PENDAFTARAN";
    public static final String AFDELING_BLOCK = "AFDELING_BLOCK";
    public static final String FILTER_TRANSAKSI = "FILTER_TRANSAKSI";
    public static final String STAMP_IMAGES = "STAMP_IMAGES";
    public static final String CONNECT_BUFFER_SERVER = "CONNECT_BUFFER_SERVER";
    public static final String SYNC_HISTORY = "SYNC_HISTORY";
    public static final String SYNC_TIME = "SYNC_TIME";
    public static final String SUBSTITUTE_KRANI = "SUBSTITUTE_KRANI";
    public static final String AFDELING_PANEN = "AFDELING_PANEN";
    public static final String SUBSTITUTE_KRANI_ANGKUT = "SUBSTITUTE_KRANIANGKUT";
    public static final String COUNTER_TRANSKASI = "COUNTER_TRANSKASI";
    public static final String SYNC_TRANSAKSI_AFDELING = "SYNC_TRANSAKSI_AFDELING";
    public static final String SYNC_TRANSAKSI = "SYNC_TRANSAKSI";
    public static final String TRANSAKSI_PANEN_PREF = "TRANSAKSI_PANEN_PREF";
    public static final String FOTO_PANEN_PREF = "FOTO_PANEN_PREF";
    public static final String USER_APP_PREF = "USER_APP_PREF";
    public static final String TRANSAKSI_HA_PREF = "TRANSAKSI_HA_PREF";
    public static final String FOTO_HA_PREF = "FOTO_HA_PREF";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        HarvestApp.context = getApplicationContext();
        try {
            GlobalHelper.overrideFont(getContext(),"DEFAULT",GlobalHelper.FONT_CORBERT);
            GlobalHelper.overrideFont(getContext(),"SERIF",GlobalHelper.FONT_CORBERT);
        } catch (Exception e) {
            Log.e("Can not set custom font","Fail");
        }
        CustomActivityOnCrash.install(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static Context getContext(){
        return HarvestApp.context;
    }

}
