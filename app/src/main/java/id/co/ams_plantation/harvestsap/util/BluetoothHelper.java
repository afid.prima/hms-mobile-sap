package id.co.ams_plantation.harvestsap.util;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.Cages;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.DynamicParameterPenghasilan;
import id.co.ams_plantation.harvestsap.model.Estate;
import id.co.ams_plantation.harvestsap.model.ISCCInformation;
import id.co.ams_plantation.harvestsap.model.InfoRestan;
import id.co.ams_plantation.harvestsap.model.Langsiran;
import id.co.ams_plantation.harvestsap.model.ManualSPB;
import id.co.ams_plantation.harvestsap.model.ModelRecyclerView;
import id.co.ams_plantation.harvestsap.model.Operator;
import id.co.ams_plantation.harvestsap.model.PengirimanGanda;
import id.co.ams_plantation.harvestsap.model.SPK;
import id.co.ams_plantation.harvestsap.model.SubTotalAngkut;
import id.co.ams_plantation.harvestsap.model.SubTotalAngkutMekanisasi;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.model.Vehicle;
import id.co.ams_plantation.harvestsap.service.BluetoothService;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.command.sdk.Command;
import id.co.command.sdk.PrintPicture;
import id.co.command.sdk.PrinterCommand;
import jp.wasabeef.recyclerview.adapters.SlideInLeftAnimationAdapter;

import static id.co.ams_plantation.harvestsap.util.QrHelper.BARCODE_HEIGHT;
import static id.co.ams_plantation.harvestsap.util.QrHelper.BARCODE_WIDTH;
import static id.co.ams_plantation.harvestsap.util.QrHelper.QR_HEIGHT;
import static id.co.ams_plantation.harvestsap.util.QrHelper.QR_WIDTH;

/**
 * Created by user on 11/22/2018.
 */

public class BluetoothHelper {

    public static final String TAG = "BluetoothHelper";
    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_CONNECTION_LOST = 6;
    public static final int MESSAGE_UNABLE_CONNECT = 7;

    // Key names received from the BluetoothService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    public static BluetoothDevice bluetoothDeviceSelected;

    // Name of the connected device
    public String mConnectedDeviceName = null;
    // Local Bluetooth adapter
    public BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the services
    public BluetoothService mService = null;
    public String BluetoothAddres = "";

    SharedPrefHelper sharedPrefHelper;
    AlertDialog alertDialog;
    Context context;

    public BluetoothHelper(Context context) {
        this.context = context;
        sharedPrefHelper = new SharedPrefHelper(context);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(this.context, context.getResources().getString(R.string.bt_not_available),
                    Toast.LENGTH_LONG).show();
        } else {
            mService = new BluetoothService(this.context, mHandler);
            if(bluetoothDeviceSelected != null){
                Log.d("bluetoothDeviceSelected",bluetoothDeviceSelected.getAddress());
                mService.connect(bluetoothDeviceSelected);
            }
        }
    }

    public void showListBluetoothPrinter(){

        alertDialog = null;
        final View view = LayoutInflater.from(context).inflate(R.layout.list_popup,null);
        TextView tv_header = (TextView) view.findViewById(R.id.tv_header);

        RecyclerView rv_object = (RecyclerView) view.findViewById(R.id.rv_object);
        ImageView iv_nobluetooth = (ImageView) view.findViewById(R.id.iv_nobluetooth);

        tv_header.setText(context.getResources().getString(R.string.bluetooth_device));
        // Get the local Bluetooth adapter
        BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        // Get a set of currently paired devices
        Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();


        if(pairedDevices.size() > 0){
            iv_nobluetooth.setVisibility(View.GONE);
            rv_object.setLayoutManager(new LinearLayoutManager(context));
            final AdapterBluetoothPrinter adapter = new AdapterBluetoothPrinter(context,pairedDevices);
            final SlideInLeftAnimationAdapter adapterLeft = new SlideInLeftAnimationAdapter(adapter);
            adapterLeft.setFirstOnly(false);
//       adapterLeft.setInterpolator(new OvershootInterpolator(1f));
            adapterLeft.setDuration(400);
            rv_object.setAdapter(adapterLeft);
        }
        alertDialog = WidgetHelper.showListReference(alertDialog,view,context);
    }

    public void selectBluetooth(BluetoothDevice bluetoothDevice){
        if (BluetoothAdapter.checkBluetoothAddress(bluetoothDevice.getAddress())) {
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(bluetoothDevice.getAddress());
            bluetoothDeviceSelected = device;
            mService.connect(device);
        }
        alertDialog.dismiss();
    }

    /*
         *SendDataByte
         */
    public void SendDataByte(byte[] data) {

        if (mService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(context, context.getResources().getString(R.string.bluetooth_not_connected) , Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        mService.write(data);
    }

    public void printPanen(String textQr, TransaksiPanen transaksiPanen) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm");
        String alasBrondol = transaksiPanen.getHasilPanen().isAlasanBrondolan() == true ? "Yes" : "No";
        Estate estate = GlobalHelper.getEstateByEstCode(transaksiPanen.getTph().getEstCode());
        String format1 = "%-11s";
        String format2 = "%-15s";
        String format3 = "%-15s";
        String format4 = "%14s";
        String format5 = "%-14s";
        String format6 = "%-14s|";
        try {
            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x11;
            SendDataByte(Command.GS_ExclamationMark);
            SendDataByte(PrinterCommand.POS_Print_Text(context.getResources().getString(R.string.transaksi_panen) + "\n", "GBK", 0, 1, 1, 35));

//            String oktextQr = textQr.replace("\n", "");
//            Log.d("BluetoothHelpder textQr", oktextQr);
//            byte[] qrcode = PrinterCommand.getBarCommand(oktextQr, 0, 3, 8);
////            byte[] qrcode = PrinterCommand.getBarCommand("Zijiang Electronic Thermal Receipt Printer!", 0, 3, 8);
            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);
//            SendDataByte(qrcode);

//            byte[] data = PrintPicture.POS_PrintBMP(setQr(oktextQr), 384, 0);
//            SendDataByte(data);

            Command.ESC_Align[2] = 0x00;
            SendDataByte(PrinterCommand.POS_Set_Bold(1));
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);
            SendDataByte(PrinterCommand.POS_Print_Text("========="+ context.getResources().getString(R.string.info_transaksi)+"=========\n", "GBK", 0, 0, 0, 0));
            if(estate != null) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " + String.format(format2, estate.getCompanyShortName()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " + String.format(format2, estate.getEstName()) + "\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_estate) + transaksiPanen.getTph().getEstCode()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_estate) + transaksiPanen.getTph().getEstCode()) + "\n", "GBK", 0, 0, 0, 0));
            }

            User userSelected = null;
            if(this.context instanceof BaseActivity){
                userSelected = GlobalHelper.dataAllUser.get(transaksiPanen.getCreateBy());
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Input Oleh")+" : "+String.format(format2, userSelected != null ? userSelected.getUserFullName() : "-")+"\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Input Oleh")+" : "+String.format(format2, transaksiPanen.getCreateBy())+"\n", "GBK", 0, 0, 0, 0));
            }
            if(transaksiPanen.getSubstitute() != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Krani Penganti")+" : "+String.format(format2, transaksiPanen.getSubstitute().getNama())+"\n", "GBK", 0, 0, 0, 0));
            }

            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Waktu Input")+" : "+String.format(format2, sdf.format(transaksiPanen.getCreateDate()))+"\n", "GBK", 0, 0, 0, 0));

            SendDataByte(PrinterCommand.POS_Print_Text("============"+ context.getResources().getString(R.string.info_tph)+"============\n", "GBK", 0, 0, 0, 0));
            if(transaksiPanen.getTph().getNamaTph() != null) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Afdeling") + " : " + String.format(format2, transaksiPanen.getTph().getAfdeling()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Block") + " : " + String.format(format2, transaksiPanen.getTph().getBlock()) + "\n", "GBK", 0, 0, 0, 0));
//                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Ancak")+" : " + String.format(format2, transaksiPanen.getAncak()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Nama TPH")+" : " + String.format(format2, transaksiPanen.getTph().getNamaTph()) + "\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Afdeling") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_tph) + transaksiPanen.getTph().getNoTph()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Block") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_tph) + transaksiPanen.getTph().getNoTph()) + "\n", "GBK", 0, 0, 0, 0));
//                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Ancak")+" : " + String.format(format2, transaksiPanen.getAncak()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Nama TPH")+" : " + String.format(format2, context.getResources().getString(R.string.dont_have_tph) + transaksiPanen.getTph().getNoTph()) + "\n", "GBK", 0, 0, 0, 0));
            }
//            SendDataByte(PrinterCommand.POS_Print_Text("Baris: "+transaksiPanen.getBaris()+"\n", "GBK", 0, 0, 0, 0));
//
            SendDataByte(PrinterCommand.POS_Print_Text("=========="+ context.getResources().getString(R.string.panen_tph)+"==========\n", "GBK", 0, 0, 0, 0));
            if(transaksiPanen.getKongsi().getSubPemanens() != null) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6, "     Gang     ") + " " +
                        String.format(format5, "    Pemanen   ") + "\n", "GBK", 0, 0, 0, 0));
                for(int i = 0 ; i < transaksiPanen.getKongsi().getSubPemanens().size(); i++){
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6, transaksiPanen.getKongsi().getSubPemanens().get(i).getPemanen().getGank())+ " " +
                            String.format(format5, transaksiPanen.getKongsi().getSubPemanens().get(i).getPemanen().getNama()) + "\n", "GBK", 0, 0, 0, 0));
                }
            }else if(transaksiPanen.getPemanen().getNama() != null) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Gang")+" : " + String.format(format5, transaksiPanen.getPemanen().getGank()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Pemanen")+" : " + String.format(format5, transaksiPanen.getPemanen().getNama()) + "\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Gang")+" : " + String.format(format5, context.getResources().getString(R.string.dont_have_pemanen) + transaksiPanen.getPemanen().getKodePemanen()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Pemanen")+" : " + String.format(format5, context.getResources().getString(R.string.dont_have_pemanen) + transaksiPanen.getPemanen().getKodePemanen()) + "\n", "GBK", 0, 0, 0, 0));
            }
            SendDataByte(PrinterCommand.POS_Print_Text("==========="+ context.getResources().getString(R.string.info_panen)+"===========\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5, "Σ Jjg")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getTotalJanjang()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5, "Σ Jjg Pendapat")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getTotalJanjangPendapatan()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Janjang Masak")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getJanjangNormal()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Mentah")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getBuahMentah()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Busuk & Jangkos")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getBusukNJangkos()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Lewat Matang")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getBuahLewatMatang()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Abnormal")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getBuahAbnormal()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Tangkai Panjang")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getTangkaiPanjang()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Dimakan Tikus")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getBuahDimakanTikus()) + " Jjg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Brondolan")+" : "+String.format(format4, String.format("%,d",transaksiPanen.getHasilPanen().getBrondolan())+"  Kg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3, "Alas Brondolan")+" : "+String.format(format5,alasBrondol)+"\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format3,"Waktu Panen")+" : "+sdf.format(transaksiPanen.getHasilPanen().getWaktuPanen())+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte("================================\n".getBytes("GBK"));
            SendDataByte(PrinterCommand.POS_Print_Text("Print "+ GlobalHelper.getUser().getUserFullName()+" "+ sdf.format(System.currentTimeMillis()) +"\n", "GBK", 0, 0, 0, 2));
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(58));
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(58));
            SendDataByte(Command.GS_V_m_n);
        }catch (Exception e){
            e.printStackTrace();
//        } catch (UnsupportedEncodingException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
        }

    }

    public void printTransaksiAngkut(String textQr, TransaksiAngkut transaksiAngkut,ArrayList<Angkut> angkutView) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm");
        String tujuan = transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_PKS ? TransaksiAngkut.PKS : TransaksiAngkut.LANGSIR;
        if(tujuan.equals(TransaksiAngkut.LANGSIR) && transaksiAngkut.getLangsiran() != null){
            tujuan = transaksiAngkut.getLangsiran().getNamaTujuan();
        }

        int angkutTph = 0;
        int angkutTransit = 0;
        int angkutManual = 0;

        for (int i = 0 ; i < angkutView.size(); i++){
            if(angkutView.get(i).getTransaksiPanen() != null){
                angkutTph ++;
            }else if (angkutView.get(i).getTransaksiAngkut() != null){
                angkutTransit++;
            }else{
                angkutManual ++;
            }
        }

        Estate estate = GlobalHelper.getEstateByEstCode(transaksiAngkut.getEstCode());
        String format1 = "%-11s";
        String format2 = "%-15s";
        String format3 = "%-9s|";
        String format4 = "%5s|";
        String format5 = "|%2s|";
        String format6 = "%-18s";
        String format7 = "%-15s";
        String format8 = "|%-12s|";
        String format9 = "%-7s|";
        String format10 = "|%-30s|";

        try {
            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x11;
            SendDataByte(Command.GS_ExclamationMark);
            SendDataByte(PrinterCommand.POS_Print_Text("Surat Pengantaran\nLangsiran" + "\n", "GBK", 0, 1, 1, 35));

            String text = textQr.replace("\n","");

//            String oktextQr = textQr.replace("\n","");
//            Log.d("BluetoothHelpder textQr",oktextQr);
//            byte[] qrcode = PrinterCommand.getBarCommand(oktextQr, 0, 1, 6);
            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);
            //SendDataByte(qrcode);

            byte[] data = PrintPicture.POS_PrintBMP(setQr(text), 384, 0);
            SendDataByte(data);

            Command.ESC_Align[2] = 0x00;
            SendDataByte(PrinterCommand.POS_Set_Bold(1));
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);
            SendDataByte(PrinterCommand.POS_Print_Text("========="+ context.getResources().getString(R.string.info_transaksi)+"=========\n", "GBK", 0, 0, 0, 0));
            if(estate != null) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " + String.format(format2, estate.getCompanyShortName()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " + String.format(format2, estate.getEstName()) + "\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_estate) + transaksiAngkut.getEstCode()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_estate) + transaksiAngkut.getEstCode()) + "\n", "GBK", 0, 0, 0, 0));
            }

            User userSelected = null;
            if(this.context instanceof BaseActivity){
                userSelected = GlobalHelper.dataAllUser.get(transaksiAngkut.getCreateBy());
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Input Oleh")+" : "+String.format(format2, userSelected != null ?  userSelected.getUserFullName() : "-")+"\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Input Oleh")+" : "+String.format(format2, transaksiAngkut.getCreateBy())+"\n", "GBK", 0, 0, 0, 0));
            }
            if(transaksiAngkut.getSubstitute() != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Krn Penganti")+" : "+String.format(format2, transaksiAngkut.getSubstitute().getNama())+"\n", "GBK", 0, 0, 0, 0));
            }

            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Waktu Input")+" : "+String.format(format2, sdf.format(transaksiAngkut.getEndDate()))+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "ID Trx")+" : "+String.format(format2, transaksiAngkut.getIdTAngkut())+"\n", "GBK", 0, 0, 0, 0));

            if(transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR) {
                if (transaksiAngkut.getLangsiran() != null) {
                    if (transaksiAngkut.getLangsiran().getNamaTujuan() != null) {
                        Estate estateLangsiran = GlobalHelper.getEstateByEstCode(transaksiAngkut.getLangsiran().getEstCode());
                        SendDataByte(PrinterCommand.POS_Print_Text("=========="+ context.getResources().getString(R.string.info_langsiran)+"========\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " +String.format(format2, estateLangsiran.getCompanyShortName())+"\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " +String.format(format2, estateLangsiran.getEstName())+"\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Afdeling") + " : " +String.format(format2, transaksiAngkut.getLangsiran().getAfdeling())+"\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Block") + " : " +String.format(format2, transaksiAngkut.getLangsiran().getBlock())+"\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Nama") + " : " +String.format(format2, transaksiAngkut.getLangsiran().getNamaTujuan())+"\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Type") + " : " +String.format(format2, Langsiran.LIST_TIPE_LANGSIRAN[transaksiAngkut.getLangsiran().getTypeTujuan()])+"\n", "GBK", 0, 0, 0, 0));
                    }
                }
            }

            Operator operator = transaksiAngkut.getSupir();
            SendDataByte(PrinterCommand.POS_Print_Text("==========="+ context.getResources().getString(R.string.info_vehicle)+"=========\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Supir") + " : " +String.format(format2, operator != null ?
                    operator.getNama() : '-'
            )+"\n", "GBK", 0, 0, 0, 0));

            Vehicle vehicle= transaksiAngkut.getVehicle();
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Kendaraan") + " : "+String.format(format2,
                    TransaksiAngkutHelper.showVehicle(vehicle)
            )+"\n", "GBK", 0, 0, 0, 0));

            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Tujuan") + " : "+String.format(format2, tujuan)+"\n", "GBK", 0, 0, 0, 0));

            if(transaksiAngkut.getBin() != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Bin") + " : "+String.format(format2, transaksiAngkut.getBin())+"\n", "GBK", 0, 0, 0, 0));
            }

            if(transaksiAngkut.getPengirimanGandas() != null) {
                Collections.sort(transaksiAngkut.getPengirimanGandas(), new Comparator<PengirimanGanda>() {
                    @Override
                    public int compare(PengirimanGanda o1, PengirimanGanda o2) {
                        return o1.getIdx() < o2.getIdx() ? -1 : (o1.getIdx() > o2.getIdx()) ? 1 : 0;
                    }
                });

                SendDataByte(PrinterCommand.POS_Print_Text("======"+ context.getResources().getString(R.string.info_pengiriman_lanjut)+"====\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5,"Ke")+
                        String.format(format9,"Pemilik")+
                        String.format(format3,"Unit")+
                        String.format(format3,"Operator")+"\n", "GBK", 0, 0, 0, 0));
                for (int i = 0; i < transaksiAngkut.getPengirimanGandas().size(); i++) {
                    PengirimanGanda ganda = transaksiAngkut.getPengirimanGandas().get(i);
                    int ke = i + 2;
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5, ke)+
                            String.format(format9,ganda.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN ? TransaksiAngkut.KEBUN : TransaksiAngkut.LUAR)+
                            String.format(format3,TransaksiAngkutHelper.showVehicle(ganda.getVehicle()))+
                            String.format(format3,ganda.getOperator().getNama())+"\n", "GBK", 0, 0, 0, 0));
                }
            }

            if(transaksiAngkut.getLoader() != null){
                SendDataByte(PrinterCommand.POS_Print_Text("==========="+ context.getResources().getString(R.string.info_loader)+"==========\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format8,"Nama")+
                        String.format(format3,"NIK")+
                        String.format(format9,"Estate")+"\n", "GBK", 0, 0, 0, 0));
                for (int i = 0; i < transaksiAngkut.getLoader().size(); i++) {
                    Operator loader = transaksiAngkut.getLoader().get(i);
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format8,loader.getNama())+
                            String.format(format3,loader.getNik())+
                            String.format(format9,loader.getArea())+"\n", "GBK", 0, 0, 0, 0));
                }
            }

            SendDataByte(PrinterCommand.POS_Print_Text("============"+ context.getResources().getString(R.string.info_angkut)+"=========\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ Angkut TPH") +" : "+String.format("%,d",angkutTph)+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ Angkut Langsiran") +" : "+String.format("%,d",angkutTransit)+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ Angkut Manual") +" : "+String.format("%,d",angkutManual)+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ Angkut") +" : "+String.format("%,d",angkutView.size())+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format7,"Mulai Angkut") +" : "+sdf.format(transaksiAngkut.getStartDate())+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format7,"Selesai Angkut") +" : "+sdf.format(transaksiAngkut.getEndDate())+"\n", "GBK", 0, 0, 0, 0));

            SendDataByte(PrinterCommand.POS_Print_Text("==========="+ context.getResources().getString(R.string.tbs_angkut)+"===========\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Janjang")+" : "+ String.format(format2,String.format("%,d",transaksiAngkut.getTotalJanjang()) + " Jjg") +"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Brondolan")+" : "+String.format(format2,String.format("%,d",transaksiAngkut.getTotalBrondolan()) + " Kg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Berat")+" : "+String.format(format2,String.format("%.0f",transaksiAngkut.getTotalBeratEstimasi()) + " Kg")+"\n", "GBK", 0, 0, 0, 0));

            Collections.sort(angkutView, new Comparator<Angkut>() {
                @Override
                public int compare(Angkut t0, Angkut t1) {
                    String a = t0.getBlock();
                    String b = t1.getBlock();
                    return (a.compareTo(b));
                }
            });

            Angkut subTotalAngkutBlock = null;
            int count = 0;
            for (int i = 0 ; i < angkutView.size(); i++){
                Angkut angkut = angkutView.get(i);
                count = count + 1;
                if(i == 0 ){
                    SendDataByte(PrinterCommand.POS_Print_Text("=========="+ context.getResources().getString(R.string.detail_angkut)+"=========\n", "GBK", 0, 0, 0, 0));
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5,"No")+ String.format(format3,"Angkut")+
                            String.format(format4,"Jjg")+
                            String.format(format4,"Brdl")+
                            String.format(format4,"Berat")+"\n", "GBK", 0, 0, 0, 0));
                }
                String nama = "M";
                String nama1 = "0";
                String nama2 = "0";
                String nama3 = "0";
                String block = "";
                if(angkut.getTransaksiPanen() != null){
                    nama = "TPH";
                    if(angkut.getTransaksiPanen().getTph() != null){
                        nama = angkut.getTransaksiPanen().getTph().getNamaTph() != null ?
                                angkut.getTransaksiPanen().getTph().getNamaTph() : "TPH";
                        nama += angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                "/" +angkut.getTransaksiPanen().getTph().getBlock() : "-";
                        block = angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                angkut.getTransaksiPanen().getTph().getBlock() : "-";
                    }
                }else if (angkut.getTph() != null) {
                    nama = angkut.getTph().getNamaTph() != null ?
                            angkut.getTph().getNamaTph() : "TPH";
                    nama += angkut.getTph().getBlock() != null ?
                            "/" +angkut.getTph().getBlock() : "-";
                    block = angkut.getTph().getBlock() != null ?
                            angkut.getTph().getBlock() : "-";
                    nama += " M";
                }else if (angkut.getTransaksiAngkut() != null){
                    nama = "Langsiran";
                    if(angkut.getTransaksiAngkut().getLangsiran() != null){
//                        nama = angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() != null ?
//                                angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() : "Langsiran";
                        nama = "L";
                        nama += angkut.getTransaksiAngkut().getLangsiran().getBlock() != null ?
                                "/" + angkut.getTransaksiAngkut().getLangsiran().getBlock() : "-";
                        block = angkut.getBlock() != null ?
                                angkut.getBlock() : "-";
                    }
                }else if (angkut.getLangsiran() != null) {
//                    nama = angkut.getLangsiran().getNamaTujuan() != null ?
//                            angkut.getLangsiran().getNamaTujuan(): "Langsiran";
                    nama = "L";
                    nama += angkut.getLangsiran().getBlock() != null ?
                            "/" +angkut.getLangsiran().getBlock(): "-";
                    block = angkut.getLangsiran().getBlock() != null ?
                            angkut.getLangsiran().getBlock(): "-";
                    nama += " M";
                }

                if(subTotalAngkutBlock == null){
                    subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
                            angkut.getBrondolan(),
                            block,
                            angkut.getBerat());
                }else if(subTotalAngkutBlock.getBlock().equals(block)){
                    subTotalAngkutBlock.setJanjang(subTotalAngkutBlock.getJanjang() + angkut.getJanjang());
                    subTotalAngkutBlock.setBrondolan(subTotalAngkutBlock.getBrondolan() + angkut.getBrondolan());
                    subTotalAngkutBlock.setBerat(subTotalAngkutBlock.getBerat() + angkut.getBerat());
                }else if(!subTotalAngkutBlock.getBlock().equals(block)){
                    SendDataByte("================================\n".getBytes("GBK"));
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format8,"Block "+subTotalAngkutBlock.getBlock())+
                            String.format(format4,subTotalAngkutBlock.getJanjang())+
                            String.format(format4,subTotalAngkutBlock.getBrondolan())+
                            String.format(format4,String.format("%.0f",subTotalAngkutBlock.getBerat()))+"\n", "GBK", 0, 0, 0, 0));
                    SendDataByte("================================\n".getBytes("GBK"));
                    subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
                            angkut.getBrondolan(),
                            block,
                            angkut.getBerat());
                    count = 1;
                }


                nama1 = String.format("%,d",angkut.getJanjang());
                nama2 = String.format("%,d",angkut.getBrondolan());
                nama3 = String.format("%.0f",angkut.getBerat());
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5, count) +String.format(format3,nama)+
                        String.format(format4,nama1)+
                        String.format(format4,nama2)+
                        String.format(format4,nama3)+"\n", "GBK", 0, 0, 0, 0));

//                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format10,String.valueOf(angkut.getOph()))+"\n", "GBK", 0, 0, 0, 0));
            }

            SendDataByte("================================\n".getBytes("GBK"));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format8,"Block "+subTotalAngkutBlock.getBlock())+
                    String.format(format4,subTotalAngkutBlock.getJanjang())+
                    String.format(format4,subTotalAngkutBlock.getBrondolan())+
                    String.format(format4,String.format("%.0f",subTotalAngkutBlock.getBerat()))+"\n", "GBK", 0, 0, 0, 0));

            SendDataByte("================================\n".getBytes("GBK"));
            SendDataByte(PrinterCommand.POS_Print_Text("Print "+ GlobalHelper.getUser().getUserFullName()+" "+ sdf.format(System.currentTimeMillis()) +"\n", "GBK", 0, 0, 0, 2));
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(58));
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(58));
            SendDataByte(Command.GS_V_m_n);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void printRekonsilasi(String textQr, TransaksiAngkut transaksiAngkut, int totalKartuRusak, int totalKartuHilang,ArrayList<Angkut> angkutView){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm");
        String tujuan = transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_PKS ? TransaksiAngkut.PKS : TransaksiAngkut.LANGSIR;
        if(tujuan.equals(TransaksiAngkut.LANGSIR) && transaksiAngkut.getLangsiran() != null){
            tujuan = transaksiAngkut.getLangsiran().getNamaTujuan();
        }

        int angkutTph = 0;
        int angkutTransit = 0;
        int angkutManual = 0;

        for (int i = 0 ; i < angkutView.size(); i++){
            if(angkutView.get(i).getTransaksiPanen() != null){
                angkutTph ++;
            }else if (angkutView.get(i).getTransaksiAngkut() != null){
                angkutTransit++;
            }else{
                angkutManual ++;
            }
        }

        Estate estate = GlobalHelper.getEstateByEstCode(transaksiAngkut.getEstCode());
        String format1 = "%-11s";
        String format2 = "%-15s";
        String format3 = "%-9s|";
        String format4 = "%5s|";
        String format5 = "|%2s|";
        String format6 = "%-18s";
        String format7 = "%-15s";
        String format8 = "|%-12s|";
        String format9 = "%6s|";
        String format10 = "%-13s|";
        String format11 = "|%-16s|";

        try {
//            Command.ESC_Align[2] = 0x01;
//            SendDataByte(Command.ESC_Align);
//            Command.GS_ExclamationMark[2] = 0x11;
//            SendDataByte(Command.GS_ExclamationMark);
//            SendDataByte(PrinterCommand.POS_Print_Text(context.getResources().getString(R.string.surat_rekonsilasi) + "\n", "GBK", 0, 1, 1, 34));
//
//            String qrData = textQr.replace("\n","");
//
////            String oktextQr = textQr.replace("\n","");
////            Log.d("BluetoothHelpder textQr",oktextQr);
////            byte[] qrcode = PrinterCommand.getBarCommand(oktextQr, 0, 1, 6);
//            Command.ESC_Align[2] = 0x01;
//            SendDataByte(Command.ESC_Align);
//            //SendDataByte(qrcode);
//
//            byte[] data = PrintPicture.POS_PrintBMP(setQr(qrData), 384, 0);
//            SendDataByte(data);

            /*
             * barCodeTAngkut
             */
            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x11;
            SendDataByte(Command.GS_ExclamationMark);
            SendDataByte(PrinterCommand.POS_Print_Text(context.getResources().getString(R.string.surat_rekonsilasi) + "\n", "GBK", 0, 1, 1, 34));

            String idTAngkut = transaksiAngkut.getIdTAngkut();
            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);

            byte[] dataTAngkut = PrintPicture.POS_PrintBMP(setBarcode(idTAngkut), 384, 0);
            SendDataByte(PrinterCommand.POS_Print_Text("================================\n", "GBK", 0, 0, 0, 0));

            SendDataByte(dataTAngkut);
            SendDataByte("\n".getBytes("GBK"));
            SendDataByte(PrinterCommand.POS_Print_Text(idTAngkut + "\n", "GBK", 0, 0, 0, 5));
            SendDataByte(PrinterCommand.POS_Print_Text("================================\n", "GBK", 0, 0, 0, 0));

            Command.ESC_Align[2] = 0x00;
            SendDataByte(PrinterCommand.POS_Set_Bold(1));
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);
            SendDataByte(PrinterCommand.POS_Print_Text("======="+ context.getResources().getString(R.string.info_rekonsilasi)+"========\n", "GBK", 0, 0, 0, 0));
            if(estate != null) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " + String.format(format2, estate.getCompanyShortName()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " + String.format(format2, estate.getEstName()) + "\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_estate) + transaksiAngkut.getEstCode()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_estate) + transaksiAngkut.getEstCode()) + "\n", "GBK", 0, 0, 0, 0));
            }

            User userSelected = null;
            if(this.context instanceof BaseActivity){
                userSelected = GlobalHelper.dataAllUser.get(transaksiAngkut.getCreateBy());
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Input Oleh")+" : "+String.format(format2, userSelected != null ?  userSelected.getUserFullName() : "-")+"\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Input Oleh")+" : "+String.format(format2, transaksiAngkut.getCreateBy())+"\n", "GBK", 0, 0, 0, 0));
            }
            if(transaksiAngkut.getSubstitute() != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Krani Penganti")+" : "+String.format(format2, transaksiAngkut.getSubstitute().getNama())+"\n", "GBK", 0, 0, 0, 0));
            }

            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Waktu Input")+" : "+String.format(format2, sdf.format(transaksiAngkut.getEndDate()))+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "ID Transaksi")+" : "+String.format(format2, transaksiAngkut.getIdTAngkut())+"\n", "GBK", 0, 0, 0, 0));

            Operator operator = transaksiAngkut.getSupir();
            SendDataByte(PrinterCommand.POS_Print_Text("==========="+ context.getResources().getString(R.string.info_vehicle)+"=========\n", "GBK", 0, 0, 0, 0));
            if(operator != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Supir") + " : " +String.format(format2, operator.getNama())+"\n", "GBK", 0, 0, 0, 0));
            }else {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Supir") + " : " +String.format(format2, "")+"\n", "GBK", 0, 0, 0, 0));
            }

            Vehicle vehicle= transaksiAngkut.getVehicle();
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Kendaraan") + " : "+String.format(format2,
                    TransaksiAngkutHelper.showVehicle(vehicle)
            )+"\n", "GBK", 0, 0, 0, 0));

            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Tujuan") + " : "+String.format(format2, tujuan)+"\n", "GBK", 0, 0, 0, 0));

            if(transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR) {
                if (transaksiAngkut.getLangsiran() != null) {
                    if (transaksiAngkut.getLangsiran().getNamaTujuan() != null) {
                        Estate estateLangsiran = GlobalHelper.getEstateByEstCode(transaksiAngkut.getLangsiran().getEstCode());
                        SendDataByte(PrinterCommand.POS_Print_Text("=========="+ context.getResources().getString(R.string.info_langsiran)+"========\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " +String.format(format2, estateLangsiran.getCompanyShortName())+"\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " +String.format(format2, estateLangsiran.getEstName())+"\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Afdeling") + " : " +String.format(format2, transaksiAngkut.getLangsiran().getAfdeling())+"\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Block") + " : " +String.format(format2, transaksiAngkut.getLangsiran().getBlock())+"\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Nama") + " : " +String.format(format2, transaksiAngkut.getLangsiran().getNamaTujuan())+"\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Type") + " : " +String.format(format2, Langsiran.LIST_TIPE_LANGSIRAN[transaksiAngkut.getLangsiran().getTypeTujuan()])+"\n", "GBK", 0, 0, 0, 0));
                    }
                }
            }


            SendDataByte(PrinterCommand.POS_Print_Text("============"+ context.getResources().getString(R.string.info_angkut)+"=========\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ Angkut TPH") +" : "+String.format("%,d",angkutTph)+"\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ Angkut Langsiran") +" : "+String.format("%,d",angkutTransit)+"\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ Angkut Manual") +" : "+String.format("%,d",angkutManual)+"\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ Angkut") +" : "+String.format("%,d",transaksiAngkut.getAngkuts().size())+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format7,"Σ Manual Data") +" : "+String.format("%,d", angkutManual)+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format7,"Mulai Angkut") +" : "+sdf.format(transaksiAngkut.getStartDate())+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format7,"Selesai Angkut") +" : "+sdf.format(transaksiAngkut.getEndDate())+"\n", "GBK", 0, 0, 0, 0));

//            SendDataByte(PrinterCommand.POS_Print_Text("==========="+ context.getResources().getString(R.string.tbs_angkut)+"===========\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text("============ALASAN==============\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Janjang")+" : "+ String.format(format2,String.format("%,d",transaksiAngkut.getTotalJanjang()) + " Jjg") +"\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Brondolan")+" : "+String.format(format2,String.format("%,d",transaksiAngkut.getTotalBrondolan()) + " Kg")+"\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Berat")+" : "+String.format(format2,String.format("%.0f",transaksiAngkut.getTotalBeratEstimasi()) + " Kg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Kartu Rusak")+" : "+ String.format(format2,String.format("%,d",totalKartuRusak)) +"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Kartu Hilang")+" : "+String.format(format2,String.format("%,d",totalKartuHilang))+"\n", "GBK", 0, 0, 0, 0));


            Collections.sort(angkutView, new Comparator<Angkut>() {
                @Override
                public int compare(Angkut t0, Angkut t1) {
                    String a = t0.getBlock();
                    String b = t1.getBlock();
                    return (a.compareTo(b));
                }
            });

            Angkut subTotalAngkutBlock = null;
            int count = 0;
            int totalBlockKartuRusak = 0;
            int totalBlockKartuHilang = 0;
            boolean awal = true;
            for (int i = 0 ; i < angkutView.size(); i++){
                Angkut angkut = angkutView.get(i);
                if(angkut.getManualSPBData()!=null){
                    count = count + 1;
                    int subKartuHilang=0;
                    int subKartuRusak=0;
                    if(awal){
                        SendDataByte(PrinterCommand.POS_Print_Text("=========="+ context.getResources().getString(R.string.detail_angkut)+"=========\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5,"No")+ String.format(format10,"TPH")+
                                String.format(format9,"Rusak")+
                                String.format(format9,"Hilang")+"\n", "GBK", 0, 0, 0, 0));
                        awal = false;
                    }
                    String nama = "M";
                    String nama1 = "0";
                    String nama2 = "0";
                    String block = "";

                    if(angkut.getTransaksiPanen() != null){
                        nama = "TPH";
                        if(angkut.getTransaksiPanen().getTph() != null){
                            nama = angkut.getTransaksiPanen().getTph().getNamaTph() != null ?
                                    angkut.getTransaksiPanen().getTph().getNamaTph() : "TPH";
                            nama += angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                    "/" +angkut.getTransaksiPanen().getTph().getBlock() : "-";
                            block = angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                    angkut.getTransaksiPanen().getTph().getBlock() : "-";
                        }
                    }else if (angkut.getTph() != null) {
                        nama = angkut.getTph().getNamaTph() != null ?
                                angkut.getTph().getNamaTph() : "TPH";
                        nama += angkut.getTph().getBlock() != null ?
                                "/" +angkut.getTph().getBlock() : "-";
                        block = angkut.getTph().getBlock() != null ?
                                angkut.getTph().getBlock() : "-";
//                    nama += " M";
                    }else if (angkut.getTransaksiAngkut() != null){
                        nama = "Langsiran";
                        if(angkut.getTransaksiAngkut().getLangsiran() != null){
//                            nama = angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() != null ?
//                                    angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() : "Langsiran";
                            nama = "L";
//                            nama += angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() != null ?
//                                    "/"+angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() : "-";
                            block = angkut.getTransaksiAngkut().getLangsiran().getBlock() != null ?
                                    angkut.getTransaksiAngkut().getLangsiran().getBlock() : "-";
                        }
                    }else if (angkut.getLangsiran() != null) {
//                        nama = angkut.getLangsiran().getNamaTujuan() != null ?
//                                angkut.getLangsiran().getNamaTujuan(): "Langsiran";
                        nama = "L";
                        nama += angkut.getLangsiran().getBlock() != null ?
                                "/" +angkut.getLangsiran().getBlock(): "-";
                        block = angkut.getLangsiran().getBlock() != null ?
                                angkut.getLangsiran().getBlock(): "-";
//                    nama += " M";
                    }
//                if(subTotalAngkutBlock == null){
//                    subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
//                            angkut.getBrondolan(),
//                            block,
//                            angkut.getBerat());
//                }else if(subTotalAngkutBlock.getBlock().equals(block)){
//                    subTotalAngkutBlock.setJanjang(subTotalAngkutBlock.getJanjang() + angkut.getJanjang());
//                    subTotalAngkutBlock.setBrondolan(subTotalAngkutBlock.getBrondolan() + angkut.getBrondolan());
//                    subTotalAngkutBlock.setBerat(subTotalAngkutBlock.getBerat() + angkut.getBerat());
//                }else if(!subTotalAngkutBlock.getBlock().equals(block)){
//                    SendDataByte("================================\n".getBytes("GBK"));
//                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format8,"Block "+subTotalAngkutBlock.getBlock())+
//                            String.format(format4,subTotalAngkutBlock.getJanjang())+
//                            String.format(format4,subTotalAngkutBlock.getBrondolan())+
//                            String.format(format4,String.format("%.0f",subTotalAngkutBlock.getBerat()))+"\n", "GBK", 0, 0, 0, 0));
//                    SendDataByte("================================\n".getBytes("GBK"));
//                    subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
//                            angkut.getBrondolan(),
//                            block,
//                            angkut.getBerat());
//                    count = 1;
//                }
                    if(subTotalAngkutBlock == null){
                        subTotalAngkutBlock = angkut;
                        if(angkut.getManualSPBData()!=null){
                            if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                                totalBlockKartuRusak++;
                            }else{
                                totalBlockKartuHilang++;
                            }
                        }
                    }else if(subTotalAngkutBlock.getBlock().equals(block)){
                        if(angkut.getManualSPBData()!=null){
                            if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                                totalBlockKartuRusak++;
                            }else{
                                totalBlockKartuHilang++;
                            }
                        }
                    }else if(!subTotalAngkutBlock.getBlock().equals(block)){
//                    arrayList.add(new ModelRecyclerView("subTotal","Block "+subTotalAngkutBlock.getBlock() ,
//                            String.format("%,d", totalBlockKartuRusak),
//                            String.format("%,d", totalBlockKartuHilang)));
//                    Log.i("getManualSPBData", "totalBlockKartuRusak: "+totalBlockKartuRusak+" totalBlockKartuHilang: "+totalBlockKartuHilang);
                        SendDataByte("================================\n".getBytes("GBK"));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format11,"Block "+subTotalAngkutBlock.getBlock())+
                                String.format(format9,totalBlockKartuRusak)+
                                String.format(format9,totalBlockKartuHilang)+"\n", "GBK", 0, 0, 0, 0));
                        SendDataByte("================================\n".getBytes("GBK"));
                        totalBlockKartuRusak = 0;
                        totalBlockKartuHilang = 0;
                        subTotalAngkutBlock = angkut;
                        if(subTotalAngkutBlock.getManualSPBData().getTipe().equals("Kartu Rusak")){
                            totalBlockKartuRusak++;
                        }else{
                            totalBlockKartuHilang++;
                        }
                        count = 1;
                    }

                    if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                        subKartuRusak = 1;
                        subKartuHilang = 0;
                    }else{
                        subKartuRusak = 0;
                        subKartuHilang = 1;
                    }

//                nama1 = String.format("%,d",angkut.getJanjang());
//                nama2 = String.format("%,d",angkut.getBrondolan());
//                nama3 = String.format("%.0f",angkut.getBerat());
//                if(angkut.getManualSPBData().getTipeKongsi().equals("Kartu Rusak")){
//                    nama1 = String.format("%,d", 1);
//                    nama2 = String.format("%,d", 0);
//                }else{
//                    nama1 = String.format("%,d", 0);
//                    nama2 = String.format("%,d", 1);
//                }
                    nama1 = String.format("%,d",subKartuRusak) ;
                    nama2 = String.format("%,d",subKartuHilang) ;

                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5, count) +String.format(format10,nama)+
                            String.format(format9,nama1)+
                            String.format(format9,nama2)+"\n", "GBK", 0, 0, 0, 0));
                    if(i == transaksiAngkut.getAngkuts().size()-1){
                        SendDataByte("================================\n".getBytes("GBK"));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format11,"Block "+subTotalAngkutBlock.getBlock())+
                                String.format(format9,totalBlockKartuRusak)+
                                String.format(format9,totalBlockKartuHilang)+"\n", "GBK", 0, 0, 0, 0));
                        SendDataByte("================================\n".getBytes("GBK"));

                        Log.i("getManualSPBData", "totalBlockKartuRusak: "+totalBlockKartuRusak+" totalBlockKartuHilang: "+totalBlockKartuHilang);
                    }
                }
            }

//            Command.ESC_Align[2] = 0x01;
//            SendDataByte(Command.ESC_Align);
//            Command.GS_ExclamationMark[2] = 0x11;
//            SendDataByte(Command.GS_ExclamationMark);
//
//            String qrData = textQr.replace("\n","");
//
//            Command.ESC_Align[2] = 0x01;
//            SendDataByte(Command.ESC_Align);
//
//            byte[] data = PrintPicture.POS_PrintBMP(setQr(qrData), 384, 0);
//            SendDataByte(data);
            SendDataByte(PrinterCommand.POS_Print_Text("Print "+ GlobalHelper.getUser().getUserFullName()+" "+ sdf.format(System.currentTimeMillis()) +"\n", "GBK", 0, 0, 0, 2));
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(58));
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(58));
            SendDataByte(Command.GS_V_m_n);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void printRekonsilasiSPB(TransaksiSPB transaksiSpb, int totalKartuRusak, int totalKartuHilang,ArrayList<Angkut> angkutView){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm");

        int angkutTph = 0;
        int angkutTransit = 0;
        int angkutManual = 0;

        if(angkutView != null) {
            for (int i = 0; i < angkutView.size(); i++) {
                if (angkutView.get(i).getTransaksiPanen() != null) {
                    angkutTph++;
                } else if (angkutView.get(i).getTransaksiAngkut() != null) {
                    angkutTransit++;
                } else {
                    angkutManual++;
                }
            }
        }

        Estate estate = GlobalHelper.getEstateByEstCode(transaksiSpb.getEstCode());
        String format1 = "%-11s";
        String format2 = "%-15s";
        String format3 = "%-9s|";
        String format4 = "%5s|";
        String format5 = "|%2s|";
        String format6 = "%-18s";
        String format7 = "%-15s";
        String format8 = "|%-12s|";
        String format9 = "%6s|";
        String format10 = "%-13s|";
        String format11 = "|%-16s|";
        try {
//            final byte[][] byteCodebar = {
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//            };
//            SendDataByte(byteCodebar[9]);
            // String textQr = transaksiSpb.getIdSPB();

            String noSpb = transaksiSpb.getNoSpb() == null ? TransaksiSpbHelper.converIdSPBToNoSPB(transaksiSpb.getIdSPB()) : transaksiSpb.getNoSpb();

            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x11;
            SendDataByte(Command.GS_ExclamationMark);
            SendDataByte(PrinterCommand.POS_Print_Text(context.getResources().getString(R.string.surat_rekonsilasi) + "\n", "GBK", 0, 1, 1, 30));

            Command.ESC_Align[2] = 0x00;
            SendDataByte(PrinterCommand.POS_Set_Bold(1));
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);
            SendDataByte(PrinterCommand.POS_Print_Text("================================\n", "GBK", 0, 0, 0, 0));

            byte[] barCode = PrintPicture.POS_PrintBMP(setBarcode(noSpb), 384, 0);
            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);
            SendDataByte(barCode);
            SendDataByte("\n".getBytes("GBK"));
            SendDataByte(PrinterCommand.POS_Print_Text(noSpb + "\n", "GBK", 0, 0, 0, 5));
            SendDataByte(PrinterCommand.POS_Print_Text("================================\n", "GBK", 0, 0, 0, 0));

            Command.ESC_Align[2] = 0x00;
            SendDataByte(PrinterCommand.POS_Set_Bold(1));
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);
            SendDataByte(PrinterCommand.POS_Print_Text("================================\n", "GBK", 0, 0, 0, 0));

            String idTAngkut = transaksiSpb.getTransaksiAngkut().getIdTAngkut();
            byte[] barCodeIDDeklarasiAngkut = PrintPicture.POS_PrintBMP(setBarcode(idTAngkut), 384, 0);
            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);
            SendDataByte(barCodeIDDeklarasiAngkut);
            SendDataByte("\n".getBytes("GBK"));
            SendDataByte(PrinterCommand.POS_Print_Text(idTAngkut + "\n", "GBK", 0, 0, 0, 5));
            SendDataByte(PrinterCommand.POS_Print_Text("================================\n", "GBK", 0, 0, 0, 0));

            Command.ESC_Align[2] = 0x00;
            SendDataByte(PrinterCommand.POS_Set_Bold(1));
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);

            //info transaksi
            SendDataByte(PrinterCommand.POS_Print_Text("========="+ context.getResources().getString(R.string.info_transaksi)+"=========\n", "GBK", 0, 0, 0, 0));
            if(estate != null) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " + String.format(format2, estate.getCompanyShortName()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " + String.format(format2, estate.getEstName()) + "\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_estate) + transaksiSpb.getEstCode()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_estate) + transaksiSpb.getEstCode()) + "\n", "GBK", 0, 0, 0, 0));
            }

            User userSelected = null;
            if(this.context instanceof BaseActivity){
                userSelected = GlobalHelper.dataAllUser.get(transaksiSpb.getCreateBy());
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Input Oleh")+" : "+String.format(format2, userSelected != null ? userSelected.getUserFullName() : "-")+"\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Input Oleh")+" : "+String.format(format2, transaksiSpb.getCreateBy())+"\n", "GBK", 0, 0, 0, 0));
            }

            if(transaksiSpb.getSubstitute() != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Krani Penganti")+" : "+String.format(format2, transaksiSpb.getSubstitute().getNama())+"\n", "GBK", 0, 0, 0, 0));
            }
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Waktu Input")+" : "+String.format(format2, sdf.format(transaksiSpb.getCreateDate()))+"\n", "GBK", 0, 0, 0, 0));


            User assAfd = null;
            if(this.context instanceof BaseActivity) {
                assAfd = GlobalHelper.dataAllAsstAfd.get(transaksiSpb.getApproveBy());
            }

            SendDataByte(PrinterCommand.POS_Print_Text("============"+context.getResources().getString(R.string.info_spb)+"============\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Id SPB")+" : "+noSpb+"\n", "GBK", 0, 0, 0, 0));
            if(assAfd != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Approve") + " : " + assAfd.getUserFullName() + "\n", "GBK", 0, 0, 0, 0));
            }else {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Approve") + " : " + transaksiSpb.getApproveBy() + "\n", "GBK", 0, 0, 0, 0));
            }
            String tujuan = transaksiSpb.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS ? TransaksiAngkut.PKS :
                    transaksiSpb.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_KELUAR ? TransaksiAngkut.KELAUR : "-";
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Tujuan")+" : "+ tujuan +"\n", "GBK", 0, 0, 0, 0));

            if(tujuan == TransaksiAngkut.PKS){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Nama PKS")+" : "+ transaksiSpb.getTransaksiAngkut().getPks().getNamaPKS() +"\n", "GBK", 0, 0, 0, 0));
            }

            userSelected = null;
            if(this.context instanceof BaseActivity) {
                userSelected = GlobalHelper.dataAllUser.get(transaksiSpb.getTransaksiAngkut().getCreateBy());
            }
            SendDataByte(PrinterCommand.POS_Print_Text("==========="+ context.getResources().getString(R.string.info_vehicle)+"=========\n", "GBK", 0, 0, 0, 0));
            Operator operator = GlobalHelper.dataOperator.get(transaksiSpb.getTransaksiAngkut().getSupir());
            if(operator != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Supir") + " : " +String.format(format2, operator.getNama())+"\n", "GBK", 0, 0, 0, 0));
            }else {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Supir") + " : " +String.format(format2, transaksiSpb.getTransaksiAngkut().getSupir() == null ? "" : transaksiSpb.getTransaksiAngkut().getSupir())+"\n", "GBK", 0, 0, 0, 0));
            }

            Vehicle vehicle = GlobalHelper.dataVehicle.get(transaksiSpb.getTransaksiAngkut().getVehicle());
            if(vehicle != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Kendaraan") + " : "+String.format(format2, TransaksiAngkutHelper.showVehicle(vehicle))+"\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Kendaraan") + " : "+String.format(format2, TransaksiAngkutHelper.showVehicle(transaksiSpb.getTransaksiAngkut().getVehicle()))+"\n", "GBK", 0, 0, 0, 0));
            }

            if(transaksiSpb.getTransaksiAngkut().getBin() != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Bin") + " : "+String.format(format2, transaksiSpb.getTransaksiAngkut().getBin())+"\n", "GBK", 0, 0, 0, 0));
            }

            if(transaksiSpb.getTransaksiAngkut().getPengirimanGandas() != null) {
                Collections.sort(transaksiSpb.getTransaksiAngkut().getPengirimanGandas(), new Comparator<PengirimanGanda>() {
                    @Override
                    public int compare(PengirimanGanda o1, PengirimanGanda o2) {
                        return o1.getIdx() < o2.getIdx() ? -1 : (o1.getIdx() > o2.getIdx()) ? 1 : 0;
                    }
                });

                SendDataByte(PrinterCommand.POS_Print_Text("======"+ context.getResources().getString(R.string.info_pengiriman_lanjut)+"====\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5,"Ke")+
                        String.format(format9,"Pemilik")+
                        String.format(format3,"Unit")+
                        String.format(format3,"Operator")+"\n", "GBK", 0, 0, 0, 0));
                for (int i = 0; i < transaksiSpb.getTransaksiAngkut().getPengirimanGandas().size(); i++) {
                    PengirimanGanda ganda = transaksiSpb.getTransaksiAngkut().getPengirimanGandas().get(i);
                    int ke = i + 2;
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5, ke)+
                            String.format(format9,ganda.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN ? TransaksiAngkut.KEBUN : TransaksiAngkut.LUAR)+
                            String.format(format3,TransaksiAngkutHelper.showVehicle(ganda.getVehicle()))+
                            String.format(format3,ganda.getOperator().getNama())+"\n", "GBK", 0, 0, 0, 0));
                }
            }

            if(transaksiSpb.getTransaksiAngkut().getLoader() != null){
                SendDataByte(PrinterCommand.POS_Print_Text("==========="+ context.getResources().getString(R.string.info_loader)+"==========\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format8,"Nama")+
                        String.format(format3,"NIK")+
                        String.format(format9,"Estate")+"\n", "GBK", 0, 0, 0, 0));
                for (int i = 0; i < transaksiSpb.getTransaksiAngkut().getLoader().size(); i++) {
                    Operator loader = transaksiSpb.getTransaksiAngkut().getLoader().get(i);
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format8,loader.getNama())+
                            String.format(format3,loader.getNik())+
                            String.format(format9,loader.getArea())+"\n", "GBK", 0, 0, 0, 0));
                }
            }

            SendDataByte(PrinterCommand.POS_Print_Text("============"+ context.getResources().getString(R.string.info_angkut)+"=========\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ Angkut TPH") +" : "+String.format("%,d",angkutTph)+"\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ Angkut Langsiran") +" : "+String.format("%,d",angkutTransit)+"\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ Angkut Manual") +" : "+String.format("%,d",angkutManual)+"\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ Angkut") +" : "+String.format("%,d",transaksiAngkut.getAngkuts().size())+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format7,"Σ Manual Data") +" : "+String.format("%,d", angkutManual)+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format7,"Mulai Angkut") +" : "+sdf.format(transaksiSpb.getTransaksiAngkut().getStartDate())+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format7,"Selesai Angkut") +" : "+sdf.format(transaksiSpb.getTransaksiAngkut().getEndDate())+"\n", "GBK", 0, 0, 0, 0));

//            SendDataByte(PrinterCommand.POS_Print_Text("==========="+ context.getResources().getString(R.string.tbs_angkut)+"===========\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text("============ALASAN==============\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Janjang")+" : "+ String.format(format2,String.format("%,d",transaksiAngkut.getTotalJanjang()) + " Jjg") +"\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Brondolan")+" : "+String.format(format2,String.format("%,d",transaksiAngkut.getTotalBrondolan()) + " Kg")+"\n", "GBK", 0, 0, 0, 0));
//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Berat")+" : "+String.format(format2,String.format("%.0f",transaksiAngkut.getTotalBeratEstimasi()) + " Kg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Kartu Rusak")+" : "+ String.format(format2,String.format("%,d",totalKartuRusak)) +"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Kartu Hilang")+" : "+String.format(format2,String.format("%,d",totalKartuHilang))+"\n", "GBK", 0, 0, 0, 0));


            Collections.sort(angkutView, new Comparator<Angkut>() {
                @Override
                public int compare(Angkut t0, Angkut t1) {
                    String a = t0.getBlock();
                    String b = t1.getBlock();
                    return (a.compareTo(b));
                }
            });

            Angkut subTotalAngkutBlock = null;
            int count = 0;
            int totalBlockKartuRusak = 0;
            int totalBlockKartuHilang = 0;
            boolean awal = true;
            for (int i = 0 ; i < angkutView.size(); i++){
                Angkut angkut = angkutView.get(i);
                if(angkut.getManualSPBData()!=null){
                    count = count + 1;
                    int subKartuHilang=0;
                    int subKartuRusak=0;
                    if(awal){
                        awal = false;
                        SendDataByte(PrinterCommand.POS_Print_Text("=========="+ context.getResources().getString(R.string.detail_angkut)+"=========\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5,"No")+ String.format(format10,"TPH")+
                                String.format(format9,"Rusak")+
                                String.format(format9,"Hilang")+"\n", "GBK", 0, 0, 0, 0));
                    }
                    String nama = "M";
                    String nama1 = "0";
                    String nama2 = "0";
                    String block = "";

                    if(angkut.getTransaksiPanen() != null){
                        nama = "TPH";
                        if(angkut.getTransaksiPanen().getTph() != null){
                            nama = angkut.getTransaksiPanen().getTph().getNamaTph() != null ?
                                    angkut.getTransaksiPanen().getTph().getNamaTph() : "TPH";
                            nama += angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                    "/" +angkut.getTransaksiPanen().getTph().getBlock() : "-";
                            block = angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                    angkut.getTransaksiPanen().getTph().getBlock() : "-";
                        }
                    }else if (angkut.getTph() != null) {
                        nama = angkut.getTph().getNamaTph() != null ?
                                angkut.getTph().getNamaTph() : "TPH";
                        nama += angkut.getTph().getBlock() != null ?
                                "/" +angkut.getTph().getBlock() : "-";
                        block = angkut.getTph().getBlock() != null ?
                                angkut.getTph().getBlock() : "-";
//                    nama += " M";
                    }else if (angkut.getTransaksiAngkut() != null){
                        nama = "L";
                        if(angkut.getTransaksiAngkut().getLangsiran() != null){
//                            nama = angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() != null ?
//                                    angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() : "Langsiran";
//                            nama += angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() != null ?
//                                    "/"+angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() : "-";
                            block = angkut.getTransaksiAngkut().getLangsiran().getBlock() != null ?
                                    angkut.getTransaksiAngkut().getLangsiran().getBlock() : "-";
                        }
                    }else if (angkut.getLangsiran() != null) {
//                        nama = angkut.getLangsiran().getNamaTujuan() != null ?
//                                angkut.getLangsiran().getNamaTujuan(): "Langsiran";
                        nama = "L";
                        nama += angkut.getLangsiran().getBlock() != null ?
                                "/" +angkut.getLangsiran().getBlock(): "-";
                        block = angkut.getLangsiran().getBlock() != null ?
                                angkut.getLangsiran().getBlock(): "-";
//                    nama += " M";
                    }


//                if(subTotalAngkutBlock == null){
//                    subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
//                            angkut.getBrondolan(),
//                            block,
//                            angkut.getBerat());
//                }else if(subTotalAngkutBlock.getBlock().equals(block)){
//                    subTotalAngkutBlock.setJanjang(subTotalAngkutBlock.getJanjang() + angkut.getJanjang());
//                    subTotalAngkutBlock.setBrondolan(subTotalAngkutBlock.getBrondolan() + angkut.getBrondolan());
//                    subTotalAngkutBlock.setBerat(subTotalAngkutBlock.getBerat() + angkut.getBerat());
//                }else if(!subTotalAngkutBlock.getBlock().equals(block)){
//                    SendDataByte("================================\n".getBytes("GBK"));
//                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format8,"Block "+subTotalAngkutBlock.getBlock())+
//                            String.format(format4,subTotalAngkutBlock.getJanjang())+
//                            String.format(format4,subTotalAngkutBlock.getBrondolan())+
//                            String.format(format4,String.format("%.0f",subTotalAngkutBlock.getBerat()))+"\n", "GBK", 0, 0, 0, 0));
//                    SendDataByte("================================\n".getBytes("GBK"));
//                    subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
//                            angkut.getBrondolan(),
//                            block,
//                            angkut.getBerat());
//                    count = 1;
//                }

                    if(subTotalAngkutBlock == null){
                        subTotalAngkutBlock = angkut;
                        if(angkut.getManualSPBData()!=null){
                            if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                                totalBlockKartuRusak++;
                            }else{
                                totalBlockKartuHilang++;
                            }
                        }
                    }else if(subTotalAngkutBlock.getBlock().equals(block)){
                        if(angkut.getManualSPBData()!=null){
                            if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                                totalBlockKartuRusak++;
                            }else{
                                totalBlockKartuHilang++;
                            }
                        }
                    }else if(!subTotalAngkutBlock.getBlock().equals(block)){
//                    arrayList.add(new ModelRecyclerView("subTotal","Block "+subTotalAngkutBlock.getBlock() ,
//                            String.format("%,d", totalBlockKartuRusak),
//                            String.format("%,d", totalBlockKartuHilang)));
//                    Log.i("getManualSPBData", "totalBlockKartuRusak: "+totalBlockKartuRusak+" totalBlockKartuHilang: "+totalBlockKartuHilang);
                        SendDataByte("================================\n".getBytes("GBK"));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format11,"Block "+subTotalAngkutBlock.getBlock())+
                                String.format(format9,totalBlockKartuRusak)+
                                String.format(format9,totalBlockKartuHilang)+"\n", "GBK", 0, 0, 0, 0));
                        SendDataByte("================================\n".getBytes("GBK"));
                        totalBlockKartuRusak = 0;
                        totalBlockKartuHilang = 0;
                        subTotalAngkutBlock = angkut;
                        if(subTotalAngkutBlock.getManualSPBData().getTipe().equals("Kartu Rusak")){
                            totalBlockKartuRusak++;
                        }else{
                            totalBlockKartuHilang++;
                        }
                        count = 1;
                    }

                    if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                        subKartuRusak = 1;
                        subKartuHilang = 0;
                    }else{
                        subKartuRusak = 0;
                        subKartuHilang = 1;
                    }


//                nama1 = String.format("%,d",angkut.getJanjang());
//                nama2 = String.format("%,d",angkut.getBrondolan());
//                nama3 = String.format("%.0f",angkut.getBerat());
//                if(angkut.getManualSPBData().getTipeKongsi().equals("Kartu Rusak")){
//                    nama1 = String.format("%,d", 1);
//                    nama2 = String.format("%,d", 0);
//                }else{
//                    nama1 = String.format("%,d", 0);
//                    nama2 = String.format("%,d", 1);
//                }
                    nama1 = String.format("%,d",subKartuRusak) ;
                    nama2 = String.format("%,d",subKartuHilang) ;

                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5, count) +String.format(format10,nama)+
                            String.format(format9,nama1)+
                            String.format(format9,nama2)+"\n", "GBK", 0, 0, 0, 0));
                    if(i == transaksiSpb.getTransaksiAngkut().getAngkuts().size()-1){
                        SendDataByte("================================\n".getBytes("GBK"));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format11,"Block "+subTotalAngkutBlock.getBlock())+
                                String.format(format9,totalBlockKartuRusak)+
                                String.format(format9,totalBlockKartuHilang)+"\n", "GBK", 0, 0, 0, 0));
                        SendDataByte("================================\n".getBytes("GBK"));
                        Log.i("getManualSPBData", "totalBlockKartuRusak: "+totalBlockKartuRusak+" totalBlockKartuHilang: "+totalBlockKartuHilang);
                    }
                }
            }

//            String text = textQr.replace("\n","");
//            byte[] qrcode = PrintPicture.POS_PrintBMP(setQr(text), 384, 0);
//            Command.ESC_Align[2] = 0x01;
//            SendDataByte(Command.ESC_Align);
//            SendDataByte(qrcode);

            SendDataByte("================================\n".getBytes("GBK"));
            Command.ESC_Align[2] = 0x00;
            SendDataByte(PrinterCommand.POS_Set_Bold(1));
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);

            SendDataByte(PrinterCommand.POS_Print_Text("Print "+ GlobalHelper.getUser().getUserFullName()+" "+ sdf.format(System.currentTimeMillis()) +"\n", "GBK", 0, 0, 0, 2));
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(58));
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(58));
            SendDataByte(Command.GS_V_m_n);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void printTransaksiSpb(TransaksiSPB transaksiSpb, ArrayList<Angkut> angkutView, boolean copy,DynamicParameterPenghasilan dynamicParameterPenghasilan) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm");

        ISCCInformation isccInformation = null;
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ISCCINFORMATION);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            ISCCInformation isccInformationX = gson.fromJson(dataNitrit.getValueDataNitrit(),ISCCInformation.class);
            if(isccInformationX.getEstCode().equals(transaksiSpb.getEstCode()) && ((new Double(isccInformationX.getValidFrom())).longValue() <= transaksiSpb.getCreateDate() && (new Double(isccInformationX.getValidUntil())).longValue() >= transaksiSpb.getCreateDate())){
                isccInformation = isccInformationX;
                break;
            }
        }
        db.close();

        int angkutTph = 0;
        int angkutTransit = 0;
        int angkutKartuHilang = 0;
        int angkutKartuRusak = 0;
        int angkutMaxRestan = 0;

        InfoRestan infoRestan = new InfoRestan();

        if(angkutView != null) {
            for (int i = 0; i < angkutView.size(); i++) {
                Angkut angkut = angkutView.get(i);
                if (angkut.getManualSPBData() != null){
                    if(angkut.getManualSPBData().getTipeId() == ManualSPB.TYPE_KARTU_HILANG){
                        angkutKartuHilang++;
                    }else if (angkut.getManualSPBData().getTipeId() == ManualSPB.TYPE_KARTU_RUSAK){
                        angkutKartuRusak++;
                    }else if(angkut.getManualSPBData().getTipeId() == ManualSPB.TYPE_KARTU_MAX_RESTAN){
                        angkutMaxRestan++;
                    }
                }else if (angkut.getTransaksiPanen() != null) {
                    if(angkut.getTransaksiPanen().getCreateDate() > 0) {
                        Integer selisih = GlobalHelper.getDateDiff(transaksiSpb.getCreateDate(),angkut.getTransaksiPanen().getCreateDate());
//                        Integer selisih = Integer.parseInt(String.valueOf(
//                                Math.abs(TimeUnit.MILLISECONDS.toDays(
//                                        transaksiSpb.getCreateDate() - angkut.getTransaksiPanen().getCreateDate()
//                                ))
//                        ));

                        if (selisih >= 2) {
                            infoRestan.setJjgH2(infoRestan.getJjgH2() + angkut.getJanjang());
                            if (selisih > infoRestan.getLamaRestan()) {
                                infoRestan.setLamaRestan(selisih);
                            }
                        }
                    }
                    angkutTph++;
                } else if (angkut.getTransaksiAngkut() != null) {
                    angkutTransit++;
                }
            }
        }

        Estate estate = GlobalHelper.getEstateByEstCode(transaksiSpb.getEstCode());

        String afdeling = null;
        SPK spk = null;

        if(transaksiSpb.getTransaksiAngkut().getSpk() != null){
            spk = transaksiSpb.getTransaksiAngkut().getSpk();
        }

//        if(transaksiSpb.getAfdeling() != null){
//            if(transaksiSpb.getAfdeling().size() > 0){
//                for(String s : transaksiSpb.getAfdeling()){
//                    if(afdeling == null){
//                        afdeling = s ;
//                    }else{
//                        afdeling += "," + s;
//                    }
//                }
//            }
//        }

        String format1 = "%-11s";
        String format2 = "%-15s";
        String format3 = "%-9s|";
        String format4 = "%5s|";
        String format5 = "|%2s|";
        String format6 = "%-18s";
        String format7 = "%-15s";
        String format8 = "|%-12s|";
        String format9 = "%-7s|";
        String format10 = "|%-30s|";

        try {
//            final byte[][] byteCodebar = {
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//            };
//            SendDataByte(byteCodebar[9]);
            // String textQr = transaksiSpb.getIdSPB();

            String noSpb = transaksiSpb.getNoSpb() == null ? TransaksiSpbHelper.converIdSPBToNoSPB(transaksiSpb.getIdSPB()) : transaksiSpb.getNoSpb();

            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x11;
            SendDataByte(Command.GS_ExclamationMark);
            SendDataByte(PrinterCommand.POS_Print_Text("Surat\n", "GBK", 0, 1, 1, 35));
            SendDataByte(PrinterCommand.POS_Print_Text("Pengangkutan\n", "GBK", 0, 1, 1, 35));
            SendDataByte(PrinterCommand.POS_Print_Text("Buah\n", "GBK", 0, 1, 1, 35));

            if(transaksiSpb.getTransaksiAngkut().isRevisi()){
                SendDataByte(PrinterCommand.POS_Print_Text("-------------------------------\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text("R E V I S I O N    C O P Y\n", "GBK", 0, 1, 1, 35));
                SendDataByte(PrinterCommand.POS_Print_Text("-------------------------------\n", "GBK", 0, 0, 0, 0));
            }else if(copy) {
                SendDataByte(PrinterCommand.POS_Print_Text("-------------------------------\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text("C O P Y\n", "GBK", 0, 1, 1, 35));
                SendDataByte(PrinterCommand.POS_Print_Text("-------------------------------\n", "GBK", 0, 0, 0, 0));
            }

            Command.ESC_Align[2] = 0x00;
            SendDataByte(PrinterCommand.POS_Set_Bold(1));
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);
            SendDataByte(PrinterCommand.POS_Print_Text("================================\n", "GBK", 0, 0, 0, 0));


            byte[] barCode = PrintPicture.POS_PrintBMP(setBarcode(noSpb), 384, 0);
            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);
            SendDataByte(barCode);
            SendDataByte("\n".getBytes("GBK"));
            SendDataByte(PrinterCommand.POS_Print_Text(noSpb + "\n", "GBK", 0, 0, 0, 5));
            SendDataByte(PrinterCommand.POS_Print_Text("================================\n", "GBK", 0, 0, 0, 0));

            if(isccInformation != null) {
                SendDataByte(PrinterCommand.POS_Print_Text("FFB SUSTAINABLE ISCC " + isccInformation.getEstNewCode()+ "\n", "GBK", 0, 0, 0, 5));
                SendDataByte(PrinterCommand.POS_Print_Text(isccInformation.getCertificateNumber()+ "\n", "GBK", 0, 0, 0, 5));
                SendDataByte(PrinterCommand.POS_Print_Text("GHG Value Eec : "+isccInformation.getGhgValue()+" "+isccInformation.getGhgSatuan()+ "\n", "GBK", 0, 0, 0, 5));
                SendDataByte(PrinterCommand.POS_Print_Text("SC Model : "+isccInformation.getCertificateType()+ "\n", "GBK", 0, 0, 0, 5));
                SendDataByte(PrinterCommand.POS_Print_Text("ISCC Compliant\n", "GBK", 0, 0, 0, 5));
                double pksDistance = TransaksiSpbHelper.cekLandDistance(transaksiSpb.getTransaksiAngkut());
                if(pksDistance > 0) {
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format("Jarak Ke PKS : %.1f KM", pksDistance) + "\n", "GBK", 0, 0, 0, 5));
                }
                SendDataByte(PrinterCommand.POS_Print_Text("================================\n", "GBK", 0, 0, 0, 5));
            }

            if(transaksiSpb.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS) {
                String text = TransaksiSpbHelper.toQrSpb(transaksiSpb).replace("\n", "");
                afdeling = TransaksiSpbHelper.getAfdelingFromQr(text);
                byte[] qrcode = PrintPicture.POS_PrintBMP(setQr(text), 384, 0);
                Command.ESC_Align[2] = 0x01;
                SendDataByte(Command.ESC_Align);
                SendDataByte(qrcode);
            }

            Command.ESC_Align[2] = 0x00;
            SendDataByte(PrinterCommand.POS_Set_Bold(1));
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);

            SendDataByte(PrinterCommand.POS_Print_Text("========="+ context.getResources().getString(R.string.info_transaksi)+"=========\n", "GBK", 0, 0, 0, 0));
            if(estate != null) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " + String.format(format2, estate.getCompanyShortName()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " + String.format(format2, estate.getEstName()) + "\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_estate) + transaksiSpb.getEstCode()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_estate) + transaksiSpb.getEstCode()) + "\n", "GBK", 0, 0, 0, 0));
            }

            if(afdeling != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Afdeling") + " : " + String.format(format2, afdeling) + "\n", "GBK", 0, 0, 0, 0));
            }

            if(spk != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "OA") + " : " + String.format(format2, TransaksiPanenHelper.showSPK(spk)) + "\n", "GBK", 0, 0, 0, 0));
            }

            User userSelected = null;
            if(this.context instanceof BaseActivity){
                userSelected = GlobalHelper.dataAllUser.get(transaksiSpb.getCreateBy());
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Input Oleh")+" : "+String.format(format2, userSelected != null ? userSelected.getUserFullName() : "-")+"\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Input Oleh")+" : "+String.format(format2, transaksiSpb.getCreateBy())+"\n", "GBK", 0, 0, 0, 0));
            }

            if(transaksiSpb.getSubstitute() != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Krani Penganti")+" : "+String.format(format2, transaksiSpb.getSubstitute().getNama())+"\n", "GBK", 0, 0, 0, 0));
            }

            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Waktu Input")+" : "+String.format(format2, sdf.format(transaksiSpb.getCreateDate()))+"\n", "GBK", 0, 0, 0, 0));

            int langsiranCount = 0;
            if(transaksiSpb.getTransaksiAngkut().getPengirimanGandas() != null) {
                Collections.sort(transaksiSpb.getTransaksiAngkut().getPengirimanGandas(), new Comparator<PengirimanGanda>() {
                    @Override
                    public int compare(PengirimanGanda o1, PengirimanGanda o2) {
                        return o1.getIdx() < o2.getIdx() ? -1 : (o1.getIdx() > o2.getIdx()) ? 1 : 0;
                    }
                });

                SendDataByte(PrinterCommand.POS_Print_Text("======="+ context.getResources().getString(R.string.info_pengiriman_lanjut)+"======\n", "GBK", 0, 0, 0, 0));
//                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5,"Ke")+
//                        String.format(format9,"Pemilik")+
//                        String.format(format3,"Unit")+
//                        String.format(format3,"Operator")+"\n", "GBK", 0, 0, 0, 0));
                for (int i = 0; i < transaksiSpb.getTransaksiAngkut().getPengirimanGandas().size(); i++) {
                    PengirimanGanda ganda = transaksiSpb.getTransaksiAngkut().getPengirimanGandas().get(i);
                    int ke = i + 1;
                    String awal = String.valueOf(ke);
                    if(ganda.getLangsiran() != null){
                        if(ganda.getLangsiran().getIdTujuan() != null){
                            langsiranCount++;
                            awal += " - "+ ganda.getLangsiran().getNamaTujuan() + "("+ ganda.getLangsiran().getBlock() +")";
//                            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Langsiran") + " : " +String.format(format2, ganda.getLangsiran().getNamaTujuan() + "("+ ganda.getLangsiran().getBlock() +")")+"\n", "GBK", 0, 0, 0, 0));
                        }
                    }

                    String kendaraanShowGanda = TransaksiAngkutHelper.showVehicle(ganda.getVehicle());
                    kendaraanShowGanda +=  ganda.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN ? "(" +TransaksiAngkut.KENDARAAN_CODE_KEBUN + ")" : "(" +TransaksiAngkut.KENDARAAN_CODE_LUAR + ")";
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Ke") + " : " +String.format(format2, awal)+"\n", "GBK", 0, 0, 0, 0));
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Kendaraan") + " : " +String.format(format2, kendaraanShowGanda)+"\n", "GBK", 0, 0, 0, 0));
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Supir") + " : " +String.format(format2, ganda.getOperator().getNama())+"\n", "GBK", 0, 0, 0, 0));

//                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5,String.valueOf(ke))+
//                            String.format(format9,ganda.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN ? TransaksiAngkut.KEBUN : TransaksiAngkut.LUAR)+
//                            String.format(format3,TransaksiAngkutHelper.showVehicle(ganda.getVehicle()))+
//                            String.format(format3,ganda.getOperator().getNama())+"\n", "GBK", 0, 0, 0, 0));
                    if(ganda.getSpk() != null){
                        if (ganda.getSpk().getIdSPK() != null){
                            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "SPK") + " : " +String.format(format2, ganda.getSpk().getIdSPK())+"\n", "GBK", 0, 0, 0, 0));
//                            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5,"")+
//                                    String.format(format9,"SPK")+
//                                    String.format(format3,ganda.getSpk().getIdSPK())+
//                                    String.format(format3,"")+"\n", "GBK", 0, 0, 0, 0));
                        }
                    }
                    SendDataByte(PrinterCommand.POS_Print_Text("--------------------------------\n", "GBK", 0, 0, 0, 5));

                }
            }

            User assAfd = null;
            if(this.context instanceof BaseActivity) {
                assAfd = GlobalHelper.dataAllAsstAfd.get(transaksiSpb.getApproveBy());
            }

            SendDataByte(PrinterCommand.POS_Print_Text("============"+context.getResources().getString(R.string.info_spb)+"============\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Id SPB")+" : "+noSpb+"\n", "GBK", 0, 0, 0, 0));
            if(assAfd != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Approve") + " : " + assAfd.getUserFullName() + "\n", "GBK", 0, 0, 0, 0));
            }else {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Approve") + " : " + transaksiSpb.getApproveBy() + "\n", "GBK", 0, 0, 0, 0));
            }
            String tujuan = transaksiSpb.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS ? TransaksiAngkut.PKS :
                    transaksiSpb.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_KELUAR ? TransaksiAngkut.KELAUR : "-";
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Tujuan")+" : "+ tujuan +"\n", "GBK", 0, 0, 0, 0));

            if(tujuan == TransaksiAngkut.PKS){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Nama PKS   ")+" : "+ transaksiSpb.getTransaksiAngkut().getPks().getNamaPKS() +"\n", "GBK", 0, 0, 0, 0));
            }

            userSelected = null;
            if(this.context instanceof BaseActivity) {
                userSelected = GlobalHelper.dataAllUser.get(transaksiSpb.getTransaksiAngkut().getCreateBy());
            }
            String kendaraanShow = TransaksiAngkutHelper.showVehicle(transaksiSpb.getTransaksiAngkut().getVehicle());
            kendaraanShow +=  transaksiSpb.getTransaksiAngkut().getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN ? "(" +TransaksiAngkut.KENDARAAN_CODE_KEBUN + ")" : "(" +TransaksiAngkut.KENDARAAN_CODE_LUAR + ")";

            SendDataByte(PrinterCommand.POS_Print_Text("==========="+ context.getResources().getString(R.string.info_vehicle)+"=========\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Supir") + " : " +String.format(format2, transaksiSpb.getTransaksiAngkut().getSupir().getNama())+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Kendaraan") + " : "+String.format(format2,kendaraanShow )+"\n", "GBK", 0, 0, 0, 0));
            if(transaksiSpb.getTransaksiAngkut().getVehicle().getVraOrderNumber() != null) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Vehicle No") + " : " + String.format(format2, transaksiSpb.getTransaksiAngkut().getVehicle().getVraOrderNumber()) + "\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Vehicle No") + " : " + String.format(format2, "-") + "\n", "GBK", 0, 0, 0, 0));
            }

            if (transaksiSpb.getTransaksiAngkut().getCages() != null) {
                if (!transaksiSpb.getTransaksiAngkut().getCages().isEmpty()) {
                    int count = 1;
                    for (int i = 0; i < transaksiSpb.getTransaksiAngkut().getCages().size(); i++) {
                        Cages cages = transaksiSpb.getTransaksiAngkut().getCages().get(i);
                        SendDataByte(PrinterCommand.POS_Print_Text(
                                String.format("Cages No. %d : %s\n", count, cages.getAssetName()), "GBK", 0, 0, 0, 0));
                        count++;
                    }
                }
            }




            if(transaksiSpb.getTransaksiAngkut().getBin() != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Bin") + " : "+String.format(format2, transaksiSpb.getTransaksiAngkut().getBin())+"\n", "GBK", 0, 0, 0, 0));
            }




            if(transaksiSpb.getTransaksiAngkut().getLoader() != null){
                if(transaksiSpb.getTransaksiAngkut().getLoader().size() > 0) {
                    SendDataByte(PrinterCommand.POS_Print_Text("===========" + context.getResources().getString(R.string.info_loader) + "==========\n", "GBK", 0, 0, 0, 0));
                    int count = 1;
                    for (int i = 0; i < transaksiSpb.getTransaksiAngkut().getLoader().size(); i++) {
                        Operator loader = transaksiSpb.getTransaksiAngkut().getLoader().get(i);

                        SendDataByte(PrinterCommand.POS_Print_Text(String.format("%,d . ",count)+ loader.getNama() + " ("+ loader.getNik() + ")\n", "GBK", 0, 0, 0, 5));
                        count++;
                    }
                }
            }

            SendDataByte(PrinterCommand.POS_Print_Text("============"+ context.getResources().getString(R.string.info_angkut)+"=========\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ TPH") +" : "+String.format("%,d",angkutTph)+"\n", "GBK", 0, 0, 0, 0));
            if(langsiranCount > 0) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6, "Σ Langsiran") + " : " + String.format("%,d", langsiranCount) + "\n", "GBK", 0, 0, 0, 0));
            }
            if(angkutKartuHilang != 0 ){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ Kartu Hilang") +" : "+String.format("%,d",angkutKartuHilang)+"\n", "GBK", 0, 0, 0, 0));
            }
            if(angkutKartuRusak != 0 ){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ Kartu Rusak") +" : "+String.format("%,d",angkutKartuRusak)+"\n", "GBK", 0, 0, 0, 0));
            }
            if(angkutMaxRestan != 0 ){
                int restanDay = GlobalHelper.MAX_RESTAN;
                if (dynamicParameterPenghasilan != null){
                    restanDay = dynamicParameterPenghasilan.getMaxRestan();
                }
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ Kartu Restan >"+restanDay+"H") +" : "+String.format("%,d",angkutMaxRestan)+"\n", "GBK", 0, 0, 0, 0));
            }
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ OPH") +" : "+String.format("%,d",transaksiSpb.getTransaksiAngkut().getAngkuts().size())+"\n", "GBK", 0, 0, 0, 0));

            SendDataByte(PrinterCommand.POS_Print_Text("==========="+ context.getResources().getString(R.string.tbs_angkut)+"===========\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Janjang")+" : "+ String.format(format2,String.format("%,d",transaksiSpb.getTotalJanjang()) + " Jjg") +"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Brondolan")+" : "+String.format(format2,String.format("%,d",transaksiSpb.getTotalBrondolan()) + " Kg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Berat")+" : "+String.format(format2,String.format("%.0f",transaksiSpb.getTransaksiAngkut().getTotalBeratEstimasi()) +  " Kg")+"\n", "GBK", 0, 0, 0, 0));

            if(infoRestan.getJjgH2() > 0) {
                int persen  = (int) Math.round((Double.parseDouble(String.valueOf(infoRestan.getJjgH2())) / Double.parseDouble(String.valueOf(transaksiSpb.getTotalJanjang())) ) * 100);
                SendDataByte(PrinterCommand.POS_Print_Text("========" + context.getResources().getString(R.string.info_lama_restan) + "========\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, ">= 2Hari") + " : " + String.format(format2,String.valueOf(infoRestan.getJjgH2()) + "Jjg ("+String.valueOf(persen)+"%)") + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Paling Lama") + " : " + String.format(format2,String.valueOf(infoRestan.getLamaRestan()) +" Hari") + "\n", "GBK", 0, 0, 0, 0));
            }
            Collections.sort(angkutView, new Comparator<Angkut>() {
                @Override
                public int compare(Angkut t0, Angkut t1) {
                    String a = t0.getBlock();
                    String b = t1.getBlock();
                    return (a.compareTo(b));
                }
            });

            Angkut subTotalAngkutBlock = null;
            int count = 1;
            for(int i = 0; i < angkutView.size(); i++) {
                Angkut angkut = angkutView.get(i);
                if(i == 0 ){
                    SendDataByte(PrinterCommand.POS_Print_Text("=========="+ context.getResources().getString(R.string.detail_angkut)+"=========\n", "GBK", 0, 0, 0, 0));
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5,"No")+ String.format(format3,"Blok")+
                            String.format(format4,"Jjg")+
                            String.format(format4,"Brdl")+
                            String.format(format4,"Berat")+"\n", "GBK", 0, 0, 0, 0));
                }

                String nama = "M";
                String nama1 = "0";
                String nama2 = "0";
                String nama3 = "0";
                String block = angkut.getBlock() == null ? "" : angkut.getBlock();
                if(angkut.getTransaksiPanen() != null){
                    nama = "TPH";
                    if(angkut.getTransaksiPanen().getTph() != null){
                        nama = angkut.getTransaksiPanen().getTph().getNamaTph() != null ?
                                angkut.getTransaksiPanen().getTph().getNamaTph() : "TPH";
                        nama += angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                "/" +angkut.getTransaksiPanen().getTph().getBlock() : "-";
                        block = angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                angkut.getTransaksiPanen().getTph().getBlock() : "-";
                    }
                }else if (angkut.getTph() != null) {
                    nama = angkut.getTph().getNamaTph() != null ?
                            angkut.getTph().getNamaTph() : "TPH";
                    nama += angkut.getTph().getBlock() != null ?
                            "/" +angkut.getTph().getBlock() : "-";
                    block = angkut.getTph().getBlock() != null ?
                            angkut.getTph().getBlock() : "-";
                    nama += " M";
                }else if (angkut.getTransaksiAngkut() != null){
                    if(angkut.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS){
                        nama = "PKS";
                    }else {
                        //harusnya nga ada lagi yang ke kondisi ini
                        if (angkut.getTransaksiAngkut().getLangsiran() == null) {
                            nama = "L";
                        } else {
//                            nama = angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() != null ?
//                                    angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() : "Langsiran";
                            nama = "L";
                            nama += angkut.getTransaksiAngkut().getLangsiran().getBlock() != null ?
                                    "/"+angkut.getTransaksiAngkut().getLangsiran().getBlock() : "-";
                            block = angkut.getBlock() != null ?
                                    angkut.getBlock() : "-";
//                            nama += " L";
                        }
                    }
                }else if (angkut.getLangsiran() != null) {
//                    nama = angkut.getLangsiran().getNamaTujuan() != null ?
//                            angkut.getLangsiran().getNamaTujuan(): "L";
                    nama = "L";
                    nama += angkut.getLangsiran().getBlock() != null ?
                            "/" +angkut.getLangsiran().getBlock(): "-";
                    block = angkut.getLangsiran().getBlock() != null ?
                            angkut.getLangsiran().getBlock(): "-";
                    nama += " M";
                }

                if(subTotalAngkutBlock == null){
                    subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
                            angkut.getBrondolan(),
                            block,
                            angkut.getBerat());
                }else if(subTotalAngkutBlock.getBlock().equals(block)){
                    subTotalAngkutBlock.setJanjang(subTotalAngkutBlock.getJanjang() + angkut.getJanjang());
                    subTotalAngkutBlock.setBrondolan(subTotalAngkutBlock.getBrondolan() + angkut.getBrondolan());
                    subTotalAngkutBlock.setBerat(subTotalAngkutBlock.getBerat() + angkut.getBerat());
                }else if(!subTotalAngkutBlock.getBlock().equals(block)){
//                    SendDataByte("================================\n".getBytes("GBK"));
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5, count) +
                            String.format(format3,subTotalAngkutBlock.getBlock())+
                            String.format(format4,subTotalAngkutBlock.getJanjang())+
                            String.format(format4,subTotalAngkutBlock.getBrondolan())+
                            String.format(format4, String.format("%.0f",subTotalAngkutBlock.getBerat()))+"\n", "GBK", 0, 0, 0, 0));
//
//                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format8,String.valueOf(count) +". Block "+subTotalAngkutBlock.getBlock())+
//                             SendDataByte("================================\n".getBytes("GBK"));
                    subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
                            angkut.getBrondolan(),
                            block,
                            angkut.getBerat());

                    count++;
                }

//                nama1 = String.format("%,d",angkut.getJanjang());
//                nama2 = String.format("%,d",angkut.getBrondolan());
//                nama3 = String.format("%.0f",angkut.getBerat());
//                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5,String.valueOf(count)) +String.format(format3,nama)+
//                        String.format(format4,nama1)+
//                        String.format(format4,nama2)+
//                        String.format(format4,nama3)+"\n", "GBK", 0, 0, 0, 0));
//                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format10,String.valueOf(angkut.getOph()))+"\n", "GBK", 0, 0, 0, 0));
            }

//            SendDataByte("================================\n".getBytes("GBK"));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5, count) +
                    String.format(format3,subTotalAngkutBlock.getBlock())+
                    String.format(format4,subTotalAngkutBlock.getJanjang())+
                    String.format(format4,subTotalAngkutBlock.getBrondolan())+
                    String.format(format4, String.format("%.0f",subTotalAngkutBlock.getBerat()))+"\n", "GBK", 0, 0, 0, 0));

//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format8,String.valueOf(count) +". Block "+subTotalAngkutBlock.getBlock())+
//                    String.format(format4,subTotalAngkutBlock.getJanjang())+
//                    String.format(format4,subTotalAngkutBlock.getBrondolan())+
//                    String.format(format4,String.format("%.0f",subTotalAngkutBlock.getBerat()))+"\n", "GBK", 0, 0, 0, 0));

//            SendDataByte("================================\n".getBytes("GBK"));

            String sharePrefQr = sharedPrefHelper.getName(SharedPrefHelper.KEY_DoubleQrSPB);
            int positionQr = QrDoubleSPBHelpper.DoubleQr;
            if(sharePrefQr != null){
                positionQr = Integer.parseInt(sharePrefQr);
            }

            if(positionQr == QrDoubleSPBHelpper.DoubleQr) {
                if (transaksiSpb.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS) {
                    String text = TransaksiSpbHelper.toQrSpb(transaksiSpb).replace("\n", "");
                    byte[] qrcode = PrintPicture.POS_PrintBMP(setQr(text), 384, 0);
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    SendDataByte(qrcode);
                }
            }

//            String qRDetailAngkut = TransaksiSpbHelper.toQRDetailAngkut(transaksiSpb);
//            if(qRDetailAngkut != null) {
//                SendDataByte(PrinterCommand.POS_Print_Text("=========="+ context.getResources().getString(R.string.detail_angkut)+"=========\n", "GBK", 0, 0, 0, 0));
//                byte[] qrcode = PrintPicture.POS_PrintBMP(setQr(qRDetailAngkut), 384, 0);
//                Command.ESC_Align[2] = 0x01;
//                SendDataByte(Command.ESC_Align);
//                SendDataByte(qrcode);
//            }
            Command.ESC_Align[2] = 0x00;
            SendDataByte(PrinterCommand.POS_Set_Bold(1));
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);

            SendDataByte("================================\n".getBytes("GBK"));
            SendDataByte(PrinterCommand.POS_Print_Text("Print "+ GlobalHelper.getUser().getUserFullName()+" "+ sdf.format(System.currentTimeMillis()) +"\n", "GBK", 0, 0, 0, 2));
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(58));
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(58));
            SendDataByte(Command.GS_V_m_n);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void printTransaksiSpbMekanisasi(TransaksiSPB transaksiSpb, ArrayList<TransaksiPanen> angkutView,ArrayList<Angkut> angkutViewKartu, boolean copy,DynamicParameterPenghasilan dynamicParameterPenghasilan) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm");

        ISCCInformation isccInformation = null;
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ISCCINFORMATION);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            ISCCInformation isccInformationX = gson.fromJson(dataNitrit.getValueDataNitrit(),ISCCInformation.class);
            if(isccInformationX.getEstCode().equals(transaksiSpb.getEstCode()) && ((new Double(isccInformationX.getValidFrom())).longValue() <= transaksiSpb.getCreateDate() && (new Double(isccInformationX.getValidUntil())).longValue() >= transaksiSpb.getCreateDate())){
                isccInformation = isccInformationX;
                break;
            }
        }
        db.close();

        int angkutTph = 0;
        int angkutTransit = 0;
        int angkutKartuHilang = 0;
        int angkutKartuRusak = 0;
        int angkutMaxRestan = 0;

        InfoRestan infoRestan = new InfoRestan();

        ArrayList<SubTotalAngkutMekanisasi> subTotalAngkutMekanisasis = new ArrayList<>();

        if(angkutView != null) {
            for (int i = 0; i < angkutView.size(); i++) {
                TransaksiPanen panen = angkutView.get(i);

                SubTotalAngkutMekanisasi subTotalAngkutMekanisasi = new SubTotalAngkutMekanisasi(panen.getIdTPanen(),panen,null);
                subTotalAngkutMekanisasis.add(subTotalAngkutMekanisasi);

                if (panen != null) {
                    if(panen.getCreateDate() > 0) {
                        Integer selisih = GlobalHelper.getDateDiff(transaksiSpb.getCreateDate(),panen.getCreateDate());
//                        Integer selisih = Integer.parseInt(String.valueOf(
//                                Math.abs(TimeUnit.MILLISECONDS.toDays(
//                                        transaksiSpb.getCreateDate() - angkut.getTransaksiPanen().getCreateDate()
//                                ))
//                        ));

                        if (selisih >= 2) {
                            infoRestan.setJjgH2(infoRestan.getJjgH2() + panen.getHasilPanen().getTotalJanjang());
                            if (selisih > infoRestan.getLamaRestan()) {
                                infoRestan.setLamaRestan(selisih);
                            }
                        }
                    }
                    angkutTph++;
                }
            }
        }

        if(angkutViewKartu != null) {
            for (int i = 0; i < angkutViewKartu.size(); i++) {
                Angkut angkut = angkutViewKartu.get(i);

                SubTotalAngkutMekanisasi subTotalAngkutMekanisasi = new SubTotalAngkutMekanisasi(angkut.getIdAngkut(),null,angkut);
                subTotalAngkutMekanisasis.add(subTotalAngkutMekanisasi);

                if (angkut.getManualSPBData() != null){
                    if(angkut.getManualSPBData().getTipeId() == ManualSPB.TYPE_KARTU_HILANG){
                        angkutKartuHilang++;
                    }else if (angkut.getManualSPBData().getTipeId() == ManualSPB.TYPE_KARTU_RUSAK){
                        angkutKartuRusak++;
                    }else if(angkut.getManualSPBData().getTipeId() == ManualSPB.TYPE_KARTU_MAX_RESTAN){
                        angkutMaxRestan++;
                    }
                }else if (angkut.getTransaksiPanen() != null) {
                    if(angkut.getTransaksiPanen().getCreateDate() > 0) {
                        Integer selisih = GlobalHelper.getDateDiff(transaksiSpb.getCreateDate(),angkut.getTransaksiPanen().getCreateDate());
//                        Integer selisih = Integer.parseInt(String.valueOf(
//                                Math.abs(TimeUnit.MILLISECONDS.toDays(
//                                        transaksiSpb.getCreateDate() - angkut.getTransaksiPanen().getCreateDate()
//                                ))
//                        ));

                        if (selisih >= 2) {
                            infoRestan.setJjgH2(infoRestan.getJjgH2() + angkut.getJanjang());
                            if (selisih > infoRestan.getLamaRestan()) {
                                infoRestan.setLamaRestan(selisih);
                            }
                        }
                    }
                    angkutTph++;
                } else if (angkut.getTransaksiAngkut() != null) {
                    angkutTransit++;
                }
            }
        }

        Estate estate = GlobalHelper.getEstateByEstCode(transaksiSpb.getEstCode());

        String afdeling = null;
        SPK spk = null;

        if(transaksiSpb.getTransaksiAngkut().getSpk() != null){
            spk = transaksiSpb.getTransaksiAngkut().getSpk();
        }

        String format1 = "%-11s";
        String format2 = "%-15s";
        String format3 = "%-9s|";
        String format4 = "%5s|";
        String format5 = "|%2s|";
        String format6 = "%-18s";
        String format7 = "%-15s";
        String format8 = "|%-12s|";
        String format9 = "%-7s|";
        String format10 = "|%-30s|";

        try {
//            final byte[][] byteCodebar = {
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//                    { 0x1b, 0x40 },// 复位打印机
//            };
//            SendDataByte(byteCodebar[9]);
           // String textQr = transaksiSpb.getIdSPB();

            String noSpb = transaksiSpb.getNoSpb() == null ? TransaksiSpbHelper.converIdSPBToNoSPB(transaksiSpb.getIdSPB()) : transaksiSpb.getNoSpb();

            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x11;
            SendDataByte(Command.GS_ExclamationMark);
            SendDataByte(PrinterCommand.POS_Print_Text("Surat\n", "GBK", 0, 1, 1, 35));
            SendDataByte(PrinterCommand.POS_Print_Text("Pengangkutan\n", "GBK", 0, 1, 1, 35));
            SendDataByte(PrinterCommand.POS_Print_Text("Buah\n", "GBK", 0, 1, 1, 35));

            if(transaksiSpb.getTransaksiAngkut().isRevisi()){
                SendDataByte(PrinterCommand.POS_Print_Text("-------------------------------\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text("R E V I S I O N    C O P Y\n", "GBK", 0, 1, 1, 35));
                SendDataByte(PrinterCommand.POS_Print_Text("-------------------------------\n", "GBK", 0, 0, 0, 0));
            }else if(copy) {
                SendDataByte(PrinterCommand.POS_Print_Text("-------------------------------\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text("C O P Y\n", "GBK", 0, 1, 1, 35));
                SendDataByte(PrinterCommand.POS_Print_Text("-------------------------------\n", "GBK", 0, 0, 0, 0));
            }

            Command.ESC_Align[2] = 0x00;
            SendDataByte(PrinterCommand.POS_Set_Bold(1));
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);
            SendDataByte(PrinterCommand.POS_Print_Text("================================\n", "GBK", 0, 0, 0, 0));


            byte[] barCode = PrintPicture.POS_PrintBMP(setBarcode(noSpb), 384, 0);
            Command.ESC_Align[2] = 0x01;
            SendDataByte(Command.ESC_Align);
            SendDataByte(barCode);
            SendDataByte("\n".getBytes("GBK"));
            SendDataByte(PrinterCommand.POS_Print_Text(noSpb + "\n", "GBK", 0, 0, 0, 5));
            SendDataByte(PrinterCommand.POS_Print_Text("================================\n", "GBK", 0, 0, 0, 0));

            if(isccInformation != null) {
                SendDataByte(PrinterCommand.POS_Print_Text("FFB SUSTAINABLE ISCC " + isccInformation.getEstNewCode()+ "\n", "GBK", 0, 0, 0, 5));
                SendDataByte(PrinterCommand.POS_Print_Text(isccInformation.getCertificateNumber()+ "\n", "GBK", 0, 0, 0, 5));
                SendDataByte(PrinterCommand.POS_Print_Text("GHG Value Eec : "+isccInformation.getGhgValue()+" "+isccInformation.getGhgSatuan()+ "\n", "GBK", 0, 0, 0, 5));
                SendDataByte(PrinterCommand.POS_Print_Text("SC Model : "+isccInformation.getCertificateType()+ "\n", "GBK", 0, 0, 0, 5));
                SendDataByte(PrinterCommand.POS_Print_Text("ISCC Compliant\n", "GBK", 0, 0, 0, 5));
                double pksDistance = TransaksiSpbHelper.cekLandDistance(transaksiSpb.getTransaksiAngkut());
                if(pksDistance > 0) {
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format("Jarak Ke PKS : %.1f KM", pksDistance) + "\n", "GBK", 0, 0, 0, 5));
                }
                SendDataByte(PrinterCommand.POS_Print_Text("================================\n", "GBK", 0, 0, 0, 5));
            }

            if(transaksiSpb.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS) {
                String text = TransaksiSpbHelper.toQrSpb(transaksiSpb).replace("\n", "");
                afdeling = TransaksiSpbHelper.getAfdelingFromQr(text);
                byte[] qrcode = PrintPicture.POS_PrintBMP(setQr(text), 384, 0);
                Command.ESC_Align[2] = 0x01;
                SendDataByte(Command.ESC_Align);
                SendDataByte(qrcode);
            }

            Command.ESC_Align[2] = 0x00;
            SendDataByte(PrinterCommand.POS_Set_Bold(1));
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);

            SendDataByte(PrinterCommand.POS_Print_Text("========="+ context.getResources().getString(R.string.info_transaksi)+"=========\n", "GBK", 0, 0, 0, 0));
            if(estate != null) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " + String.format(format2, estate.getCompanyShortName()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " + String.format(format2, estate.getEstName()) + "\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "PT") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_estate) + transaksiSpb.getEstCode()) + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Estate") + " : " + String.format(format2, context.getResources().getString(R.string.dont_have_estate) + transaksiSpb.getEstCode()) + "\n", "GBK", 0, 0, 0, 0));
            }

            if(afdeling != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Afdeling") + " : " + String.format(format2, afdeling) + "\n", "GBK", 0, 0, 0, 0));
            }

            if(spk != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "OA") + " : " + String.format(format2, TransaksiPanenHelper.showSPK(spk)) + "\n", "GBK", 0, 0, 0, 0));
            }
            User userSelected = null;
            if(this.context instanceof BaseActivity){
                userSelected = GlobalHelper.dataAllUser.get(transaksiSpb.getCreateBy());
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Input Oleh")+" : "+String.format(format2, userSelected != null ? userSelected.getUserFullName() : "-")+"\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Input Oleh")+" : "+String.format(format2, transaksiSpb.getCreateBy())+"\n", "GBK", 0, 0, 0, 0));
            }

            if(transaksiSpb.getSubstitute() != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Krani Penganti")+" : "+String.format(format2, transaksiSpb.getSubstitute().getNama())+"\n", "GBK", 0, 0, 0, 0));
            }

            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Waktu Input")+" : "+String.format(format2, sdf.format(transaksiSpb.getCreateDate()))+"\n", "GBK", 0, 0, 0, 0));

            int langsiranCount = 0;
            if(transaksiSpb.getTransaksiAngkut().getPengirimanGandas() != null) {
                Collections.sort(transaksiSpb.getTransaksiAngkut().getPengirimanGandas(), new Comparator<PengirimanGanda>() {
                    @Override
                    public int compare(PengirimanGanda o1, PengirimanGanda o2) {
                        return o1.getIdx() < o2.getIdx() ? -1 : (o1.getIdx() > o2.getIdx()) ? 1 : 0;
                    }
                });

                SendDataByte(PrinterCommand.POS_Print_Text("======="+ context.getResources().getString(R.string.info_pengiriman_lanjut)+"======\n", "GBK", 0, 0, 0, 0));
//                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5,"Ke")+
//                        String.format(format9,"Pemilik")+
//                        String.format(format3,"Unit")+
//                        String.format(format3,"Operator")+"\n", "GBK", 0, 0, 0, 0));
                for (int i = 0; i < transaksiSpb.getTransaksiAngkut().getPengirimanGandas().size(); i++) {
                    PengirimanGanda ganda = transaksiSpb.getTransaksiAngkut().getPengirimanGandas().get(i);
                    int ke = i + 1;
                    String awal = String.valueOf(ke);
                    if(ganda.getLangsiran() != null){
                        if(ganda.getLangsiran().getIdTujuan() != null){
                            langsiranCount++;
                            awal += " - "+ ganda.getLangsiran().getNamaTujuan() + "("+ ganda.getLangsiran().getBlock() +")";
//                            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Langsiran") + " : " +String.format(format2, ganda.getLangsiran().getNamaTujuan() + "("+ ganda.getLangsiran().getBlock() +")")+"\n", "GBK", 0, 0, 0, 0));
                        }
                    }

                    String kendaraanShowGanda = TransaksiAngkutHelper.showVehicle(ganda.getVehicle());
                    kendaraanShowGanda +=  ganda.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN ? "(" +TransaksiAngkut.KENDARAAN_CODE_KEBUN + ")" : "(" +TransaksiAngkut.KENDARAAN_CODE_LUAR + ")";
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Ke") + " : " +String.format(format2, awal)+"\n", "GBK", 0, 0, 0, 0));
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Kendaraan") + " : " +String.format(format2, kendaraanShowGanda)+"\n", "GBK", 0, 0, 0, 0));
                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Supir") + " : " +String.format(format2, ganda.getOperator().getNama())+"\n", "GBK", 0, 0, 0, 0));

//                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5,String.valueOf(ke))+
//                            String.format(format9,ganda.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN ? TransaksiAngkut.KEBUN : TransaksiAngkut.LUAR)+
//                            String.format(format3,TransaksiAngkutHelper.showVehicle(ganda.getVehicle()))+
//                            String.format(format3,ganda.getOperator().getNama())+"\n", "GBK", 0, 0, 0, 0));
                    if(ganda.getSpk() != null){
                        if (ganda.getSpk().getIdSPK() != null){
                            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "SPK") + " : " +String.format(format2, ganda.getSpk().getIdSPK())+"\n", "GBK", 0, 0, 0, 0));
//                            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5,"")+
//                                    String.format(format9,"SPK")+
//                                    String.format(format3,ganda.getSpk().getIdSPK())+
//                                    String.format(format3,"")+"\n", "GBK", 0, 0, 0, 0));
                        }
                    }
                    SendDataByte(PrinterCommand.POS_Print_Text("--------------------------------\n", "GBK", 0, 0, 0, 5));

                }
            }

            User assAfd = null;
            if(this.context instanceof BaseActivity) {
                assAfd = GlobalHelper.dataAllAsstAfd.get(transaksiSpb.getApproveBy());
            }

            SendDataByte(PrinterCommand.POS_Print_Text("============"+context.getResources().getString(R.string.info_spb)+"============\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Id SPB")+" : "+noSpb+"\n", "GBK", 0, 0, 0, 0));
            if(assAfd != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Approve") + " : " + assAfd.getUserFullName() + "\n", "GBK", 0, 0, 0, 0));
            }else {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Approve") + " : " + transaksiSpb.getApproveBy() + "\n", "GBK", 0, 0, 0, 0));
            }
            String tujuan = transaksiSpb.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS ? TransaksiAngkut.PKS :
                    transaksiSpb.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_KELUAR ? TransaksiAngkut.KELAUR : "-";
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Tujuan")+" : "+ tujuan +"\n", "GBK", 0, 0, 0, 0));

            if(tujuan == TransaksiAngkut.PKS){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Nama PKS   ")+" : "+ transaksiSpb.getTransaksiAngkut().getPks().getNamaPKS() +"\n", "GBK", 0, 0, 0, 0));
            }

            userSelected = null;
            if(this.context instanceof BaseActivity) {
                userSelected = GlobalHelper.dataAllUser.get(transaksiSpb.getTransaksiAngkut().getCreateBy());
            }
            String kendaraanShow = TransaksiAngkutHelper.showVehicle(transaksiSpb.getTransaksiAngkut().getVehicle());
            kendaraanShow +=  transaksiSpb.getTransaksiAngkut().getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN ? "(" +TransaksiAngkut.KENDARAAN_CODE_KEBUN + ")" : "(" +TransaksiAngkut.KENDARAAN_CODE_LUAR + ")";

            SendDataByte(PrinterCommand.POS_Print_Text("==========="+ context.getResources().getString(R.string.info_vehicle)+"=========\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Supir") + " : " +String.format(format2, transaksiSpb.getTransaksiAngkut().getSupir().getNama())+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Kendaraan") + " : "+String.format(format2,kendaraanShow )+"\n", "GBK", 0, 0, 0, 0));
            if(transaksiSpb.getTransaksiAngkut().getVehicle().getVraOrderNumber() != null) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Vehicle No") + " : " + String.format(format2, transaksiSpb.getTransaksiAngkut().getVehicle().getVraOrderNumber()) + "\n", "GBK", 0, 0, 0, 0));
            }else{
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Vehicle No") + " : " + String.format(format2, "-") + "\n", "GBK", 0, 0, 0, 0));
            }

            if (transaksiSpb.getTransaksiAngkut().getCages() != null) {
                if (!transaksiSpb.getTransaksiAngkut().getCages().isEmpty()) {
                    int count = 1;
                    for (int i = 0; i < transaksiSpb.getTransaksiAngkut().getCages().size(); i++) {
                        Cages cages = transaksiSpb.getTransaksiAngkut().getCages().get(i);
                        SendDataByte(PrinterCommand.POS_Print_Text(
                                String.format("Cages No. %d : %s\n", count, cages.getAssetName()), "GBK", 0, 0, 0, 0));
                        count++;
                    }
                }
            }

            if(transaksiSpb.getTransaksiAngkut().getBin() != null){
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Bin") + " : "+String.format(format2, transaksiSpb.getTransaksiAngkut().getBin())+"\n", "GBK", 0, 0, 0, 0));
            }

            if(transaksiSpb.getTransaksiAngkut().getLoader() != null){
                if(transaksiSpb.getTransaksiAngkut().getLoader().size() > 0) {
                    SendDataByte(PrinterCommand.POS_Print_Text("===========" + context.getResources().getString(R.string.info_loader) + "==========\n", "GBK", 0, 0, 0, 0));
//                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format8, "Nama") +
//                            String.format(format3, "NIK") +
//                            String.format(format9, "Estate") + "\n", "GBK", 0, 0, 0, 0));
                    int count = 1;
                    for (int i = 0; i < transaksiSpb.getTransaksiAngkut().getLoader().size(); i++) {
                        Operator loader = transaksiSpb.getTransaksiAngkut().getLoader().get(i);

                        SendDataByte(PrinterCommand.POS_Print_Text(String.format("%,d . ",count)+ loader.getNama() + " ("+ loader.getNik() + ")\n", "GBK", 0, 0, 0, 5));
                        count++;
//                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format8, loader.getNama()) +
//                                String.format(format3, loader.getNik()) +
//                                String.format(format9, loader.getArea()) + "\n", "GBK", 0, 0, 0, 0));
                    }
                }
            }

            SendDataByte(PrinterCommand.POS_Print_Text("============"+ context.getResources().getString(R.string.info_angkut)+"=========\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ TPH") +" : "+String.format("%,d",angkutTph)+"\n", "GBK", 0, 0, 0, 0));
            if(langsiranCount > 0) {
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6, "Σ Langsiran") + " : " + String.format("%,d", langsiranCount) + "\n", "GBK", 0, 0, 0, 0));
            }
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format6,"Σ OPH") +" : "+String.format("%,d",angkutTph)+"\n", "GBK", 0, 0, 0, 0));

            SendDataByte(PrinterCommand.POS_Print_Text("==========="+ context.getResources().getString(R.string.tbs_angkut)+"===========\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Janjang")+" : "+ String.format(format2,String.format("%,d",transaksiSpb.getTotalJanjang()) + " Jjg") +"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Brondolan")+" : "+String.format(format2,String.format("%,d",transaksiSpb.getTotalBrondolan()) + " Kg")+"\n", "GBK", 0, 0, 0, 0));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1,"Berat")+" : "+String.format(format2,String.format("%.0f",transaksiSpb.getTransaksiAngkut().getTotalBeratEstimasi()) +  " Kg")+"\n", "GBK", 0, 0, 0, 0));

            if(infoRestan.getJjgH2() > 0) {
                int persen  = (int) Math.round((Double.parseDouble(String.valueOf(infoRestan.getJjgH2())) / Double.parseDouble(String.valueOf(transaksiSpb.getTotalJanjang())) ) * 100);
                SendDataByte(PrinterCommand.POS_Print_Text("========" + context.getResources().getString(R.string.info_lama_restan) + "========\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, ">= 2Hari") + " : " + String.format(format2,String.valueOf(infoRestan.getJjgH2()) + "Jjg ("+String.valueOf(persen)+"%)") + "\n", "GBK", 0, 0, 0, 0));
                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format1, "Paling Lama") + " : " + String.format(format2,String.valueOf(infoRestan.getLamaRestan()) +" Hari") + "\n", "GBK", 0, 0, 0, 0));
            }
            Collections.sort(subTotalAngkutMekanisasis, new Comparator<SubTotalAngkutMekanisasi>() {
                @Override
                public int compare(SubTotalAngkutMekanisasi t0, SubTotalAngkutMekanisasi t1) {
                    String a = "";
                    String b = "";
                    if(t0.getTransaksiPanen() != null){
                        a = t0.getTransaksiPanen().getTph().getBlock();
                    }else if (t0.getAngkut() != null){
                        a = t0.getAngkut().getBlock();
                    }

                    if(t1.getTransaksiPanen() != null) {
                        b = t1.getTransaksiPanen().getTph().getBlock();
                    }else if(t1.getAngkut() != null) {
                        b = t1.getAngkut().getBlock();
                    }

                    return (a.compareTo(b));
                }
            });

            Angkut subTotalAngkutBlock = null;
            int count = 1;
            for(int i = 0; i < subTotalAngkutMekanisasis.size(); i++) {
                if (subTotalAngkutMekanisasis.get(i).getTransaksiPanen() != null) {
                    TransaksiPanen panen = subTotalAngkutMekanisasis.get(i).getTransaksiPanen();
                    if (i == 0) {
                        SendDataByte(PrinterCommand.POS_Print_Text("==========" + context.getResources().getString(R.string.detail_angkut) + "=========\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5, "No") + String.format(format3, "Blok") +
                                String.format(format4, "Jjg") +
                                String.format(format4, "Brdl") +
                                String.format(format4, "Berat") + "\n", "GBK", 0, 0, 0, 0));
                    }

                    String nama = "M";
                    String nama1 = "0";
                    String nama2 = "0";
                    String nama3 = "0";
                    String block = panen.getBlock() == null ? "" : panen.getBlock();
                    if (panen != null) {
                        nama = "TPH";
                        if (panen.getTph() != null) {
                            nama = panen.getTph().getNamaTph() != null ?
                                    panen.getTph().getNamaTph() : "TPH";
                            nama += panen.getTph().getBlock() != null ?
                                    "/" + panen.getTph().getBlock() : "-";
                            block = panen.getTph().getBlock() != null ?
                                    panen.getTph().getBlock() : "-";
                        }
                    }

                    if (subTotalAngkutBlock == null) {
                        subTotalAngkutBlock = new Angkut(panen.getHasilPanen().getTotalJanjang(),
                                panen.getHasilPanen().getBrondolan(),
                                block,
                                (panen.getHasilPanen().getTotalJanjang() * panen.getBjrMekanisasi())
                        );
                    } else if (subTotalAngkutBlock.getBlock().equals(block)) {
                        subTotalAngkutBlock.setJanjang(subTotalAngkutBlock.getJanjang() + panen.getHasilPanen().getTotalJanjang());
                        subTotalAngkutBlock.setBrondolan(subTotalAngkutBlock.getBrondolan() + panen.getHasilPanen().getBrondolan());
                        subTotalAngkutBlock.setBerat(subTotalAngkutBlock.getBerat() + (panen.getHasilPanen().getTotalJanjang() * panen.getBjrMekanisasi()));
                    } else if (!subTotalAngkutBlock.getBlock().equals(block)) {
//                    SendDataByte("================================\n".getBytes("GBK"));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5, count) +
                                String.format(format3, subTotalAngkutBlock.getBlock()) +
                                String.format(format4, subTotalAngkutBlock.getJanjang()) +
                                String.format(format4, subTotalAngkutBlock.getBrondolan()) +
                                String.format(format4, String.format("%.0f", subTotalAngkutBlock.getBerat())) + "\n", "GBK", 0, 0, 0, 0));
//
//                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format8,String.valueOf(count) +". Block "+subTotalAngkutBlock.getBlock())+
//                             SendDataByte("================================\n".getBytes("GBK"));
                        subTotalAngkutBlock = new Angkut(panen.getHasilPanen().getTotalJanjang(),
                                panen.getHasilPanen().getBrondolan(),
                                block,
                                (panen.getHasilPanen().getTotalJanjang() * panen.getBjrMekanisasi()));

                        count++;
                    }

//                nama1 = String.format("%,d",angkut.getJanjang());
//                nama2 = String.format("%,d",angkut.getBrondolan());
//                nama3 = String.format("%.0f",angkut.getBerat());
//                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5,String.valueOf(count)) +String.format(format3,nama)+
//                        String.format(format4,nama1)+
//                        String.format(format4,nama2)+
//                        String.format(format4,nama3)+"\n", "GBK", 0, 0, 0, 0));
//                SendDataByte(PrinterCommand.POS_Print_Text(String.format(format10,String.valueOf(angkut.getOph()))+"\n", "GBK", 0, 0, 0, 0));
                }else if (subTotalAngkutMekanisasis.get(i).getAngkut() != null) {
                    Angkut angkut = subTotalAngkutMekanisasis.get(i).getAngkut();
                    if(i == 0 ){
                        SendDataByte(PrinterCommand.POS_Print_Text("=========="+ context.getResources().getString(R.string.detail_angkut)+"=========\n", "GBK", 0, 0, 0, 0));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5,"No")+ String.format(format3,"Blok")+
                                String.format(format4,"Jjg")+
                                String.format(format4,"Brdl")+
                                String.format(format4,"Berat")+"\n", "GBK", 0, 0, 0, 0));
                    }

                    String nama = "M";
                    String nama1 = "0";
                    String nama2 = "0";
                    String nama3 = "0";
                    String block = angkut.getBlock() == null ? "" : angkut.getBlock();
                    if(angkut.getTransaksiPanen() != null){
                        nama = "TPH";
                        if(angkut.getTransaksiPanen().getTph() != null){
                            nama = angkut.getTransaksiPanen().getTph().getNamaTph() != null ?
                                    angkut.getTransaksiPanen().getTph().getNamaTph() : "TPH";
                            nama += angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                    "/" +angkut.getTransaksiPanen().getTph().getBlock() : "-";
                            block = angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                    angkut.getTransaksiPanen().getTph().getBlock() : "-";
                        }
                    }else if (angkut.getTph() != null) {
                        nama = angkut.getTph().getNamaTph() != null ?
                                angkut.getTph().getNamaTph() : "TPH";
                        nama += angkut.getTph().getBlock() != null ?
                                "/" +angkut.getTph().getBlock() : "-";
                        block = angkut.getTph().getBlock() != null ?
                                angkut.getTph().getBlock() : "-";
                        nama += " M";
                    }else if (angkut.getTransaksiAngkut() != null){
                        if(angkut.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS){
                            nama = "PKS";
                        }else {
                            //harusnya nga ada lagi yang ke kondisi ini
                            if (angkut.getTransaksiAngkut().getLangsiran() == null) {
                                nama = "L";
                            } else {
//                            nama = angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() != null ?
//                                    angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() : "Langsiran";
                                nama = "L";
                                nama += angkut.getTransaksiAngkut().getLangsiran().getBlock() != null ?
                                        "/"+angkut.getTransaksiAngkut().getLangsiran().getBlock() : "-";
                                block = angkut.getBlock() != null ?
                                        angkut.getBlock() : "-";
//                            nama += " L";
                            }
                        }
                    }else if (angkut.getLangsiran() != null) {
//                    nama = angkut.getLangsiran().getNamaTujuan() != null ?
//                            angkut.getLangsiran().getNamaTujuan(): "L";
                        nama = "L";
                        nama += angkut.getLangsiran().getBlock() != null ?
                                "/" +angkut.getLangsiran().getBlock(): "-";
                        block = angkut.getLangsiran().getBlock() != null ?
                                angkut.getLangsiran().getBlock(): "-";
                        nama += " M";
                    }

                    if(subTotalAngkutBlock == null){
                        subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
                                angkut.getBrondolan(),
                                block,
                                angkut.getBerat());
                    }else if(subTotalAngkutBlock.getBlock().equals(block)){
                        subTotalAngkutBlock.setJanjang(subTotalAngkutBlock.getJanjang() + angkut.getJanjang());
                        subTotalAngkutBlock.setBrondolan(subTotalAngkutBlock.getBrondolan() + angkut.getBrondolan());
                        subTotalAngkutBlock.setBerat(subTotalAngkutBlock.getBerat() + angkut.getBerat());
                    }else if(!subTotalAngkutBlock.getBlock().equals(block)){
//                    SendDataByte("================================\n".getBytes("GBK"));
                        SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5, count) +
                                String.format(format3,subTotalAngkutBlock.getBlock())+
                                String.format(format4,subTotalAngkutBlock.getJanjang())+
                                String.format(format4,subTotalAngkutBlock.getBrondolan())+
                                String.format(format4, String.format("%.0f",subTotalAngkutBlock.getBerat()))+"\n", "GBK", 0, 0, 0, 0));
//
//                    SendDataByte(PrinterCommand.POS_Print_Text(String.format(format8,String.valueOf(count) +". Block "+subTotalAngkutBlock.getBlock())+
//                             SendDataByte("================================\n".getBytes("GBK"));
                        subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
                                angkut.getBrondolan(),
                                block,
                                angkut.getBerat());

                        count++;
                    }
                }
            }

//            SendDataByte("================================\n".getBytes("GBK"));
            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format5, count) +
                    String.format(format3,subTotalAngkutBlock.getBlock())+
                    String.format(format4,subTotalAngkutBlock.getJanjang())+
                    String.format(format4,subTotalAngkutBlock.getBrondolan())+
                    String.format(format4, String.format("%.0f",subTotalAngkutBlock.getBerat()))+"\n", "GBK", 0, 0, 0, 0));

//            SendDataByte(PrinterCommand.POS_Print_Text(String.format(format8,String.valueOf(count) +". Block "+subTotalAngkutBlock.getBlock())+
//                    String.format(format4,subTotalAngkutBlock.getJanjang())+
//                    String.format(format4,subTotalAngkutBlock.getBrondolan())+
//                    String.format(format4,String.format("%.0f",subTotalAngkutBlock.getBerat()))+"\n", "GBK", 0, 0, 0, 0));

//            SendDataByte("================================\n".getBytes("GBK"));

            String sharePrefQr = sharedPrefHelper.getName(SharedPrefHelper.KEY_DoubleQrSPB);
            int positionQr = QrDoubleSPBHelpper.DoubleQr;
            if(sharePrefQr != null){
                positionQr = Integer.parseInt(sharePrefQr);
            }

            if(positionQr == QrDoubleSPBHelpper.DoubleQr) {
                if (transaksiSpb.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS) {
                    String text = TransaksiSpbHelper.toQrSpb(transaksiSpb).replace("\n", "");
                    byte[] qrcode = PrintPicture.POS_PrintBMP(setQr(text), 384, 0);
                    Command.ESC_Align[2] = 0x01;
                    SendDataByte(Command.ESC_Align);
                    SendDataByte(qrcode);
                }
            }

//            String qRDetailAngkut = TransaksiSpbHelper.toQRDetailAngkut(transaksiSpb);
//            if(qRDetailAngkut != null) {
//                SendDataByte(PrinterCommand.POS_Print_Text("=========="+ context.getResources().getString(R.string.detail_angkut)+"=========\n", "GBK", 0, 0, 0, 0));
//                byte[] qrcode = PrintPicture.POS_PrintBMP(setQr(qRDetailAngkut), 384, 0);
//                Command.ESC_Align[2] = 0x01;
//                SendDataByte(Command.ESC_Align);
//                SendDataByte(qrcode);
//            }
            Command.ESC_Align[2] = 0x00;
            SendDataByte(PrinterCommand.POS_Set_Bold(1));
            SendDataByte(Command.ESC_Align);
            Command.GS_ExclamationMark[2] = 0x00;
            SendDataByte(Command.GS_ExclamationMark);

            SendDataByte("================================\n".getBytes("GBK"));
            SendDataByte(PrinterCommand.POS_Print_Text("Print "+ GlobalHelper.getUser().getUserFullName()+" "+ sdf.format(System.currentTimeMillis()) +"\n", "GBK", 0, 0, 0, 2));
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(58));
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(58));
            SendDataByte(Command.GS_V_m_n);
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private Bitmap setQr(String text){
        try {

            QRCodeWriter writer = new QRCodeWriter();

            Log.i(TAG, "生成的文本：" + text);
            if (text == null || "".equals(text) || text.length() < 1) {
                //Toast.makeText(this, "Kosong Bos", Toast.LENGTH_SHORT).show();
                return null;
            }

            // 把输入的文本转为二维码
            BitMatrix martix = writer.encode(text, BarcodeFormat.QR_CODE, QR_WIDTH, QR_HEIGHT);

            System.out.println("w:" + martix.getWidth() + "h:"
                    + martix.getHeight());

            Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            BitMatrix bitMatrix = new QRCodeWriter().encode(text,
                    BarcodeFormat.QR_CODE, QR_WIDTH, QR_HEIGHT, hints);
            int[] pixels = new int[QR_WIDTH * QR_HEIGHT];
            for (int y = 0; y < QR_HEIGHT; y++) {
                for (int x = 0; x < QR_WIDTH; x++) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * QR_WIDTH + x] = 0xff000000;
                    } else {
                        pixels[y * QR_WIDTH + x] = 0xffffffff;
                    }

                }
            }

            Bitmap bitmap = Bitmap.createBitmap(QR_WIDTH, QR_HEIGHT,
                    Bitmap.Config.ARGB_8888);

            bitmap.setPixels(pixels, 0, QR_WIDTH, 0, 0, QR_WIDTH, QR_HEIGHT);
            return bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Bitmap setBarcode(String text){
        try {
            MultiFormatWriter writer = new MultiFormatWriter();
            String finalData = Uri.encode(text);

            // Use 1 as the height of the matrix as this is a 1D Barcode.
            BitMatrix bm = writer.encode(finalData, BarcodeFormat.CODE_128, BARCODE_WIDTH, 1);
            int bmWidth = bm.getWidth();

            Bitmap imageBitmap = Bitmap.createBitmap(bmWidth, BARCODE_HEIGHT, Bitmap.Config.ARGB_8888);

            for (int i = 0; i < bmWidth; i++) {
                // Paint columns of width 1
                int[] column = new int[BARCODE_HEIGHT];
                Arrays.fill(column, bm.get(i, 0) ? Color.BLACK : Color.WHITE);
                imageBitmap.setPixels(column, 0, 1, i, 0, 1, BARCODE_HEIGHT);
            }

            return imageBitmap;
        } catch (WriterException e) {
            e.printStackTrace();
            return null;
        }
    }

    /****************************************************************************************************/
    @SuppressLint("HandlerLeak")
    public final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    Log.i("HandlerLeak", "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
//					mTitle.setText(R.string.title_connected_to);
//					mTitle.append(mConnectedDeviceName);
                            break;
                        case BluetoothService.STATE_CONNECTING:
//					mTitle.setText(R.string.title_connecting);
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
//					mTitle.setText(R.string.title_not_connected);
                            break;
                    }
                    break;
                case MESSAGE_WRITE:

                    break;
                case MESSAGE_READ:

                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(HarvestApp.getContext(),
                            "Connected to " + mConnectedDeviceName,
                            Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(HarvestApp.getContext(),
                            msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
                            .show();
                    break;
                case MESSAGE_CONNECTION_LOST:    //蓝牙已断开连接
                    Toast.makeText(HarvestApp.getContext(), "Device connection was lost",
                            Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_UNABLE_CONNECT:     //无法连接设备
                    Toast.makeText(HarvestApp.getContext(), "Unable to connect device",
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    class AdapterBluetoothPrinter extends RecyclerView.Adapter<AdapterBluetoothPrinter.Holder> {

        Context context;
        private final List<BluetoothDevice> originLists;

        public AdapterBluetoothPrinter(Context context, Set<BluetoothDevice> pairedDevices){
            this.context = context;
            originLists = new ArrayList<BluetoothDevice>(pairedDevices);
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.list_popup_item,parent,false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(AdapterBluetoothPrinter.Holder holder, int position) {
            holder.setValue(originLists.get(position));
        }

        @Override
        public int getItemCount() {
            return originLists.size();
        }

        public class Holder extends RecyclerView.ViewHolder {
            TextView item_blockId;
            TextView item_lastUpdate;
            View view;

            public Holder(View itemView) {
                super(itemView);
                item_blockId = (TextView) itemView.findViewById(R.id.item_blockId);
                item_lastUpdate = (TextView) itemView.findViewById(R.id.item_lastUpdate);
                view = itemView;
            }

            public void setValue(final BluetoothDevice bluetoothDevice) {
                item_blockId.setText(bluetoothDevice.getName());
                item_lastUpdate.setText(bluetoothDevice.getAddress());

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        selectBluetooth(bluetoothDevice);
                    }
                });
            }

        }
    }
}
