package id.co.ams_plantation.harvestsap.model;

public class StampImage {
    Pemanen pemanen;
    TPH tph;
    Langsiran langsiran;
    PKS pks;
    String block;
    Operator operator;
    Vehicle vehicle;
    String Bin;
    HasilPanen hasilPanen;

    public StampImage() {
    }

    public Pemanen getPemanen() {
        return pemanen;
    }

    public void setPemanen(Pemanen pemanen) {
        this.pemanen = pemanen;
    }

    public TPH getTph() {
        return tph;
    }

    public void setTph(TPH tph) {
        this.tph = tph;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public Langsiran getLangsiran() {
        return langsiran;
    }

    public void setLangsiran(Langsiran langsiran) {
        this.langsiran = langsiran;
    }

    public PKS getPks() {
        return pks;
    }

    public void setPks(PKS pks) {
        this.pks = pks;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public String getBin() {
        return Bin;
    }

    public void setBin(String bin) {
        Bin = bin;
    }

    public HasilPanen getHasilPanen() {
        return hasilPanen;
    }

    public void setHasilPanen(HasilPanen hasilPanen) {
        this.hasilPanen = hasilPanen;
    }
}
