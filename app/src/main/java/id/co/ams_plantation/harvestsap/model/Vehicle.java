package id.co.ams_plantation.harvestsap.model;

public class Vehicle {

    public static final String vehicleClasificationLand = "LAND";
    public static final String vehicleClasificationWater = "WATER";

    private String vraOrderNumber;
    private String vehicleCode;
    private String vehicleName;
    private String policeNo;
    private String groupCode;
    private String groupName;
    private String estCode;
    private String vehicleClasification;
    private String estCodeSAP;
    private String QRCode;

    private String inputType;

    public static final String inputTypeQr = "0" ;//=> qr = >hijau
    public static final String inputTypeMaster = "1" ;//=> master = >kuning
    public static final String inputTypeNoMaster = "2" ;//=> tidak ada di master = >merah

    public String getVraOrderNumber() {
        return vraOrderNumber;
    }

    public void setVraOrderNumber(String vraOrderNumber) {
        this.vraOrderNumber = vraOrderNumber;
    }

    public String getVehicleCode() {
        return vehicleCode;
    }

    public void setVehicleCode(String vehicleCode) {
        this.vehicleCode = vehicleCode;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getPoliceNo() {
        return policeNo;
    }

    public void setPoliceNo(String policeNo) {
        this.policeNo = policeNo;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getVehicleClasification() {
        return vehicleClasification;
    }

    public void setVehicleClasification(String vehicleClasification) {
        this.vehicleClasification = vehicleClasification;
    }

    public String getEstCodeSAP() {
        return estCodeSAP;
    }

    public void setEstCodeSAP(String estCodeSAP) {
        this.estCodeSAP = estCodeSAP;
    }

    public String getQRCode() {
        return QRCode;
    }

    public void setQRCode(String QRCode) {
        this.QRCode = QRCode;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }
}
