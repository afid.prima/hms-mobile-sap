package id.co.ams_plantation.harvestsap.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.LocationDisplayManager;
import com.esri.android.map.ags.ArcGISLocalTiledLayer;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.android.map.ogc.kml.KmlLayer;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.CompositeSymbol;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.esri.core.symbol.TextSymbol;
import com.evrencoskun.tableview.TableView;
import com.google.gson.Gson;
import com.labo.kaji.fragmentanimations.MoveAnimation;
import com.mikepenz.iconics.utils.Utils;
import com.philliphsu.bottomsheetpickers.BottomSheetPickerDialog;
import com.philliphsu.bottomsheetpickers.time.BottomSheetTimePickerDialog;
import com.philliphsu.bottomsheetpickers.time.numberpad.NumberPadTimePickerDialog;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.SwipeItemRecycleView.SwipeController;
import id.co.ams_plantation.harvestsap.SwipeItemRecycleView.SwipeControllerActions;
import id.co.ams_plantation.harvestsap.adapter.SelectGangAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectPemanenAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectSpkAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectSupervisionAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectTphAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectTphnLangsiranAdapter;
import id.co.ams_plantation.harvestsap.adapter.SubPemanenAdapter;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.DynamicParameterPenghasilan;
import id.co.ams_plantation.harvestsap.model.Gerdang;
import id.co.ams_plantation.harvestsap.model.GerdangConfig;
import id.co.ams_plantation.harvestsap.model.HasilPanen;
import id.co.ams_plantation.harvestsap.model.Kongsi;
import id.co.ams_plantation.harvestsap.model.Langsiran;
import id.co.ams_plantation.harvestsap.model.ManualSPB;
import id.co.ams_plantation.harvestsap.model.MekanisasiOperator;
import id.co.ams_plantation.harvestsap.model.NearestBlock;
import id.co.ams_plantation.harvestsap.model.Pemanen;
import id.co.ams_plantation.harvestsap.model.ReasonTphBayangan;
import id.co.ams_plantation.harvestsap.model.SPK;
import id.co.ams_plantation.harvestsap.model.StampImage;
import id.co.ams_plantation.harvestsap.model.SubPemanen;
import id.co.ams_plantation.harvestsap.model.SubTotalAngkut;
import id.co.ams_plantation.harvestsap.model.SupervisionList;
import id.co.ams_plantation.harvestsap.model.TPH;
import id.co.ams_plantation.harvestsap.model.TPHnLangsiran;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.model.Vehicle;
import id.co.ams_plantation.harvestsap.service.BluetoothService;
import id.co.ams_plantation.harvestsap.ui.AngkutActivity;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.MekanisasiActivity;
import id.co.ams_plantation.harvestsap.ui.QcMutuBuahActivity;
import id.co.ams_plantation.harvestsap.ui.SpbActivity;
import id.co.ams_plantation.harvestsap.ui.TphActivity;
import id.co.ams_plantation.harvestsap.util.BjrHelper;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.ModelRecyclerViewHelper;
import id.co.ams_plantation.harvestsap.util.NfcHelper;
import id.co.ams_plantation.harvestsap.util.PemanenGerdangHelper;
import id.co.ams_plantation.harvestsap.util.SharedPrefHelper;
import id.co.ams_plantation.harvestsap.util.TphHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiPanenHelper;
import id.co.ams_plantation.harvestsap.util.ValidationUtil;
import id.co.ams_plantation.harvestsap.util.VehicleMekanisasiHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import ng.max.slideview.SlideView;
import pl.aprilapps.easyphotopicker.EasyImage;

import static org.dizitart.no2.objects.filters.ObjectFilters.and;
import static org.dizitart.no2.objects.filters.ObjectFilters.eq;
import static id.co.ams_plantation.harvestsap.util.GlobalHelper.isDoubleClick;

/**
 * Created by user on 12/4/2018.
 */

public class TphInputFragment extends Fragment implements BottomSheetTimePickerDialog.OnTimeSetListener {

    final int infoNormal = 0;
    final int infoMentah = 1;
    final int infoBusuk = 2;
    final int infoLewatMatang = 3;
    final int infoAbnormal = 4;
    final int infoTangkaiPanjang = 5;
    final int infoTotalJanjang= 6;
    final int infoTotalJanjangPendapatan= 7;
    final int infoBuahDimakanTikus= 8;

    final int LongOperation_NFC_With_Save = 0;
    final int LongOperation_Print_With_Save = 1;
    final int LongOperation_Share_With_Save = 2;
    final int LongOperation_Save = 3;
    final int LongOperation_NFC_Only = 4;
    final int LongOperation_Print_Only = 5;
    final int LongOperation_Share_Only = 6;
    final int LongOperation_Delet_Data = 7;
    final int LongOperation_Save_Panen_T2 = 8;
    final int LongOperation_Delet_Data_T2 = 9;
    final int LongOperation_Setup_Data_TPH = 10;
    final int LongOperation_Setup_Data_TPHnLangsrian = 11;
    final int LongOperation_Save_Data_Qc = 12;
    final int LongOperation_Delet_Data_Qc = 13;
//    public final int LongOperation_Save_Panen_T2_NFC = 14;
//    final int LongOperation_Save_Panen_T2_Print = 15;
//    final int LongOperation_Save_Panen_T2_Share = 16;
    final int LongOperation_Save_Panen_T2_MANUAL = 14;
    final int LongOperation_Setup_Point = 15;
    final int LongOperation_Setup_SPK = 16;
    final int LongOperation_UpdateLocation_Setup_Point = 17;

    final int YesNo_Simpan = 10;
    final int YesNo_SimpanT2 = 11;
    final int YesNo_Hapus = 12;
    final int YesNo_HapusT2 = 13;
    final int YesNo_Batal = 14;
    final int YesNo_SimpanQc = 15;
    final int YesNo_HapusQc = 16;
    final int YesNo_UpdateQc = 17;
    final int YesNo_SimpanT2_MANUAL = 18;
    final int YesNo_Pilih_TPH = 19;
    final int YesNo_Pilih_TPH_Bayangan = 20;

    public double latitude;
    public double longitude;
    int totalJjgJelek;
    int totalJjg;
    int totalJjgPendapatan;
    SegmentedButtonGroup tipeKongsi;
    LinearLayout lSisa;
    RecyclerView rvPemanen;
    TextView tvSisa;
    TextView tvSisa2;
    TextView tvSisa3;
    CompleteTextViewHelper etPemanenSisa;
    CompleteTextViewHelper etSpk;
    TextView tvOa;
    SubPemanenAdapter subPemanenAdapter;
    SwipeController swipeController = null;

    AppCompatEditText etSearch;
    Button btnSearch;
    TableView tableview;

//    RecyclerView rv;
//    LinearLayout lnfc;
//    LinearLayout lprint;
//    LinearLayout lshare;
    LinearLayout lldeleted;
//    LinearLayout newTph;
    LinearLayout historyTransaksi;
    LinearLayout lsave;
    LinearLayout lcancel;
    LinearLayout lWarning;
    LinearLayout llkeyBoard;
    LinearLayout ketAction1;
    LinearLayout ketAction2;
    LinearLayout ketAction3;

    View tphphoto;
    View tphKrani;
    ImageView ivPhoto;
    ImageView imagecacnel;
    ImageView imagesview;
    SliderLayout sliderLayout;
    RelativeLayout rl_ipf_takepicture;

    LinearLayout lInfoTph;
    LinearLayout lInfoSubPemanen;
    View pemanenInfo;
    LinearLayout lTph;
    LinearLayout lBlock;
    LinearLayout lAncak;
    LinearLayout lTphBayangan;
    LinearLayout lStatus;
    LinearLayout lNoKartu;
    LinearLayout lKraniInfo;
    CompleteTextViewHelper etNotph;
    CompleteTextViewHelper etBlock;
    AppCompatEditText etAncak;
////    AppCompatEditText etBaris;
//    CompleteTextViewHelper etGang;
//    CompleteTextViewHelper etPemanen;
    CompleteTextViewHelper etSubstitute;
    public CompleteTextViewHelper etPemanenGerdang;
    public CompleteTextViewHelper etGangGerdang;
    CheckBox cbTphBayangan;
    CheckBox cbBorongan;
    CheckBox cbKraniSubstitute;
    CheckBox cbGerdang;

    TextView tvInfoTph;
    TextView tvTph;
    public TextView tvGerdang;

    LinearLayout lReasonTphBayangan;
    LinearLayout lBorongan;
    LinearLayout lNormal;
    LinearLayout lMentah;
    LinearLayout lBusuk;
    LinearLayout lLewatMatang;
    LinearLayout lAbnormal;
    LinearLayout lTangkaiPanjang;
    LinearLayout lBrondolan;
    LinearLayout lBuahDimakanTikus;
    LinearLayout lTotalJanjangBuruk;
    LinearLayout lTotalJanjang;
    LinearLayout lTotalJanjangPendapatan;
    LinearLayout lJarak;
    LinearLayout lRemarks;
    LinearLayout lAlasBrondol;
    LinearLayout lInfoPemanen;
    LinearLayout lGerdang;
    CardView cvGerdang;

    LinearLayout lVehicleMekanisasi;
    public BetterSpinner lsKendaraanMekanisasi;
    public CompleteTextViewHelper etVehicleMekanisasi;

    View vTphPanen;
    View vTphphoto;
    LinearLayout lSubstitute;
    TextView tvNormal;
    TextView tvNormalSymbol;
    TextView tvMeasureNormal;
    TextView tvMentah;
    TextView tvMentahSymbol;
    TextView tvMeasureMentah;
    AppCompatEditText etReasonTphBayangan;
    AppCompatEditText etNormal;
    AppCompatEditText etMentah;
    AppCompatEditText etBusuk;
    AppCompatEditText etLewatMatang;
    AppCompatEditText etAbnormal;
    AppCompatEditText etBuahDimakanTikus;
    AppCompatEditText etBrondolan;
    AppCompatEditText etTangkaiPanjang;
    AppCompatEditText etJarak;
    AppCompatEditText etTotalJanjangBuruk;
    AppCompatEditText etTotalJanjang;
    AppCompatEditText etTotalJanjangPendapatan;
    AppCompatEditText etNoNFC;
    AppCompatEditText etRemarks;
//    ImageView ivTph;
    ImageView ivNormal;
    ImageView ivTotal;
    ImageView ivTotalPendapatan;
    ImageView ivMentah;
    ImageView ivBusuk;
    ImageView ivLewatMatang;
    ImageView ivAbnormal;
    ImageView ivTangkaiPanjang;
    ImageView ivBuahDimakanTikus;
    BetterSpinner lsAlasBrondol;
    BetterSpinner lsStatus;
    BetterSpinner lsReasonTphBayangan;

    LinearLayout infoAngkut;
    TextView tvTotalAngkut;
    TextView tvTotalBrondolan;
    TextView tvTotalJanjang;
    TextView tvTotalBeratEstimasi;

    LocationDisplayManager locationDisplayManager;
    ArcGISLocalTiledLayer tiledLayer;
    KmlLayer kmlLayer;
    SpatialReference spatialReference;
    GraphicsLayer graphicsLayer;
    GraphicsLayer graphicsLayerTPH;
    GraphicsLayer graphicsLayerLangsiran;
    GraphicsLayer graphicsLayerText;
    String locKmlMap;
    boolean isMapLoaded;

    public Vehicle vehicleMekanisasi;
    public int typeVehicleMekanisasi;
    public Pemanen pemanenSelected;
    public Pemanen pemanenGerdang;
    Pemanen pemanenSisa;
    HashMap<String,Pemanen> pemanenOperator;
    HashMap<String,Pemanen> pemanenOperatorTonase;
    SupervisionList substituteSelected;
    String blockSelected;
    TransaksiPanen selectedTransaksiPanen;
    TransaksiAngkut transaksiAngkutNFC;
    TransaksiAngkut transaksiAngkutNfcSave;
    String dataTransaksiNFC;
    TransaksiAngkut selectedTransaksiAngkut;
    ArrayList<SubTotalAngkut> subTotalAngkuts;
    ArrayList<Angkut> angkuts;
    Angkut selectedAngkut;
    TPH tphSelected;
    BaseActivity baseActivity;
    TphActivity tphActivity;
    AngkutActivity angkutActivity;
    SpbActivity spbActivity;
    MekanisasiActivity mekanisasiActivity;
    QcMutuBuahActivity qcMutuBuahActivity;

//    PemanenHelper pemanenHelper;
    public TphHelper tphHelper;
    TransaksiPanenHelper transaksiPanenHelper;
    SharedPrefHelper sharedPrefHelper;
    ModelRecyclerViewHelper modelRecyclerViewHelper;
    PemanenGerdangHelper pemanenGerdangHelper;
    VehicleMekanisasiHelper vehicleMekanisasiHelper;

    TransaksiAngkutHelper transaksiAngkutHelper;

    ArrayList<File> ALselectedImage;
    Long waktuPanen;
    SimpleDateFormat sdfT = new SimpleDateFormat("HH:mm");
    AlertDialog alertDialog;
    File kml;

    Boolean popupTPH = false;
    SelectGangAdapter adapterGang;
    SelectPemanenAdapter adapterPemanen;
    SelectSupervisionAdapter adapterSupervision;

    ArrayList<TPH> allTph;
    SelectTphAdapter adapterTph;
    TPH tphTerdekat;

    ArrayList<TPHnLangsiran> allTPHnLangsiran;
    SelectTphnLangsiranAdapter adapterTPHnLangsiran;
    TPHnLangsiran selectedTpHnLangsiran;
    TPHnLangsiran tpHnLangsiranTerdekat;
    DynamicParameterPenghasilan dynamicParameterPenghasilan;
    GerdangConfig gerdangConfig;
    boolean checkBoxTphBayanganEfect;
    double bjr;
    TransaksiPanen idPanenAngkut;
    TransaksiAngkut idAngkutAngkut;
    ArrayList<SubPemanen> subPemanens;
    HashMap<String,Pemanen> pemanens;

    SPK spkSelected;
    HashMap<String,SPK> allSpk;
    SelectSpkAdapter adapterSpk;

    HasilPanen hasilPanenStamp;
    StampImage stampImage;
    private final String blockCharacterSet = "~#^|$%&*!";
    private static final String TAG = TphInputFragment.class.getSimpleName();
    private int isManual = 0;
    private String blockName;
    HashMap<String,SupervisionList> sSupervisionList;
    int idKongsi = 0;
    HasilPanen sisaJjgBrdl;
    Set<String> idNfc;
    boolean cariTphTerdekat = true;
    String afdelingPanen;
    public HashMap<String, Gerdang> currentGerdang;

    HashMap<String, MekanisasiOperator> currentMekanisasiOperator;

    boolean dariSharePref = false;

    public static TphInputFragment getInstance() {
        Log.i(TAG, "getInstance: no bundle");
        TphInputFragment tphInputFragment = new TphInputFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("isManual", 0);
        tphInputFragment.setArguments(bundle);
        return tphInputFragment;
    }

    /*
     * Existing param isManual
     * 1 = manual
     */
    public static TphInputFragment getInstance(int isManual) {
        Log.i(TAG, "getInstance: WITH bundle");
        Bundle bundle = new Bundle();
        bundle.putInt("isManual", isManual);
        TphInputFragment tphInputFragment = new TphInputFragment();
        tphInputFragment.setArguments(bundle);
        return tphInputFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            if(getArguments().getInt("isManual") != 0){
                isManual = getArguments().getInt("isManual");
            }
        }
        Log.i(TAG, "arguments: "+ getArguments().getInt("isManual")+ " isManual : "+ isManual);
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return MoveAnimation.create(MoveAnimation.UP,enter,600);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tph_input, null, false);
        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
//        if(tphActivity != null) {
//            if (tphActivity.varibaleSavedInstanceState == null) {
//                tphActivity.varibaleSavedInstanceState = etTotalJanjang.getText().toString();
//                outState.putString("T", tphActivity.varibaleSavedInstanceState);
//            }
//            Log.d(TAG, "on savedInstanceState: " + tphActivity.varibaleSavedInstanceState);
//        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: ");

//        if (savedInstanceState != null) {
//            String tJJG = savedInstanceState.getString("T");
//            Log.d(TAG, "savedInstanceState: " + tJJG);
//        }

        waktuPanen = System.currentTimeMillis() - (2*60*60*1000);
        totalJjgJelek = 0;
        totalJjg = 0;
        bjr = 0.0;
        cariTphTerdekat = true;
        afdelingPanen = null;

//        pemanenHelper = new PemanenHelper(getActivity());
        tphHelper = new TphHelper(getActivity());
        transaksiPanenHelper = new TransaksiPanenHelper(getActivity());
        sharedPrefHelper = new SharedPrefHelper(getActivity());
        modelRecyclerViewHelper = new ModelRecyclerViewHelper(getActivity());
        pemanenGerdangHelper = new PemanenGerdangHelper(getActivity());
        vehicleMekanisasiHelper = new VehicleMekanisasiHelper(getActivity());

        baseActivity = ((BaseActivity) getActivity());
        if(getActivity() instanceof  TphActivity) {
            tphActivity = ((TphActivity) getActivity());
            tphActivity.tulisUlangNFC = false;
            if(tphActivity.selectedTransaksiPanen != null) {
                selectedTransaksiPanen = tphActivity.selectedTransaksiPanen;
            }
        }else if(getActivity() instanceof  AngkutActivity) {
            angkutActivity = ((AngkutActivity) getActivity());
            transaksiAngkutHelper = new TransaksiAngkutHelper(getActivity());
            if(angkutActivity.selectedTransaksiPanen != null) {
                selectedTransaksiPanen = angkutActivity.selectedTransaksiPanen;
            }
            if (angkutActivity.selectedAngkut != null){
                selectedAngkut = angkutActivity.selectedAngkut;
            }
        }else if (getActivity() instanceof SpbActivity){
            spbActivity = ((SpbActivity) getActivity());
        }else if (getActivity() instanceof MekanisasiActivity){
            mekanisasiActivity = ((MekanisasiActivity) getActivity());
        }else if (getActivity() instanceof QcMutuBuahActivity){
            qcMutuBuahActivity = ((QcMutuBuahActivity) getActivity());
            if(qcMutuBuahActivity.qcMutuBuah != null) {
                if (qcMutuBuahActivity.qcMutuBuah.getTransaksiPanenKrani() != null) {
                    selectedTransaksiPanen = qcMutuBuahActivity.qcMutuBuah.getTransaksiPanenKrani();
                    if (qcMutuBuahActivity.qcMutuBuah.getTransaksiPanenQc() != null) {
                        selectedTransaksiPanen = qcMutuBuahActivity.qcMutuBuah.getTransaksiPanenQc();
                    }
                    if (selectedTransaksiPanen.getTph().getNoTph() == null) {
                        qcMutuBuahActivity.backProses();
                        qcMutuBuahActivity.searchingGPS = Snackbar.make(qcMutuBuahActivity.myCoordinatorLayout, "Anda Tidak Punya Master TPH Ini", Snackbar.LENGTH_INDEFINITE);
                        qcMutuBuahActivity.searchingGPS.show();
                        return;
                    }
                } else {
                    qcMutuBuahActivity.backProses();
                    qcMutuBuahActivity.searchingGPS = Snackbar.make(qcMutuBuahActivity.myCoordinatorLayout, "Transaksi Panen Tidak Diketahui", Snackbar.LENGTH_INDEFINITE);
                    qcMutuBuahActivity.searchingGPS.show();
                    return;
                }
            }else{
                qcMutuBuahActivity.backProses();
                qcMutuBuahActivity.searchingGPS = Snackbar.make(qcMutuBuahActivity.myCoordinatorLayout, "Qc Panen Tidak Di Ketahui", Snackbar.LENGTH_INDEFINITE);
                qcMutuBuahActivity.searchingGPS.show();
                return;
            }

//            if(!qcMutuBuahActivity.currentLocationOK()){
//                qcMutuBuahActivity.backProses();
//                qcMutuBuahActivity.searchingGPS = Snackbar.make(qcMutuBuahActivity.myCoordinatorLayout,
//                        qcMutuBuahActivity.getResources().getString(R.string.gps_not_accurate),
//                        Snackbar.LENGTH_INDEFINITE);
//                qcMutuBuahActivity.searchingGPS.show();
//                return;
//            }
        }

        tphKrani = (View) view.findViewById(R.id.tphKrani);
        tphphoto = (View) view.findViewById(R.id.tphphoto);
        ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
        imagecacnel = (ImageView) view.findViewById(R.id.imagecacnel);
        imagesview = (ImageView) view.findViewById(R.id.imagesview);
        rl_ipf_takepicture = (RelativeLayout) view.findViewById(R.id.rl_ipf_takepicture);
        sliderLayout = (SliderLayout) view.findViewById(R.id.sliderLayout);

        lBorongan = (LinearLayout) view.findViewById(R.id.lBorongan);
        lKraniInfo = (LinearLayout) view.findViewById(R.id.lKraniInfo);
        lInfoTph = (LinearLayout) view.findViewById(R.id.lInfoTph);
        lInfoSubPemanen = (LinearLayout) view.findViewById(R.id.lInfoSubPemanen);
        pemanenInfo = (View) view.findViewById(R.id.pemanenInfo);
//        lAngkut = (LinearLayout) view.findViewById(R.id.lAngkut);
//        angkutInfo = (View) view.findViewById(R.id.angkutInfo);
        lTph = (LinearLayout) view.findViewById(R.id.lTph);
        lBlock = (LinearLayout) view.findViewById(R.id.lBlock);
        lAncak = (LinearLayout) view.findViewById(R.id.lAncak);
        lTphBayangan = (LinearLayout) view.findViewById(R.id.lTphBayangan);
        lStatus = (LinearLayout) view.findViewById(R.id.lStatus);
        lNoKartu = (LinearLayout) view.findViewById(R.id.lNoKartu);
        etNotph = (CompleteTextViewHelper) view.findViewById(R.id.etNotph);
        etBlock = (CompleteTextViewHelper) view.findViewById(R.id.etBlock);
        etAncak = (AppCompatEditText) view.findViewById(R.id.etAncak);
        cbTphBayangan = (CheckBox) view.findViewById(R.id.cbTphBayangan);
        cbKraniSubstitute = (CheckBox) view.findViewById(R.id.cb_krani_substitute);

        lReasonTphBayangan = (LinearLayout) view.findViewById(R.id.lReasonTphBayangan);
        lsReasonTphBayangan = (BetterSpinner) view.findViewById(R.id.lsReasonTphBayangan);
        etReasonTphBayangan = (AppCompatEditText) view.findViewById(R.id.etReasonTphBayangan);

//        etBaris = (AppCompatEditText) view.findViewById(R.id.etBaris);
//        etGang = (CompleteTextViewHelper) view.findViewById(R.id.etGang);
//        etPemanen = (CompleteTextViewHelper) view.findViewById(R.id.etPemanen);InsertSPB

        cbBorongan = (CheckBox) view.findViewById(R.id.cb_borongan);
        etSpk = (CompleteTextViewHelper) view.findViewById(R.id.etSpk);
        tvOa = (TextView) view.findViewById(R.id.tvOa);

        tipeKongsi = (SegmentedButtonGroup) view.findViewById(R.id.tipeKongsi);
        lSisa = (LinearLayout) view.findViewById(R.id.lSisa);
        rvPemanen = (RecyclerView) view.findViewById(R.id.rvPemanen);
        tvSisa = (TextView) view.findViewById(R.id.tvSisa);
        tvSisa2 = (TextView) view.findViewById(R.id.tvSisa2);
        tvSisa3 = (TextView) view.findViewById(R.id.tvSisa3);
        etPemanenSisa = (CompleteTextViewHelper) view.findViewById(R.id.etPemanenSisa);

        lGerdang = (LinearLayout) view.findViewById(R.id.lGerdang);
        cvGerdang = (CardView) view.findViewById(R.id.cvGerdang);
        cbGerdang = (CheckBox) view.findViewById(R.id.cbGerdang);
        etPemanenGerdang = (CompleteTextViewHelper) view.findViewById(R.id.etPemanenGerdang);
        etGangGerdang = (CompleteTextViewHelper) view.findViewById(R.id.etGangGerdang);
        tvGerdang = (TextView) view.findViewById(R.id.tvGerdang);

        lVehicleMekanisasi = (LinearLayout) view.findViewById(R.id.lVehicleMekanisasi);
        lsKendaraanMekanisasi = (BetterSpinner) view.findViewById(R.id.lsKendaraanMekanisasi);
        etVehicleMekanisasi = (CompleteTextViewHelper) view.findViewById(R.id.etVehicleMekanisasi);

        etSearch = (AppCompatEditText) view.findViewById(R.id.etSearch);
        btnSearch = (Button) view.findViewById(R.id.btnSearch);
        tableview = (TableView) view.findViewById(R.id.tableview);
        tvInfoTph = (TextView) view.findViewById(R.id.tvInfoTph);
        tvTph = (TextView) view.findViewById(R.id.tvTph);

        lNormal = (LinearLayout) view.findViewById(R.id.lNormal);
        lMentah = (LinearLayout) view.findViewById(R.id.lMentah);
        lBusuk = (LinearLayout) view.findViewById(R.id.lBusuk);
        lLewatMatang = (LinearLayout) view.findViewById(R.id.lLewatMatang);
        lAbnormal = (LinearLayout) view.findViewById(R.id.lAbnormal);
        lTangkaiPanjang = (LinearLayout) view.findViewById(R.id.lTangkaiPanjang);
        lBuahDimakanTikus = (LinearLayout) view.findViewById(R.id.lBuahDimakanTikus);
        lBrondolan = (LinearLayout) view.findViewById(R.id.lBrondolan);
        lTotalJanjangBuruk = (LinearLayout) view.findViewById(R.id.lTotalJanjangBuruk);
        lTotalJanjang = (LinearLayout) view.findViewById(R.id.lTotalJanjang);
        lTotalJanjangPendapatan = (LinearLayout) view.findViewById(R.id.lTotalJanjangPendapatan);
        lJarak = (LinearLayout) view.findViewById(R.id.lJarak);
        lRemarks = (LinearLayout) view.findViewById(R.id.lRemarks);
        lAlasBrondol = (LinearLayout) view.findViewById(R.id.lAlasBrondol);
        lSubstitute = (LinearLayout) view.findViewById(R.id.lSubstitute);
//        LinearLayout lInfoPemanen;
//        LinearLayout lTphPanen;
        lInfoPemanen = (LinearLayout) view.findViewById(R.id.lInfoPemanen) ;
        vTphPanen = (View) view.findViewById(R.id.tphPanen);
        vTphphoto = (View) view.findViewById(R.id.tphphoto);
        tvNormal = (TextView) view.findViewById(R.id.tvNormal);
        tvNormalSymbol = (TextView) view.findViewById(R.id.tvNormalSymbol);
        tvMeasureNormal = (TextView) view.findViewById(R.id.tvMeasureNormal);
        tvMentah = (TextView) view.findViewById(R.id.tvMentah);
        tvMentahSymbol = (TextView) view.findViewById(R.id.tvMentahSymbol);
        tvMeasureMentah = (TextView) view.findViewById(R.id.tvMeasureMentah);
        etNormal = (AppCompatEditText) view.findViewById(R.id.etNormal);
        etMentah = (AppCompatEditText) view.findViewById(R.id.etMentah);
        etBusuk = (AppCompatEditText) view.findViewById(R.id.etBusuk);
        etLewatMatang = (AppCompatEditText) view.findViewById(R.id.etLewatMatang);
        etBuahDimakanTikus = (AppCompatEditText) view.findViewById(R.id.etBuahDimakanTikus);
        etAbnormal = (AppCompatEditText) view.findViewById(R.id.etAbnormal);
        etBrondolan = (AppCompatEditText) view.findViewById(R.id.etBrondolan);
        etTangkaiPanjang = (AppCompatEditText) view.findViewById(R.id.etTangkaiPanjang);
        etTotalJanjangBuruk = (AppCompatEditText) view.findViewById(R.id.etTotalJanjangBuruk);
        etTotalJanjang = (AppCompatEditText) view.findViewById(R.id.etTotalJanjang);
        etTotalJanjangPendapatan = (AppCompatEditText) view.findViewById(R.id.etTotalJanjangPendapatan);
        etNoNFC = (AppCompatEditText) view.findViewById(R.id.etNoNFC);
        etJarak = (AppCompatEditText) view.findViewById(R.id.etJarak);
        etRemarks = (AppCompatEditText) view.findViewById(R.id.etRemarks);
        ivTotal = (ImageView) view.findViewById(R.id.ivTotal);
        ivTotalPendapatan = (ImageView) view.findViewById(R.id.ivTotalPendapatan);
        ivNormal = (ImageView) view.findViewById(R.id.ivNormal);
//        ivTph = (ImageView) view.findViewById(R.id.ivTph);
        ivMentah = (ImageView) view.findViewById(R.id.ivMentah);
        ivBusuk = (ImageView) view.findViewById(R.id.ivBusuk);
        ivLewatMatang = (ImageView) view.findViewById(R.id.ivLewatMatang);
        ivAbnormal = (ImageView) view.findViewById(R.id.ivAbnormal);
        ivBuahDimakanTikus = (ImageView) view.findViewById(R.id.ivBuahDimakanTikus);
        ivTangkaiPanjang = (ImageView) view.findViewById(R.id.ivTangkaiPanjang);
        lsAlasBrondol = (BetterSpinner) view.findViewById(R.id.lsAlasBrondol);
        etSubstitute = (CompleteTextViewHelper) view.findViewById(R.id.etSubstitute);
        lsStatus = (BetterSpinner) view.findViewById(R.id.lsStatusSpin);

        lWarning = (LinearLayout) view.findViewById(R.id.lWarning);
        llkeyBoard = (LinearLayout) view.findViewById(R.id.llkeyBoard);
        ketAction1 = (LinearLayout) view.findViewById(R.id.ketAction1);
        ketAction2 = (LinearLayout) view.findViewById(R.id.ketAction2);
        ketAction3 = (LinearLayout) view.findViewById(R.id.ketAction3);

        infoAngkut = (LinearLayout) view.findViewById(R.id.infoAngkut);
        tvTotalAngkut = (TextView) view.findViewById(R.id.tvTotalAngkut);
        tvTotalJanjang = (TextView) view.findViewById(R.id.tvTotalJanjang);
        tvTotalBrondolan = (TextView) view.findViewById(R.id.tvTotalBrondolan);
        tvTotalBeratEstimasi = (TextView) view.findViewById(R.id.tvTotalBeratEstimasi);
//        lnfc = (LinearLayout) view.findViewById(R.id.lnfc);
//        lprint = (LinearLayout) view.findViewById(R.id.lprint);
//        lshare = (LinearLayout) view.findViewById(R.id.lshare);
        historyTransaksi = (LinearLayout) view.findViewById(R.id.historyTransaksi);
        lsave = (LinearLayout) view.findViewById(R.id.lsave);
        lldeleted = (LinearLayout) view.findViewById(R.id.lldeleted);
        lcancel = (LinearLayout) view.findViewById(R.id.lcancel);
//        newTph = (LinearLayout) view.findViewById(R.id.newTph);

        lGerdang.setVisibility(View.GONE);
        lVehicleMekanisasi.setVisibility(View.GONE);
        lAncak.setVisibility(View.GONE);
        historyTransaksi.setVisibility(View.GONE);
        ketAction3.setVisibility(View.GONE);
        checkBoxTphBayanganEfect = true;
        infoAngkut.setVisibility(View.GONE);


        main();
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    alertDialog = WidgetHelper.showWaitingDialog(getActivity(), getResources().getString(R.string.wait));
//                    if(tphActivity != null){
//                        tphActivity.runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                main();
//                                if(alertDialog != null){
//                                    alertDialog.dismiss();
//                                }
//                            }
//                        });
//                    }else if(angkutActivity != null) {
//                        angkutActivity.runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                main();
//                            }
//                        });
//                    }
//                }
//                catch (Exception e) {
//                    //print the error here
//                }
//            }
//        }).start();

    }

    private void main(){
        lJarak.setVisibility(View.GONE);
        lTotalJanjangBuruk.setVisibility(View.GONE);
        lWarning.setVisibility(View.GONE);
        llkeyBoard.setVisibility(View.GONE);
        lldeleted.setVisibility(View.GONE);
        lBorongan.setVisibility(View.GONE);
//        tvOa.setVisibility(View.GONE);
//        etNotph.setFocusable(false);
//        etBlock.setFocusable(false);
//        etGang.setFocusable(false);
//        etPemanen.setFocusable(false);
        etJarak.setFocusable(false);
        etTotalJanjangBuruk.setFocusable(false);
        etTotalJanjangPendapatan.setFocusable(false);

        ALselectedImage = new ArrayList<>();
        List<String> bAlas = new ArrayList<>();
        bAlas.add("Yes");
        bAlas.add("No");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, bAlas);
        lsAlasBrondol.setAdapter(adapter);
        lsAlasBrondol.setText(bAlas.get(1));
        ArrayAdapter<String> adapterStatus = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line,Angkut.LIST_STATUS_KARTU);
        lsStatus.setAdapter(adapterStatus);
        lsStatus.setText(adapterStatus.getItem(0));
        etJarak.setText(sdfT.format(waktuPanen));

        kml = new File(GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK));
        HashMap<String,DynamicParameterPenghasilan> appConfig = GlobalHelper.getAppConfig();
        dynamicParameterPenghasilan = appConfig.get(GlobalHelper.getEstate().getEstCode());

        gerdangConfig = GlobalHelper.getGerdangConfig();

        subPemanens = new ArrayList<>();

        List<String> reasons = ReasonTphBayangan.listReason;
        ArrayAdapter<String> adapterReasons = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, reasons);
        lsReasonTphBayangan.setAdapter(adapterReasons);
        lsReasonTphBayangan.setText(reasons.get(reasons.size() - 1));
        etReasonTphBayangan.setVisibility(View.GONE);

        if(tphActivity != null) {
            mainTph();
            dariSharePref = showSharePref();
            mapSetup();
            etNormal.setFocusable(false);

        }else if(angkutActivity != null){
//            mainAngkut();
//            etNormal.setFocusable(selectedTransaksiPanen == null);
        }else if(qcMutuBuahActivity != null){
            mainQcMutuBuah();
            etNormal.setFocusable(false);
        }else if (spbActivity != null){
            mainSpb();
        }else if (mekanisasiActivity != null){
            mainSpbMekanisais();
        }

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.SUBSTITUTE_KRANI, Context.MODE_PRIVATE);
        String stringSubstituteKrani = preferences.getString(HarvestApp.SUBSTITUTE_KRANI, null);
        if(stringSubstituteKrani != null) {
            Gson gson = new Gson();
            cbKraniSubstitute.setChecked(true);
            substituteSelected = gson.fromJson(stringSubstituteKrani,SupervisionList.class);
            if(substituteSelected != null) {
                etSubstitute.setText(substituteSelected.getNama());
            }else{
                if(adapterSupervision != null) {
                    Log.d(this.getClass()+"stringSubstituteKrani", "count "+ adapterSupervision.getCount());
                    if(adapterSupervision.getCount() > 0) {
                        substituteSelected = adapterSupervision.getItemAt(0);
                        etSubstitute.setText(substituteSelected.getNama());
                    }
                }else{
                    etSubstitute.setText("");
                }
            }
            lSubstitute.setVisibility(View.VISIBLE);
        }else{
            substituteSelected = null;
            lSubstitute.setVisibility(View.GONE);
        }

        if(selectedTransaksiPanen != null){
            setUpValueTransaksiPanen();
            subPemanens = new ArrayList<>();
            if(selectedTransaksiPanen.getKongsi().getSubPemanens() != null){
                subPemanens = selectedTransaksiPanen.getKongsi().getSubPemanens();
            }else if(pemanenSelected != null ) {
                subPemanens.add(new SubPemanen(
                        0, pemanenSelected,100,new HasilPanen()
                ));
            }
        }else if (dariSharePref){

        }else{
            subPemanens.add(new SubPemanen(
                    0, new Pemanen(),100,new HasilPanen()
            ));

//            if(sharedPrefHelper.getName(SharedPrefHelper.KEY_TipeKongsi) != null) {
//                idKongsi = Integer.parseInt(sharedPrefHelper.getName(SharedPrefHelper.KEY_TipeKongsi));
//            }

            if(tipeKongsi != null && tphActivity != null) {
                tipeKongsi.setPosition(tphActivity.flgNewHarvest);
                if (idKongsi == Kongsi.idMekanis) {
                    lVehicleMekanisasi.setVisibility(View.VISIBLE);
                }

                if (tphActivity.flgNewHarvest == Kongsi.idMekanis) {
                    idKongsi = tphActivity.flgNewHarvest;
                    lVehicleMekanisasi.setVisibility(View.VISIBLE);
                }
            }

            lSisa.setVisibility(View.GONE);
        }
        if (selectedAngkut != null){
            setUpValueAngkut();
        }

        subPemanenAdapter = new SubPemanenAdapter(getActivity(),subPemanens);
        rvPemanen.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvPemanen.setAdapter(subPemanenAdapter);

        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                if(tphActivity != null) {
                    if (subPemanenAdapter.subPemanens.size() > 1) {
                        subPemanenAdapter.subPemanens.remove(position);

                        RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(position);
                        View v = viewHolder.itemView;
                        TextView tvValue = v.findViewById(R.id.tvValue);

                        if (!tvValue.getText().toString().isEmpty()) {
                            Gson gson = new Gson();
                            Pemanen pemanenX = gson.fromJson(tvValue.getText().toString(), Pemanen.class);

                            for (int i = 0; i < subPemanenAdapter.pemanenHasBeenSelected.size(); i++) {
                                if (pemanenX.getKodePemanen().equalsIgnoreCase(subPemanenAdapter.pemanenHasBeenSelected.get(i))) {
                                    subPemanenAdapter.pemanenHasBeenSelected.remove(i);
                                    break;
                                }
                            }
                        }

                        subPemanenAdapter.notifyItemRemoved(position);
                        distribusiJanjangSubPemanen(true);
                    }
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Hanya Bisa Diubah Pada Menu Panen",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onLeftClicked(int position) {
                if(tphActivity != null) {
                    for (int i = 0; i < rvPemanen.getAdapter().getItemCount(); i++) {
                        RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
                        if (viewHolder != null) {
                            View view = viewHolder.itemView;
                            EditText etPemanenSub = view.findViewById(R.id.etPemanenSub);
                            TextView tvValue = view.findViewById(R.id.tvValue);

                            if (tvValue.getText().toString().isEmpty()) {
                                Toast.makeText(HarvestApp.getContext(), getActivity().getResources().getString(R.string.chose_pemanen), Toast.LENGTH_SHORT).show();
                                etPemanenSub.requestFocus();
                                return;
                            }
                        }
                    }

                    if (idKongsi == Kongsi.idNone) {
                        Toast.makeText(HarvestApp.getContext(), subPemanenAdapter.subPemanens.size() + " " + getActivity().getResources().getString(R.string.max_pemanen), Toast.LENGTH_SHORT).show();
                        return;
                    } else if (idKongsi == Kongsi.idKelompok) {
                        if (subPemanenAdapter.subPemanens.size() >= GlobalHelper.MAX_PEMANEN_KELOMPOK) {
                            Toast.makeText(HarvestApp.getContext(), subPemanenAdapter.subPemanens.size() + " " + getActivity().getResources().getString(R.string.max_pemanen), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        SubPemanen subPemanen = new SubPemanen(subPemanenAdapter.subPemanens.size(), new Pemanen(), 0, new HasilPanen());
                        subPemanenAdapter.subPemanens.add(subPemanen);
                        subPemanenAdapter.notifyItemInserted(subPemanenAdapter.subPemanens.size());
                    } else if (idKongsi == Kongsi.idMekanis) {
                        if (subPemanenAdapter.subPemanens.size() >= GlobalHelper.MAX_PEMANEN_MEKANIS) {
                            Toast.makeText(HarvestApp.getContext(), subPemanenAdapter.subPemanens.size() + " " + getActivity().getResources().getString(R.string.max_pemanen), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        SubPemanen subPemanen = new SubPemanen(subPemanenAdapter.subPemanens.size(), new Pemanen(), 0, new HasilPanen());
                        subPemanenAdapter.subPemanens.add(subPemanen);
                        subPemanenAdapter.notifyItemInserted(subPemanenAdapter.subPemanens.size());
                    }
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Hanya Bisa Diubah Pada Menu Panen",Toast.LENGTH_SHORT).show();
                }
            }
        },subPemanenAdapter);

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(rvPemanen);

        tipeKongsi.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(int position) {
                if(tphActivity != null) {
                    if(cbBorongan.isChecked()){
                        tipeKongsi.setPosition(idKongsi);
                        Toast.makeText(HarvestApp.getContext(),"Pemanen Borongan Tidak Pilih Pemane",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    idKongsi = position;
                    tipeKongsi.setPosition(idKongsi);
                    sharedPrefHelper.commit(SharedPrefHelper.KEY_TipeKongsi, String.valueOf(position));
                    switch (position) {
                        case Kongsi.idNone: {
                            pemanenOperator = new HashMap<>();
                            break;
                        }
                        case Kongsi.idKelompok: {
                            pemanenOperator = new HashMap<>();
                            break;
                        }
                    }

                    if(position == Kongsi.idNone){
                        if(System.currentTimeMillis() >= gerdangConfig.getDateStart() && System.currentTimeMillis() <= gerdangConfig.getDateEnd()) {
                            lGerdang.setVisibility(View.VISIBLE);
                        }
                        lVehicleMekanisasi.setVisibility(View.GONE);
                        vehicleMekanisasi = null;
                    }else if (position == Kongsi.idMekanis){
                        lGerdang.setVisibility(View.GONE);
                        cbGerdang.setChecked(false);
                        lVehicleMekanisasi.setVisibility(View.VISIBLE);
                        vehicleMekanisasi = new Vehicle();
                    }else {
                        lGerdang.setVisibility(View.GONE);
                        cbGerdang.setChecked(false);
                        lVehicleMekanisasi.setVisibility(View.GONE);
                        vehicleMekanisasi = null;
                    }

                    distribusiJanjangSubPemanen(true);
                }else{
                    tipeKongsi.setPosition(idKongsi);
                    Toast.makeText(HarvestApp.getContext(),"Hanya Bisa Diubah Pada Menu Panen",Toast.LENGTH_SHORT).show();
                }
            }
        });

        etPemanenSisa.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pemanenSisa = adapterPemanen.getItemAt(position);
                etPemanenSisa.setText(pemanenSisa.getNama());
            }
        });

        etPemanenSisa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etPemanenSisa.showDropDown();
            }
        });

        cbBorongan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    new LongOperation(getView()).execute(String.valueOf(LongOperation_Setup_SPK));
                }else{
                    lBorongan.setVisibility(View.GONE);
                    tvOa.setVisibility(View.GONE);
                    pemanenInfo.setVisibility(View.VISIBLE);
                    etSpk.setText("");
                    tvOa.setText("");
                    spkSelected = null;
                }
            }
        });

        etSpk.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                spkSelected = ((SelectSpkAdapter) parent.getAdapter()).getItemAt(position);
                etSpk.setText(spkSelected.getIdSPK());
                tvOa.setText(TransaksiPanenHelper.showSPK(spkSelected));
            }
        });

        etSpk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSpk.showDropDown();
            }
        });

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (baseActivity.currentLocationOK()) {
                    if(ALselectedImage.size() > 3) {
                        Toast.makeText(HarvestApp.getContext(),getActivity().getResources().getString(R.string.warning_take_foto),Toast.LENGTH_LONG).show();
                    }else {
                        if(tphActivity != null && stampImage == null){
//                            etPemanen.requestFocus();
                            Toast.makeText(HarvestApp.getContext(),getActivity().getResources().getString(R.string.please_chose_pemanen),Toast.LENGTH_LONG).show();
                            return;
                        }
                        takePicture();
                    }
                } else {
                    if(tphActivity != null) {
//                        tphActivity.searchingGPS = Snackbar.make(tphActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                        tphActivity.searchingGPS.show();
                        WidgetHelper.warningFindGps(tphActivity,tphActivity.myCoordinatorLayout);
                    }else if(angkutActivity != null){
//                        angkutActivity.searchingGPS = Snackbar.make(angkutActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                        angkutActivity.searchingGPS.show();
                        WidgetHelper.warningFindGps(angkutActivity,angkutActivity.myCoordinatorLayout);
                    }
                }

            }
        });

//        ivTph.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (etBlock.getText().toString().isEmpty()) {
//                    Toast.makeText(HarvestApp.getContext(),getActivity().getResources().getString(R.string.please_chose_block),Toast.LENGTH_SHORT).show();
//                    etBlock.showDropDown();
//                } else {
//                    tphHelper.choseTph(etBlock.getText().toString());
//                }
//            }
//        });
        ivTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoPanenPopUp(infoTotalJanjang);
            }
        });
        ivTotalPendapatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoPanenPopUp(infoTotalJanjangPendapatan);
            }
        });
        ivNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoPanenPopUp(infoNormal);
            }
        });
        ivMentah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoPanenPopUp(infoMentah);
            }
        });
        ivBusuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoPanenPopUp(infoBusuk);
            }
        });
        ivLewatMatang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoPanenPopUp(infoLewatMatang);
            }
        });
        ivAbnormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoPanenPopUp(infoAbnormal);
            }
        });
        ivTangkaiPanjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoPanenPopUp(infoTangkaiPanjang);
            }
        });
        ivBuahDimakanTikus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                infoPanenPopUp(infoBuahDimakanTikus);
            }
        });

//        etNormal.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                setTotalJjg(true);
//                setTotalJjgPendapatan(true);
//            }
//        });
        etMentah.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(tphActivity != null) {
                    setTotalJjgJelek(false);
                    setJjgNormal(true);
                    setTotalJjgPendapatan(true);
                    distribusiJanjangSubPemanen(false);
                    setupHasilPanenStamp();
//                }else if(angkutActivity != null) {
//                    if (etMentah.hasFocus()) {
//                        Log.d("etMentah", s.toString());
//                        if (transaksiAngkutNFC != null) {
//                            getBjrAngkut("etMentah");
//                        } else {
//                            int nilai = (int) (Integer.parseInt(s.toString().isEmpty() ? "0" : s.toString()) / bjr);
//                            etNormal.setText(String.valueOf(nilai));
//                        }
//                    }
                }
            }
        });

        etTotalJanjang.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setJjgNormal(true);
                setTotalJjgPendapatan(true);
                distribusiJanjangSubPemanen(false);
                setupHasilPanenStamp();
            }
        });

        etLewatMatang.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setTotalJjgJelek(false);
                setJjgNormal(true);
                setTotalJjgPendapatan(true);
                distribusiJanjangSubPemanen(false);
                setupHasilPanenStamp();
            }
        });

        etBusuk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setTotalJjgJelek(false);
                setJjgNormal(true);
                setTotalJjgPendapatan(true);
                distribusiJanjangSubPemanen(false);
                setupHasilPanenStamp();
            }
        });

        etTangkaiPanjang.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setTotalJjgJelek(false);
                setJjgNormal(true);
                setTotalJjgPendapatan(true);
                distribusiJanjangSubPemanen(false);
                setupHasilPanenStamp();
            }
        });

        etAbnormal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setTotalJjgJelek(false);
                setJjgNormal(true);
                setTotalJjgPendapatan(true);
                distribusiJanjangSubPemanen(false);
                setupHasilPanenStamp();
            }
        });

        etBuahDimakanTikus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                setTotalJjgJelek(false);
                setJjgNormal(true);
                setTotalJjgPendapatan(true);
                distribusiJanjangSubPemanen(false);
                setupHasilPanenStamp();
            }
        });

        etBrondolan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                if(angkutActivity != null) {
//                    if (etBrondolan.hasFocus()) {
//                        Log.d("etBrondolan", s.toString());
//                        if (transaksiAngkutNFC != null) {
//                            getBjrAngkut("etBrondolan");
//                        }else if(selectedTransaksiPanen != null) {
//                            distribusiJanjangSubPemanen(false);
//                        }
//                    }
//                }else
                if (tphActivity != null){
                    distribusiJanjangSubPemanen(false);
                    setupHasilPanenStamp();
                }
            }
        });

        etNoNFC.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() == 8){
                    if(ValidationUtil.isValidNfc(editable.toString().replaceAll("[^A-Za-z0-9]","").toUpperCase())){
                        etNoNFC.setTextColor(HarvestApp.getContext().getResources().getColor(R.color.Green));
//                        Toast.makeText(getActivity(), "No kartu benar!", Toast.LENGTH_SHORT).show();
                    }else{
                        etNoNFC.setTextColor(HarvestApp.getContext().getResources().getColor(R.color.red_color));
                        Toast.makeText(getActivity(), "Format no kartu salah, mohon periksa kembali", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        lsReasonTphBayangan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(lsReasonTphBayangan.getText().toString().equals(ReasonTphBayangan.nameReasonOthers)){
                    etReasonTphBayangan.setVisibility(View.VISIBLE);
                }else{
                    etReasonTphBayangan.setVisibility(View.GONE);
                }
            }
        });
//        lnfc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(selectedTransaksiPanen == null){
//                    new LongOperation(view).execute(String.valueOf(LongOperation_NFC_With_Save));
//                }else {
//                    if (selectedTransaksiPanen.getStatus() == TransaksiPanen.SAVE_ONLY) {
//                        new LongOperation(view).execute(String.valueOf(LongOperation_NFC_With_Save));
//                    } else {
//                        new LongOperation(view).execute(String.valueOf(LongOperation_NFC_Only));
//                    }
//                }
//            }
//        });
//
//        lprint.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (baseActivity.bluetoothHelper.mService.getState() == BluetoothService.STATE_CONNECTED) {
//                    if (selectedTransaksiPanen == null) {
//                        new LongOperation(view).execute(String.valueOf(LongOperation_Print_With_Save));
//                    } else {
//                        if (selectedTransaksiPanen.getStatus() == TransaksiPanen.SAVE_ONLY) {
//                            new LongOperation(view).execute(String.valueOf(LongOperation_Print_With_Save));
//                            new LongOperation(view).execute(String.valueOf(LongOperation_Print_Only));
//                        } else {
//                            new LongOperation(view).execute(String.valueOf(LongOperation_Print_Only));
//                        }
//                    }
//                } else {
//                    transaksiPanenHelper.printBluetooth(selectedTransaksiPanen);
//                }
//            }
//        });
//
//        lshare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(selectedTransaksiPanen == null) {
//                    new LongOperation(view).execute(String.valueOf(LongOperation_Share_With_Save));
//                }else {
//                    if (selectedTransaksiPanen.getStatus() == TransaksiPanen.SAVE_ONLY) {
//                        new LongOperation(view).execute(String.valueOf(LongOperation_Share_With_Save));
//                    } else {
//                        new LongOperation(view).execute(String.valueOf(LongOperation_Share_Only));
//                    }
//                }
//            }
//        });
//        newTph.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(tphActivity.currentLocationOK()) {
//                    tphActivity.getSupportFragmentManager().beginTransaction()
//                            .replace(R.id.content_container, TphNewInputFragment.getInstance())
//                            .commit();
//                }else{
//                    tphActivity.searchingGPS = Snackbar.make(tphActivity.myCoordinatorLayout,getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
//                    tphActivity.searchingGPS.show();
//                }
//            }
//        });

        lsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isDoubleClick()){
                    return;
                }

                String stringIsi = SharedPrefHelper.getSharePref(HarvestApp.STAMP_IMAGES);
                Gson gson  = new Gson();
                StampImage stampImageX = gson.fromJson(stringIsi,StampImage.class);

                if(tphActivity != null) {
                    Log.i(TAG, "tphActivity not null");

                    if(cbBorongan.isChecked()){
                        if(spkSelected == null){
                            lBorongan.setVisibility(View.VISIBLE);
                            Toast.makeText(HarvestApp.getContext(), "Mohon Pilih SPK Dahulu", Toast.LENGTH_LONG).show();
                            return;
                        }
                    }

                    if(spkSelected == null) {
                        if (pemanenSelected == null) {
//                        etPemanen.requestFocus();
                            Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_chose_pemanen), Toast.LENGTH_LONG).show();
                            return;
                        } else if (pemanenSelected.getKodePemanen() == null) {
//                        etPemanen.requestFocus();
                            Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_chose_pemanen), Toast.LENGTH_LONG).show();
                            return;
                        }
                    }

                    if(cbBorongan.isChecked() && spkSelected == null){
                        Toast.makeText(HarvestApp.getContext(), "Mohon Pilih SPK", Toast.LENGTH_LONG).show();
                        etSpk.requestFocus();
                        return;
                    }else {
                        if(spkSelected != null ) {
                            Pemanen pemanenSpk = SubPemanen.pemanenSPK;
//                            pemanenSpk.setNama(spkSelected.getIdSPK());
                            setPemanen(pemanenSpk);
                        }else if(spkSelected != null && pemanenSelected.getKodePemanen().equals(SubPemanen.pemanenSPK.getKodePemanen())){
                            Pemanen pemanenSpk = SubPemanen.pemanenSPK;
//                            pemanenSpk.setNama(spkSelected.getIdSPK());
                            setPemanen(pemanenSpk);
                        }else{
                            if (!validasiSubPemanen()) {
                                return;
                            }
                        }
                    }

                    if(cbGerdang.isChecked()){
                        Pemanen pemanenUtama = null;
                        for (int i = 0; i < rvPemanen.getAdapter().getItemCount(); i++) {
                            RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
                            View v = viewHolder.itemView;
                            TextView tvValue = v.findViewById(R.id.tvValue);
                            pemanenUtama = gson.fromJson(tvValue.getText().toString(),Pemanen.class);
                            break;
                        }

                        if(idKongsi != Kongsi.idNone){
                            Toast.makeText(HarvestApp.getContext(), "Pemanen Gerdang Hanya Bisa Tidak Kongsi", Toast.LENGTH_LONG).show();
                            return;
                        }else if (rvPemanen.getAdapter().getItemCount() != 1){
                            Toast.makeText(HarvestApp.getContext(), "Pemanen Gerdang Hanya Bisa Satu Pemanen Utama", Toast.LENGTH_LONG).show();
                            return;
                        }else if(pemanenGerdang == null){
                            Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Pemanen Gerdang", Toast.LENGTH_LONG).show();
                            return;
                        }else if (pemanenUtama == null){
                            Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Pemanen", Toast.LENGTH_LONG).show();
                            return;
                        }else if (pemanenUtama.getNik().equals(SubPemanen.pemanenSPK.getNik())){
                            Toast.makeText(HarvestApp.getContext(), "Pemanen Utama Tidak Boleh Pemanen Kontanan", Toast.LENGTH_LONG).show();
                            return;
                        }else if (pemanenGerdang.getNik().equals(pemanenUtama.getNik())){
                            Toast.makeText(HarvestApp.getContext(), "Pemanen Gerdang dan Pemanen Utama Tidak Boleh Sama", Toast.LENGTH_LONG).show();
                            return;
                        }

                        // cek apakah gerdang yang dipilih sudah di ikatkan dengan pemanen utama lain
                        if(currentGerdang.size() > 0){
                            for(String key : currentGerdang.keySet()){
                                Gerdang gerdang = currentGerdang.get(key);
                                if(gerdang != null) {
                                    if (gerdang.getPemanenGerdang().getNik().equals(pemanenGerdang.getNik())) {
                                        if (!gerdang.getPemanenUtama().getNik().equals(pemanenUtama.getNik())) {
                                            Toast.makeText(HarvestApp.getContext(), "Pemanen Gerdang " +gerdang.getPemanenGerdang().getNama()
                                                    +" Sudah Di Ikatkan dengan Pemanen " + gerdang.getPemanenUtama().getNama(), Toast.LENGTH_LONG).show();
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        pemanenGerdang = null;
                        etPemanenGerdang.setText("");
                        tvGerdang.setText("Gang - Nik - Nama");
                    }


                    if(tphSelected == null){
                        etNotph.requestFocus();
                        Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_chose_tph), Toast.LENGTH_LONG).show();
                        return;
                    }

                    if(tphSelected.getEstCode() == null){
                        etNotph.requestFocus();
                        Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_chose_tph), Toast.LENGTH_LONG).show();
                        return;
                    }

                    if(tphSelected.getBlock() == null){
                        etNotph.requestFocus();
                        Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_chose_tph), Toast.LENGTH_LONG).show();
                        return;
                    }

                    if(tphSelected.getNoTph() == null){
                        etNotph.requestFocus();
                        Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_chose_tph), Toast.LENGTH_LONG).show();
                        return;
                    }

                    if(tphSelected.getAncak() == null){
                        etNotph.requestFocus();
                        Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_chose_tph), Toast.LENGTH_LONG).show();
                        return;
                    }

                    if(spkSelected != null){
                        boolean adaBlk = false;
                        if(spkSelected.getBlock() != null) {
                            for (int i = 0; i < spkSelected.getBlock().size(); i++) {
                                if (spkSelected.getBlock().get(i).trim().toUpperCase().equals(tphSelected.getBlock().trim().toUpperCase())) {
                                    adaBlk = true;
                                    break;
                                }
                            }
                        }

                        if(!adaBlk){
                            Toast.makeText(HarvestApp.getContext(), "SPK yang ada pilih tidak bisa di input pada blok ini", Toast.LENGTH_SHORT).show();
                            return ;
                        }
                    }

                    if(cbTphBayangan.isChecked()){
                        lReasonTphBayangan.setVisibility(View.VISIBLE);
                        if(lsReasonTphBayangan.getText().toString().equals(ReasonTphBayangan.nameReasonKosong)){
                            if(etReasonTphBayangan.getText().toString().trim().isEmpty()){
                                Toast.makeText(HarvestApp.getContext(), "Harap Pilih Reason Tph Bayangan", Toast.LENGTH_LONG).show();
                                return;
                            }
                        }
                        if(lsReasonTphBayangan.getText().toString().equals(ReasonTphBayangan.nameReasonOthers)){
                            if(etReasonTphBayangan.getText().toString().trim().isEmpty()){
                                Toast.makeText(HarvestApp.getContext(), "Harap Input Alasan Tph Bayangan", Toast.LENGTH_LONG).show();
                                return;
                            }
                        }
                    }

                    if(etNormal.getText().toString().isEmpty()){
                        etNormal.requestFocus();
                        Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_input_janjang_normal), Toast.LENGTH_LONG).show();
                        return;
                    }

                    if(ALselectedImage.size() == 0){
                        takePicture();
                        Toast.makeText(HarvestApp.getContext(), getActivity().getResources().getString(R.string.please_take_picture), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if(stampImageX.getPemanen() == null){
                        if(pemanenSelected != null) {
                            setPemanen(pemanenSelected);
                            takePicture();
                            Toast.makeText(HarvestApp.getContext(), "Stamp Pemanen Tidak Ketemu", Toast.LENGTH_LONG).show();
                            return;
                        }else{
                            Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_chose_pemanen), Toast.LENGTH_LONG).show();
                            return;
                        }
                    }else if(stampImageX.getTph() == null){
                        if(tphSelected != null) {
                            setTph(tphSelected);
                            takePicture();
                            etNotph.requestFocus();
                            Toast.makeText(HarvestApp.getContext(), "Stamp TPH Tidak Ketemu", Toast.LENGTH_LONG).show();
                            return;
                        }else{
                            Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_chose_tph), Toast.LENGTH_LONG).show();
                            return;
                        }
                    }else if (!stampImageX.getPemanen().getKodePemanen().equals(pemanenSelected.getKodePemanen())) {
                        clearImages();
                        takePicture();
//                        etPemanen.requestFocus();
                        Toast.makeText(HarvestApp.getContext(),"Pemanen Tidak Sama",Toast.LENGTH_LONG).show();
                        return;
                    }else if (!stampImageX.getTph().getNoTph().equals(tphSelected.getNoTph())){
                        clearImages();
                        takePicture();
                        etNotph.requestFocus();
                        Toast.makeText(HarvestApp.getContext(),"Tph Tidak Sama",Toast.LENGTH_LONG).show();
                        return;
                    }else if (stampImageX.getHasilPanen() == null){
                        clearImages();
                        takePicture();
                        etTotalJanjang.requestFocus();
                        Toast.makeText(HarvestApp.getContext(),"stampImage Hasil Panen Kosong Update Total Janjang",Toast.LENGTH_LONG).show();
                        return;
                    }else if (stampImageX.getHasilPanen().getTotalJanjang() != hasilPanenStamp.getTotalJanjang()){
                        clearImages();
                        takePicture();
                        etTotalJanjang.requestFocus();
                        Toast.makeText(HarvestApp.getContext(),"Total Janjang Tidak Sama Dengan Stamp Foto Mohon Foto Ulang",Toast.LENGTH_LONG).show();
                        return;
                    }else if (stampImageX.getHasilPanen().getBuahMentah() != hasilPanenStamp.getBuahMentah()){
                        clearImages();
                        takePicture();
                        etMentah.requestFocus();
                        Toast.makeText(HarvestApp.getContext(),"Buah Mentah Tidak Sama Dengan Stamp Foto Mohon Foto Ulang",Toast.LENGTH_LONG).show();
                        return;
                    }else if (stampImageX.getHasilPanen().getTangkaiPanjang() != hasilPanenStamp.getTangkaiPanjang()){
                        clearImages();
                        takePicture();
                        etTangkaiPanjang.requestFocus();
                        Toast.makeText(HarvestApp.getContext(),"Tangkai Panjang Tidak Sama Dengan Stamp Foto Mohon Foto Ulang",Toast.LENGTH_LONG).show();
                        return;
                    }else if (stampImageX.getHasilPanen().getBrondolan() != hasilPanenStamp.getBrondolan()){
                        clearImages();
                        takePicture();
                        etBrondolan.requestFocus();
                        Toast.makeText(HarvestApp.getContext(),"Brondolan Tidak Sama Dengan Stamp Foto Mohon Foto Ulang",Toast.LENGTH_LONG).show();
                        return;
                    }

                    if(etNormal.getText().toString().equals("")){
                        Toast.makeText(HarvestApp.getContext(), "Mohon Input Janjang Normal", Toast.LENGTH_SHORT).show();
                        return;
                    }else{
                        Integer normal = Integer.parseInt(etNormal.getText().toString());
                        if(normal < 0 ){
                            Toast.makeText(HarvestApp.getContext(), "Janjang Normal "+getActivity().getResources().getString(R.string.canot_minus), Toast.LENGTH_SHORT).show();
                            return;
                        }


//                        //karena jjg normal bisa 0
//                        int deduction = 0;
//                        if(!etTangkaiPanjang.getText().toString().isEmpty() ){
//                            deduction += Integer.parseInt(etTangkaiPanjang.getText().toString());
//                        }
//
//                        if(!etMentah.getText().toString().isEmpty() ){
//                            deduction += Integer.parseInt(etMentah.getText().toString());
//                        }
//
//
//                        if(normal <= deduction){
//                            Toast.makeText(HarvestApp.getContext(), "Buah Mentah dan Tangkai Panjang Tidak Bisa Lebih Dari Janjang Normal ", Toast.LENGTH_SHORT).show();
//                            return;
//                        }
                    }

                    if(etTotalJanjang.getText().toString().equals("")){
                        Toast.makeText(HarvestApp.getContext(), "Mohon Input Total Janjang", Toast.LENGTH_SHORT).show();
                        return;
                    }else{
                        Integer tJanjang = Integer.parseInt(etTotalJanjang.getText().toString());
                        if(tJanjang < 1 ){
                            Toast.makeText(HarvestApp.getContext(), "Total Janjang "+getActivity().getResources().getString(R.string.canot_nol), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if(!etBuahDimakanTikus.getText().toString().isEmpty()){
                            int dimakanTikus = Integer.parseInt(etBuahDimakanTikus.getText().toString());
                            if(tJanjang <= dimakanTikus){
                                Toast.makeText(HarvestApp.getContext(), "Yang Di Makan Tikus Terlalu Banyak"+getActivity().getResources().getString(R.string.canot_nol), Toast.LENGTH_SHORT).show();
                                return;
                            }
                        }

                        int deduction = 0;
                        if(!etTangkaiPanjang.getText().toString().isEmpty() ){
                            deduction += Integer.parseInt(etTangkaiPanjang.getText().toString());
                        }

                        if(!etMentah.getText().toString().isEmpty() ){
                            deduction += Integer.parseInt(etMentah.getText().toString());
                        }


                        if(tJanjang < deduction){
                            Toast.makeText(HarvestApp.getContext(), "Buah Mentah dan Tangkai Panjang Tidak Bisa Lebih Dari Total Janjang ", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }

                    //validasi ini yang melakukan di web closing
                    if(spkSelected != null && rvPemanen.getAdapter().getItemCount() == 1){
                        idKongsi = Kongsi.idNone;
                    }else {
                        for (int i = 0; i < rvPemanen.getAdapter().getItemCount(); i++) {
                            RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
                            View v = viewHolder.itemView;
                            AppCompatEditText etTotalJanjangSubPemanen = v.findViewById(R.id.etTotalJanjangSubPemanen);
                            AppCompatEditText etNormalSubPemanen = v.findViewById(R.id.etNormalSubPemanen);
                            AppCompatEditText etBuahMentahSubPemanen = v.findViewById(R.id.etBuahMentahSubPemanen);
                            AppCompatEditText etBusukSubPemanen = v.findViewById(R.id.etBusukSubPemanen);
                            AppCompatEditText etLewatMatangSubPemanen = v.findViewById(R.id.etLewatMatangSubPemanen);
                            AppCompatEditText etAbnormalSubPemanen = v.findViewById(R.id.etAbnormalSubPemanen);
                            AppCompatEditText etTangkaiPanjangSubPemanen = v.findViewById(R.id.etTangkaiPanjangSubPemanen);
                            AppCompatEditText etBuahDimakanTikusSubPemanen = v.findViewById(R.id.etBuahDimakanTikusSubPemanen);
                            TextView tvPemanen = v.findViewById(R.id.tvPemanen);
                            CompleteTextViewHelper lsTipe = v.findViewById(R.id.lsTipe);
                            if (!lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperatorTonase])) {
                                if (etNormalSubPemanen.getText().toString().equals("")) {
                                    Toast.makeText(HarvestApp.getContext(), "Mohon Input Janjang Normal Pada Informasi Pemanen ", Toast.LENGTH_SHORT).show();
                                    return;
                                } else {
                                    Integer normal = Integer.parseInt(etNormalSubPemanen.getText().toString());
                                    if (normal < 0) {
                                        Toast.makeText(HarvestApp.getContext(), "Janjang Normal Pada Informasi Pemanen " + getActivity().getResources().getString(R.string.canot_minus), Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                }

                                if (etTotalJanjangSubPemanen.getText().toString().equals("")) {
                                    Toast.makeText(HarvestApp.getContext(), "Mohon Input Total Janjang Pada Informasi Pemanen ", Toast.LENGTH_SHORT).show();
                                    return;
                                } else {
                                    Integer tJanjang = Integer.parseInt(etTotalJanjangSubPemanen.getText().toString());
                                    if (tJanjang < 1) {
                                        Toast.makeText(HarvestApp.getContext(), "Total Janjang Pada Informasi Pemanen " + getActivity().getResources().getString(R.string.canot_nol), Toast.LENGTH_SHORT).show();
                                        return;
                                    }

                                    try {

                                        Integer tJanjangX = GlobalHelper.getTotalJanjang(new HasilPanen(
                                                Integer.parseInt(etNormalSubPemanen.getText().toString()),
                                                Integer.parseInt(etBuahMentahSubPemanen.getText().toString()),
                                                Integer.parseInt(etBusukSubPemanen.getText().toString()),
                                                Integer.parseInt(etLewatMatangSubPemanen.getText().toString()),
                                                Integer.parseInt(etAbnormalSubPemanen.getText().toString()),
                                                Integer.parseInt(etTangkaiPanjangSubPemanen.getText().toString()),
                                                Integer.parseInt(etBuahDimakanTikusSubPemanen.getText().toString()),
                                                0, 0, 0, 0
                                        ));

                                        if (!tJanjang.equals(tJanjangX)) {
                                            Toast.makeText(HarvestApp.getContext(), "Total Janjang tidak bisa di kalkulasi pada sub pemanen " + tvPemanen.getText().toString(), Toast.LENGTH_SHORT).show();
                                            return;
                                        }
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }

                                }
                            }
                        }
                    }

                    if(!cekTphDanPemanen()){
                        return;
                    }

                    if (tphActivity.currentLocationOK()) {
                        if(selectedTransaksiPanen == null) {
//                            cek lokasi tph denggan lokasi sekarang
//                            jika terlalu jauh maka tidak bisa save
                            latitude = tphActivity.currentlocation.getLatitude();
                            longitude = tphActivity.currentlocation.getLongitude();
                            double distance = GlobalHelper.distance(tphSelected.getLatitude(), latitude,
                                    tphSelected.getLongitude(), longitude);
                            if (distance > GlobalHelper.RADIUS && !tphSelected.getNamaTph().equals("999")) {
                                Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.radius_to_long_with_tph) +" "+  tphSelected.getNamaTph(), Toast.LENGTH_LONG).show();
                                new LongOperation(getView()).execute(String.valueOf(LongOperation_UpdateLocation_Setup_Point));
                                return;
                            } else {
                                showYesNo("Apakah Anda Yakin Untuk Simpan Data Ini",YesNo_Simpan);
                            }
                        } else {
                            showYesNo("Apakah Anda Yakin Untuk Simpan Data Ini",YesNo_Simpan);
                        }
                    } else {
//                        tphActivity.searchingGPS = Snackbar.make(tphActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                        tphActivity.searchingGPS.show();
                        WidgetHelper.warningFindGps(tphActivity,tphActivity.myCoordinatorLayout);
                    }
                } else if (angkutActivity != null || spbActivity != null || mekanisasiActivity != null){
//                    Log.i(TAG, "angkutActivity not null");
                    if(selectedTransaksiPanen == null){
                        showYesNo("Apakah Anda Yakin Untuk Simpan Data Ini", YesNo_SimpanT2_MANUAL);
                    }else {
                        showYesNo("Apakah Anda Yakin Untuk Simpan Data Ini", YesNo_SimpanT2);
                    }
                } else if(qcMutuBuahActivity != null){
//                    Log.i(TAG, "qcMutuBuahActivity not null");
                    showYesNo("Apakah Anda Yakin Untuk Simpan Data Ini",YesNo_SimpanQc);
                }
            }
        });

        lldeleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(tphActivity != null) {
//                    if (tphActivity.currentLocationOK()) {
//                        latitude = baseActivity.currentlocation.getLatitude();
//                        longitude = baseActivity.currentlocation.getLongitude();
//                        double distance = GlobalHelper.distance(selectedTransaksiPanen.getLatitude(),latitude,
//                                selectedTransaksiPanen.getLongitude(),longitude);
//                        if(distance <= GlobalHelper.RADIUS) {
//                            showYesNo("Apakah Anda Yakin Ingin Menghapus Data Ini",YesNo_Hapus);
//                        }else{
//                            WidgetHelper.warningRadius(tphActivity,tphActivity.myCoordinatorLayout,distance);
//                        }
//                    } else {
////                        tphActivity.searchingGPS = Snackbar.make(tphActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
////                        tphActivity.searchingGPS.show();
//                        WidgetHelper.warningFindGps(tphActivity,tphActivity.myCoordinatorLayout);
//                    }
//                }else if (angkutActivity != null){
//                    showYesNo("Apakah Anda Yakin Ingin Menghapus Data Ini",YesNo_HapusT2);
//                }else if (qcMutuBuahActivity != null){
//                    showYesNo("Apakah Anda Yakin Ingin Menghapus Data Ini",YesNo_HapusQc);
//                }
            }
        });

        lcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showYesNo("Apakah Anda Yakin Ingin Keluar Form",YesNo_Batal);
            }
        });

        llkeyBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalHelper.showKeyboard(getActivity());
            }
        });

        lsStatus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(lsStatus.getAdapter().getItem(i).equals("Kartu Rusak")){
                    lNoKartu.setVisibility(View.VISIBLE);
                }else if(lsStatus.getAdapter().getItem(i).equals("Kartu Hilang")){
                    lNoKartu.setVisibility(View.GONE);
                }else{
                    lNoKartu.setVisibility(View.GONE);
                }
            }
        });
        lsStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.i("lsStatusOnItemSelected", "onItemSelected: "+lsStatus.getText().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.i("lsStatusOnItemSelected", "onItemSelected: "+lsStatus.getText().toString());
            }
        });

        historyTransaksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transaksiAngkutHelper.showHistoryTransaksi(selectedTransaksiAngkut);
            }
        });
    }

    private void mainTph(){

        sSupervisionList = new HashMap<>();
        if(GlobalHelper.dataAllSupervision != null) {
            if (GlobalHelper.dataAllSupervision.size() > 0) {
                for (HashMap.Entry<String, SupervisionList> entry : GlobalHelper.dataAllSupervision.entrySet()) {
                    sSupervisionList.put(entry.getValue().getNik(), entry.getValue());
                }
                adapterSupervision = new SelectSupervisionAdapter(getActivity(), R.layout.row_setting, new ArrayList<SupervisionList>(sSupervisionList.values()));
                etSubstitute.setThreshold(1);
                etSubstitute.setAdapter(adapterSupervision);
                adapterSupervision.notifyDataSetChanged();
            }
        }

        currentGerdang = new HashMap<>();
        if(System.currentTimeMillis() >= gerdangConfig.getDateStart() && System.currentTimeMillis() <= gerdangConfig.getDateEnd()){
            lGerdang.setVisibility(View.VISIBLE);
            cvGerdang.setVisibility(View.GONE);
            pemanenGerdangHelper.setDropdownGangGerdang();
            currentGerdang = pemanenGerdangHelper.getCurrentGerdang(System.currentTimeMillis());
            cbGerdang.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        cvGerdang.setVisibility(View.VISIBLE);
                    }else{
                        cvGerdang.setVisibility(View.GONE);
                    }
                }
            });
        }else{
            lGerdang.setVisibility(View.GONE);
        }

        currentMekanisasiOperator = new HashMap<>();
        currentMekanisasiOperator = vehicleMekanisasiHelper.getCurrentMekanisasiOperator(System.currentTimeMillis());
//
        lsKendaraanMekanisasi.setVisibility(View.VISIBLE);
        vehicleMekanisasiHelper.setDropdownVehicleMekanisasi(dynamicParameterPenghasilan);
//        if(etGang.getText().toString().isEmpty()){
//            etGang.requestFocus();
//            etGang.callOnClick();
//        }else{
//            etPemanen.requestFocus();
//            etPemanen.callOnClick();
//        }

//        Set<String> allBlock = GlobalHelper.getAllBlock(kml);
//        ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
//                (getActivity(),android.R.layout.select_dialog_item, allBlock.toArray(new String[allBlock.size()]));
//
//        etBlock.setThreshold(1);
//        etBlock.setAdapter(adapterBlock);
        etBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateNearestBlock();
            }
        });

        //cek apakah form bisa melakukan edit atau tidak
        // karena unutk data yang sudah di print , tap nfc , dan share maka data tidak bisa di edit lagi
        boolean bisaEdit = true;
        if (selectedTransaksiPanen != null) {
            // data boleh di edit dan hapus jika
            // user yang create T1 baru masih bisa hapus dan edit data asalkan status masih tap dan save only
            // user yang create T1 harus sama denggan user saat ini
            // user yang edit harus tulis ulang pada kartu nfc sebelumnya
            // user yang menghapus data harus tap kartu nfc sebelumnya
            if (transaksiPanenHelper.validasiBolehEdit(selectedTransaksiPanen)) {
                bisaEdit = true;
                tphActivity.tulisUlangNFC = true;
//                lldeleted.setVisibility(View.VISIBLE);

                //cek apakah data masih ada didb
                Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_TPH);
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                Gson gson = new Gson();
                DataNitrit dataNitrit = new DataNitrit(selectedTransaksiPanen.getIdTPanen(),
                        gson.toJson(selectedTransaksiPanen),
                        selectedTransaksiPanen.getTph().getNoTph(),
                        selectedTransaksiPanen.getPemanen().getKodePemanen()
                );
                Cursor<DataNitrit> cursor = repository.find(eq("idDataNitrit", dataNitrit.getIdDataNitrit()));
                boolean tidakAdaDidbLagi = true;
                if (cursor.size() > 0) {
                    tidakAdaDidbLagi = false;
                }
                db.close();

                if (tidakAdaDidbLagi) {
                    Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.not_exists_in_db), Toast.LENGTH_SHORT).show();
                    tphActivity.backProses();
                    return;
                }

                //ini untuk memunculkan lagi adapter blok
                //agar bisa edit blok dan juga tph jika memang masih bisa di edit
                if(tphActivity.nearestBlock != null) {
                    if (tphActivity.nearestBlock.getBlocks().size() > 0) {
                        ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
                                (getActivity(), android.R.layout.select_dialog_item, tphActivity.nearestBlock.getBlocks().toArray(new String[tphActivity.nearestBlock.getBlocks().size()]));

                        etBlock.setThreshold(1);
                        etBlock.setAdapter(adapterBlock);
                    }
                }

//                lnfc.setVisibility(View.GONE);
//                lprint.setVisibility(View.GONE);
//                lshare.setVisibility(View.GONE);
            } else {
                bisaEdit = false;
            }
        }else{
            setNearestBlock();
        }

//            if (tphActivity != null && selectedTransaksiPanen == null) {
//                pemanenHelper.chosePemanen(etGang.getText().toString());
//            }

        if (bisaEdit) {
            etNotph.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!etBlock.getText().toString().isEmpty()){
                        if (baseActivity.currentLocationOK()) {
                            latitude = baseActivity.currentlocation.getLatitude();
                            longitude = baseActivity.currentlocation.getLongitude();
//                                showmapTPH();
                            setDropdownTph();
                        } else {
                            if (tphActivity != null) {
//                                tphActivity.searchingGPS = Snackbar.make(tphActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                                tphActivity.searchingGPS.show();
                                WidgetHelper.warningFindGps(tphActivity,tphActivity.myCoordinatorLayout);
                            } else if (angkutActivity != null) {
//                                angkutActivity.searchingGPS = Snackbar.make(angkutActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                                angkutActivity.searchingGPS.show();
                                WidgetHelper.warningFindGps(angkutActivity,angkutActivity.myCoordinatorLayout);
                            }
                        }
                        etNotph.setText("");
                        etNotph.showDropDown();
                    }
                }
            });

            etNotph.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    adapterTph = (SelectTphAdapter) parent.getAdapter();
                    TPH tph = adapterTph.getItemAt(position);
                    if (baseActivity.currentLocationOK()) {
                        latitude = baseActivity.currentlocation.getLatitude();
                        longitude = baseActivity.currentlocation.getLongitude();

                        Double distance = GlobalHelper.distance(latitude,tph.getLatitude(),longitude,tph.getLongitude());
                        if(distance <= GlobalHelper.RADIUS) {
                            setTph(tph);
                        }else{
                            setDropdownTph();
//                            setTph(tphSelected);
                            Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.radius_to_long_with_tph), Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        if (tphActivity != null) {
                            WidgetHelper.warningFindGps(tphActivity,tphActivity.myCoordinatorLayout);
//                            tphActivity.searchingGPS = Snackbar.make(tphActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                            tphActivity.searchingGPS.show();
                        }
                    }
                }
            });

            etNotph.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus){
                        setTph(tphSelected);
                    }
                }
            });

            etBlock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    etNotph.setText("");
                    blockSelected = etBlock.getText().toString();
                    blockName = etBlock.getText().toString();
                    if (baseActivity.currentLocationOK()) {
                        cariTphTerdekat = false;
                        latitude = baseActivity.currentlocation.getLatitude();
                        longitude = baseActivity.currentlocation.getLongitude();

//                            showmapTPH();
                        setDropdownTph();
                    } else {
                        if (tphActivity != null) {
                            WidgetHelper.warningFindGps(tphActivity,tphActivity.myCoordinatorLayout);
//                            tphActivity.searchingGPS = Snackbar.make(tphActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
////                            if(lsStatus.getVisibility() == View.GONE){
////                                tphActivity.searchingGPS.show();
////                            }
//                            tphActivity.searchingGPS.show();

                        }
                    }

                }
            });
            etBlock.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus){
                        etBlock.setText(blockSelected == "" || blockSelected == null ? "" : blockSelected);
                        blockName = etBlock.getText().toString();
                    }else{
                        updateNearestBlock();
                    }
                }
            });

            cbTphBayangan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (checkBoxTphBayanganEfect) {
                        if (isChecked) {
                            lReasonTphBayangan.setVisibility(View.VISIBLE);
                            if (baseActivity.currentLocationOK()) {
                                if (!etBlock.getText().toString().isEmpty()) {
                                    lTph.setVisibility(View.GONE);
                                    lAncak.setVisibility(View.GONE);
                                    tphSelected = tphHelper.getTphBayangan(GlobalHelper.getEstate().getEstCode(), etBlock.getText().toString());
                                    if(tphSelected != null) {
                                        etBlock.setText(tphSelected.getBlock());
                                        tphSelected.setLatitude(baseActivity.currentlocation.getLatitude());
                                        tphSelected.setLongitude(baseActivity.currentlocation.getLongitude());
                                        etTotalJanjang.requestFocus();
                                        cbTphBayangan.setChecked(isChecked);
                                        if(stampImage != null) {
                                            stampImage.setTph(tphSelected);
                                        }else{
                                            stampImage = new StampImage();
                                            stampImage.setTph(tphSelected);
                                        }
                                    }else{
                                        lTph.setVisibility(View.VISIBLE);
                                        cbTphBayangan.setChecked(false);

                                        etNotph.requestFocus();
                                        Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.tph_imaginary_not_found), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    lTph.setVisibility(View.VISIBLE);
                                    cbTphBayangan.setChecked(false);

                                    etBlock.requestFocus();
                                    Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_chose_block), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                lTph.setVisibility(View.VISIBLE);
                                cbTphBayangan.setChecked(false);

//                                tphActivity.searchingGPS = Snackbar.make(tphActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                                tphActivity.searchingGPS.show();
                                WidgetHelper.warningFindGps(tphActivity,tphActivity.myCoordinatorLayout);
                            }
                        } else {
                            lReasonTphBayangan.setVisibility(View.GONE);
                            lTph.setVisibility(View.VISIBLE);
                            setTph(null);
                        }
                    }
                }
            });

            etJarak.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogFragment dialog = createDialogWithSetters();
                    dialog.show(getFragmentManager(), "TPHInput");
                }
            });

            cbKraniSubstitute.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        lSubstitute.setVisibility(View.VISIBLE);
                    }else{
                        lSubstitute.setVisibility(View.GONE);
                    }
                }
            });

            etSubstitute.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    adapterSupervision = (SelectSupervisionAdapter) parent.getAdapter();
                    SupervisionList supervisionList = adapterSupervision.getItemAt(position);
                    substituteSelected = supervisionList;
                    etSubstitute.setText(supervisionList.getNama());
                }
            });
        } else {
            ivPhoto.setVisibility(View.GONE);
            imagecacnel.setVisibility(View.GONE);
//            etGang.setFocusable(false);
//            etPemanen.setFocusable(false);
            etBlock.setFocusable(false);
            etNotph.setFocusable(false);
            etAncak.setFocusable(false);
//            etBaris.setFocusable(false);
            etMentah.setFocusable(false);
            etBusuk.setFocusable(false);
            etLewatMatang.setFocusable(false);
            etAbnormal.setFocusable(false);
            etTangkaiPanjang.setFocusable(false);
            etTotalJanjangBuruk.setFocusable(false);
            etBuahDimakanTikus.setFocusable(false);
            etJarak.setFocusable(false);
            lsAlasBrondol.setFocusable(false);
//            newTph.setVisibility(View.GONE);

            if (tphActivity != null) {
                etNormal.setFocusable(false);
                etBrondolan.setFocusable(false);
            }
        }

    }
//
//    private void mainAngkut(){
//        tphKrani.setVisibility(View.GONE);
//        lKraniInfo.setVisibility(View.GONE);
//        //cek apakah ini dari tap tableview pada angkutInputFragment Atau bukan
//        //jika dari tap tableview pada angkutInputFragment maka selectedAngkut Tidak null
//        if(selectedAngkut == null) {
//            //cek apakah transaksi panen atau transaksi angkut sudah pernah di input atau belum
//            //jika sudah ada di db maka kembali ke fragment angkutInputFragment
//            boolean sudahAda = false;
//            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT);
//            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
//            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
//            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
//                DataNitrit dataNitrit = (DataNitrit) iterator.next();
//                Gson gson = new Gson();
//                Angkut angkut = gson.fromJson(dataNitrit.getValueDataNitrit(), Angkut.class);
//                if (angkut.getTransaksiPanen() != null && selectedTransaksiPanen != null) {
//                    if (angkut.getTransaksiPanen().getIdTPanen().equals(selectedTransaksiPanen.getIdTPanen())) {
//                        Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.transaksiPanen_has_exists), Toast.LENGTH_LONG).show();
//                        sudahAda = true;
//                        break;
//                    }
//                }
//                if(selectedTransaksiPanen != null) {
//                    for (int i = 0; i < selectedTransaksiPanen.getKongsi().getSubPemanens().size(); i++) {
//                        String oph = TransaksiPanenHelper.getOph(selectedTransaksiPanen.getIdTPanen(), selectedTransaksiPanen.getKongsi().getSubPemanens().get(i).getOph());
//                        if (angkut.getOph().equalsIgnoreCase(oph)) {
//                            Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.transaksiPanen_has_exists), Toast.LENGTH_LONG).show();
//                            sudahAda = true;
//                            break;
//                        }
//                    }
//                }
//            }
//            db.close();
//
//            if(!sudahAda){
//                if(selectedTransaksiPanen != null) {
//                    idPanenAngkut = TransaksiAngkutHelper.cekIdTPanenHasBeenTap(selectedTransaksiPanen.getIdTPanen());
//                    if (idPanenAngkut != null) {
//                        modelRecyclerViewHelper.showSummaryTransaksi(TransaksiAngkutHelper.showHasBeenTap(idPanenAngkut,angkutActivity));
//                        sudahAda = true;
//                    }
//
//                    if(!selectedTransaksiPanen.getTph().getEstCode().equalsIgnoreCase(GlobalHelper.getEstate().getEstCode())){
//                        Toast.makeText(HarvestApp.getContext(),"Bukan Dari Estate Yang Sama",Toast.LENGTH_SHORT).show();
//                        sudahAda = true;
//                    }
//
//                    if(!selectedTransaksiPanen.getEstCode().equalsIgnoreCase(GlobalHelper.getEstate().getEstCode())){
//                        Toast.makeText(HarvestApp.getContext(),"Bukan Dari Estate Yang Sama",Toast.LENGTH_SHORT).show();
//                        sudahAda = true;
//                    }
//
//                    if(selectedTransaksiPanen.getNfcFlagTap() != null) {
//                        if(selectedTransaksiPanen.getNfcFlagTap().getUserTap() != null) {
//                            if (!selectedTransaksiPanen.getNfcFlagTap().getUserTap().equalsIgnoreCase(GlobalHelper.getUser().getUserID())) {
//                                Toast.makeText(HarvestApp.getContext(), "Kartu ini Sudah Pernah Di Tap Angkut Kok Di Tap Lagi", Toast.LENGTH_SHORT).show();
//                                sudahAda = true;
//                            }
//                        }
//                        if(selectedTransaksiPanen.getNfcFlagTap().getCountTap() != 0) {
//                            if (selectedTransaksiPanen.getNfcFlagTap().getCountTap() > 2) {
//                                Toast.makeText(HarvestApp.getContext(), "Kartu ini Sudah Pernah Di Tap Lebih Dari "+ selectedTransaksiPanen.getNfcFlagTap().getCountTap(), Toast.LENGTH_SHORT).show();
//                                sudahAda = true;
//                            }
//                        }
//                        if(selectedTransaksiPanen.getNfcFlagTap().getWaktuTap() != 0) {
//                            SimpleDateFormat sdfD = new SimpleDateFormat("dd MMM yyyy");
//                            String now = sdfD.format(System.currentTimeMillis());
//                            String waktuTap = sdfD.format(selectedTransaksiPanen.getNfcFlagTap().getWaktuTap());
//                            if (!now.equalsIgnoreCase(waktuTap)) {
//                                Toast.makeText(HarvestApp.getContext(), "Kartu Ini Sudah Di Tap Pada "+waktuTap, Toast.LENGTH_SHORT).show();
//                                sudahAda = true;
//                            }
//                        }
//                    }
//                }
//            }
//
//            if (sudahAda) {
//                angkutActivity.backProses();
//            }
//
//            if(angkutActivity.myTag != null) {
//                idNfc = new ArraySet<>();
//                String idNfcS = GlobalHelper.bytesToHexString(angkutActivity.myTag.getId());
//                if (!idNfcS.isEmpty()) {
//                    idNfc.add(idNfcS);
//                }
//                angkutActivity.myTag = null;
//            }
//        }else {
//            idNfc = selectedAngkut.getIdNfc();
//        }
//        //kalau dari angkutactivity maka new tph hide
//        //rule jika berasal dari angkut activity
//        if(baseActivity.currentLocationOK()){
//            if (angkutActivity.searchingGPS != null) {
//                angkutActivity.searchingGPS.dismiss();
//            }
//            longitude = baseActivity.currentlocation.getLongitude();
//            latitude = baseActivity.currentlocation.getLatitude();
//        }
////        else{
////            angkutActivity.searchingGPS = Snackbar.make(angkutActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
////            angkutActivity.searchingGPS.show();
////        }
//
//        infoAngkut.setVisibility(View.VISIBLE);
//        tvTotalAngkut.setText(String.valueOf(angkutActivity.sigmaAngkut.getAngkuts().size()));
//        tvTotalJanjang.setText(String.format("%,d",angkutActivity.sigmaAngkut.getTotalJanjang()) + "Jjg");
//        tvTotalBrondolan.setText(String.format("%,d",angkutActivity.sigmaAngkut.getTotalBrondolan()) + "Kg");
//        tvTotalBeratEstimasi.setText(String.format("%.0f", angkutActivity.sigmaAngkut.getTotalBeratEstimasi()) + "Kg");
//
//        if(angkutActivity.selectedTransaksiPanen == null){
//            Log.i(TAG, "mainAngkut:  selectedTransaksiPanen NULL");
//            lInfoSubPemanen.setVisibility(View.GONE);
//            pemanenInfo.setVisibility(View.GONE);
//            lTph.setVisibility(View.GONE);
//            lBlock.setVisibility(View.GONE);
//            lAncak.setVisibility(View.GONE);
//            lTphBayangan.setVisibility(View.GONE);
//        }else{
//            Log.i(TAG, "mainAngkut: DISINI");
//            ketAction1.setVisibility(View.GONE);
//        }
//
//        // tambahan zendi
//        if(isManual == 1){
//            Log.i(TAG, "isManual 1");
//            lStatus.setVisibility(View.VISIBLE);
//            lNoKartu.setVisibility(View.VISIBLE);
//            lInfoPemanen.setVisibility(View.GONE);
//            vTphPanen.setVisibility(View.GONE);
//            vTphphoto.setVisibility(View.GONE);
//            ketAction2.setVisibility(View.GONE);
//        }else{
//            Log.i(TAG, "isManual 0");
//        }
//
//
////            etNormal.requestFocus();
//        lTotalJanjangBuruk.setVisibility(View.GONE);
//        if(selectedTransaksiPanen == null) {
//            lInfoPemanen.setVisibility(View.GONE);
//            vTphPanen.setVisibility(View.GONE);
//
//            lTph.setVisibility(View.VISIBLE);
//            lBlock.setVisibility(View.VISIBLE);
////                ivTph.setVisibility(View.GONE);
//
//            ((LinearLayout.LayoutParams) etNotph.getLayoutParams()).weight = 0.6f;
//            ((LinearLayout.LayoutParams) etBlock.getLayoutParams()).weight = 0.6f;
//
//            Set<String> allBlock = GlobalHelper.getAllBlock(kml);
//            ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
//                    (getActivity(),android.R.layout.select_dialog_item, allBlock.toArray(new String[allBlock.size()]));
//
//            etBlock.setThreshold(1);
//            etBlock.setAdapter(adapterBlock);
//            try {
//                String [] estAfdBlock = GlobalHelper.getAndAssignBlock(kml,latitude,longitude).split(";");
//                etBlock.setText(estAfdBlock[2]);
////                bjr = getBjrAngkut("");
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//            etBlock.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    etBlock.showDropDown();
//                }
//            });
//
//            etBlock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
////                    if (baseActivity.currentLocationOK()) {
//                    etNotph.setText("");
//                    blockName = etBlock.getText().toString();
//                    if(baseActivity.currentlocation != null) {
//                        latitude = baseActivity.currentlocation.getLatitude();
//                        longitude = baseActivity.currentlocation.getLongitude();
//                        setDropDownTphnLangsiran();
////                        bjr = getBjrAngkut("");
//                    } else {
//                        if (angkutActivity != null) {
//                            if(lsStatus.getVisibility() == View.GONE){
////                                angkutActivity.searchingGPS = Snackbar.make(angkutActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
////                                angkutActivity.searchingGPS.show();
//                                WidgetHelper.warningFindGps(angkutActivity,angkutActivity.myCoordinatorLayout);
//                            }else{
//                                setDropDownTphnLangsiran();
//                            }
//
//                        }
//                    }
//                }
//            });
//
//            etNotph.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    adapterTPHnLangsiran = (SelectTphnLangsiranAdapter) parent.getAdapter();
//                    TPHnLangsiran tpHnLangsiran = adapterTPHnLangsiran.getItemAt(position);
//                    setTphnLangsiran(tpHnLangsiran);
////                    bjr = getBjrAngkut("");
//                }
//            });
//        }else{
//            lBusuk.setVisibility(View.VISIBLE);
//            lLewatMatang.setVisibility(View.VISIBLE);
//            lAbnormal.setVisibility(View.VISIBLE);
//            lTangkaiPanjang.setVisibility(View.VISIBLE);
//            lBuahDimakanTikus.setVisibility(View.VISIBLE);
//            lTotalJanjang.setVisibility(View.VISIBLE);
//            lTotalJanjangPendapatan.setVisibility(View.VISIBLE);
//
//            if(selectedTransaksiPanen.getSubstitute() != null){
//                lKraniInfo.setVisibility(View.VISIBLE);
//                tphKrani.setVisibility(View.VISIBLE);
//                lSubstitute.setVisibility(View.VISIBLE);
//                cbKraniSubstitute.setChecked(true);
//                etSubstitute.setText(selectedTransaksiPanen.getSubstitute().getNama());
//                substituteSelected = selectedTransaksiPanen.getSubstitute();
//            }
//        }
//
//        lJarak.setVisibility(View.GONE);
//        lAlasBrondol.setVisibility(View.GONE);
////            newTph.setVisibility(View.GONE);
////            lnfc.setVisibility(View.GONE);
////            lshare.setVisibility(View.GONE);
////            lprint.setVisibility(View.GONE);
//
//        //fileAngkutSelected sebagai penanda bahwa transaksi panen masih bisa di update atau tidak
//        File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
//        if(fileAngkutSelected.exists()){
//            lWarning.setVisibility(View.VISIBLE);
//            llkeyBoard.setVisibility(View.VISIBLE);
//            lsave.setVisibility(View.VISIBLE);
//        }else{
//            lWarning.setVisibility(View.GONE);
//            llkeyBoard.setVisibility(View.GONE);
//            lsave.setVisibility(View.GONE);
//        }
//
//        blockName = etBlock.getText().toString();
//        if(selectedAngkut!=null){
//            blockName = selectedAngkut.getBlock();
//            blockName = etBlock.getText().toString();
//            if(selectedAngkut.getManualSPBData()!=null){
//                Log.i(TAG, "12345: "+selectedAngkut.getManualSPBData().getTipe()+" etBlock:"+blockName);
//                setDropDownTphnLangsiran();
//            }
//        }
//    }

    private void mainSpb(){
        tphKrani.setVisibility(View.GONE);
        lKraniInfo.setVisibility(View.GONE);
        cbBorongan.setVisibility(View.GONE);

        infoAngkut.setVisibility(View.VISIBLE);
        tvTotalAngkut.setText(String.valueOf(spbActivity.originAngkut.size()));
        tvTotalJanjang.setText(String.format("%,d",spbActivity.tJanjang) + "Jjg");
        tvTotalBrondolan.setText(String.format("%,d",spbActivity.tBrondolan) + "Kg");
        tvTotalBeratEstimasi.setText(String.format("%.0f", spbActivity.tBeratEstimasi) + "Kg");

//        lGang.setVisibility(View.GONE);
//        lPemanen.setVisibility(View.GONE);
        lAncak.setVisibility(View.GONE);
        lTphBayangan.setVisibility(View.GONE);

        lStatus.setVisibility(View.VISIBLE);
        lNoKartu.setVisibility(View.VISIBLE);
        lInfoPemanen.setVisibility(View.GONE);
        vTphPanen.setVisibility(View.GONE);
        vTphphoto.setVisibility(View.GONE);
        ketAction2.setVisibility(View.GONE);
        lInfoSubPemanen.setVisibility(View.GONE);
        pemanenInfo.setVisibility(View.GONE);

        lTotalJanjangBuruk.setVisibility(View.GONE);

        ((LinearLayout.LayoutParams) etNotph.getLayoutParams()).weight = 0.6f;
        ((LinearLayout.LayoutParams) etBlock.getLayoutParams()).weight = 0.6f;

        Set<String> allBlock = GlobalHelper.getAllBlock(kml);
        ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
                (getActivity(),android.R.layout.select_dialog_item, allBlock.toArray(new String[allBlock.size()]));

        etBlock.setThreshold(1);
        etBlock.setAdapter(adapterBlock);
        try {
            String [] estAfdBlock = GlobalHelper.getAndAssignBlock(kml,latitude,longitude).split(";");
            etBlock.setText(estAfdBlock[2]);
//            bjr = getBjrAngkut("");
        }catch (Exception e){
            e.printStackTrace();
        }
        etBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etBlock.showDropDown();
            }
        });

        etBlock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                    if (baseActivity.currentLocationOK()) {
                blockName = etBlock.getText().toString();
                if(baseActivity.currentlocation != null) {
                    latitude = baseActivity.currentlocation.getLatitude();
                    longitude = baseActivity.currentlocation.getLongitude();
                }
                setDropDownTphnLangsiran();
            }
        });

        etNotph.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapterTPHnLangsiran = (SelectTphnLangsiranAdapter) parent.getAdapter();
                TPHnLangsiran tpHnLangsiran = adapterTPHnLangsiran.getItemAt(position);
                setTphnLangsiran(tpHnLangsiran);
            }
        });

        lTotalJanjang.setVisibility(View.GONE);
        lBusuk.setVisibility(View.GONE);
        lLewatMatang.setVisibility(View.GONE);
        lAbnormal.setVisibility(View.GONE);
        lTangkaiPanjang.setVisibility(View.GONE);
        lBuahDimakanTikus.setVisibility(View.GONE);
        lTotalJanjangPendapatan.setVisibility(View.GONE);


        lJarak.setVisibility(View.GONE);
        lAlasBrondol.setVisibility(View.GONE);
        lRemarks.setVisibility(View.GONE);

        lWarning.setVisibility(View.VISIBLE);
        llkeyBoard.setVisibility(View.VISIBLE);
        lsave.setVisibility(View.VISIBLE);

        setDropDownTphnLangsiran();

        blockName = etBlock.getText().toString();
    }

    private void mainSpbMekanisais(){
        tphKrani.setVisibility(View.GONE);
        lKraniInfo.setVisibility(View.GONE);
        cbBorongan.setVisibility(View.GONE);

        infoAngkut.setVisibility(View.VISIBLE);
        tvTotalAngkut.setVisibility(View.GONE);
        tvTotalJanjang.setVisibility(View.GONE);
        tvTotalBrondolan.setVisibility(View.GONE);
        tvTotalBeratEstimasi.setVisibility(View.GONE);

//        lGang.setVisibility(View.GONE);
//        lPemanen.setVisibility(View.GONE);
        lAncak.setVisibility(View.GONE);
        lTphBayangan.setVisibility(View.GONE);

        lStatus.setVisibility(View.VISIBLE);
        lNoKartu.setVisibility(View.VISIBLE);
        lInfoPemanen.setVisibility(View.GONE);
        vTphPanen.setVisibility(View.GONE);
        vTphphoto.setVisibility(View.GONE);
        ketAction2.setVisibility(View.GONE);
        lInfoSubPemanen.setVisibility(View.GONE);
        pemanenInfo.setVisibility(View.GONE);

        lTotalJanjangBuruk.setVisibility(View.GONE);

        ((LinearLayout.LayoutParams) etNotph.getLayoutParams()).weight = 0.6f;
        ((LinearLayout.LayoutParams) etBlock.getLayoutParams()).weight = 0.6f;

        Set<String> allBlock = GlobalHelper.getAllBlock(kml);
        ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
                (getActivity(),android.R.layout.select_dialog_item, allBlock.toArray(new String[allBlock.size()]));

        etBlock.setThreshold(1);
        etBlock.setAdapter(adapterBlock);
        try {
            String [] estAfdBlock = GlobalHelper.getAndAssignBlock(kml,latitude,longitude).split(";");
            etBlock.setText(estAfdBlock[2]);
//            bjr = getBjrAngkut("");
        }catch (Exception e){
            e.printStackTrace();
        }
        etBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etBlock.showDropDown();
            }
        });

        etBlock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                    if (baseActivity.currentLocationOK()) {
                blockName = etBlock.getText().toString();
                if(baseActivity.currentlocation != null) {
                    latitude = baseActivity.currentlocation.getLatitude();
                    longitude = baseActivity.currentlocation.getLongitude();
                }
                setDropDownTphnLangsiran();
            }
        });

        etNotph.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapterTPHnLangsiran = (SelectTphnLangsiranAdapter) parent.getAdapter();
                TPHnLangsiran tpHnLangsiran = adapterTPHnLangsiran.getItemAt(position);
                setTphnLangsiran(tpHnLangsiran);
            }
        });

        lTotalJanjang.setVisibility(View.GONE);
        lBusuk.setVisibility(View.GONE);
        lLewatMatang.setVisibility(View.GONE);
        lAbnormal.setVisibility(View.GONE);
        lTangkaiPanjang.setVisibility(View.GONE);
        lBuahDimakanTikus.setVisibility(View.GONE);
        lTotalJanjangPendapatan.setVisibility(View.GONE);


        lJarak.setVisibility(View.GONE);
        lAlasBrondol.setVisibility(View.GONE);
        lRemarks.setVisibility(View.GONE);

        lWarning.setVisibility(View.VISIBLE);
        llkeyBoard.setVisibility(View.VISIBLE);
        lsave.setVisibility(View.VISIBLE);

        setDropDownTphnLangsiran();

        blockName = etBlock.getText().toString();
    }

    private void mapSetup(){

        graphicsLayer = new GraphicsLayer();
        graphicsLayerTPH = new GraphicsLayer();
        graphicsLayerLangsiran = new GraphicsLayer();
        graphicsLayerText = new GraphicsLayer();

        tphActivity.rlMap.setVisibility(View.VISIBLE);

        baseActivity.mapView.removeAll();
        graphicsLayer.removeAll();
        baseActivity.mapView.setMinScale(250000.0d);
        baseActivity.mapView.setMaxScale(1000.0d);
        baseActivity.mapView.enableWrapAround(true);

        String locTiledMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VKM);
        if(locTiledMap != null){
            File file = new File(locTiledMap);
            if(!file.exists()){
                locTiledMap = null;
            }
        }

        if(locTiledMap != null){

            locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
            if (locKmlMap != null) {
                kmlLayer = new KmlLayer(locKmlMap);
//                kmlLayer.setOpacity(0.03f);
                baseActivity.mapView.addLayer(kmlLayer);
                kmlLayer.setVisible(false);
            }
            tiledLayer = new ArcGISLocalTiledLayer(locTiledMap, true);
            baseActivity.mapView.addLayer(tiledLayer);
        }else{
            locTiledMap = GlobalHelper.getFilePath(GlobalHelper.TYPE_VKM, GlobalHelper.getEstate());
            if (locTiledMap != null) {
                locTiledMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VKM);
                tiledLayer = new ArcGISLocalTiledLayer(locTiledMap, true);
                baseActivity.mapView.addLayer(tiledLayer);
            }

            locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
            if(locKmlMap!=null){
                kmlLayer = new KmlLayer(locKmlMap);
//                kmlLayer.setOpacity(0.8f);
                baseActivity.mapView.addLayer(kmlLayer);
                kmlLayer.setVisible(true);
            }
        }

        baseActivity.mapView.setOnStatusChangedListener(new OnStatusChangedListener() {
            @Override
            public void onStatusChanged(Object o, STATUS status) {
                if(o==baseActivity.mapView && status==STATUS.INITIALIZED) {
                    spatialReference = baseActivity.mapView.getSpatialReference();
                    isMapLoaded = true;
                    locationListenerSetup();
                    if (!dariSharePref) {
                        new LongOperation(getView()).execute(String.valueOf(LongOperation_Setup_Point));
                    }
                }
            }
        });

        baseActivity.zoomToLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTph(null);
                if(baseActivity.mapView!=null) {
                    if (baseActivity.mapView.isLoaded()) {
                        if (baseActivity.currentlocation != null || locationDisplayManager != null) {
                            if (locationDisplayManager.getLocation() != null) {
                                zoomToLocation(locationDisplayManager.getLocation().getLatitude(), locationDisplayManager.getLocation().getLongitude(), spatialReference);
                            }
                        }
                    }
                }
                new LongOperation(getView()).execute(String.valueOf(LongOperation_UpdateLocation_Setup_Point));
            }
        });

        graphicsLayer.setMinScale(150000d);
        graphicsLayer.setMaxScale(1000d);
        graphicsLayerTPH.setMinScale(150000d);
        graphicsLayerTPH.setMaxScale(1000d);
        graphicsLayerLangsiran.setMinScale(150000d);
        graphicsLayerLangsiran.setMaxScale(1000d);
        graphicsLayerText.setMinScale(70000d);
        graphicsLayerText.setMaxScale(1000d);
        baseActivity.mapView.addLayer(graphicsLayer);
        baseActivity.mapView.addLayer(graphicsLayerTPH);
        baseActivity.mapView.addLayer(graphicsLayerLangsiran);
        baseActivity.mapView.addLayer(graphicsLayerText);
        baseActivity.mapView.invalidate();
    }

    public void locationListenerSetup(){

        if(baseActivity.mapView!=null && baseActivity.mapView.isLoaded()){

            locationDisplayManager = baseActivity.mapView.getLocationDisplayManager();
            locationDisplayManager.setAllowNetworkLocation(false);
            locationDisplayManager.setAccuracyCircleOn(true);
            locationDisplayManager.setLocationListener(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    baseActivity.currentlocation = location;
//                    zoomToLocation(location.getLatitude(), location.getLongitude(), spatialReference);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("GPS anda tidak hidup! Apakah anda ingin menghidupkan GPS?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();

                                }
                            });
                    final AlertDialog alert = builder.create();
                    alert.show();

                }


            });
            locationDisplayManager.start();
        }
    }

    public void setupPoint(){

        Set<String> block = new ArraySet<>();
        if(tphActivity.nearestBlock == null){
            block.add(etBlock.getText().toString());
            tphActivity.nearestBlock = new NearestBlock(null,
                    etBlock.getText().toString(),
                    block);
        }
        ArrayList<TPHnLangsiran> tphArrayList = tphHelper.setDataTphnLangsiran(GlobalHelper.getEstate().getEstCode(),tphActivity.nearestBlock.getBlocks().toArray(new String[tphActivity.nearestBlock.getBlocks().size()]));
        for(TPHnLangsiran tpHnLangsiran: tphArrayList){
            if(tpHnLangsiran.getLangsiran() != null){
                addGraphicLangsiran(tpHnLangsiran);
            }else if (tpHnLangsiran.getTph() != null){
                if(tpHnLangsiran.getTph().getStatus() == TPH.STATUS_TPH_MASTER) {
                    addGraphicTph(tpHnLangsiran);
                }
            }
        }
        if(tphActivity.currentlocation != null) {
            latitude = tphActivity.currentlocation.getLatitude();
            longitude = tphActivity.currentlocation.getLongitude();

            if (selectedTransaksiPanen != null) {
                addGrapTemp(selectedTransaksiPanen.getLatitude(), selectedTransaksiPanen.getLongitude());
            }

            zoomToLocation(latitude, longitude, spatialReference);
        }
    }

    public void zoomToLocation(double lat, double lon, SpatialReference spatialReference) {
        Log.d(TAG, "zoomToLocation: "+lat +":"+lon);
        Point mapPoint = GeometryEngine.project(lon,lat,spatialReference);
        try {
            baseActivity.mapView.centerAt(mapPoint,true);
            baseActivity.mapView.zoomTo(mapPoint,15000f);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void addGraphicTph(TPHnLangsiran tpHnLangsiran){

        Point fromPoint = new Point(tpHnLangsiran.getTph().getLongitude(), tpHnLangsiran.getTph().getLatitude());
        Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

        CompositeSymbol cms = new CompositeSymbol();
        int color = tpHnLangsiran.getTph().getResurvey() == 1 ? TPHnLangsiran.COLOR_TPH_RESURVEY : TPHnLangsiran.COLOR_TPH;
        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(color, Utils.convertDpToPx(tphActivity, GlobalHelper.MARKER_SIZE_TRANSKASI), SimpleMarkerSymbol.STYLE.CIRCLE);
        SimpleLineSymbol sls = new SimpleLineSymbol(TPHnLangsiran.COLOR_BELUMUPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        if(tpHnLangsiran.getTph().getStatus() == TPH.STATUS_TPH_MASTER || tpHnLangsiran.getTph().getStatus() == TPH.STATUS_TPH_UPLOAD){
            sls = new SimpleLineSymbol(TPHnLangsiran.COLOR_UPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        }
        sms.setOutline(sls);
        cms.add(sms);

        Graphic pointGraphic = new Graphic(toPoint, cms);
        graphicsLayerTPH.addGraphic(pointGraphic);

        TextSymbol.HorizontalAlignment horizontalAlignment = TextSymbol.HorizontalAlignment.CENTER;
        TextSymbol.VerticalAlignment verticalAlignment = TextSymbol.VerticalAlignment.BOTTOM;

        int Tcolor = Color.parseColor("#000000");
        TextSymbol txtSymbol = new TextSymbol(GlobalHelper.TEXT_SIZE_TRANSKASI,tpHnLangsiran.getTph().getNamaTph(),Tcolor) ;
        txtSymbol.setHorizontalAlignment(horizontalAlignment);
        txtSymbol.setVerticalAlignment(verticalAlignment);

        pointGraphic = new Graphic(toPoint,txtSymbol);
        graphicsLayerText.addGraphic(pointGraphic);
        Log.d(TAG, "addGraphicTph: "+ tpHnLangsiran.getTph().getNamaTph());
    }

    public void addGraphicLangsiran(TPHnLangsiran tpHnLangsiran){
        Point fromPoint = new Point(tpHnLangsiran.getLangsiran().getLongitude(), tpHnLangsiran.getLangsiran().getLatitude());
        Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

        CompositeSymbol cms = new CompositeSymbol();
        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(TPHnLangsiran.COLOR_LANGSIRAN, Utils.convertDpToPx(tphActivity, GlobalHelper.MARKER_SIZE_TRANSKASI), SimpleMarkerSymbol.STYLE.CIRCLE);
        SimpleLineSymbol sls = new SimpleLineSymbol(TPHnLangsiran.COLOR_BELUMUPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        if(tpHnLangsiran.getLangsiran().getStatus() == Langsiran.STATUS_LANGSIRAN_MASTER || tpHnLangsiran.getLangsiran().getStatus() == Langsiran.STATUS_LANGSIRAN_UPLOAD){
            sls = new SimpleLineSymbol(TPHnLangsiran.COLOR_UPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        }
        sms.setOutline(sls);
        cms.add(sms);
        Graphic pointGraphic = new Graphic(toPoint, cms);
        graphicsLayerLangsiran.addGraphic(pointGraphic);

        TextSymbol.HorizontalAlignment horizontalAlignment = TextSymbol.HorizontalAlignment.CENTER;
        TextSymbol.VerticalAlignment verticalAlignment = TextSymbol.VerticalAlignment.BOTTOM;

        int Tcolor = Color.parseColor("#000000");
        TextSymbol txtSymbol = new TextSymbol(GlobalHelper.TEXT_SIZE_TRANSKASI,tpHnLangsiran.getLangsiran().getNamaTujuan(),Tcolor) ;
        txtSymbol.setHorizontalAlignment(horizontalAlignment);
        txtSymbol.setVerticalAlignment(verticalAlignment);

        pointGraphic = new Graphic(toPoint,txtSymbol);
        graphicsLayerText.addGraphic(pointGraphic);
        Log.d(TAG, "addGraphicLangsiran: "+ tpHnLangsiran.getLangsiran().getNamaTujuan());
    }

    public void addGrapTemp(Double lat, Double lon){
        if(baseActivity.mapView!=null && baseActivity.mapView.isLoaded()){
            Point fromPoint = new Point(lon, lat);
            Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

            CompositeSymbol cms = new CompositeSymbol();
            SimpleMarkerSymbol sms = new SimpleMarkerSymbol(Color.GREEN, Utils.convertDpToPx(tphActivity, GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.X);
            SimpleLineSymbol sls = new SimpleLineSymbol(Color.GREEN, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
            sms.setOutline(sls);
            cms.add(sms);
            Graphic pointGraphic = new Graphic(toPoint, cms);
            graphicsLayer.addGraphic(pointGraphic);
            Log.d(TAG, "addGrapTemp: "+lat + ":"+ lon);
        }
    }

    public int cekPersentase(){
        int max = 100;
        int persen = 0;
        for(int i = 0; i < rvPemanen.getAdapter().getItemCount(); i++){
            RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
            if(viewHolder != null) {
                View view = viewHolder.itemView;
                AppCompatEditText etPersentase = view.findViewById(R.id.etPersentase);

                if (!etPersentase.getText().toString().isEmpty()) {
                    persen += Integer.parseInt(etPersentase.getText().toString());
                }
            }else{
                SubPemanen subPemanen = subPemanens.get(i);
                persen += subPemanen.getPersentase();
            }
        }

        if(persen <= max){
            return 0;
        }else{
            return persen - max ;
        }
    }

    public void distribusiJanjangSubPemanen(boolean setupAwal){
        HasilPanen hasilPanenX = new HasilPanen();
//        int totalBrondolan = 0;
//        int buahMentah = 0;
//        int tangkaiPanjang = 0;

        if(!etNormal.getText().toString().isEmpty()){
            hasilPanenX.setJanjangNormal(Integer.parseInt(etNormal.getText().toString()));
        }
        if(!etMentah.getText().toString().isEmpty()) {
            hasilPanenX.setBuahMentah(Integer.parseInt(etMentah.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etBusuk.getText().toString().isEmpty()) {
            hasilPanenX.setBusukNJangkos(Integer.parseInt(etBusuk.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etLewatMatang.getText().toString().isEmpty()) {
            hasilPanenX.setBuahLewatMatang(Integer.parseInt(etLewatMatang.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etAbnormal.getText().toString().isEmpty()) {
            hasilPanenX.setBuahAbnormal(Integer.parseInt(etAbnormal.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etTangkaiPanjang.getText().toString().isEmpty()) {
            hasilPanenX.setTangkaiPanjang(Integer.parseInt(etTangkaiPanjang.getText().toString()));
//            tangkaiPanjang = Integer.parseInt(etTangkaiPanjang.getText().toString());
        }
        if(!etBuahDimakanTikus.getText().toString().isEmpty()) {
            hasilPanenX.setBuahDimakanTikus(Integer.parseInt(etBuahDimakanTikus.getText().toString()));
//            totalJjg = Integer.parseInt(etTotalJanjang.getText().toString());
        }
        if(!etBrondolan.getText().toString().isEmpty()) {
            hasilPanenX.setBrondolan(Integer.parseInt(etBrondolan.getText().toString()));
//            totalBrondolan = Integer.parseInt(etBrondolan.getText().toString());
        }
        if(!etTotalJanjangPendapatan.getText().toString().isEmpty()) {
            hasilPanenX.setTotalJanjangPendapatan(Integer.parseInt(etTotalJanjangPendapatan.getText().toString()));
//            totalJjgPendapatan = Integer.parseInt(etTotalJanjangPendapatan.getText().toString());
        }
        if(!etTotalJanjang.getText().toString().isEmpty()) {
            hasilPanenX.setTotalJanjang(Integer.parseInt(etTotalJanjang.getText().toString()));
//            totalJjg = Integer.parseInt(etTotalJanjang.getText().toString());
        }

        Log.d(TAG, "distribusiJanjangSubPemanen: ------------------------------------------------------------------ ");
        Log.d(TAG, "distribusiJanjangSubPemanen: Normal "+ String.valueOf(hasilPanenX.getJanjangNormal()));
        Log.d(TAG, "distribusiJanjangSubPemanen: Mentah "+ String.valueOf(hasilPanenX.getBuahMentah()));
        Log.d(TAG, "distribusiJanjangSubPemanen: Busuk "+ String.valueOf(hasilPanenX.getBusukNJangkos()));
        Log.d(TAG, "distribusiJanjangSubPemanen: Lewat "+ String.valueOf(hasilPanenX.getBuahLewatMatang()));
        Log.d(TAG, "distribusiJanjangSubPemanen: Abnormal "+ String.valueOf(hasilPanenX.getBuahAbnormal()));
        Log.d(TAG, "distribusiJanjangSubPemanen: ------------------------------------------------------------------ ");

        SubPemanen totalDistribusi = new SubPemanen(new HasilPanen(),SubPemanen.idPemnaen);
        ArrayList<SubPemanen> subPemanensX = subPemanens;
        pemanens = new HashMap<>();
        subPemanens = new ArrayList<>();
        pemanenOperator = new HashMap<>();
        pemanenOperatorTonase = new HashMap<>();
        int operatorCount = 0;
        int operatorCountTonase = 0;
        int pemanenCount = 0;

        int nilaiPersenPemanen = 0;
        int nilaiPersenOperator = 0;
        int maxPersenPemanen = 100;
        ArrayList<Integer> intPersenPemanen = new ArrayList<Integer>();
        ArrayList<Integer> intPersenOperator = new ArrayList<Integer>();
        ArrayList<Integer> intPersenOperatorTonase = new ArrayList<Integer>();
        if(tipeKongsi.getPosition() == Kongsi.idMekanis) {
            maxPersenPemanen = dynamicParameterPenghasilan.getPersenPemanen();
            for (int i = 0; i < rvPemanen.getAdapter().getItemCount(); i++) {
                RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
                if (viewHolder != null) {
                    View view = viewHolder.itemView;
                    CompleteTextViewHelper lsTipe = view.findViewById(R.id.lsTipe);
                    TextView tvValue = view.findViewById(R.id.tvValue);
                    if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperator])) {
                        Gson gson = new Gson();
                        Pemanen pemanen = gson.fromJson(tvValue.getText().toString(), Pemanen.class);
                        if(pemanen != null){
                            pemanenOperator.put(pemanen.getKodePemanen(), pemanen);
                        }
                    }else if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperatorTonase])){
                        Gson gson = new Gson();
                        Pemanen pemanen = gson.fromJson(tvValue.getText().toString(), Pemanen.class);
                        if(pemanen!= null){
                            pemanenOperatorTonase.put(pemanen.getKodePemanen(), pemanen);
                        }
                    }
                }
            }

            if(pemanenOperatorTonase.size() > 0 && pemanenOperator.size() == 0){
                maxPersenPemanen = 100;
            }
        }

        for(int i = 0 ; i < rvPemanen.getAdapter().getItemCount(); i ++){
            RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
            if (viewHolder != null) {
                View view = viewHolder.itemView;
                CompleteTextViewHelper lsTipe = view.findViewById(R.id.lsTipe);
                if(pemanenOperator.size() > 0 || pemanenOperatorTonase.size() > 0){
                    if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperator])) {
                        int persen = 0;
                        if (dynamicParameterPenghasilan.getPersenOperator() > 0 && pemanenOperator.size() > 0) {
                            persen = (int) Math.floor(dynamicParameterPenghasilan.getPersenOperator() / pemanenOperator.size());
                        }

                        intPersenOperator.add(persen);
                        nilaiPersenOperator += persen;
                    } else if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperatorTonase])) {
                        intPersenOperatorTonase.add(0);
                    } else {
                        int pemanenSize = rvPemanen.getAdapter().getItemCount() - pemanenOperator.size() - pemanenOperatorTonase.size();
                        int persen = 0;
                        if (pemanenSize > 0) {
                            persen = (int) Math.floor(maxPersenPemanen / pemanenSize);
                        }
                        intPersenPemanen.add(persen);
                        nilaiPersenPemanen += persen;
                    }
                }else{
                    int pemanenSize = rvPemanen.getAdapter().getItemCount();
                    int persen = 0;
                    if (pemanenSize > 0) {
                        persen = (int) Math.floor(maxPersenPemanen / pemanenSize);
                    }
                    intPersenPemanen.add(persen);
                    nilaiPersenPemanen += persen;
                }
            }
        }

        if(intPersenOperator.size() > 0) {
            if (nilaiPersenOperator < dynamicParameterPenghasilan.getPersenOperator()) {
                int jmlOperator = intPersenOperator.size();
                if(intPersenOperatorTonase.size() > 0){
                    jmlOperator = intPersenOperator.size() - intPersenOperatorTonase.size();
                }

                ArrayList<Integer> intPersenOperatorFix = new ArrayList<Integer>();
                int sisa = dynamicParameterPenghasilan.getPersenOperator() - nilaiPersenOperator;
                int ditriButSisa = (int) Math.ceil(sisa / Double.parseDouble(String.valueOf(jmlOperator)));
                for (int i = 0; i < jmlOperator; i++) {
                    intPersenOperatorFix.add(i, intPersenOperator.get(i) + ditriButSisa);
                    sisa -= ditriButSisa;

                    if (sisa == 0) {
                        break;
                    } else if (ditriButSisa > sisa) {
                        ditriButSisa = sisa;
                    }
                }

                intPersenOperator = intPersenOperatorFix;
            }
        }

        if(intPersenPemanen.size() > 0) {
            if (nilaiPersenPemanen < maxPersenPemanen) {
                int sisa = maxPersenPemanen - nilaiPersenPemanen;
                int ditriButSisa = (int) Math.ceil(sisa / Double.parseDouble(String.valueOf(intPersenPemanen.size())));
                for (int i = 0; i < intPersenPemanen.size(); i++) {
                    intPersenPemanen.set(i, intPersenPemanen.get(i) + ditriButSisa);
                    sisa -= ditriButSisa;

                    if (sisa == 0) {
                        break;
                    } else if (ditriButSisa > sisa) {
                        ditriButSisa = sisa;
                    }
                }
            }
        }

        int idxPemanen = 0 ;
        int idxOperator = 0;
        int idxOperatorTonase = 0;

        for(int i = 0; i < rvPemanen.getAdapter().getItemCount(); i++){
            RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
            int tipePemanen = SubPemanen.idPemnaen;
            if(viewHolder != null) {
                View view = viewHolder.itemView;

                AppCompatEditText etPersentase = view.findViewById(R.id.etPersentase);
                TextView tvValue = view.findViewById(R.id.tvValue);
                CompleteTextViewHelper lsTipe = view.findViewById(R.id.lsTipe);

                Gson gson = new Gson();
                HasilPanen hasilPanenSubPemanen = new HasilPanen();
//                int totalJanjangSub = 0;
//                int totalBrodolanSub = 0;
//                int totalJanjangPdtSub = 0;
//                int buahMentahSub = 0;
//                int tangkaiPanjangSub = 0;

                Pemanen pemanen = gson.fromJson(tvValue.getText().toString(), Pemanen.class);
                //agar 1 pemanen bisa ot dan oj dan p sekaligus
                if(lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperator])){
                    operatorCount ++;
                    tipePemanen = SubPemanen.idOperator;
                }else if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperatorTonase])){
                    operatorCountTonase ++;
                    tipePemanen = SubPemanen.idOperatorTonase;
                }
//                validasi agar 1 pemanen tidak bisa ot dan oj dan p sekaligus
//                boolean operatorPosision = false;
//                if(pemanen != null && (pemanenOperator.size() > 0 || pemanenOperatorTonase.size() > 0)) {
//                    if(pemanenOperator.size() > 0){
//                        if (pemanenOperator.get(pemanen.getKodePemanen()) != null) {
//                            operatorPosision = true;
//                            operatorCount ++;
//                            tipePemanen = SubPemanen.idOperator;
//                        }
//                    }
//                    if (pemanenOperatorTonase.size() > 0){
//                        if (pemanenOperatorTonase.get(pemanen.getKodePemanen()) != null) {
//                            operatorCountTonase ++;
//                            tipePemanen = SubPemanen.idOperatorTonase;
//                        }
//                    }
//                }

                if(tipePemanen == SubPemanen.idPemnaen){
                    pemanenCount++;
                }

                if(setupAwal){
//                    if(idKongsi == Kongsi.idMekanis){
                        if(tipePemanen == SubPemanen.idPemnaen){
                            if(intPersenPemanen.size() > 0) {
                                if(idxPemanen < intPersenPemanen.size()) {
                                    etPersentase.setText(String.valueOf(intPersenPemanen.get(idxPemanen)));
                                }
                                idxPemanen++;
                            }else{
                                etPersentase.setText(String.valueOf(0));
                            }
                        } else if (tipePemanen == SubPemanen.idOperatorTonase){
                            idxOperatorTonase++;
                            etPersentase.setText(String.valueOf(0));
                        }else {
                            if (intPersenOperator.size() > 0) {
                                if(idxOperator < intPersenOperator.size()) {
                                    etPersentase.setText(String.valueOf(intPersenOperator.get(idxOperator)));
                                }
                                idxOperator++;
                            }else{
                                etPersentase.setText(String.valueOf(0));
                            }
                        }
//                    }else {
//                        etPersentase.setText(String.valueOf(SubPemanenHelper.getPersentase(
//                                dynamicParameterPenghasilan,
//                                rvPemanen.getAdapter().getItemCount(),
//                                i,
//                                idKongsi,
//                                operatorPosision,
//                                pemanenOperator.size(),
//                                operatorCount, pemanenCount
//                        )));
//                    }
                }

                if (!etPersentase.getText().toString().isEmpty()) {
                    if (etPersentase.getText().toString().matches(".*\\d.*")) {
                        if (totalDistribusi.getHasilPanen().getTotalJanjang() <= hasilPanenX.getTotalJanjang() || totalDistribusi.getHasilPanen().getBrondolan() <= hasilPanenX.getBrondolan()) {
                            int persentase = Integer.parseInt(etPersentase.getText().toString());

                            hasilPanenSubPemanen.setJanjangNormal((int) Math.floor((float) (hasilPanenX.getJanjangNormal() * persentase) / 100));
                            hasilPanenSubPemanen.setBuahMentah((int) Math.floor((float) (hasilPanenX.getBuahMentah() * persentase) / 100));
                            hasilPanenSubPemanen.setBusukNJangkos((int) Math.floor((float) (hasilPanenX.getBusukNJangkos() * persentase) / 100));
                            hasilPanenSubPemanen.setBuahLewatMatang((int) Math.floor((float) (hasilPanenX.getBuahLewatMatang() * persentase) / 100));
                            hasilPanenSubPemanen.setBuahAbnormal((int) Math.floor((float) (hasilPanenX.getBuahAbnormal() * persentase) / 100));
                            hasilPanenSubPemanen.setTangkaiPanjang((int) Math.floor((float) (hasilPanenX.getTangkaiPanjang() * persentase) / 100));
                            hasilPanenSubPemanen.setBuahDimakanTikus((int) Math.floor((float) (hasilPanenX.getBuahDimakanTikus() * persentase) / 100));
                            hasilPanenSubPemanen.setBrondolan((int) Math.floor((float) (hasilPanenX.getBrondolan() * persentase) / 100));
                            hasilPanenSubPemanen.setTotalJanjang((int) Math.floor((float) (hasilPanenX.getTotalJanjang() * persentase) / 100));
                            hasilPanenSubPemanen.setTotalJanjangPendapatan((int) Math.floor((float) (hasilPanenX.getTotalJanjangPendapatan() * persentase) / 100));

                            Log.d(TAG, "0 distribusiJanjangSubPemanen: ------------------------------------------------------------------ ");
                            Log.d(TAG, "0 distribusiJanjangSubPemanen: "+ i);
                            Log.d(TAG, "0 distribusiJanjangSubPemanen: Normal "+ String.valueOf(hasilPanenSubPemanen.getJanjangNormal()));
                            Log.d(TAG, "0 distribusiJanjangSubPemanen: Mentah "+ String.valueOf(hasilPanenSubPemanen.getBuahMentah()));
                            Log.d(TAG, "0 distribusiJanjangSubPemanen: Busuk "+ String.valueOf(hasilPanenSubPemanen.getBusukNJangkos()));
                            Log.d(TAG, "0 distribusiJanjangSubPemanen: Lewat "+ String.valueOf(hasilPanenSubPemanen.getBuahLewatMatang()));
                            Log.d(TAG, "0 distribusiJanjangSubPemanen: Abnormal "+ String.valueOf(hasilPanenSubPemanen.getBuahAbnormal()));
                            Log.d(TAG, "0 distribusiJanjangSubPemanen: tipePemanen "+ String.valueOf(tipePemanen));
                            Log.d(TAG, "0 distribusiJanjangSubPemanen: ------------------------------------------------------------------ ");

                            totalDistribusi.setHasilPanen(transaksiPanenHelper.combineHasilPanen(totalDistribusi.getHasilPanen(), hasilPanenSubPemanen));

                            subPemanens.add(i,
                                    new SubPemanen(i,
                                            GlobalHelper.getCharForNumber(i),
                                            persentase,
                                            hasilPanenSubPemanen,
                                            tipePemanen,
                                            pemanen
                                    )
                            );
                            if (pemanen != null) {
                                pemanens.put(pemanen.getKodePemanen() +"-"+ tipePemanen, pemanen);
                            }

                            if(tipePemanen == SubPemanen.idOperatorTonase || tipePemanen == SubPemanen.idOperator){
                                MekanisasiOperator mekanisasiOperator = currentMekanisasiOperator.get(pemanen.getKodePemanen());
                                vehicleMekanisasiHelper.setVehicleOperator(mekanisasiOperator);
                            }
                        } else {
                            subPemanens.add(i,
                                    new SubPemanen(i,
                                            GlobalHelper.getCharForNumber(i),
                                            0,
                                            new HasilPanen(),
                                            tipePemanen,
                                            pemanen
                                    )
                            );
                            if (pemanen != null) {
                                pemanens.put(pemanen.getKodePemanen()+"-"+ tipePemanen, pemanen);
                            }
                            if(tipePemanen == SubPemanen.idOperatorTonase || tipePemanen == SubPemanen.idOperator){
                                MekanisasiOperator mekanisasiOperator = currentMekanisasiOperator.get(pemanen.getKodePemanen());
                                vehicleMekanisasiHelper.setVehicleOperator(mekanisasiOperator);
                            }
                        }
                    }
                }
            }else {
                SubPemanen subPemanen = subPemanensX.get(i);
                totalDistribusi.setHasilPanen(transaksiPanenHelper.combineHasilPanen(totalDistribusi.getHasilPanen(),subPemanen.getHasilPanen()));

                subPemanens.add(i, subPemanen);
                if (subPemanen.getPemanen() != null) {
                    pemanens.put(subPemanen.getPemanen().getKodePemanen()+"-"+ tipePemanen, subPemanen.getPemanen());
                }
                if(tipePemanen == SubPemanen.idOperatorTonase || tipePemanen == SubPemanen.idOperator){
                    MekanisasiOperator mekanisasiOperator = currentMekanisasiOperator.get(subPemanen.getPemanen().getKodePemanen());
                    vehicleMekanisasiHelper.setVehicleOperator(mekanisasiOperator);
                }
            }
        }

        adapterPemanen = new SelectPemanenAdapter(getActivity(),
                R.layout.row_record,
                new ArrayList<>(pemanens.values())
        );

        if(transaksiPanenHelper.cekSisaHasilPanen(totalDistribusi.getHasilPanen(),hasilPanenX)){

            sisaJjgBrdl = new HasilPanen(
                    hasilPanenX.getJanjangNormal() - totalDistribusi.getHasilPanen().getJanjangNormal(),
                    hasilPanenX.getBuahMentah() - totalDistribusi.getHasilPanen().getBuahMentah(),
                    hasilPanenX.getBusukNJangkos() - totalDistribusi.getHasilPanen().getBusukNJangkos(),
                    hasilPanenX.getBuahLewatMatang() - totalDistribusi.getHasilPanen().getBuahLewatMatang(),
                    hasilPanenX.getBuahAbnormal() - totalDistribusi.getHasilPanen().getBuahAbnormal(),
                    hasilPanenX.getTangkaiPanjang() - totalDistribusi.getHasilPanen().getTangkaiPanjang(),
                    hasilPanenX.getBuahDimakanTikus() - totalDistribusi.getHasilPanen().getBuahDimakanTikus(),
                    hasilPanenX.getBrondolan() - totalDistribusi.getHasilPanen().getBrondolan(),
                    0,
                    hasilPanenX.getTotalJanjang() - totalDistribusi.getHasilPanen().getTotalJanjang(),
                    hasilPanenX.getTotalJanjangPendapatan() - totalDistribusi.getHasilPanen().getTotalJanjangPendapatan()

            );

            HasilPanen distribusiSisa = new HasilPanen(
                    sisaJjgBrdl.getJanjangNormal() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getJanjangNormal())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getBuahMentah() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getBuahMentah())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getBusukNJangkos() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getBusukNJangkos())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getBuahLewatMatang() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getBuahLewatMatang())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getBuahAbnormal() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getBuahAbnormal())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getTangkaiPanjang() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getTangkaiPanjang())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getBuahDimakanTikus() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getBuahDimakanTikus())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getBrondolan() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getBrondolan())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    0,
                    sisaJjgBrdl.getTotalJanjang() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getTotalJanjang())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0,
                    sisaJjgBrdl.getTotalJanjangPendapatan() != 0 ? (int) Math.ceil(Double.parseDouble(String.valueOf(sisaJjgBrdl.getTotalJanjangPendapatan())) / Double.parseDouble(String.valueOf(subPemanens.size()))) :0
            );

            Collections.sort(subPemanens, new Comparator<SubPemanen>() {
                @Override
                public int compare(SubPemanen t0, SubPemanen t1) {
                    int a = GlobalHelper.getTotalJanjang(t0.getHasilPanen());
                    int b = GlobalHelper.getTotalJanjang(t1.getHasilPanen());
                    return a < b ? -1 : (a > b) ? 1 : 0;
                }
            });

            ArrayList<SubPemanen> subPemanensXX = new ArrayList<>();
            for(int i = 0 ; i < subPemanens.size(); i++){
                SubPemanen subPemanen = subPemanens.get(i);
                if(subPemanen.getTipePemanen() != SubPemanen.idOperatorTonase) {
                    HasilPanen subPemanaenHasilPanen = subPemanen.getHasilPanen();
                    if (sisaJjgBrdl.getJanjangNormal() > 0) {
                        int tambahDistribusi = distribusiSisa.getJanjangNormal();
                        if (distribusiSisa.getJanjangNormal() > sisaJjgBrdl.getJanjangNormal()) {
                            tambahDistribusi = sisaJjgBrdl.getJanjangNormal();
                        }
                        subPemanaenHasilPanen.setJanjangNormal(subPemanaenHasilPanen.getJanjangNormal() + tambahDistribusi);
                        sisaJjgBrdl.setJanjangNormal(sisaJjgBrdl.getJanjangNormal() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getBuahMentah() > 0) {
                        int tambahDistribusi = distribusiSisa.getBuahMentah();
                        if (distribusiSisa.getBuahMentah() > sisaJjgBrdl.getBuahMentah()) {
                            tambahDistribusi = sisaJjgBrdl.getBuahMentah();
                        }
                        subPemanaenHasilPanen.setBuahMentah(subPemanaenHasilPanen.getBuahMentah() + tambahDistribusi);
                        sisaJjgBrdl.setBuahMentah(sisaJjgBrdl.getBuahMentah() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getBusukNJangkos() > 0) {
                        int tambahDistribusi = distribusiSisa.getBusukNJangkos();
                        if (distribusiSisa.getBusukNJangkos() > sisaJjgBrdl.getBusukNJangkos()) {
                            tambahDistribusi = sisaJjgBrdl.getBusukNJangkos();
                        }
                        subPemanaenHasilPanen.setBusukNJangkos(subPemanaenHasilPanen.getBusukNJangkos() + tambahDistribusi);
                        sisaJjgBrdl.setBusukNJangkos(sisaJjgBrdl.getBusukNJangkos() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getBuahLewatMatang() > 0) {
                        int tambahDistribusi = distribusiSisa.getBuahLewatMatang();
                        if (distribusiSisa.getBuahLewatMatang() > sisaJjgBrdl.getBuahLewatMatang()) {
                            tambahDistribusi = sisaJjgBrdl.getBuahLewatMatang();
                        }
                        subPemanaenHasilPanen.setBuahLewatMatang(subPemanaenHasilPanen.getBuahLewatMatang() + tambahDistribusi);
                        sisaJjgBrdl.setBuahLewatMatang(sisaJjgBrdl.getBuahLewatMatang() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getBuahAbnormal() > 0) {
                        int tambahDistribusi = distribusiSisa.getBuahAbnormal();
                        if (distribusiSisa.getBuahAbnormal() > sisaJjgBrdl.getBuahAbnormal()) {
                            tambahDistribusi = sisaJjgBrdl.getBuahAbnormal();
                        }
                        subPemanaenHasilPanen.setBuahAbnormal(subPemanaenHasilPanen.getBuahAbnormal() + tambahDistribusi);
                        sisaJjgBrdl.setBuahAbnormal(sisaJjgBrdl.getBuahAbnormal() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getTangkaiPanjang() > 0) {
                        int tambahDistribusi = distribusiSisa.getTangkaiPanjang();
                        if (distribusiSisa.getTangkaiPanjang() > sisaJjgBrdl.getTangkaiPanjang()) {
                            tambahDistribusi = sisaJjgBrdl.getTangkaiPanjang();
                        }
                        subPemanaenHasilPanen.setTangkaiPanjang(subPemanaenHasilPanen.getTangkaiPanjang() + tambahDistribusi);
                        sisaJjgBrdl.setTangkaiPanjang(sisaJjgBrdl.getTangkaiPanjang() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getBuahDimakanTikus() > 0) {
                        int tambahDistribusi = distribusiSisa.getBuahDimakanTikus();
                        if (distribusiSisa.getBuahDimakanTikus() > sisaJjgBrdl.getBuahDimakanTikus()) {
                            tambahDistribusi = sisaJjgBrdl.getBuahDimakanTikus();
                        }
                        subPemanaenHasilPanen.setBuahDimakanTikus(subPemanaenHasilPanen.getBuahDimakanTikus() + tambahDistribusi);
                        sisaJjgBrdl.setBuahDimakanTikus(sisaJjgBrdl.getBuahDimakanTikus() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getBrondolan() > 0) {
                        int tambahDistribusi = distribusiSisa.getBrondolan();
                        if (distribusiSisa.getBrondolan() > sisaJjgBrdl.getBrondolan()) {
                            tambahDistribusi = sisaJjgBrdl.getBrondolan();
                        }
                        subPemanaenHasilPanen.setBrondolan(subPemanaenHasilPanen.getBrondolan() + tambahDistribusi);
                        sisaJjgBrdl.setBrondolan(sisaJjgBrdl.getBrondolan() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getTotalJanjang() > 0) {
                        int tambahDistribusi = distribusiSisa.getTotalJanjang();
                        if (distribusiSisa.getTotalJanjang() > sisaJjgBrdl.getTotalJanjang()) {
                            tambahDistribusi = sisaJjgBrdl.getTotalJanjang();
                        }
                        subPemanaenHasilPanen.setTotalJanjang(subPemanaenHasilPanen.getTotalJanjang() + tambahDistribusi);
                        sisaJjgBrdl.setTotalJanjang(sisaJjgBrdl.getTotalJanjang() - tambahDistribusi);
                    }
                    if (sisaJjgBrdl.getTotalJanjangPendapatan() > 0) {
                        int tambahDistribusi = distribusiSisa.getTotalJanjangPendapatan();
                        if (distribusiSisa.getTotalJanjangPendapatan() > sisaJjgBrdl.getTotalJanjangPendapatan()) {
                            tambahDistribusi = sisaJjgBrdl.getTotalJanjangPendapatan();
                        }
                        subPemanaenHasilPanen.setTotalJanjangPendapatan(subPemanaenHasilPanen.getTotalJanjangPendapatan() + tambahDistribusi);
                        sisaJjgBrdl.setTotalJanjangPendapatan(sisaJjgBrdl.getTotalJanjangPendapatan() - tambahDistribusi);
                    }

                    try {
                        Log.d(TAG, "s distribusiJanjangSubPemanen: ---------------------------------------------------------- ");
                        Log.d(TAG, "s distribusiJanjangSubPemanen: " + subPemanen.getNoUrutKongsi());
                        Log.d(TAG, "s distribusiJanjangSubPemanen: Normal " + String.valueOf(subPemanaenHasilPanen.getJanjangNormal()));
                        Log.d(TAG, "s distribusiJanjangSubPemanen: Mentah " + String.valueOf(subPemanaenHasilPanen.getBuahMentah()));
                        Log.d(TAG, "s distribusiJanjangSubPemanen: Busuk " + String.valueOf(subPemanaenHasilPanen.getBusukNJangkos()));
                        Log.d(TAG, "s distribusiJanjangSubPemanen: Lewat " + String.valueOf(subPemanaenHasilPanen.getBuahLewatMatang()));
                        Log.d(TAG, "s distribusiJanjangSubPemanen: Abnormal " + String.valueOf(subPemanaenHasilPanen.getBuahAbnormal()));
                        Log.d(TAG, "s distribusiJanjangSubPemanen: TotalJanjang " + String.valueOf(subPemanaenHasilPanen.getTotalJanjang()));
                        Log.d(TAG, "s distribusiJanjangSubPemanen: ---------------------------------------------------------- ");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    subPemanen.setHasilPanen(subPemanaenHasilPanen);
                }
                subPemanensXX.add(subPemanen);
            }

            subPemanens = new ArrayList<>();
            for(int i = 0 ; i < subPemanensXX.size(); i++){
                SubPemanen subPemanen = subPemanensXX.get(i);
                HasilPanen hasilPanen = new HasilPanen();

                hasilPanen.setJanjangNormal(subPemanen.getHasilPanen().getJanjangNormal());
                hasilPanen.setBuahMentah(subPemanen.getHasilPanen().getBuahMentah());
                hasilPanen.setBusukNJangkos(subPemanen.getHasilPanen().getBusukNJangkos());
                hasilPanen.setBuahLewatMatang(subPemanen.getHasilPanen().getBuahLewatMatang());
                hasilPanen.setBuahAbnormal(subPemanen.getHasilPanen().getBuahAbnormal());
                hasilPanen.setTangkaiPanjang(subPemanen.getHasilPanen().getTangkaiPanjang());
                hasilPanen.setBuahDimakanTikus(subPemanen.getHasilPanen().getBuahDimakanTikus());
                hasilPanen.setBrondolan(subPemanen.getHasilPanen().getBrondolan());
                hasilPanen.setTotalJanjangBuruk(subPemanen.getHasilPanen().getTotalJanjangBuruk());


//                Log.d(TAG, "b distribusiJanjangSubPemanen: ---------------------------------------------------------- ");
//                Log.d(TAG, "b distribusiJanjangSubPemanen "+ subPemanen.getNoUrutKongsi());
//                Log.d(TAG, "b distribusiJanjangSubPemanen: Normal "+ String.valueOf(hasilPanen.getJanjangNormal()));
//                Log.d(TAG, "b distribusiJanjangSubPemanen: Mentah "+ String.valueOf(hasilPanen.getBuahMentah()));
//                Log.d(TAG, "b distribusiJanjangSubPemanen: Busuk "+ String.valueOf(hasilPanen.getBusukNJangkos()));
//                Log.d(TAG, "b distribusiJanjangSubPemanen: Lewat "+ String.valueOf(hasilPanen.getBuahLewatMatang()));
//                Log.d(TAG, "b distribusiJanjangSubPemanen: Abnormal "+ String.valueOf(hasilPanen.getBuahAbnormal()));
//                Log.d(TAG, "b distribusiJanjangSubPemanen: TotalJanjang "+ String.valueOf(hasilPanen.getTotalJanjang()));
//                Log.d(TAG, "b distribusiJanjangSubPemanen: ---------------------------------------------------------- ");

                hasilPanen.setTotalJanjang(GlobalHelper.getTotalJanjang(hasilPanen));
                hasilPanen.setTotalJanjangPendapatan(GlobalHelper.getTotalJanjangPendapatan(hasilPanen,dynamicParameterPenghasilan));

                Log.d(TAG, "1 distribusiJanjangSubPemanen: ---------------------------------------------------------- ");
                Log.d(TAG, "1 distribusiJanjangSubPemanen "+ subPemanen.getNoUrutKongsi());
                Log.d(TAG, "1 distribusiJanjangSubPemanen: Normal "+ String.valueOf(hasilPanen.getJanjangNormal()));
                Log.d(TAG, "1 distribusiJanjangSubPemanen: Mentah "+ String.valueOf(hasilPanen.getBuahMentah()));
                Log.d(TAG, "1 distribusiJanjangSubPemanen: Busuk "+ String.valueOf(hasilPanen.getBusukNJangkos()));
                Log.d(TAG, "1 distribusiJanjangSubPemanen: Lewat "+ String.valueOf(hasilPanen.getBuahLewatMatang()));
                Log.d(TAG, "1 distribusiJanjangSubPemanen: Abnormal "+ String.valueOf(hasilPanen.getBuahAbnormal()));
                Log.d(TAG, "1 distribusiJanjangSubPemanen: TotalJanjang "+ String.valueOf(hasilPanen.getTotalJanjang()));
                Log.d(TAG, "1 distribusiJanjangSubPemanen: ---------------------------------------------------------- ");

                subPemanen.setHasilPanen(hasilPanen);

//                JJG Normalnya jadi nga bener karena total jjg tidak bisa di update di detail
//                HasilPanen hasilPanen = new HasilPanen(
//                        0,
//                        subPemanen.getHasilPanen().getBuahMentah(),
//                        subPemanen.getHasilPanen().getBusukNJangkos(),
//                        subPemanen.getHasilPanen().getBuahLewatMatang(),
//                        subPemanen.getHasilPanen().getBuahAbnormal(),
//                        subPemanen.getHasilPanen().getTangkaiPanjang(),
//                        subPemanen.getHasilPanen().getBuahDimakanTikus(),
//                        subPemanen.getHasilPanen().getBrondolan(),
//                        subPemanen.getHasilPanen().getTotalJanjangBuruk(),
//                        subPemanen.getHasilPanen().getTotalJanjang(),
//                        0,
//                        subPemanen.getHasilPanen().getWaktuPanen(),
//                        subPemanen.getHasilPanen().isAlasanBrondolan()
//                );
//
//                int jjgT = GlobalHelper.getTotalJanjang(
//                    new HasilPanen(
//                            0,
//                            hasilPanen.getBuahMentah(),
//                            hasilPanen.getBusukNJangkos(),
//                            hasilPanen.getBuahLewatMatang(),
//                            hasilPanen.getBuahAbnormal(),
//                            hasilPanen.getTangkaiPanjang(),
//                            hasilPanen.getBuahDimakanTikus(),
//                            hasilPanen.getTotalJanjang(),
//                            0,
//                            hasilPanen.getBrondolan(),
//                            hasilPanen.isAlasanBrondolan()
//                    )
//                );
//
//                int Tjjg = hasilPanen.getTotalJanjang() - GlobalHelper.getTotalJanjangPendapatan(new HasilPanen(
//                        0,
//                        hasilPanen.getBuahMentah(),
//                        hasilPanen.getBusukNJangkos(),
//                        hasilPanen.getBuahLewatMatang(),
//                        hasilPanen.getBuahAbnormal(),
//                        hasilPanen.getTangkaiPanjang(),
//                        hasilPanen.getBuahDimakanTikus(),
//                        hasilPanen.getBrondolan(),
//                        0,
//                        hasilPanen.getTotalJanjang(),
//                        0
//                ),dynamicParameterPenghasilan);
//
//                Gson gson = new Gson();
//                Log.d(TAG, "distribusiJanjangSubPemanen: Total JJg " + String.valueOf(Tjjg) + " Hasil Total JJg "+ String.valueOf(hasilPanen.getTotalJanjang()) + " Normal Sebelumnya "+ String.valueOf(jjgT));
//                Log.d(TAG, "distribusiJanjangSubPemanen: Total "+ gson.toJson(hasilPanen));
//                Log.d(TAG, "distribusiJanjangSubPemanen: Total subPemanen "+ gson.toJson(subPemanen.getHasilPanen()));
//
//                int nJJG = hasilPanen.getTotalJanjang() - jjgT;
//                hasilPanen.setJanjangNormal(nJJG);
//
//                hasilPanen.setTotalJanjangPendapatan(GlobalHelper.getTotalJanjangPendapatan(hasilPanen,dynamicParameterPenghasilan));
//
//                subPemanen.setHasilPanen(new HasilPanen(
//                        hasilPanen.getJanjangNormal(),
//                        subPemanen.getHasilPanen().getBuahMentah(),
//                        subPemanen.getHasilPanen().getBusukNJangkos(),
//                        subPemanen.getHasilPanen().getBuahLewatMatang(),
//                        subPemanen.getHasilPanen().getBuahAbnormal(),
//                        subPemanen.getHasilPanen().getTangkaiPanjang(),
//                        subPemanen.getHasilPanen().getBuahDimakanTikus(),
//                        subPemanen.getHasilPanen().getBrondolan(),
//                        subPemanen.getHasilPanen().getTotalJanjangBuruk(),
//                        subPemanen.getHasilPanen().getTotalJanjang(),
//                        hasilPanen.getTotalJanjangPendapatan(),
//                        subPemanen.getHasilPanen().getWaktuPanen(),
//                        subPemanen.getHasilPanen().isAlasanBrondolan()
//                ));
                subPemanens.add(subPemanen);
            }

            if(transaksiPanenHelper.cekSisaHasilPanen(sisaJjgBrdl)) {
                lSisa.setVisibility(View.VISIBLE);
                tvSisa.setText("Sisa" +
                        "\nJJg   : " + sisaJjgBrdl.getTotalJanjang() +
                        "\nJ Pdt : " + sisaJjgBrdl.getTotalJanjangPendapatan() +
                        "\nBrdl  : " + sisaJjgBrdl.getBrondolan()
                );
                tvSisa2.setText(
                        "\n(N)  : " + sisaJjgBrdl.getJanjangNormal() +
                                "\n(A)  : " + sisaJjgBrdl.getBuahMentah() +
                                "\n(E) : " + sisaJjgBrdl.getBusukNJangkos()
                );
                tvSisa3.setText(
                        "\n(0) : " + sisaJjgBrdl.getBuahLewatMatang() +
                                "\n(BA) : " + sisaJjgBrdl.getBuahAbnormal() +
                                "\n(TP) : " + sisaJjgBrdl.getTangkaiPanjang() +
                                "\n(BDT) : " + sisaJjgBrdl.getBuahDimakanTikus()
                );
                etPemanenSisa.setThreshold(1);
                etPemanenSisa.setAdapter(adapterPemanen);
            }else{
                lSisa.setVisibility(View.GONE);
            }
        }else{
            lSisa.setVisibility(View.GONE);
        }

        adapterPemanen.notifyDataSetChanged();



        Collections.sort(subPemanens, new Comparator<SubPemanen>() {
            @Override
            public int compare(SubPemanen t0, SubPemanen t1) {
                int a = t0.getNoUrutKongsi();
                int b = t1.getNoUrutKongsi();
                return a < b ? -1 : (a > b) ? 1 : 0;
            }
        });

        for(int i = 0 ; i < subPemanens.size(); i++) {
            SubPemanen subPemanen = subPemanens.get(i);
            Log.d(TAG, "f distribusiJanjangSubPemanen: "+ subPemanen.getNoUrutKongsi());

            RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(subPemanen.getNoUrutKongsi());
            if (viewHolder != null) {
                View view = viewHolder.itemView;
                AppCompatEditText etNormalSubPemanen = view.findViewById(R.id.etNormalSubPemanen);
                AppCompatEditText etBuahMentahSubPemanen = view.findViewById(R.id.etBuahMentahSubPemanen);
                AppCompatEditText etBusukSubPemanen = view.findViewById(R.id.etBusukSubPemanen);
                AppCompatEditText etLewatMatangSubPemanen = view.findViewById(R.id.etLewatMatangSubPemanen);
                AppCompatEditText etAbnormalSubPemanen = view.findViewById(R.id.etAbnormalSubPemanen);
                AppCompatEditText etTangkaiPanjangSubPemanen = view.findViewById(R.id.etTangkaiPanjangSubPemanen);
                AppCompatEditText etBuahDimakanTikusSubPemanen = view.findViewById(R.id.etBuahDimakanTikusSubPemanen);
                AppCompatEditText etTotalBrondolanSubPemanen = view.findViewById(R.id.etTotalBrondolanSubPemanen);
                AppCompatEditText etTotalJanjangSubPemanen = view.findViewById(R.id.etTotalJanjangSubPemanen);
                AppCompatEditText etTotalPdtSubPemanen = view.findViewById(R.id.etTotalPdtSubPemanen);
                CompleteTextViewHelper lsTipe = view.findViewById(R.id.lsTipe);

                etNormalSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getJanjangNormal()));
                etBuahMentahSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getBuahMentah()));
                etBusukSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getBusukNJangkos()));
                etLewatMatangSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getBuahLewatMatang()));
                etAbnormalSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getBuahAbnormal()));
                etTangkaiPanjangSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getTangkaiPanjang()));
                etBuahDimakanTikusSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getBuahDimakanTikus()));
                etTotalBrondolanSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getBrondolan()));
                etTotalJanjangSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getTotalJanjang()));
                etTotalPdtSubPemanen.setText(String.valueOf(subPemanen.getHasilPanen().getTotalJanjangPendapatan()));

                lsTipe.setText(SubPemanen.arrayCodeTipePemanen[subPemanen.getTipePemanen()]);
            }

            if(subPemanen.getNoUrutKongsi() == 0){
                pemanenSelected = subPemanen.getPemanen();
            }
        }
    }

    public boolean validasiSubPemanen(){
        int persentase = 0;
        HasilPanen hasilPanenSubPemane = new HasilPanen();
        HasilPanen hasilPanenX = new HasilPanen();
//        int totalJanjangSubPemanen = 0;
//        int totalBrondolanSubPemanen = 0;
//        int totalJanjangPendapatanSubPemanen = 0;
//        int totalBrondolan = 0;

        if(!etNormal.getText().toString().isEmpty()){
            hasilPanenX.setJanjangNormal(Integer.parseInt(etNormal.getText().toString()));
        }
        if(!etMentah.getText().toString().isEmpty()) {
            hasilPanenX.setBuahMentah(Integer.parseInt(etMentah.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etBusuk.getText().toString().isEmpty()) {
            hasilPanenX.setBusukNJangkos(Integer.parseInt(etBusuk.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etLewatMatang.getText().toString().isEmpty()) {
            hasilPanenX.setBuahLewatMatang(Integer.parseInt(etLewatMatang.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etAbnormal.getText().toString().isEmpty()) {
            hasilPanenX.setBuahAbnormal(Integer.parseInt(etAbnormal.getText().toString()));
//            buahMentah = Integer.parseInt(etMentah.getText().toString());
        }
        if(!etTangkaiPanjang.getText().toString().isEmpty()) {
            hasilPanenX.setTangkaiPanjang(Integer.parseInt(etTangkaiPanjang.getText().toString()));
//            tangkaiPanjang = Integer.parseInt(etTangkaiPanjang.getText().toString());
        }
        if(!etBuahDimakanTikus.getText().toString().isEmpty()) {
            hasilPanenX.setBuahDimakanTikus(Integer.parseInt(etBuahDimakanTikus.getText().toString()));
//            totalJjg = Integer.parseInt(etTotalJanjang.getText().toString());
        }
        if(!etBrondolan.getText().toString().isEmpty()) {
            hasilPanenX.setBrondolan(Integer.parseInt(etBrondolan.getText().toString()));
//            totalBrondolan = Integer.parseInt(etBrondolan.getText().toString());
        }
        if(!etTotalJanjangPendapatan.getText().toString().isEmpty()) {
            hasilPanenX.setTotalJanjangPendapatan(Integer.parseInt(etTotalJanjangPendapatan.getText().toString()));
//            totalJjgPendapatan = Integer.parseInt(etTotalJanjangPendapatan.getText().toString());
        }
        if(!etTotalJanjang.getText().toString().isEmpty()) {
            hasilPanenX.setTotalJanjang(Integer.parseInt(etTotalJanjang.getText().toString()));
//            totalJjg = Integer.parseInt(etTotalJanjang.getText().toString());
        }

        if(idKongsi == Kongsi.idNone){
            if(rvPemanen.getAdapter().getItemCount() > 1){
                Toast.makeText(HarvestApp.getContext(), Kongsi.arrayTipeKongsi[idKongsi]+" Hanya Bisa 1 Pemanen Saja", Toast.LENGTH_LONG).show();
                return false;
            }
        }else if(idKongsi == Kongsi.idKelompok){
            if(rvPemanen.getAdapter().getItemCount() > GlobalHelper.MAX_PEMANEN_KELOMPOK || rvPemanen.getAdapter().getItemCount() == 1){
                Toast.makeText(HarvestApp.getContext(), Kongsi.arrayTipeKongsi[idKongsi]+" Hanya Bisa "+ GlobalHelper.MAX_PEMANEN_KELOMPOK +" Pemanen Dan Tidak Bisa 1 Pemanen", Toast.LENGTH_LONG).show();
                return false;
            }
        }else if(idKongsi == Kongsi.idMekanis){
            if(rvPemanen.getAdapter().getItemCount() > GlobalHelper.MAX_PEMANEN_MEKANIS || rvPemanen.getAdapter().getItemCount() == 1){
                Toast.makeText(HarvestApp.getContext(), Kongsi.arrayTipeKongsi[idKongsi]+" Hanya Bisa "+ GlobalHelper.MAX_PEMANEN_MEKANIS +" Pemanen Dan Tidak Bisa 1 Pemanen", Toast.LENGTH_LONG).show();
                return false;
            }

            if(vehicleMekanisasi == null){
                Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Vehicle Mekanisasi", Toast.LENGTH_LONG).show();
                return false;
            }else if (etVehicleMekanisasi.getText().toString().isEmpty()){
                Toast.makeText(HarvestApp.getContext(), "Vehicle Mekanisasi Tidak Boleh Kosong", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if(lSisa.getVisibility() == View.VISIBLE && pemanenSisa == null){
            Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Pemanen Yang akan di Berikan Sisa Kalkulasi", Toast.LENGTH_LONG).show();
            etPemanenSisa.requestFocus();
            return false;
        }

        if(tipeKongsi.getPosition() == Kongsi.idMekanis && (pemanenOperator.size() == 0 && pemanenOperatorTonase.size() == 0)){
            Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Operator (O)", Toast.LENGTH_LONG).show();
            return false;
        }

        if(rvPemanen.getAdapter().getItemCount() <= pemanenOperator.size()){
            Toast.makeText(HarvestApp.getContext(), "Tidak Bisa Semua Tipe Operator (OJ)", Toast.LENGTH_LONG).show();
            return false;
        }

        if(rvPemanen.getAdapter().getItemCount() <= pemanenOperatorTonase.size()){
            Toast.makeText(HarvestApp.getContext(), "Tidak Bisa Semua Tipe Operator (OT)", Toast.LENGTH_LONG).show();
            return false;
        }

        if(pemanenOperator.size() >= 1 && pemanenOperatorTonase.size() >= 1){
            Toast.makeText(HarvestApp.getContext(), "Tidak Boleh Ada Operator Janjang Dan Operator Tonase Secara Bersamaan", Toast.LENGTH_LONG).show();
            return false;
        }

        if(pemanenOperatorTonase.size() >= 3 ){
            Toast.makeText(HarvestApp.getContext(), "Operator Tonase Tidak Bisa Lebih Dari 2 Orang ", Toast.LENGTH_LONG).show();
            return false;
        }

        Pemanen pemanenSpk = null;
        Set<String> kodePemanen = new ArraySet<>();
        AppCompatEditText etPersentase = null;
        int pesenOT = 0;
        for(int i = 0; i < rvPemanen.getAdapter().getItemCount(); i++){
            boolean canRead = false;
            RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
            if(viewHolder != null) {
                Gson gson = new Gson();
                canRead = false;
                View view = viewHolder.itemView;
                etPersentase = view.findViewById(R.id.etPersentase);
                AppCompatEditText etNormalSubPemanen = view.findViewById(R.id.etNormalSubPemanen);
                AppCompatEditText etBuahMentahSubPemanen = view.findViewById(R.id.etBuahMentahSubPemanen);
                AppCompatEditText etBusukSubPemanen = view.findViewById(R.id.etBusukSubPemanen);
                AppCompatEditText etLewatMatangSubPemanen = view.findViewById(R.id.etLewatMatangSubPemanen);
                AppCompatEditText etAbnormalSubPemanen = view.findViewById(R.id.etAbnormalSubPemanen);
                AppCompatEditText etTangkaiPanjangSubPemanen = view.findViewById(R.id.etTangkaiPanjangSubPemanen);
                AppCompatEditText etBuahDimakanTikusSubPemanen = view.findViewById(R.id.etBuahDimakanTikusSubPemanen);
                AppCompatEditText etTotalBrondolanSubPemanen = view.findViewById(R.id.etTotalBrondolanSubPemanen);
                AppCompatEditText etTotalJanjangSubPemanen = view.findViewById(R.id.etTotalJanjangSubPemanen);
                AppCompatEditText etTotalPdtSubPemanen = view.findViewById(R.id.etTotalPdtSubPemanen);
                TextView tvValue = view.findViewById(R.id.tvValue);
                CompleteTextViewHelper lsTipe = view.findViewById(R.id.lsTipe);
                CompleteTextViewHelper etPemanen = view.findViewById(R.id.etPemanenSub);

                if (tvValue.getText().toString().isEmpty()) {
                    etPemanen.requestFocus();
                    Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_chose_pemanen), Toast.LENGTH_LONG).show();
                    return false;
                }else{
                    Pemanen pemanen = gson.fromJson(tvValue.getText().toString(),Pemanen.class);
                    kodePemanen.add(pemanen.getKodePemanen() +";"+ lsTipe.getText().toString());

                    if(pemanen.getGank().equals(SubPemanen.pemanenSPK.getGank())){
                        pemanenSpk = pemanen;
                    }
                }

                if (!etPersentase.getText().toString().isEmpty()) {
                    persentase += Integer.parseInt(etPersentase.getText().toString());
                    if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperatorTonase])) {
                        pesenOT += Integer.parseInt(etPersentase.getText().toString());
                    }
                }
                if (!etNormalSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setJanjangNormal(hasilPanenSubPemane.getJanjangNormal() + Integer.parseInt(etNormalSubPemanen.getText().toString()));
                }
                if (!etBuahMentahSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setBuahMentah(hasilPanenSubPemane.getBuahMentah() + Integer.parseInt(etBuahMentahSubPemanen.getText().toString()));
                }
                if (!etBusukSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setBusukNJangkos(hasilPanenSubPemane.getBusukNJangkos() + Integer.parseInt(etBusukSubPemanen.getText().toString()));
                }
                if (!etLewatMatangSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setBuahLewatMatang(hasilPanenSubPemane.getBuahLewatMatang() + Integer.parseInt(etLewatMatangSubPemanen.getText().toString()));
                }
                if (!etAbnormalSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setBuahAbnormal(hasilPanenSubPemane.getBuahAbnormal() + Integer.parseInt(etAbnormalSubPemanen.getText().toString()));
                }
                if (!etTangkaiPanjangSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setTangkaiPanjang(hasilPanenSubPemane.getTangkaiPanjang() + Integer.parseInt(etTangkaiPanjangSubPemanen.getText().toString()));
                }
                if (!etBuahDimakanTikusSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setBuahDimakanTikus(hasilPanenSubPemane.getBuahDimakanTikus() + Integer.parseInt(etBuahDimakanTikusSubPemanen.getText().toString()));
                }
                if (!etTotalJanjangSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setTotalJanjang(hasilPanenSubPemane.getTotalJanjang() + Integer.parseInt(etTotalJanjangSubPemanen.getText().toString()));
                }
                if (!etTotalBrondolanSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setBrondolan(hasilPanenSubPemane.getBrondolan() + Integer.parseInt(etTotalBrondolanSubPemanen.getText().toString()));
                }
                if (!etTotalPdtSubPemanen.getText().toString().isEmpty()) {
                    hasilPanenSubPemane.setTotalJanjangPendapatan(hasilPanenSubPemane.getTotalJanjangPendapatan() + Integer.parseInt(etTotalPdtSubPemanen.getText().toString()));
                }
            }else{
                SubPemanen subPemanen = subPemanens.get(i);
                if (subPemanen.getPemanen().getKodePemanen() == null) {
                    Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.please_chose_pemanen), Toast.LENGTH_LONG).show();
                    return false;
                }
                if(subPemanen.getPemanen().getKodePemanen().equals(SubPemanen.pemanenSPK.getKodePemanen())){
                    pemanenSpk = subPemanen.getPemanen();
                }

                kodePemanen.add(subPemanen.getPemanen().getKodePemanen() +";"+ subPemanen.getTipePemanen());
                persentase += subPemanen.getPersentase();
                hasilPanenSubPemane = subPemanen.getHasilPanen();
//                hasilPanenSubPemane.setTotalJanjang(subPemanen.getHasilPanen().getTotalJanjang());
//                hasilPanenSubPemane.setTotalJanjangPendapatan(subPemanen.getHasilPanen().getTotalJanjangPendapatan());
//                hasilPanenSubPemane.setBrondolan(subPemanen.getHasilPanen().getBrondolan());
//                totalJanjangSubPemanen += subPemanen.getJanjang();
//                totalBrondolanSubPemanen += subPemanen.getBrondolan();
//                totalJanjangPendapatanSubPemanen += subPemanen.getJanjangPendapatan();
            }

            if(persentase > 100){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Persentase Lebih Dari 100", Toast.LENGTH_LONG).show();
                return false;
            }

            if(pesenOT >= 1){
                Toast.makeText(HarvestApp.getContext(), "Pastikan Tipe OT Hanya 0%", Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getJanjangNormal() > hasilPanenX.getJanjangNormal()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Janjang Normal Lebih Besar Dari "+ hasilPanenX.getJanjangNormal(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getBuahMentah() > hasilPanenX.getBuahMentah()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Buah Mentah Lebih Besar Dari "+ hasilPanenX.getBuahMentah(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getBusukNJangkos() > hasilPanenX.getBusukNJangkos()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Busuk dan Jangkos Lebih Besar Dari "+ hasilPanenX.getBusukNJangkos(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getBuahLewatMatang() > hasilPanenX.getBuahLewatMatang()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Buah Lewat Matang Lebih Besar Dari "+ hasilPanenX.getBuahLewatMatang(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getBuahAbnormal() > hasilPanenX.getBuahAbnormal()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Buah Abnormal Lebih Besar Dari "+ hasilPanenX.getBuahAbnormal(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getTangkaiPanjang() > hasilPanenX.getTangkaiPanjang()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Tangkai Panjang Lebih Besar Dari "+ hasilPanenX.getTangkaiPanjang(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getBuahDimakanTikus() > hasilPanenX.getBuahDimakanTikus()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Buah Dimakan Tikus Lebih Besar Dari "+ hasilPanenX.getBuahDimakanTikus(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getBrondolan() > hasilPanenX.getBrondolan()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Total Brondolan Lebih Besar Dari "+ hasilPanenX.getBrondolan(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getTotalJanjang() > hasilPanenX.getTotalJanjang()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Total Janjang Lebih Besar Dari "+ hasilPanenX.getTotalJanjang(), Toast.LENGTH_LONG).show();
                return false;
            }

            if(hasilPanenSubPemane.getTotalJanjangPendapatan() > hasilPanenX.getTotalJanjangPendapatan()){
                if(canRead) {
                    etPersentase.requestFocus();
                }
                Toast.makeText(HarvestApp.getContext(), "Total Janjang Lebih Besar Dari "+ hasilPanenX.getTotalJanjangPendapatan(), Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if(pemanenSisa != null){
            boolean pemanenSisaMasuk = false;
            for (String pemanen : kodePemanen){
                String kPemanen = pemanen.split(";")[0];
                if(kPemanen.equalsIgnoreCase(pemanenSisa.getKodePemanen())){
                    pemanenSisaMasuk = true;
                    break;
                }
            }
            if(!pemanenSisaMasuk){
                Toast.makeText(HarvestApp.getContext(), "Pemanen Sisa Harus ada Di List Info Pemanen", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if(kodePemanen.size() != rvPemanen.getAdapter().getItemCount()){
            Toast.makeText(HarvestApp.getContext(), "Ada Pemanen Yang Dipilih 2 kali Mohon Di Cek", Toast.LENGTH_LONG).show();
            return false;
        }

        if(pemanenSpk != null && rvPemanen.getAdapter().getItemCount() > 1){
            Toast.makeText(HarvestApp.getContext(), "Jika Ada Pemanen Kontanan Hanya Bisa Tidak Kongsi", Toast.LENGTH_LONG).show();
            return false;
        }

        if(persentase != 100){
            if(etPersentase != null) {
                etPersentase.requestFocus();
            }
            Toast.makeText(HarvestApp.getContext(), "Persentase Tidak Sama Dengan 100", Toast.LENGTH_LONG).show();
            return false;
        }

        if(hasilPanenSubPemane.getTotalJanjang() != hasilPanenX.getTotalJanjang()){
            if(pemanenSisa == null) {
                etPemanenSisa.requestFocus();
                Toast.makeText(HarvestApp.getContext(), "Total Janjang Tidak Sama Dengan " + hasilPanenX.getTotalJanjang(), Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if(hasilPanenSubPemane.getBrondolan() != hasilPanenX.getBrondolan()){
            if(pemanenSisa == null) {
                etPemanenSisa.requestFocus();
                Toast.makeText(HarvestApp.getContext(), "Total Brondolan Tidak Sama Dengan " + hasilPanenX.getBrondolan(), Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if(kodePemanen.size() > 1){
            Gerdang gerdang = null;
            String KodePemane = "";
            for(String pemanen : kodePemanen){
                String s = pemanen.split(";")[0];
                gerdang = currentGerdang.get(s);
                KodePemane = s;

                if(gerdang != null){
                    break;
                }else{
                    boolean brekGerdang = false;
                    if(currentGerdang.size() > 0) {
                        for (Gerdang ger : currentGerdang.values()) {
                            try {
                                if (ger.getPemanenGerdang().getKodePemanen().equals(s)) {
                                    gerdang = ger;
                                    brekGerdang = true;
                                    break;
                                } else if (ger.getPemanenUtama().getKodePemanen().equals(s)) {
                                    gerdang = ger;
                                    brekGerdang = true;
                                    break;
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                    if(brekGerdang){
                        break;
                    }
                }
            }

            if(gerdang != null){
                Toast.makeText(HarvestApp.getContext(), "Kode Pemanen "+ KodePemane + " masuk kedalam pemanen gerdang utama atau pemanen gerdang", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        return true;
    }

    public void cekPemanenGerdang(Pemanen pemanenUtama){
        if(pemanenUtama != null) {
            if (currentGerdang != null) {
                if (currentGerdang.size() > 0) {
                    Gerdang gerdang = currentGerdang.get(pemanenUtama.getNik());
                    if (gerdang != null) {
                        cbGerdang.setChecked(true);
                        cvGerdang.setVisibility(View.VISIBLE);
                        Pemanen pemanenGerdang = gerdang.getPemanenGerdang();
                        etGangGerdang.setText(pemanenGerdang.getGank());
                        pemanenGerdangHelper.setAdapterPemanenGerdang(pemanenGerdang.getGank());
                        pemanenGerdangHelper.setPemanen(pemanenGerdang);
                    }
                }
            }
        }
    }

    public void setUpTransaksiAngkutNFCBaseOnHistory(TransaksiAngkut transaksiAngkut){
        angkutActivity.transaksiAngkutFromNFC = transaksiAngkut;
        lsave.setVisibility(View.VISIBLE);
        setUpTransaksiAngkutNFC(false);
        transaksiAngkutHelper.listrefrence.dismiss();
    }

    private void setUpTransaksiAngkutNFC(boolean withUpdateVersion){
        if(angkutActivity.transaksiAngkutFromNFC.getLangsiran() != null) {
            if(stampImage == null){
                stampImage = new StampImage();
            }
            stampImage.setBlock(angkutActivity.transaksiAngkutFromNFC.getLangsiran().getBlock());
            stampImage.setLangsiran(angkutActivity.transaksiAngkutFromNFC.getLangsiran());

            etBlock.setText(angkutActivity.transaksiAngkutFromNFC.getLangsiran().getBlock());
            etNotph.setText(angkutActivity.transaksiAngkutFromNFC.getLangsiran().getNamaTujuan());

            etBlock.setFocusable(false);
            etNotph.setFocusable(false);

            tvTph.setText("Langsiran");
        }
        etNormal.setText(String.valueOf(angkutActivity.transaksiAngkutFromNFC.getTotalJanjang()));
        etMentah.setText(String.format("%.0f",angkutActivity.transaksiAngkutFromNFC.getTotalBeratEstimasi()));
        etBrondolan.setText(String.valueOf(angkutActivity.transaksiAngkutFromNFC.getTotalBrondolan()));
        transaksiAngkutNFC = angkutActivity.transaksiAngkutFromNFC;
        angkutActivity.transaksiAngkutFromNFC = null;
//        bjr = getBjrAngkut("");



        if(withUpdateVersion) {
            if (transaksiAngkutNFC != null) {
                Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", transaksiAngkutNFC.getIdTAngkut()));
                if (cursor.size() > 0) {
                    for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                        DataNitrit dataNitrit = (DataNitrit) iterator.next();
                        Gson gson = new Gson();
                        selectedTransaksiAngkut = gson.fromJson(dataNitrit.getValueDataNitrit(), TransaksiAngkut.class);
                        break;
                    }
                }
                db.close();
            }
        }
    }

    private void setMasterSpk(TPH tph){
        if(allSpk == null || allSpk.size() == 0) {
            allSpk = GlobalHelper.getDataAllSPKPanen(tph);
        }
    }

    private void setupHasilPanenStamp(){
        Boolean alas = true;
        if(lsAlasBrondol.getText().toString().equalsIgnoreCase("no")){
            alas = false;
        }
        hasilPanenStamp = new HasilPanen(
                Integer.parseInt(etNormal.getText().toString().isEmpty() ? "0" : etNormal.getText().toString()),
                Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()),
                Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()),
                Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()),
                Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString()),
                Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()),
                Integer.parseInt(etBuahDimakanTikus.getText().toString().isEmpty() ? "0" : etBuahDimakanTikus.getText().toString()),
                Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()),
                totalJjgJelek,
                totalJjg,
                totalJjgPendapatan,
                waktuPanen,
                alas
        );

        Log.d(TAG, "setupHasilPanenStamp: 1 update Hasil Panen " +String.valueOf(hasilPanenStamp.getTotalJanjang()));
    }

    private void mainQcMutuBuah(){
//        etGang.setFocusable(false);
//        etPemanen.setFocusable(false);
        etBlock.setFocusable(false);
        etNotph.setFocusable(false);
        etAncak.setFocusable(false);

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BUAH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("param1", selectedTransaksiPanen.getIdTPanen()));
        if(cursor.size() > 0) {
            showYesNo("Apakah Anda Akan Melakukan Update Qc Buah pada Tph "+ selectedTransaksiPanen.getTph().getNamaTph(),YesNo_UpdateQc);
        }
        db.close();
    }



    /*
     * list param
     * stat :
     *  1. YesNo_Simpan = 10
     *  2. YesNo_SimpanT2 = 11
     *  3. YEsNo_Hapus = 12
     *  4. YesNo_HapusT2 = 13
     *  5. YesNo_Batal = 14
     *  6. YesNo_SimpanQc = 15
     *  7. YesNo_HapusQc = 16
     *  8. YesNo_UpdateQc = 17
     */
    public void showYesNo(String text,int stat){
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.yes_no, null);
        TextView tvDialogKet = (TextView) dialogView.findViewById(R.id.ket1);
        SlideView btnCancelDialog = (SlideView) dialogView.findViewById(R.id.btnCancel);
        SlideView btnOkDialog = (SlideView) dialogView.findViewById(R.id.btnOK);

        final AlertDialog dialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle)
                .setView(dialogView)
                .setCancelable(false)
                .create();

        final ForegroundColorSpan fcs = new ForegroundColorSpan(getActivity().getResources().getColor(R.color.Red));
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

        tvDialogKet.setText(text);
        btnOkDialog.setText("Yes");
        btnCancelDialog.setText("No");

        btnCancelDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                switch (stat) {
                    case YesNo_UpdateQc:
                        qcMutuBuahActivity.backProses();
                        dialog.dismiss();
                        break;

                    case YesNo_Pilih_TPH:{
                        etBlock.requestFocus();
                        etBlock.setText("");
                        etNotph.setText("");
                        tphSelected = new TPH();
                        dialog.dismiss();
                        break;
                    }
                    default:
                        dialog.dismiss();
                        break;
                }
            }
        });
        btnOkDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                switch (stat){
                    case YesNo_Simpan:{
                        showDialogCloseTransaksiPanen();
                        break;
                    }
                    case YesNo_SimpanT2:{
//  hapus                 if(transaksiAngkutNFC != null){
//                            if(etNormal.getText().toString().isEmpty()){
//                                Toast.makeText(HarvestApp.getContext(),"Nilai Total JJg Tidak Boleh Kosong",Toast.LENGTH_SHORT).show();
//                            }else if(etMentah.getText().toString().isEmpty()){
//                                Toast.makeText(HarvestApp.getContext(),"Nilai Total Berat Tidak Boleh Kosong",Toast.LENGTH_SHORT).show();
//                            }else if(etBrondolan.getText().toString().isEmpty()){
//                                Toast.makeText(HarvestApp.getContext(),"Nilai Total Brondolan Tidak Boleh Kosong",Toast.LENGTH_SHORT).show();
//                            }else if(transaksiAngkutNFC.getTotalJanjang() < Integer.parseInt(etNormal.getText().toString())){
//                                Toast.makeText(HarvestApp.getContext(),"Nilai Total JJg Di Input Harus Dibawah Total Janjang Awal",Toast.LENGTH_SHORT).show();
//                            }else if((int) Math.round(transaksiAngkutNFC.getTotalBeratEstimasi()) < Integer.parseInt(etMentah.getText().toString())){
//                                Toast.makeText(HarvestApp.getContext(),"Nilai Total Berat Estimasi Di Input Harus Dibawah Total Berat Estimasi Awal",Toast.LENGTH_SHORT).show();
//                            }else if(cekPrubahanDataAngkut()) {
//                                showDialogCloseAngkut();
//                            }else{
//                                new LongOperation(getView()).execute(String.valueOf(LongOperation_Save_Panen_T2));
//                            }
//                        }else
                        if (selectedTransaksiPanen != null){
                            if(Integer.parseInt(etTotalJanjang.getText().toString()) <= selectedTransaksiPanen.getHasilPanen().getTotalJanjang()) {
                                new LongOperation(getView()).execute(String.valueOf(LongOperation_Save_Panen_T2));
                            }else{
                                Toast.makeText(HarvestApp.getContext(),"Nilai Total JJg Di Input Harus Dibawah Total Janjang Awal",Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            // maka dia manual harusnya ke YesNo_SimpanT2_MANUAL
//                            new LongOperation(getView()).execute(String.valueOf(LongOperation_Save_Panen_T2));
                        }

                        break;
                    }
                    case YesNo_SimpanT2_MANUAL:{
                        // iki manual save
                        if(lsStatus.getText().toString().equals("Kartu Rusak")){
                            if(etNoNFC.getText().toString().equals("")){
                                Toast.makeText(HarvestApp.getContext(), "Silahkan isi no kartu terlebih dahulu", Toast.LENGTH_SHORT).show();
                                break;
                            }
                        } else {
                            if(selectedTpHnLangsiran == null){
                                Toast.makeText(HarvestApp.getContext(), "Mohon Pilih TPH Dahulu", Toast.LENGTH_SHORT).show();
                                break;
                            }else if(selectedTpHnLangsiran.getTph() == null){
                                Toast.makeText(HarvestApp.getContext(), "Mohon Pilih TPH Dahulu Ya", Toast.LENGTH_SHORT).show();
                                break;
                            }else if(selectedTpHnLangsiran.getTph().getNoTph() == null){
                                Toast.makeText(HarvestApp.getContext(), "Please Pilih TPH Dahulu", Toast.LENGTH_SHORT).show();
                                break;
                            }else if(selectedTpHnLangsiran.getTph().getNoTph().trim().isEmpty()){
                                Toast.makeText(HarvestApp.getContext(), "Cek lagi tphnya udah bener input belum", Toast.LENGTH_SHORT).show();
                                break;
                            }else if (etBlock.getText().toString().equals("")) {
                                Toast.makeText(HarvestApp.getContext(), "Silahkan pilih block terlebih dahulu", Toast.LENGTH_SHORT).show();
                                break;
                            } else if (etNotph.getText().toString().equals("")) {
                                Toast.makeText(HarvestApp.getContext(), "Silahkan pilih TPH / langsiran terlebih dahulu", Toast.LENGTH_SHORT).show();
                                break;
                            }
                        }
                        new LongOperation(getView()).execute(String.valueOf(LongOperation_Save_Panen_T2_MANUAL));
                        break;
                    }
                    case YesNo_Hapus:{
                        new LongOperation(getView()).execute(String.valueOf(LongOperation_Delet_Data));
                        break;
                    }
                    case YesNo_HapusT2:{
                        new LongOperation(getView()).execute(String.valueOf(LongOperation_Delet_Data_T2));
                        break;
                    }
                    case YesNo_Batal:{
                        if(tphActivity != null) {
                            tphActivity.backProses();
                        }else if (angkutActivity != null){
                            angkutActivity.backProses();
                        }else if (qcMutuBuahActivity != null){
                            qcMutuBuahActivity.backProses();
                        }
                        break;
                    }
                    case YesNo_SimpanQc:{
                        new LongOperation(getView()).execute(String.valueOf(LongOperation_Save_Data_Qc));
                        break;
                    }
                    case YesNo_HapusQc:{
                        new LongOperation(getView()).execute(String.valueOf(LongOperation_Delet_Data_Qc));
                        break;
                    }
                    case YesNo_UpdateQc:{
                        dialog.dismiss();
                        break;
                    }
                    case YesNo_Pilih_TPH:{
                        dialog.dismiss();
                        break;
                    }
                    case YesNo_Pilih_TPH_Bayangan:{
                        cbTphBayangan.setChecked(true);
                        dialog.dismiss();
                        break;
                    }
                }

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void showDialogCloseTransaksiPanen(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.metode_penyimpanan_transaksi,null);
        TextView tv_header = view.findViewById(R.id.tv_header);
        LinearLayout lnfc = view.findViewById(R.id.lnfc);
        LinearLayout lprint = view.findViewById(R.id.lprint);
        LinearLayout lshare = view.findViewById(R.id.lshare);
        Button btnClose = view.findViewById(R.id.btnClose);

        tv_header.setText(getResources().getString(R.string.chose_metode_panen));
        btnClose.setVisibility(View.GONE);

        latitude = baseActivity.currentlocation.getLatitude();
        longitude = baseActivity.currentlocation.getLongitude();

        if(selectedTransaksiPanen != null) {
            if (selectedTransaksiPanen.getStatus() == TransaksiPanen.UPLOAD ||
                selectedTransaksiPanen.getStatus() == TransaksiPanen.SUDAH_DIANGKUT ||
                selectedTransaksiPanen.getStatus() == TransaksiPanen.SUDAH_DIPKS
            ) {
                lnfc.setVisibility(View.GONE);
            }
        }

        lnfc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(GlobalHelper.isDoubleClick()){
                    return;
                }
                if(selectedTransaksiPanen == null){
                    NfcHelper.showNFCTap(getActivity(),LongOperation_NFC_With_Save);
                    //new LongOperation(view).execute(String.valueOf(LongOperation_NFC_With_Save));
                }else {
                    if (tphActivity.tulisUlangNFC) {
                        double distance = GlobalHelper.distance(latitude,selectedTransaksiPanen.getLatitude(),
                                longitude,selectedTransaksiPanen.getLongitude());
                        if(distance <= GlobalHelper.RADIUS) {
                            NfcHelper.showNFCTap(getActivity(),LongOperation_NFC_With_Save);
//                            if (transaksiPanenHelper.hapusDataNFC(selectedTransaksiPanen)) {
//                                //new LongOperation(view).execute(String.valueOf(LongOperation_NFC_With_Save));
//                            }
                        }else{
                            WidgetHelper.warningRadius(tphActivity,tphActivity.myCoordinatorLayout,distance);
                        }
                    } else {
                        NfcHelper.showNFCTap(getActivity(),LongOperation_NFC_Only);
                        //new LongOperation(view).execute(String.valueOf(LongOperation_NFC_Only));
                    }
                }
            }
        });

        lprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(GlobalHelper.isDoubleClick()){
                    return;
                }
                if (baseActivity.bluetoothHelper.mService.getState() == BluetoothService.STATE_CONNECTED) {
                    if (selectedTransaksiPanen == null) {
                        new LongOperation(view).execute(String.valueOf(LongOperation_Print_With_Save));
                    } else {
                        if (tphActivity.tulisUlangNFC) {
                            double distance = GlobalHelper.distance(latitude,selectedTransaksiPanen.getLatitude(),
                                    longitude,selectedTransaksiPanen.getLongitude());
                            if(distance <= GlobalHelper.RADIUS) {
                                new LongOperation(view).execute(String.valueOf(LongOperation_Print_With_Save));
                            }else{
                                WidgetHelper.warningRadius(tphActivity,tphActivity.myCoordinatorLayout,distance);
                            }
                        } else {
                            new LongOperation(view).execute(String.valueOf(LongOperation_Print_Only));
                        }
                    }
                } else {
                    transaksiPanenHelper.printBluetooth(selectedTransaksiPanen);
                }
            }
        });

        lshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(GlobalHelper.isDoubleClick()){
                    return;
                }
                //jika data tidak ada file angkut selected berarti
                //data sudah di hapus karena ini data yang sebelumnya sudah pernah di create
                if(selectedTransaksiPanen == null) {
                    new LongOperation(view).execute(String.valueOf(LongOperation_Share_With_Save));
                }else {
                    if (tphActivity.tulisUlangNFC) {
                        double distance = GlobalHelper.distance(latitude,selectedTransaksiPanen.getLatitude(),
                                longitude,selectedTransaksiPanen.getLongitude());
                        if(distance <= GlobalHelper.RADIUS) {
                            new LongOperation(view).execute(String.valueOf(LongOperation_Share_With_Save));
                        }else{
                            Toast.makeText(HarvestApp.getContext(),tphActivity.getResources().getString(R.string.radius_to_long_with_tph) + " Data Tidak Akan Di Update",Toast.LENGTH_SHORT).show();
                            new LongOperation(view).execute(String.valueOf(LongOperation_Share_Only));
                        }
                    } else {
                        new LongOperation(view).execute(String.valueOf(LongOperation_Share_Only));
                    }
                }
            }
        });

        alertDialog = WidgetHelper.showListReference(alertDialog,view,getActivity());
    }

    private boolean validasiSaveTransaksiPanen(){
        //validasi untuk tph aktivity
        if(tphActivity != null) {
            if (pemanenSelected == null && spkSelected == null) {
                Toast.makeText(HarvestApp.getContext(), getActivity().getResources().getString(R.string.please_chose_pemanen), Toast.LENGTH_SHORT).show();
//                etPemanen.requestFocus();
//                pemanenHelper.chosePemanen(etGang.getText().toString());
                return false;
            }

            if(spkSelected != null && rvPemanen.getAdapter().getItemCount() == 1){

            }else{
                if (pemanenSelected == null) {
                    Toast.makeText(HarvestApp.getContext(), getActivity().getResources().getString(R.string.please_chose_pemanen), Toast.LENGTH_SHORT).show();
                    return false;
                }
            }

            if (tphSelected == null) {
                Toast.makeText(HarvestApp.getContext(), getActivity().getResources().getString(R.string.please_chose_tph), Toast.LENGTH_SHORT).show();
                if (tphActivity.currentLocationOK()) {
                    latitude = tphActivity.currentlocation.getLatitude();
                    longitude = tphActivity.currentlocation.getLongitude();
//                    showmapTPH();
                    setDropdownTph();
                } else {
                    WidgetHelper.warningFindGps(tphActivity,tphActivity.myCoordinatorLayout);
                }
                return false;
            }else{
                if(tphSelected.getNoTph() == null){
                    Toast.makeText(HarvestApp.getContext(), getActivity().getResources().getString(R.string.tidak_bisa_ubah_tidak_punya_tph), Toast.LENGTH_SHORT).show();
                    return false;
                }
            }

            if(spkSelected != null){
                boolean adaBlk = false;
                if(spkSelected.getBlock() != null) {
                    for (int i = 0; i < spkSelected.getBlock().size(); i++) {
                        if (spkSelected.getBlock().get(i).trim().toUpperCase().equals(tphSelected.getBlock().trim().toUpperCase())) {
                            adaBlk = true;
                            break;
                        }
                    }
                }

                if(!adaBlk){
                    Toast.makeText(HarvestApp.getContext(), "SPK yang ada pilih tidak bisa di input pada blok ini", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }

            if (waktuPanen == null) {
                Toast.makeText(HarvestApp.getContext(), "Mohon Input Waktu Panen", Toast.LENGTH_SHORT).show();
                return false;
            }

            if(selectedTransaksiPanen != null){
                if(selectedTransaksiPanen.getIdNfc() != null){
                    if(!selectedTransaksiPanen.getIdNfc().equalsIgnoreCase(GlobalHelper.bytesToHexString(tphActivity.myTag.getId()))){
                        Toast.makeText(HarvestApp.getContext(), "Ini Tap Dari Kartu Yang Berbeda", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                }
            }
        }else if (angkutActivity != null){
            if(baseActivity.currentLocationOK()){
//                // unutk proses angkut radius tidak di ikutkan karena tidak memungkinkan tap nfc pada tahap ini
//                longitude = baseActivity.currentlocation.getLongitude();
//                latitude = baseActivity.currentlocation.getLatitude();
//                //jika dari nfc biru ada maka cek apa kah jarak lokasi sekarang denggan tph masih sesuai toleransi
//                //jika dari nfc hijau ada maka cek apa kah jarak lokasi sekarang denggan tph langsiran masih sesuai toleransi
//                if(selectedTransaksiPanen != null){
//                    double distance = GlobalHelper.distance(selectedTransaksiPanen.getLatitude(),latitude,
//                            selectedTransaksiPanen.getLongitude(),longitude);
//                    if(distance > GlobalHelper.RADIUS){
//                        Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.radius_to_long_with_tph),Toast.LENGTH_SHORT).show();
//                        return false;
//                    }
//                }else if (transaksiAngkutNFC != null){
//                    double distance = GlobalHelper.distance(transaksiAngkutNFC.getLatitudeTujuan(),latitude,
//                            transaksiAngkutNFC.getLongitudeTujuan(),longitude);
//                    if(distance > GlobalHelper.RADIUS){
//                        Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.radius_to_long_with_tph) + " Langsiran",Toast.LENGTH_SHORT).show();
//                        return false;
//                    }
//                }
            }else{
//                // jika angkut tidak perlu cek gps
//                angkutActivity.searchingGPS = Snackbar.make(angkutActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                angkutActivity.searchingGPS.show();
//                return false;
            }
        }else if (qcMutuBuahActivity != null){
            if(baseActivity.currentLocationOK()){
                longitude = baseActivity.currentlocation.getLongitude();
                latitude = baseActivity.currentlocation.getLatitude();
                //jika dari nfc biru ada maka cek apa kah jarak lokasi sekarang denggan tph masih sesuai toleransi
                //jika dari nfc hijau ada maka cek apa kah jarak lokasi sekarang denggan tph langsiran masih sesuai toleransi
                if(selectedTransaksiPanen != null){
                    double distance = GlobalHelper.distance(selectedTransaksiPanen.getTph().getLatitude(),latitude,
                            selectedTransaksiPanen.getTph().getLongitude(),longitude);
                    if(distance > GlobalHelper.RADIUS){
                        WidgetHelper.warningRadiusTph(qcMutuBuahActivity,qcMutuBuahActivity.myCoordinatorLayout,distance);
//                        Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.radius_to_long_with_tph),Toast.LENGTH_SHORT).show();
                        return false;
                    }
                }
            }else{
                WidgetHelper.warningFindGps(qcMutuBuahActivity,qcMutuBuahActivity.myCoordinatorLayout);
//                qcMutuBuahActivity.searchingGPS = Snackbar.make(qcMutuBuahActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                qcMutuBuahActivity.searchingGPS.show();
                return false;
            }
        }

        if(etNormal.getText().toString().equals("")){
            Toast.makeText(HarvestApp.getContext(), "Mohon Input Janjang Normal", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            Integer normal = Integer.parseInt(etNormal.getText().toString());
            if(normal < 0 ){
                Toast.makeText(HarvestApp.getContext(), "Janjang Normal "+getActivity().getResources().getString(R.string.canot_minus), Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return true;
    }

    private void saveTransaksiPanen(){
        SimpleDateFormat sdfD = new SimpleDateFormat("ddMMyy");
        HashSet<String> sFoto = new HashSet<>();
        if(ALselectedImage.size() > 0){
            for(File file : ALselectedImage){
                sFoto.add(file.getAbsolutePath());
            }
        }

        String idTransaksi;
        Boolean insert = true;
        String createBy;
        Long createDate;
        if(selectedTransaksiPanen != null){
            insert = false;
            idTransaksi = selectedTransaksiPanen.getIdTPanen();
            createBy = selectedTransaksiPanen.getCreateBy();
            createDate = selectedTransaksiPanen.getCreateDate();
        }else{
            insert = true;
            idTransaksi = "H" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_TRANSAKSI_TPH))
                    + sdfD.format(System.currentTimeMillis())
                    + GlobalHelper.getUser().getUserID();
            createBy = GlobalHelper.getUser().getUserID();
            createDate = System.currentTimeMillis();

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_TPH);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", idTransaksi));
            if(cursor.size() > 0) {
                GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_TRANSAKSI_TPH);
                idTransaksi = "H" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_TRANSAKSI_TPH))
                        + sdfD.format(System.currentTimeMillis())
                        + GlobalHelper.getUser().getUserID();
            }
            db.close();
        }

        Boolean alas = true;
        String remarks = etRemarks.getText().toString();
        if(lsAlasBrondol.getText().toString().equalsIgnoreCase("no")){
            alas = false;
        }


        HasilPanen hasilPanen = new HasilPanen(
                Integer.parseInt(etNormal.getText().toString().isEmpty() ? "0" : etNormal.getText().toString()),
                Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()),
                Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()),
                Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()),
                Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString()),
                Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()),
                Integer.parseInt(etBuahDimakanTikus.getText().toString().isEmpty() ? "0" : etBuahDimakanTikus.getText().toString()),
                Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()),
                totalJjgJelek,
                totalJjg,
                totalJjgPendapatan,
                waktuPanen,
                alas
        );

        int Status = TransaksiPanen.TAP;

        ArrayList<SubPemanen> subPemanens= new ArrayList<>();
        if(spkSelected != null ){
            idKongsi = Kongsi.idNone;
            subPemanens.add(new SubPemanen(0,
                    GlobalHelper.getCharForNumber(0),
                    100,
                    hasilPanen,
                    SubPemanen.idPemnaen,
                    pemanenSelected)
            );
        }else {
            for (int i = 0; i < rvPemanen.getAdapter().getItemCount(); i++) {
                RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
                View view = viewHolder.itemView;
                TextView tvValue = view.findViewById(R.id.tvValue);
                AppCompatEditText etPersentase = view.findViewById(R.id.etPersentase);
                AppCompatEditText etNormalSubPemanen = view.findViewById(R.id.etNormalSubPemanen);
                AppCompatEditText etBuahMentahSubPemanen = view.findViewById(R.id.etBuahMentahSubPemanen);
                AppCompatEditText etBusukSubPemanen = view.findViewById(R.id.etBusukSubPemanen);
                AppCompatEditText etLewatMatangSubPemanen = view.findViewById(R.id.etLewatMatangSubPemanen);
                AppCompatEditText etAbnormalSubPemanen = view.findViewById(R.id.etAbnormalSubPemanen);
                AppCompatEditText etTangkaiPanjangSubPemanen = view.findViewById(R.id.etTangkaiPanjangSubPemanen);
                AppCompatEditText etBuahDimakanTikusSubPemanen = view.findViewById(R.id.etBuahDimakanTikusSubPemanen);
                AppCompatEditText etTotalJanjangSubPemanen = view.findViewById(R.id.etTotalJanjangSubPemanen);
                AppCompatEditText etTotalBrondolanSubPemanen = view.findViewById(R.id.etTotalBrondolanSubPemanen);
                AppCompatEditText etTotalPdtSubPemanen = view.findViewById(R.id.etTotalPdtSubPemanen);
                CompleteTextViewHelper lsTipe = view.findViewById(R.id.lsTipe);

                Gson gson = new Gson();
//                int jjg = Integer.parseInt(etTotalJanjangSubPemanen.getText().toString());
//                int brondolan = Integer.parseInt(etTotalBrondolanSubPemanen.getText().toString());
//                int jjgPdt = Integer.parseInt(etTotalPdtSubPemanen.getText().toString());
//                int jjgBuahMentah = Integer.parseInt(etBuahMentahSubPemanen.getText().toString());
//                int jjgTangkaiPanjang = Integer.parseInt(etTangkaiPanjangSubPemanen.getText().toString());
                HasilPanen hasilPanenSubPemanen = new HasilPanen(
                        Integer.parseInt(etNormalSubPemanen.getText().toString()),
                        Integer.parseInt(etBuahMentahSubPemanen.getText().toString()),
                        Integer.parseInt(etBusukSubPemanen.getText().toString()),
                        Integer.parseInt(etLewatMatangSubPemanen.getText().toString()),
                        Integer.parseInt(etAbnormalSubPemanen.getText().toString()),
                        Integer.parseInt(etTangkaiPanjangSubPemanen.getText().toString()),
                        Integer.parseInt(etBuahDimakanTikusSubPemanen.getText().toString()),
                        Integer.parseInt(etTotalBrondolanSubPemanen.getText().toString()),
                        0,
                        Integer.parseInt(etTotalJanjangSubPemanen.getText().toString()),
                        Integer.parseInt(etTotalPdtSubPemanen.getText().toString())
                );
                Pemanen pemanen = gson.fromJson(tvValue.getText().toString(), Pemanen.class);

                if (pemanenSisa != null) {
                    if (pemanen.getKodePemanen().equalsIgnoreCase(pemanenSisa.getKodePemanen())) {
                        hasilPanenSubPemanen = transaksiPanenHelper.combineHasilPanen(hasilPanenSubPemanen,sisaJjgBrdl);
//                        jjg += sisaJjgBrdl.getTotalJanjang();
//                        brondolan += sisaJjgBrdl.getBrondolan();
//                        jjgPdt += sisaJjgBrdl.getTotalJanjangPendapatan();
//                        jjgBuahMentah += sisaJjgBrdl.getBuahMentah();
//                        jjgTangkaiPanjang += sisaJjgBrdl.getTangkaiPanjang();
                    }
                }

                int tipe = SubPemanen.idPemnaen;
                if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperator])) {
                    tipe = SubPemanen.idOperator;
                }
                if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperatorTonase])) {
                    tipe = SubPemanen.idOperatorTonase;
                }

                subPemanens.add(new SubPemanen(i,
                        GlobalHelper.getCharForNumber(i),
                        Integer.parseInt(etPersentase.getText().toString()),
                        hasilPanenSubPemanen,
                        tipe,
                        pemanen)
                );

                if (i == 0) {
                    pemanenSelected = pemanen;
                }
            }
        }
        Kongsi kongsi = new Kongsi(idKongsi,subPemanens);
        if(pemanenGerdang != null){
            kongsi.setPemanenGerdang(pemanenGerdang);
        }

        TransaksiPanen transaksiPanen = new TransaksiPanen(idTransaksi,
                latitude,
                longitude,
                createBy,
                createDate,
                GlobalHelper.getUser().getUserID(),
                System.currentTimeMillis(),
                tphSelected.getAncak(),
                "",
                pemanenSelected,
                tphSelected,
                hasilPanen,
                sFoto,
                Status,
                0,
                kongsi,
                spkSelected,
                remarks);

        if(vehicleMekanisasi != null) {
            transaksiPanen.setVehicleMekanisasi(vehicleMekanisasi);
            transaksiPanen.setTypeVehicleMekanisasi(typeVehicleMekanisasi);
        }

        if(cbTphBayangan.isChecked()){
            ReasonTphBayangan reasonTphBayangan = new ReasonTphBayangan();
            List<String> reasons = ReasonTphBayangan.listReason;
            for(int i = 0 ; i < reasons.size();i++){
                if(reasons.get(i).toLowerCase().equals(lsReasonTphBayangan.getText().toString().toLowerCase())){
                    reasonTphBayangan.setIdReason(i);
                    if(reasons.get(i).toLowerCase().equals(ReasonTphBayangan.nameReasonOthers.toLowerCase())){
                        reasonTphBayangan.setNameReason(etReasonTphBayangan.getText().toString());
                    }else{
                        reasonTphBayangan.setNameReason(lsReasonTphBayangan.getText().toString());
                    }
                }
            }
            if(!reasonTphBayangan.getNameReason().isEmpty()){
                transaksiPanen.setReasonTphBayangan(reasonTphBayangan);
            }
        }

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.SUBSTITUTE_KRANI, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        if(cbKraniSubstitute.isChecked()){
            Gson gson = new Gson();
            editor.putString(HarvestApp.SUBSTITUTE_KRANI,gson.toJson(substituteSelected));
            editor.apply();
            transaksiPanen.setSubstitute(substituteSelected);
        }else {
            editor.remove(HarvestApp.SUBSTITUTE_KRANI);
            editor.apply();
        }

        if(tphActivity != null){
            if(tphActivity.myTag != null) {
                transaksiPanen.setIdNfc(GlobalHelper.bytesToHexString(tphActivity.myTag.getId()));
            }
        }

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_TPH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(transaksiPanen.getIdTPanen(),
                gson.toJson(transaksiPanen),
                transaksiPanen.getTph().getNoTph(),
                transaksiPanen.getPemanen().getKodePemanen()
        );
        if(insert) {
            repository.insert(dataNitrit);
            GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_TRANSAKSI_TPH);
        }else{
            repository.update(dataNitrit);
        }
        db.close();
        selectedTransaksiPanen = transaksiPanen;

        if(transaksiPanen.getKongsi() != null) {
            if (transaksiPanen.getKongsi().getPemanenGerdang() != null) {
                pemanenGerdangHelper.savePemanenGerdang(transaksiPanen);
            }
        }

        //if(transaksiPanen.getVehicleMekanisasi() != null){
        //    vehicleMekanisasiHelper.saveMekanisasiOperator(transaksiPanen);
        //}

        SharedPreferences prefAFDPanen = HarvestApp.getContext().getSharedPreferences(HarvestApp.AFDELING_PANEN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editorAFDPanen = prefAFDPanen.edit();
        editorAFDPanen.putString(HarvestApp.AFDELING_PANEN,selectedTransaksiPanen.getTph().getAfdeling());
        editorAFDPanen.apply();

    }

    private void hapusData(TransaksiPanen transaksiPanen){
        if(tphActivity.tulisUlangNFC) {
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_TPH);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Gson gson = new Gson();
            transaksiPanen.setStatus(TransaksiPanen.DELETED);
            DataNitrit dataNitrit = new DataNitrit(transaksiPanen.getIdTPanen(),
                    gson.toJson(transaksiPanen),
                    transaksiPanen.getTph().getNoTph(),
                    transaksiPanen.getPemanen().getKodePemanen()
            );
            repository.update(dataNitrit);
            db.close();
            tphActivity.backProses();
        }
    }

    private boolean cekPrubahanDataAngkut(){
        totalJjg = Integer.parseInt(etNormal.getText().toString().isEmpty() ? "0" : etNormal.getText().toString());

        transaksiAngkutNfcSave = new TransaksiAngkut();
        Gson gson = new Gson();
        dataTransaksiNFC = gson.toJson(transaksiAngkutNFC);
        transaksiAngkutNfcSave = gson.fromJson(dataTransaksiNFC,TransaksiAngkut.class);

        if((transaksiAngkutNFC.getTotalJanjang() == totalJjg) &&
                (transaksiAngkutNFC.getTotalBrondolan() == Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()))){
            transaksiAngkutNfcSave.setFullAngkut(true);
            return false;
        }else{
            transaksiAngkutNfcSave.setFullAngkut(false);
            return true;
        }
    }

//hapus
//    public void showDialogCloseAngkut(){
//        View view = LayoutInflater.from(getActivity()).inflate(R.layout.metode_penyimpanan_transaksi,null);
//        TextView tv_header = view.findViewById(R.id.tv_header);
//        LinearLayout lnfc = view.findViewById(R.id.lnfc);
//        LinearLayout lprint = view.findViewById(R.id.lprint);
//        LinearLayout lshare = view.findViewById(R.id.lshare);
//        Button btnClose = view.findViewById(R.id.btnClose);
//
//        tv_header.setText(getResources().getString(R.string.chose_metode_angkut));
//        btnClose.setVisibility(View.GONE);
//
//        if(baseActivity.currentlocation == null){
//            if(tphActivity  != null) {
////                tphActivity.searchingGPS = Snackbar.make(tphActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
////                tphActivity.searchingGPS.show();
//                WidgetHelper.warningFindGps(tphActivity,tphActivity.myCoordinatorLayout);
//            }else if (angkutActivity != null){
////                angkutActivity.searchingGPS = Snackbar.make(angkutActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
////                angkutActivity.searchingGPS.show();
//                WidgetHelper.warningFindGps(angkutActivity,angkutActivity.myCoordinatorLayout);
//            }
//            return;
//        }
//        latitude = baseActivity.currentlocation.getLatitude();
//        longitude = baseActivity.currentlocation.getLongitude();
//
//        lnfc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                NfcHelper.showNFCTap(getActivity(),LongOperation_Save_Panen_T2_NFC);
//                alertDialog.dismiss();
//            }
//        });
//
//        lprint.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (baseActivity.bluetoothHelper.mService.getState() == BluetoothService.STATE_CONNECTED) {
//                    new LongOperation(view).execute(String.valueOf(LongOperation_Save_Panen_T2_Print));
//                }else{
//                    baseActivity.bluetoothHelper.showListBluetoothPrinter();
//                }
//                alertDialog.dismiss();
//            }
//        });
//
//        lshare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new LongOperation(view).execute(String.valueOf(LongOperation_Save_Panen_T2_Share));
//                alertDialog.dismiss();
//            }
//        });
//
//        alertDialog = WidgetHelper.showListReference(alertDialog,view,getActivity());
//    }

    private void saveTransaksiPanenT2(Object longOperation) {

        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmm");
        String idTransaksi;
        HashSet<String> sFoto = new HashSet<>();
        double berat = 0.0;

        if (ALselectedImage.size() > 0) {
            for (File file : ALselectedImage) {
                sFoto.add(file.getAbsolutePath());
            }
        }
        if (selectedAngkut != null) {
            idTransaksi = selectedAngkut.getIdAngkut();
        } else {
            idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_ANGKUT))
                    + sdf.format(System.currentTimeMillis());
        }

        if (selectedTransaksiPanen != null) {
            selectedTransaksiPanen.getHasilPanen().setJanjangNormal(Integer.parseInt(etNormal.getText().toString().isEmpty() ? "0" : etNormal.getText().toString()));
            selectedTransaksiPanen.getHasilPanen().setBuahMentah(Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()));
            selectedTransaksiPanen.getHasilPanen().setBuahLewatMatang(Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()));
            selectedTransaksiPanen.getHasilPanen().setBusukNJangkos(Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()));
            selectedTransaksiPanen.getHasilPanen().setTangkaiPanjang(Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()));
            selectedTransaksiPanen.getHasilPanen().setBuahAbnormal(Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString()));
            selectedTransaksiPanen.getHasilPanen().setBrondolan(Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()));
            setTotalJjg(false);
            setTotalJjgPendapatan(false);

            Date createDate = new Date(selectedTransaksiPanen.getCreateDate());
            Calendar cal = Calendar.getInstance();
            cal.setTime(createDate);
// hapus      bjr = getBjrAngkut("");
            berat = selectedTransaksiPanen.getHasilPanen().getTotalJanjang() * bjr;
        } else{
            totalJjg = Integer.parseInt(etNormal.getText().toString().isEmpty() ? "0" : etNormal.getText().toString());
            berat = Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString());
        }

//hapus        if(transaksiAngkutNFC != null) {
//
//            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT);
//            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
//            if(selectedAngkut != null){
//                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("param1", selectedAngkut.getIdAngkutRef()));
//                if(cursor.size() > 0) {
//                    for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
//                        DataNitrit dataNitrit = (DataNitrit) iterator.next();
//                        repository.remove(dataNitrit);
//                    }
//                }
//            }
//
//            idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_ANGKUT))
//                    + sdf.format(System.currentTimeMillis());
//            String idTransaksiRef = idTransaksi;
//            if(angkuts != null) {
//                for (int i = 0; i < angkuts.size(); i++) {
//                    Angkut angkut = angkuts.get(i);
//                    angkut.setIdAngkut(idTransaksi);
//                    angkut.setWaktuAngkut(System.currentTimeMillis());
//                    angkut.setIdxAngkut(angkut.getIdxAngkut() + 1);
//                    angkut.setIdAngkutRef(idTransaksiRef);
//                    angkut.setTransaksiAngkut(transaksiAngkutNFC);
//                    angkut.setLatitude(latitude);
//                    angkut.setLongitude(longitude);
//                    angkut.setLangsiran(selectedTpHnLangsiran == null ? null : selectedTpHnLangsiran.getLangsiran() == null ? null : selectedTpHnLangsiran.getLangsiran());
//                    angkut.setFoto(sFoto);
//
//                    Gson gson = new Gson();
//                    DataNitrit dataNitrit = new DataNitrit(angkut.getIdAngkut(), gson.toJson(angkut), idTransaksiRef);
//                    repository.insert(dataNitrit);
//                    GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_ANGKUT);
//
//                    idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_ANGKUT))
//                            + sdf.format(System.currentTimeMillis());
//                }
//            }
//            db.close();
//        }else{

            Date createDate = new Date(System.currentTimeMillis());
            Calendar cal = Calendar.getInstance();
            cal.setTime(createDate);
            double bjr = BjrHelper.getBJRInformasi(etBlock.getText().toString().isEmpty() ? null : etBlock.getText().toString(),cal.get(Calendar.MONTH) + 1,cal.get(Calendar.YEAR));

            String idTransaksiRef = selectedTransaksiPanen.getIdTPanen();

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            if(selectedAngkut != null){
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("param1", selectedAngkut.getIdAngkutRef()));
                if(cursor.size() > 0) {
                    for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                        DataNitrit dataNitrit = (DataNitrit) iterator.next();
                        repository.remove(dataNitrit);
                    }
                }
            }

            int count = 1;
            for(int i = 0 ; i < subPemanens.size();i++){
                SubPemanen subPemanen = subPemanens.get(i);
                String oph = TransaksiPanenHelper.getOph(selectedTransaksiPanen.getIdTPanen(),subPemanen.getOph());
                berat = subPemanen.getHasilPanen().getTotalJanjang() * bjr;
                if(oph != ""){
                    idTransaksi = "A"+ oph.replace("-","");
                }
                Angkut angkut = new Angkut(idTransaksi,
                        System.currentTimeMillis(),
                        selectedTransaksiPanen,
                        transaksiAngkutNFC,
                        latitude,
                        longitude,
                        subPemanen.getHasilPanen().getTotalJanjang(),
                        subPemanen.getHasilPanen().getBrondolan(),
                        bjr,
                        berat,
                        etBlock.getText().toString().isEmpty() ? null : etBlock.getText().toString(),
                        oph,
                        0,
                        selectedTpHnLangsiran == null ? null : selectedTpHnLangsiran.getTph() == null ? null : selectedTpHnLangsiran.getTph(),
                        selectedTpHnLangsiran == null ? null : selectedTpHnLangsiran.getLangsiran() == null ? null : selectedTpHnLangsiran.getLangsiran(),
                        sFoto);
                angkut.setIdAngkutRef(idTransaksiRef);
                angkut.setIdNfc(idNfc);

                Gson gson = new Gson();
                DataNitrit dataNitrit = new DataNitrit(angkut.getIdAngkut(), gson.toJson(angkut),idTransaksiRef);
                Log.d(TAG, "saveTransaksiPanenT2: "+ dataNitrit.getValueDataNitrit());

                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", angkut.getIdAngkut()));
                if(cursor.size() > 0) {
                    repository.update(dataNitrit);
                }else{
                    repository.insert(dataNitrit);
                }
                GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_ANGKUT);

                idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_ANGKUT))
                        + sdf.format(System.currentTimeMillis());

                try {
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket", "Insert Data Oph");
                    objProgres.put("persen", (count * 100 / subPemanens.size()));
                    objProgres.put("count", count);
                    objProgres.put("total", (subPemanens.size()));
                    if(longOperation instanceof LongOperation){
                        ((LongOperation) longOperation).publishProgress(objProgres);
                    }
                    count++;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            db.close();

//        }

//hapus
//        if (saveOnly) {
//            Log.d(this.getClass() + " tapNFC", "Close0");

// hapus  }else{
//
//            Log.d(this.getClass() + "tot Janjang NFC awal",String.valueOf(transaksiAngkutNFC.getTotalJanjang()));
////          akan kesini jika diangkut sebagian di langsiran
//            transaksiAngkutNfcSave.setTotalJanjang(transaksiAngkutNFC.getTotalJanjang() - totalJjg);
//            transaksiAngkutNfcSave.setTotalBrondolan(transaksiAngkutNFC.getTotalBrondolan() - Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()));
//            transaksiAngkutNfcSave.setTotalBeratEstimasi(transaksiAngkutNFC.getTotalBeratEstimasi() - Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()));
//
//            Log.d(this.getClass() + "tot Janjang NFC awal 2",String.valueOf(transaksiAngkutNFC.getTotalJanjang()));
//            Log.d(this.getClass() + "janjang NFC",String.valueOf(totalJjg));
//            Log.d(this.getClass() + "total Janjang SAVE NFC",String.valueOf(transaksiAngkutNfcSave.getTotalJanjang()));
//
//            ArrayList<SubTotalAngkut> subTotalAngkutFinal = new ArrayList<>();
//            for(int j = 0 ; j < transaksiAngkutNFC.getSubTotalAngkuts().size();j++) {
//                SubTotalAngkut subTotalAngkut = transaksiAngkutNFC.getSubTotalAngkuts().get(j);
//
//                Gson gson = new Gson();
//                Log.d(this.getClass() + "subTotAngkut Awal NFC",gson.toJson(subTotalAngkut));
//
//                boolean add = true;
//                for (int i = 0; i < subTotalAngkuts.size(); i++) {
//                    SubTotalAngkut subTotalAngkutX = subTotalAngkuts.get(i);
//                    Log.d(this.getClass() + "subTotAngkut Simpan NFC",gson.toJson(subTotalAngkutX));
//
//                    if(subTotalAngkut.getBlock().equalsIgnoreCase(subTotalAngkutX.getBlock())){
//                        if(subTotalAngkut.getJanjang() == subTotalAngkutX.getJanjang() && subTotalAngkut.getBrondolan() == subTotalAngkutX.getBrondolan()) {
//                            add = false;
//                        }else{
//                            subTotalAngkut.setJanjang(subTotalAngkut.getJanjang() - subTotalAngkutX.getJanjang());
//                            subTotalAngkut.setBerat(subTotalAngkut.getBerat() - subTotalAngkutX.getBerat());
//                            subTotalAngkut.setBrondolan(subTotalAngkut.getBrondolan() - subTotalAngkutX.getBrondolan());
//                        }
//                        break;
//                    }
//                }
//                if(add){
//                    subTotalAngkutFinal.add(subTotalAngkut);
//                    Log.d(this.getClass() + "subTotAngkut Final NFC",gson.toJson(subTotalAngkut));
//                }
//            }
//
//            transaksiAngkutNfcSave.setSubTotalAngkuts(subTotalAngkutFinal);
//            TransaksiAngkut transaksiAngkut1 = null;
//            Gson gson = new Gson();
//
//            //update history angkut ke table tangkutnya jika angkut sebagian
//            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
//            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
//            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", transaksiAngkutNFC.getIdTAngkut()));
//            if(cursor.size() > 0) {
//                for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
//                    DataNitrit dataNitrit = (DataNitrit) iterator.next();
//                    transaksiAngkut1 = gson.fromJson(dataNitrit.getValueDataNitrit(),TransaksiAngkut.class);
//                    ArrayList<TranskasiAngkutVersion> transkasiAngkutVersions = new ArrayList<>();
//                    if(transaksiAngkut1.getTranskasiAngkutVersions() != null) {
//                        transkasiAngkutVersions = transaksiAngkut1.getTranskasiAngkutVersions();
//                        transaksiAngkut1.setTranskasiAngkutVersions(null);
//                    }
//                    transaksiAngkutNfcSave.setEndDate(System.currentTimeMillis());
//                    transkasiAngkutVersions.add(new TranskasiAngkutVersion(transkasiAngkutVersions.size() +  1 , transaksiAngkutNfcSave));
//                    transaksiAngkut1.setTranskasiAngkutVersions(transkasiAngkutVersions);
//                    break;
//                }
//            }
//
//            if(transaksiAngkut1 != null){
//                DataNitrit dataNitrit = new DataNitrit(transaksiAngkut1.getIdTAngkut(), gson.toJson(transaksiAngkut1), ""+transaksiAngkut1.isManual());
//                repository.update(dataNitrit);
//            }
//            db.close();
//        }
//
//        Gson gson = new Gson();
//        Log.d(this.getClass() + "angkut NFC",gson.toJson(transaksiAngkutNFC));
//        Log.d(this.getClass() + "angkut NFC SAVE",gson.toJson(transaksiAngkutNfcSave));
//
//        transaksiAngkutNFC = gson.fromJson(dataTransaksiNFC,TransaksiAngkut.class);
//        Log.d(this.getClass() + "angkut NFC data",gson.toJson(transaksiAngkutNFC));
    }

    private void saveTransaksiPanenT2_MANUAL(boolean saveOnly, boolean isRusak) {

        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmm");
        String idTransaksi = "";
        String idManualAngkut="";
        HashSet<String> sFoto = new HashSet<>();
        double berat = 0.0;

        if (ALselectedImage.size() > 0) {
            for (File file : ALselectedImage) {
                sFoto.add(file.getAbsolutePath());
            }
        }
        if (selectedAngkut != null) {
            idTransaksi = selectedAngkut.getIdAngkut();
        } else {
            if (angkutActivity != null) {
                idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_ANGKUT))
                        + sdf.format(System.currentTimeMillis());
            }else if (spbActivity != null){
                idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_ANGKUT_SPB))
                        + sdf.format(System.currentTimeMillis());
            }else if (mekanisasiActivity != null){
                idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_ANGKUT_SPB))
                        + sdf.format(System.currentTimeMillis());
            }else {
                return;
            }
        }

        idManualAngkut = idTransaksi+";manual_angkut";
        long waktuAngkut = System.currentTimeMillis();


        ManualSPB manualSPBData = null;
        int typeId=0;
        Set<String> idNfc = new ArraySet<>();
        if(lsStatus.getText().toString().equals(ManualSPB.TYPE_KARTU_HILANG_NAME)){
            typeId = ManualSPB.TYPE_KARTU_HILANG;
        }else if(lsStatus.getText().toString().equals(ManualSPB.TYPE_KARTU_RUSAK_NAME)){
            typeId = ManualSPB.TYPE_KARTU_RUSAK;
        }
        if(isRusak){
            manualSPBData = new ManualSPB(  idManualAngkut,
                                            GlobalHelper.getUser().getUserID(),
                                            GlobalHelper.getEstate().getEstCode(),
                                            typeId,
                                            lsStatus.getText().toString(),
                    selectedTpHnLangsiran == null ? null : selectedTpHnLangsiran.getTph() == null ? null : selectedTpHnLangsiran.getTph().getBlock(),
                    selectedTpHnLangsiran == null ? null : selectedTpHnLangsiran.getTph() == null ? null : selectedTpHnLangsiran.getTph(),
                                            waktuAngkut,
                                            waktuAngkut,
                                            etNoNFC.getText().toString());
            idNfc.add(etNoNFC.getText().toString());
        }else{
            manualSPBData = new ManualSPB(  idManualAngkut,
                                            GlobalHelper.getUser().getUserID(),
                                            GlobalHelper.getEstate().getEstCode(),
                                            typeId,
                                            lsStatus.getText().toString(),
                    selectedTpHnLangsiran == null ? null : selectedTpHnLangsiran.getTph() == null ? null : selectedTpHnLangsiran.getTph().getBlock(),
                    selectedTpHnLangsiran == null ? null : selectedTpHnLangsiran.getTph() == null ? null : selectedTpHnLangsiran.getTph(),
                                            waktuAngkut,
                                            waktuAngkut);
        }

        Angkut angkut = new Angkut( idTransaksi,
                waktuAngkut,
                selectedTransaksiPanen,
                transaksiAngkutNFC,
                latitude,
                longitude,
                selectedTpHnLangsiran == null ? null : selectedTpHnLangsiran.getTph() == null ? null : selectedTpHnLangsiran.getTph().getBlock(),
                selectedTpHnLangsiran == null ? null : selectedTpHnLangsiran.getTph() == null ? null : selectedTpHnLangsiran.getTph(),
                selectedTpHnLangsiran == null ? null : selectedTpHnLangsiran.getLangsiran() == null ? null : selectedTpHnLangsiran.getLangsiran(),
                sFoto, manualSPBData);
        angkut.setIdAngkutRef(idTransaksi);
        if(idNfc.size() > 0){
            angkut.setIdNfc(idNfc);
        }

        Nitrite db = null;
        if(angkutActivity != null){
            db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT);
        }else if (spbActivity != null){
            db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT_SPB);
        }else if (mekanisasiActivity != null){
            db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI);
        }else{
            return;
        }
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(angkut.getIdAngkut(), gson.toJson(angkut),angkut.getIdAngkut());

        //tambahan zendi tambah input TableRekosilasi
//        Nitrite dbManualSPB = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_MANUAL_SPB);
//        ObjectRepository<DataNitrit> repoManualSPB = dbManualSPB.getRepository(DataNitrit.class);
//        DataNitrit dataNitritManualSPB = new DataNitrit(manualSPBData.getId(), gson.toJson(manualSPBData));
        if (selectedAngkut == null) {
            repository.insert(dataNitrit);
            if(angkutActivity != null) {
                GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_ANGKUT);
            }else if (spbActivity != null){
                GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_ANGKUT_SPB);
            }else if (mekanisasiActivity != null){
                GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_ANGKUT_SPB);
            }
//            repoManualSPB.insert(dataNitritManualSPB);
//            GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_MANUAL_SPB);
        } else {
            repository.update(dataNitrit);
//            repository.update(dataNitritManualSPB);
        }
        db.close();

        if (saveOnly) {
            Log.d(this.getClass() + " tapNFC", "Close0");
            if(angkutActivity != null) {
                angkutActivity.backProses();
            }else if(spbActivity != null){
                spbActivity.backProses();
            }else if(mekanisasiActivity != null){
                mekanisasiActivity.backProses();
            }
        }
    }

    private void hapusDataT2(){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("param1", selectedAngkut.getIdAngkutRef()));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                repository.remove(dataNitrit);
            }
        }else{
            Gson gson = new Gson();
            DataNitrit dataNitrit = new DataNitrit(selectedAngkut.getIdAngkut(),gson.toJson(selectedAngkut));
            repository.remove(dataNitrit);
        }
        if(selectedAngkut.getTransaksiPanen() != null){
            if(selectedAngkut.getTransaksiPanen().getIdTPanen() != null){
                TransaksiAngkutHelper.removeIdTPanenHasBeenTap(selectedAngkut.getTransaksiPanen().getIdTPanen());
            }
        }else if (selectedAngkut.getTransaksiAngkut() != null){
            if(selectedAngkut.getTransaksiAngkut().getIdTAngkut() != null){
                TransaksiAngkutHelper.removeIdTAngkutHasBeenTap(selectedAngkut.getTransaksiAngkut().getIdTAngkut());
            }
        }
        db.close();
        angkutActivity.backProses();
    }

//    private void saveTransaksiPanenQc(){
//        HashSet<String> sFoto = new HashSet<>();
//        if(ALselectedImage.size() > 0){
//            for(File file : ALselectedImage){
//                sFoto.add(file.getAbsolutePath());
//            }
//        }
//
//        String idTransaksi = selectedTransaksiPanen.getIdTPanen();
//
//        Boolean alas = true;
//        if(lsAlasBrondol.getText().toString().equalsIgnoreCase("no")){
//            alas = false;
//        }
//        HasilPanen hasilPanen = new HasilPanen(
//                Integer.parseInt(etNormal.getText().toString().isEmpty() ? "0" : etNormal.getText().toString()),
//                Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()),
//                Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()),
//                Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()),
//                Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString()),
//                Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()),
//                Integer.parseInt(etBuahDimakanTikus.getText().toString().isEmpty() ? "0" : etBuahDimakanTikus.getText().toString()),
//                Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()),
//                totalJjgJelek,
//                totalJjg,
//                totalJjgPendapatan,
//                waktuPanen,
//                alas
//        );
//
//        int Status = TransaksiPanen.TAP;
//
//        ArrayList<SubPemanen> subPemanens= new ArrayList<>();
//        for(int i = 0; i < rvPemanen.getAdapter().getItemCount(); i++){
//            RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
//            View view = viewHolder.itemView;
//            TextView tvValue = view.findViewById(R.id.tvValue);
//            AppCompatEditText etPersentase = view.findViewById(R.id.etPersentase);
//            AppCompatEditText etNormalSubPemanen = view.findViewById(R.id.etNormalSubPemanen);
//            AppCompatEditText etBuahMentahSubPemanen = view.findViewById(R.id.etBuahMentahSubPemanen);
//            AppCompatEditText etBusukSubPemanen = view.findViewById(R.id.etBusukSubPemanen);
//            AppCompatEditText etLewatMatangSubPemanen = view.findViewById(R.id.etLewatMatangSubPemanen);
//            AppCompatEditText etAbnormalSubPemanen = view.findViewById(R.id.etAbnormalSubPemanen);
//            AppCompatEditText etTangkaiPanjangSubPemanen = view.findViewById(R.id.etTangkaiPanjangSubPemanen);
//            AppCompatEditText etBuahDimakanTikusSubPemanen = view.findViewById(R.id.etBuahDimakanTikusSubPemanen);
//            AppCompatEditText etTotalJanjangSubPemanen = view.findViewById(R.id.etTotalJanjangSubPemanen);
//            AppCompatEditText etTotalBrondolanSubPemanen = view.findViewById(R.id.etTotalBrondolanSubPemanen);
//            AppCompatEditText etTotalPdtSubPemanen = view.findViewById(R.id.etTotalPdtSubPemanen);
//            CompleteTextViewHelper lsTipe = view.findViewById(R.id.lsTipe);
//
//            Gson gson = new Gson();
////            int jjg = Integer.parseInt(etTotalJanjangSubPemanen.getText().toString());
////            int brondolan = Integer.parseInt(etTotalBrondolanSubPemanen.getText().toString());
////            int jjgPdt = Integer.parseInt(etTotalPdtSubPemanen.getText().toString());
////            int buahMentah = Integer.parseInt(etBuahMentahSubPemanen.getText().toString());
////            int tangkaiPanjang = Integer.parseInt(etTangkaiPanjangSubPemanen.getText().toString());
//            HasilPanen hasilPanenSubPemanen = new HasilPanen(
//                    Integer.parseInt(etNormalSubPemanen.getText().toString()),
//                    Integer.parseInt(etBuahMentahSubPemanen.getText().toString()),
//                    Integer.parseInt(etBusukSubPemanen.getText().toString()),
//                    Integer.parseInt(etLewatMatangSubPemanen.getText().toString()),
//                    Integer.parseInt(etAbnormalSubPemanen.getText().toString()),
//                    Integer.parseInt(etTangkaiPanjangSubPemanen.getText().toString()),
//                    Integer.parseInt(etBuahDimakanTikusSubPemanen.getText().toString()),
//                    Integer.parseInt(etTotalBrondolanSubPemanen.getText().toString()),
//                    0,
//                    Integer.parseInt(etTotalJanjangSubPemanen.getText().toString()),
//                    Integer.parseInt(etTotalPdtSubPemanen.getText().toString())
//            );
//            Pemanen pemanen = gson.fromJson(tvValue.getText().toString(),Pemanen.class);
//
//            if(pemanenSisa != null) {
//                if (pemanen.getKodePemanen().equalsIgnoreCase(pemanenSisa.getKodePemanen())) {
//                    hasilPanenSubPemanen = transaksiPanenHelper.combineHasilPanen(hasilPanenSubPemanen,sisaJjgBrdl);
////                    jjg += sisaJjgBrdl.getTotalJanjang();
////                    brondolan += sisaJjgBrdl.getBrondolan();
////                    jjgPdt += sisaJjgBrdl.getTotalJanjangPendapatan();
////                    buahMentah += sisaJjgBrdl.getBuahMentah();
////                    tangkaiPanjang += sisaJjgBrdl.getTangkaiPanjang();
//                }
//            }
//
//            int tipe = SubPemanen.idPemnaen;
//            if(lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperator])){
//                tipe = SubPemanen.idOperator;
//            }
//
//            subPemanens.add(new SubPemanen(i,
//                    GlobalHelper.getCharForNumber(i),
//                    Integer.parseInt(etPersentase.getText().toString()),
//                    hasilPanenSubPemanen,
//                    tipe,
//                    pemanen)
//            );
//
//            if(i == 0 ){
//                pemanenSelected = gson.fromJson(tvValue.getText().toString(),Pemanen.class);
//            }
//        }
//
//        Kongsi kongsi = new Kongsi(idKongsi,subPemanens);
//
//        TransaksiPanen transaksiPanen = new TransaksiPanen(idTransaksi,
//                latitude,
//                longitude,
//                GlobalHelper.getUser().getUserID(),
//                System.currentTimeMillis(),
//                GlobalHelper.getUser().getUserID(),
//                System.currentTimeMillis(),
//                tphSelected.getAncak(),
//                "",
//                pemanenSelected,
//                tphSelected,
//                hasilPanen,
//                sFoto,
//                Status,
//                0,
//                kongsi,
//                spkSelected);
//
//        QcMutuBuah qcMutuBuah = new QcMutuBuah(
//                "QC1_"+ GlobalHelper.getUser().getUserID()+"_"+ idTransaksi,
//                idTransaksi,
//                selectedTransaksiPanen,transaksiPanen);
//
//        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BUAH);
//
//        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
//        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", qcMutuBuah.getIdQCMutuBuah()));
//        Gson gson = new Gson();
//        DataNitrit dataNitrit = new DataNitrit(qcMutuBuah.getIdQCMutuBuah(),gson.toJson(qcMutuBuah),qcMutuBuah.getIdTPanen());
//        if(cursor.size() > 0) {
//            repository.update(dataNitrit);
//        }else{
//            repository.insert(dataNitrit);
//        }
//        db.close();
//        selectedTransaksiPanen = transaksiPanen;
//        qcMutuBuahActivity.backProses();
//    }

    private void hapusDataQc(TransaksiPanen transaksiPanen){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BUAH);
        String idQcBuah = "QC1_"+ GlobalHelper.getUser().getUserID()+"_"+ transaksiPanen.getIdTPanen();
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", idQcBuah));
        if(cursor.size() > 0){
            for (Iterator iterator = cursor.iterator(); iterator.hasNext();) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                repository.remove(dataNitrit);
                break;
            }
        }
        db.close();
        qcMutuBuahActivity.backProses();
    }

    private void setTotalJjgJelek(boolean withEditText){
        totalJjgJelek = Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()) +
                Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()) +
//                Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()) +
                Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()) +
                Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString());
        if (withEditText) {
            etTotalJanjangBuruk.setText(String.valueOf(totalJjgJelek));
        }
    }

    private void setTotalJjg(boolean withEditText) {
        totalJjg = GlobalHelper.getTotalJanjang(
                    new HasilPanen(
                            Integer.parseInt(etNormal.getText().toString().isEmpty() ? "0" : etNormal.getText().toString()),
                            Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()),
                            Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()),
                            Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()),
                            Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString()),
                            Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()),
                            Integer.parseInt(etBuahDimakanTikus.getText().toString().isEmpty() ? "0" : etBuahDimakanTikus.getText().toString()),
                            Integer.parseInt(etTotalJanjang.getText().toString().isEmpty() ? "0" : etTotalJanjang.getText().toString()),
                            Integer.parseInt(etTotalJanjangPendapatan.getText().toString().isEmpty() ? "0" : etTotalJanjangPendapatan.getText().toString()),
                            Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()),
                            !lsAlasBrondol.getText().toString().equalsIgnoreCase("no")
                    )
                );
        if (withEditText) {
            etTotalJanjang.setText(String.valueOf(totalJjg));
        }
    }

    private void setJjgNormal(boolean withEditText) {
         int jjgT = GlobalHelper.getTotalJanjang(
                new HasilPanen(
                        0,
                        Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()),
                        Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()),
                        Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()),
                        Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString()),
                        Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()),
                        Integer.parseInt(etBuahDimakanTikus.getText().toString().isEmpty() ? "0" : etBuahDimakanTikus.getText().toString()),
                        Integer.parseInt(etTotalJanjang.getText().toString().isEmpty() ? "0" : etTotalJanjang.getText().toString()),
                        Integer.parseInt(etTotalJanjangPendapatan.getText().toString().isEmpty() ? "0" : etTotalJanjangPendapatan.getText().toString()),
                        Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()),
                        !lsAlasBrondol.getText().toString().equalsIgnoreCase("no")
                )
        );
//        int jjgT = GlobalHelper.getTotalJanjangPendapatan(new HasilPanen(
//                        0,
//                        Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()),
//                        Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()),
//                        Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()),
//                        Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString()),
//                        Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()),
//                        Integer.parseInt(etBuahDimakanTikus.getText().toString().isEmpty() ? "0" : etBuahDimakanTikus.getText().toString()),
//                        Integer.parseInt(etTotalJanjang.getText().toString().isEmpty() ? "0" : etTotalJanjang.getText().toString()),
//                        Integer.parseInt(etTotalJanjangPendapatan.getText().toString().isEmpty() ? "0" : etTotalJanjangPendapatan.getText().toString()),
//                        Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()),
//                        lsAlasBrondol.getText().toString().equalsIgnoreCase("no") ? false : true
//                ),dynamicParameterPenghasilan
//        );
        totalJjg = Integer.parseInt(etTotalJanjang.getText().toString().isEmpty() ? "0" : etTotalJanjang.getText().toString());

        if (withEditText) {
//            if(total == 0){
//                etMentah.setText("0");
//                etBusuk.setText("0");
//                etLewatMatang.setText("0");
//                etAbnormal.setText("0");7
//                etTangkaiPanjang.setText("0");
//                etBuahDimakanTikus.setText("0");
//                etTotalJanjang.setText("0");
//                etTotalJanjangPendapatan.setText("0");
//            }
            etNormal.setText(String.valueOf(totalJjg - jjgT));
        }
    }

    private void setTotalJjgPendapatan(boolean withEditText) {

        totalJjgPendapatan = GlobalHelper.getTotalJanjangPendapatan(new HasilPanen(
                Integer.parseInt(etNormal.getText().toString().isEmpty() ? "0" : etNormal.getText().toString()),
                Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()),
                Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()),
                Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()),
                Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString()),
                Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()),
                Integer.parseInt(etBuahDimakanTikus.getText().toString().isEmpty() ? "0" : etBuahDimakanTikus.getText().toString()),
                Integer.parseInt(etTotalJanjang.getText().toString().isEmpty() ? "0" : etTotalJanjang.getText().toString()),
                Integer.parseInt(etTotalJanjangPendapatan.getText().toString().isEmpty() ? "0" : etTotalJanjangPendapatan.getText().toString()),
                Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()),
                !lsAlasBrondol.getText().toString().equalsIgnoreCase("no")
                ),dynamicParameterPenghasilan
        );
        if (withEditText) {
            etTotalJanjangPendapatan.setText(String.valueOf(totalJjgPendapatan));
        }
    }

    private void infoPanenPopUp(int param){
        String message = "";
        switch (param){
            case infoTotalJanjang:
                message = getActivity().getResources().getString(R.string.info_total_janjang);
                break;
            case infoTotalJanjangPendapatan:
                message = getActivity().getResources().getString(R.string.info_total_janjang_pendapatan);
                break;
            case infoNormal:
                message = getActivity().getResources().getString(R.string.info_tbs_normal);
                break;
            case infoMentah:
                message = getActivity().getResources().getString(R.string.info_tbs_mentah);
                break;
            case infoBusuk:
                message = getActivity().getResources().getString(R.string.info_tbs_busuk);
                break;
            case infoLewatMatang:
                message = getActivity().getResources().getString(R.string.info_tbs_lewat_matang);
                break;
            case infoAbnormal:
                message = getActivity().getResources().getString(R.string.info_tbs_abnormal);
                break;
            case infoTangkaiPanjang:
                message = getActivity().getResources().getString(R.string.info_tbs_tangkai);
                break;
            case infoBuahDimakanTikus:
                message = getActivity().getResources().getString(R.string.info_tbs_bdt);
                break;
        }
        WidgetHelper.showOKDialog(getActivity(),
                "Info",
                message,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }
        );
    }

    private void setUpValueTransaksiPanen(){
        latitude = selectedTransaksiPanen.getLatitude();
        longitude = selectedTransaksiPanen.getLongitude();
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy");
        String alas;
        waktuPanen = selectedTransaksiPanen.getHasilPanen().getWaktuPanen();
        if(selectedTransaksiPanen.getHasilPanen().isAlasanBrondolan()){
            alas = "Yes";
        }else{
            alas = "No";
        }

        ALselectedImage.clear();
        if(selectedTransaksiPanen.getFoto() != null) {
            for (String s : selectedTransaksiPanen.getFoto()) {
                File file = new File(s);
                ALselectedImage.add(file);
            }
        }
        setupimageslide();

//        List<String> lGang = new ArrayList<>();
//        lGang.add(selectedTransaksiPanen.getPemanen().getGank());
//        ArrayAdapter<String> adapterGang = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_dropdown_item_1line, lGang);
//        etGang.setAdapter(adapterGang);
//        etGang.setText(lGang.get(0));

        checkBoxTphBayanganEfect = false;
        if(selectedTransaksiPanen.getPemanen() != null) {
            setPemanen(selectedTransaksiPanen.getPemanen());
        }else if (selectedTransaksiPanen.getKongsi().getSubPemanens() != null){
            if(selectedTransaksiPanen.getKongsi().getSubPemanens().size() > 0) {
                setPemanen(selectedTransaksiPanen.getKongsi().getSubPemanens().get(0).getPemanen());
            }
        }
        setTph(selectedTransaksiPanen.getTph());
        etAncak.setText(selectedTransaksiPanen.getAncak());
        checkBoxTphBayanganEfect = true;

        if(selectedTransaksiPanen.getReasonTphBayangan() != null){
            if(!selectedTransaksiPanen.getReasonTphBayangan().getNameReason().isEmpty()) {
                lReasonTphBayangan.setVisibility(View.VISIBLE);
                lsReasonTphBayangan.setText(ReasonTphBayangan.listReason.get(selectedTransaksiPanen.getReasonTphBayangan().getIdReason()));
                if (selectedTransaksiPanen.getReasonTphBayangan().getIdReason() == ReasonTphBayangan.idReasonOthers) {
                    etReasonTphBayangan.setVisibility(View.VISIBLE);
                    etReasonTphBayangan.setText(selectedTransaksiPanen.getReasonTphBayangan().getNameReason());
                } else {
                    etReasonTphBayangan.setVisibility(View.GONE);
                }
            }
        }
//        etBaris.setText(selectedTransaksiPanen.getBaris());
        hasilPanenStamp = selectedTransaksiPanen.getHasilPanen();
        Log.d(TAG, "hasilPanenStamp: 2 update hasilPanenStamp " + String.valueOf(hasilPanenStamp.getTotalJanjang()) );
//        etTotalJanjang.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getTotalJanjang()));
//        etTotalJanjangPendapatan.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getTotalJanjangPendapatan()));
        etNormal.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getJanjangNormal()));
        etMentah.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getBuahMentah()));
        etBusuk.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getBusukNJangkos()));
        etLewatMatang.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getBuahLewatMatang()));
        etAbnormal.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getBuahAbnormal()));
        etTangkaiPanjang.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getTangkaiPanjang()));
        etBuahDimakanTikus.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getBuahDimakanTikus()));
        etBrondolan.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getBrondolan()));
        etTotalJanjangBuruk.setText(String.valueOf(selectedTransaksiPanen.getHasilPanen().getTotalJanjangBuruk()));
        etJarak.setText(sdfT.format(waktuPanen));
        lsAlasBrondol.setText(alas);

        idKongsi = selectedTransaksiPanen.getKongsi().getTipeKongsi();
        tipeKongsi.setPosition(idKongsi);

        sharedPrefHelper.commit(SharedPrefHelper.KEY_TipeKongsi,String.valueOf(idKongsi));
        pemanenOperator = new HashMap<>();
        pemanenOperatorTonase = new HashMap<>();
        lSisa.setVisibility(View.GONE);

        for(int i = 0 ; i < selectedTransaksiPanen.getKongsi().getSubPemanens().size();i++){
            SubPemanen subPemanen = selectedTransaksiPanen.getKongsi().getSubPemanens().get(i);
            if(subPemanen.getTipePemanen() == SubPemanen.idOperator) {
                pemanenOperator.put(subPemanen.getPemanen().getKodePemanen(),subPemanen.getPemanen());
            }else if (subPemanen.getTipePemanen() == SubPemanen.idOperatorTonase){
                pemanenOperatorTonase.put(subPemanen.getPemanen().getKodePemanen(),subPemanen.getPemanen());
            }
        }

        spkSelected = selectedTransaksiPanen.getSpk();
        if(spkSelected != null){
            cbBorongan.setChecked(true);
            lBorongan.setVisibility(View.VISIBLE);
            tvOa.setVisibility(View.VISIBLE);
            pemanenInfo.setVisibility(View.GONE);
            etSpk.setText(spkSelected.getIdSPK());
            tvOa.setText(TransaksiPanenHelper.showSPK(spkSelected));
        }

        if(selectedTransaksiPanen.getKongsi().getPemanenGerdang() != null){
            Gerdang dariMaster  = currentGerdang.get(pemanenSelected.getNik());

            if(dariMaster != null){
                pemanenGerdang = dariMaster.getPemanenGerdang();
            }else{
                pemanenGerdang = selectedTransaksiPanen.getKongsi().getPemanenGerdang();
            }

            cbGerdang.setChecked(true);
            lGerdang.setVisibility(View.VISIBLE);
            etGangGerdang.setText(pemanenGerdang.getGank());
            pemanenGerdangHelper.setAdapterPemanenGerdang(pemanenGerdang.getGank());
            pemanenGerdangHelper.setPemanen(pemanenGerdang);
        }

        if(selectedTransaksiPanen.getSubstitute() != null){
            substituteSelected = selectedTransaksiPanen.getSubstitute();
            etSubstitute.setText(substituteSelected.getNama());
            cbKraniSubstitute.setChecked(true);
            lSubstitute.setVisibility(View.VISIBLE);
        }else{
            substituteSelected = null;
            etSubstitute.setText("");
            cbKraniSubstitute.setChecked(false);
            lSubstitute.setVisibility(View.GONE);
        }

        if(selectedTransaksiPanen.getVehicleMekanisasi() != null){
            vehicleMekanisasi = selectedTransaksiPanen.getVehicleMekanisasi();
            typeVehicleMekanisasi = selectedTransaksiPanen.getTypeVehicleMekanisasi();
            lVehicleMekanisasi.setVisibility(View.VISIBLE);
            if(typeVehicleMekanisasi == vehicleMekanisasiHelper.freeTextIdx){
                lsKendaraanMekanisasi.setText(vehicleMekanisasiHelper.freeText);
                etVehicleMekanisasi.setText(vehicleMekanisasi.getVehicleCode());
            }else if (typeVehicleMekanisasi == vehicleMekanisasiHelper.masterIdx){
                lsKendaraanMekanisasi.setText(vehicleMekanisasiHelper.master);
                etVehicleMekanisasi.setText(vehicleMekanisasi.getVehicleName());
            }
        }

        if(idKongsi == Kongsi.idMekanis){
            lVehicleMekanisasi.setVisibility(View.VISIBLE);
            lGerdang.setVisibility(View.GONE);
        }

        etRemarks.setText(selectedTransaksiPanen.getRemarks());
        setTotalJjg(true);
        setTotalJjgPendapatan(true);
    }

    private void setUpValueAngkut(){
        ALselectedImage.clear();
        if(selectedAngkut.getFoto() != null) {
            for (String s : selectedAngkut.getFoto()) {
                File file = new File(s);
                ALselectedImage.add(file);
            }
        }
        setupimageslide();

        latitude = selectedAngkut.getLatitude();
        longitude = selectedAngkut.getLongitude();
        if(selectedAngkut.getTransaksiPanen() == null){
            etBlock.setText(selectedAngkut.getBlock());
            blockName = selectedAngkut.getBlock();
            Log.i(TAG, "blockName: "+blockName);
            if(selectedAngkut.getTph() != null) {
                setDropDownTphnLangsiran();
                etNotph.setText(selectedAngkut.getTph().getNamaTph());
                selectedTpHnLangsiran = new TPHnLangsiran(selectedAngkut.getTph().getNoTph(),selectedAngkut.getTph());
            }else if (selectedAngkut.getLangsiran() != null){
                setDropDownTphnLangsiran();
                etNotph.setText(selectedAngkut.getLangsiran().getNamaTujuan()); //disin
                selectedTpHnLangsiran = new TPHnLangsiran(selectedAngkut.getLangsiran().getIdTujuan(),selectedAngkut.getLangsiran());
            }
            etNormal.setText(String.valueOf(selectedAngkut.getJanjang()));
            etBrondolan.setText(String.valueOf(selectedAngkut.getBrondolan()));
            etMentah.setText(String.format("%.0f",selectedAngkut.getBerat()));
        }

        if(lsave.getVisibility() == View.VISIBLE) {
//            lldeleted.setVisibility(View.VISIBLE);
            if(selectedAngkut.getTransaksiAngkut() != null){
                lsave.setVisibility(View.GONE);
                llkeyBoard.setVisibility(View.GONE);

                //jika inggin update angkut tipe transaksi angkut
                if(selectedTransaksiAngkut.getTranskasiAngkutVersions() != null) {
                    if (selectedTransaksiAngkut.getTranskasiAngkutVersions().size() > 0) {
                        historyTransaksi.setVisibility(View.VISIBLE);
                        ketAction3.setVisibility(View.VISIBLE);
                    }
                }
            }
        }

        //tambahan zendi penambahan set value bila manual angkut
        if(selectedAngkut.getManualSPBData()!=null){
            lStatus.setVisibility(View.VISIBLE);
            lsStatus.setText(""+selectedAngkut.getManualSPBData().getTipe());
            if(selectedAngkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                lNoKartu.setVisibility(View.VISIBLE);
                etNoNFC.setText(""+selectedAngkut.getManualSPBData().getCardId());
            }else{
                lNoKartu.setVisibility(View.GONE);
            }
            lInfoPemanen.setVisibility(View.GONE);
            vTphPanen.setVisibility(View.GONE);
            vTphphoto.setVisibility(View.GONE);
            ketAction2.setVisibility(View.GONE);
        }
    }

    private void setNearestBlock(){
        if(tphActivity != null && selectedTransaksiPanen == null ){
            if (tphActivity.nearestBlock != null) {
//                latitude = tphActivity.nearestBlock.getLocation().getLatitude();
//                longitude = tphActivity.nearestBlock.getLocation().getLongitude();

                ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
                        (getActivity(),android.R.layout.select_dialog_item, tphActivity.nearestBlock.getBlocks().toArray(new String[tphActivity.nearestBlock.getBlocks().size()]));

                etBlock.setThreshold(1);
                etBlock.setAdapter(adapterBlock);
                if(tphActivity.nearestBlock.getBlocks().size() == 1) {
                    etBlock.setText(tphActivity.nearestBlock.getBlock());
                }

                baseActivity.currentlocation = tphActivity.nearestBlock.getLocation();
//                setDropdownTph();
            } else {
                if(tphActivity != null) {
                    WidgetHelper.warningFindGps(tphActivity,tphActivity.myCoordinatorLayout);
                }
            }

            SharedPreferences prefAFDPanen = HarvestApp.getContext().getSharedPreferences(HarvestApp.AFDELING_PANEN, Context.MODE_PRIVATE);
            afdelingPanen = prefAFDPanen.getString(HarvestApp.AFDELING_PANEN,null);
        }
    }

    private void updateNearestBlock(){
        if(tphActivity != null && selectedTransaksiPanen == null ){
            if (baseActivity.currentLocationOK()) {
                latitude = baseActivity.currentlocation.getLatitude();
                longitude = baseActivity.currentlocation.getLongitude();

                Set<String> allBlock = GlobalHelper.getNearBlock(kml,latitude,longitude);
                Set<String> bloks = new ArraySet<>();
                for(String est : allBlock){
                    try {
                        String [] split = est.split(";");
                        bloks.add(split[2]);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
                        (tphActivity,android.R.layout.select_dialog_item, bloks.toArray(new String[bloks.size()]));

                etBlock.setThreshold(1);
                etBlock.setAdapter(adapterBlock);
                try {
                    String [] estAfdBlock = GlobalHelper.getAndAssignBlock(kml,latitude,longitude).split(";");
                    if(bloks.size() == 1) {
                        etBlock.setText(estAfdBlock[2]);
                        setDropdownTph();
                    }else {
                        etBlock.showDropDown();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            } else {
                if(tphActivity != null) {
                    WidgetHelper.warningFindGps(tphActivity,tphActivity.myCoordinatorLayout);
                }
            }
        }
    }

    public void setPemanen(Pemanen pemanen){
//        etPemanen.setText(pemanen.getNama());
        pemanenSelected = pemanen;
        if(stampImage == null){
            stampImage = new StampImage();
        }
        stampImage.setPemanen(pemanen);
    }

    private void takePicture(){
        if(tphActivity != null) {
            if(tphSelected != null && baseActivity.currentlocation != null) {
                if(tphSelected.getLatitude() != null ) {
                    if (GlobalHelper.distance(tphSelected.getLatitude(), baseActivity.currentlocation.getLatitude(),
                            tphSelected.getLongitude(), baseActivity.currentlocation.getLongitude()) > GlobalHelper.RADIUS) {
                        Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.radius_to_long_with_tph), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }else{
                    Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.chose_tph),Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.chose_tph),Toast.LENGTH_SHORT).show();
            }
            saveSharePrefPanen();
        }

        if(hasilPanenStamp != null){
            if(hasilPanenStamp.getTotalJanjang() > 0){
                if(stampImage != null) {
                    stampImage.setHasilPanen(hasilPanenStamp);
                }else{
                    return;
                }
            }
        }
        Gson gson  = new Gson();
        SharedPrefHelper.applySharepref(HarvestApp.STAMP_IMAGES,gson.toJson(stampImage));

        GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_TRANSAKSI_TPH;
        EasyImage.openCamera(getActivity(),1);

    }

    private void clearImages(){
        ALselectedImage = new ArrayList<>();
        SharedPrefHelper.applySharepref(HarvestApp.FOTO_PANEN_PREF,null);
    }

//    private void showmapTPH(){
//        popupTPH = true;
//        new LongOperation(getView()).execute();
//    }

    public void saveSharePrefPanenFoto(){
        JSONArray jsonArray = new JSONArray();
        for (int i = 0 ; i < ALselectedImage.size(); i++){
            File file = ALselectedImage.get(i);
            if(file.exists()){
                jsonArray.put(file.getAbsolutePath());
            }
        }

        SharedPrefHelper.applySharepref(HarvestApp.FOTO_PANEN_PREF,jsonArray.toString());
    }

    public void saveSharePrefPanen(){
        Boolean alas = true;
        String remarks = etRemarks.getText().toString();
        if(lsAlasBrondol.getText().toString().equalsIgnoreCase("no")){
            alas = false;
        }

        HasilPanen hasilPanen = new HasilPanen(
                Integer.parseInt(etNormal.getText().toString().isEmpty() ? "0" : etNormal.getText().toString()),
                Integer.parseInt(etMentah.getText().toString().isEmpty() ? "0" : etMentah.getText().toString()),
                Integer.parseInt(etBusuk.getText().toString().isEmpty() ? "0" : etBusuk.getText().toString()),
                Integer.parseInt(etLewatMatang.getText().toString().isEmpty() ? "0" : etLewatMatang.getText().toString()),
                Integer.parseInt(etAbnormal.getText().toString().isEmpty() ? "0" : etAbnormal.getText().toString()),
                Integer.parseInt(etTangkaiPanjang.getText().toString().isEmpty() ? "0" : etTangkaiPanjang.getText().toString()),
                Integer.parseInt(etBuahDimakanTikus.getText().toString().isEmpty() ? "0" : etBuahDimakanTikus.getText().toString()),
                Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()),
                totalJjgJelek,
                totalJjg,
                totalJjgPendapatan,
                waktuPanen,
                alas
        );

        ArrayList<SubPemanen> subPemanens= new ArrayList<>();
        if(spkSelected != null ){
            idKongsi = Kongsi.idNone;
            subPemanens.add(new SubPemanen(0,
                    GlobalHelper.getCharForNumber(0),
                    100,
                    hasilPanen,
                    SubPemanen.idPemnaen,
                    pemanenSelected)
            );
        }else {
            for (int i = 0; i < rvPemanen.getAdapter().getItemCount(); i++) {
                RecyclerView.ViewHolder viewHolder = rvPemanen.findViewHolderForAdapterPosition(i);
                View view = viewHolder.itemView;
                TextView tvValue = view.findViewById(R.id.tvValue);
                AppCompatEditText etPersentase = view.findViewById(R.id.etPersentase);
                AppCompatEditText etNormalSubPemanen = view.findViewById(R.id.etNormalSubPemanen);
                AppCompatEditText etBuahMentahSubPemanen = view.findViewById(R.id.etBuahMentahSubPemanen);
                AppCompatEditText etBusukSubPemanen = view.findViewById(R.id.etBusukSubPemanen);
                AppCompatEditText etLewatMatangSubPemanen = view.findViewById(R.id.etLewatMatangSubPemanen);
                AppCompatEditText etAbnormalSubPemanen = view.findViewById(R.id.etAbnormalSubPemanen);
                AppCompatEditText etTangkaiPanjangSubPemanen = view.findViewById(R.id.etTangkaiPanjangSubPemanen);
                AppCompatEditText etBuahDimakanTikusSubPemanen = view.findViewById(R.id.etBuahDimakanTikusSubPemanen);
                AppCompatEditText etTotalJanjangSubPemanen = view.findViewById(R.id.etTotalJanjangSubPemanen);
                AppCompatEditText etTotalBrondolanSubPemanen = view.findViewById(R.id.etTotalBrondolanSubPemanen);
                AppCompatEditText etTotalPdtSubPemanen = view.findViewById(R.id.etTotalPdtSubPemanen);
                CompleteTextViewHelper lsTipe = view.findViewById(R.id.lsTipe);

                Gson gson = new Gson();
//                int jjg = Integer.parseInt(etTotalJanjangSubPemanen.getText().toString());
//                int brondolan = Integer.parseInt(etTotalBrondolanSubPemanen.getText().toString());
//                int jjgPdt = Integer.parseInt(etTotalPdtSubPemanen.getText().toString());
//                int jjgBuahMentah = Integer.parseInt(etBuahMentahSubPemanen.getText().toString());
//                int jjgTangkaiPanjang = Integer.parseInt(etTangkaiPanjangSubPemanen.getText().toString());
                HasilPanen hasilPanenSubPemanen = new HasilPanen(
                        Integer.parseInt(etNormalSubPemanen.getText().toString()),
                        Integer.parseInt(etBuahMentahSubPemanen.getText().toString()),
                        Integer.parseInt(etBusukSubPemanen.getText().toString()),
                        Integer.parseInt(etLewatMatangSubPemanen.getText().toString()),
                        Integer.parseInt(etAbnormalSubPemanen.getText().toString()),
                        Integer.parseInt(etTangkaiPanjangSubPemanen.getText().toString()),
                        Integer.parseInt(etBuahDimakanTikusSubPemanen.getText().toString()),
                        Integer.parseInt(etTotalBrondolanSubPemanen.getText().toString()),
                        0,
                        Integer.parseInt(etTotalJanjangSubPemanen.getText().toString()),
                        Integer.parseInt(etTotalPdtSubPemanen.getText().toString())
                );
                Pemanen pemanen = gson.fromJson(tvValue.getText().toString(), Pemanen.class);

                if (pemanenSisa != null) {
                    if (pemanen.getKodePemanen().equalsIgnoreCase(pemanenSisa.getKodePemanen())) {
                        hasilPanenSubPemanen = transaksiPanenHelper.combineHasilPanen(hasilPanenSubPemanen,sisaJjgBrdl);
//                        jjg += sisaJjgBrdl.getTotalJanjang();
//                        brondolan += sisaJjgBrdl.getBrondolan();
//                        jjgPdt += sisaJjgBrdl.getTotalJanjangPendapatan();
//                        jjgBuahMentah += sisaJjgBrdl.getBuahMentah();
//                        jjgTangkaiPanjang += sisaJjgBrdl.getTangkaiPanjang();
                    }
                }

                int tipe = SubPemanen.idPemnaen;
                if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperator])) {
                    tipe = SubPemanen.idOperator;
                }
                if (lsTipe.getText().toString().equals(SubPemanen.arrayCodeTipePemanen[SubPemanen.idOperatorTonase])) {
                    tipe = SubPemanen.idOperatorTonase;
                }

                subPemanens.add(new SubPemanen(i,
                        GlobalHelper.getCharForNumber(i),
                        Integer.parseInt(etPersentase.getText().toString()),
                        hasilPanenSubPemanen,
                        tipe,
                        pemanen)
                );

                if (i == 0) {
                    pemanenSelected = pemanen;
                }
            }
        }
        Kongsi kongsi = new Kongsi(idKongsi,subPemanens);
        if(pemanenGerdang != null){
            kongsi.setPemanenGerdang(pemanenGerdang);
        }

        TransaksiPanen transaksiPanen = new TransaksiPanen();
        transaksiPanen.setLatitude(latitude);
        transaksiPanen.setLongitude(longitude);
        transaksiPanen.setTph(tphSelected);
        transaksiPanen.setHasilPanen(hasilPanen);
        transaksiPanen.setKongsi(kongsi);
        transaksiPanen.setPemanen(pemanenSelected);
        transaksiPanen.setSpk(spkSelected);
        transaksiPanen.setRemarks(remarks);

        if(transaksiPanen.getLatitude() == 0.0 && transaksiPanen.getLongitude() == 0.0) {
            if (selectedTransaksiPanen != null) {
                transaksiPanen.setLatitude(selectedTransaksiPanen.getLatitude());
                transaksiPanen.setLongitude(selectedTransaksiPanen.getLatitude());
            }
        }

        if(cbTphBayangan.isChecked()){
            ReasonTphBayangan reasonTphBayangan = new ReasonTphBayangan();
            List<String> reasons = ReasonTphBayangan.listReason;
            for(int i = 0 ; i < reasons.size();i++){
                if(reasons.get(i).toLowerCase().equals(lsReasonTphBayangan.getText().toString().toLowerCase())){
                    reasonTphBayangan.setIdReason(i);
                    if(reasons.get(i).toLowerCase().equals(ReasonTphBayangan.nameReasonOthers.toLowerCase())){
                        reasonTphBayangan.setNameReason(etReasonTphBayangan.getText().toString());
                    }else{
                        reasonTphBayangan.setNameReason(lsReasonTphBayangan.getText().toString());
                    }
                }
            }
            if(!reasonTphBayangan.getNameReason().isEmpty()){
                transaksiPanen.setReasonTphBayangan(reasonTphBayangan);
                Log.d(TAG, "saveSharePrefPanen: "+ reasonTphBayangan);
            }
            Log.d(TAG, "saveSharePrefPanen: "+ reasonTphBayangan);
        }

        if(vehicleMekanisasi != null){
            if(lsKendaraanMekanisasi.getText().toString().equals(vehicleMekanisasiHelper.master)) {
                transaksiPanen.setTypeVehicleMekanisasi(vehicleMekanisasiHelper.masterIdx);
                transaksiPanen.setVehicleMekanisasi(vehicleMekanisasi);
            }else if(lsKendaraanMekanisasi.getText().toString().equals(vehicleMekanisasiHelper.freeText)) {
                transaksiPanen.setTypeVehicleMekanisasi(vehicleMekanisasiHelper.freeTextIdx);

                Vehicle vehicle = new Vehicle();
                vehicle.setVehicleCode(etVehicleMekanisasi.getText().toString().toUpperCase());
                transaksiPanen.setVehicleMekanisasi(vehicle);
            }
        }

        Gson gson  = new Gson();
        SharedPrefHelper.applySharepref(HarvestApp.TRANSAKSI_PANEN_PREF,gson.toJson(transaksiPanen));
    }

    public boolean showSharePref() {

        TransaksiPanen transaksiPanen = SharedPrefHelper.getSharePrefTransaksiPanen();

        if(transaksiPanen == null){
            return false;
        }

        if(transaksiPanen.getTph() == null){
            return false;
        }

        etNotph.setText(transaksiPanen.getTph().getNamaTph());
        etAncak.setText(transaksiPanen.getTph().getAncak());
        etBlock.setText(transaksiPanen.getTph().getBlock());
        tphSelected = transaksiPanen.getTph();
        blockSelected = transaksiPanen.getTph().getBlock();
        etTotalJanjang.requestFocus();

        if(transaksiPanen.getReasonTphBayangan() != null){
            if(!transaksiPanen.getReasonTphBayangan().getNameReason().isEmpty()) {
                cbTphBayangan.setChecked(true);
                lReasonTphBayangan.setVisibility(View.VISIBLE);
                lsReasonTphBayangan.setText(ReasonTphBayangan.listReason.get(transaksiPanen.getReasonTphBayangan().getIdReason()));
                if (transaksiPanen.getReasonTphBayangan().getIdReason() == ReasonTphBayangan.idReasonOthers) {
                    etReasonTphBayangan.setVisibility(View.VISIBLE);
                    etReasonTphBayangan.setText(transaksiPanen.getReasonTphBayangan().getNameReason());
                } else {
                    etReasonTphBayangan.setVisibility(View.GONE);
                }
            }
        }

        if(transaksiPanen.getRemarks() != null){
            etRemarks.setText(transaksiPanen.getRemarks());
        }

        if(transaksiPanen.getHasilPanen() != null){
            String alas;
            waktuPanen = transaksiPanen.getHasilPanen().getWaktuPanen();
            if(transaksiPanen.getHasilPanen().isAlasanBrondolan()){
                alas = "Yes";
            }else{
                alas = "No";
            }

//        etBaris.setText(selectedTransaksiPanen.getBaris());
            hasilPanenStamp = transaksiPanen.getHasilPanen();
            Log.d(TAG, "hasilPanenStamp: 3. update hasilPanenStamp " + String.valueOf(hasilPanenStamp.getTotalJanjang()) );

            etTotalJanjang.setText(String.valueOf(transaksiPanen.getHasilPanen().getTotalJanjang()));
            etTotalJanjangPendapatan.setText(String.valueOf(transaksiPanen.getHasilPanen().getTotalJanjangPendapatan()));
            etNormal.setText(String.valueOf(transaksiPanen.getHasilPanen().getJanjangNormal()));
            etMentah.setText(String.valueOf(transaksiPanen.getHasilPanen().getBuahMentah()));
            etBusuk.setText(String.valueOf(transaksiPanen.getHasilPanen().getBusukNJangkos()));
            etLewatMatang.setText(String.valueOf(transaksiPanen.getHasilPanen().getBuahLewatMatang()));
            etAbnormal.setText(String.valueOf(transaksiPanen.getHasilPanen().getBuahAbnormal()));
            etTangkaiPanjang.setText(String.valueOf(transaksiPanen.getHasilPanen().getTangkaiPanjang()));
            etBuahDimakanTikus.setText(String.valueOf(transaksiPanen.getHasilPanen().getBuahDimakanTikus()));
            etBrondolan.setText(String.valueOf(transaksiPanen.getHasilPanen().getBrondolan()));
            etTotalJanjangBuruk.setText(String.valueOf(transaksiPanen.getHasilPanen().getTotalJanjangBuruk()));
//            etJarak.setText(sdfT.format(waktuPanen));
            lsAlasBrondol.setText(alas);

            totalJjgJelek = transaksiPanen.getHasilPanen().getTotalJanjangBuruk();
            totalJjg = transaksiPanen.getHasilPanen().getTotalJanjang();
            totalJjgPendapatan = transaksiPanen.getHasilPanen().getTotalJanjangPendapatan();
            waktuPanen =transaksiPanen.getHasilPanen().getWaktuPanen();

        }

        if(transaksiPanen.getKongsi() != null){
            idKongsi = transaksiPanen.getKongsi().getTipeKongsi();
            tipeKongsi.setPosition(idKongsi);
            sharedPrefHelper.commit(SharedPrefHelper.KEY_TipeKongsi,String.valueOf(idKongsi));
            pemanenOperator = new HashMap<>();
            pemanenOperatorTonase = new HashMap<>();
            lSisa.setVisibility(View.GONE);

            for(int i = 0 ; i < transaksiPanen.getKongsi().getSubPemanens().size();i++){
                SubPemanen subPemanen = transaksiPanen.getKongsi().getSubPemanens().get(i);
                if(subPemanen.getTipePemanen() == SubPemanen.idOperator) {
                    pemanenOperator.put(subPemanen.getPemanen().getKodePemanen(),subPemanen.getPemanen());
                }else if (subPemanen.getTipePemanen() == SubPemanen.idOperatorTonase){
                    pemanenOperatorTonase.put(subPemanen.getPemanen().getKodePemanen(),subPemanen.getPemanen());
                }

                if(subPemanen.getOph().toLowerCase().equals("a")){
                    pemanenSelected = subPemanen.getPemanen();
                }
            }

            spkSelected = transaksiPanen.getSpk();
            if(spkSelected != null){
                cbBorongan.setChecked(true);
                lBorongan.setVisibility(View.VISIBLE);
                tvOa.setVisibility(View.VISIBLE);
                etSpk.setText(spkSelected.getIdSPK());
                tvOa.setText(TransaksiPanenHelper.showSPK(spkSelected));
                pemanenInfo.setVisibility(View.GONE);
            }

            if(transaksiPanen.getKongsi().getPemanenGerdang() != null){
                Gerdang dariMaster  = currentGerdang.get(pemanenSelected.getNik());

                if(dariMaster != null){
                    pemanenGerdang = dariMaster.getPemanenGerdang();
                }else{
                    pemanenGerdang = transaksiPanen.getKongsi().getPemanenGerdang();
                }

                cbGerdang.setChecked(true);
                lGerdang.setVisibility(View.VISIBLE);
                etGangGerdang.setText(pemanenGerdang.getGank());
                pemanenGerdangHelper.setAdapterPemanenGerdang(pemanenGerdang.getGank());
                pemanenGerdangHelper.setPemanen(pemanenGerdang);
            }

            subPemanens = new ArrayList<>();
            if(transaksiPanen.getKongsi().getSubPemanens() != null){
                subPemanens = transaksiPanen.getKongsi().getSubPemanens();
            }else if(pemanenSelected != null ) {
                subPemanens.add(new SubPemanen(
                        0, pemanenSelected,100,new HasilPanen()
                ));
            }

            if(idKongsi == Kongsi.idMekanis){
                if(transaksiPanen.getVehicleMekanisasi() != null ){
                    lVehicleMekanisasi.setVisibility(View.VISIBLE);
                    if(transaksiPanen.getTypeVehicleMekanisasi() == vehicleMekanisasiHelper.masterIdx){
                        lsKendaraanMekanisasi.setText(vehicleMekanisasiHelper.master);
                        vehicleMekanisasiHelper.setVehicle(transaksiPanen.getVehicleMekanisasi());
                    }else {
                        lsKendaraanMekanisasi.setText(vehicleMekanisasiHelper.freeText);
                        if (transaksiPanen.getVehicleMekanisasi().getVehicleCode() != null) {
                            etVehicleMekanisasi.setText(transaksiPanen.getVehicleMekanisasi().getVehicleCode());
                        }
                    }
                }
            }
        }

        String sFotoPanenPref = SharedPrefHelper.getSharePref(HarvestApp.FOTO_PANEN_PREF);
        JSONArray jsonArray = null;
        if(sFotoPanenPref != null) {
            try {
                jsonArray = new JSONArray(sFotoPanenPref);
                ALselectedImage = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    File file = new File(String.valueOf(jsonArray.get(i)));
                    if (file.exists()) {
                        ALselectedImage.add(file);
                    }
                }
                setupimageslide();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        stampImage = new StampImage();
        stampImage.setHasilPanen(hasilPanenStamp);
        stampImage.setTph(tphSelected);
        stampImage.setBlock(tphSelected.getBlock());
        stampImage.setPemanen(pemanenSelected);
        Gson gson = new Gson();
        SharedPrefHelper.applySharepref(HarvestApp.STAMP_IMAGES,gson.toJson(stampImage));
        etBlock.dismissDropDown();
        return true;
    }

    private void setDropdownTph(){
        popupTPH = true;
        new LongOperation(getView()).execute(String.valueOf(LongOperation_Setup_Data_TPH));
    }

    public void setTph(TPH tph) {
        if (tph == null) {
            etNotph.setText("");
            etAncak.setText("");
            tphSelected = null;
            blockSelected = null;
        } else {
            if(stampImage == null){
                stampImage = new StampImage();
            }
            stampImage.setTph(tph);
            stampImage.setBlock(tph.getBlock());

            if (tph.getCreateBy() == TPH.USER_GIS_SYSTEM) {
                lTph.setVisibility(View.GONE);
                lAncak.setVisibility(View.GONE);
                cbTphBayangan.setChecked(true);
                lReasonTphBayangan.setVisibility(View.VISIBLE);
            } else {
                lTph.setVisibility(View.VISIBLE);
                cbTphBayangan.setChecked(false);
                lReasonTphBayangan.setVisibility(View.GONE);
            }

//            if(angkutActivity != null) {
//                setDropDownTphnLangsiran();
//            }else if (tphActivity != null){
//                setDropdownTph();
//            }

            etNotph.setText(tph.getNamaTph());
            etAncak.setText(tph.getAncak());
            etBlock.setText(tph.getBlock());
            tphSelected = tph;
            blockSelected = tph.getBlock();
            etTotalJanjang.requestFocus();

            GlobalHelper.showKeyboard(getActivity());

            if (selectedTransaksiPanen == null) {
                if (tphSelected.getNamaTph() != null) {
                    boolean bedaAfdeling = false;
                    if(afdelingPanen != null){
                        if (afdelingPanen.toLowerCase().equals(tphSelected.getAfdeling().toLowerCase())) {
                            bedaAfdeling = true;
                        }
                    }
                    if (!bedaAfdeling) {
                        showYesNo("Apakah Anda Yakin Untuk \nPilih Afdeling "+ tphSelected.getAfdeling() +"\nBlock " + tphSelected.getBlock() + "\nTph " + tphSelected.getNamaTph(), YesNo_Pilih_TPH);
                    }
                }else{
                    etBlock.requestFocus();
                }
            }

            if(cbBorongan != null) {
                if (cbBorongan.isChecked()) {
                    new LongOperation(getView()).execute(String.valueOf(LongOperation_Setup_SPK));
                }
            }
        }
    }

    private boolean cekTphDanPemanen(){
        if(selectedTransaksiPanen != null){
            if(selectedTransaksiPanen.getStatus() > TransaksiPanen.TAP) {
                return true;
            }
        }


        if(tphSelected == null || pemanenSelected == null){
            return false;
        }

        if(tphSelected.getNamaTph().equalsIgnoreCase("999")){
            return true;
        }
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_TPH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(
            and(
                eq("param1", tphSelected.getNoTph()),
                eq("param2", pemanenSelected.getKodePemanen())
            )
        );
        for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            TransaksiPanen transaksiPanen = gson.fromJson(dataNitrit.getValueDataNitrit(), TransaksiPanen.class);
            long jarakWaktu = System.currentTimeMillis() - transaksiPanen.getCreateDate();
            if(selectedTransaksiPanen != null) {
                if (transaksiPanen.getIdTPanen().equals(selectedTransaksiPanen.getIdTPanen())){
                    db.close();
                    return true;
                }else{
                    if (GlobalHelper.MAX_TIME_SAME_TPH_AND_SAME_PEMANEN > jarakWaktu) {
                        int jarakWaktuPanen = (int) ((GlobalHelper.MAX_TIME_SAME_TPH_AND_SAME_PEMANEN - jarakWaktu) / 60000);
                        String redWord = jarakWaktuPanen + " Menit";
                        String keterangan = "Anda Bisa Input Pemanen " + pemanenSelected.getNama() + " Dan Tph " + tphSelected.getNamaTph() + " Dalam " + redWord + " Kedepan";
                        SpannableString spanKeterangan = new SpannableString(keterangan);
                        spanKeterangan.setSpan(new ForegroundColorSpan(Color.RED),
                                spanKeterangan.toString().indexOf(redWord),
                                spanKeterangan.toString().indexOf(redWord) + redWord.length(),
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        WidgetHelper.showOKDialogWithHighline(tphActivity,
                                "Warning",
                                spanKeterangan,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                }
                        );
                        db.close();
                        return false;
                    }
                }
            } else {
                if (GlobalHelper.MAX_TIME_SAME_TPH_AND_SAME_PEMANEN > jarakWaktu) {
                    int jarakWaktuPanen = (int) ((GlobalHelper.MAX_TIME_SAME_TPH_AND_SAME_PEMANEN - jarakWaktu) / 60000);
                    String redWord = jarakWaktuPanen + " Menit";
                    String keterangan = "Anda Bisa Input Pemanen " + pemanenSelected.getNama() + " Dan Tph " + tphSelected.getNamaTph() + " Dalam " + redWord + " Kedepan";
                    SpannableString spanKeterangan = new SpannableString(keterangan);
                    spanKeterangan.setSpan(new ForegroundColorSpan(Color.RED),
                            spanKeterangan.toString().indexOf(redWord),
                            spanKeterangan.toString().indexOf(redWord) + redWord.length(),
                            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    WidgetHelper.showOKDialogWithHighline(tphActivity,
                            "Warning",
                            spanKeterangan,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }
                    );
                    db.close();
                    return false;
                }
            }
        }
        db.close();
        return true;
    }

    private void setDropDownTphnLangsiran() {
        //kalau dari nfc biru atau scan qr panen maka ini tidak udah di jalankan
        if (selectedTransaksiPanen == null) {
            popupTPH = true;
            new LongOperation(getView()).execute(String.valueOf(LongOperation_Setup_Data_TPHnLangsrian));
        }
    }

    public void setTphnLangsiran(TPHnLangsiran tphnLangsiran){
        if(tphnLangsiran == null){
            etNotph.setText("");
        }else{
            if(stampImage == null){
                stampImage = new StampImage();
            }

            if(tphnLangsiran.getTph()!= null) {
                stampImage.setBlock(tphnLangsiran.getTph().getBlock());
                stampImage.setTph(tphnLangsiran.getTph());

                etNotph.setText(tphnLangsiran.getTph().getNamaTph());
                etBlock.setText(tphnLangsiran.getTph().getBlock());
            }else if (tphnLangsiran.getLangsiran() != null){
                stampImage.setBlock(tphnLangsiran.getLangsiran().getBlock());
                stampImage.setLangsiran(tphnLangsiran.getLangsiran());

                etNotph.setText(tphnLangsiran.getLangsiran().getNamaTujuan());
                etBlock.setText(tphnLangsiran.getLangsiran().getBlock());
            }
        }
        selectedTpHnLangsiran = tphnLangsiran;
    }

    @Override
    public void onTimeSet(ViewGroup viewGroup, int hourOfDay, int minute) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minute);
        waktuPanen = cal.getTimeInMillis();
        etJarak.setText(hourOfDay+" : "+minute);
    }

    private DialogFragment createDialogWithSetters() {
        BottomSheetPickerDialog dialog = NumberPadTimePickerDialog.newInstance(this,true);
        ((NumberPadTimePickerDialog) dialog).setHeaderTextColor(0xFFFF4081);
        dialog.setAccentColor(0xFFFF4081);
        dialog.setBackgroundColor(0xFF2196F3);
        dialog.setHeaderColor(0xFF2196F3);
        dialog.setHeaderTextDark(true);
        return dialog;
    }

    public void setImages(File images){
        ALselectedImage.add(images);
        setupimageslide();
        saveSharePrefPanenFoto();
    }

    private void setupimageslide(){
        Set<File> sf = new HashSet<>();
        sf.addAll(ALselectedImage);
        ALselectedImage.clear();
        ALselectedImage.addAll(sf);

        if(ALselectedImage.size() > 3){
            ivPhoto.setVisibility(View.GONE);
        }else{
            ivPhoto.setVisibility(View.VISIBLE);
        }
        if(ALselectedImage.size() != 0){
//            tphphoto.setVisibility(View.VISIBLE);
            imagecacnel.setVisibility(View.VISIBLE);
            imagesview.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.VISIBLE);
        }else{
//            tphphoto.setVisibility(View.GONE);
            imagecacnel.setVisibility(View.GONE);
            imagesview.setVisibility(View.GONE);
            sliderLayout.setVisibility(View.GONE);
        }
        sliderLayout.removeAllSliders();
        for(File file : ALselectedImage){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            if(file.toString().startsWith("http:")){
                String surl = file.toString();
                if(file.toString().startsWith("http://")){

                }else if(file.toString().startsWith("http:/")){
                    surl =surl.replace("http:/","http://");
                }

                textSliderView
                        .image(GlobalHelper.setUrlFoto(surl))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }else {
                textSliderView
                        .image(file)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }
            sliderLayout.addSlider(textSliderView);
        }


        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());

        imagecacnel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
                try {
                    if (sliderLayout.getCurrentPosition() == 0 && ALselectedImage.size() == 1) {
                        ALselectedImage.clear();
                        sliderLayout.removeAllSliders();
                        sliderLayout.setVisibility(View.GONE);
                        imagecacnel.setVisibility(View.GONE);
                        imagesview.setVisibility(View.GONE);
                    } else {
                        ALselectedImage.remove(sliderLayout.getCurrentPosition());
                        sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
                    }
                }catch (Exception e){
                    Log.e("sliderLayout", e.toString());
                }
            }
        });
        imagesview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WidgetHelper.showImagesZoom(getActivity(),ALselectedImage);
            }
        });
    }

    public void startLongOperation(int stat){
        if(tphActivity != null) {
            if (tphActivity.tulisUlangNFC && selectedTransaksiPanen != null) {
                transaksiPanenHelper.hapusDataNFC(selectedTransaksiPanen);
            }
        }else if (angkutActivity != null){
            if (transaksiAngkutNFC != null) {
                if(!transaksiAngkutHelper.updateDataNFC(transaksiAngkutNFC,angkutActivity.valueNFC)){
                    return;
                }
            }else{
                Toast.makeText(HarvestApp.getContext(),"KOK data Angkutnya hilang",Toast.LENGTH_SHORT);
                return;
            }
        }

        new LongOperation(getView()).execute(String.valueOf(stat));
    }

    private class LongOperation extends AsyncTask<String, String, String> {
        private AlertDialog alertDialogAllpoin ;
        private final View view;
        Snackbar snackbar;
        private Boolean validasi;
        //tambahan zendi, untuk check
        private boolean isRusak = false;
        private String blockName;
        public String messageError = "";

        public LongOperation(View v) {
            view = v;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(getActivity(), getResources().getString(R.string.wait));
            if (popupTPH) {
                validasi = true;
            } else {
                if(lsStatus.getVisibility()== View.VISIBLE){
                    validasi = true;
                }else{
                    validasi = validasiSaveTransaksiPanen();
                }
            }
            if(lsStatus.getText().toString().equals("Kartu Rusak")){
                isRusak = true;
            }else{
                etNoNFC.setText("");
                isRusak = false;
            }
            blockName = etBlock.getText().toString();
        }

        @Override
        protected String doInBackground(String... params) {
            if(validasi) {
                switch (Integer.parseInt(params[0])) {
                    case LongOperation_Setup_Point:{
                        setupPoint();
                        break;
                    }
                    case LongOperation_UpdateLocation_Setup_Point:{
                        if(baseActivity.currentlocation != null) {
                            tphActivity.nearestBlock = tphHelper.getNearBlock(baseActivity.currentlocation);
                            setupPoint();
                        }
                        break;
                    }
                    case LongOperation_Setup_SPK:{
                        setMasterSpk(tphSelected);
                        break;
                    }
                    case LongOperation_NFC_With_Save: {
                        String idTpanenNfc = null;
                        if(tphActivity.valueNFC != null){
                            try {
                                JSONObject obj = (JSONObject) new JSONObject(tphActivity.valueNFC);
                                idTpanenNfc = obj.getString("a");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        if(selectedTransaksiPanen != null){
                            if(selectedTransaksiPanen.getIdNfc() != null){
                                if(tphActivity.myTag != null){
                                    String nfcId = GlobalHelper.bytesToHexString(tphActivity.myTag.getId());
                                    Log.d(TAG, "doInBackground: LongOperation_NFC_With_Save 1"+ nfcId);
                                    Log.d(TAG, "doInBackground: LongOperation_NFC_With_Save 2"+ selectedTransaksiPanen.getIdNfc());
                                    if(!selectedTransaksiPanen.getIdNfc().equalsIgnoreCase(nfcId)){
                                        validasi = false;
                                        messageError = "Kartu Panen Yang Berbeda Seharusnya "+ selectedTransaksiPanen.getIdNfc();
                                    }else if (idTpanenNfc != null){
                                        if(idTpanenNfc.equalsIgnoreCase(selectedTransaksiPanen.getIdTPanen())){
                                            saveTransaksiPanen();
                                        }else{
                                            validasi = false;
                                            messageError = "Kartu Ini Sudah Di Isi Dengan Id Panen Lain ";
                                            tphActivity.myTag = null;
                                        }
                                    }else{
                                        saveTransaksiPanen();
                                    }
                                }else{
                                    validasi = false;
                                    messageError = "NFC Tag Tidak Terbaca";
                                }
                            }else{
                                saveTransaksiPanen();
                            }
                        }else if (idTpanenNfc != null) {
                            validasi = false;
                            messageError = "Kartu Ini Sudah Di Isi Dengan Id Panen Lain ";
                            tphActivity.myTag = null;
                        }else{
                            saveTransaksiPanen();
                        }
                        break;
                    }
                    case LongOperation_Print_With_Save: {
                        saveTransaksiPanen();
                        transaksiPanenHelper.printBluetooth(selectedTransaksiPanen);
                        break;
                    }
                    case LongOperation_Print_Only: {
                        transaksiPanenHelper.printBluetooth(selectedTransaksiPanen);
                        break;
                    }
                    case LongOperation_Share_With_Save: {
                        saveTransaksiPanen();
                        break;
                    }
                    case LongOperation_Save: {
                        saveTransaksiPanen();
                        break;
                    }
                    case LongOperation_Delet_Data: {
                        hapusData(selectedTransaksiPanen);
                        break;
                    }
                    case LongOperation_Save_Panen_T2: {
                        saveTransaksiPanenT2(this);
                        break;
                    }
                    case LongOperation_Save_Panen_T2_MANUAL: {
                        saveTransaksiPanenT2_MANUAL(true, isRusak);
                        break;
                    }
//hapus                    case LongOperation_Save_Panen_T2_NFC: {
//                        saveTransaksiPanenT2(false);
//                        break;
//                    }
//                    case LongOperation_Save_Panen_T2_Print: {
//                        saveTransaksiPanenT2(false);
//                        break;
//                    }
//                    case LongOperation_Save_Panen_T2_Share: {
//                        saveTransaksiPanenT2(false);
//                        break;
//                    }
                    case LongOperation_Delet_Data_T2: {
                        hapusDataT2();
                        break;
                    }
                    case LongOperation_Save_Data_Qc: {
//                        saveTransaksiPanenQc();
                        break;
                    }
                    case LongOperation_Delet_Data_Qc: {
                        hapusDataQc(selectedTransaksiPanen);
                        break;
                    }
                    case LongOperation_Setup_Data_TPH:{
                        allTph = new ArrayList<>();
                        allTph = tphHelper.setDataTph(GlobalHelper.getEstate().getEstCode(),blockName);
                        if(allTph.size() > 0) {
                            allTph = tphHelper.cariTphTerdekat(allTph);
                        }
//                        Log.d(TAG, "doInBackground: tphTerdekat "+ tphTerdekat.getBlock());
                        break;
                    }
                    case LongOperation_Setup_Data_TPHnLangsrian:{
                        String [] block = {blockName};
                        allTPHnLangsiran = tphHelper.setDataTphnLangsiran(GlobalHelper.getEstate().getEstCode(), block );
                        Log.i(TAG, "allTPHnLangsiran: "+allTPHnLangsiran.size());
                        if(baseActivity.currentlocation!=null){
                            tpHnLangsiranTerdekat = tphHelper.cariTphnLangsiran(allTPHnLangsiran);
                        }

                        ArrayList<TPHnLangsiran> tpHnLangsirans = new ArrayList<>();
                        for(int i = 0 ;i < allTPHnLangsiran.size();i++){
                            if(allTPHnLangsiran.get(i).getTph() != null){
                                tpHnLangsirans.add(allTPHnLangsiran.get(i));
                            }
                        }
                        allTPHnLangsiran = tpHnLangsirans;
                        if(baseActivity.currentlocation!=null){
                            tpHnLangsiranTerdekat = tphHelper.cariTphnLangsiran(allTPHnLangsiran);
                        }
                        break;
                    }
                }

                return String.valueOf(params[0]);
            }
            return "salah";
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            angkutActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject objProgres = new JSONObject(String.valueOf(values[0]));

                        if(snackbar== null) {
                            if (angkutActivity != null) {
                                snackbar = Snackbar.make(angkutActivity.myCoordinatorLayout, objProgres.getString("ket"), Snackbar.LENGTH_INDEFINITE);
                            }
                        }
                        snackbar.setText(String.format(objProgres.getString("ket") + " %d %s [%d/%d] ",
                                objProgres.getInt("persen"), "%", objProgres.getInt("count"), objProgres.getInt("total")));
                        snackbar.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result.equals("salah")){
                if (alertDialogAllpoin != null) {
                    alertDialogAllpoin.cancel();
                }
            }else if(validasi && !popupTPH) {
                if (alertDialogAllpoin != null) {
                    alertDialogAllpoin.cancel();
                }
                switch (Integer.parseInt(result)) {
                    case LongOperation_Save_Panen_T2: {
                        angkutActivity.backProses();
                        if(snackbar != null) {
                            snackbar.dismiss();
                        }
                        break;
                    }
                    case LongOperation_Setup_Point:{
                        if(selectedTransaksiPanen == null) {
                            updateNearestBlock();
                        }
                        break;
                    }
                    case LongOperation_UpdateLocation_Setup_Point:{
                        if(selectedTransaksiPanen == null) {
                            updateNearestBlock();
                        }
                        break;
                    }
                    case LongOperation_NFC_With_Save: {
                        transaksiPanenHelper.tapNFC(selectedTransaksiPanen);
                        break;
                    }
                    case LongOperation_NFC_Only: {
                        transaksiPanenHelper.tapNFC(selectedTransaksiPanen);
                        break;
                    }
                    case LongOperation_Share_With_Save: {
                        transaksiPanenHelper.showQr(selectedTransaksiPanen, tphActivity.qrHelper);
                        break;
                    }
                    case LongOperation_Share_Only: {
                        transaksiPanenHelper.showQr(selectedTransaksiPanen, tphActivity.qrHelper);
                        break;
                    }
                    case LongOperation_Setup_SPK:{
                        if(allSpk.size() > 0){
                            adapterSpk = new SelectSpkAdapter(getActivity(), R.layout.row_setting, new ArrayList<SPK>(allSpk.values()));
                            etSpk.setThreshold(1);
                            etSpk.setAdapter(adapterSpk);
                            adapterSpk.notifyDataSetChanged();

                            pemanenInfo.setVisibility(View.GONE);
                            lBorongan.setVisibility(View.VISIBLE);
//                            tvOa.setVisibility(View.VISIBLE);
                            etSpk.setText("");
                            tvOa.setText("");
                            etSpk.showDropDown();
                            spkSelected = null;
                        }else{
                            pemanenInfo.setVisibility(View.VISIBLE);
                            lBorongan.setVisibility(View.GONE);
//                            tvOa.setVisibility(View.GONE);
                            etSpk.setText("");
                            tvOa.setText("");
                            spkSelected = null;
                            cbBorongan.setChecked(false);
                            Toast.makeText(HarvestApp.getContext(),"Tidak Ada Master SPK Yang Cocok",Toast.LENGTH_SHORT).show();
                        }
                        break;
                    }
                }
            }else if(validasi && popupTPH) {
                popupTPH = false;
                switch (Integer.parseInt(result)) {
                    case LongOperation_Setup_Data_TPH: {
                        if (allTph.size() > 0 && allTph != null) {
                            adapterTph = new SelectTphAdapter(getActivity(), R.layout.row_setting, allTph);
                            etNotph.setThreshold(1);
                            etNotph.setAdapter(adapterTph);
                            adapterTph.notifyDataSetChanged();
                            if(allTph.size() == 1){
                                setTph(allTph.get(0));
                            }else{
                                etNotph.showDropDown();
                            }
                        } else {
                            showYesNo("TPH Block "+ etBlock.getText().toString() + " Tidak ditemukan Dalam Radius\nApakah Anda Ingin mengunakan Tph Bayangan",YesNo_Pilih_TPH_Bayangan);
//                            setTph(null);
                        }
                        break;
                    }
                    case LongOperation_Setup_Data_TPHnLangsrian:{
                        Log.i(TAG, "onPostExecute: iki "+allTPHnLangsiran.size());
                        if (allTPHnLangsiran.size() > 0 && allTPHnLangsiran != null) {
                            adapterTPHnLangsiran = new SelectTphnLangsiranAdapter(getActivity(), R.layout.row_record, allTPHnLangsiran);
                            etNotph.setThreshold(1);
                            etNotph.setAdapter(adapterTPHnLangsiran);
                            adapterTPHnLangsiran.notifyDataSetChanged();
                            etNotph.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    etNotph.showDropDown();
                                }
                            });
//                            etNotph.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                                @Override
//                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                                }
//                            });
//                            etNotph.setSelection(1);
                            if(lsStatus.getVisibility() == View.GONE){
                                setTphnLangsiran(tpHnLangsiranTerdekat);
                            }
//                            setTphnLangsiran(tpHnLangsiranTerdekat);
                        } else {
                            setTphnLangsiran(null);
                        }
                        break;
                    }
                }
                if (alertDialogAllpoin != null) {
                    alertDialogAllpoin.cancel();
                }
//                new Handler().post(new Runnable() {
//                    @Override
//                    public void run() {
//                        tphHelper.choseTph(etBlock.getText().toString());
//                        popupTPH = false;
//                        if (alertDialogAllpoin != null) {
//                            alertDialogAllpoin.cancel();
//                        }
//                    }
//                });
            } else if (!validasi){
                if (alertDialogAllpoin != null) {
                    alertDialogAllpoin.cancel();
                }
                switch (Integer.parseInt(result)) {
                    case LongOperation_NFC_With_Save: {
                        if(tphActivity != null) {
                            NfcHelper.uiFailedTapNFC(tphActivity, messageError);
                        }
                        Toast.makeText(HarvestApp.getContext(),messageError,Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
            }
        }

        public void publishProgress(JSONObject objProgres){
            onProgressUpdate(String.valueOf(objProgres));
        }
    }
}
