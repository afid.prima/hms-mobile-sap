package id.co.ams_plantation.harvestsap.Fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.mikepenz.iconics.utils.Utils;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.adapter.AncakPohonAdapter;
import id.co.ams_plantation.harvestsap.TableView.holder.AncakPohonRowHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestsap.TableView.view_model.AncakPohonTableViewModel;
import id.co.ams_plantation.harvestsap.adapter.SelectTphAdapter;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.QcAncak;
import id.co.ams_plantation.harvestsap.model.QcAncakPohon;
import id.co.ams_plantation.harvestsap.model.TPH;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.QcMutuAncakActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.MutuAncakHelper;
import id.co.ams_plantation.harvestsap.util.TphHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import ng.max.slideview.SlideView;

/**
 * Created by user on 12/4/2018.
 */

public class QcMutuAncakInputFragmet extends Fragment {

    public static final int STATUS_LONG_OPERATION_NONE = 0;
    public static final int STATUS_LONG_OPERATION_SEARCH = 1;
    public static final int STATUS_LONG_OPERATION_SELECT_ITEM = 2;
    public static final int STATUS_LONG_OPERATION_NEW_DATA = 3;
    public static final int STATUS_LONG_OPERATION_SETUP_DATA_TPH = 4;
    public static final int STATUS_LONG_OPERATION_SAVE = 5;
    public static final int STATUS_LONG_OPERATION_DELETED = 6;

    HashMap<String,QcAncakPohon> origin;
    AncakPohonAdapter adapter;
    TableView mTableView;
//    Filter mTableFilter;
    AncakPohonTableViewModel mTableViewModel;
    CompleteTextViewHelper etSearch;
    Button btnSearch;
    TextView tvCreateTime;
    CompleteTextViewHelper etTph;
    CompleteTextViewHelper etBlock;
    AppCompatEditText etAncak;
    AppCompatEditText etBrondolan;
//    ChipView etBaris;
    RelativeLayout ltableview;
    TextView tvTotalPokok;
    TextView tvTotalJanjang;
    TextView tvTotalBuahTinggal;
    TextView tvTotalBrondolan;
    Button btnClsoe;

    LinearLayout llNewData;
    TextView footerUser;
    TextView footerEstate;

    SelectTphAdapter adapterTph;

    Set<String> listSearch;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    QcMutuAncakActivity qcMutuAncakActivity;
    MutuAncakHelper mutuAncakHelper;
    ArrayList<TPH> allTph;
    TphHelper tphHelper;
    TPH tphTerdekat;
    File kml;

    public double latitude;
    public double longitude;

    int TotalJanjang;
    int TotalBuahTinggal;
    int TotalBrondolan;
    QcAncak selectedQcAncak;
    ArrayList<QcAncakPohon> qcPohons;

    public static QcMutuAncakInputFragmet getInstance(){
        QcMutuAncakInputFragmet fragment = new QcMutuAncakInputFragmet();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qc_ancak_input_layout,null,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        tvCreateTime = view.findViewById(R.id.tvCreateTime);
        etTph = view.findViewById(R.id.etTph);
        etBlock = view.findViewById(R.id.etBlock);
        etAncak = view.findViewById(R.id.etAncak);
        etBrondolan = view.findViewById(R.id.etBrondolan);
//        etBaris = view.findViewById(R.id.etBaris);
        ltableview = view.findViewById(R.id.ltableview);
        mTableView = view.findViewById(R.id.tableview);
        llNewData = view.findViewById(R.id.llNewData);
        tvTotalPokok = view.findViewById(R.id.tvTotalPokok);
        tvTotalJanjang = view.findViewById(R.id.tvTotalJanjang);
        tvTotalBuahTinggal = view.findViewById(R.id.tvTotalBuahTinggal);
        tvTotalBrondolan = view.findViewById(R.id.tvTotalBrondolan);
        btnClsoe = view.findViewById(R.id.btnClsoe);

        mutuAncakHelper = new MutuAncakHelper(getActivity());
        if(getActivity() instanceof  QcMutuAncakActivity){
            qcMutuAncakActivity = (QcMutuAncakActivity) getActivity();
            if(qcMutuAncakActivity.seletedQcAncak != null) {
                selectedQcAncak = qcMutuAncakActivity.seletedQcAncak;
            }else{
                selectedQcAncak = new QcAncak();
                selectedQcAncak.setStatus(QcAncak.Save);
                selectedQcAncak.setStartTime(System.currentTimeMillis());
            }
        }

        etAncak.setFocusable(false);
        origin = new HashMap<>();
        allTph = new ArrayList<>();
        tphHelper = new TphHelper(getActivity());
        llNewData.setVisibility(selectedQcAncak.getStatus() == QcAncak.Save ? View.VISIBLE : View.GONE);

        kml = new File(GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK));
        Set<String> allBlock = GlobalHelper.getAllBlock(kml);
        ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
                (getActivity(),android.R.layout.select_dialog_item, allBlock.toArray(new String[allBlock.size()]));


        etBlock.setThreshold(1);
        etBlock.setAdapter(adapterBlock);
        etBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etBlock.showDropDown();
            }
        });
        if(qcMutuAncakActivity.seletedQcAncak != null) {
            if (sdf.format(qcMutuAncakActivity.seletedQcAncak.getStartTime()).equals(sdf.format(System.currentTimeMillis()))
                    && qcMutuAncakActivity.seletedQcAncak.getStatus() == QcAncak.Save) {
                llNewData.setVisibility(View.VISIBLE);
            } else {
                llNewData.setVisibility(View.GONE);
            }
        }else{
            llNewData.setVisibility(View.VISIBLE);
        }

        etBlock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setDropDownTph();
            }
        });

        if(qcMutuAncakActivity.currentlocation != null) {
            latitude = qcMutuAncakActivity.currentlocation.getLatitude();
            longitude = qcMutuAncakActivity.currentlocation.getLongitude();
        }

//        etBaris.setChipBuilder(new CustomLayoutChip());
//        etBaris.setDeleteOnClick(true);
//        etBaris.setOnChipClickListener(new OnChipClickListener() {
//            @Override
//            public void onChipClick(ChipView v, String chip) {
//                Toast.makeText(HarvestApp.getContext(), chip + " was deleted", Toast.LENGTH_SHORT).show();
//            }
//        });

        setUpValueDeklarasiQcAncak();

        etTph.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapterTph = (SelectTphAdapter) parent.getAdapter();
                setTph(adapterTph.getItemAt(position));
            }
        });

        llNewData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                simpanBaris();
                if (qcMutuAncakActivity.seletedQcAncak != null) {
                    if(qcMutuAncakActivity.seletedQcAncak.getTph() != null) {
                        startLongOperation(STATUS_LONG_OPERATION_NEW_DATA);
                    }else{
                        Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Tph Atau Ancak Unutk Di Qc",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Tph Atau Ancak Unutk Di Qc",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etSearch.getText().toString().isEmpty()){
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
                }else{
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SEARCH));
                }
            }
        });

        btnClsoe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeQcAncak();
            }
        });

        startLongOperation(STATUS_LONG_OPERATION_NONE);
    }

    public void closeQcAncak(){
        //untuk data angkut baru saja yang bisa masuk ke sini
        //jika data transaksi angkut yang lama maka bisa share print dan tap nfc saja
        if(qcMutuAncakActivity.seletedQcAncak == null && origin.size() == 0){
            qcMutuAncakActivity.closeActivity();
        }else if(qcMutuAncakActivity.seletedQcAncak.getStatus() == QcAncak.Save) {
            if(llNewData.getVisibility() == View.VISIBLE) {
                if (qcMutuAncakActivity.currentLocationOK()) {
//                // unutk proses angkut radius tidak di ikutkan karena tidak memungkinkan tap nfc pada tahap ini
//                if (selectedLangsiran != null) {
//                    double distance = GlobalHelper.distance(selectedLangsiran.getLatitude(),baseActivity.currentlocation.getLatitude(),
//                            selectedLangsiran.getLongitude(),baseActivity.currentlocation.getLongitude());
//                    if(distance > GlobalHelper.RADIUS){
//                        Toast.makeText(HarvestApp.getContext(),getActivity().getResources().getString(R.string.radius_to_long_with_langsiran),Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//                }
                } else {
                    qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
                    qcMutuAncakActivity.searchingGPS.show();
                    return;
                }

                View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.yes_no, null);
                TextView tvDialogKet = (TextView) dialogView.findViewById(R.id.ket1);
                SlideView btnCancelDialog = (SlideView) dialogView.findViewById(R.id.btnCancel);
                SlideView btnOkDialog = (SlideView) dialogView.findViewById(R.id.btnOK);

                final AlertDialog dialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle)
                        .setView(dialogView)
                        .setCancelable(false)
                        .create();

                final SpannableStringBuilder sb = new SpannableStringBuilder(etAncak.getText().toString());
                final ForegroundColorSpan fcs = new ForegroundColorSpan(getActivity().getResources().getColor(R.color.Red));
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
                sb.setSpan(fcs, 0, etAncak.getText().toString().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                sb.setSpan(bss, 0, etAncak.getText().toString().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                String text = "Apakah Anda Sudah Selesai Qc Ancak " + sb;
                if(origin.size() == 0) {
                    text += "\nQc di Ancak "+ sb + " Akan Dihapus";
                }

                tvDialogKet.setText(text);
                btnOkDialog.setText("Sudah");
                btnCancelDialog.setText("Belum");

                btnCancelDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                    @Override
                    public void onSlideComplete(SlideView slideView) {
                        dialog.dismiss();
                    }
                });
                btnOkDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                    @Override
                    public void onSlideComplete(SlideView slideView) {
                        dialog.dismiss();
                        if(origin.size() > 0) {
                            new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SAVE));
                        }else{
                            new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_DELETED));
                        }
                    }
                });
                dialog.show();
            }else{
                qcMutuAncakActivity.closeActivity();
            }
        }else{
            qcMutuAncakActivity.closeActivity();
        }
    }

    public void simpanBaris(){
        ArrayList<Integer> basirs = new ArrayList<>();
//        for (String s : etBaris.getChips()){
//            try {
//                basirs.add(Integer.parseInt(s));
//            }catch (Exception e){
//
//            }
//        }
        for(int i = 1; i <= GlobalHelper.MAX_BARIS_TPH;i++){
            basirs.add(i);
        }
        if(selectedQcAncak.getTph() != null) {
            selectedQcAncak.getTph().setBaris(basirs);
        }
        selectedQcAncak.setBrondolanDiTph(Integer.parseInt(etBrondolan.getText().toString().isEmpty() ? "0" : etBrondolan.getText().toString()));
        qcMutuAncakActivity.mutuAncakHelper.createNewMutuAncak(selectedQcAncak);
        qcMutuAncakActivity.seletedQcAncak = selectedQcAncak;
        qcMutuAncakActivity.seletedQcAncak.setQcAncakPohons(qcPohons);
    }

    private void setUpValueDeklarasiQcAncak(){
        if (selectedQcAncak.getTph() != null) {
            setTph(selectedQcAncak.getTph());
            etBrondolan.setText(String.valueOf(selectedQcAncak.getBrondolanDiTph()));
        } else {
            String[] estAfdBlock = GlobalHelper.getAndAssignBlock(kml, latitude, longitude).split(";");
            try {
                etBlock.setText(estAfdBlock[2]);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        tvCreateTime.setText(sdf.format(selectedQcAncak.getStartTime()));
    }

    private void setDropDownTph(){
        new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SETUP_DATA_TPH));
    }

    private void updateDataRv(int status){
        origin = new HashMap<>();
        qcPohons = new ArrayList<>();
        listSearch = new android.support.v4.util.ArraySet<>();

        TotalJanjang = 0;
        TotalBuahTinggal = 0;
        TotalBrondolan = 0;

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_ANCAK_POHON);
        Log.d("lokasi pohon",db.getContext().getFilePath());
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            QcAncakPohon qcAncakPohon = gson.fromJson(dataNitrit.getValueDataNitrit(),QcAncakPohon.class);
            qcPohons.add(qcAncakPohon);

            if (status == STATUS_LONG_OPERATION_SEARCH) {
                if (timeFormat.format(qcAncakPohon.getWaktuQcAncakPohon()).toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                        qcAncakPohon.getIdPohon().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                        String.valueOf(qcAncakPohon.getNoBaris()).contains(etSearch.getText().toString().toLowerCase()) ||
                        String.valueOf(qcAncakPohon.getJanjangPanen()).contains(etSearch.getText().toString().toLowerCase()) ||
                        String.valueOf(qcAncakPohon.getBuahTinggal()).contains(etSearch.getText().toString().toLowerCase()) ||
                        String.valueOf(qcAncakPohon.getBrondolan()).contains(etSearch.getText().toString().toLowerCase())) {
                    addRowTable(qcAncakPohon);
                }
            } else if (status == STATUS_LONG_OPERATION_NONE) {
                addRowTable(qcAncakPohon);
            }
            listSearch.add(timeFormat.format(qcAncakPohon.getWaktuQcAncakPohon()));
            listSearch.add(qcAncakPohon.getIdPohon());
            listSearch.add(String.valueOf(qcAncakPohon.getNoBaris()));
            listSearch.add(String.valueOf(qcAncakPohon.getJanjangPanen()));
            listSearch.add(String.valueOf(qcAncakPohon.getBrondolan()));

        }
        db.close();
    }

    private void addRowTable(QcAncakPohon qcAncakPohon){
        Gson gson = new Gson();
        Log.d("detail pohon",gson.toJson(qcAncakPohon));
        origin.put(qcAncakPohon.getIdQcAncakPohon(), qcAncakPohon);
        TotalJanjang += qcAncakPohon.getJanjangPanen();
        TotalBuahTinggal += qcAncakPohon.getBuahTinggal();
        TotalBrondolan += qcAncakPohon.getBrondolan();
    }

    private void updateUIRV(int status){
        if(status == STATUS_LONG_OPERATION_NONE){
            etSearch.setText("");
        }

        List<String> lSearch = new ArrayList<>();
        lSearch.addAll(listSearch);
        lSearch = GlobalHelper.removeNullUsingIterator(lSearch);
        ArrayAdapter<String> adapterSearch = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, lSearch);
        etSearch.setAdapter(adapterSearch);
        etSearch.setThreshold(1);
        adapterSearch.notifyDataSetChanged();

        if(origin.size() > 0 ) {
            ArrayList<QcAncakPohon> list = new ArrayList<>(origin.values());

            Collections.sort(list, new Comparator<QcAncakPohon>() {
                @Override
                public int compare(QcAncakPohon o1, QcAncakPohon o2) {
                    return o1.getWaktuQcAncakPohon() > o2.getWaktuQcAncakPohon() ? -1 : (o1.getWaktuQcAncakPohon() < o2.getWaktuQcAncakPohon()) ? 1 : 0;
                }
            });

            ltableview.setVisibility(View.VISIBLE);
            mTableViewModel = new AncakPohonTableViewModel(getContext(), list);
            // Create TableView Adapter
            adapter = new AncakPohonAdapter(getContext(), mTableViewModel);
            mTableView.setAdapter(adapter);
            mTableView.setRowHeaderWidth(Utils.convertDpToPx(getActivity(),100));
            mTableView.setTableViewListener(new ITableViewListener() {
                @Override
                public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellDoubleClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {
                        // Create Long Press Popup
                        ColumnHeaderPopup popup = new ColumnHeaderPopup(
                                (ColumnHeaderViewHolder) columnHeaderView, mTableView);
                        // Show
                        popup.show();
                    }
                }

                @Override
                public void onColumnHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    if (rowHeaderView != null && rowHeaderView instanceof AncakPohonRowHeaderViewHolder) {
                        simpanBaris();
                        String [] sid = ((AncakPohonRowHeaderViewHolder) rowHeaderView).getCellId().split("-");
                        qcMutuAncakActivity.seletedQcAncakPohon = origin.get(sid[1]);
                        startLongOperation(STATUS_LONG_OPERATION_SELECT_ITEM);

                    }

                }

                @Override
                public void onRowHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }

                @Override
                public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }
            });
            adapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel
                    .getRowHeaderList(), mTableViewModel.getCellList());

            tvTotalPokok.setText(String.valueOf(origin.size()));
            tvTotalJanjang.setText(String.valueOf(TotalJanjang));
            tvTotalBuahTinggal.setText(String.valueOf(TotalBuahTinggal));
            tvTotalBrondolan.setText(String.valueOf(TotalBrondolan));
        }else{
            ltableview.setVisibility(View.GONE);
        }

    }

    private void setTph(TPH tph) {
        if (tph == null) {
            etTph.setText("");
            etAncak.setText("");
        } else {
            etTph.setText(tph.getNamaTph());
            etAncak.setText(tph.getAncak());
            etBlock.setText(tph.getBlock());
//            if(tph.getBaris() != null){
//                if(tph.getBaris().size() > 0){
//                    String chip = "";
//                    for(int i= 0 ; i < tph.getBaris().size();i++){
//                        chip += tph.getBaris().get(i) +" ";
//                    }
//                    etBaris.setText(chip);
//                    etBaris.createChips();
//                }
//            }
            GlobalHelper.showKeyboard(getActivity());
            selectedQcAncak.setTph(tph);
            mutuAncakHelper.createNewMutuAncak(selectedQcAncak);

        }
    }

    public void startLongOperation(int status){
        new LongOperation().execute(String.valueOf(status));
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        boolean validasi;
        int status = 0;

        public LongOperation(int status) {
            this.status = status;
        }

        public LongOperation() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (((BaseActivity) getActivity()).alertDialogBase != null) {
                ((BaseActivity) getActivity()).alertDialogBase.cancel();
            }
            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),getResources().getString(R.string.wait));
            validasi = true;
            if(status == STATUS_LONG_OPERATION_SAVE){
                validasi = mutuAncakHelper.validasiAncak();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            if(validasi) {
                switch (Integer.parseInt(strings[0])) {
                    case STATUS_LONG_OPERATION_NONE:
                        updateDataRv(Integer.parseInt(strings[0]));
                        break;

                    case STATUS_LONG_OPERATION_SEARCH:
                        updateDataRv(Integer.parseInt(strings[0]));
                        break;

                    case STATUS_LONG_OPERATION_SETUP_DATA_TPH:
                        allTph = new ArrayList<>();
                        allTph = tphHelper.setDataTph(GlobalHelper.getEstate().getEstCode(), etBlock.getText().toString());
                        if (((BaseActivity) getActivity()).currentLocationOK() && (selectedQcAncak.getTph() == null)) {
                            tphTerdekat = tphHelper.cariTphTerdekat(allTph).get(0);
                        }
                        break;
                    case STATUS_LONG_OPERATION_SAVE:
                        validasi = mutuAncakHelper.saveQcAncak();
                        break;
                    case STATUS_LONG_OPERATION_DELETED:
                        validasi = mutuAncakHelper.hapusQcAncak();
                        break;
                }
            }
            return strings[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            switch (Integer.parseInt(result)){
                case STATUS_LONG_OPERATION_NONE:
                    updateUIRV(Integer.parseInt(result));

                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }

                    if(allTph.size() == 0){
                        startLongOperation(STATUS_LONG_OPERATION_SETUP_DATA_TPH);
                    }
                    break;

                case STATUS_LONG_OPERATION_SEARCH:
                    updateUIRV(Integer.parseInt(result));

                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;

                case STATUS_LONG_OPERATION_SELECT_ITEM:
                    if (!((BaseActivity) getActivity()).currentLocationOK() && qcMutuAncakActivity.seletedQcAncak.getStatus() == QcAncak.Save){
                        qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout,getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
                        qcMutuAncakActivity.searchingGPS.show();
                    }else if(qcMutuAncakActivity.seletedQcAncak.getTph().getBaris() == null){
                        qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout,"Mohon Isi Baris",Snackbar.LENGTH_INDEFINITE);
                        qcMutuAncakActivity.searchingGPS.show();
                    }else if(qcMutuAncakActivity.seletedQcAncak.getTph().getBaris().size() == 0){
                        qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout,"Mohon Isi Baris",Snackbar.LENGTH_INDEFINITE);
                        qcMutuAncakActivity.searchingGPS.show();
                    }else{
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.content_container, QcMutuAncakInputEntryFragment.getInstance())
                                .commit();
                    }

                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;

                case STATUS_LONG_OPERATION_NEW_DATA:
                    if (!((BaseActivity) getActivity()).currentLocationOK()){
                        qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout,getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
                        qcMutuAncakActivity.searchingGPS.show();
                    }else if(qcMutuAncakActivity.seletedQcAncak.getTph().getBaris() == null){
                        qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout,"Mohon Isi Baris",Snackbar.LENGTH_INDEFINITE);
                        qcMutuAncakActivity.searchingGPS.show();
                    }else if(qcMutuAncakActivity.seletedQcAncak.getTph().getBaris().size() == 0){
                        qcMutuAncakActivity.searchingGPS = Snackbar.make(qcMutuAncakActivity.myCoordinatorLayout,"Mohon Isi Baris",Snackbar.LENGTH_INDEFINITE);
                        qcMutuAncakActivity.searchingGPS.show();
                    }else{
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.content_container, QcMutuAncakInputEntryFragment.getInstance())
                                .commit();
                    }

                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;

                case STATUS_LONG_OPERATION_SETUP_DATA_TPH:
                    if(allTph.size() > 0 && allTph != null) {
                        adapterTph = new SelectTphAdapter(getActivity(), R.layout.row_setting, allTph);
                        etTph.setThreshold(1);
                        etTph.setAdapter(adapterTph);
                        adapterTph.notifyDataSetChanged();

                        if (selectedQcAncak.getTph() == null) {
                            setTph(tphTerdekat);
                        }
                    }
                        if (((BaseActivity) getActivity()).alertDialogBase != null) {
                            ((BaseActivity) getActivity()).alertDialogBase.cancel();
                        }
                    break;

                case STATUS_LONG_OPERATION_SAVE:
                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    if(validasi) {
                        Toast.makeText(HarvestApp.getContext(),"Berhasil Simpan Qc Ancak",Toast.LENGTH_SHORT).show();
                        qcMutuAncakActivity.keawalFragment();
                    }else{
                        Toast.makeText(HarvestApp.getContext(),"Gagal Simpan Qc Ancak",Toast.LENGTH_SHORT).show();
                    }
                    break;
                case STATUS_LONG_OPERATION_DELETED:
                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    if(validasi) {
                        Toast.makeText(HarvestApp.getContext(),"Berhasil Hapus Qc Ancak",Toast.LENGTH_SHORT).show();
                        qcMutuAncakActivity.keawalFragment();
                    }else{
                        Toast.makeText(HarvestApp.getContext(),"Gagal Hapus Qc Ancak",Toast.LENGTH_SHORT).show();
                    }
                    break;
            }

        }
    }
}
