package id.co.ams_plantation.harvestsap.util;

import android.util.Log;

import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import java.util.HashMap;
import java.util.Iterator;

import id.co.ams_plantation.harvestsap.model.BJRInformation;
import id.co.ams_plantation.harvestsap.model.DataNitrit;

public class BjrHelper {

    public static double getBJRInformasi(String block,int bulan,int tahun) {
        String idBJRInformasi = tahun + "-" + bulan + "-" + block;
        Log.d("idBJR",idBJRInformasi);
        BJRInformation value = null;
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_BJRINFORMATION);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            if(dataNitrit.getIdDataNitrit().contains(idBJRInformasi)) {
                Gson gson = new Gson();
                value = gson.fromJson(dataNitrit.getValueDataNitrit(), BJRInformation.class);
                break;
            }
        }
        db.close();
        if (value == null) {
            return 0.0;
        } else {
            return value.getBjrAdjustment();
        }
    }

    public static String getAfdeling(String block){

        Gson gson = new Gson();
        try {
            BJRInformation value = null;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_BJRINFORMATION);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                BJRInformation bjr = gson.fromJson(dataNitrit.getValueDataNitrit(), BJRInformation.class);
                if(bjr.getBlock().toLowerCase().equals(block.toLowerCase())){
                    value = bjr;
                    break;
                }
            }
            db.close();
            if (value == null) {
                return "";
            } else {
                return value.getAfdeling();
            }
        }catch (Exception e){
            return "";
        }

    }

    public static HashMap<String,BJRInformation> getAllBJR(){
        Gson gson = new Gson();
        HashMap<String,BJRInformation> bjrInformationHashMap = new HashMap<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_BJRINFORMATION);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            BJRInformation bjrInformation = gson.fromJson(dataNitrit.getValueDataNitrit(), BJRInformation.class);
            bjrInformationHashMap.put(bjrInformation.getIdBJRInformation(),bjrInformation);
        }
        db.close();
        return bjrInformationHashMap;
    }
}
