package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;

import id.co.ams_plantation.harvestsap.Fragment.AngkutInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.MekanisasiInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.SpbInputFragment;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.SwipeItemRecycleView.SwipeControllerInterface;
import id.co.ams_plantation.harvestsap.model.Cages;
import id.co.ams_plantation.harvestsap.ui.AngkutActivity;
import id.co.ams_plantation.harvestsap.ui.MekanisasiActivity;
import id.co.ams_plantation.harvestsap.ui.SpbActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;

public class CagesInputAdapter extends RecyclerView.Adapter<CagesInputAdapter.Holder> implements SwipeControllerInterface {
    Context context;
    ArrayList<Cages> listCages;
    public ArrayList<Cages> Cages;
    SelectCagesAdapter adapterCages;

    public CagesInputAdapter(Context context, ArrayList<Cages> listCages, ArrayList<Cages> cages) {
        this.context = context;
        this.listCages = listCages;
        this.Cages = cages;
        Log.d("CagesInputAdapter", "listCages size: " + listCages.size());
    }

    @NonNull
    @Override
    public CagesInputAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.cages_transaksi_item, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CagesInputAdapter.Holder holder, int position) {
        holder.setValue(Cages.get(position));
        Log.d("CagesInputAdapter", "listCages size (onBindViewHolder): " + listCages.size());
    }

    @Override
    public int getItemCount() {
        return Cages != null ? Cages.size() : 0;
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        Collections.swap(listCages, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        ///updateCages();
    }

//    public void updateCages(){
//        if(context instanceof AngkutActivity) {
//            if (((AngkutActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof AngkutInputFragment) {
//                AngkutInputFragment fragment = (AngkutInputFragment) ((AngkutActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
//                fragment.updateCages();
//            }
//        }else if(context instanceof SpbActivity){
//            if (((SpbActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof SpbInputFragment) {
//                SpbInputFragment fragment = (SpbInputFragment) ((SpbActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
//                fragment.updateCages();
//            }
//        }else if(context instanceof MekanisasiActivity){
//            if (((MekanisasiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof MekanisasiInputFragment) {
//                MekanisasiInputFragment fragment = (MekanisasiInputFragment) ((MekanisasiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
//                fragment.updateCages();
//            }
//        }
//    }

    public class Holder extends RecyclerView.ViewHolder {
        CompleteTextViewHelper etCages;
        TextView assetNo;
        TextView tvValue;
        View vInputType;

        public Holder(View v) {
            super(v);
            etCages = v.findViewById(R.id.etCages);
            assetNo = v.findViewById(R.id.cagesName);
            tvValue = v.findViewById(R.id.tvValue);
            vInputType = v.findViewById(R.id.vInputType);
        }

        public void setValue(Cages cages) {
            Log.d("CagesInputAdapter", "listCages size (Holder - setValue): " + listCages.size());
            updateCagesUI(cages);

            adapterCages = new SelectCagesAdapter(context, R.layout.row_setting, listCages);
            etCages.setThreshold(1);
            etCages.setAdapter(adapterCages);
            adapterCages.notifyDataSetChanged();


            /*etCages.setOnClickListener(v -> etCages.showDropDown());*/

            etCages.setOnItemClickListener((parent, view, position, id) -> {
                Cages cages1 = ((SelectCagesAdapter) parent.getAdapter()).getItemAt(position);
                updateCagesUI(cages1);
                //updateCages();
            });
        }

        private void updateCagesUI(Cages value) {
            if (value.getAssetNo() != null) {
                Gson gson = new Gson();
                etCages.setText(value.getAssetNo());
                assetNo.setText(value.getAssetName());
                if(value.getInputType() == id.co.ams_plantation.harvestsap.model.Cages.inputTypeQr){
                    vInputType.setBackgroundColor(id.co.ams_plantation.harvestsap.model.Cages.COLOR_INPUTTYPEQR);
                }else if(value.getInputType() == id.co.ams_plantation.harvestsap.model.Cages.inputTypeMaster){
                    vInputType.setBackgroundColor(id.co.ams_plantation.harvestsap.model.Cages.COLOR_INPUTTYPEMASTER);
                }else{
                    vInputType.setBackgroundColor(id.co.ams_plantation.harvestsap.model.Cages.COLOR_INPUTTYPENOMASTER);
                }
                tvValue.setText(gson.toJson(value));
            }
        }
    }
}
