package id.co.ams_plantation.harvestsap.TableView.model;

/**
 * Created by user on 12/24/2018.
 */

public class TableViewColumnHeader extends TableViewCell {

    public TableViewColumnHeader(String id) {
        super(id);
    }

    public TableViewColumnHeader(String id, String data) {
        super(id, data);
    }
}

