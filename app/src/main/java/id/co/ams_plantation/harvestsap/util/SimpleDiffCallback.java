package id.co.ams_plantation.harvestsap.util;

import android.support.v7.util.DiffUtil;

import java.util.List;

import id.co.ams_plantation.harvestsap.model.ModelRecyclerView;

/**
 * Created by user on 12/4/2018.
 */

public class SimpleDiffCallback extends DiffUtil.Callback {

    private final List<ModelRecyclerView> oldList;
    private final List<ModelRecyclerView> newList;

    public SimpleDiffCallback(List<ModelRecyclerView> oldList, List<ModelRecyclerView> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override public int getOldListSize() {
        return oldList.size();
    }

    @Override public int getNewListSize() {
        return newList.size();
    }

    @Override public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).equals(newList.get(newItemPosition));
    }

    @Override public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return areItemsTheSame(oldItemPosition, newItemPosition);
    }
}