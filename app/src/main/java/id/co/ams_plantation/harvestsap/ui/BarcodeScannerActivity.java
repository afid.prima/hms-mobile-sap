package id.co.ams_plantation.harvestsap.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;


import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.zxing.Result;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.presenter.BarcodeScannerPresenter;
import id.co.ams_plantation.harvestsap.view.BarcodeScannerView;

public class BarcodeScannerActivity extends BaseActivity implements BarcodeScannerView, DecodeCallback, CodeScanner.FinishPreviewCallback {
    private static final Logger log = LoggerFactory.getLogger(BarcodeScannerActivity.class);
    private CodeScanner mCodeScanner;
    public static final String SCAN_RESULT = "scan_result";
    private static final String TAG = BarcodeScannerActivity.class.getSimpleName();

    public static final int STATUS_LONG_SCAN_CAGES_REQUEST = 10;
    public static final int STATUS_LONG_SCAN_VEHICLE_REQUEST =  11;
    private int maxZoom = 0;
    SeekBar sliderZoom;
    public BarcodeScannerPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.barcode_scanner);
        getSupportActionBar().hide();
        CodeScannerView scannerView = findViewById(R.id.scanner_view);
        TextView toolbarText = (TextView) findViewById(R.id.toolbar_text);
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        sliderZoom = findViewById(R.id.sliderZoom);

        // Ambil request code dari Intent
        Intent intent = getIntent();
        if (intent != null) {
            int requestCode = intent.getIntExtra("requestCode", -1);
            if(requestCode == STATUS_LONG_SCAN_CAGES_REQUEST){
                toolbarText.setText("SCAN QR CAGES");
            }else if(requestCode == STATUS_LONG_SCAN_VEHICLE_REQUEST){
                toolbarText.setText("SCAN QR UNIT");
            }
        }

        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();

            }
        });
        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setFinishPreviewCallback(BarcodeScannerActivity.this);
        mCodeScanner.setDecodeCallback((DecodeCallback) BarcodeScannerActivity.this);

//        mCodeScanner.setDecodeCallback(result -> BarcodeScannerActivity.this.runOnUiThread(() -> {
//            Intent resultIntent =  new Intent();
//            resultIntent.putExtra(SCAN_RESULT, result.getText());
//            Log.d("BarcodeScannerActivity", "SCAN_RESULT: " + result.getText());
//            setResult(RESULT_OK, resultIntent);
//            finish();
//        }));
//        scannerView.setOnClickListener(view -> mCodeScanner.startPreview());
    }

    @Override
    public void onDecoded(@NonNull Result result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent resultIntent =  new Intent();
                resultIntent.putExtra(SCAN_RESULT, result.getText());
                Log.d("BarcodeScannerActivity", "SCAN_RESULT: " + result.getText());
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });
    }

    @Override
    public void onFinishStartCamera(int maxZoom) {
        Log.i(TAG, "onFinishStartCamera: maxZoom: "+maxZoom);
        Log.i(TAG, "onFinishStartCamera: maxZoomDivide: "+(maxZoom/2));
        this.maxZoom = maxZoom;
        sliderZoom.setMax(maxZoom);
        sliderZoom.setProgress(1);
        sliderZoom.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mCodeScanner.setZoom(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
//        mCodeScanner.setZoom(maxZoom/2);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    protected void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        super.onPointerCaptureChanged(hasCapture);
    }

    @Override
    protected void initPresenter() {
        presenter = new BarcodeScannerPresenter(this);
    }
}
