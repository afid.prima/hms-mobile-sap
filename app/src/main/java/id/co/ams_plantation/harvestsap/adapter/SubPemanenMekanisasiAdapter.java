package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import id.co.ams_plantation.harvestsap.Fragment.MekanisasiPanenInputFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.SwipeItemRecycleView.SwipeControllerInterface;
import id.co.ams_plantation.harvestsap.model.HasilPanen;
import id.co.ams_plantation.harvestsap.model.MekanisasiPanen;
import id.co.ams_plantation.harvestsap.model.Pemanen;
import id.co.ams_plantation.harvestsap.model.SubPemanen;
import id.co.ams_plantation.harvestsap.model.TPH;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.MekanisasiActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.PemanenHelper;
import id.co.ams_plantation.harvestsap.util.SharedPrefHelper;
import id.co.ams_plantation.harvestsap.util.TphHelper;

public class SubPemanenMekanisasiAdapter extends RecyclerView.Adapter<SubPemanenMekanisasiAdapter.Holder> implements SwipeControllerInterface {

    Context context;
    public boolean updateValue;
    public  ArrayList<TransaksiPanen> mekanisasiPanens;
    public ArrayAdapter<String> adapterBlock;
    PemanenHelper pemanenHelper;
    TphHelper tphHelper;
    String gangSelected;
    SharedPrefHelper sharedPrefHelper;
    public ArrayList<String> pemanenHasBeenSelected;

    String TAG = this.getClass().getName();

    public SubPemanenMekanisasiAdapter(Context context,  ArrayList<TransaksiPanen> mekanisasiPanens,ArrayAdapter<String> adapterBlock) {
        this.context = context;
        this.mekanisasiPanens = mekanisasiPanens;
        this.adapterBlock = adapterBlock;
        updateValue = false;
        pemanenHelper = new PemanenHelper((BaseActivity)context);
        tphHelper = new TphHelper((BaseActivity)context);
        pemanenHasBeenSelected = new ArrayList<>();
        sharedPrefHelper = new SharedPrefHelper(this.context);
        gangSelected = sharedPrefHelper.getName(SharedPrefHelper.KEY_GENGSELECTED);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.pemanen_mekanisasi_item, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.setValue(mekanisasiPanens.get(position));
    }

    @Override
    public int getItemCount() {
        return mekanisasiPanens != null ? mekanisasiPanens.size() : 0;
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
//        Collections.swap(mekanisasiPanens, fromPosition, toPosition);
//        notifyItemMoved(fromPosition, toPosition);
    }

    public class Holder extends RecyclerView.ViewHolder {
        CompleteTextViewHelper etBlock;
        CompleteTextViewHelper etAncak;
        CompleteTextViewHelper etGang;
        CompleteTextViewHelper etPemanen;
        AppCompatEditText etTotalJanjangSubPemanen;
        TextView tvPemanen;
        TextView tvTPH;
        TextView tvValue;
        TextView tvValue1;

        SelectGangAdapter adapterGang;
        SelectPemanenAdapter adapterPemanen;
        SelectTphMekanisasiAdapter adapterTPH;

        public Holder(View view) {
            super(view);
            etBlock = view.findViewById(R.id.etBlockSub);
            etAncak = view.findViewById(R.id.etAncak);
            etGang = view.findViewById(R.id.etGangSub);
            etPemanen = view.findViewById(R.id.etPemanenSub);
            etTotalJanjangSubPemanen = view.findViewById(R.id.etTotalJanjangSubPemanen);
            tvValue = view.findViewById(R.id.tvValue);
            tvValue1 = view.findViewById(R.id.tvValue1);
            tvPemanen = view.findViewById(R.id.tvPemanen);
            tvTPH = view.findViewById(R.id.tvTPH);

        }

        public void setValue(final TransaksiPanen value) {

            Gson gson = new Gson();
            if (value.getPemanen() != null) {

                if(value.getPemanen().getGank() != null) {
                    Log.d(this.getClass() + " setValue 1",value.getPemanen().getGank());
                    etGang.setText(value.getPemanen().getGank());
                }else {
                    if (gangSelected != null) {
                        etGang.setText(gangSelected);
                    }else {
                        etGang.setText("");
                    }
                }

                if(value.getPemanen().getNama() != null){
                    Log.d(this.getClass() + " setValue 1",value.getPemanen().getNama());
                    etPemanen.setText(value.getPemanen().getNama());
                    tvValue.setText(gson.toJson(value.getPemanen()));
                    if(value.getPemanen().getGank() != null && value.getPemanen().getNik() != null && value.getPemanen().getNama() != null){
                        tvPemanen.setText(value.getPemanen().getGank() + " - " + value.getPemanen().getNik() +" - "+ value.getPemanen().getNama());
                    }
                    pemanenHasBeenSelected.add(value.getPemanen().getKodePemanen());
                }else{
                    etPemanen.setText("");
                    tvPemanen.setText("Gang - NIK - Nama Pemanen");
                    tvValue.setText("");
                }
            }else{
                etPemanen.setText("");
                tvPemanen.setText("Gang - NIK - Nama Pemanen");
                tvValue.setText("");
            }

            if(value.getTph() != null){
                if(value.getTph().getBlock() != null && value.getTph().getAncak() != null && value.getTph().getNamaTph() != null) {
                    etBlock.setText(value.getTph().getBlock());
                    etAncak.setText(value.getTph().getAncak());
                    tvTPH.setText(value.getTph().getBlock() + " - " + value.getTph().getAncak() + " - " + value.getTph().getNamaTph());
                    tvValue1.setText(gson.toJson(value.getTph()));
                }else{
                    etBlock.setText("");
                    etAncak.setText("");
                    tvTPH.setText("Block - Ancak - TPH");
                    tvValue1.setText("");
                }
            }else{
                etBlock.setText("");
                etAncak.setText("");
                tvTPH.setText("Block - Ancak - TPH");
                tvValue1.setText("");
            }

            HashMap<String,Pemanen> sGang = new HashMap<>();
            if(GlobalHelper.dataPemanen.size() > 0) {
                for (HashMap.Entry<String, Pemanen> entry : GlobalHelper.dataPemanen.entrySet()) {
                    sGang.put(entry.getValue().getGank(), entry.getValue());
                }
                adapterGang = new SelectGangAdapter(context, R.layout.row_setting, new ArrayList<Pemanen>(sGang.values()));
                etGang.setThreshold(1);
                etGang.setAdapter(adapterGang);
                adapterGang.notifyDataSetChanged();
            }

            if(adapterBlock.getCount() > 0){
                etBlock.setThreshold(1);
                etBlock.setAdapter(adapterBlock);
            }

            if(value.getBlock()!= null){
                etBlock.setText(value.getBlock());
                setDropDownTPH(etBlock.getText().toString());
            }

            if(gangSelected != null){
                Pemanen adaPemanen = sGang.get(gangSelected);
                if (adaPemanen != null) {
                    if(etGang.getText().toString().isEmpty()) {
                        etGang.setText(gangSelected);
                    }
                    adapterPemanen = new SelectPemanenAdapter(context, R.layout.row_record,
                            pemanenHelper.getListPemanenByGangWithCompare(GlobalHelper.dataPemanen,
                                    pemanenHasBeenSelected,
                                    etGang.getText().toString())
                    );
                    etPemanen.setThreshold(1);
                    etPemanen.setAdapter(adapterPemanen);
                    adapterPemanen.notifyDataSetChanged();
                }
            }

            if(value.getHasilPanen() != null){
                etTotalJanjangSubPemanen.setText(String.valueOf(value.getHasilPanen().getTotalJanjang()));
                if (context instanceof MekanisasiActivity) {
                    if (((MekanisasiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof MekanisasiPanenInputFragment) {
                        MekanisasiPanenInputFragment fragment = (MekanisasiPanenInputFragment) ((MekanisasiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
                        fragment.updateTotalJanjang();
                    }
                }
            }else{
                etTotalJanjangSubPemanen.setText("");
            }

            etBlock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etBlock.showDropDown();
                }
            });

            etBlock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    if (context instanceof MekanisasiActivity) {
                        if (((MekanisasiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof MekanisasiPanenInputFragment) {
                            MekanisasiPanenInputFragment fragment = (MekanisasiPanenInputFragment) ((MekanisasiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
                            fragment.selectedBlock = etBlock.getText().toString();
                        }
                    }
                    setDropDownTPH(etBlock.getText().toString());
                }
            });

            etAncak.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etAncak.showDropDown();
                }
            });

            etAncak.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                    adapterTPH = (SelectTphMekanisasiAdapter) adapterView.getAdapter();
                    TPH tph = adapterTPH.getItemAt(position);
                    setTPH(value,tph);
                }
            });

            etGang.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    adapterGang = (SelectGangAdapter) adapterView.getAdapter();
                    gangSelected =  adapterGang.getItemAt(i).getGank();

                    if(!gangSelected.contains("PN")){
                        AlertDialog alertDialog = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
                                .setTitle("Perhatian")
                                .setMessage("Apakah Anda Yakin Untuk Simpan Bukan Gang Panen")
                                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        etGang.setText("");
                                        etPemanen.setText("");
                                        tvValue.setText("");
                                    }
                                })
                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                        setGang(gangSelected);
                                    }
                                })
                                .create();
                        alertDialog.show();
                    }else{
                        setGang(gangSelected);
                    }
                }
            });

            etGang.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etGang.showDropDown();
                }
            });

            etGang.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus){
                        if(gangSelected == null){
                            etGang.setText("");
                        }else{
                            etGang.setText(gangSelected);
                        }
                    }
                }
            });

            etPemanen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (context instanceof MekanisasiActivity) {
                        etPemanen.showDropDown();
                    }
                }
            });

            etPemanen.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    if(!tvValue.getText().toString().isEmpty()) {
                        Pemanen pemanenX = gson.fromJson(tvValue.getText().toString(),Pemanen.class);
                        for (int i = 0; i < pemanenHasBeenSelected.size(); i++) {
                            if (pemanenX.getKodePemanen().equalsIgnoreCase(pemanenHasBeenSelected.get(i))){
                                pemanenHasBeenSelected.remove(i);
                                break;
                            }
                        }
                    }

                    adapterPemanen = (SelectPemanenAdapter) adapterView.getAdapter();
                    Pemanen pemanen = adapterPemanen.getItemAt(position);
                    if(pemanen.getGank() != null){
                        if(!pemanen.getGank().contains("PN")){
                            AlertDialog alertDialog = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
                                    .setTitle("Perhatian")
                                    .setMessage("Apakah Anda Yakin Untuk Simpan Pemanen Bukan Gang Panen")
                                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            etPemanen.setText("");
                                            tvValue.setText("");
                                        }
                                    })
                                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            setPemanen(value,adapterPemanen.getItemAt(position));
                                        }
                                    })
                                    .create();
                            alertDialog.show();
                        }else{
                            setPemanen(value,adapterPemanen.getItemAt(position));
                        }
                    }else {
                        setPemanen(value,adapterPemanen.getItemAt(position));
                    }
                }
            });

            etTotalJanjangSubPemanen.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String sJJg = s.toString();
                    int jjg = 0;
                    if(!sJJg.trim().isEmpty()){
                        jjg = Integer.parseInt(sJJg);
                    }
                    HasilPanen hasilPanen = new HasilPanen(
                            jjg
                            ,0
                            ,0
                            ,0
                            ,0
                            ,0
                            ,0
                            ,0
                            ,0
                            ,jjg
                            ,jjg
                            ,System.currentTimeMillis()
                            ,false
                    );
                    setHasilPanen(value,hasilPanen);
                    if (context instanceof MekanisasiActivity) {
                        if (((MekanisasiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof MekanisasiPanenInputFragment) {
                            MekanisasiPanenInputFragment fragment = (MekanisasiPanenInputFragment) ((MekanisasiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
                            fragment.updateTotalJanjang();
                        }
                    }
                }
            });

        }

        private void setDropDownTPH(String block){
            adapterTPH = new SelectTphMekanisasiAdapter(context, R.layout.row_record,
                    tphHelper.setDataTph(GlobalHelper.getEstate().getEstCode(), block)
            );
            etAncak.setThreshold(1);
            etAncak.setAdapter(adapterTPH);
//            etAncak.showDropDown();
        }

        private void setTPH(TransaksiPanen transaksiPanen,TPH tph){
            Gson gson = new Gson();
            etAncak.setText(tph.getAncak());
            tvTPH.setText(tph.getBlock() + " - " + tph.getAncak() + " - " + tph.getNamaTph());
            tvValue1.setText(gson.toJson(tph));

            transaksiPanen.setTph(tph);
            transaksiPanen.setBlock(tph.getBlock());
            for(int i = 0 ; i < mekanisasiPanens.size(); i ++){
                if(transaksiPanen.getIdTPanen().toLowerCase().equals(mekanisasiPanens.get(i).getIdTPanen())){
                    mekanisasiPanens.set(i,transaksiPanen);
                    break;
                }
            }

            if(etGang.getText().toString().isEmpty()){
                etGang.requestFocus();
            }else if (etPemanen.getText().toString().isEmpty()){
                etPemanen.showDropDown();
            }

        }

        private void setGang(String Gang){
            etGang.setText(Gang);
            sharedPrefHelper.commit(SharedPrefHelper.KEY_GENGSELECTED,Gang);
//                        File fileGangSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_PANEN] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_PANEN_GANG] );
//                        GlobalHelper.writeFileContent(fileGangSelected.getAbsolutePath(),etGang.getText().toString());

            adapterPemanen = new SelectPemanenAdapter(context, R.layout.row_record,
                    pemanenHelper.getListPemanenByGangWithCompare(GlobalHelper.dataPemanen,
                            pemanenHasBeenSelected,
                            etGang.getText().toString())
            );
            etPemanen.setThreshold(1);
            etPemanen.setAdapter(adapterPemanen);
            adapterPemanen.notifyDataSetChanged();
            etPemanen.requestFocus();
            etPemanen.callOnClick();
        }

        private void setPemanen(TransaksiPanen transaksiPanen, Pemanen pemanen){
            Gson gson = new Gson();
            etPemanen.setText(pemanen.getNama());
            tvValue.setText(gson.toJson(pemanen));
            pemanenHasBeenSelected.add(pemanen.getKodePemanen());
            tvPemanen.setText(pemanen.getGank() + " - " + pemanen.getNik() +" - "+ pemanen.getNama());

            transaksiPanen.setPemanen(pemanen);
            for(int i = 0 ; i < mekanisasiPanens.size(); i++){
                if(transaksiPanen.getIdTPanen().toLowerCase().equals(mekanisasiPanens.get(i).getIdTPanen())){
                    mekanisasiPanens.set(i,transaksiPanen);
                    break;
                }
            }

            etTotalJanjangSubPemanen.requestFocus();
        }

        private void setHasilPanen(TransaksiPanen transaksiPanen, HasilPanen hasilPanen){
            transaksiPanen.setHasilPanen(hasilPanen);
            for(int i = 0 ; i < mekanisasiPanens.size(); i++){
                if(transaksiPanen.getIdTPanen().toLowerCase().equals(mekanisasiPanens.get(i).getIdTPanen())){
                    mekanisasiPanens.set(i,transaksiPanen);
                    break;
                }
            }
        }
    }
}
