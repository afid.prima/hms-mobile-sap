package id.co.ams_plantation.harvestsap.model;

import java.util.HashSet;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;

/**
 * Created by user on 12/28/2018.
 */

public class TransaksiSPB {

    public static final int BELUM_CLOSE = -1;
    public static final int BELUM_UPLOAD = 0;
    public static final int HAPUS = 1;
    public static final int UPLOAD = 2;
    public static final int UPLOAD_HAPUS = -2;


    public static final int COLOR_TAP = HarvestApp.getContext().getResources().getColor(R.color.DeepSkyBlue);
    public static final int COLOR_UPLOAD = HarvestApp.getContext().getResources().getColor(R.color.Yellow);

    String idSPB;
    String noSpb;
    String createBy;
    String approveBy;
    Long createDate;
    TransaksiAngkut transaksiAngkut;
    double latitude;
    double longitude;
    int totalJanjang;
    int totalBrondolan;
    int status;
    String estCode;
    SupervisionList substitute;
    HashSet<String> afdeling;
    HashSet<String> foto;

    public TransaksiSPB(){}

    public TransaksiSPB(String idSPB,String noSpb, String createBy, String approveBy,String estCode, Long createDate, TransaksiAngkut transaksiAngkut, double latitude, double longitude, int totalJanjang, int totalBrondolan, int status,HashSet<String> afdeling,HashSet<String> foto) {
        this.idSPB = idSPB;
        this.noSpb = noSpb;
        this.createBy = createBy;
        this.approveBy = approveBy;
        this.estCode  = estCode;
        this.createDate = createDate;
        this.transaksiAngkut = transaksiAngkut;
        this.latitude = latitude;
        this.longitude = longitude;
        this.totalJanjang = totalJanjang;
        this.totalBrondolan = totalBrondolan;
        this.status = status;
        this.afdeling = afdeling;
        this.foto = foto;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getIdSPB() {
        return idSPB;
    }

    public void setIdSPB(String idSPB) {
        this.idSPB = idSPB;
    }

    public String getNoSpb() {
        return noSpb;
    }

    public void setNoSpb(String noSpb) {
        this.noSpb = noSpb;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }

    public TransaksiAngkut getTransaksiAngkut() {
        return transaksiAngkut;
    }

    public void setTransaksiAngkut(TransaksiAngkut transaksiAngkut) {
        this.transaksiAngkut = transaksiAngkut;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getTotalJanjang() {
        return totalJanjang;
    }

    public void setTotalJanjang(int totalJanjang) {
        this.totalJanjang = totalJanjang;
    }

    public int getTotalBrondolan() {
        return totalBrondolan;
    }

    public void setTotalBrondolan(int totalBrondolan) {
        this.totalBrondolan = totalBrondolan;
    }

    public String getApproveBy() {
        return approveBy;
    }

    public void setApproveBy(String approveBy) {
        this.approveBy = approveBy;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public SupervisionList getSubstitute() {
        return substitute;
    }

    public void setSubstitute(SupervisionList substitute) {
        this.substitute = substitute;
    }

    public HashSet<String> getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(HashSet<String> afdeling) {
        this.afdeling = afdeling;
    }

    public HashSet<String> getFoto() {
        return foto;
    }

    public void setFoto(HashSet<String> foto) {
        this.foto = foto;
    }
}
