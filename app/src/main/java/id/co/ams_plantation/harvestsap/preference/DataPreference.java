package id.co.ams_plantation.harvestsap.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.alibaba.fastjson.JSON;

import id.co.ams_plantation.harvestsap.BuildConfig;
import id.co.ams_plantation.harvestsap.security.HashHelper;

/**
 * Created by Chandra on 10/16/17.
 * Need some help?
 * Contact me at y.pristyan.chandra@gmail.com
 */

public class DataPreference {

    private final SharedPreferences prefs;
    private static DataPreference dataPreference;

    public static DataPreference getInstance(Context context) {
        if (dataPreference == null) dataPreference = new DataPreference(context);
        return dataPreference;
    }

    private DataPreference(Context context) {
        prefs = context.getSharedPreferences(BuildConfig.APPLICATION_ID, 0);
    }

    public void setData(String tag, String data) {
        if (prefs != null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(tag, data);
            editor.apply();
        }
    }

    public void setData(String tag, boolean data) {
        if (prefs != null) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(tag, data);
            editor.apply();
        }
    }

    public void setData(String tag, Object data) {
        if (prefs != null) {
            if (data != null) {
                String source = JSON.toJSONString(data);
                String encrypted = HashHelper.encrypt(source);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(tag, encrypted);
                editor.apply();
            } else {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(tag, null);
                editor.apply();
            }
        }
    }

    public String getData(String tag) {
        if (prefs != null) return prefs.getString(tag, null);
        return null;
    }

    public String getData(String tag, String defaultValue) {
        if (prefs != null) return prefs.getString(tag, defaultValue);
        return null;
    }

    public boolean getDataBoolean(String tag) {
        return prefs != null && prefs.getBoolean(tag, false);
    }

    public <T> T getData(String tag, Class<T> classOfT) {
        if (prefs != null && prefs.getString(tag, null) != null) {
            String decrypted = HashHelper.decrypt(prefs.getString(tag, null));
            return JSON.parseObject(decrypted, classOfT);
        }
        return null;
    }
}
