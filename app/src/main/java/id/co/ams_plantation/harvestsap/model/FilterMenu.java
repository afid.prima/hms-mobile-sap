package id.co.ams_plantation.harvestsap.model;

public class FilterMenu {
    String menuName;
    Boolean selected;

    public FilterMenu(String menuName, Boolean selected) {
        this.menuName = menuName;
        this.selected = selected;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
}
