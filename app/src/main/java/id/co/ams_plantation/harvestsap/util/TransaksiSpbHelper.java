package id.co.ams_plantation.harvestsap.util;

import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.QrItemAdapter;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.Cages;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.DynamicParameterPenghasilan;
import id.co.ams_plantation.harvestsap.model.Estate;
import id.co.ams_plantation.harvestsap.model.ISCCInformation;
import id.co.ams_plantation.harvestsap.model.InfoRestan;
import id.co.ams_plantation.harvestsap.model.ManualSPB;
import id.co.ams_plantation.harvestsap.model.MekanisasiPanen;
import id.co.ams_plantation.harvestsap.model.ModelRecyclerView;
import id.co.ams_plantation.harvestsap.model.Operator;
import id.co.ams_plantation.harvestsap.model.PengirimanGanda;
import id.co.ams_plantation.harvestsap.model.SPK;
import id.co.ams_plantation.harvestsap.model.SubTotalAngkut;
import id.co.ams_plantation.harvestsap.model.SubTotalAngkutMekanisasi;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.model.Vehicle;
import id.co.ams_plantation.harvestsap.service.BluetoothService;
import id.co.ams_plantation.harvestsap.ui.AngkutActivity;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.MekanisasiActivity;
import id.co.ams_plantation.harvestsap.ui.SpbActivity;
import id.co.command.sdk.PrinterCommand;

/**
 * Created by user on 1/11/2019.
 */

public class TransaksiSpbHelper {
    FragmentActivity activity;
    AlertDialog listrefrence;

    public TransaksiSpbHelper(FragmentActivity activity) {
        this.activity = activity;
    }

    public static String toQrSpb(TransaksiSPB transaksiSPB){
        try {
            if(transaksiSPB.getTransaksiAngkut().getSubTotalAngkuts() == null){
                transaksiSPB.getTransaksiAngkut().setSubTotalAngkuts(TransaksiAngkutHelper.getSubTotalAngkut(transaksiSPB.getTransaksiAngkut().getAngkuts()));
            }

            Collections.sort(transaksiSPB.getTransaksiAngkut().getSubTotalAngkuts(), new Comparator<SubTotalAngkut>() {
                @Override
                public int compare(SubTotalAngkut t0, SubTotalAngkut t1) {
                    String a = t0.getBlock();
                    String b = t1.getBlock();
                    return (a.compareTo(b));
                }
            });
            JSONArray jsonArray = new JSONArray();
            for(SubTotalAngkut subTotalAngkut :transaksiSPB.getTransaksiAngkut().getSubTotalAngkuts()){
                if(subTotalAngkut != null) {
                    String isi = BjrHelper.getAfdeling(subTotalAngkut.getBlock()) + ";" +
                            subTotalAngkut.getBlock() + ";" +
                            subTotalAngkut.getJanjang() + ";" +
                            subTotalAngkut.getBrondolan();
                    jsonArray.put(isi);
                }
            }

            JSONArray jsonArrayCages = new JSONArray();
            StringBuilder semiColonSeparatedCages = new StringBuilder();

            if(transaksiSPB.getTransaksiAngkut().getCages() != null) {
                for (int i = 0; i < transaksiSPB.getTransaksiAngkut().getCages().size(); i++) {
                    Cages cages = transaksiSPB.getTransaksiAngkut().getCages().get(i);
                    String isi = cages.getAssetNo() != null ? cages.getAssetNo() : "";
                    semiColonSeparatedCages.append(isi);
                    if (i < transaksiSPB.getTransaksiAngkut().getCages().size() - 1) {
                        semiColonSeparatedCages.append(";");
                    }
                }
            }

            String result = semiColonSeparatedCages.toString();

            Vehicle vehicle = transaksiSPB.getTransaksiAngkut().getVehicle();
            Operator operator = transaksiSPB.getTransaksiAngkut().getSupir();

            String d  = "-";
            if(transaksiSPB.getTransaksiAngkut().getSpk() != null){
                if(transaksiSPB.getTransaksiAngkut().getSpk().getVendorCode() != null){
                    d = transaksiSPB.getTransaksiAngkut().getSpk().getVendorCode();
                }
            }else if (transaksiSPB.getTransaksiAngkut().getPks() != null){
                if(transaksiSPB.getTransaksiAngkut().getPks().getCustomerCode() != null){
                    d = transaksiSPB.getTransaksiAngkut().getPks().getCustomerCode();
                }
            }


            JSONObject object = new JSONObject();
            object.put("a", vehicle.getVraOrderNumber() != null ? vehicle.getVraOrderNumber() : vehicle.getVehicleCode() != null ? vehicle.getVehicleCode() : "");
            object.put("b", operator.getNama() != null ? operator.getNama() : operator.getKodeOperator());
            object.put("c", transaksiSPB.getNoSpb() != null ? transaksiSPB.getNoSpb() : converIdSPBToNoSPB(transaksiSPB.getIdSPB()));
            object.put("d", d);
            object.put("e", transaksiSPB.getTransaksiAngkut().getPks() != null ? transaksiSPB.getTransaksiAngkut().getPks().getEstNew() == null ? "-" : transaksiSPB.getTransaksiAngkut().getPks().getEstNew() : "-");
            object.put("f", jsonArray);
            object.put("g", TransaksiAngkut.KENDARAAN_CODE[transaksiSPB.getTransaksiAngkut().getKendaraanDari()]);
            object.put("h", result);

            return object.toString();
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String toQrSpbMekanisasi(TransaksiSPB transaksiSPB){
        try {
            if(transaksiSPB.getTransaksiAngkut().getSubTotalAngkuts() == null){
                ArrayList<SubTotalAngkutMekanisasi> subTotalAngkutMekanisasis = new ArrayList<>();
                for(int i = 0 ; i < transaksiSPB.getTransaksiAngkut().getMekanisasiPanens().size(); i++){
                    MekanisasiPanen mekanisasiPanen = transaksiSPB.getTransaksiAngkut().getMekanisasiPanens().get(i);
                    for(int j = 0 ; j < mekanisasiPanen.getTransaksiPanens().size(); j++){
                        TransaksiPanen panen = mekanisasiPanen.getTransaksiPanens().get(j);
                        SubTotalAngkutMekanisasi subTotalAngkutMekanisasi = new SubTotalAngkutMekanisasi(panen.getIdTPanen(),panen,null);
                        subTotalAngkutMekanisasis.add(subTotalAngkutMekanisasi);
                    }
                }

                for(int i = 0 ; i < transaksiSPB.getTransaksiAngkut().getAngkuts().size(); i++){
                    Angkut angkut = transaksiSPB.getTransaksiAngkut().getAngkuts().get(i);
                    SubTotalAngkutMekanisasi subTotalAngkutMekanisasi = new SubTotalAngkutMekanisasi(angkut.getIdAngkut(),null,angkut);
                    subTotalAngkutMekanisasis.add(subTotalAngkutMekanisasi);
                }

                transaksiSPB.getTransaksiAngkut().setSubTotalAngkuts(TransaksiAngkutHelper.getSubTotalMekanisasi(subTotalAngkutMekanisasis));
            }

            Collections.sort(transaksiSPB.getTransaksiAngkut().getSubTotalAngkuts(), new Comparator<SubTotalAngkut>() {
                @Override
                public int compare(SubTotalAngkut t0, SubTotalAngkut t1) {
                    String a = t0.getBlock();
                    String b = t1.getBlock();
                    return (a.compareTo(b));
                }
            });
            JSONArray jsonArray = new JSONArray();
            for(SubTotalAngkut subTotalAngkut :transaksiSPB.getTransaksiAngkut().getSubTotalAngkuts()){
                if(subTotalAngkut != null) {
                    String isi = BjrHelper.getAfdeling(subTotalAngkut.getBlock()) + ";" +
                            subTotalAngkut.getBlock() + ";" +
                            subTotalAngkut.getJanjang() + ";" +
                            subTotalAngkut.getBrondolan();
                    jsonArray.put(isi);
                }
            }

            JSONArray jsonArrayCages = new JSONArray();
            StringBuilder semiColonSeparatedCages = new StringBuilder();

            if(transaksiSPB.getTransaksiAngkut().getCages() != null) {
                for (int i = 0; i < transaksiSPB.getTransaksiAngkut().getCages().size(); i++) {
                    Cages cages = transaksiSPB.getTransaksiAngkut().getCages().get(i);
                    String isi = cages.getAssetNo() != null ? cages.getAssetNo() : "";
                    semiColonSeparatedCages.append(isi);
                    if (i < transaksiSPB.getTransaksiAngkut().getCages().size() - 1) {
                        semiColonSeparatedCages.append(";");
                    }
                }
            }

            String result = semiColonSeparatedCages.toString();


            Vehicle vehicle = transaksiSPB.getTransaksiAngkut().getVehicle();
            Operator operator = transaksiSPB.getTransaksiAngkut().getSupir();

            String d  = "-";
            if(transaksiSPB.getTransaksiAngkut().getSpk() != null){
                if(transaksiSPB.getTransaksiAngkut().getSpk().getVendorCode() != null){
                    d = transaksiSPB.getTransaksiAngkut().getSpk().getVendorCode();
                }
            }else if (transaksiSPB.getTransaksiAngkut().getPks() != null){
                if(transaksiSPB.getTransaksiAngkut().getPks().getCustomerCode() != null){
                    d = transaksiSPB.getTransaksiAngkut().getPks().getCustomerCode();
                }
            }

            JSONObject object = new JSONObject();
            object.put("a", vehicle.getVraOrderNumber() != null ? vehicle.getVraOrderNumber() : vehicle.getVehicleCode() != null ? vehicle.getVehicleCode() : "");
            object.put("b", operator.getNama() != null ? operator.getNama() : operator.getKodeOperator());
            object.put("c", transaksiSPB.getNoSpb() != null ? transaksiSPB.getNoSpb() : converIdSPBToNoSPB(transaksiSPB.getIdSPB()));
            object.put("d", d);
            object.put("e", transaksiSPB.getTransaksiAngkut().getPks() != null ? transaksiSPB.getTransaksiAngkut().getPks().getEstNew() == null ? "-" : transaksiSPB.getTransaksiAngkut().getPks().getEstNew() : "-");
            object.put("f", jsonArray);
            object.put("g", TransaksiAngkut.KENDARAAN_CODE[transaksiSPB.getTransaksiAngkut().getKendaraanDari()]);
            object.put("h", result);

            return object.toString();
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getAfdelingFromQr(String qr){
        try {
            JSONObject object = new JSONObject(qr);
            JSONArray array = object.getJSONArray("f");
            HashSet<String> distinctAfd = new HashSet<>();
            for(int i = 0 ; i < array.length();i++){
                String [] split = array.get(i).toString().split(";");
                if(split.length > 0) {
                    distinctAfd.add(split[0]);
                }
            }

            String afd = "";
            for(String disAFD :distinctAfd){
                if(afd.isEmpty()) {
                    afd += disAFD;
                }else{
                    afd += ","+disAFD;
                }
            }
            return afd;
        }catch (Exception e){
            return null;
        }
    }

    public String toCompres(TransaksiSPB transaksiSPB){

        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmm");
        try {

            JSONArray array = new JSONArray();
            for(int i = 0 ; i < transaksiSPB.getTransaksiAngkut().getAngkuts().size(); i++){
                array.put(transaksiSPB.getTransaksiAngkut().getAngkuts().get(i).getIdAngkut());
            }

            JSONArray latlon = new JSONArray();
            latlon.put(String.format("%.5f",transaksiSPB.getLatitude()));
            latlon.put(String.format("%.5f",transaksiSPB.getLongitude()));

            TransaksiAngkut transaksiAngkut = transaksiSPB.getTransaksiAngkut();
            if(transaksiAngkut.getSubTotalAngkuts() == null){
                transaksiAngkut.setSubTotalAngkuts(TransaksiAngkutHelper.getSubTotalAngkut(transaksiAngkut.getAngkuts()));
            }
            JSONArray arraySubTotal = new JSONArray();
            for(int i = 0; i < transaksiAngkut.getSubTotalAngkuts().size();i++){
                SubTotalAngkut subTotalAngkut = transaksiAngkut.getSubTotalAngkuts().get(i);
                int berat = (int) Math.round(subTotalAngkut.getBerat());
                arraySubTotal.put(
                        subTotalAngkut.getBlock()+";"+
                                Integer.toHexString(subTotalAngkut.getJanjang())+";"+
                                Integer.toHexString(subTotalAngkut.getBrondolan())+";"+
                                Integer.toHexString(berat)
                );
            }

            JSONObject object = new JSONObject();
            object.put("a",transaksiSPB.getIdSPB());
            object.put("b",Integer.toHexString(transaksiSPB.getTotalJanjang()) +
                    "_"+ Integer.toHexString(transaksiSPB.getTotalBrondolan()));
            object.put("c",transaksiSPB.getTransaksiAngkut().getIdTAngkut() +
                    "_"+transaksiSPB.getTransaksiAngkut().getEstCode() +
                    "_"+ transaksiSPB.getTransaksiAngkut().getCreateBy() +
                    "_"+ transaksiSPB.getTransaksiAngkut().getVehicle());
            object.put("d",Long.toHexString(Long.parseLong(sdf.format(transaksiSPB.getCreateDate()))) +
                    "_"+ transaksiSPB.getApproveBy());
            object.put("e",array);
            object.put("f",latlon);
            object.put("g",arraySubTotal);
            object.put("z",GlobalHelper.TYPE_NFC_RED);

            Log.d("json object",object.toString());

            return GlobalHelper.compress(object.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static TransaksiSPB toDecompre(JSONObject value){
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmm");
        try {
            TransaksiSPB transaksiSPB = new TransaksiSPB();
            TransaksiAngkut transaksiAngkut = new TransaksiAngkut();

            ArrayList<Angkut> angkuts = new ArrayList<>();

            JSONArray eObject = (JSONArray) value.get("e");
            JSONArray fObject = (JSONArray) value.get("f");
            for (int i = 0 ; i < eObject.length(); i++){
                angkuts.add(new Angkut(eObject.get(i).toString(),
                        sdf.parse(eObject.get(i).toString().substring(5)).getTime()));
            }
            transaksiAngkut.setAngkuts(angkuts);

            if(value.has("g")){
                ArrayList<SubTotalAngkut> subTotalAngkuts = new ArrayList<>();
                JSONArray gObject = (JSONArray) value.get("g");
                for (int i = 0 ; i < gObject.length(); i++){
                    String [] gSplit = String.valueOf(gObject.get(i)).split(";");
                    if(gSplit.length == 4) {
                        int berat = (int) Long.parseLong(gSplit[3], 16);
                        subTotalAngkuts.add(new SubTotalAngkut(
                                gSplit[0],
                                (int) Long.parseLong(gSplit[1], 16),
                                (int) Long.parseLong(gSplit[2], 16),
                                (double) berat
                        ));
                    }
                }
                transaksiAngkut.setSubTotalAngkuts(subTotalAngkuts);
            }

            transaksiSPB.setLatitude(Double.parseDouble(fObject.getString(0).replace(",",".")));
            transaksiSPB.setLongitude(Double.parseDouble(fObject.getString(1).replace(",",".")));
            transaksiSPB.setIdSPB(value.getString("a"));
            transaksiSPB.setCreateBy(value.getString("a").substring(5));

            String [] janjangBrondolan = value.getString("b").split("_");
            transaksiSPB.setTotalJanjang((int) Long.parseLong(janjangBrondolan[0], 16));
            transaksiSPB.setTotalBrondolan((int) Long.parseLong(janjangBrondolan[1], 16));

//            String [] idTAngkutEstDriverVehicle = value.getString("c").split("_");
//            transaksiAngkut.setIdTAngkut(idTAngkutEstDriverVehicle[0]);
//            transaksiAngkut.setEstCode(idTAngkutEstDriverVehicle[1]);
//            transaksiAngkut.setCreateBy(idTAngkutEstDriverVehicle[2]);
//            transaksiAngkut.setVehicle(idTAngkutEstDriverVehicle[3]);

            transaksiSPB.setTransaksiAngkut(transaksiAngkut);

            String [] createApprove = value.getString("d").split("_");
            transaksiSPB.setCreateDate(sdf.parse(String.valueOf((Long) Long.parseLong(createApprove[0], 16))).getTime());
            transaksiSPB.setApproveBy(createApprove[1]);

            return transaksiSPB;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getAfdeling(Angkut angkut){
        if(angkut.getTransaksiPanen() != null){
            if(angkut.getTransaksiPanen().getTph() != null){
                return  angkut.getTransaksiPanen().getTph().getAfdeling();
            }
        }else if (angkut.getTransaksiAngkut() != null){
            if(angkut.getTransaksiAngkut().getLangsiran()  != null){
                return TphHelper.getAfdeling(GlobalHelper.getEstate().getEstCode(),angkut.getBlock());
            }
        }else if (angkut.getTph() != null) {
            if (angkut.getTph().getAfdeling() != null) {
                return angkut.getTph().getAfdeling();
            }
        }else if (angkut.getLangsiran() != null) {
            if (angkut.getLangsiran().getAfdeling() != null) {
                return angkut.getLangsiran().getAfdeling();
            }
        }
        return "";
    }

    public void printTransaksiSPB(TransaksiSPB transaksiSPB, ArrayList angkutView, boolean copy, DynamicParameterPenghasilan dynamicParameterPenghasilan){
        if (((BaseActivity) activity).bluetoothHelper.mService.getState() != BluetoothService.STATE_CONNECTED) {
            ((BaseActivity) activity).bluetoothHelper.showListBluetoothPrinter();
        }else {
            ((BaseActivity) activity).bluetoothHelper.printTransaksiSpb(transaksiSPB,angkutView,copy,dynamicParameterPenghasilan);
            if (activity instanceof AngkutActivity) {
                ((AngkutActivity) activity).closeActivity();
            }else if (activity instanceof SpbActivity){
                ((SpbActivity) activity).closeActivity();
            }
        }
    }

    public void printTransaksiSPBMekanisasi(TransaksiSPB transaksiSPB, ArrayList angkutView,ArrayList angkutViewKartu, boolean copy, DynamicParameterPenghasilan dynamicParameterPenghasilan){
        if (((BaseActivity) activity).bluetoothHelper.mService.getState() != BluetoothService.STATE_CONNECTED) {
            ((BaseActivity) activity).bluetoothHelper.showListBluetoothPrinter();
        }else {
            ((BaseActivity) activity).bluetoothHelper.printTransaksiSpbMekanisasi(transaksiSPB,angkutView,angkutViewKartu,copy,dynamicParameterPenghasilan);
            if (activity instanceof MekanisasiActivity) {
                ((MekanisasiActivity) activity).closeActivity();
            }
        }
    }

    public void printRekonsilasiSPB(TransaksiSPB transaksiSPB, int tKartuRusak, int tKartuHilang,ArrayList<Angkut> angkutView){
        if (((BaseActivity) activity).bluetoothHelper.mService.getState() != BluetoothService.STATE_CONNECTED) {
            ((BaseActivity) activity).bluetoothHelper.showListBluetoothPrinter();
        }else {
            ((BaseActivity) activity).bluetoothHelper.printRekonsilasiSPB(transaksiSPB,tKartuRusak,tKartuHilang,angkutView);
            if (activity instanceof AngkutActivity) {
                ((AngkutActivity) activity).closeActivity();
            }else if (activity instanceof SpbActivity){
                ((SpbActivity) activity).closeActivity();
            }
        }
    }



    public void showQr(TransaksiSPB transaksiSPB,QrHelper qrHelper,ArrayList<Angkut> angkutView,boolean copy,DynamicParameterPenghasilan dynamicParameterPenghasilan){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyy HH:mm");
        View view = LayoutInflater.from(activity).inflate(R.layout.show_qr,null);
        RelativeLayout isi = (RelativeLayout) view.findViewById(R.id.isi);
//        ImageView iv_qrcode = (ImageView) view.findViewById(R.id.iv_qrcode);
        Button shareqr = (Button) view.findViewById(R.id.btn_shareqr);
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);
//        TextView tvHeaderQr = (TextView) view.findViewById(R.id.tvHeaderQr);

//        tvHeaderQr.setText(activity.getResources().getString(R.string.spb));
        Operator operator = new Operator();
        Vehicle vehicle = new Vehicle();
//        try {sp
//            iv_qrcode.setImageBitmap(qrHelper.encodeAsBitmap(compres));
//        } catch (WriterException e) {
//            e.printStackTrace();
//        }

        ISCCInformation isccInformation = null;
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ISCCINFORMATION);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            ISCCInformation isccInformationX = gson.fromJson(dataNitrit.getValueDataNitrit(),ISCCInformation.class);
            if(isccInformationX.getEstCode().equals(transaksiSPB.getEstCode()) && ((new Double(isccInformationX.getValidFrom())).longValue() <= transaksiSPB.getCreateDate() && (new Double(isccInformationX.getValidUntil())).longValue() >= transaksiSPB.getCreateDate())){
                isccInformation = isccInformationX;
                break;
            }
        }
        db.close();

        int angkutTph = 0;
        int angkutTransit = 0;
        int angkutKartuHilang = 0;
        int angkutKartuRusak = 0;
        int angkutMaxRestan = 0;

        InfoRestan infoRestan = new InfoRestan();

        for (int i = 0 ; i < angkutView.size(); i++){
            Angkut angkut = angkutView.get(i);
            if (angkut.getManualSPBData() != null){
                if(angkut.getManualSPBData().getTipeId() == ManualSPB.TYPE_KARTU_HILANG){
                    angkutKartuHilang++;
                }else if (angkut.getManualSPBData().getTipeId() == ManualSPB.TYPE_KARTU_RUSAK){
                    angkutKartuRusak++;
                }else if(angkut.getManualSPBData().getTipeId() == ManualSPB.TYPE_KARTU_MAX_RESTAN){
                    angkutMaxRestan++;
                }
            }else if(angkut.getTransaksiPanen() != null){
                if(angkut.getTransaksiPanen().getCreateDate() > 0) {
                    Integer selisih = GlobalHelper.getDateDiff(transaksiSPB.getCreateDate(),angkut.getTransaksiPanen().getCreateDate());
//                    Integer.parseInt(String.valueOf(
//                            Math.abs(TimeUnit.MILLISECONDS.toDays(
//                                    transaksiSPB.getCreateDate() - angkut.getTransaksiPanen().getCreateDate()
//                            ))
//                    ));

                    if (selisih >= 2) {
                        infoRestan.setJjgH2(infoRestan.getJjgH2() + angkut.getJanjang());
                        if (selisih > infoRestan.getLamaRestan()) {
                            infoRestan.setLamaRestan(selisih);
                        }
                    }
                }
                angkutTph ++;
            }else if (angkut.getTransaksiAngkut() != null){
                angkutTransit++;
            }
        }

        Estate estate = GlobalHelper.getEstateByEstCode(transaksiSPB.getEstCode());
        String afdeling = null;
        String compres = null;
        SPK spk = null;
        if(transaksiSPB.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS) {
            compres = toQrSpb(transaksiSPB);
            afdeling = TransaksiSpbHelper.getAfdelingFromQr(compres);
        }

        if(transaksiSPB.getTransaksiAngkut().getSpk() != null){
            spk = transaksiSPB.getTransaksiAngkut().getSpk();
        }
//        if(transaksiSPB.getAfdeling() != null){
//            if(transaksiSPB.getAfdeling().size() > 0){
//                for(String s : transaksiSPB.getAfdeling()){
//                    if(afdeling == null){
//                        afdeling = s ;
//                    }else{
//                        afdeling += "," + s;
//                    }
//                }
//            }
//        }

        ArrayList<ModelRecyclerView> arrayList = new ArrayList<>();
        arrayList.add(new ModelRecyclerView("headerQr","Surat Pengangkutan Buah","",""));
        arrayList.add(new ModelRecyclerView("Barcode",transaksiSPB.getNoSpb() == null ? converIdSPBToNoSPB(transaksiSPB.getIdSPB()) : transaksiSPB.getNoSpb(),"",""));
        if(transaksiSPB.getTransaksiAngkut().isRevisi()){
            arrayList.add(new ModelRecyclerView("header", "R E V I S I O N    C O P Y", "", ""));
        } else if(copy) {
            arrayList.add(new ModelRecyclerView("header", "C O P Y", "", ""));
        }

        if(isccInformation != null) {
            arrayList.add(new ModelRecyclerView("header", "", "", ""));
            arrayList.add(new ModelRecyclerView("iscc", "FFB SUSTAINABLE ISCC " + isccInformation.getEstNewCode(), "", ""));
            arrayList.add(new ModelRecyclerView("iscc", isccInformation.getCertificateNumber(), "", ""));
            arrayList.add(new ModelRecyclerView("iscc", "GHG Value Eec : " + isccInformation.getGhgValue(), "", ""));
            arrayList.add(new ModelRecyclerView("iscc", "SC Model : " + isccInformation.getCertificateType(), "", ""));
            arrayList.add(new ModelRecyclerView("iscc", "ISCC Compliant", "", ""));
            double pksDistance = TransaksiSpbHelper.cekLandDistance(transaksiSPB.getTransaksiAngkut());
            if (pksDistance > 0) {
                arrayList.add(new ModelRecyclerView("iscc", String.format("Jarak Ke PKS : %.1f KM", pksDistance),"" , ""));
            }
        }
        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_transaksi),"",""));
        if(estate != null) {
            arrayList.add(new ModelRecyclerView("detail", "PT", estate.getCompanyShortName(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Estate", estate.getEstName(), ""));
        }else{
            arrayList.add(new ModelRecyclerView("detail", "PT", activity.getResources().getString(R.string.dont_have_estate) + transaksiSPB.getEstCode(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Estate", activity.getResources().getString(R.string.dont_have_estate) + transaksiSPB.getEstCode(), ""));
        }

        if(afdeling != null){
            arrayList.add(new ModelRecyclerView("detail", "Afdeling", afdeling, ""));
        }

        if(spk != null){
            arrayList.add(new ModelRecyclerView("detail", "OA", TransaksiPanenHelper.showSPK(spk), ""));
        }

        User userSelected = null;
        if(qrHelper.context instanceof BaseActivity){
            userSelected = GlobalHelper.dataAllUser.get(transaksiSPB.getCreateBy());
            if(userSelected == null){
                arrayList.add(new ModelRecyclerView("detail", "Input Oleh", transaksiSPB.getCreateBy(), ""));
            }else {
                arrayList.add(new ModelRecyclerView("detail", "Input Oleh", userSelected.getUserFullName(), ""));
            }
        }else{
            arrayList.add(new ModelRecyclerView("detail","Input Oleh" ,transaksiSPB.getCreateBy(),""));
        }
        if(transaksiSPB.getSubstitute() != null){
            arrayList.add(new ModelRecyclerView("detail","Krani Penganti" ,transaksiSPB.getSubstitute().getNama(),""));
        }

        arrayList.add(new ModelRecyclerView("detail","Waktu Input" ,dateFormat.format(transaksiSPB.getCreateDate()),""));


        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_spb),"",""));
        arrayList.add(new ModelRecyclerView("detail","Id SPB" ,transaksiSPB.getNoSpb() == null ? converIdSPBToNoSPB(transaksiSPB.getIdSPB()) : transaksiSPB.getNoSpb(),""));
        User assAfdSelected = null;
        if(qrHelper.context instanceof BaseActivity){
            assAfdSelected = GlobalHelper.dataAllAsstAfd.get(transaksiSPB.getApproveBy());
            boolean adaNama = false;
            if(assAfdSelected != null){
                if(assAfdSelected.getUserFullName() != null){
                    adaNama = true;
                }
            }

            if(adaNama) {
                arrayList.add(new ModelRecyclerView("detail", "Approve", assAfdSelected.getUserFullName(), ""));
            }else{
                arrayList.add(new ModelRecyclerView("detail","Approve" ,transaksiSPB.getApproveBy(),""));
            }
        }else{
            arrayList.add(new ModelRecyclerView("detail","Approve" ,transaksiSPB.getApproveBy(),""));
        }

        String tujuan = transaksiSPB.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS ? TransaksiAngkut.PKS :
                transaksiSPB.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_KELUAR ? TransaksiAngkut.KELAUR : "-";
        arrayList.add(new ModelRecyclerView("detail","Tujuan" ,tujuan,""));
        if(tujuan == TransaksiAngkut.PKS){
            arrayList.add(new ModelRecyclerView("detail","Nama PKS" ,transaksiSPB.getTransaksiAngkut().getPks().getNamaPKS() ,""));
        }

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_vehicle),"",""));
        operator = transaksiSPB.getTransaksiAngkut().getSupir();
        vehicle = transaksiSPB.getTransaksiAngkut().getVehicle();


        arrayList.add(new ModelRecyclerView("detail","Supir" ,operator != null ? operator.getNama():"",""));
        arrayList.add(new ModelRecyclerView("detail","Kendaraan" , TransaksiAngkutHelper.showVehicle(vehicle),""));

        if(vehicle.getVraOrderNumber() != null){
            arrayList.add(new ModelRecyclerView("detail","Vehicle No " , vehicle.getVraOrderNumber(),""));
        }else{
            arrayList.add(new ModelRecyclerView("detail","Vehicle No " , "-",""));
        }
        if (transaksiSPB.getTransaksiAngkut().getCages() != null) {
            if (!transaksiSPB.getTransaksiAngkut().getCages().isEmpty()){
                int count = 1;
                for (int i = 0; i < transaksiSPB.getTransaksiAngkut().getCages().size(); i++){
                    Cages cages = transaksiSPB.getTransaksiAngkut().getCages().get(i);
                    arrayList.add(new ModelRecyclerView("detail", "Cages No."  + String.format("%,d", count) ,cages.getAssetNo() != null ? cages.getAssetNo() : "", "" ));
                    count++;
                }
            }
        }

//        arrayList.add(new ModelRecyclerView("detail", "Cages", operator != null ? operator.getNama() : "", ""));
//        arrayList.add(new ModelRecyclerView("detail", "Cages No.", operator != null ? operator.getNik() : "", ""));

        if(transaksiSPB.getTransaksiAngkut().getBin() != null){
            arrayList.add(new ModelRecyclerView("detail","Bin" ,transaksiSPB.getTransaksiAngkut().getBin(),""));
        }

        if(transaksiSPB.getTransaksiAngkut().getPengirimanGandas() != null) {
            Collections.sort(transaksiSPB.getTransaksiAngkut().getPengirimanGandas(), new Comparator<PengirimanGanda>() {
                @Override
                public int compare(PengirimanGanda o1, PengirimanGanda o2) {
                    return o1.getIdx() < o2.getIdx() ? -1 : (o1.getIdx() > o2.getIdx()) ? 1 : 0;
                }
            });

            arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_pengiriman_lanjut),"",""));
            arrayList.add(new ModelRecyclerView("detailgandaheader",
                    "Unit Ke",
                    "Kepemilikan",
                    "Unit",
                    "Operator"
            ));
            for (int i = 0; i < transaksiSPB.getTransaksiAngkut().getPengirimanGandas().size(); i++) {
                PengirimanGanda ganda = transaksiSPB.getTransaksiAngkut().getPengirimanGandas().get(i);
                int ke = i + 2;
                arrayList.add(new ModelRecyclerView("detailganda",
                        String.valueOf(ke) ,
                        ganda.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN ? TransaksiAngkut.KEBUN : TransaksiAngkut.LUAR,
                        TransaksiAngkutHelper.showVehicle(ganda.getVehicle()),
                        ganda.getOperator().getNama()
                ));
                if(ganda.getSpk() != null){
                    if(ganda.getSpk().getIdSPK() != null){
                        arrayList.add(new ModelRecyclerView("detailganda",
                                "" ,
                                "SPK",
                                ganda.getSpk().getIdSPK(),
                                ""
                        ));
                    }
                }
                if(ganda.getLangsiran() != null){
                    if(ganda.getLangsiran().getIdTujuan() != null){
                        arrayList.add(new ModelRecyclerView("detailganda",
                                "" ,
                                "Langsiran",
                                ganda.getLangsiran().getNamaTujuan(),
                                ganda.getLangsiran().getBlock()
                        ));
                    }
                }
            }
        }

        if(transaksiSPB.getTransaksiAngkut().getLoader() != null){
            for (int i = 0; i < transaksiSPB.getTransaksiAngkut().getLoader().size(); i++) {
                if(i == 0){
                    arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_loader),"",""));
                    arrayList.add(new ModelRecyclerView("detailloaderheader","Nama" ,"NIK","Estate"));
                }
                Operator loader = transaksiSPB.getTransaksiAngkut().getLoader().get(i);
                arrayList.add(new ModelRecyclerView("detailloader",loader.getNama() ,loader.getNik(),loader.getArea()));
            }
        }



        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_angkut),"",""));
        arrayList.add(new ModelRecyclerView("detail","Σ Angkut TPH" , angkutTph +" TPH",""));
        if(angkutTransit > 0 ) {
            arrayList.add(new ModelRecyclerView("detail", "Σ Angkut Langsiran", angkutTransit + " Langsiran", ""));
        }
        if(angkutKartuHilang != 0 ){
            arrayList.add(new ModelRecyclerView("detail", "Σ Kartu Hilang", angkutKartuHilang + " Kartu", ""));
        }
        if(angkutKartuRusak != 0 ){
            arrayList.add(new ModelRecyclerView("detail", "Σ Kartu Rusak", angkutKartuRusak + " Kartu", ""));
        }

        if(angkutMaxRestan != 0 ){
            int restanDay = GlobalHelper.MAX_RESTAN;
            if (dynamicParameterPenghasilan != null){
                restanDay = dynamicParameterPenghasilan.getMaxRestan();
            }
            arrayList.add(new ModelRecyclerView("detail", "Σ Kartu Restan Lebih "+restanDay+"H", angkutMaxRestan + " Kartu", ""));
        }
        arrayList.add(new ModelRecyclerView("detail","Σ Angkut" , angkutView.size() + " Pengangkutan",""));

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.tbs_angkut),"",""));
        arrayList.add(new ModelRecyclerView("detail","Janjang" ,String.format("%,d",transaksiSPB.getTotalJanjang()),"Jjg"));
        arrayList.add(new ModelRecyclerView("detail","Brondolan" ,String.format("%,d",transaksiSPB.getTotalBrondolan()),"Kg"));
        arrayList.add(new ModelRecyclerView("detail","Berat" ,String.format("%.0f",transaksiSPB.getTransaksiAngkut().getTotalBeratEstimasi()),"Kg"));

        if(infoRestan.getJjgH2() > 0) {
            int persen  = (infoRestan.getJjgH2() * transaksiSPB.getTotalJanjang()) / 100;
            arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_lama_restan),"",""));
            arrayList.add(new ModelRecyclerView("detail","Jjg Diatas 2 Hari" ,String.valueOf(infoRestan.getJjgH2())+ " ("+String.valueOf(persen)+")",""));
            arrayList.add(new ModelRecyclerView("detail","Jjg Paling Lama" ,String.valueOf(infoRestan.getLamaRestan()) +" Hari",""));
        }

        arrayList.add(new ModelRecyclerView("header","Detail Angkut","",""));
        arrayList.add(new ModelRecyclerView("subTotal","Angkut" ,
                "Janjang",
                "Brdl",
                "Berat"
        ));

        Collections.sort(angkutView, new Comparator<Angkut>() {
            @Override
            public int compare(Angkut t0, Angkut t1) {
                String a = t0.getBlock();
                String b = t1.getBlock();
                return (a.compareTo(b));
            }
        });
        Angkut subTotalAngkutBlock = null;
        int count = 0;
        for(int i = 0; i <angkutView.size(); i++) {
            Angkut angkut = angkutView.get(i);
            count = count + 1;
            String isiRow = "";
            String isiRow1 = "";
            String isiRow2 = "";
            String isiRow3 = "";
            String block = angkut.getBlock() == null ? "" : angkut.getBlock();
            if(angkut.getTransaksiPanen() != null){
                String tph = "TPH";
                if(angkut.getTransaksiPanen().getTph() != null){
                    tph = angkut.getTransaksiPanen().getTph().getNamaTph() != null ?
                        angkut.getTransaksiPanen().getTph().getNamaTph() : "TPH";
                    tph += angkut.getTransaksiPanen().getTph().getBlock() != null ?
                            "/" +angkut.getTransaksiPanen().getTph().getBlock() : "-";
                    block = angkut.getTransaksiPanen().getTph().getBlock() != null ?
                            angkut.getTransaksiPanen().getTph().getBlock() : "-";
                }
                isiRow = isiRow + tph;
            }else if (angkut.getTph() != null) {
                String nama ;
                nama = angkut.getTph().getNamaTph() != null ?
                        angkut.getTph().getNamaTph() : "TPH";
                nama += angkut.getTph().getBlock() != null ?
                        "/" +angkut.getTph().getBlock() : "-";
                block = angkut.getTph().getBlock() != null ?
                        angkut.getTph().getBlock() : "-";
                nama += " Manual";
                isiRow = isiRow + nama;
            }else if (angkut.getTransaksiAngkut() != null){
                String langsiran;
                if(angkut.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS){
                    langsiran = "PKS";
                }else{
                    if(angkut.getTransaksiAngkut().getLangsiran() != null){
                        langsiran = angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() != null ?
                                angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() : "Langsiran";
                        langsiran += angkut.getTransaksiAngkut().getLangsiran().getBlock() != null ?
                                "-" +angkut.getTransaksiAngkut().getLangsiran().getBlock() : "-";
                        block = angkut.getBlock() != null ?
                                angkut.getBlock() : "-";
                    }else{
                        langsiran = "Langsiran";
                    }
                }
                isiRow = isiRow + langsiran + "/" + block;
            }else if (angkut.getLangsiran() != null) {
                String nama;
                nama = angkut.getLangsiran().getNamaTujuan() != null ?
                        angkut.getLangsiran().getNamaTujuan(): "Langsiran";
                nama += angkut.getLangsiran().getBlock() != null ?
                        "/" +angkut.getLangsiran().getBlock(): "-";
                block = angkut.getLangsiran().getBlock() != null ?
                        angkut.getLangsiran().getBlock(): "-";
                isiRow = isiRow + nama;
            }else{
                isiRow = block;
            }

            if(subTotalAngkutBlock == null){
                subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
                        angkut.getBrondolan(),
                        block,
                        angkut.getBerat());
            }else if(subTotalAngkutBlock.getBlock().equals(block)){
                subTotalAngkutBlock.setJanjang(subTotalAngkutBlock.getJanjang() + angkut.getJanjang());
                subTotalAngkutBlock.setBrondolan(subTotalAngkutBlock.getBrondolan() + angkut.getBrondolan());
                subTotalAngkutBlock.setBerat(subTotalAngkutBlock.getBerat() + angkut.getBerat());
            }else if(!subTotalAngkutBlock.getBlock().equals(block)){
                arrayList.add(new ModelRecyclerView("subTotal","Block "+subTotalAngkutBlock.getBlock() ,
                        String.format("%,d Jjg",subTotalAngkutBlock.getJanjang()),
                        String.format("%,d Kg",subTotalAngkutBlock.getBrondolan()),
                        String.format("%.0f Kg",subTotalAngkutBlock.getBerat())
                ));
                subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
                        angkut.getBrondolan(),
                        block,
                        angkut.getBerat());
                count = 1;
            }

            isiRow1 = String.format("%,d",angkut.getJanjang()) + "Jjg";
            isiRow2 = String.format("%,d",angkut.getBrondolan())+"Kg";
            isiRow3 = String.format("%.0f",angkut.getBerat())+"Kg";
            arrayList.add(new ModelRecyclerView("detailisi", count + ". "+isiRow ,isiRow1,isiRow2,isiRow3));
        }
        arrayList.add(new ModelRecyclerView("subTotal","Block "+subTotalAngkutBlock.getBlock() ,
                String.format("%,d Jjg",subTotalAngkutBlock.getJanjang()),
                String.format("%,d Kg",subTotalAngkutBlock.getBrondolan()),
                String.format("%.0f Kg",subTotalAngkutBlock.getBerat())
        ));

        if(transaksiSPB.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS) {
            arrayList.add(new ModelRecyclerView("Qr", compres, "", ""));
        }

//        String qRDetailAngkut = toQRDetailAngkut(transaksiSPB);
//        if(qRDetailAngkut != null) {
//            arrayList.add(new ModelRecyclerView("header", activity.getResources().getString(R.string.detail_angkut), "", ""));
//            arrayList.add(new ModelRecyclerView("Qr", qRDetailAngkut, "", ""));
//        }

        rv.setLayoutManager(new LinearLayoutManager(activity));
        QrItemAdapter adapter = new QrItemAdapter(activity,arrayList);
        rv.setAdapter(adapter);

        shareqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qrHelper.share(activity,rv);
                if (activity instanceof AngkutActivity) {
                    ((AngkutActivity) activity).closeActivity();
                }else if (activity instanceof SpbActivity){
                    ((SpbActivity) activity).closeActivity();
                }
            }
        });
        listrefrence = WidgetHelper.showListReference(listrefrence,view,activity);
    }

    public void showQrMekanisasi(TransaksiSPB transaksiSPB, QrHelper qrHelper, ArrayList<TransaksiPanen> angkutView,ArrayList<Angkut> angkutViewKartu, boolean copy, DynamicParameterPenghasilan dynamicParameterPenghasilan){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyy HH:mm");
        View view = LayoutInflater.from(activity).inflate(R.layout.show_qr,null);
        RelativeLayout isi = (RelativeLayout) view.findViewById(R.id.isi);
//        ImageView iv_qrcode = (ImageView) view.findViewById(R.id.iv_qrcode);
        Button shareqr = (Button) view.findViewById(R.id.btn_shareqr);
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);
//        TextView tvHeaderQr = (TextView) view.findViewById(R.id.tvHeaderQr);

//        tvHeaderQr.setText(activity.getResources().getString(R.string.spb));f
        Operator operator = new Operator();
        Vehicle vehicle = new Vehicle();
//        try {sp
//            iv_qrcode.setImageBitmap(qrHelper.encodeAsBitmap(compres));
//        } catch (WriterException e) {
//            e.printStackTrace();
//        }

        ISCCInformation isccInformation = null;
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ISCCINFORMATION);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            ISCCInformation isccInformationX = gson.fromJson(dataNitrit.getValueDataNitrit(),ISCCInformation.class);
            if(isccInformationX.getEstCode().equals(transaksiSPB.getEstCode()) && ((new Double(isccInformationX.getValidFrom())).longValue() <= transaksiSPB.getCreateDate() && (new Double(isccInformationX.getValidUntil())).longValue() >= transaksiSPB.getCreateDate())){
                isccInformation = isccInformationX;
                break;
            }
        }
        db.close();

        int angkutTph = 0;
        int angkutTransit = 0;
        int angkutKartuHilang = 0;
        int angkutKartuRusak = 0;
        int angkutMaxRestan = 0;

        InfoRestan infoRestan = new InfoRestan();
        ArrayList<SubTotalAngkutMekanisasi> subTotalAngkutMekanisasis = new ArrayList<>();

        if(angkutView != null) {
            for (int i = 0; i < angkutView.size(); i++) {
                TransaksiPanen transaksiPanen = angkutView.get(i);

                SubTotalAngkutMekanisasi subTotalAngkutMekanisasi = new SubTotalAngkutMekanisasi(transaksiPanen.getIdTPanen(), transaksiPanen, null);
                subTotalAngkutMekanisasis.add(subTotalAngkutMekanisasi);
                if (transaksiPanen != null) {
                    if (transaksiPanen.getCreateDate() > 0) {
                        Integer selisih = GlobalHelper.getDateDiff(transaksiSPB.getCreateDate(), transaksiPanen.getCreateDate());
//                    Integer.parseInt(String.valueOf(
//                            Math.abs(TimeUnit.MILLISECONDS.toDays(
//                                    transaksiSPB.getCreateDate() - angkut.getTransaksiPanen().getCreateDate()
//                            ))
//                    ));

                        if (selisih >= 2) {
                            infoRestan.setJjgH2(infoRestan.getJjgH2() + transaksiPanen.getHasilPanen().getTotalJanjang());
                            if (selisih > infoRestan.getLamaRestan()) {
                                infoRestan.setLamaRestan(selisih);
                            }
                        }
                        angkutTph++;
                    }
                }
            }
        }

        if(angkutViewKartu != null) {
            for (int i = 0; i < angkutViewKartu.size(); i++) {
                Angkut angkut = angkutViewKartu.get(i);

                SubTotalAngkutMekanisasi subTotalAngkutMekanisasi = new SubTotalAngkutMekanisasi(angkut.getIdAngkut(),null,angkut);
                subTotalAngkutMekanisasis.add(subTotalAngkutMekanisasi);

                if (angkut.getManualSPBData() != null){
                    if(angkut.getManualSPBData().getTipeId() == ManualSPB.TYPE_KARTU_HILANG){
                        angkutKartuHilang++;
                    }else if (angkut.getManualSPBData().getTipeId() == ManualSPB.TYPE_KARTU_RUSAK){
                        angkutKartuRusak++;
                    }else if(angkut.getManualSPBData().getTipeId() == ManualSPB.TYPE_KARTU_MAX_RESTAN){
                        angkutMaxRestan++;
                    }
                }else if (angkut.getTransaksiPanen() != null) {
                    if(angkut.getTransaksiPanen().getCreateDate() > 0) {
                        Integer selisih = GlobalHelper.getDateDiff(transaksiSPB.getCreateDate(),angkut.getTransaksiPanen().getCreateDate());
//                        Integer selisih = Integer.parseInt(String.valueOf(
//                                Math.abs(TimeUnit.MILLISECONDS.toDays(
//                                        transaksiSpb.getCreateDate() - angkut.getTransaksiPanen().getCreateDate()
//                                ))
//                        ));

                        if (selisih >= 2) {
                            infoRestan.setJjgH2(infoRestan.getJjgH2() + angkut.getJanjang());
                            if (selisih > infoRestan.getLamaRestan()) {
                                infoRestan.setLamaRestan(selisih);
                            }
                        }
                    }
                    angkutTph++;
                } else if (angkut.getTransaksiAngkut() != null) {
                    angkutTransit++;
                }
            }
        }

        Estate estate = GlobalHelper.getEstateByEstCode(transaksiSPB.getEstCode());
        String afdeling = null;
        String compres = null;
        SPK spk = null;
        if(transaksiSPB.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS) {
            compres = toQrSpbMekanisasi(transaksiSPB);
            afdeling = TransaksiSpbHelper.getAfdelingFromQr(compres);
        }

        if(transaksiSPB.getTransaksiAngkut().getSpk() != null){
            spk = transaksiSPB.getTransaksiAngkut().getSpk();
        }
//        if(transaksiSPB.getAfdeling() != null){
//            if(transaksiSPB.getAfdeling().size() > 0){
//                for(String s : transaksiSPB.getAfdeling()){
//                    if(afdeling == null){
//                        afdeling = s ;
//                    }else{
//                        afdeling += "," + s;
//                    }
//                }
//            }
//        }

        ArrayList<ModelRecyclerView> arrayList = new ArrayList<>();
        arrayList.add(new ModelRecyclerView("headerQr","Surat Pengangkutan Buah","",""));
        arrayList.add(new ModelRecyclerView("Barcode",transaksiSPB.getNoSpb() == null ? converIdSPBToNoSPB(transaksiSPB.getIdSPB()) : transaksiSPB.getNoSpb(),"",""));
        if(transaksiSPB.getTransaksiAngkut().isRevisi()){
            arrayList.add(new ModelRecyclerView("header", "R E V I S I O N    C O P Y", "", ""));
        } else if(copy) {
            arrayList.add(new ModelRecyclerView("header", "C O P Y", "", ""));
        }

        if(isccInformation != null) {
            arrayList.add(new ModelRecyclerView("header", "", "", ""));
            arrayList.add(new ModelRecyclerView("iscc", "FFB SUSTAINABLE ISCC " + isccInformation.getEstNewCode(), "", ""));
            arrayList.add(new ModelRecyclerView("iscc", isccInformation.getCertificateNumber(), "", ""));
            arrayList.add(new ModelRecyclerView("iscc", "GHG Value Eec : " + isccInformation.getGhgValue(), "", ""));
            arrayList.add(new ModelRecyclerView("iscc", "SC Model : " + isccInformation.getCertificateType(), "", ""));
            arrayList.add(new ModelRecyclerView("iscc", "ISCC Compliant", "", ""));
            double pksDistance = TransaksiSpbHelper.cekLandDistance(transaksiSPB.getTransaksiAngkut());
            if (pksDistance > 0) {
                arrayList.add(new ModelRecyclerView("iscc", String.format("Jarak Ke PKS : %.1f KM", pksDistance),"" , ""));
            }
        }
        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_transaksi),"",""));
        if(estate != null) {
            arrayList.add(new ModelRecyclerView("detail", "PT", estate.getCompanyShortName(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Estate", estate.getEstName(), ""));
        }else{
            arrayList.add(new ModelRecyclerView("detail", "PT", activity.getResources().getString(R.string.dont_have_estate) + transaksiSPB.getEstCode(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Estate", activity.getResources().getString(R.string.dont_have_estate) + transaksiSPB.getEstCode(), ""));
        }

        if(afdeling != null){
            arrayList.add(new ModelRecyclerView("detail", "Afdeling", afdeling, ""));
        }
        if(spk != null){
            arrayList.add(new ModelRecyclerView("detail", "OA", TransaksiPanenHelper.showSPK(spk), ""));
        }

        User userSelected = null;
        if(qrHelper.context instanceof BaseActivity){
            userSelected = GlobalHelper.dataAllUser.get(transaksiSPB.getCreateBy());
            if(userSelected == null){
                arrayList.add(new ModelRecyclerView("detail", "Input Oleh", transaksiSPB.getCreateBy(), ""));
            }else {
                arrayList.add(new ModelRecyclerView("detail", "Input Oleh", userSelected.getUserFullName(), ""));
            }
        }else{
            arrayList.add(new ModelRecyclerView("detail","Input Oleh" ,transaksiSPB.getCreateBy(),""));
        }
        if(transaksiSPB.getSubstitute() != null){
            arrayList.add(new ModelRecyclerView("detail","Krani Penganti" ,transaksiSPB.getSubstitute().getNama(),""));
        }

        arrayList.add(new ModelRecyclerView("detail","Waktu Input" ,dateFormat.format(transaksiSPB.getCreateDate()),""));


        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_spb),"",""));
        arrayList.add(new ModelRecyclerView("detail","Id SPB" ,transaksiSPB.getNoSpb() == null ? converIdSPBToNoSPB(transaksiSPB.getIdSPB()) : transaksiSPB.getNoSpb(),""));
        User assAfdSelected = null;
        if(qrHelper.context instanceof BaseActivity){
            assAfdSelected = GlobalHelper.dataAllAsstAfd.get(transaksiSPB.getApproveBy());
            arrayList.add(new ModelRecyclerView("detail","Approve" ,assAfdSelected.getUserFullName(),""));
        }else{
            arrayList.add(new ModelRecyclerView("detail","Approve" ,transaksiSPB.getApproveBy(),""));
        }

        String tujuan = transaksiSPB.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS ? TransaksiAngkut.PKS :
                transaksiSPB.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_KELUAR ? TransaksiAngkut.KELAUR : "-";
        arrayList.add(new ModelRecyclerView("detail","Tujuan" ,tujuan,""));
        if(tujuan == TransaksiAngkut.PKS){
            arrayList.add(new ModelRecyclerView("detail","Nama PKS" ,transaksiSPB.getTransaksiAngkut().getPks().getNamaPKS() ,""));
        }

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_vehicle),"",""));
        operator = transaksiSPB.getTransaksiAngkut().getSupir();
        vehicle = transaksiSPB.getTransaksiAngkut().getVehicle();

        arrayList.add(new ModelRecyclerView("detail","Supir" ,operator != null ? operator.getNama():"",""));
        arrayList.add(new ModelRecyclerView("detail","Kendaraan" , TransaksiAngkutHelper.showVehicle(vehicle),""));

        if(vehicle.getVraOrderNumber() != null){
            arrayList.add(new ModelRecyclerView("detail","Vehicle No " , vehicle.getVraOrderNumber(),""));
        }else{
            arrayList.add(new ModelRecyclerView("detail","Vehicle No " , "-",""));
        }


        if (transaksiSPB.getTransaksiAngkut().getCages() != null) {
            if (!transaksiSPB.getTransaksiAngkut().getCages().isEmpty()){
                int count = 1;
                for (int i = 0; i < transaksiSPB.getTransaksiAngkut().getCages().size(); i++){
                    Cages cages = transaksiSPB.getTransaksiAngkut().getCages().get(i);
                    arrayList.add(new ModelRecyclerView("detail", "Cages No. "  + String.format("%,d", count) ,cages.getAssetNo() != null ? cages.getAssetNo() : "", "" ));
                    count++;
                }
            }
        }

//        arrayList.add(new ModelRecyclerView("detail", "Cages", operator != null ? operator.getNama() : "", ""));
//        arrayList.add(new ModelRecyclerView("detail", "Cages No.", operator != null ? operator.getNik() : "", ""));

        if(transaksiSPB.getTransaksiAngkut().getBin() != null){
            arrayList.add(new ModelRecyclerView("detail","Bin" ,transaksiSPB.getTransaksiAngkut().getBin(),""));
        }

        if(transaksiSPB.getTransaksiAngkut().getPengirimanGandas() != null) {
            Collections.sort(transaksiSPB.getTransaksiAngkut().getPengirimanGandas(), new Comparator<PengirimanGanda>() {
                @Override
                public int compare(PengirimanGanda o1, PengirimanGanda o2) {
                    return o1.getIdx() < o2.getIdx() ? -1 : (o1.getIdx() > o2.getIdx()) ? 1 : 0;
                }
            });

            arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_pengiriman_lanjut),"",""));
            arrayList.add(new ModelRecyclerView("detailgandaheader",
                    "Unit Ke",
                    "Kepemilikan",
                    "Unit",
                    "Operator"
            ));
            for (int i = 0; i < transaksiSPB.getTransaksiAngkut().getPengirimanGandas().size(); i++) {
                PengirimanGanda ganda = transaksiSPB.getTransaksiAngkut().getPengirimanGandas().get(i);
                int ke = i + 2;
                arrayList.add(new ModelRecyclerView("detailganda",
                        String.valueOf(ke) ,
                        ganda.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN ? TransaksiAngkut.KEBUN : TransaksiAngkut.LUAR,
                        TransaksiAngkutHelper.showVehicle(ganda.getVehicle()),
                        ganda.getOperator().getNama()
                ));
                if(ganda.getSpk() != null){
                    if(ganda.getSpk().getIdSPK() != null){
                        arrayList.add(new ModelRecyclerView("detailganda",
                                "" ,
                                "SPK",
                                ganda.getSpk().getIdSPK(),
                                ""
                        ));
                    }
                }
                if(ganda.getLangsiran() != null){
                    if(ganda.getLangsiran().getIdTujuan() != null){
                        arrayList.add(new ModelRecyclerView("detailganda",
                                "" ,
                                "Langsiran",
                                ganda.getLangsiran().getNamaTujuan(),
                                ganda.getLangsiran().getBlock()
                        ));
                    }
                }
            }
        }

        if(transaksiSPB.getTransaksiAngkut().getLoader() != null){
            for (int i = 0; i < transaksiSPB.getTransaksiAngkut().getLoader().size(); i++) {
                if(i == 0){
                    arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_loader),"",""));
                    arrayList.add(new ModelRecyclerView("detailloaderheader","Nama" ,"NIK","Estate"));
                }
                Operator loader = transaksiSPB.getTransaksiAngkut().getLoader().get(i);
                arrayList.add(new ModelRecyclerView("detailloader",loader.getNama() ,loader.getNik(),loader.getArea()));
            }
        }

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_angkut),"",""));
        arrayList.add(new ModelRecyclerView("detail","Σ Angkut TPH" , angkutTph +" TPH",""));

        arrayList.add(new ModelRecyclerView("detail","Σ Angkut" , angkutView.size() + " Pengangkutan",""));

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.tbs_angkut),"",""));
        arrayList.add(new ModelRecyclerView("detail","Janjang" ,String.format("%,d",transaksiSPB.getTotalJanjang()),"Jjg"));
        arrayList.add(new ModelRecyclerView("detail","Brondolan" ,String.format("%,d",transaksiSPB.getTotalBrondolan()),"Kg"));
        arrayList.add(new ModelRecyclerView("detail","Berat" ,String.format("%.0f",transaksiSPB.getTransaksiAngkut().getTotalBeratEstimasi()),"Kg"));

        if(infoRestan.getJjgH2() > 0) {
            int persen  = (infoRestan.getJjgH2() * transaksiSPB.getTotalJanjang()) / 100;
            arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_lama_restan),"",""));
            arrayList.add(new ModelRecyclerView("detail","Jjg Diatas 2 Hari" ,String.valueOf(infoRestan.getJjgH2())+ " ("+String.valueOf(persen)+")",""));
            arrayList.add(new ModelRecyclerView("detail","Jjg Paling Lama" ,String.valueOf(infoRestan.getLamaRestan()) +" Hari",""));
        }

        arrayList.add(new ModelRecyclerView("header","Detail Angkut","",""));
        arrayList.add(new ModelRecyclerView("subTotal","Angkut" ,
                "Janjang",
                "Brdl",
                "Berat"
        ));

        Collections.sort(subTotalAngkutMekanisasis, new Comparator<SubTotalAngkutMekanisasi>() {
            @Override
            public int compare(SubTotalAngkutMekanisasi t0, SubTotalAngkutMekanisasi t1) {
                String a = "";
                String b = "";
                if(t0.getTransaksiPanen() != null){
                    a = t0.getTransaksiPanen().getTph().getBlock();
                }else if (t0.getAngkut() != null){
                    a = t0.getAngkut().getBlock();
                }

                if(t1.getTransaksiPanen() != null) {
                    b = t1.getTransaksiPanen().getTph().getBlock();
                }else if(t1.getAngkut() != null) {
                    b = t1.getAngkut().getBlock();
                }

                return (a.compareTo(b));
            }
        });
        Angkut subTotalAngkutBlock = null;
        int count = 0;
        for(int i = 0; i <subTotalAngkutMekanisasis.size(); i++) {
            if (subTotalAngkutMekanisasis.get(i).getTransaksiPanen() != null) {
                TransaksiPanen transaksiPanen = subTotalAngkutMekanisasis.get(i).getTransaksiPanen();
                count = count + 1;
                String isiRow = "";
                String isiRow1 = "";
                String isiRow2 = "";
                String isiRow3 = "";
                String block = transaksiPanen.getTph().getBlock() == null ? "" : transaksiPanen.getTph().getBlock();
                if (transaksiPanen != null) {
                    String tph = "TPH";
                    if (transaksiPanen.getTph() != null) {
                        tph = transaksiPanen.getTph().getNamaTph() != null ?
                                transaksiPanen.getTph().getNamaTph() : "TPH";
                        tph += transaksiPanen.getTph().getBlock() != null ?
                                "/" + transaksiPanen.getTph().getBlock() : "-";
                        block = transaksiPanen.getTph().getBlock() != null ?
                                transaksiPanen.getTph().getBlock() : "-";
                    }
                    isiRow = isiRow + tph;
                } else {
                    isiRow = block;
                }

                if (subTotalAngkutBlock == null) {
                    subTotalAngkutBlock = new Angkut(
                            transaksiPanen.getHasilPanen().getTotalJanjang(),
                            transaksiPanen.getHasilPanen().getBrondolan(),
                            block,
                            (transaksiPanen.getHasilPanen().getTotalJanjang() * transaksiPanen.getBjrMekanisasi())
                    );
                } else if (subTotalAngkutBlock.getBlock().equals(block)) {
                    subTotalAngkutBlock.setJanjang(subTotalAngkutBlock.getJanjang() + transaksiPanen.getHasilPanen().getTotalJanjang());
                    subTotalAngkutBlock.setBrondolan(subTotalAngkutBlock.getBrondolan() + transaksiPanen.getHasilPanen().getBrondolan());
                    subTotalAngkutBlock.setBerat(subTotalAngkutBlock.getBerat() + (transaksiPanen.getHasilPanen().getTotalJanjang() * transaksiPanen.getBjrMekanisasi()));
                } else if (!subTotalAngkutBlock.getBlock().equals(block)) {
                    arrayList.add(new ModelRecyclerView("subTotal", "Block " + subTotalAngkutBlock.getBlock(),
                            String.format("%,d Jjg", subTotalAngkutBlock.getJanjang()),
                            String.format("%,d Kg", subTotalAngkutBlock.getBrondolan()),
                            String.format("%.0f Kg", subTotalAngkutBlock.getBerat())
                    ));
                    subTotalAngkutBlock = new Angkut(
                            transaksiPanen.getHasilPanen().getTotalJanjang(),
                            transaksiPanen.getHasilPanen().getBrondolan(),
                            block,
                            (transaksiPanen.getHasilPanen().getTotalJanjang() * transaksiPanen.getBjrMekanisasi())
                    );
                    count = 1;
                }

                isiRow1 = String.format("%,d", transaksiPanen.getHasilPanen().getTotalJanjang()) + "Jjg";
                isiRow2 = String.format("%,d", transaksiPanen.getHasilPanen().getBrondolan()) + "Kg";
                isiRow3 = String.format("%.0f", (transaksiPanen.getHasilPanen().getTotalJanjang() * transaksiPanen.getBjrMekanisasi())) + "Kg";
                arrayList.add(new ModelRecyclerView("detailisi", count + ". " + isiRow, isiRow1, isiRow2, isiRow3));
            }else if (subTotalAngkutMekanisasis.get(i).getAngkut() != null) {

                Angkut angkut = subTotalAngkutMekanisasis.get(i).getAngkut();
                count = count + 1;
                String isiRow = "";
                String isiRow1 = "";
                String isiRow2 = "";
                String isiRow3 = "";
                String block = angkut.getBlock() == null ? "" : angkut.getBlock();
                if(angkut.getTransaksiPanen() != null){
                    String tph = "TPH";
                    if(angkut.getTransaksiPanen().getTph() != null){
                        tph = angkut.getTransaksiPanen().getTph().getNamaTph() != null ?
                                angkut.getTransaksiPanen().getTph().getNamaTph() : "TPH";
                        tph += angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                "/" +angkut.getTransaksiPanen().getTph().getBlock() : "-";
                        block = angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                angkut.getTransaksiPanen().getTph().getBlock() : "-";
                    }
                    isiRow = isiRow + tph;
                }else if (angkut.getTph() != null) {
                    String nama ;
                    nama = angkut.getTph().getNamaTph() != null ?
                            angkut.getTph().getNamaTph() : "TPH";
                    nama += angkut.getTph().getBlock() != null ?
                            "/" +angkut.getTph().getBlock() : "-";
                    block = angkut.getTph().getBlock() != null ?
                            angkut.getTph().getBlock() : "-";
                    nama += " Manual";
                    isiRow = isiRow + nama;
                }else if (angkut.getTransaksiAngkut() != null){
                    String langsiran;
                    if(angkut.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS){
                        langsiran = "PKS";
                    }else{
                        if(angkut.getTransaksiAngkut().getLangsiran() != null){
                            langsiran = angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() != null ?
                                    angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() : "Langsiran";
                            langsiran += angkut.getTransaksiAngkut().getLangsiran().getBlock() != null ?
                                    "-" +angkut.getTransaksiAngkut().getLangsiran().getBlock() : "-";
                            block = angkut.getBlock() != null ?
                                    angkut.getBlock() : "-";
                        }else{
                            langsiran = "Langsiran";
                        }
                    }
                    isiRow = isiRow + langsiran + "/" + block;
                }else if (angkut.getLangsiran() != null) {
                    String nama;
                    nama = angkut.getLangsiran().getNamaTujuan() != null ?
                            angkut.getLangsiran().getNamaTujuan(): "Langsiran";
                    nama += angkut.getLangsiran().getBlock() != null ?
                            "/" +angkut.getLangsiran().getBlock(): "-";
                    block = angkut.getLangsiran().getBlock() != null ?
                            angkut.getLangsiran().getBlock(): "-";
                    isiRow = isiRow + nama;
                }else{
                    isiRow = block;
                }

                if(subTotalAngkutBlock == null){
                    subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
                            angkut.getBrondolan(),
                            block,
                            angkut.getBerat());
                }else if(subTotalAngkutBlock.getBlock().equals(block)){
                    subTotalAngkutBlock.setJanjang(subTotalAngkutBlock.getJanjang() + angkut.getJanjang());
                    subTotalAngkutBlock.setBrondolan(subTotalAngkutBlock.getBrondolan() + angkut.getBrondolan());
                    subTotalAngkutBlock.setBerat(subTotalAngkutBlock.getBerat() + angkut.getBerat());
                }else if(!subTotalAngkutBlock.getBlock().equals(block)){
                    arrayList.add(new ModelRecyclerView("subTotal","Block "+subTotalAngkutBlock.getBlock() ,
                            String.format("%,d Jjg",subTotalAngkutBlock.getJanjang()),
                            String.format("%,d Kg",subTotalAngkutBlock.getBrondolan()),
                            String.format("%.0f Kg",subTotalAngkutBlock.getBerat())
                    ));
                    subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
                            angkut.getBrondolan(),
                            block,
                            angkut.getBerat());
                    count = 1;
                }

                isiRow1 = String.format("%,d",angkut.getJanjang()) + "Jjg";
                isiRow2 = String.format("%,d",angkut.getBrondolan())+"Kg";
                isiRow3 = String.format("%.0f",angkut.getBerat())+"Kg";
                arrayList.add(new ModelRecyclerView("detailisi", count + ". "+isiRow ,isiRow1,isiRow2,isiRow3));
            }
        }
        arrayList.add(new ModelRecyclerView("subTotal","Block "+subTotalAngkutBlock.getBlock() ,
                String.format("%,d Jjg",subTotalAngkutBlock.getJanjang()),
                String.format("%,d Kg",subTotalAngkutBlock.getBrondolan()),
                String.format("%.0f Kg",subTotalAngkutBlock.getBerat())
        ));

        if(transaksiSPB.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS) {
            arrayList.add(new ModelRecyclerView("Qr", compres, "", ""));
        }

//        String qRDetailAngkut = toQRDetailAngkut(transaksiSPB);
//        if(qRDetailAngkut != null) {
//            arrayList.add(new ModelRecyclerView("header", activity.getResources().getString(R.string.detail_angkut), "", ""));
//            arrayList.add(new ModelRecyclerView("Qr", qRDetailAngkut, "", ""));
//        }

        rv.setLayoutManager(new LinearLayoutManager(activity));
        QrItemAdapter adapter = new QrItemAdapter(activity,arrayList);
        rv.setAdapter(adapter);

        shareqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qrHelper.share(activity,rv);
                if (activity instanceof MekanisasiActivity) {
                    ((MekanisasiActivity) activity).closeActivity();
                }
            }
        });
        listrefrence = WidgetHelper.showListReference(listrefrence,view,activity);
    }

    public void showRekonsiliasiSPBQr(TransaksiSPB transaksiSPB,QrHelper qrHelper,ArrayList<Angkut> angkutView){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyy HH:mm");
        View view = LayoutInflater.from(activity).inflate(R.layout.show_qr,null);
        RelativeLayout isi = (RelativeLayout) view.findViewById(R.id.isi);
//        ImageView iv_qrcode = (ImageView) view.findViewById(R.id.iv_qrcode);
        Button shareqr = (Button) view.findViewById(R.id.btn_shareqr);
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);
//        TextView tvHeaderQr = (TextView) view.findViewById(R.id.tvHeaderQr);

//        tvHeaderQr.setText(activity.getResources().getString(R.string.spb));
//        String compres = toQrSpb(transaksiSPB);
//        try {
//            iv_qrcode.setImageBitmap(qrHelper.encodeAsBitmap(compres));
//        } catch (WriterException e) {
//            e.printStackTrace();
//        }




        Estate estate = GlobalHelper.getEstateByEstCode(transaksiSPB.getEstCode());

        ArrayList<ModelRecyclerView> arrayList = new ArrayList<>();
        arrayList.add(new ModelRecyclerView("headerQr","Surat Rekonsiliasi","",""));
        arrayList.add(new ModelRecyclerView("Barcode",transaksiSPB.getNoSpb() == null ? converIdSPBToNoSPB(transaksiSPB.getIdSPB()) : transaksiSPB.getNoSpb(),"",""));
//        if(isccInformation != null) {
//            arrayList.add(new ModelRecyclerView("header","","",""));
//            arrayList.add(new ModelRecyclerView("iscc", "FFB SUSTAINABLE ISCC " + isccInformation.getEstNewCode(), "", ""));
//            arrayList.add(new ModelRecyclerView("iscc", isccInformation.getCertificateNumber(), "", ""));
//            arrayList.add(new ModelRecyclerView("iscc", "GHG Value Eec : "+isccInformation.getGhgValue(), "", ""));
//            arrayList.add(new ModelRecyclerView("iscc", "SC Model : "+isccInformation.getCertificateType(), "", ""));
//        }
        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_rekonsilasi).toUpperCase(),"",""));
        if(estate != null) {
            arrayList.add(new ModelRecyclerView("detail", "PT", estate.getCompanyShortName(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Estate", estate.getEstName(), ""));
        }else{
            arrayList.add(new ModelRecyclerView("detail", "PT", activity.getResources().getString(R.string.dont_have_estate) + transaksiSPB.getEstCode(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Estate", activity.getResources().getString(R.string.dont_have_estate) + transaksiSPB.getEstCode(), ""));
        }

        User userSelected = null;
        if(qrHelper.context instanceof BaseActivity){
            userSelected = GlobalHelper.dataAllUser.get(transaksiSPB.getCreateBy());
            arrayList.add(new ModelRecyclerView("detail","Input Oleh" ,userSelected.getUserFullName(),""));
        }else{
            arrayList.add(new ModelRecyclerView("detail","Input Oleh" ,transaksiSPB.getCreateBy(),""));
        }
        arrayList.add(new ModelRecyclerView("detail","Waktu Input" ,dateFormat.format(transaksiSPB.getCreateDate()),""));

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_spb),"",""));
        arrayList.add(new ModelRecyclerView("detail","Id SPB" ,transaksiSPB.getNoSpb(),""));
        User assAfdSelected = null;
        if(qrHelper.context instanceof BaseActivity){
            assAfdSelected = GlobalHelper.dataAllAsstAfd.get(transaksiSPB.getApproveBy());
            arrayList.add(new ModelRecyclerView("detail","Approve" ,assAfdSelected.getUserFullName(),""));
        }else{
            arrayList.add(new ModelRecyclerView("detail","Approve" ,transaksiSPB.getApproveBy(),""));
        }

        String tujuan = transaksiSPB.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS ? TransaksiAngkut.PKS :
                transaksiSPB.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_KELUAR ? TransaksiAngkut.KELAUR : "-";
        arrayList.add(new ModelRecyclerView("detail","Tujuan" ,tujuan,""));
        if(tujuan == TransaksiAngkut.PKS){
            arrayList.add(new ModelRecyclerView("detail","Nama PKS" ,transaksiSPB.getTransaksiAngkut().getPks().getNamaPKS() ,""));
        }


        Operator operator = transaksiSPB.getTransaksiAngkut().getSupir();
        Vehicle vehicle = transaksiSPB.getTransaksiAngkut().getVehicle();
        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_vehicle),"",""));
        arrayList.add(new ModelRecyclerView("detail","Supir" ,operator.getNama() != null ? operator.getNama() : "",""));
        arrayList.add(new ModelRecyclerView("detail","Kendaraan" ,vehicle.getVehicleCode() != null ? vehicle.getVehicleCode() : "",""));

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_angkut),"",""));
//        arrayList.add(new ModelRecyclerView("detail","Σ Angkut TPH" ,String.valueOf(angkutTph)+" TPH",""));
//        arrayList.add(new ModelRecyclerView("detail","Σ Angkut Langsiran" ,String.valueOf(angkutTransit)+" Langsiran",""));
        arrayList.add(new ModelRecyclerView("detail","Σ Manual Data" , transaksiSPB.getTransaksiAngkut().getAngkuts().size() + " Pengangkutan",""));
        arrayList.add(new ModelRecyclerView("detail","Mulai Angkut" ,dateFormat.format(transaksiSPB.getTransaksiAngkut().getStartDate()),""));
        arrayList.add(new ModelRecyclerView("detail","Selesai Angkut" ,dateFormat.format(transaksiSPB.getTransaksiAngkut().getEndDate()),""));

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.tbs_tipe).toUpperCase(),"",""));
        int totalKartuRusak = 0;
        int totalKartuHilang = 0;

        for(Angkut angkut : transaksiSPB.getTransaksiAngkut().getAngkuts()){
            if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                totalKartuRusak++;
            }else if(angkut.getManualSPBData().getTipe().equals("Kartu Hilang")){
                totalKartuHilang++;
            }
        }
        arrayList.add(new ModelRecyclerView("detail","Kartu Rusak" ,String.format("%,d",totalKartuRusak),""));
        arrayList.add(new ModelRecyclerView("detail","Kartu Hilang" ,String.format("%,d",totalKartuHilang),""));

        arrayList.add(new ModelRecyclerView("header","DETAIL ANGKUT","",""));
        arrayList.add(new ModelRecyclerView("subTotal","TPH" ,
                "Kartu Rusak",
                "Kartu Hilang"
        ));

        Collections.sort(angkutView, new Comparator<Angkut>() {
            @Override
            public int compare(Angkut t0, Angkut t1) {
                String a = t0.getBlock();
                String b = t1.getBlock();
                return (a.compareTo(b));
            }
        });
        Angkut subTotalAngkutBlock = null;
        int count = 0;
        int totalBlockKartuRusak = 0;
        int totalBlockKartuHilang = 0;
        for (int i = 0 ; i < angkutView.size(); i++){
            Angkut angkut = angkutView.get(i);
            if(angkut.getManualSPBData()!=null){
                count = count + 1;
                String nama = "Manual";
                String nama1 = "0";
                String nama2 = "0";
                String block = "";

                int subKartuHilang=0;
                int subKartuRusak=0;

                if(angkut.getTransaksiPanen() != null){
                    nama = "TPH";
                    if(angkut.getTransaksiPanen().getTph() != null){
                        nama = angkut.getTransaksiPanen().getTph().getNamaTph() != null ?
                                angkut.getTransaksiPanen().getTph().getNamaTph() : "TPH";
                        nama += angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                "/" +angkut.getTransaksiPanen().getTph().getBlock() : "-";
                        block = angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                angkut.getTransaksiPanen().getTph().getBlock() : "-";
                    }
                }else if (angkut.getTph() != null) {
                    nama = angkut.getTph().getNamaTph() != null ?
                            angkut.getTph().getNamaTph() : "TPH";
                    nama += angkut.getTph().getBlock() != null ?
                            "/" +angkut.getTph().getBlock() : "-";
                    block = angkut.getTph().getBlock() != null ?
                            angkut.getTph().getBlock() : "-";
                }else if (angkut.getTransaksiAngkut() != null){
                    nama = "Langsiran";
                    if(angkut.getTransaksiAngkut().getLangsiran() != null){
                        nama = angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() != null ?
                                angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() : "Langsiran";
                        nama += angkut.getTransaksiAngkut().getLangsiran().getBlock() != null ?
                                "/" +angkut.getTransaksiAngkut().getLangsiran().getBlock() : "-";
                        block = angkut.getTransaksiAngkut().getLangsiran().getBlock() != null ?
                                angkut.getTransaksiAngkut().getLangsiran().getBlock() : "-";
                    }
                }else if (angkut.getLangsiran() != null) {
                    nama = angkut.getLangsiran().getNamaTujuan() != null ?
                            angkut.getLangsiran().getNamaTujuan(): "Langsiran";
                    nama += angkut.getLangsiran().getBlock() != null ?
                            "/" +angkut.getLangsiran().getBlock(): "-";
                    block = angkut.getLangsiran().getBlock() != null ?
                            angkut.getLangsiran().getBlock(): "-";
                }

                if(subTotalAngkutBlock == null){
                    subTotalAngkutBlock = angkut;
                    if(angkut.getManualSPBData()!=null){
                        if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                            totalBlockKartuRusak++;
                        }else{
                            totalBlockKartuHilang++;
                        }
                    }
                }else if(subTotalAngkutBlock.getBlock().equals(block)){
                    if(angkut.getManualSPBData()!=null){
                        if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                            totalBlockKartuRusak++;
                        }else{
                            totalBlockKartuHilang++;
                        }
                    }
                }else if(!subTotalAngkutBlock.getBlock().equals(block)){
                    arrayList.add(new ModelRecyclerView("subTotal","Block "+subTotalAngkutBlock.getBlock() ,
                            String.format("%,d", totalBlockKartuRusak),
                            String.format("%,d", totalBlockKartuHilang)));
                    Log.i("getManualSPBData", "totalBlockKartuRusak: "+totalBlockKartuRusak+" totalBlockKartuHilang: "+totalBlockKartuHilang);
                    totalBlockKartuRusak = 0;
                    totalBlockKartuHilang = 0;
                    subTotalAngkutBlock = angkut;
                    if(subTotalAngkutBlock.getManualSPBData().getTipe().equals("Kartu Rusak")){
                        totalBlockKartuRusak++;
                    }else{
                        totalBlockKartuHilang++;
                    }
                    count = 1;
                }

                if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                    subKartuRusak = 1;
                    subKartuHilang = 0;
                }else{
                    subKartuRusak = 0;
                    subKartuHilang = 1;
                }

                nama1 = String.format("%,d",subKartuRusak) ;
                nama2 = String.format("%,d",subKartuHilang) ;
                arrayList.add(new ModelRecyclerView("detailisi", count + ". "+nama,nama1,nama2));
                if(i == transaksiSPB.getTransaksiAngkut().getAngkuts().size()-1){
                    arrayList.add(new ModelRecyclerView("subTotal","Block "+subTotalAngkutBlock.getBlock() ,
                            String.format("%,d", totalBlockKartuRusak),
                            String.format("%,d", totalBlockKartuHilang)));
                    Log.i("getManualSPBData", "totalBlockKartuRusak: "+totalBlockKartuRusak+" totalBlockKartuHilang: "+totalBlockKartuHilang);
                }
            }

        }

        arrayList.add(new ModelRecyclerView("subTotal","Grand Total" ,
                String.format("%,d",totalKartuRusak),
                String.format("%,d",totalKartuHilang)
        ));

//        arrayList.add(new ModelRecyclerView("Qr",compres,"",""));

        rv.setLayoutManager(new LinearLayoutManager(activity));
        QrItemAdapter adapter = new QrItemAdapter(activity,arrayList);
        rv.setAdapter(adapter);

        shareqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qrHelper.share(activity,rv);
                if (activity instanceof AngkutActivity) {
                    ((AngkutActivity) activity).closeActivity();
                }else if (activity instanceof SpbActivity){
                    ((SpbActivity) activity).closeActivity();
                }
            }
        });
        listrefrence = WidgetHelper.showListReference(listrefrence,view,activity);
    }

    public static double cekLandDistance(TransaksiAngkut transaksiAngkut){
        double landDistance = 0;
        if(transaksiAngkut.getPks() != null) {
            if (transaksiAngkut.getPks().getLandDistance() != null) {
                landDistance = transaksiAngkut.getPks().getLandDistance();
                if (transaksiAngkut.getVehicle().getVehicleClasification() != null) {
                    if (transaksiAngkut.getVehicle().getVehicleClasification().equalsIgnoreCase(Vehicle.vehicleClasificationWater)) {
                        landDistance = transaksiAngkut.getPks().getWaterDistance();
                    }
                }
            }
        }
        return landDistance;
    }

    public static boolean cekFileSpbSelected(){
        File fileSPBSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_SPB] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_SPB] );
        return fileSPBSelected.exists();
    }

    public static void hapusFileAngkut(boolean withDb){
        File fileSPBSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_SPB] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_SPB] );
        File fileDBAngkut = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT_SPB) + "/" +Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT_SPB)+ ".db");
        File fileDBAngkut2 = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI) + "/" +Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI)+ ".db");

        if(fileSPBSelected.exists()){
            fileSPBSelected.delete();
        }
        if(fileDBAngkut.exists() && withDb){
            fileDBAngkut.delete();
        }
        if(fileDBAngkut2.exists() && withDb){
            fileDBAngkut2.delete();
        }
    }

    public static void createFileSpb(String selected){
        File fileSPBSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_SPB] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_SPB] );
        GlobalHelper.writeFileContent(fileSPBSelected.getAbsolutePath(),selected);
        Log.d("transaksispbhelper", "createFileSpb: " + selected);
    }

    public static String readFileSpb(){
        File fileSPBSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_SPB] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_SPB] );
        Log.d("transaksispbhelper", "readFileSpb: " + GlobalHelper.readFileContent(fileSPBSelected.getAbsolutePath()));
        return GlobalHelper.readFileContent(fileSPBSelected.getAbsolutePath());
    }

    public static String converIdSPBToNoSPB(String idSpb){
        return idSpb.substring(3,5) + "-"+ idSpb.substring(5,11)+"-"+ idSpb.substring(11);
    }

    public static String toQRDetailAngkut(TransaksiSPB transaksiSPB){
        TransaksiAngkut transaksiAngkut = transaksiSPB.getTransaksiAngkut();
        JSONArray jsonArray = new JSONArray();

        for(int i = 0 ; i < transaksiAngkut.getAngkuts().size();i++){
            Angkut angkut = transaksiAngkut.getAngkuts().get(i);
            String ophCode = angkut.getOph();

            if(ophCode != null){
                ophCode = ophCode.equals("MANUAL") ? null : ophCode;
            }

            if(ophCode != null) {
                try {
                    String[] ophSplit = ophCode.split("-");
                    String ophA = ophSplit[1];
                    String runningNumber = ophSplit[0].substring(1, 5);
                    String date = ophSplit[0].substring(5, 11);
                    String create = ophSplit[0].substring(11);

                    if(jsonArray.length() > 0) {
                        for (int j = 0; j < jsonArray.length(); j++) {
                            JSONObject joCreate = jsonArray.getJSONObject(j);
                            if(joCreate.getString("a").equals(create)){
                                JSONArray jaCreate = joCreate.getJSONArray("aa");
                                for(int k = 0 ; k < jaCreate.length() ; k++){
                                    JSONObject joDate = jaCreate.getJSONObject(k);
                                    if(joDate.getString("b").equals(date)){
                                        JSONArray jaDate = joDate.getJSONArray("ba");
                                        jaDate.put(runningNumber +"-"+ ophA);
                                    }else{
                                        JSONArray jaDate = new JSONArray();
                                        jaDate.put(runningNumber +"-"+ ophA);
                                        joDate = new JSONObject();
                                        joDate.put("ba",jaDate);
                                        joDate.put("b",date);

                                        jaCreate.put(joDate);
                                    }
                                }
                            }else{
                                JSONArray jaDate = new JSONArray();
                                jaDate.put(runningNumber +"-"+ ophA);
                                JSONObject joDate = new JSONObject();
                                joDate.put("ba",jaDate);
                                joDate.put("b",date);

                                JSONArray jaCreate = new JSONArray();
                                jaCreate.put(joDate);
                                joCreate = new JSONObject();
                                joCreate.put("aa",jaCreate);
                                joCreate.put("a",create);

                                jsonArray.put(joCreate);
                            }
                        }
                    }else{

                        JSONArray jaDate = new JSONArray();
                        jaDate.put(runningNumber +"-"+ ophA);
                        JSONObject joDate = new JSONObject();
                        joDate.put("ba",jaDate);
                        joDate.put("b",date);

                        JSONArray jaCreate = new JSONArray();
                        jaCreate.put(joDate);
                        JSONObject joCreate = new JSONObject();
                        joCreate.put("aa",jaCreate);
                        joCreate.put("a",create);

                        jsonArray.put(joCreate);

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        if(jsonArray.length() > 0) {
            JSONObject objSend = new JSONObject();
            try {
                objSend.put("x", jsonArray);
                objSend.put("y", transaksiSPB.getIdSPB());
                objSend.put("z", GlobalHelper.TYPE_ISI_ANGKUT);
                Log.d("TRANSAKSISPBHELPER", "toQRDetailAngkut: "+ objSend.toString());
                return objSend.toString();
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }else {
            return null;
        }
    }
}
