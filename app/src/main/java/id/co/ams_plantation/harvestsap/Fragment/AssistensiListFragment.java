package id.co.ams_plantation.harvestsap.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.labo.kaji.fragmentanimations.MoveAnimation;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.AssistensiAdapter;
import id.co.ams_plantation.harvestsap.adapter.RefreshHeaderRecycleView;
import id.co.ams_plantation.harvestsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestsap.connection.Tag;
import id.co.ams_plantation.harvestsap.model.AssistensiRequest;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.ui.AssistensiActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.SetUpDataSyncHelper;
import id.co.ams_plantation.harvestsap.util.UploadHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;

import static id.co.ams_plantation.harvestsap.Fragment.SettingFragment.ButtonGroup_Lokal;
import static id.co.ams_plantation.harvestsap.Fragment.SettingFragment.ButtonGroup_Public;

public class AssistensiListFragment extends Fragment {

    AssistensiActivity assistensiActivity;
    SegmentedButtonGroup segmentedButtonGroup;
    LinearLayout lnoData;
    CompleteTextViewHelper etSearch;
    RecyclerView rv;
    RefreshLayout refreshRv;
    AssistensiAdapter assistensiAdapter;
    SetUpDataSyncHelper setUpDataSyncHelper;
    UploadHelper uploadHelper;
    JSONObject objSetUp;

    ServiceResponse serviceResponse;
    ArrayList<AssistensiRequest> requestArrayList;

    final int LongOperation_SetupDataUpload = 0;
    final int LongOperation_SetUpDataDownload = 1;
    final int LongOperation_SetUpData = 2;

    public static AssistensiListFragment getInstance() {
        AssistensiListFragment inputFragment = new AssistensiListFragment();
        return inputFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return MoveAnimation.create(MoveAnimation.UP,enter,600);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.assistensi_list_layout, null, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        assistensiActivity = ((AssistensiActivity) getActivity());
        setUpDataSyncHelper = new SetUpDataSyncHelper(getActivity());
        uploadHelper = new UploadHelper(getActivity());
        assistensiActivity.imgMenu.setVisibility(View.VISIBLE);
        requestArrayList = new ArrayList<>();

        segmentedButtonGroup = view.findViewById(R.id.segmentedButtonGroup);
        lnoData = view.findViewById(R.id.lnoData);
        etSearch = view.findViewById(R.id.etSearch);
        rv = view.findViewById(R.id.rv);
        refreshRv = view.findViewById(R.id.refreshRv);

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
        if(preferences.getBoolean(HarvestApp.CONNECTION_PREF,false)){
            segmentedButtonGroup.setPosition(ButtonGroup_Public);
        }else{
            segmentedButtonGroup.setPosition(ButtonGroup_Lokal);
        }

        rv.setLayoutManager(new LinearLayoutManager(getActivity()));

        refreshRv.setEnableAutoLoadMore(true);
        refreshRv.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                refresh();
            }
        });
        refreshRv.setRefreshHeader(new RefreshHeaderRecycleView(getActivity()));
        refreshRv.setHeaderHeight(60);
        refreshRv.autoRefresh();
    }

    private void refresh() {
        lnoData.setVisibility(View.GONE);
        refreshRv.getLayout().postDelayed(() -> {
            new LongOperation().execute(String.valueOf(LongOperation_SetupDataUpload));
        }, 2000);
    }


    private void setupData(){
        requestArrayList = new ArrayList<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_REQUEST_ASSISTENSI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            AssistensiRequest assistensiRequest = gson.fromJson(dataNitrit.getValueDataNitrit(),AssistensiRequest.class);
            if(assistensiRequest.getStatus() >= 0) {
                requestArrayList.add(assistensiRequest);
            }
        }
        db.close();
    }

    private void setupAdapter(){
        assistensiAdapter = new AssistensiAdapter(getActivity(),requestArrayList);
        rv.setAdapter(assistensiAdapter);
        assistensiAdapter.notifyDataSetChanged();
        if(requestArrayList.size() > 0){
            etSearch.setVisibility(View.VISIBLE);
            lnoData.setVisibility(View.GONE);
        }else{
            etSearch.setVisibility(View.GONE);
            lnoData.setVisibility(View.VISIBLE);
        }
    }

    public void setSelectedAssistensiRequest(AssistensiRequest assistensiRequest) {
        if (assistensiActivity != null) {
            assistensiActivity.selectedAssistensiRequest = assistensiRequest;
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_container, AssistensiInputFragment.getInstance())
                    .commit();
        }
    }

    public void onSuccesRespon(ServiceResponse serviceResponse){
        try {
            switch (serviceResponse.getTag()){
                case InsertAssistanceRequest:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),assistensiActivity.idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != assistensiActivity.idxUpload) {
                        assistensiActivity.idxUpload ++;
                        uploadHelper.uploadData(this,Tag.InsertAssistanceRequest,objSetUp.getJSONArray("data"), assistensiActivity.idxUpload);
                    }else{
                        this.serviceResponse = serviceResponse;
                        new LongOperation().execute(String.valueOf(LongOperation_SetUpDataDownload));
                    }
                    break;
                case GetLatestRequestAssistensiByRequestUserId:
                    this.serviceResponse = serviceResponse;
                    new LongOperation().execute(String.valueOf(LongOperation_SetUpDataDownload));
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void onBadRespon(){
        WidgetHelper.showSnackBar(assistensiActivity.myCoordinatorLayout,"Gagal Sync Data Assistensi");
        new LongOperation().execute(String.valueOf(LongOperation_SetUpData));
    }

    public class LongOperation extends AsyncTask<String, String, String> {
        boolean error = false;

        @Override
        protected String doInBackground(String... strings) {
            try {
                switch (Integer.parseInt(strings[0])) {
                    case LongOperation_SetupDataUpload:
                        objSetUp = setUpDataSyncHelper.UploadAssistanceRequest(false, this);
                        if (objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadData(this, Tag.InsertAssistanceRequest, objSetUp.getJSONArray("data"), 0);
                            } else {
                                assistensiActivity.connectionPresenter.GetLatestRequestAssistensiByRequestUserId();
                            }
                        } else {
                            assistensiActivity.connectionPresenter.GetLatestRequestAssistensiByRequestUserId();
                        }
                        break;
                    case LongOperation_SetUpDataDownload:
                        if(setUpDataSyncHelper.downLoadAssistensiRequest(serviceResponse,this)){
                            if(setUpDataSyncHelper.UpdateAssistensiRequestCounterByUserID(serviceResponse)){
                                setupData();
                            }else{
                                error = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_SetUpData:
                        setupData();
                        break;
                }
                return strings[0];
            } catch (Exception e) {
                return "Exception";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(error){
                WidgetHelper.showSnackBar(assistensiActivity.myCoordinatorLayout,"Error Sync");
                refreshRv.finishRefresh();
            }else if(result.equals("Exception")){
                WidgetHelper.showSnackBar(assistensiActivity.myCoordinatorLayout,"Gagal Prepare Data Long Operation");
                refreshRv.finishRefresh();
            }else {
                switch (Integer.parseInt(result)) {
                    case LongOperation_SetUpDataDownload:
                        setupAdapter();
                        refreshRv.finishRefresh();
                        Toast.makeText(HarvestApp.getContext(),"Sync Berhasil",Toast.LENGTH_SHORT).show();
                        break;
                    case LongOperation_SetUpData:
                        setupAdapter();
                        refreshRv.finishRefresh();
                        break;
                }
            }
        }
    }
}
