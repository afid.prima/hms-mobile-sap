package id.co.ams_plantation.harvestsap.TableView.holder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewRowHeader;

public class AngkutRowHeaderCheckBoxViewHolder extends AbstractViewHolder {

    private final CheckBox row_header_textview;
    private TableViewCell cell;

    public AngkutRowHeaderCheckBoxViewHolder(View layout) {
        super(layout);
        row_header_textview = (CheckBox) itemView.findViewById(R.id.row_header_textview);
    }

    public void setCell(TableViewRowHeader cell) {
        String [] id = cell.getId().split("-");
        this.cell = cell;
        row_header_textview.setChecked(cell.getData().equals("1"));

        row_header_textview.getLayoutParams().width = LinearLayout.LayoutParams.WRAP_CONTENT;
        row_header_textview.requestLayout();
    }

    public String getCellId(){
        return this.cell.getId();
    }

    public boolean isChecked (){
        return  row_header_textview.isChecked();
    }

    public void chkedChange(boolean isChked){
        row_header_textview.setChecked(isChked);
    }
}
