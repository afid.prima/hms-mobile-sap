package id.co.ams_plantation.harvestsap.Fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.labo.kaji.fragmentanimations.MoveAnimation;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.SelectEstateAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectUserAdapter;
import id.co.ams_plantation.harvestsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestsap.model.AssistensiRequest;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.Estate;
import id.co.ams_plantation.harvestsap.model.EstateAndAssistance;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.ui.AssistensiActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.SetUpDataSyncHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import ng.max.slideview.SlideView;
import ru.slybeaver.slycalendarview.SlyCalendarDialog;

public class AssistensiInputFragment extends Fragment {

    AssistensiActivity assistensiActivity;
    TextView tvEstRequest;
    CompleteTextViewHelper etEstReciveEstate;
    CompleteTextViewHelper etAssAfdResive;
    AppCompatEditText etPemanen;
    AppCompatEditText etVehicle;
    AppCompatEditText etPeriode;
    RelativeLayout synReceive;
    LinearLayout lsave;
    LinearLayout lldeleted;
    LinearLayout lcancel;
    AlertDialog progressDialog;

    Long startDate;
    Long endDate;
    Estate estateRequest;
    Estate estateRecive;
    User assistenRecive;
    User createBy;
    AssistensiRequest selectedAssistensiRequest;
    SetUpDataSyncHelper setUpDataSyncHelper;

    SelectEstateAdapter adapterEstate;
    SelectUserAdapter adapterAssAfd;
    ArrayList<Estate> alEstate;
    ArrayList<User> alAssisten;

    SimpleDateFormat formatDate = new SimpleDateFormat("dd MMMM yyyy");
    SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat day = new SimpleDateFormat("dd");

    private static final String TAG = AssistensiInputFragment.class.getSimpleName();

    final int YesNo_Hapus = 0;
    final int YesNo_Simpan = 1;

    public final int LongOperation_HitApiEstateAndAssistensi = 0;
    public final int LongOperation_UpdateTableEstateAndAssistensi = 1;
    public final int LongOperation_SetupAdapter = 2;


    public static AssistensiInputFragment getInstance() {
        AssistensiInputFragment inputFragment = new AssistensiInputFragment();
        return inputFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return MoveAnimation.create(MoveAnimation.UP,enter,600);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.assistensi_input_layout, null, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        assistensiActivity = (AssistensiActivity) getActivity();
        setUpDataSyncHelper = new SetUpDataSyncHelper(getActivity());
        assistensiActivity.imgMenu.setVisibility(View.GONE);
        selectedAssistensiRequest = assistensiActivity.selectedAssistensiRequest;
        assistensiActivity.selectedAssistensiRequest = null;

        tvEstRequest = view.findViewById(R.id.tvEstRequest);
        etEstReciveEstate = view.findViewById(R.id.etEstReciveEstate);
        etAssAfdResive = view.findViewById(R.id.etAssAfdResive);
        etPemanen = view.findViewById(R.id.etPemanen);
        etVehicle = view.findViewById(R.id.etVehicle);
        etPeriode = view.findViewById(R.id.etPeriode);
        synReceive = view.findViewById(R.id.synReceive);
        lsave = view.findViewById(R.id.lsave);
        lldeleted = view.findViewById(R.id.lldeleted);
        lcancel = view.findViewById(R.id.lcancel);

        lldeleted.setVisibility(View.GONE);

        alEstate = new ArrayList<>();
        alAssisten = new ArrayList<>();
        setupDataAdapterEstate();

        estateRequest = GlobalHelper.getEstate();
        tvEstRequest.setText(estateRequest.getEstCode() + " - " +estateRequest.getEstName());

        setupAdapterEstate();

        if(selectedAssistensiRequest != null) {
            setValueAssistensi();
        }

        etEstReciveEstate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etEstReciveEstate.showDropDown();
            }
        });

        etEstReciveEstate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etEstReciveEstate.setText("");
                etEstReciveEstate.showDropDown();
            }
        });

        etEstReciveEstate.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                estateRecive = ((SelectEstateAdapter) parent.getAdapter()).getItemAt(position);
                etEstReciveEstate.setText(estateRecive.getEstCode()+ " - " +estateRecive.getEstName());
                setupAdapterAssisten();
                etAssAfdResive.requestFocus();
            }
        });

        etAssAfdResive.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etAssAfdResive.showDropDown();
            }
        });

        etAssAfdResive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etAssAfdResive.setText("");
                etAssAfdResive.showDropDown();
            }
        });

        etAssAfdResive.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapterAssAfd = (SelectUserAdapter) parent.getAdapter();
                assistenRecive = adapterAssAfd.getItemAt(position);
                etAssAfdResive.setText(assistenRecive.getUserFullName());

                etPemanen.requestFocus();
            }
        });

        etPeriode.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });

        synReceive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LongOperation(null).execute(String.valueOf(LongOperation_HitApiEstateAndAssistensi));
            }
        });

        lsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(assistenRecive == null){
                    Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Assisten Afdeling",Toast.LENGTH_SHORT).show();
                    etAssAfdResive.requestFocus();
                    return;
                }
                if(estateRecive == null){
                    Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Kebun",Toast.LENGTH_SHORT).show();
                    etEstReciveEstate.requestFocus();
                    return;
                }
                if(startDate == null){
                    Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Waktu Periode",Toast.LENGTH_SHORT).show();
                    openDateRangePicker();
                    return;
                }

                if(etPemanen.getText().toString().isEmpty() && etVehicle.getText().toString().isEmpty()){
                    Toast.makeText(HarvestApp.getContext(),"Mohon Input Pemanen Atau Kendaraan",Toast.LENGTH_SHORT).show();
                    if(etPemanen.getText().toString().isEmpty()){
                        etPemanen.requestFocus();
                    }

                    if(etVehicle.getText().toString().isEmpty()){
                        etVehicle.requestFocus();
                    }
                    return;
                }
                showYesNo("Apakah Anda Yakin Simpan Data Ini",YesNo_Simpan);
            }
        });

        lldeleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showYesNo("Apakah Anda Yakin Hapus Data Ini",YesNo_Hapus);
            }
        });

        lcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assistensiActivity.backProses();
            }
        });
    }

    public void setValueAssistensi(){
        startDate = selectedAssistensiRequest.getStartRequest();
        endDate = selectedAssistensiRequest.getEndRequest();
        estateRecive = selectedAssistensiRequest.getEstateReceive();
        estateRequest = selectedAssistensiRequest.getEstateRequest();
        assistenRecive = selectedAssistensiRequest.getAssistenRecive();

        Long timeDiff = endDate - startDate;

        tvEstRequest.setText(estateRequest.getEstCode() + " - " +estateRequest.getEstName());
        etEstReciveEstate.setText(estateRecive.getEstCode() + " - " +estateRecive.getEstName());
        etAssAfdResive.setText(assistenRecive.getUserFullName());
        etPemanen.setText(String.valueOf(selectedAssistensiRequest.getRequestPemanen()));
        etVehicle.setText(String.valueOf(selectedAssistensiRequest.getRequestVehicle()));
        etPeriode.setText(formatDate.format(startDate)
                + " - " + formatDate.format(endDate)
                +" ("+ day.format(timeDiff) + " Hari )" );

        if(GlobalHelper.getUser().getUserID().equalsIgnoreCase(GlobalHelper.getUser().getUserID())){
            if(selectedAssistensiRequest.getStatus() == AssistensiRequest.SAVE){
                lldeleted.setVisibility(View.VISIBLE);
            }else{
                lsave.setVisibility(View.GONE);
            }
        }else{
            lsave.setVisibility(View.GONE);
        }
    }

    private void openDateRangePicker(){
        Date today = new Date();
        Date today8 = today;

        if(startDate != null ){
            today = new Date(startDate);
        }

        if(endDate != null){
            today8 = new Date(endDate);
        }

        SlyCalendarDialog dialog = new SlyCalendarDialog()
            .setSingle(false)
            .setHeaderColor(getResources().getColor(R.color.Green))
            .setSelectedColor(getResources().getColor(R.color.md_orange_400))
            .setStartDate(today)
            .setEndDate(today8)
            .setCallback(new SlyCalendarDialog.Callback() {
                @Override
                public void onCancelled() {

                }

                @Override
                public void onDataSelected(Calendar firstDate, Calendar secondDate, int hours, int minutes) {
                    int tgl = Integer.parseInt(date.format(System.currentTimeMillis()));
                    int end = Integer.parseInt(date.format(System.currentTimeMillis()));
                    if(secondDate != null){
                        end =  Integer.parseInt(date.format(secondDate.getTimeInMillis()));
                    }

                    if(end < tgl){
                        Toast.makeText(HarvestApp.getContext(),"Tanggal Terakhir Harus Lebih Dari Tanggal Hari Ini ",Toast.LENGTH_SHORT).show();
                        openDateRangePicker();
                        return;
                    }


                    if(firstDate != null){
                        startDate = firstDate.getTimeInMillis();
                    }else{
                        startDate = System.currentTimeMillis();
                    }

                    if(secondDate != null) {
                        endDate = secondDate.getTimeInMillis();
                    }else{
                        endDate = System.currentTimeMillis();
                    }
                    Long timeDiff = endDate - startDate;

                    etPeriode.setText(formatDate.format(startDate)
                            + " - " + formatDate.format(endDate)
                            +" ("+ day.format(timeDiff) + " Hari )" );
                }
            });
        dialog.show(getActivity().getSupportFragmentManager(), "TAG_SLYCALENDAR");
    }

    private void setupDataAdapterEstate(){
        alEstate = new ArrayList<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ESTATE_AND_ASSISTENSI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            EstateAndAssistance estateAndAssistance = gson.fromJson(dataNitrit.getValueDataNitrit(), EstateAndAssistance.class);
            if(!GlobalHelper.getEstate().getEstCode().equals(estateAndAssistance.getEstCode())) {
                if(GlobalHelper.getEstate().getCompanyShortName().equals("SAM") && estateAndAssistance.getEstCode().equals("KPE")){
                    alEstate.add(new Estate(estateAndAssistance.getEstCode(), estateAndAssistance.getEstName(), estateAndAssistance.getCompanyShortName()));
                }else if(GlobalHelper.getEstate().getCompanyShortName().equals("PANP-S") && estateAndAssistance.getEstCode().equals("KSN")){
                    alEstate.add(new Estate(estateAndAssistance.getEstCode(), estateAndAssistance.getEstName(), estateAndAssistance.getCompanyShortName()));
                }else if (GlobalHelper.getEstate().getCompanyShortName().equals(estateAndAssistance.getCompanyShortName())) {
                    alEstate.add(new Estate(estateAndAssistance.getEstCode(), estateAndAssistance.getEstName(), estateAndAssistance.getCompanyShortName()));
                }
            }
        }
        db.close();
    }

    private void setupAdapterAssisten(){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ESTATE_AND_ASSISTENSI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", estateRecive.getEstCode()));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                EstateAndAssistance estateAndAssistance = gson.fromJson(dataNitrit.getValueDataNitrit(),EstateAndAssistance.class);
                alAssisten = estateAndAssistance.getAssistenAfdeling();
                break;
            }
        }
        db.close();

        if(alAssisten.size() == 0){
            Toast.makeText(HarvestApp.getContext(),"Tidak Ada Assisten Di Kebun "+ estateRecive.getEstName(),Toast.LENGTH_SHORT).show();
        }

        adapterAssAfd = new SelectUserAdapter(getActivity(),R.layout.row_object, alAssisten);
        assistenRecive = null;
        etAssAfdResive.setText("");
        etAssAfdResive.setAdapter(adapterAssAfd);
        etAssAfdResive.setThreshold(1);
        adapterAssAfd.notifyDataSetChanged();
    }

    private void setupAdapterEstate(){
        adapterEstate = new SelectEstateAdapter(getActivity(), R.layout.row_setting,alEstate);
        etEstReciveEstate.setAdapter(adapterEstate);
        etEstReciveEstate.setThreshold(1);
        adapterEstate.notifyDataSetChanged();

    }

    private void showYesNo(String text,int stat){
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.yes_no, null);
        TextView tvDialogKet = (TextView) dialogView.findViewById(R.id.ket1);
        SlideView btnCancelDialog = (SlideView) dialogView.findViewById(R.id.btnCancel);
        SlideView btnOkDialog = (SlideView) dialogView.findViewById(R.id.btnOK);

        final AlertDialog dialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle)
                .setView(dialogView)
                .setCancelable(false)
                .create();

        final ForegroundColorSpan fcs = new ForegroundColorSpan(getActivity().getResources().getColor(R.color.Red));
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

        tvDialogKet.setText(text);
        btnOkDialog.setText("Yes");
        btnCancelDialog.setText("No");

        btnCancelDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                dialog.dismiss();
            }
        });
        btnOkDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                switch (stat){
                    case YesNo_Simpan:{
                        saveRequestAsistensi();
                        break;
                    }
                    case YesNo_Hapus:{
                        deletedRequestAssistensi();
                        break;
                    }
                }

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void saveRequestAsistensi(){
        String idAssistensi = null;

        if(selectedAssistensiRequest != null){
            idAssistensi = selectedAssistensiRequest.getIdAssistensi();
        }

        if(idAssistensi == null){
            SimpleDateFormat year = new SimpleDateFormat("yyyy");
            idAssistensi = "R" + String.format("%03d", GlobalHelper.getCountNumberYear(GlobalHelper.LIST_FOLDER_REQUEST_ASSISTENSI))
                    + year.format(System.currentTimeMillis())
                    + GlobalHelper.getUser().getUserID();
        }

        createBy = GlobalHelper.getUser();
        createBy.setEstates(null);

        //hapus
//        assistenRecive.setUserID("2247");

        AssistensiRequest assistensiRequest = new AssistensiRequest(
                idAssistensi,
                estateRequest,
                estateRecive,
                assistenRecive,
                createBy,
                startDate,
                endDate,
                System.currentTimeMillis(),
                Integer.parseInt(etPemanen.getText().toString().isEmpty() ? "0" : etPemanen.getText().toString()),
                Integer.parseInt(etVehicle.getText().toString().isEmpty() ? "0" : etVehicle.getText().toString()),
                0
        );

        Gson gson = new Gson();
        Log.d(TAG, "saveRequestAsistensi: "+gson.toJson(assistensiRequest));

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_REQUEST_ASSISTENSI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        DataNitrit dataNitrit = new DataNitrit(assistensiRequest.getIdAssistensi(), gson.toJson(assistensiRequest));
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", dataNitrit.getIdDataNitrit()));
        if (cursor.size() > 0) {
            repository.update(dataNitrit);
        } else {
            repository.insert(dataNitrit);
            GlobalHelper.setCountNumberPlusOneYear(GlobalHelper.LIST_FOLDER_REQUEST_ASSISTENSI);
        }
        db.close();

        assistensiActivity.backProses();
        Toast.makeText(HarvestApp.getContext(),"Data Berhasil Di Simpan",Toast.LENGTH_SHORT).show();
    }

    private void deletedRequestAssistensi(){
        selectedAssistensiRequest.setStatus(AssistensiRequest.REMOVE);
        Gson gson = new Gson();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_REQUEST_ASSISTENSI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        DataNitrit dataNitrit = new DataNitrit(selectedAssistensiRequest.getIdAssistensi(), gson.toJson(selectedAssistensiRequest));
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", dataNitrit.getIdDataNitrit()));
        if (cursor.size() > 0) {
            repository.remove(dataNitrit);
            Toast.makeText(HarvestApp.getContext(),"Data Berhasil Di Hapus",Toast.LENGTH_SHORT).show();
        }
        db.close();

        assistensiActivity.backProses();
    }

    public void onBadRespon(){
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        WidgetHelper.showSnackBar(assistensiActivity.myCoordinatorLayout,"Data Tidak Berhasil Di Update");
    }

    public class LongOperation extends AsyncTask<String, String, String> {
        boolean error = false;
        ServiceResponse serviceResponse;

        public LongOperation(ServiceResponse serviceResponse) {
            this.serviceResponse = serviceResponse;
        }

        @Override
        protected void onPreExecute() {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    progressDialog = WidgetHelper.showWaitingDialog(getActivity(), getActivity().getResources().getString(R.string.wait));
                }
            });
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                switch (Integer.parseInt(strings[0])) {
                    case LongOperation_HitApiEstateAndAssistensi:
                        assistensiActivity.connectionPresenter.GetEstateAndAssistanceList();
                        break;
                    case LongOperation_UpdateTableEstateAndAssistensi:
                        error = !setUpDataSyncHelper.downLoadEstateAndAssistensi(serviceResponse,this);
                        break;
                    case LongOperation_SetupAdapter:
                        setupDataAdapterEstate();
                        break;
                }
                return strings[0];
            } catch (Exception e) {
                return "Exception";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(error){
                WidgetHelper.showSnackBar(assistensiActivity.myCoordinatorLayout,"Error Data Api");
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }else if(result.equals("Exception")){
                WidgetHelper.showSnackBar(assistensiActivity.myCoordinatorLayout,"Gagal Prepare Data Long Operation");
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }else {
                switch (Integer.parseInt(result)) {
                    case LongOperation_UpdateTableEstateAndAssistensi:
                        new LongOperation(null).execute(String.valueOf(LongOperation_SetupAdapter));
                        break;
                    case LongOperation_SetupAdapter:
                        setupAdapterEstate();
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        Toast.makeText(HarvestApp.getContext(),"Data Estate Dan Assisten Berhasil Di Update",Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }

        public void publishProgress(JSONObject objProgres){
            onProgressUpdate(String.valueOf(objProgres));
        }
    }
}
