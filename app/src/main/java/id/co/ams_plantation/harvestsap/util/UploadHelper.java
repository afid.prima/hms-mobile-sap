package id.co.ams_plantation.harvestsap.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.ArraySet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestsap.Fragment.SettingFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.SelectGangSupervisionAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectSupervisionAdapter;
import id.co.ams_plantation.harvestsap.connection.ServiceRequest;
import id.co.ams_plantation.harvestsap.connection.Tag;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.LastSyncTime;
import id.co.ams_plantation.harvestsap.model.QcMutuBuah;
import id.co.ams_plantation.harvestsap.model.SuperVision;
import id.co.ams_plantation.harvestsap.model.SupervisionList;
import id.co.ams_plantation.harvestsap.model.SyncHistroy;
import id.co.ams_plantation.harvestsap.model.SyncHistroyItem;
import id.co.ams_plantation.harvestsap.model.SyncTime;
import id.co.ams_plantation.harvestsap.model.SyncTransaksi;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.ui.ActiveActivity;
import id.co.ams_plantation.harvestsap.ui.AssistensiActivity;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestsap.ui.MapActivity;
import ng.max.slideview.SlideView;


public class UploadHelper {

    Context context;
    MapActivity mapActivity;
    SettingFragment settingFragment;
    MainMenuActivity mainMenuActivity;
    AssistensiActivity assistensiActivity;
    ActiveActivity activeActivity;
    AlertDialog alertDialog;
    AlertDialog alertDialog2;
    AlertDialog alertDialogPilihJaringan;
    Snackbar snackbar;
    SelectGangSupervisionAdapter adapterGangSupervision;
    SelectSupervisionAdapter adapterSupervisionS1;
    SelectSupervisionAdapter adapterSupervisionS2;
    SelectSupervisionAdapter adapterSupervisionS3;
    SelectSupervisionAdapter adapterSupervisionS4;
    SuperVision selectSuperVision;
    SyncHistroy syncHistroy;

    Long uploadDataTime;
    Long syncDataTransaksi;
    Long syncDataMaster;
    LastSyncTime lastSyncTime;

    public UploadHelper(Context context) {
        this.context = context;
    }

    public void SelectUploadSync(){
        if(context instanceof MapActivity){
            mapActivity = ((MapActivity) context);
        }else if (context instanceof MainMenuActivity){
            mainMenuActivity = ((MainMenuActivity) context);
            Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
            if(fragment instanceof SettingFragment) {
                settingFragment = ((SettingFragment) fragment);
            }
        }

        View baseView = LayoutInflater.from(context).inflate(R.layout.upload_layout,null);
        TextView tvLastSinkron = (TextView) baseView.findViewById(R.id.tv_last_sinkron);
        TextView tvComment = (TextView) baseView.findViewById(R.id.tvComment);
        TextView tvUpload = (TextView) baseView.findViewById(R.id.tv_upload);
        LinearLayout btnUploadSinkron = (LinearLayout) baseView.findViewById(R.id.btn_upload_sinkron);
        LinearLayout btnSinkronOnly = (LinearLayout) baseView.findViewById(R.id.btn_sinkron_only);
        ImageView ivUploadSinkron = (ImageView) baseView.findViewById(R.id.iv_upload_sinkron);
        ImageView ivSinkronOnly = (ImageView) baseView.findViewById(R.id.iv_sinkron_only);
//        final RadioButton rdLokal = (RadioButton) baseView.findViewById(R.id.rd_use_lokal);
//        RadioButton rdPublik = (RadioButton) baseView.findViewById(R.id.rd_use_public);

        ivUploadSinkron.setImageDrawable(new IconicsDrawable(context)
                .icon(MaterialDesignIconic.Icon.gmi_upload)
                .colorRes(R.color.Gray)
        );
        ivSinkronOnly.setImageDrawable(new IconicsDrawable(context)
                .icon(MaterialDesignIconic.Icon.gmi_refresh_sync)
                .colorRes(R.color.Gray)
        );

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECT_BUFFER_SERVER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        Boolean connectBufferServer;
        if(NetworkHelper.getIPAddress(true).startsWith("192.168.0")){
            editor.putBoolean(HarvestApp.CONNECT_BUFFER_SERVER,true);
            connectBufferServer = true;
        }else{
            editor.putBoolean(HarvestApp.CONNECT_BUFFER_SERVER,false);
            connectBufferServer =false;
        }
        editor.apply();

        String lastSync = null;
        if(mapActivity != null) {
            if(mapActivity.setUpDataSyncHelper != null) {
                lastSync = mapActivity.setUpDataSyncHelper.getLastSync();
            }
        } else if (settingFragment != null) {
            if (settingFragment.setUpDataSyncHelper != null) {
                lastSync = settingFragment.setUpDataSyncHelper.getLastSync();
            }
        }
        if(lastSync != null){
            long timeLastSinkron = System.currentTimeMillis();
            try {
                JSONObject object = new JSONObject(lastSync);
                timeLastSinkron = object.getLong("Time");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd-MMMM-yyyy HH:mm");
            Date dateLastSinkron = new Date(timeLastSinkron);
            Calendar calendarNow = Calendar.getInstance();
            calendarNow.setTime(new Date(System.currentTimeMillis()-dateLastSinkron.getTime()));
            String tglLastSync = sdf.format(dateLastSinkron);
            SpannableString spanLastSinkron = new SpannableString("Terakhir sinkronisasi : "+tglLastSync);
            spanLastSinkron.setSpan(new ForegroundColorSpan(Color.BLUE),
                    spanLastSinkron.toString().indexOf(tglLastSync),
                    spanLastSinkron.toString().indexOf(tglLastSync)+tglLastSync.length(),
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            tvComment.setVisibility(View.GONE);
            btnUploadSinkron.setEnabled(true);
            btnUploadSinkron.setVisibility(View.VISIBLE);
            tvLastSinkron.setVisibility(View.VISIBLE);
            tvLastSinkron.setText(spanLastSinkron);
        }else{
//            tvLastSinkron.setText("Terakhir sinkronisasi : belum sinkronisasi");
//            tvLastSinkron.setVisibility(View.GONE);
//            tvComment.setVisibility(View.VISIBLE);
        }

        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, Context.MODE_PRIVATE);
        String sync_time = prefer.getString(HarvestApp.SYNC_TIME,null);
        Gson gson = new Gson();
        lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
        syncHistroy = SyncHistoryHelper.getSyncHistory();
        if(syncHistroy != null){
            if(syncHistroy.getAction().equalsIgnoreCase(SyncHistroy.Action_Sync)){
                SyncHistoryHelper.insertHistorySync(SyncHistroy.Sync_Berhasil);
                syncHistroy = null;
//                if(lastSyncTime == null){
//                    btnUploadSinkron.setVisibility(View.GONE);
//                }if(lastSyncTime.getSyncTimeHashMap() == null){
//                    btnUploadSinkron.setVisibility(View.GONE);
//                }else if(lastSyncTime.getSyncTimeHashMap().get(SyncTime.SYNC_DATA_MASTER).isSelected()) {
//                    btnUploadSinkron.setVisibility(View.GONE);
//                }
            }else if (syncHistroy.getAction().equalsIgnoreCase(SyncHistroy.Action_Upload)) {
                boolean syncOnlyHide = true;
                lastSyncTime = gson.fromJson(sync_time, LastSyncTime.class);
                if(lastSyncTime != null) {
                    if (lastSyncTime.getSyncTimeHashMap().size() > 0) {
                        for (SyncTime syncTime : lastSyncTime.getSyncTimeHashMap().values()) {
                            if (syncTime.getNamaListCekBox().equalsIgnoreCase(SyncTime.UPLOAD_TRANSAKSI)) {
                                syncOnlyHide = !syncTime.isFinish();
                                break;
                            }
                        }
                    }
                }else{
                    lastSyncTime = new LastSyncTime();
                }
                if (syncOnlyHide) {
                    btnSinkronOnly.setVisibility(View.GONE);
                }else{
                    btnSinkronOnly.setVisibility(View.VISIBLE);
                }
            }
        }

        if(settingFragment != null){
            if(settingFragment.adaDataTransaksi) {
                btnSinkronOnly.setVisibility(View.GONE);
            }
        }

        btnUploadSinkron.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                String messagePilihJaringan = "Silahkan pilih jaringan untuk upload!";
//                ServiceRequest serviceRequest = new ServiceRequest();
//
//                View view = LayoutInflater.from(context).inflate(R.layout.public_network_sync_dialog,null);
//                TextView tvKet1PilihJaringan = (TextView) view.findViewById(R.id.ket1);
//                SlideView btnJaringanPublik= (SlideView) view.findViewById(R.id.btnJaringanPublik);
//                SlideView btnJaringanLokal = (SlideView) view.findViewById(R.id.btnJaringanKantor);
//
//                if(connectBufferServer){
//                    btnJaringanLokal.setText(context.getResources().getString(R.string.wifi_hms));
//                }else{
//                    btnJaringanLokal.setText(context.getResources().getString(R.string.wifi_kebun));
//                }
//
//                tvKet1PilihJaringan.setText(messagePilihJaringan);
//                btnJaringanLokal.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
//                    @Override
//                    public void onSlideComplete(SlideView slideView) {
//                        alertDialogPilihJaringan.dismiss();
//
//                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
//                        SharedPreferences.Editor editor = preferences.edit();
//                        editor.putBoolean(HarvestApp.CONNECTION_PREF,false);
//                        editor.apply();
//                        String message = "Apakah anda yakin untuk melakukan upload ? Data terbaru anda akan disimpan ke server!";
//                        String redString = "Data terbaru anda akan disimpan ke server!";
//                        SpannableString spannableMessage = new SpannableString(message);
//                        spannableMessage.setSpan(new ForegroundColorSpan(Color.RED),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_INCLUSIVE_INCLUSIVE);
//                        spannableMessage.setSpan(new StyleSpan(Typeface.BOLD),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_INCLUSIVE_INCLUSIVE);
//
//                        View view = LayoutInflater.from(context).inflate(R.layout.sync_dialog,null);
//                        TextView tvket1 = (TextView) view.findViewById(R.id.ket1);
//                        SlideView btnCancel= (SlideView) view.findViewById(R.id.btnCancel);
//                        SlideView btnOk = (SlideView) view.findViewById(R.id.btnOK);
//
//                        tvket1.setText(spannableMessage);
//                        btnCancel.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
//                            @Override
//                            public void onSlideComplete(SlideView slideView) {
//                                alertDialog2.dismiss();
//                            }
//                        });
//                        btnOk.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
//                            @Override
//                            public void onSlideComplete(SlideView slideView) {
//                                alertDialog2.dismiss();
//                                alertDialog.dismiss();
//                                if(mapActivity != null){
//                                    if(syncHistroy == null) {
//                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_MapActivity, SyncHistroy.Action_Upload);
//                                    }
//                                    ceklistUploadnDownload(true);
//                                }else if(settingFragment!= null){
//                                    if(syncHistroy == null) {
//                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Setting, SyncHistroy.Action_Upload);
//                                    }
////                                    if(settingFragment.segmentedButtonGroup != null) {
////                                        settingFragment.segmentedButtonGroup.setPosition(SettingFragment.ButtonGroup_Lokal);
////                                    }
//                                    ceklistUploadnDownload(true);
//                                }
//                            }
//                        });
//                        alertDialog2 = WidgetHelper.showFormDialog(alertDialog2,view,context);
//                    }
//                });

//                btnJaringanPublik.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
//                    @Override
//                    public void onSlideComplete(SlideView slideView) {
//                        alertDialogPilihJaringan.dismiss();
//
                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF,true);
                        editor.apply();

                        String message = "Apakah anda yakin untuk melakukan upload ? Data terbaru anda akan disimpan ke server!";
                        String redString = "Data terbaru anda akan disimpan ke server!";
                        SpannableString spannableMessage = new SpannableString(message);
                        spannableMessage.setSpan(new ForegroundColorSpan(Color.RED),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        spannableMessage.setSpan(new StyleSpan(Typeface.BOLD),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_INCLUSIVE_INCLUSIVE);

                        View view = LayoutInflater.from(context).inflate(R.layout.sync_dialog,null);
                        TextView tvket1 = (TextView) view.findViewById(R.id.ket1);
                        SlideView btnCancel= (SlideView) view.findViewById(R.id.btnCancel);
                        SlideView btnOk = (SlideView) view.findViewById(R.id.btnOK);

                        tvket1.setText(spannableMessage);
                        btnCancel.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                            @Override
                            public void onSlideComplete(SlideView slideView) {
                                alertDialog2.dismiss();
                            }
                        });
                        btnOk.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                            @Override
                            public void onSlideComplete(SlideView slideView) {
                                alertDialog2.dismiss();
                                alertDialog.dismiss();

                                if(mapActivity != null){
                                    if(syncHistroy == null) {
                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_MapActivity, SyncHistroy.Action_Upload);
                                    }
                                    ceklistUploadnDownload(true);
                                }else if(settingFragment!= null){
                                    if(syncHistroy == null) {
                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Setting, SyncHistroy.Action_Upload);
                                    }
//                                    if(settingFragment.segmentedButtonGroup != null) {
//                                        settingFragment.segmentedButtonGroup.setPosition(SettingFragment.ButtonGroup_Public);
//                                    }
                                    ceklistUploadnDownload(true);
                                }
                            }
                        });
                        alertDialog2 = WidgetHelper.showFormDialog(alertDialog2,view,context);
//                    }
//                });
//                alertDialogPilihJaringan = WidgetHelper.showFormDialog(alertDialogPilihJaringan,view,context);
//                alertDialogPilihJaringan.setCancelable(false);



            }
        });
        btnSinkronOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String messagePilihJaringan = "Silahkan pilih jaringan untuk sinkronisasi!";
//                ServiceRequest serviceRequest = new ServiceRequest();
//
//                View view = LayoutInflater.from(context).inflate(R.layout.public_network_sync_dialog,null);
//                TextView tvKet1PilihJaringan = (TextView) view.findViewById(R.id.ket1);
//                SlideView btnJaringanPublik= (SlideView) view.findViewById(R.id.btnJaringanPublik);
//                SlideView btnJaringanLokal = (SlideView) view.findViewById(R.id.btnJaringanKantor);
//
//                if(connectBufferServer){
//                    btnJaringanLokal.setText(context.getResources().getString(R.string.wifi_hms));
//                }else{
//                    btnJaringanLokal.setText(context.getResources().getString(R.string.wifi_kebun));
//                }
//                tvKet1PilihJaringan.setText(messagePilihJaringan);
//
//                btnJaringanLokal.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
//                    @Override
//                    public void onSlideComplete(SlideView slideView) {
//                        alertDialogPilihJaringan.dismiss();
//
//                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
//                        SharedPreferences.Editor editor = preferences.edit();
//                        editor.putBoolean(HarvestApp.CONNECTION_PREF,false);
//                        editor.apply();
//
//                        String message = "Apakah anda yakin untuk melakukan sinkronisasi ? Data hasil input yang belum diupload akan terhapus!";
//                        String redString = "Data hasil input yang belum diupload akan terhapus!";
//                        SpannableString spannableMessage = new SpannableString(message);
//                        spannableMessage.setSpan(new ForegroundColorSpan(Color.RED),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//                        spannableMessage.setSpan(new StyleSpan(Typeface.BOLD),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//                        View view = LayoutInflater.from(context).inflate(R.layout.sync_dialog,null);
//                        TextView tvket1 = (TextView) view.findViewById(R.id.ket1);
//                        SlideView btnCancel= (SlideView) view.findViewById(R.id.btnCancel);
//                        SlideView btnOk = (SlideView) view.findViewById(R.id.btnOK);
//
//                        tvket1.setText(spannableMessage);
//                        btnCancel.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
//                            @Override
//                            public void onSlideComplete(SlideView slideView) {
//                                alertDialog2.dismiss();
//                            }
//                        });
//                        btnOk.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
//                            @Override
//                            public void onSlideComplete(SlideView slideView) {
//                                alertDialog2.dismiss();
//                                alertDialog.dismiss();
//                                if(mapActivity != null){
//                                    if(syncHistroy == null) {
//                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_MapActivity, SyncHistroy.Action_Sync);
//                                    }
//                                    ceklistUploadnDownload(false);
//                                }else if(settingFragment!= null){
//                                    if(syncHistroy == null) {
//                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Setting, SyncHistroy.Action_Sync);
//                                    }
////                                    if(settingFragment.segmentedButtonGroup != null) {
////                                        settingFragment.segmentedButtonGroup.setPosition(SettingFragment.ButtonGroup_Lokal);
////                                    }
//                                    ceklistUploadnDownload(false);
//                                }
//                            }
//                        });
//                        alertDialog2 = WidgetHelper.showFormDialog(alertDialog2,view,context);
//                    }
//                });
//                btnJaringanPublik.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
//                    @Override
//                    public void onSlideComplete(SlideView slideView) {
//                        alertDialogPilihJaringan.dismiss();
//
                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF,true);
                        editor.apply();
                        String message = "Apakah anda yakin untuk melakukan sinkronisasi ? Data hasil input yang belum diupload akan terhapus!";
                        String redString = "Data hasil input yang belum diupload akan terhapus!";
                        SpannableString spannableMessage = new SpannableString(message);
                        spannableMessage.setSpan(new ForegroundColorSpan(Color.RED),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                        spannableMessage.setSpan(new StyleSpan(Typeface.BOLD),message.indexOf(redString),message.indexOf(redString)+redString.length(),Spanned.SPAN_INCLUSIVE_INCLUSIVE);

                        View view = LayoutInflater.from(context).inflate(R.layout.sync_dialog,null);
                        TextView tvket1 = (TextView) view.findViewById(R.id.ket1);
                        SlideView btnCancel= (SlideView) view.findViewById(R.id.btnCancel);
                        SlideView btnOk = (SlideView) view.findViewById(R.id.btnOK);

                        tvket1.setText(spannableMessage);
                        btnCancel.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                            @Override
                            public void onSlideComplete(SlideView slideView) {
                                alertDialog2.dismiss();
                            }
                        });
                        btnOk.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                            @Override
                            public void onSlideComplete(SlideView slideView) {
                                alertDialog2.dismiss();
                                alertDialog.dismiss();
                                if(mapActivity != null){
                                    if(syncHistroy == null) {
                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_MapActivity, SyncHistroy.Action_Sync);
                                    }
                                    ceklistUploadnDownload(false);
                                }else if(settingFragment!= null){
                                    if(syncHistroy == null) {
                                        syncHistroy = SyncHistoryHelper.creaetNewSyncHistory(SyncHistroy.Sync_From_Setting, SyncHistroy.Action_Sync);
                                    }
//                                    if(settingFragment.segmentedButtonGroup != null) {
//                                        settingFragment.segmentedButtonGroup.setPosition(SettingFragment.ButtonGroup_Public);
//                                    }
                                    ceklistUploadnDownload(false);
                                }
                            }
                        });
                        alertDialog2 = WidgetHelper.showFormDialog(alertDialog2,view,context);
//                    }
//                });
//                alertDialogPilihJaringan = WidgetHelper.showFormDialog(alertDialogPilihJaringan,view,context);
//                alertDialogPilihJaringan.setCancelable(false);
            }
        });

        alertDialog = WidgetHelper.showFormDialog(alertDialog,baseView,context);
    }

    public void ceklistUploadnDownload(boolean withUplaod){

        if(context instanceof MapActivity){
            mapActivity = ((MapActivity) context);
        }else if (context instanceof MainMenuActivity){
            mainMenuActivity = ((MainMenuActivity) context);
            Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
            if(fragment instanceof SettingFragment) {
                settingFragment = ((SettingFragment) fragment);
            }
        }else if (context instanceof ActiveActivity){
            activeActivity = ((ActiveActivity) context);
        }

        View baseView = LayoutInflater.from(context).inflate(R.layout.ceklist_upload_download,null);
        CardView ll_upload_data = (CardView) baseView.findViewById(R.id.ll_upload_data);
        LinearLayout llfilterTransaksi = (LinearLayout) baseView.findViewById(R.id.llfilterTransaksi);
        LinearLayout llfilterMaster = (LinearLayout) baseView.findViewById(R.id.llfilterMaster);
        TextView tv_upload_data_time = (TextView) baseView.findViewById(R.id.tv_upload_data_time);
        TextView tv_sync_data_transaksi_date_time = (TextView) baseView.findViewById(R.id.tv_sync_data_transaksi_date_time);
        TextView tv_sync_data_master_date_time = (TextView) baseView.findViewById(R.id.tv_sync_data_master_date_time);
        CheckBox cb_upload_data = (CheckBox) baseView.findViewById(R.id.cb_upload_data);
        CheckBox cb_sync_data_transaksi = (CheckBox) baseView.findViewById(R.id.cb_sync_data_transaksi);
        CheckBox cb_sync_data_master = (CheckBox) baseView.findViewById(R.id.cb_sync_data_master);
        ImageView iv_cb_upload_data = (ImageView) baseView.findViewById(R.id.iv_cb_upload_data);
        ImageView iv_cb_sync_data_transaksi = (ImageView) baseView.findViewById(R.id.iv_cb_sync_data_transaksi);
        ImageView iv_cb_sync_data_master = (ImageView) baseView.findViewById(R.id.iv_cb_sync_data_master);
        BetterSpinner bsAfdeling = (BetterSpinner) baseView.findViewById(R.id.bsAfdeling);
        BetterSpinner bsAfdelingTPH = (BetterSpinner) baseView.findViewById(R.id.bsAfdelingTPH);
        SegmentedButtonGroup segmentedInputData = (SegmentedButtonGroup) baseView.findViewById(R.id.segmentedInputData);
        Button btnOk = (Button) baseView.findViewById(R.id.btnOk);
        Button btnClose = (Button) baseView.findViewById(R.id.btnCancel);

        uploadDataTime = System.currentTimeMillis();
        syncDataTransaksi = System.currentTimeMillis();
        syncDataMaster = System.currentTimeMillis();

        lastSyncTime = null;

        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, Context.MODE_PRIVATE);
        String sync_time = prefer.getString(HarvestApp.SYNC_TIME,null);
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.AFDELING_BLOCK, Context.MODE_PRIVATE);
        String afdelingObj = preferences.getString(HarvestApp.AFDELING_BLOCK,null);
        SharedPreferences afdelingPref = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TRANSAKSI, Context.MODE_PRIVATE);
        String syncafdelingS = afdelingPref.getString(HarvestApp.SYNC_TRANSAKSI,null);
        SyncTransaksi syncTransaksi = new SyncTransaksi();
        if(syncafdelingS != null){
            Gson gson = new Gson();
            syncTransaksi = gson.fromJson(syncafdelingS,SyncTransaksi.class);
        }
        if(!withUplaod){
            ll_upload_data.setVisibility(View.GONE);
        }else{
            cb_upload_data.setChecked(true);
            if(settingFragment != null){
                String uploadText = context.getResources().getString(R.string.upload_data);
                try{
                    if(settingFragment.dynamicParameterPenghasilan.getUploadFotoTransaksi().equals("1")){
                        cb_upload_data.setText(uploadText + " Dan Foto");
                    }else{
                        cb_upload_data.setText(uploadText + " Tanpa Foto");
                    }
                }catch (Exception e){
                    cb_upload_data.setText(uploadText + " Tanpa Foto");
                }
            }
        }

        if(mainMenuActivity != null || activeActivity != null){
            int positionSelected = 0;
            List<String> lAfd = new ArrayList<>();
//            lAfd.add(FilterHelper.No_Filter_Afdeling);
            try {
                JSONObject objAfdeling = new JSONObject(afdelingObj);
                if(objAfdeling.names() != null) {
                    for (int i = 0; i < objAfdeling.names().length(); i++) {
                        lAfd.add(objAfdeling.names().get(i).toString());
                        if (syncTransaksi.getAfdeling() != null) {
                            if (syncTransaksi.getAfdeling().equalsIgnoreCase(objAfdeling.names().get(i).toString())) {
                                positionSelected = i;
                            }
                        }
                    }
                }else{
                    lAfd.add("AFD-1");
                }

                if(syncTransaksi.getUserId() != null){
                    segmentedInputData.setPosition(1);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                    android.R.layout.simple_dropdown_item_1line, lAfd);
            bsAfdeling.setAdapter(adapter);
            bsAfdeling.setText(lAfd.get(positionSelected));
            bsAfdelingTPH.setAdapter(adapter);
            bsAfdelingTPH.setText(lAfd.get(positionSelected));

            bsAfdeling.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    bsAfdelingTPH.setText(lAfd.get(position));
                }
            });

            bsAfdelingTPH.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    bsAfdeling.setText(lAfd.get(position));
                }
            });
        }else{
            SharedPreferences.Editor editorAfdelingPref = afdelingPref.edit();
            editorAfdelingPref.remove(HarvestApp.SYNC_TRANSAKSI);
            editorAfdelingPref.apply();
            bsAfdeling.setVisibility(View.GONE);
            bsAfdelingTPH.setVisibility(View.GONE);
            segmentedInputData.setVisibility(View.GONE);
            llfilterTransaksi.setVisibility(View.GONE);
            llfilterMaster.setVisibility(View.GONE);
        }

        cb_upload_data.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked && withUplaod){
                    cb_upload_data.setChecked(true);
                }
            }
        });

        cb_sync_data_master.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(sync_time == null){
                    cb_sync_data_master.setChecked(true);
                }else {
                    if (!isChecked) {
                        SyncTime syncTime = lastSyncTime.getSyncTimeHashMap().get(SyncTime.SYNC_DATA_MASTER);
                        if(!syncTime.isFinish()){
                            cb_sync_data_master.setChecked(true);
                        }
                    }
                }
            }
        });

        cb_sync_data_transaksi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(sync_time == null){
//                    cb_sync_data_transaksi.setChecked(true);
//                }else {
//                    if (!isChecked) {
                        //SyncTime syncTime = lastSyncTime.getSyncTimeHashMap().get(SyncTime.SYNC_DATA_TRANSAKSI);
//                        if(!syncTime.isFinish()){
                            cb_sync_data_transaksi.setChecked(isChecked);
//                        }
//                    }
//                }
            }
        });

        if(sync_time == null){
            tv_upload_data_time.setText("");
            tv_sync_data_transaksi_date_time.setText("");
            tv_sync_data_master_date_time.setText("");
        }else{
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd-MMMM-yyyy HH:mm");
            Gson gson = new Gson();
            lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
            if(lastSyncTime.getSyncTimeHashMap().size() > 0) {
                for (SyncTime syncTime : lastSyncTime.getSyncTimeHashMap().values()) {
                    if (syncTime.getNamaListCekBox().equalsIgnoreCase(SyncTime.UPLOAD_TRANSAKSI)) {
                        tv_upload_data_time.setText(sdf.format(syncTime.getTimeSync()));
                        cb_upload_data.setChecked(syncTime.isSelected());
                        iv_cb_upload_data.setImageDrawable(new
                                IconicsDrawable(context)
                                .icon(syncTime.isFinish() ? MaterialDesignIconic.Icon.gmi_check_circle : MaterialDesignIconic.Icon.gmi_close_circle)
                                .sizeDp(20)
                                .colorRes(syncTime.isFinish() ? R.color.Green : R.color.Red));
                        uploadDataTime = syncTime.getTimeSync();
                    } else if (syncTime.getNamaListCekBox().equalsIgnoreCase(SyncTime.SYNC_DATA_MASTER)) {
                        tv_sync_data_master_date_time.setText(sdf.format(syncTime.getTimeSync()));
                        cb_sync_data_master.setChecked(syncTime.isSelected());
                        iv_cb_sync_data_master.setImageDrawable(new
                                IconicsDrawable(context)
                                .icon(syncTime.isFinish() ? MaterialDesignIconic.Icon.gmi_check_circle : MaterialDesignIconic.Icon.gmi_close_circle)
                                .sizeDp(20)
                                .colorRes(syncTime.isFinish() ? R.color.Green : R.color.Red));
                        syncDataMaster = syncTime.getTimeSync();
                    } else if (syncTime.getNamaListCekBox().equalsIgnoreCase(SyncTime.SYNC_DATA_TRANSAKSI)) {
                        tv_sync_data_transaksi_date_time.setText(sdf.format(syncTime.getTimeSync()));
                        cb_sync_data_transaksi.setChecked(syncTime.isSelected());
                        iv_cb_sync_data_transaksi.setImageDrawable(new
                                IconicsDrawable(context)
                                .icon(syncTime.isFinish() ? MaterialDesignIconic.Icon.gmi_check_circle : MaterialDesignIconic.Icon.gmi_close_circle)
                                .sizeDp(20)
                                .colorRes(syncTime.isFinish() ? R.color.Green : R.color.Red));
                        syncDataTransaksi = syncTime.getTimeSync();
                    }
                }
            }else{
                tv_upload_data_time.setText("");
                tv_sync_data_transaksi_date_time.setText("");
                tv_sync_data_master_date_time.setText("");
            }

        }

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cb_upload_data.isChecked() && !cb_sync_data_transaksi.isChecked() && !cb_sync_data_master.isChecked()) {
                    Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Salah Satu",Toast.LENGTH_SHORT).show();
                    return;
                }

                HashMap<String,SyncTime> syncTimeHashMap = new HashMap<>();
                if (cb_upload_data.isChecked() && ll_upload_data.getVisibility() == View.VISIBLE) {
                    SyncTime syncTime = new SyncTime(SyncTime.UPLOAD_TRANSAKSI,System.currentTimeMillis(),true);
                    syncTimeHashMap.put(syncTime.getNamaListCekBox(),syncTime);
                } else {
                    if (lastSyncTime == null) {
                        SyncTime syncTime = new SyncTime(SyncTime.UPLOAD_TRANSAKSI, uploadDataTime,false,true);
                        syncTimeHashMap.put(syncTime.getNamaListCekBox(),syncTime);
                        Log.d(this.getClass() + " upload_data ",syncTime.toString());
                    } else {
                        SyncTime syncTime = lastSyncTime.getSyncTimeHashMap().get(SyncTime.UPLOAD_TRANSAKSI);
                        syncTimeHashMap.put(syncTime.getNamaListCekBox(), syncTime);
                        Log.d(this.getClass() + " upload_data ",syncTime.toString());
                    }
                }

                if (cb_sync_data_transaksi.isChecked()) {
                    SyncTime syncTime = new SyncTime(SyncTime.SYNC_DATA_TRANSAKSI,System.currentTimeMillis(),true);
                    syncTimeHashMap.put(syncTime.getNamaListCekBox(),syncTime);
                } else {
                    if (lastSyncTime == null) {
                        SyncTime syncTime = new SyncTime(SyncTime.SYNC_DATA_TRANSAKSI, syncDataTransaksi, false, true);
                        syncTimeHashMap.put(syncTime.getNamaListCekBox(), syncTime);
                        Log.d(this.getClass() + " data_transaksi ",syncTime.toString());
                    } else {
                        SyncTime syncTime = lastSyncTime.getSyncTimeHashMap().get(SyncTime.SYNC_DATA_TRANSAKSI);
                        syncTime.setSelected(false);
                        syncTimeHashMap.put(syncTime.getNamaListCekBox(), syncTime);
                        Log.d(this.getClass() + " data_transaksi ",syncTime.toString());
                    }
                }

                if (cb_sync_data_master.isChecked()) {
                    SyncTime syncTime = new SyncTime(SyncTime.SYNC_DATA_MASTER,System.currentTimeMillis(),true);
                    syncTimeHashMap.put(syncTime.getNamaListCekBox(),syncTime);
                } else {
                    if (lastSyncTime == null) {
                        SyncTime syncTime = new SyncTime(SyncTime.SYNC_DATA_MASTER,syncDataMaster,false,true);
                        syncTimeHashMap.put(syncTime.getNamaListCekBox(),syncTime);
                        Log.d(this.getClass() + " data_master ",syncTime.toString());
                    } else {
                        SyncTime syncTime = lastSyncTime.getSyncTimeHashMap().get(SyncTime.SYNC_DATA_MASTER);
                        syncTimeHashMap.put(syncTime.getNamaListCekBox(), syncTime);
                        Log.d(this.getClass() + " data_master ",syncTime.toString());
                    }
                }

                LastSyncTime lastSyncTime1 = new LastSyncTime(GlobalHelper.getEstate(),syncTimeHashMap);
                Gson gson = new Gson();
                SharedPreferences.Editor editor = prefer.edit();
                editor.putString(HarvestApp.SYNC_TIME,gson.toJson(lastSyncTime1));
                editor.apply();

                if (cb_upload_data.isChecked() && ll_upload_data.getVisibility() == View.VISIBLE) {
                    Log.d(this.getClass().toString(),"upload Dulu");
                    if(mainMenuActivity != null){
                        SharedPreferences.Editor editorAfdelingPref = afdelingPref.edit();
                        String syncTransaksi = setSyncTransaksi(bsAfdeling.getText().toString(),segmentedInputData.getPosition());
                        if(syncTransaksi == null){
                            editorAfdelingPref.remove(HarvestApp.SYNC_TRANSAKSI);
                        }else {
                            editorAfdelingPref.putString(HarvestApp.SYNC_TRANSAKSI,syncTransaksi);
                        }
                        editorAfdelingPref.apply();

//                        settingFragment.startLongOperation(LongOperation_Upload_TransaksiSPB);
                        settingFragment.startLongOperation(SettingFragment.LongOperation_SetUpSupervision);
                    }else if(mapActivity != null){
                        mapActivity.startLongOperation(MapActivity.LongOperation_Upload_TPH,null);
                    }
                }else{
                    Log.d(this.getClass().toString(),"langsung Sync");
                    if(mainMenuActivity != null){
                        SharedPreferences.Editor editorAfdelingPref = afdelingPref.edit();
                        String syncTransaksi = setSyncTransaksi(bsAfdeling.getText().toString(),segmentedInputData.getPosition());
                        if(syncTransaksi == null){
                            editorAfdelingPref.remove(HarvestApp.SYNC_TRANSAKSI);
                        }else {
                            editorAfdelingPref.putString(HarvestApp.SYNC_TRANSAKSI,syncTransaksi);
                        }
                        editorAfdelingPref.apply();

                        settingFragment.startLongOperation(SettingFragment.LongOperation_BackUp_HMS);
                    }else if(mapActivity != null){
                        mapActivity.startLongOperation(MapActivity.LongOperation_BackUp_HMS,null);
                    }else if (activeActivity != null){
                        SharedPreferences.Editor editorAfdelingPref = afdelingPref.edit();
                        String syncTransaksi = setSyncTransaksi(bsAfdeling.getText().toString(),segmentedInputData.getPosition());
                        if(syncTransaksi == null){
                            editorAfdelingPref.remove(HarvestApp.SYNC_TRANSAKSI);
                        }else {
                            editorAfdelingPref.putString(HarvestApp.SYNC_TRANSAKSI,syncTransaksi);
                        }
                        editorAfdelingPref.apply();

                        activeActivity.startLongOperation(activeActivity.LongOperation_BackUpHMS);
                    }

                }
                alertDialog.dismiss();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog = WidgetHelper.showFormDialog(alertDialog,baseView,context);
    }

    public static String setSyncTransaksi(String afdeling,int position){
        SyncTransaksi syncTransaksi = new SyncTransaksi();
        if(!afdeling.equalsIgnoreCase(FilterHelper.No_Filter_Afdeling)) {
            syncTransaksi.setAfdeling(afdeling);
        }

        if(position == 1){
            syncTransaksi.setUserId(GlobalHelper.getUser().getUserID());
        }
        Gson gson = new Gson();
        return gson.toJson(syncTransaksi);
    }

    public void uploadImages(Object longOperation,Tag tag,JSONArray jsonArray, int idx){
        JSONArray array = new JSONArray();
        try {
//            array.put(jsonArray.get(idx));
            ((BaseActivity) context).idxUpload = idx;
            String text = "Upload Images ";
            Log.d(this.getClass()+" "+ text+" "+tag,String.valueOf(idx));

            int partitionUpload = GlobalHelper.PARTITION_UPLOAD;
            switch (tag){
                case PostTransaksiAngkut:
                    partitionUpload = GlobalHelper.PARTITION_UPLOAD_ANGKUT_SPB;
                    break;
                case PostTransaksiAngkutImage:
                    partitionUpload = GlobalHelper.PARTITION_UPLOAD_ANGKUT_SPB;
                    break;
                case PostTransaksiSPB:
                    partitionUpload = GlobalHelper.PARTITION_UPLOAD_ANGKUT_SPB;
                    break;
                case PostTransaksiSPBImage:
                    partitionUpload = GlobalHelper.PARTITION_UPLOAD_ANGKUT_SPB;
                    break;
            }

            //cek index jarray
            JSONArray jArrayPreUpload = new JSONArray();
            for(int x = 0 ; x < partitionUpload;x++) {
                int index = idx + x;
                if (index < jsonArray.length()) {
                    JSONObject jObj = (JSONObject) jsonArray.get(index);
                    if (jObj.getInt("idx") == index) {
                        jArrayPreUpload.put(jObj);
                    } else {
                        for (int a = 0 ; a < jsonArray.length(); a++) {
                            JSONObject objX = (JSONObject) jsonArray.get(a);
                            if (objX.getInt("idx") == index) {
                                jArrayPreUpload.put(objX);
                                break;
                            }
                        }
                    }
                }
            }

            //set up prepare upload
            for (int a = 0 ; a < jArrayPreUpload.length(); a++){
                JSONObject objPreUpload = (JSONObject) jArrayPreUpload.get(a);

                switch (tag){
                    case PostTphImage: {

                        JSONObject objImage = new JSONObject();
                        JSONArray jaImages = new JSONArray();
                        if(objPreUpload.has("foto")) {
                            if (objPreUpload.get("foto") != null) {
                                JSONArray allfoto = (JSONArray) objPreUpload.get("foto");
                                for (int i = 0; i < allfoto.length(); i++) {
                                    JSONObject objImg = new JSONObject();
                                    File file = new File(String.valueOf(allfoto.get(i)));
                                    if (file.exists() && !file.getAbsolutePath().contains("http:")) {
                                        objImg.put("filename", file.getName());
                                        objImg.put("filesize", file.getTotalSpace());
                                        objImg.put("content", GlobalHelper.getEncoded64ImageStringFromBitmap(file.getAbsolutePath()));
                                        objImg.put("metadata", GlobalHelper.getMetaDataFromImages(file.getAbsolutePath()));
                                        jaImages.put(objImg);
                                    }
                                }
                            }
                        }
                        objImage.put("noTph", objPreUpload.getString("noTph"));
                        objImage.put("estCode", objPreUpload.getString("estCode"));
                        objImage.put("photoBy", objPreUpload.getString("updateBy"));
                        objImage.put("images", jaImages);
                        array.put(objImage);
                        break;
                    }
                    case PostLangsiranImage: {

                        JSONObject objImage = new JSONObject();
                        JSONArray jaImages = new JSONArray();
                        if(objPreUpload.has("foto")) {
                            if (objPreUpload.get("foto") != null) {
                                JSONArray allfoto = (JSONArray) objPreUpload.get("foto");
                                for (int i = 0; i < allfoto.length(); i++) {
                                    JSONObject objImg = new JSONObject();
                                    File file = new File(String.valueOf(allfoto.get(i)));
                                    if (file.exists() && !file.getAbsolutePath().contains("http:")) {
                                        objImg.put("filename", file.getName());
                                        objImg.put("filesize", file.getTotalSpace());
                                        objImg.put("content", GlobalHelper.getEncoded64ImageStringFromBitmap(file.getAbsolutePath()));
                                        objImg.put("metadata", GlobalHelper.getMetaDataFromImages(file.getAbsolutePath()));
                                        jaImages.put(objImg);
                                    }
                                }
                            }
                        }
                        objImage.put("idTujuan", objPreUpload.getString("idTujuan"));
                        objImage.put("estCode", objPreUpload.getString("estCode"));
                        objImage.put("photoBy", objPreUpload.getString("updateBy"));
                        objImage.put("images", jaImages);
                        array.put(objImage);
                        break;
                    }
                    case PostTransaksiPanenImage: {

                        Gson gson = new Gson();
                        TransaksiPanen transaksiPanen = gson.fromJson(objPreUpload.toString(), TransaksiPanen.class);

                        JSONObject objImagePanen = new JSONObject();
                        JSONArray imagePanen = new JSONArray();
                        if (transaksiPanen.getFoto() != null) {
                            for (String foto : transaksiPanen.getFoto()) {
                                JSONObject objImage = new JSONObject();
                                File file = new File(foto);
                                if (file.exists() && !file.getAbsolutePath().contains("http:")) {
                                    objImage.put("filename", file.getName());
                                    objImage.put("filesize", file.getTotalSpace());
                                    objImage.put("content", GlobalHelper.getEncoded64ImageStringFromBitmap(file.getAbsolutePath()));
                                    objImage.put("metadata", GlobalHelper.getMetaDataFromImages(file.getAbsolutePath()));
                                    imagePanen.put(objImage);
                                }

                            }
                            objImagePanen.put("idTPanen", transaksiPanen.getIdTPanen());
                            objImagePanen.put("estCode", transaksiPanen.getTph().getEstCode());
                            objImagePanen.put("photoBy", transaksiPanen.getUpdateBy());
                            objImagePanen.put("images", imagePanen);

                            array.put(objImagePanen);
                        }
                        break;
                    }
                    case PostTransaksiAngkutImage:{

                        Gson gson = new Gson();
                        TransaksiAngkut transaksiAngkut = gson.fromJson(objPreUpload.toString(), TransaksiAngkut.class);

                        JSONArray imageHTAngkut = new JSONArray();
                        JSONObject objImageTAngkut = new JSONObject();
                        JSONArray imageTAngkut = new JSONArray();
                        for (int i = 0; i < transaksiAngkut.getAngkuts().size(); i++) {
                            JSONObject objImageAngkut = new JSONObject();
                            Angkut angkut = transaksiAngkut.getAngkuts().get(i);
                            JSONArray imageAngkut = new JSONArray();
                            if (angkut.getFoto() != null) {
                                for (String foto : angkut.getFoto()) {
                                    JSONObject objImage = new JSONObject();
                                    File file = new File(foto);
                                    if (file.exists() && !file.getAbsolutePath().contains("http:")) {
                                        objImage.put("filename", file.getName());
                                        objImage.put("filesize", file.getTotalSpace());
                                        objImage.put("content", GlobalHelper.getEncoded64ImageStringFromBitmap(file.getAbsolutePath()));
                                        objImage.put("metadata", GlobalHelper.getMetaDataFromImages(file.getAbsolutePath()));
                                        imageAngkut.put(objImage);
                                    }
                                }

                                objImageAngkut.put("images", imageAngkut);
                                objImageAngkut.put("idAngkut", angkut.getIdAngkut());
                                imageTAngkut.put(objImageAngkut);
                            }
                        }

                        if (transaksiAngkut.getFoto() != null) {
                            for (String foto : transaksiAngkut.getFoto()) {
                                JSONObject objImage = new JSONObject();
                                File file = new File(foto);
                                if (file.exists() && !file.getAbsolutePath().contains("http:")) {
                                    objImage.put("filename", file.getName());
                                    objImage.put("filesize", file.getTotalSpace());
                                    objImage.put("content", GlobalHelper.getEncoded64ImageStringFromBitmap(file.getAbsolutePath()));
                                    objImage.put("metadata", GlobalHelper.getMetaDataFromImages(file.getAbsolutePath()));
                                    imageHTAngkut.put(objImage);
                                }

                            }
                        }
                        objImageTAngkut.put("idTAngkut", transaksiAngkut.getIdTAngkut());
                        objImageTAngkut.put("estCode", transaksiAngkut.getEstCode());
                        objImageTAngkut.put("photoBy", transaksiAngkut.getCreateBy());
                        objImageTAngkut.put("angkuts", imageTAngkut);
                        objImageTAngkut.put("images", imageHTAngkut);
                        array.put(objImageTAngkut);
                        break;
                    }
                    case PostTransaksiSPBImage: {

                        Gson gson = new Gson();
                        TransaksiSPB transaksiSPB = gson.fromJson(objPreUpload.toString(), TransaksiSPB.class);

                        JSONObject objImageTSpb = new JSONObject();

                        JSONArray imageHTAngkut = new JSONArray();
                        JSONObject objImageTAngkut = new JSONObject();
                        JSONArray imageTAngkut = new JSONArray();
                        for (int i = 0; i < transaksiSPB.getTransaksiAngkut().getAngkuts().size(); i++) {
                            JSONObject objImageAngkut = new JSONObject();
                            Angkut angkut = transaksiSPB.getTransaksiAngkut().getAngkuts().get(i);
                            JSONArray imageAngkut = new JSONArray();
                            if (angkut.getFoto() != null) {
                                for (String foto : angkut.getFoto()) {
                                    JSONObject objImage = new JSONObject();
                                    File file = new File(foto);
                                    if (file.exists() && !file.getAbsolutePath().contains("http:")) {
                                        objImage.put("filename", file.getName());
                                        objImage.put("filesize", file.getTotalSpace());
                                        objImage.put("content", GlobalHelper.getEncoded64ImageStringFromBitmap(file.getAbsolutePath()));
                                        objImage.put("metadata", GlobalHelper.getMetaDataFromImages(file.getAbsolutePath()));
                                        imageAngkut.put(objImage);
                                    }
                                }

                                objImageAngkut.put("images", imageAngkut);
                                objImageAngkut.put("idAngkut", angkut.getIdAngkut());
                                imageTAngkut.put(objImageAngkut);
                            }
                        }

                        if (transaksiSPB.getFoto() != null) {
                            for (String foto : transaksiSPB.getFoto()) {
                                JSONObject objImage = new JSONObject();
                                File file = new File(foto);
                                if (file.exists() && !file.getAbsolutePath().contains("http:")) {
                                    objImage.put("filename", file.getName());
                                    objImage.put("filesize", file.getTotalSpace());
                                    objImage.put("content", GlobalHelper.getEncoded64ImageStringFromBitmap(file.getAbsolutePath()));
                                    objImage.put("metadata", GlobalHelper.getMetaDataFromImages(file.getAbsolutePath()));
                                    imageHTAngkut.put(objImage);
                                }

                            }
                        }

                        objImageTAngkut.put("idTAngkut", transaksiSPB.getTransaksiAngkut().getIdTAngkut());
                        objImageTAngkut.put("estCode", transaksiSPB.getTransaksiAngkut().getEstCode());
                        objImageTAngkut.put("photoBy", transaksiSPB.getTransaksiAngkut().getCreateBy());
                        objImageTAngkut.put("angkuts", imageTAngkut);
                        objImageTAngkut.put("images", imageHTAngkut);

                        objImageTSpb.put("transaksiAngkut", objImageTAngkut);
                        objImageTSpb.put("idSPB", transaksiSPB.getIdSPB());

                        array.put(objImageTSpb);
                        break;
                    }
                    case PostQcBuahImages:{
                        Gson gson = new Gson();
                        QcMutuBuah qcMutuBuah = gson.fromJson(objPreUpload.toString(), QcMutuBuah.class);
                        TransaksiPanen transaksiPanen = qcMutuBuah.getTransaksiPanenQc();

                        JSONObject objImagePanen = new JSONObject();
                        JSONArray imagePanen = new JSONArray();
                        if (transaksiPanen.getFoto() != null) {
                            for (String foto : transaksiPanen.getFoto()) {
                                JSONObject objImage = new JSONObject();
                                File file = new File(foto);
                                if (file.exists() && !file.getAbsolutePath().contains("http:")) {
                                    objImage.put("filename", file.getName());
                                    objImage.put("filesize", file.getTotalSpace());
                                    objImage.put("content", GlobalHelper.getEncoded64ImageStringFromBitmap(file.getAbsolutePath()));
                                    objImage.put("metadata", GlobalHelper.getMetaDataFromImages(file.getAbsolutePath()));
                                    imagePanen.put(objImage);
                                }

                            }
                            objImagePanen.put("idTPanen", transaksiPanen.getIdTPanen());
                            objImagePanen.put("estCode", transaksiPanen.getTph().getEstCode());
                            objImagePanen.put("photoBy", transaksiPanen.getUpdateBy());
                            objImagePanen.put("images", imagePanen);

                            array.put(objImagePanen);
                        }
                        break;
                    }
                    case PostSensusBjrImages:{
                        JSONArray imageBjr = objPreUpload.getJSONArray("foto");

                        JSONObject objImagePanen = new JSONObject();
                        JSONArray imagePanen = new JSONArray();
                        if (imageBjr != null) {
                            for (int i = 0 ;i < imageBjr.length();i++) {
                                JSONObject objImage = new JSONObject();
                                File file = new File(imageBjr.get(i).toString());
                                if (file.exists() && !file.getAbsolutePath().contains("http:")) {
                                    objImage.put("filename", file.getName());
                                    objImage.put("filesize", file.getTotalSpace());
                                    objImage.put("content", GlobalHelper.getEncoded64ImageStringFromBitmap(file.getAbsolutePath()));
                                    objImage.put("metadata", GlobalHelper.getMetaDataFromImages(file.getAbsolutePath()));
                                    imagePanen.put(objImage);
                                }

                            }

                            JSONObject objTph = objPreUpload.getJSONObject("tph");
                            objImagePanen.put("idTPanen", objPreUpload.getString("idTPanen"));
                            objImagePanen.put("estCode", objTph.getString("estCode"));
                            objImagePanen.put("photoBy", objPreUpload.getString("createBy"));
                            objImagePanen.put("images", imagePanen);

                            array.put(objImagePanen);
                        }
                        break;
                    }
//                    case PostQcAncakImages:{
//                        Gson gson = new Gson();
//                        QcAncak qcAncak = gson.fromJson(objPreUpload.toString(), QcAncak.class);
//                        break;
//                    }
                }
            }

            switch (tag) {
                case PostTphImage: {
                    text += "TPH";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadImagesTPH(array);
                    } else if (context instanceof MapActivity) {
                        ((MapActivity) context).connectionPresenter.UploadImagesTPH(array);
                    }
                    break;
                }
                case PostLangsiranImage: {
                    text += "Langsiran";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadImagesLangsiran(array);
                    } else if (context instanceof MapActivity) {
                        ((MapActivity) context).connectionPresenter.UploadImagesLangsiran(array);
                    }
                    break;
                }
                case PostTransaksiPanenImage: {
                    text += "Transaksi Panen";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadImagesPanen(array);
                    }
                    break;
                }
                case PostTransaksiAngkutImage: {
                    text += "Transaksi Angkut";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadImagesTransaksiAngkut(array);
                    }
                    break;
                }
                case PostTransaksiSPBImage: {
                    text += "Transaksi SPB";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadImagesSPB(array);
                    }
                    break;
                }
                case PostQcBuahImages: {
                    text += "Qc Buah Images";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadQcBuahImages(array);
                    }
                    break;
                }
                case PostSensusBjrImages:{
                    text += "Sensus Bjr";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadSensusBJRImages(array);
                    }
                    break;
                }
            }

            SyncHistroyItem item = new SyncHistroyItem(tag,idx,jsonArray.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            JSONObject objProgres = new JSONObject();
            objProgres.put("ket",text);
            objProgres.put("persen",(idx*100/jsonArray.length()));
            objProgres.put("count",idx);
            objProgres.put("total",(jsonArray.length()));
            boolean fromLongOperation = false;
            if(longOperation instanceof SettingFragment.LongOperation){
//                ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }else if(longOperation instanceof ActiveActivity.LongOperation){
//                ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }else if(longOperation instanceof MapActivity.LongOperation){
//                ((MapActivity.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }

            if(!fromLongOperation){
                if(settingFragment != null){
                    mainMenuActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateProgres(objProgres,settingFragment.myCoordinatorLayout);
                        }
                    });
                }else if (mapActivity != null){
                    mapActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateProgres(objProgres,mapActivity.coor);
                        }
                    });
                }
            }

            if(idx == jsonArray.length() - 1){
                if(snackbar != null) {
                    snackbar.dismiss();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void uploadData(Object longOperation,Tag tag,JSONArray jsonArray, int idx){
        JSONArray array = new JSONArray();
        ((BaseActivity) context).idxUpload = idx;
        String text = "Upload Data ";
        Log.d(this.getClass()+" "+ text+" "+tag,String.valueOf(idx));

        int partitionUpload = GlobalHelper.PARTITION_UPLOAD;
        switch (tag){
            case PostTransaksiAngkut:
                partitionUpload = GlobalHelper.PARTITION_UPLOAD_ANGKUT_SPB;
                break;
            case PostTransaksiAngkutImage:
                partitionUpload = GlobalHelper.PARTITION_UPLOAD_ANGKUT_SPB;
                break;
            case PostTransaksiSPB:
                partitionUpload = GlobalHelper.PARTITION_UPLOAD_ANGKUT_SPB;
                break;
            case PostTransaksiSPBImage:
                partitionUpload = GlobalHelper.PARTITION_UPLOAD_ANGKUT_SPB;
                break;
            case LogActivity:
                partitionUpload = 100;
                    break;
        }
        int idxData = 0;
        try {
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(),0);
            String userId = GlobalHelper.getUser().getUserID();
            for(int x = 0 ; x < partitionUpload;x++) {
                int index = idx + x;
                idxData = index;
                if (index < jsonArray.length()) {
                    JSONObject jObj = (JSONObject) jsonArray.get(index);
                    if (jObj.getInt("idx") == index) {
                        Log.d(this.getClass() + " "+ tag,String.valueOf(index));
                        jObj.put("userId",userId);
                        jObj.put("versionApp",pi.versionName);
                        array.put(jObj);
                    }else {
                        for (int a = 0; a < jsonArray.length(); a++) {
                            JSONObject obj = (JSONObject) jsonArray.get(a);
                            if (obj.getInt("idx") == index) {
                                Log.d(this.getClass() + " " + tag, String.valueOf(index));
                                jObj.put("userId",userId);
                                jObj.put("versionApp",pi.versionName);
                                array.put(obj);
                                break;
                            }
                        }
                    }
                }else{
                    break;
                }
            }

            switch (tag){
                case PostTph:
                    text += "TPH";
                    if(context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadTPH(array);
                    }else if(context instanceof MapActivity){
                        ((MapActivity) context).connectionPresenter.UploadTPH(array);
                    }
                    break;
                case PostLangsiran:
                    text += "Langsiran";
                    if(context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadLangsiran(array);
                    }else if(context instanceof MapActivity){
                        ((MapActivity) context).connectionPresenter.UploadLangsiran(array);
                    }
                    break;
                case PostTransaksiPanen:
                    text += "Transaksi Panen";
                    if(context instanceof MainMenuActivity) {
                        if(jsonArray.length()-1 == idxData){
                            ((MainMenuActivity) context).connectionPresenter.UploadTransaksiPanenWithSyncMaster(array);
                        } else {
                            ((MainMenuActivity) context).connectionPresenter.UploadTransaksiPanen(array);
                        }
                    }
                    break;
                case PostTransaksiPanenNFC:
                    text += "Transaksi Panen NFC";
                    if(context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadTransaksiPanenNFC(array);
                    }
                    break;
                case PostTransaksiAngkut:
                    text += "Transaksi Angkut";
                    if(context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadTransaksiAngkut(array);
                    }
                    break;
                case PostTransaksiAngkutNFC:
                    text += "Transaksi Angkut NFC";
                    if(context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadTransaksiAngkutNFC(array);
                    }
                    break;
                case PostTransaksiSPB:
                    text += "Transaksi SPB";
                    if(context instanceof MainMenuActivity) {
                        if (jsonArray.length()-1 == idxData) {
                            ((MainMenuActivity) context).connectionPresenter.UploadTransaksiSPBWithSyncMaster(array);
                        } else {
                            ((MainMenuActivity) context).connectionPresenter.UploadTransaksiSPB(array);
                        }
                    }
                    break;
                case PostQcBuah: {
                    text += "Qc Buah Images";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadQcBuah(array);
                    }
                    break;
                }
                case PostSensusBjr:{
                    text += "Sensus Bjr";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadSensusBjr(array);
                    }
                    break;
                }
                case PostQcAncak:{
                    text += "Qc Ancak";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadQcAncak(array);
                    }
                    break;
                }
                case InsertAssistanceRequest:{
                    text += "AssistanceRequest";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadAssistanceRequest(array);
                    }else if (context instanceof AssistensiActivity){
                        ((AssistensiActivity) context).connectionPresenter.UploadAssistanceRequest(array);
                    }
                    break;
                }
                case InsertOpnameNFC:{
                    text += "InsertOpnameNFC";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadOpnameNFC(array);
                    }
                    break;
                }
                case InsertGerdangDetail:{
                    text += "InsertGerdangDetail";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.InsertGerdangDetail(array);
                    }
                    break;
                }
                case LogActivity: {
                    text += "LogActivity";
                    if (context instanceof MainMenuActivity) {
                        ((MainMenuActivity) context).connectionPresenter.UploadLogActivity(array);
                    }
                    break;
                }
            }

            JSONObject objProgres = new JSONObject();
            objProgres.put("ket",text);
            objProgres.put("persen",(idx*100/jsonArray.length()));
            objProgres.put("count",idx);
            objProgres.put("total",(jsonArray.length()));
            boolean fromLongOperation = false;
            if(longOperation instanceof SettingFragment.LongOperation){
//                ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }else if(longOperation instanceof ActiveActivity.LongOperation){
//                ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }else if(longOperation instanceof MapActivity.LongOperation){
//                ((MapActivity.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }

            if(!fromLongOperation){
                if(settingFragment != null){
                    mainMenuActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateProgres(objProgres,settingFragment.myCoordinatorLayout);
                        }
                    });
                }else if (mapActivity != null){
                    mapActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateProgres(objProgres,mapActivity.coor);
                        }
                    });
                }
            }

            if(idx == jsonArray.length() - 1){
                if(snackbar != null) {
                    snackbar.dismiss();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void listTransaksiDone(Object longOperation,Tag tag,JSONArray jsonArray, int idx){
        try {
            String idString = "";
            int index = 0;
            String text = "Upload Data ";

            int partitionUpload = GlobalHelper.PARTITION_UPLOAD;
            switch (tag){
                case PostTransaksiAngkut:
                    partitionUpload = GlobalHelper.PARTITION_UPLOAD_ANGKUT_SPB;
                    break;
                case PostTransaksiAngkutImage:
                    partitionUpload = GlobalHelper.PARTITION_UPLOAD_ANGKUT_SPB;
                    break;
                case PostTransaksiSPB:
                    partitionUpload = GlobalHelper.PARTITION_UPLOAD_ANGKUT_SPB;
                    break;
                case PostTransaksiSPBImage:
                    partitionUpload = GlobalHelper.PARTITION_UPLOAD_ANGKUT_SPB;
                    break;
                case LogActivity:
                    partitionUpload = 100;
            }

            for(int x = 0 ; x < partitionUpload;x++) {
                index = idx + x;
                if (index < jsonArray.length()) {
                    JSONObject jObj = (JSONObject) jsonArray.get(index);
                    if (jObj.getInt("idx") == index) {
                        Log.d(this.getClass() + " listTransaksiDone "+tag,"idxLangsungDapat");

                        JSONObject object = transaksiListDone(tag,jObj);
                        if(object != null) {
                            if(object.has("idString")) {
                                idString += object.getString("idString");
                                text = object.getString("text");
                            }
                        }
                    }else {
                        Log.d(this.getClass() + " listTransaksiDone "+tag,"idxLooping Dahulu");

                        for (int a = 0; a < jsonArray.length(); a++) {
                            JSONObject obj = (JSONObject) jsonArray.get(a);
                            if (obj.getInt("idx") == index) {

                                JSONObject object = transaksiListDone(tag,obj);
                                if(object != null) {
                                    if(object.has("idString")) {
                                        idString += object.getString("idString");
                                        text = object.getString("text");
                                    }
                                }
                                break;
                            }
                        }
                    }
                }else{
                    index --;
                    break;
                }
            }

            //update status menjadi upload
            switch(tag){
                case PostTph:
                    SetUpDataSyncHelper.updateStatusUploadTph(idString);
//                    GlobalHelper.setIdUpload(GlobalHelper.LIST_FOLDER_TPH,idString);
                    break;
                case PostLangsiran:
                    SetUpDataSyncHelper.updateStatusUploadLangsiran(idString);
//                    GlobalHelper.setIdUpload(GlobalHelper.LIST_FOLDER_LANGSIRAN,idString);
                    break;
                case PostTransaksiPanen:
                    SetUpDataSyncHelper.updateStatusUploadTransaksiTph(idString);
//                    GlobalHelper.setIdUpload(GlobalHelper.LIST_FOLDER_TRANSAKSI_TPH,idString);
                    break;
                case PostTransaksiPanenNFC:
                    SetUpDataSyncHelper.updateStatusUploadTransaksiTphNfc(idString);
//                    GlobalHelper.setIdUpload(GlobalHelper.LIST_FOLDER_TRANSAKSI_TPH_NFC,idString);
                    break;
                case PostTransaksiAngkut:
                    SetUpDataSyncHelper.updateStatusUploadTransaksiAngkut(idString);
//                    GlobalHelper.setIdUpload(GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT,idString);
                    break;
                case PostTransaksiAngkutNFC:
                    SetUpDataSyncHelper.updateStatusUploadTransaksiAngkutNfc(idString);
//                    GlobalHelper.setIdUpload(GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT_NFC,idString);
                    break;
                case PostTransaksiSPB:
                    SetUpDataSyncHelper.updateStatusUploadTransaksiSpb(idString);
//                    GlobalHelper.setIdUpload(GlobalHelper.LIST_FOLDER_SPB,idString);
                    break;
                case PostQcBuah:
                    SetUpDataSyncHelper.updateStatusUploadQcBuah(idString);
//                    GlobalHelper.setIdUpload(GlobalHelper.LIST_FOLDER_QC_MUTU_BUAH,idString);
                    break;
                case PostSensusBjr:
                    SetUpDataSyncHelper.updateStatusUploadSensusBjr(idString);
//                    GlobalHelper.setIdUpload(GlobalHelper.LIST_FOLDER_QC_SENSUS_BJR,idString);
                    break;
                case PostQcAncak:
                    SetUpDataSyncHelper.updateStatusUploadQcAncak(idString);
//                    GlobalHelper.setIdUpload(GlobalHelper.LIST_FOLDER_QC_MUTU_ANCAK,idString);
                    break;
                case InsertAssistanceRequest:
                    SetUpDataSyncHelper.updateStatusAssistensiRequest(idString);
                    break;
                case InsertOpnameNFC:
                    SetUpDataSyncHelper.updateInsetOpname(idString);
                    break;
                case InsertGerdangDetail:
                    SetUpDataSyncHelper.updateInsetGerdang(idString);
                    break;
            }

            int persen = index + 1;
            JSONObject objProgres = new JSONObject();
            objProgres.put("ket",text);
            objProgres.put("persen",(persen*100/jsonArray.length()));
            objProgres.put("count",persen);
            objProgres.put("total",(jsonArray.length()));
            boolean fromLongOperation = false;
            if(longOperation instanceof SettingFragment.LongOperation){
//                ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }else if(longOperation instanceof ActiveActivity.LongOperation){
//                ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }else if(longOperation instanceof MapActivity.LongOperation){
//                ((MapActivity.LongOperation) longOperation).publishProgress(objProgres);
                fromLongOperation = true;
            }

            SyncHistroyItem item = new SyncHistroyItem(tag,persen,jsonArray.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            if(!fromLongOperation){
                if(settingFragment != null){
                    mainMenuActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateProgres(objProgres,settingFragment.myCoordinatorLayout);
                        }
                    });
                }else if (mapActivity != null){
                    mapActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateProgres(objProgres,mapActivity.coor);
                        }
                    });
                }
            }
            ((BaseActivity) context).idxUpload = index;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject transaksiListDone(Tag tag,JSONObject obj){
        JSONObject object = new JSONObject();
        try {
            switch (tag) {
                case PostTph:
                    object.put("text", "Upload Data TPH");
                    object.put("idString", obj.getString("noTph") + ";");
                    break;
                case PostLangsiran:
                    object.put("text", "Upload Data Langsiran");
                    object.put("idString", obj.getString("idTujuan") + ";");
                    break;
                case PostTransaksiPanen:
                    object.put("text", "Upload Data Transaksi Panen");
                    object.put("idString", obj.getString("idTPanen") + ";");
                    break;
                case PostTransaksiPanenNFC:
                    object.put("text", "Upload Data Transaksi Panen NFC");
                    object.put("idString", obj.getString("a") + ";");
                    break;
                case PostTransaksiAngkut:
                    object.put("text", "Upload Data Transaksi Angkut");
                    object.put("idString", obj.getString("idTAngkut") + ";");
                    break;
                case PostTransaksiAngkutNFC:
                    object.put("text", "Upload Data Transaksi Angkut NFC");
                    object.put("idString", obj.getString("a") + ";");
                    break;
                case PostTransaksiSPB:
                    object.put("text", "Upload Data Transaksi SPB");
                    object.put("idString", obj.getString("idSPB") + ";");
                    break;
                case PostQcBuah:
                    object.put("text", "Upload Data Qc Ancak");
                    object.put("idString", obj.getString("idTPanen") + ";");
                    break;
                case PostSensusBjr:
                    object.put("text", "Upload Data Sensus BJR");
                    object.put("idString", obj.getString("idTPanen") + ";");
                    break;
                case PostQcAncak:
                    object.put("text", "Upload Data Qc Ancak");
                    object.put("idString", obj.getString("idQcAncak") + ";");
                    break;
                case InsertAssistanceRequest:
                    object.put("text", "Upload Data Assistance Request");
                    object.put("idString", obj.getString("idAssistensi") + ";");
                    break;
                case InsertGerdangDetail:
                    object.put("text", "Upload Data Gerdang Detail");
                    object.put("idString", obj.getString("idGerdang") + ";");
                    break;
            }
            return  object;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void updateProgres(JSONObject objProgres, CoordinatorLayout coor){
        try {
            if(snackbar== null){
                snackbar = Snackbar.make(coor,objProgres.getString("ket"),Snackbar.LENGTH_INDEFINITE);
            }
            snackbar.setText(String.format(objProgres.getString("ket") + " %d %s [%d/%d] ",
                    objProgres.getInt("persen"), "%", objProgres.getInt("count"), objProgres.getInt("total")));
            snackbar.show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setSupervision(JSONArray dataPanen){
        View baseView = LayoutInflater.from(context).inflate(R.layout.supervion_layout,null);
        TextView ketPanen = (TextView) baseView.findViewById(R.id.ketPanen);
        CompleteTextViewHelper etGang = (CompleteTextViewHelper) baseView.findViewById(R.id.etGang);
//        CompleteTextViewHelper etS1 = (CompleteTextViewHelper) baseView.findViewById(R.id.etS1);
//        CompleteTextViewHelper etS2 = (CompleteTextViewHelper) baseView.findViewById(R.id.etS2);
//        CompleteTextViewHelper etS3 = (CompleteTextViewHelper) baseView.findViewById(R.id.etS3);
//        CompleteTextViewHelper etS4 = (CompleteTextViewHelper) baseView.findViewById(R.id.etS4);
        LinearLayout lsave = (LinearLayout) baseView.findViewById(R.id.lsave);

        ketPanen.setText("Jumlah Panen "+dataPanen.length());
        if(settingFragment.superVision == null) {
            selectSuperVision = new SuperVision();
        }else {
            selectSuperVision = settingFragment.superVision;
        }

        adapterGangSupervision = new SelectGangSupervisionAdapter(context, R.layout.row_setting, settingFragment.gangLists);
        etGang.setThreshold(1);
        etGang.setAdapter(adapterGangSupervision);
        adapterGangSupervision.notifyDataSetChanged();

        ArrayList<SupervisionList> supervisionLists = new ArrayList<>();
        for (SupervisionList list : settingFragment.supervisionListsAll){
//            if(list.getAfdeling().equals(settingFragment.gangLists.get(0).getAfdeling())) {
                supervisionLists.add(list);
//            }
        }


        if(selectSuperVision.getGangList() != null) {
//            etS3.setText(selectSuperVision.getS3().getNama());
            etGang.setText(selectSuperVision.getGangList().getGank());
   }

        etGang.setOnClickListener(v -> etGang.showDropDown());

        etGang.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectSuperVision.setGangList(((SelectGangSupervisionAdapter) parent.getAdapter()).getItemAt(position));
                etGang.setText(selectSuperVision.getGangList().getGank());

        }
        });

        etGang.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if(selectSuperVision.getGangList() == null){
                        etGang.setText("");
                    }else{
                        if(selectSuperVision.getGangList().getGank() != null) {
                            etGang.setText(selectSuperVision.getGangList().getGank());
                        }else{
                            etGang.setText("");
                        }
                    }
                }
            }
        });

        lsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashSet<String> distincSuperVision = new HashSet<>();
                if(selectSuperVision.getGangList() == null){
                    Toast.makeText(HarvestApp.getContext(),"Mohon pilih Gang",Toast.LENGTH_SHORT).show();
                    return;
                }
           settingFragment.superVision = TransaksiPanenHelper.saveSuperVision(selectSuperVision);
                    settingFragment.startLongOperation(SettingFragment.LongOperation_Upload_Supervision);
                    alertDialog.dismiss();
//                }
            }
        });

        alertDialog = WidgetHelper.showListReference(alertDialog,baseView,context);
    }


}
