package id.co.ams_plantation.harvestsap.adapter;

/**
 * Created by user on 12/28/2018.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.zxing.WriterException;
import com.mikepenz.iconics.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.ModelRecyclerView;
import id.co.ams_plantation.harvestsap.util.QrHelper;

public class QrItemAdapter extends RecyclerView.Adapter<QrItemAdapter.Holder>{
    Context context;
    ArrayList<ModelRecyclerView> originLists;
    public QrItemAdapter(Context context, ArrayList<ModelRecyclerView> modelRecyclerViews){
        this.context = context;
        originLists = modelRecyclerViews;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_qr,parent,false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.setValue(originLists.get(position));
    }

    @Override
    public int getItemCount() {
        return originLists.size();
    }

    @Override
    public long getItemId(int position) {
//        return super.getItemId(position);
        return position;
    }

    @Override
    public int getItemViewType(int position) {
//        return super.getItemViewType(position);
        return position;
    }

    public class Holder extends RecyclerView.ViewHolder {

        LinearLayout header;
        LinearLayout detail;
        LinearLayout subTotal;
        LinearLayout subPemanen;
        LinearLayout ll_progress;
        RelativeLayout lHeaderQr;
        RelativeLayout lQr;

        TextView tvHeaderQr;
        ImageView iv_qrcode;
        TextView header_title;
        TextView detail_title0;
        TextView detail_title1;
        TextView detail_title2;
        TextView detail_title3;
        TextView detail_title4;
        TextView detail_subTotal0;
        TextView detail_subTotal1;
        TextView detail_subTotal2;
        TextView detail_subTotal3;
        TextView detail_subPemanen0;
        TextView detail_subPemanen1;
        TextView detail_subPemanen2;
        TextView detail_subPemanen3;
        TextView detail_subPemanen4;
        TextView detail_subPemanen5;
        TextView progres_title1;
        TextView persenTextView;
        TextView tv_persen_ket;
        TextView tv_persen_ket1;
        TextView waiting;
        ProgressBar progress;

        public Holder(View itemView) {
            super(itemView);

            header = (LinearLayout) itemView.findViewById(R.id.header);
            detail = (LinearLayout) itemView.findViewById(R.id.detail);
            lHeaderQr = (RelativeLayout) itemView.findViewById(R.id.lHeaderQr);
            subTotal = (LinearLayout) itemView.findViewById(R.id.subTotal);
            subPemanen = (LinearLayout) itemView.findViewById(R.id.subPemanen);
            ll_progress = (LinearLayout) itemView.findViewById(R.id.ll_progress);
            lQr = (RelativeLayout) itemView.findViewById(R.id.lQr);

            tvHeaderQr = (TextView) itemView.findViewById(R.id.tvHeaderQr);
            header_title = (TextView) itemView.findViewById(R.id.header_title);
            iv_qrcode = (ImageView) itemView.findViewById(R.id.iv_qrcode);
            detail_title0 = (TextView) itemView.findViewById(R.id.detail_title0);
            detail_title1 = (TextView) itemView.findViewById(R.id.detail_title1);
            detail_title2 = (TextView) itemView.findViewById(R.id.detail_title2);
            detail_title3 = (TextView) itemView.findViewById(R.id.detail_title3);
            detail_title4 = (TextView) itemView.findViewById(R.id.detail_title4);
            detail_subTotal0 = (TextView) itemView.findViewById(R.id.detail_subTotal0);
            detail_subTotal1 = (TextView) itemView.findViewById(R.id.detail_subTotal1);
            detail_subTotal2 = (TextView) itemView.findViewById(R.id.detail_subTotal2);
            detail_subTotal3 = (TextView) itemView.findViewById(R.id.detail_subTotal3);
            detail_subPemanen0 = (TextView) itemView.findViewById(R.id.detail_subPemanen0);
            detail_subPemanen1 = (TextView) itemView.findViewById(R.id.detail_subPemanen1);
            detail_subPemanen2 = (TextView) itemView.findViewById(R.id.detail_subPemanen2);
            detail_subPemanen3 = (TextView) itemView.findViewById(R.id.detail_subPemanen3);
            detail_subPemanen4 = (TextView) itemView.findViewById(R.id.detail_subPemanen4);
            detail_subPemanen5 = (TextView) itemView.findViewById(R.id.detail_subPemanen5);
            progres_title1 = (TextView) itemView.findViewById(R.id.progres_title1);
            persenTextView = (TextView) itemView.findViewById(R.id.persenTextView);
            tv_persen_ket = (TextView) itemView.findViewById(R.id.tv_persen_ket);
            tv_persen_ket1 = (TextView) itemView.findViewById(R.id.tv_persen_ket1);
            progress = (ProgressBar) itemView.findViewById(R.id.progress);
        }

        public void setValue(final ModelRecyclerView value) {
            if (value.getLayout().equalsIgnoreCase("headerQr")) {
                tvHeaderQr.setText(value.getNama());
                header.setVisibility(View.GONE);
                detail.setVisibility(View.GONE);
                lQr.setVisibility(View.GONE);
                subTotal.setVisibility(View.GONE);
                subPemanen.setVisibility(View.GONE);
            } else if (value.getLayout().equalsIgnoreCase("Qr")) {
                try {
                    iv_qrcode.setImageBitmap(QrHelper.encodeAsBitmap(value.getNama()));
                } catch (WriterException e) {
                    e.printStackTrace();
                }
                lHeaderQr.setVisibility(View.GONE);
                header.setVisibility(View.GONE);
                detail.setVisibility(View.GONE);
                subTotal.setVisibility(View.GONE);
                subPemanen.setVisibility(View.GONE);
            } else if (value.getLayout().equalsIgnoreCase("Barcode")) {
                try {
                    iv_qrcode.setImageBitmap(QrHelper.encodeAsBitmapBarcode(value.getNama()));
                } catch (WriterException e) {
                    e.printStackTrace();
                }
                iv_qrcode.getLayoutParams().height = 200;
                lHeaderQr.setVisibility(View.GONE);
                header.setVisibility(View.GONE);
                detail.setVisibility(View.GONE);
                subTotal.setVisibility(View.GONE);
                subPemanen.setVisibility(View.GONE);
            } else if (value.getLayout().equalsIgnoreCase("header")) {
                header_title.setText(value.getNama());
                detail.setVisibility(View.GONE);
                lHeaderQr.setVisibility(View.GONE);
                lQr.setVisibility(View.GONE);
                subTotal.setVisibility(View.GONE);
                subPemanen.setVisibility(View.GONE);
            } else if (value.getLayout().equalsIgnoreCase("detailisi")) {
                detail_title1.setText(value.getNama());
                detail_title2.setText(value.getNama1());
                detail_title3.setText(value.getNama2());
                detail_title4.setText(value.getNama3());

                LinearLayout.LayoutParams params = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 3f);
                detail_title1.setLayoutParams(params);
                LinearLayout.LayoutParams params2 = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
                params2.setMargins(Utils.convertDpToPx(context, 10), 0, Utils.convertDpToPx(context, 10), 0);
                detail_title2.setLayoutParams(params2);
                detail_title2.setGravity(Gravity.RIGHT);
                detail_title3.setLayoutParams(params2);
                detail_title3.setGravity(Gravity.RIGHT);
                detail_title4.setLayoutParams(params2);
                detail_title4.setGravity(Gravity.RIGHT);
                detail_title0.setVisibility(View.GONE);
                header.setVisibility(View.GONE);
                lHeaderQr.setVisibility(View.GONE);
                lQr.setVisibility(View.GONE);
                subTotal.setVisibility(View.GONE);
                subPemanen.setVisibility(View.GONE);
            } else if (value.getLayout().equalsIgnoreCase("subTotal")) {
                detail_subTotal0.setText(value.getNama());
                detail_subTotal1.setText(value.getNama1());
                detail_subTotal2.setText(value.getNama2());
                detail_subTotal3.setText(value.getNama3());

                LinearLayout.LayoutParams params = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 3f);
                detail_subTotal0.setLayoutParams(params);
                LinearLayout.LayoutParams params2 = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
                params2.setMargins(Utils.convertDpToPx(context, 10), 0, Utils.convertDpToPx(context, 10), 0);
                detail_subTotal1.setLayoutParams(params2);
                detail_subTotal1.setGravity(Gravity.RIGHT);
                detail_subTotal2.setLayoutParams(params2);
                detail_subTotal2.setGravity(Gravity.RIGHT);
                detail_subTotal3.setLayoutParams(params2);
                detail_subTotal3.setGravity(Gravity.RIGHT);
                header.setVisibility(View.GONE);
                lHeaderQr.setVisibility(View.GONE);
                lQr.setVisibility(View.GONE);
                detail.setVisibility(View.GONE);
                subPemanen.setVisibility(View.GONE);
            } else if (value.getLayout().equalsIgnoreCase("iscc")) {
                detail_title1.setText(value.getNama());
                detail_title1.setGravity(Gravity.CENTER_HORIZONTAL);
                detail_title0.setVisibility(View.GONE);
                detail_title2.setVisibility(View.GONE);
                detail_title3.setVisibility(View.GONE);
                detail_title4.setVisibility(View.GONE);
                header.setVisibility(View.GONE);
                lHeaderQr.setVisibility(View.GONE);
                lQr.setVisibility(View.GONE);
                subTotal.setVisibility(View.GONE);
                subPemanen.setVisibility(View.GONE);
            }else if (value.getLayout().equalsIgnoreCase("summary")){
                String isi = value.getNama1();
                try{
                    isi = String.format("%,d",Integer.parseInt(value.getNama1()));
                }catch (Exception e){
                    e.printStackTrace();
                }

                detail_title1.setText(value.getNama());
                detail_title2.setText(isi);
                detail_title3.setText(value.getNama2());
                detail_title4.setText(value.getNama3());

                LinearLayout.LayoutParams params = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 3f);
                detail_title1.setLayoutParams(params);
                LinearLayout.LayoutParams params2 = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
                params2.setMargins(Utils.convertDpToPx(context,10),0,Utils.convertDpToPx(context,10),0);
                detail_title2.setLayoutParams(params2);
                detail_title2.setGravity(Gravity.RIGHT);
                detail_title3.setLayoutParams(params2);
                detail_title3.setGravity(Gravity.RIGHT);
                detail_title4.setLayoutParams(params2);
                detail_title4.setGravity(Gravity.RIGHT);
                detail_title4.setVisibility(View.GONE);
                header.setVisibility(View.GONE);
                lHeaderQr.setVisibility(View.GONE);
                lQr.setVisibility(View.GONE);
                subTotal.setVisibility(View.GONE);
                subPemanen.setVisibility(View.GONE);
            }else if (value.getLayout().equalsIgnoreCase("progress")) {
                int progres = 0;
                if(!value.getNama2().equals("0")) {
                    if (value.getNama1().equals("0")) {
                        progres = 0;
                    } else {
                        progres = Math.round(Integer.parseInt(value.getNama2()) / Integer.parseInt(value.getNama1()) * 100);
                    }
                }
                progres_title1.setText(value.getNama());
                progress.setProgress(progres);
                persenTextView.setText(String.format("%,d",progres));
                tv_persen_ket.setText(String.format("[ %,d / %,d ]",Integer.parseInt(value.getNama1()),Integer.parseInt(value.getNama2())));
                if(progres == 0){
                    tv_persen_ket1.setText(" - ");
                }else {
                    SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy HH:mm:ss");
                    tv_persen_ket1.setText(sdf.format(new Date(Long.parseLong(value.getNama3()))));
                }

                ll_progress.setVisibility(View.VISIBLE);
                header.setVisibility(View.GONE);
                lHeaderQr.setVisibility(View.GONE);
                lQr.setVisibility(View.GONE);
                subTotal.setVisibility(View.GONE);
                detail.setVisibility(View.GONE);
                subPemanen.setVisibility(View.GONE);
            }else if (value.getLayout().equalsIgnoreCase("subPemanenHeader")){
                detail_subPemanen0.setText(value.getNama());
                detail_subPemanen1.setText(value.getNama1());
                detail_subPemanen2.setText(value.getNama2());
                detail_subPemanen3.setText(value.getNama3());
                detail_subPemanen4.setText(value.getNama4());
                detail_subPemanen5.setText(value.getNama5());

                detail_subPemanen0.setGravity(Gravity.CENTER);
                detail_subPemanen1.setGravity(Gravity.CENTER);
                detail_subPemanen2.setGravity(Gravity.CENTER);
                detail_subPemanen3.setGravity(Gravity.CENTER);
                detail_subPemanen4.setGravity(Gravity.CENTER);
                detail_subPemanen5.setGravity(Gravity.CENTER);

                header.setVisibility(View.GONE);
                lHeaderQr.setVisibility(View.GONE);
                lQr.setVisibility(View.GONE);
                subTotal.setVisibility(View.GONE);
                detail.setVisibility(View.GONE);
            }else if (value.getLayout().equalsIgnoreCase("subPemanen")){
                detail_subPemanen0.setText(value.getNama());
                detail_subPemanen1.setText(value.getNama1());
                detail_subPemanen2.setText(value.getNama2());
                detail_subPemanen3.setText(value.getNama3());
                detail_subPemanen4.setText(value.getNama4());
                detail_subPemanen5.setText(value.getNama5());

                detail_subPemanen2.setGravity(Gravity.RIGHT);
                detail_subPemanen3.setGravity(Gravity.RIGHT);
                detail_subPemanen4.setGravity(Gravity.RIGHT);
                detail_subPemanen5.setGravity(Gravity.RIGHT);

                header.setVisibility(View.GONE);
                lHeaderQr.setVisibility(View.GONE);
                lQr.setVisibility(View.GONE);
                subTotal.setVisibility(View.GONE);
                detail.setVisibility(View.GONE);
            }else if (value.getLayout().startsWith("detailganda")){
                if(value.getLayout().contains("header")) {
                    detail.setBackgroundColor(context.getResources().getColor(R.color.Green));
                    detail_title1.setTextColor(context.getResources().getColor(R.color.White));
                    detail_title2.setTextColor(context.getResources().getColor(R.color.White));
                    detail_title3.setTextColor(context.getResources().getColor(R.color.White));
                    detail_title4.setTextColor(context.getResources().getColor(R.color.White));
                }
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                        0,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        1.5f
                );
                LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams(
                        0,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        2.5f
                );
                detail_title1.setLayoutParams(param);
                detail_title2.setLayoutParams(param1);
                detail_title3.setLayoutParams(param1);
                detail_title4.setLayoutParams(param1);

                detail_title1.setText(value.getNama());
                detail_title2.setText(value.getNama1());
                detail_title3.setText(value.getNama2());
                detail_title4.setText(value.getNama3());
                detail_title0.setVisibility(View.GONE);
                header.setVisibility(View.GONE);
                lHeaderQr.setVisibility(View.GONE);
                lQr.setVisibility(View.GONE);
                subTotal.setVisibility(View.GONE);
                subPemanen.setVisibility(View.GONE);
            }else if (value.getLayout().startsWith("detailloader")){
                if(value.getLayout().contains("header")) {
                    detail.setBackgroundColor(context.getResources().getColor(R.color.Green));
                    detail_title1.setTextColor(context.getResources().getColor(R.color.White));
                    detail_title2.setTextColor(context.getResources().getColor(R.color.White));
                    detail_title3.setTextColor(context.getResources().getColor(R.color.White));
                    detail_title4.setTextColor(context.getResources().getColor(R.color.White));
                }
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                        0,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        4.0f
                );
                LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams(
                        0,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        3.0f
                );
                detail_title1.setLayoutParams(param);
                detail_title2.setLayoutParams(param1);
                detail_title3.setLayoutParams(param1);

                detail_title1.setText(value.getNama());
                detail_title2.setText(value.getNama1());
                detail_title3.setText(value.getNama2());
                detail_title4.setVisibility(View.GONE);
                detail_title0.setVisibility(View.GONE);
                header.setVisibility(View.GONE);
                lHeaderQr.setVisibility(View.GONE);
                lQr.setVisibility(View.GONE);
                subTotal.setVisibility(View.GONE);
                subPemanen.setVisibility(View.GONE);
            }else if (value.getLayout().startsWith("detailcages")) {
                if (value.getLayout().contains("header")) {
                    detail.setBackgroundColor(context.getResources().getColor(R.color.Green));
                    detail_title1.setTextColor(context.getResources().getColor(R.color.White));
                    detail_title2.setTextColor(context.getResources().getColor(R.color.White));
                    detail_title3.setTextColor(context.getResources().getColor(R.color.White));
                    detail_title4.setTextColor(context.getResources().getColor(R.color.White));
                }
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                        0,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        4.0f
                );
                LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams(
                        0,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        3.0f
                );
                detail_title1.setLayoutParams(param);
                detail_title2.setLayoutParams(param1);
                detail_title3.setLayoutParams(param1);

                detail_title1.setText(value.getNama());
                detail_title2.setText(value.getNama1());
                detail_title3.setText(value.getNama2());
                detail_title4.setVisibility(View.GONE);
                detail_title0.setVisibility(View.GONE);
                header.setVisibility(View.GONE);
                lHeaderQr.setVisibility(View.GONE);
                lQr.setVisibility(View.GONE);
                subTotal.setVisibility(View.GONE);
                subPemanen.setVisibility(View.GONE);
            }else{
                detail_title1.setText(value.getNama());
                detail_title2.setText(value.getNama1());
                detail_title3.setText(value.getNama2());
                detail_title4.setVisibility(View.GONE);
                header.setVisibility(View.GONE);
                lHeaderQr.setVisibility(View.GONE);
                lQr.setVisibility(View.GONE);
                subTotal.setVisibility(View.GONE);
                subPemanen.setVisibility(View.GONE);
            }
        }
    }
}