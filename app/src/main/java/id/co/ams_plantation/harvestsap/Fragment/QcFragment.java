package id.co.ams_plantation.harvestsap.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.ModelRecyclerView;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestsap.ui.QcMutuAncakActivity;
import id.co.ams_plantation.harvestsap.ui.QcMutuBuahActivity;
import id.co.ams_plantation.harvestsap.ui.QcSensusBjrActivity;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

/**
 * Created by user on 12/4/2018.
 */

public class QcFragment extends Fragment {

    //setting codeID
    public static final int Qc_MutuAncak = 0;
    public static final int Qc_MutuBuah = 1;
    public static final int Qc_SensusBJR = 2;
    public static final int Qc_SortasiPKS = 3;

    AlertDialog progressDialog;
    RecyclerView rv;
    boolean isVisibleToUser;
    MainMenuActivity mainMenuActivity;

    public static QcFragment getInstance() {
        QcFragment fragment = new QcFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qc_layout, null, false);
        mainMenuActivity = (MainMenuActivity) getActivity();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        main();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rv = view.findViewById(R.id.rv);
        main();
    }

    private void main(){
        if (isVisibleToUser && getActivity()!= null) {
            mainMenuActivity.ivLogo.setVisibility(View.VISIBLE);
            mainMenuActivity.btnFilter.setVisibility(View.GONE);

            ArrayList<ModelRecyclerView> arrayList = new ArrayList<>();
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_palm),
                    getString(R.string.qc_mutu_ancak), getString(R.string.qc_mutu_ancak_info), String.valueOf(Qc_MutuAncak)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_panen),
                    getString(R.string.qc_mutu_buah), getString(R.string.qc_mutu_buah_info), String.valueOf(Qc_MutuBuah)));
            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_sensus_bjr),
                    getString(R.string.sensus_bjr), getString(R.string.sensus_bjr_info), String.valueOf(Qc_SensusBJR)));
//            arrayList.add(new ModelRecyclerView(String.valueOf(R.drawable.ic_factory),
//                    getString(R.string.sortasi_pks), getString(R.string.sortasi_pks_info), String.valueOf(Qc_SortasiPKS)));

            QcItemAdapter adapter = new QcItemAdapter(getActivity(), arrayList);
            rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
            rv.setAdapter(scaleInAnimationAdapter);
        }
    }

    class QcItemAdapter extends RecyclerView.Adapter<QcItemAdapter.Holder>{
        Context context;
        ArrayList<ModelRecyclerView> originLists;
        public QcItemAdapter(Context context, ArrayList<ModelRecyclerView> modelRecyclerViews){
            this.context = context;
            originLists = modelRecyclerViews;
        }

        @Override
        public QcItemAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.row_setting,parent,false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(QcItemAdapter.Holder holder, int position) {
            holder.setValue(originLists.get(position));
        }

        @Override
        public int getItemCount() {
            return originLists!=null ? originLists.size() : 0;
        }

        public class Holder extends RecyclerView.ViewHolder {

            CardView cv;
            ImageView iv;
            TextView tv;
            TextView tv1;

            public Holder(View itemView) {
                super(itemView);
                cv = (CardView) itemView.findViewById(R.id.cv);
                iv = (ImageView) itemView.findViewById(R.id.iv);
                tv = (TextView) itemView.findViewById(R.id.tv);
                tv1 = (TextView) itemView.findViewById(R.id.tv1);
            }

            public void setValue(ModelRecyclerView value) {
                iv.setImageResource(Integer.parseInt(value.getLayout()));
                tv.setText(value.getNama());
                tv1.setText(value.getNama1());
                cv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new LongOperation(Integer.parseInt(value.getNama2())).execute(value.getNama2());
                    }
                });

            }
        }
    }

    public class LongOperation extends AsyncTask<String, String, String> {
        int statLongOperation;

        public LongOperation(){}

        public LongOperation(int statLongOperation) {
            this.statLongOperation = statLongOperation;
        }

        @Override
        protected void onPreExecute() {
            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),getActivity().getResources().getString(R.string.wait));
        }


        @Override
        protected String doInBackground(String... strings) {
            return strings[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            switch (Integer.parseInt(result)){
                case Qc_MutuAncak:
                    Intent intent = new Intent(getActivity(), QcMutuAncakActivity.class);
                    startActivityForResult(intent,GlobalHelper.RESULT_QC_MUTU_ANCAK);
                    break;
                case Qc_MutuBuah:
                    Intent i = new Intent(getActivity(), QcMutuBuahActivity.class);
                    startActivityForResult(i,GlobalHelper.RESULT_QC_MUTU_BUAH);
                    break;
                case Qc_SensusBJR:
                    Intent in = new Intent(getActivity(), QcSensusBjrActivity.class);
                    startActivityForResult(in,GlobalHelper.RESULT_QC_SENSUS_BJR);
                    break;

                case Qc_SortasiPKS:
                    ((BaseActivity) getActivity()).alertDialogBase.dismiss();
                    Toast.makeText(HarvestApp.getContext(),"Comming Soon",Toast.LENGTH_SHORT).show();
//                                Intent intent = new Intent(getActivity(), QRScan.class);
//                                startActivityForResult(intent,GlobalHelper.RESULT_SCAN_QR);
                    break;

            }
        }
    }
}