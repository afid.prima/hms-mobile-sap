package id.co.ams_plantation.harvestsap.model;

import id.co.ams_plantation.harvestsap.util.GlobalHelper;

public class SubPemanen {
    public static final int idPemnaen = 0;
    public static final int idOperator = 1;
    public static final int idOperatorTonase = 2;

    public static final String [] arrayTipePemanen = {
      "Pemanen",
      "Operator Janjang",
      "Operator Tonase"
    };

    public static final String [] arrayCodeTipePemanen = {
            "P",
            "OJ",
            "OT"
    };

    public static final Pemanen pemanenSPK = new Pemanen(
            "9999999999",
            "99999",
            "Pemanen Kontanan",
            "PN999",
            GlobalHelper.getEstate().getEstCode(),
            GlobalHelper.getEstate().getEstCode(),
            ""
            );

    int noUrutKongsi;
    String oph;
    int persentase;
    HasilPanen hasilPanen;
    int tipePemanen;
    Pemanen pemanen;
//
//int tangkaiPanjang;
//int buahMentah;
//int janjangPendapatan;
//int brondolan;
//int janjang;

    public SubPemanen(int noUrutKongsi, Pemanen pemanen, int persentase, HasilPanen hasilPanen) {
        this.noUrutKongsi = noUrutKongsi;
        this.pemanen = pemanen;
        this.persentase = persentase;
        this.hasilPanen = hasilPanen;
    }

    public SubPemanen(int noUrutKongsi, String oph, int persentase,HasilPanen hasilPanen, int tipePemanen, Pemanen pemanen) {
        this.noUrutKongsi = noUrutKongsi;
        this.oph = oph;
        this.persentase = persentase;
        this.hasilPanen = hasilPanen;
        this.tipePemanen = tipePemanen;
        this.pemanen = pemanen;
    }



    public SubPemanen(HasilPanen hasilPanen,int tipePemanen) {
        this.hasilPanen = hasilPanen;
        this.tipePemanen = tipePemanen;
    }

//    public SubPemanen(int noUrutKongsi, Pemanen pemanen, int persentase, int janjang, int brondolan,int janjangPendapatan,int buahMentah,int tangkaiPanjang) {
//        this.noUrutKongsi = noUrutKongsi;
//        this.pemanen = pemanen;
//        this.persentase = persentase;
//        this.janjang = janjang;
//        this.brondolan = brondolan;
//        this.janjangPendapatan = janjangPendapatan;
//        this.buahMentah = buahMentah;
//        this.tangkaiPanjang = tangkaiPanjang;
//    }
//
//    public SubPemanen(int noUrutKongsi, String oph, int persentase, int janjang, int brondolan,int janjangPendapatan,Pemanen pemanen) {
//        this.noUrutKongsi = noUrutKongsi;
//        this.oph = oph;
//        this.persentase = persentase;
//        this.janjang = janjang;
//        this.brondolan = brondolan;
//        this.janjangPendapatan = janjangPendapatan;
//        this.pemanen = pemanen;
//    }

//    public SubPemanen(int noUrutKongsi, String oph, int persentase, int janjang, int brondolan,int janjangPendapatan,int buahMentah,int tangkaiPanjang, int tipePemanen, Pemanen pemanen) {
//        this.noUrutKongsi = noUrutKongsi;
//        this.oph = oph;
//        this.persentase = persentase;
//        this.janjang = janjang;
//        this.brondolan = brondolan;
//        this.janjangPendapatan = janjangPendapatan;
//        this.buahMentah = buahMentah;
//        this.tangkaiPanjang = tangkaiPanjang;
//        this.tipePemanen = tipePemanen;
//        this.pemanen = pemanen;
//    }

//    public SubPemanen(int janjang, int brondolan,int janjangPendapatan,int buahMentah,int tangkaiPanjang) {
//        this.janjang = janjang;
//        this.brondolan = brondolan;
//        this.janjangPendapatan = janjangPendapatan;
//        this.buahMentah = buahMentah;
//        this.tangkaiPanjang = tangkaiPanjang;
//    }

//    public SubPemanen(int janjang, int brondolan,int janjangPendapatan,int buahMentah,int tangkaiPanjang,int tipePemanen) {
//        this.janjang = janjang;
//        this.brondolan = brondolan;
//        this.janjangPendapatan = janjangPendapatan;
//        this.buahMentah = buahMentah;
//        this.tangkaiPanjang = tangkaiPanjang;
//        this.tipePemanen = tipePemanen;
//    }

    public int getNoUrutKongsi() {
        return noUrutKongsi;
    }

    public void setNoUrutKongsi(int noUrutKongsi) {
        this.noUrutKongsi = noUrutKongsi;
    }

    public String getOph() {
        return oph;
    }

    public void setOph(String oph) {
        this.oph = oph;
    }

    public int getPersentase() {
        return persentase;
    }

    public void setPersentase(int persentase) {
        this.persentase = persentase;
    }

    public int getTipePemanen() {
        return tipePemanen;
    }

    public void setTipePemanen(int tipePemanen) {
        this.tipePemanen = tipePemanen;
    }

    public Pemanen getPemanen() {
        return pemanen;
    }

    public void setPemanen(Pemanen pemanen) {
        this.pemanen = pemanen;
    }

    public HasilPanen getHasilPanen() {
        return hasilPanen;
    }

    public void setHasilPanen(HasilPanen hasilPanen) {
        this.hasilPanen = hasilPanen;
    }

//    public int getJanjang() {
//        return janjang;
//    }
//
//    public void setJanjang(int janjang) {
//        this.janjang = janjang;
//    }
//
//    public int getBrondolan() {
//        return brondolan;
//    }
//
//    public void setBrondolan(int brondolan) {
//        this.brondolan = brondolan;
//    }
//    public int getJanjangPendapatan() {
//        return janjangPendapatan;
//    }
//
//    public void setJanjangPendapatan(int janjangPendapatan) {
//        this.janjangPendapatan = janjangPendapatan;
//    }
//
//    public int getBuahMentah() {
//        return buahMentah;
//    }
//
//    public void setBuahMentah(int buahMentah) {
//        this.buahMentah = buahMentah;
//    }
//
//    public int getTangkaiPanjang() {
//        return tangkaiPanjang;
//    }
//
//    public void setTangkaiPanjang(int tangkaiPanjang) {
//        this.tangkaiPanjang = tangkaiPanjang;
//    }
}
