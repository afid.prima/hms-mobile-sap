package id.co.ams_plantation.harvestsap.model;

import java.util.ArrayList;

/**
 * Created by user on 12/20/2018.
 */

public class TransaksiTransit {
    String idTTransit;
    int janjangNormal;
    int brondolan;
    int jenis;
    String createBy;
    Long createDate;
    ArrayList<TransaksiAngkut> transaksiAngkuts;
    double latitude;
    double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getIdTTransit() {
        return idTTransit;
    }

    public void setIdTTransit(String idTTransit) {
        this.idTTransit = idTTransit;
    }

    public int getJanjangNormal() {
        return janjangNormal;
    }

    public void setJanjangNormal(int janjangNormal) {
        this.janjangNormal = janjangNormal;
    }

    public int getBrondolan() {
        return brondolan;
    }

    public void setBrondolan(int brondolan) {
        this.brondolan = brondolan;
    }

    public int getJenis() {
        return jenis;
    }

    public void setJenis(int jenis) {
        this.jenis = jenis;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }

    public ArrayList<TransaksiAngkut> getTransaksiAngkuts() {
        return transaksiAngkuts;
    }

    public void setTransaksiAngkuts(ArrayList<TransaksiAngkut> transaksiAngkuts) {
        this.transaksiAngkuts = transaksiAngkuts;
    }
}
