package id.co.ams_plantation.harvestsap.Fragment;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArraySet;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.mikepenz.iconics.utils.Utils;
import com.roughike.swipeselector.OnSwipeItemSelectedListener;
import com.roughike.swipeselector.SwipeItem;
import com.roughike.swipeselector.SwipeSelector;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.adapter.TphAdapter;
import id.co.ams_plantation.harvestsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.holder.TPHRowHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestsap.TableView.view_model.TPHTableViewModel;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.HeaderTableTransaksi;
import id.co.ams_plantation.harvestsap.model.ModelRecyclerView;
import id.co.ams_plantation.harvestsap.model.ModelTphFragment;
import id.co.ams_plantation.harvestsap.model.NearestBlock;
import id.co.ams_plantation.harvestsap.model.Pemanen;
import id.co.ams_plantation.harvestsap.model.SubPemanen;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestsap.ui.MekanisasiActivity;
import id.co.ams_plantation.harvestsap.ui.SpbActivity;
import id.co.ams_plantation.harvestsap.ui.TphActivity;
import id.co.ams_plantation.harvestsap.util.ActivityLoggerHelper;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.FilterHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.MekanisasiHelper;
import id.co.ams_plantation.harvestsap.util.ModelRecyclerViewHelper;
import id.co.ams_plantation.harvestsap.util.TphHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiPanenHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;

/**
 * Created by user on 12/4/2018.
 */

public class TphFragment extends Fragment {

    public static final int STATUS_LONG_OPERATION_NONE = 0;
    public static final int STATUS_LONG_OPERATION_SEARCH = 1;
    public static final int STATUS_LONG_OPERATION_SELECT_ITEM = 2;
    public static final int STATUS_LONG_OPERATION_NEW_DATA = 3;
    public static final int STATUS_LONG_OPERATION_CALCULATE_SUMMARY = 4;
    public static final int STATUS_LONG_OPERATION_NEW_DATA_MEKANISASI = 5;

    Boolean dragggin = false;

    HashMap<String, ModelTphFragment> origin;
    Set<String> listSearch;
    TphAdapter tphAdapter;
    TableView mTableView;
//    Filter mTableFilter;
    TPHTableViewModel mTableViewModel;
    CompleteTextViewHelper etSearch;
    Button btnSearch;
    RelativeLayout ltableview;

    LinearLayout llNewData;
    TextView tvStartTime;
    TextView tvFinishTime;
    TextView tvTotalPemanen;
    TextView tvTotalJanjang;
    TextView tvTotalTph;
    TextView tvTotalBrondolan;
    TextView tvUpload;
    TextView tvRestan;
    TextView tvPemanenHadir;

    Date dateSelected;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:ss");

    Long mulai = Long.valueOf(0);
    Long sampai = Long.valueOf(0);
    int tJanjang = 0;
    int tBrondolan = 0;
    int upload= 0;
    int restan = 0;
    Set<String> setGank;
    Set<String> blockMekanisasi;
    HashSet<String> tampungTph = new HashSet<>();
    HashSet<String> tampungPemanen = new HashSet<>();
    HashMap<String,Integer> blockWarna = new HashMap<>();
    HashMap<String ,User> getAllUser = new HashMap<>();

    CardView llket;
    TransaksiPanen selectedTransaksiPanen;
    boolean isVisibleToUser;
    MainMenuActivity mainMenuActivity;
    HashMap<Long,TransaksiPanen> arrayLog;

    TransaksiPanenHelper transaksiPanenHelper;
    TphHelper tphHelper;
    ArrayList<HeaderTableTransaksi> headerTableTransaksis;
    ArrayList<ModelRecyclerView> summaryTableTransaksis;
    ModelRecyclerViewHelper modelRecyclerViewHelper;

    MekanisasiHelper mekanisasiHelper;
    Long timeScreen;
    int flgNewHarvest = 0;

    int [] warnaBlok = {
            HarvestApp.getContext().getResources().getColor(R.color.DeepSkyBlue),
            HarvestApp.getContext().getResources().getColor(R.color.MediumSpringGreen),
            HarvestApp.getContext().getResources().getColor(R.color.Aqua),
            HarvestApp.getContext().getResources().getColor(R.color.CornflowerBlue),
            HarvestApp.getContext().getResources().getColor(R.color.Chartreuse),
            HarvestApp.getContext().getResources().getColor(R.color.DarkSeaGreen),
            HarvestApp.getContext().getResources().getColor(R.color.GreenYellow),
            HarvestApp.getContext().getResources().getColor(R.color.PowderBlue),
            HarvestApp.getContext().getResources().getColor(R.color.Tan),
            HarvestApp.getContext().getResources().getColor(R.color.Plum)
    };

    public static TphFragment getInstance(){
        TphFragment fragment = new TphFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tph_layout,null,false);
        mainMenuActivity = (MainMenuActivity) getActivity();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        main();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        timeScreen = System.currentTimeMillis();

        SwipeSelector menuSwipeSelector = view.findViewById(R.id.menuSwipeSelector);
        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        ltableview = view.findViewById(R.id.ltableview);
        mTableView = view.findViewById(R.id.tableview);
        llNewData = view.findViewById(R.id.llNewData);
        tvStartTime = view.findViewById(R.id.tvStartTime);
        tvFinishTime = view.findViewById(R.id.tvFinishTime);
        tvTotalPemanen = view.findViewById(R.id.tvTotalPemanen);
        tvTotalJanjang = view.findViewById(R.id.tvTotalJanjang);
        tvTotalTph = view.findViewById(R.id.tvTotalTph);
        tvTotalBrondolan = view.findViewById(R.id.tvTotalBrondolan);
        tvUpload = view.findViewById(R.id.tvUpload);
        tvRestan = view.findViewById(R.id.tvRestan);
        tvPemanenHadir = view.findViewById(R.id.tvPemanenHadir);
        llket = view.findViewById(R.id.llket);

        Log.d("TphFragment.java", "1 timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
        timeScreen = System.currentTimeMillis();

        dateSelected = new Date();
        origin = new HashMap<>();
        transaksiPanenHelper = new TransaksiPanenHelper(getActivity());
        modelRecyclerViewHelper = new ModelRecyclerViewHelper(getActivity());
        mekanisasiHelper = new MekanisasiHelper(mainMenuActivity);
        tphHelper = new TphHelper(getActivity());

        Log.d("TphFragment.java", "2 timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
        timeScreen = System.currentTimeMillis();
//        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_TPH);
//        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
//        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
//        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
//            DataNitrit dataNitrit = (DataNitrit) iterator.next();
//            Gson gson = new Gson();
//            TransaksiPanen transaksiPanen = gson.fromJson(dataNitrit.getValueDataNitrit(),TransaksiPanen.class);
//            Log.d("update RV Date 1", String.valueOf(dateSelected == new Date(transaksiPanen.getCreateDate())));
//            Log.d("update RV Date 2", sdf.format(dateSelected));
//            Log.d("update RV Date 3", sdf.format(new Date(transaksiPanen.getCreateDate())));
//            if(dateSelected.equals(new Date(transaksiPanen.getCreateDate()))) {
//                origin.add(transaksiPanen);
//            }
//        }
//
//        db.close();

        ArrayList<SwipeItem> swipeItems= new ArrayList<>();
        Long now = System.currentTimeMillis();
        for(int i = 0 ; i < GlobalHelper.MAX_LAST_DAY_DATA; i++){
            Long l = now - ( i * 24 * 60 * 60 * 1000);
            swipeItems.add(new SwipeItem(String.valueOf(i),sdf.format(l),""));
        }
        Collections.reverse(swipeItems);

        Log.d("TphFragment.java", "3 timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
        timeScreen = System.currentTimeMillis();

        SwipeItem[] tmpStrSwipe = new SwipeItem[swipeItems.size()];
        tmpStrSwipe = swipeItems.toArray(tmpStrSwipe);
        menuSwipeSelector.setItems(tmpStrSwipe);
        menuSwipeSelector.selectItemAt(swipeItems.size() - 1);
        menuSwipeSelector.setOnItemSelectedListener(new OnSwipeItemSelectedListener() {
            @Override
            public void onItemSelected(SwipeItem item) {
                try {
                    Date date = sdf.parse(item.getTitle());
                    dateSelected = date;
                    startLongOperation(STATUS_LONG_OPERATION_NONE);
                    Log.d("item Date ", date.getTime() + " "+ sdf.format(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        Log.d("TphFragment.java", "4 timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
        timeScreen = System.currentTimeMillis();


        headerTableTransaksis = transaksiPanenHelper.setHeaderTableTransaksi();

        Log.d("TphFragment.java", "5 timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
        timeScreen = System.currentTimeMillis();


//        mTableViewModel = new TPHTableViewModel(getContext(),origin);
//        tphAdapter = new TphAdapter(getActivity());
//        mTableView.setAdapter(mTableViewAdapter);
//        mTableViewAdapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel
//                .getRodynamicParameterPenghasilanwHeaderList(), mTableViewModel.getCellList());

//        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                if(newState==RecyclerView.SCROLL_STATE_IDLE){
//                    if(origin.size() > 0){
//                        fab.show();
//                    }
//                }
//
//                super.onScrollStateChanged(recyclerView, newState);
//                if(newState==RecyclerView.SCROLL_STATE_DRAGGING){
//                    dragggin = true;
//                }else{
//                    dragggin = false;
//                }
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                if (dy > 0 ||dy<0 && fab.isShown())
//                {
//                    fab.hide();
//                }
//
//                if (dy > 0) {
//
//                }else{
//                    if(dragggin) {
//                        Log.e("recycleScroll", "sync");
//                        dragggin = false;
//                    }
//                }
//            }
//        });

//        etSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                mTableFilter.set(String.valueOf(editable));
//            }
//        });

        llket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLongOperation(STATUS_LONG_OPERATION_CALCULATE_SUMMARY);
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLongOperation(STATUS_LONG_OPERATION_SEARCH);
            }
        });

        llNewData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mekanisasiHelper.popupNewPanen();

//                ActivityLoggerHelper.recordHitTime(TphFragment.class.getSimpleName(),"llNewData",new Date(),new Date());
//                startLongOperation(STATUS_LONG_OPERATION_NEW_DATA);

//                Intent intent = new Intent(getActivity(), TphActivity.class);
//                startActivityForResult(intent,GlobalHelper.RESULT_TPHACTIVITY);
            }
        });

        Log.d("TphFragment.java", "6 timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
        timeScreen = System.currentTimeMillis();

        main();

        Log.d("TphFragment.java", "7 timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
        timeScreen = System.currentTimeMillis();

//        showNewTranskasi();

        Log.d("TphFragment.java", "8 timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
        timeScreen = System.currentTimeMillis();
    }

    public void chosenPanen(int flag){
        flgNewHarvest = flag;
        ActivityLoggerHelper.recordHitTime(TphFragment.class.getSimpleName(),"llNewData",new Date(),new Date());
        startLongOperation(STATUS_LONG_OPERATION_NEW_DATA);
    }

    public void chosenSPB(){
        ActivityLoggerHelper.recordHitTime(TphFragment.class.getSimpleName(),"llNewData",new Date(),new Date());
        startLongOperation(STATUS_LONG_OPERATION_NEW_DATA_MEKANISASI);
    }

    private void showNewTranskasi(){
        if(GlobalHelper.enableToNewTransaksi(getAllUser)){
            llNewData.setVisibility(View.VISIBLE);
        }else{
            llNewData.setVisibility(View.GONE);
        }
    }

    private void main(){
        if (isVisibleToUser && getActivity() != null) {

            Log.d("TphFragment.java", "9 timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
            timeScreen = System.currentTimeMillis();

            mainMenuActivity.ivLogo.setVisibility(View.GONE);
            mainMenuActivity.btnFilter.setVisibility(View.VISIBLE);
            mainMenuActivity.filterHelper.setFilterView(this);
            startLongOperation(STATUS_LONG_OPERATION_NONE);
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    private void updateDataRv(int status) {
        mulai = Long.valueOf(0);
        sampai = Long.valueOf(0);
        tJanjang = 0;
        tBrondolan = 0;
        upload = 0;
        restan = 0;
        tampungTph = new HashSet<>();
        tampungPemanen = new HashSet<>();
        origin = new HashMap<>();
        blockWarna = new HashMap<>();
        listSearch = new android.support.v4.util.ArraySet<>();
        setGank = new android.support.v4.util.ArraySet<>();
        blockMekanisasi = new android.support.v4.util.ArraySet<>();

        String userId = GlobalHelper.getUser().getUserID();
        String filterAfdeling = mainMenuActivity.tvFilterAfdeling.getText().toString();
        String filterCreateBy = mainMenuActivity.tvFilterDataInputBy.getText().toString();

        arrayLog = new HashMap<>();
        String isiLog = GlobalHelper.readFileContent(GlobalHelper.getLogTransaksi(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TRANSAKSI_TPH], sdf.format(dateSelected)));
        if(!isiLog.isEmpty()) {
            try {
                JSONObject objIsiLog = new JSONObject(isiLog);
                Iterator<String> keys = objIsiLog.keys();

                while (keys.hasNext()) {
                    String key = keys.next();
                    if (objIsiLog.get(key) instanceof JSONObject) {
                        Gson gson = new Gson();
                        TransaksiPanen transaksiPanen = gson.fromJson(objIsiLog.toString(), TransaksiPanen.class);
                        arrayLog.put(transaksiPanen.getCreateDate(), transaksiPanen);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ArrayList<String> allDb = GlobalHelper.getAllFileDB(GlobalHelper.TABLE_TRANSAKSI_TPH,true);
        for(String dbFile : allDb) {
            Nitrite db = GlobalHelper.getTableNitritFullPath(dbFile);
            if (db != null) {
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
                for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                    DataNitrit dataNitrit = (DataNitrit) iterator.next();
                    Gson gson = new Gson();
                    TransaksiPanen transaksiPanen = gson.fromJson(dataNitrit.getValueDataNitrit(), TransaksiPanen.class);

                    if (transaksiPanen.getStatus() != TransaksiPanen.DELETED) {
                        if (sdf.format(dateSelected).equals(sdf.format(new Date(transaksiPanen.getCreateDate())))) {

                            if (transaksiPanen.getStatus() == TransaksiPanen.TAP) {
                                arrayLog.put(transaksiPanen.getCreateDate(), transaksiPanen);
                            }

                            if (transaksiPanen.getStatus() == TransaksiPanen.UPLOAD) {
                                upload++;
                                restan = transaksiPanen.getHasilPanen().getTotalJanjang() + restan;
                            } else if (transaksiPanen.getStatus() == TransaksiPanen.SUDAH_DIANGKUT){
                                upload++;
                            } else if (transaksiPanen.getStatus() == TransaksiPanen.SUDAH_DIPKS){
                                upload ++;
                            }

                            for (int a = 0 ; a < transaksiPanen.getKongsi().getSubPemanens().size(); a++){
                                SubPemanen subPemanen = transaksiPanen.getKongsi().getSubPemanens().get(a);

                                TransaksiPanen panenAdd = null;
                                if (status == STATUS_LONG_OPERATION_SEARCH) {
                                    if (transaksiPanen.getTph().getNamaTph().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                                            transaksiPanen.getTph().getBlock().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                                            subPemanen.getPemanen().getNama().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                                            subPemanen.getPemanen().getNik().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                                            String.valueOf(transaksiPanen.getHasilPanen().getTotalJanjang()).contains(etSearch.getText().toString().toLowerCase()) ||
                                            String.valueOf(transaksiPanen.getHasilPanen().getBrondolan()).contains(etSearch.getText().toString().toLowerCase())
                                    ) {
                                        panenAdd = transaksiPanen;
                                    }
                                } else if (status == STATUS_LONG_OPERATION_NONE) {
                                    panenAdd = transaksiPanen;
                                }

                                if (!filterAfdeling.equals(FilterHelper.No_Filter_Afdeling) && panenAdd != null) {
                                    boolean ada = false;
                                    String[] fafd = filterAfdeling.split(",");
                                    for (int i = 0; i < fafd.length; i++) {
                                        if (fafd[i].equalsIgnoreCase(panenAdd.getTph().getAfdeling())) {
                                            ada = true;
                                            break;
                                        }
                                    }

                                    if (!ada) {
                                        panenAdd = null;
                                    }
                                }

                                if (!filterCreateBy.equals(FilterHelper.No_Filter_User) && panenAdd != null) {
                                    if (!panenAdd.getCreateBy().equals(userId)) {
                                        panenAdd = null;
                                    }
                                }

                                int color = warnaBlok[0];
                                if(blockWarna.size() < 10){
                                    color = warnaBlok[blockWarna.size()];
                                }else if(blockWarna.size() < 20){
                                    color = warnaBlok[blockWarna.size() - 10];
                                }else if(blockWarna.size() < 30){
                                    color = warnaBlok[blockWarna.size() - 20];
                                }

                                if(blockWarna.get(transaksiPanen.getTph().getBlock()) != null) {
                                    color = blockWarna.get(transaksiPanen.getTph().getBlock());
                                }else{
                                    blockWarna.put(transaksiPanen.getTph().getBlock(),color);
                                }

                                if (panenAdd != null) {
                                    addRowTable(panenAdd,subPemanen,color);
                                }

                                listSearch.add(transaksiPanen.getTph().getNamaTph());
                                listSearch.add(transaksiPanen.getTph().getBlock());
                                listSearch.add(subPemanen.getPemanen().getNama());
                                listSearch.add(subPemanen.getPemanen().getNik());
                                listSearch.add(String.valueOf(transaksiPanen.getHasilPanen().getTotalJanjang()));
                                listSearch.add(String.valueOf(transaksiPanen.getHasilPanen().getTotalJanjangPendapatan()));
                                listSearch.add(String.valueOf(transaksiPanen.getHasilPanen().getBrondolan()));
                            }
                        }
                    }
                }
                db.close();
            }
        }
    }

    private void addRowTable(TransaksiPanen transaksiPanen,SubPemanen subPemanen,int color){
        if (mulai == 0) {
            mulai = transaksiPanen.getCreateDate();
        } else if (mulai > transaksiPanen.getCreateDate()) {
            mulai = transaksiPanen.getCreateDate();
        }

        if (sampai < transaksiPanen.getCreateDate()) {
            sampai = transaksiPanen.getCreateDate();
        }

        tampungTph.add(transaksiPanen.getTph().getNoTph());
        if(subPemanen.getPemanen().getGank() == null){
            Log.d(this.getClass().toString(), "addRowTable: "+transaksiPanen.getIdTPanen());
        }
        if(subPemanen.getPemanen().getGank() != null) {
            setGank.add(subPemanen.getPemanen().getGank());
        }
        tampungPemanen.add(subPemanen.getPemanen().getNik());

        tJanjang += subPemanen.getHasilPanen().getTotalJanjang();
        tBrondolan += subPemanen.getHasilPanen().getBrondolan();
        String oph = transaksiPanen.getIdTPanen() + "-" + subPemanen.getOph().substring(subPemanen.getOph().length() - 1);
        origin.put(oph, new ModelTphFragment(oph,transaksiPanen,subPemanen,color));
    }

    private void updateUIRV(int status){
        List<String> lSearch = new ArrayList<>();
        lSearch.addAll(listSearch);
        lSearch = GlobalHelper.removeNullUsingIterator(lSearch);

        ArrayAdapter<String> adapterSearch = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, lSearch);
        etSearch.setAdapter(adapterSearch);
        etSearch.setThreshold(1);
        adapterSearch.notifyDataSetChanged();

        if(status == STATUS_LONG_OPERATION_NONE){
//            etSearch.setText("");
        }

        if (arrayLog.size() > 0) {
            Gson gson = new Gson();
            GlobalHelper.writeFileContent(GlobalHelper.getLogTransaksi(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TRANSAKSI_TPH], sdf.format(dateSelected)), gson.toJson(arrayLog));
        }

        if(origin.size() > 0 ) {
            ArrayList<ModelTphFragment> list = new ArrayList<>(origin.values());

            Collections.sort(list, new Comparator<ModelTphFragment>() {
                @Override
                public int compare(ModelTphFragment o1, ModelTphFragment o2) {
                    Long i1 = o1.getTransaksiPanen().getCreateDate() + o1.getSubPemanen().getNoUrutKongsi();
                    Long i2 = o2.getTransaksiPanen().getCreateDate() + o2.getSubPemanen().getNoUrutKongsi();
                    return i1 > i2 ? -1 : (i1 < i2) ? 1 : 0;
                }
            });

            ltableview.setVisibility(View.VISIBLE);
            mTableViewModel = new TPHTableViewModel(getContext(), list,headerTableTransaksis);
            // Create TableView Adapter
            tphAdapter = new TphAdapter(getContext(), mTableViewModel);
            mTableView.setAdapter(tphAdapter);
            mTableView.setRowHeaderWidth(Utils.convertDpToPx(getActivity(),100));
            mTableView.setTableViewListener(new ITableViewListener() {
                @Override
                public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellDoubleClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {
                        // Create Long Press Popup
                        ColumnHeaderPopup popup = new ColumnHeaderPopup(
                                (ColumnHeaderViewHolder) columnHeaderView, mTableView);
                        // Show
                        popup.show();
                    }
                }

                @Override
                public void onColumnHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    if (rowHeaderView != null && rowHeaderView instanceof TPHRowHeaderViewHolder) {

                        String [] sid = ((TPHRowHeaderViewHolder) rowHeaderView).getCellId().split("_");
                        selectedTransaksiPanen = origin.get(sid[1]).getTransaksiPanen();
                        startLongOperation(STATUS_LONG_OPERATION_SELECT_ITEM);

                    }

                }

                @Override
                public void onRowHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }

                @Override
                public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }
            });
            tphAdapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel
                    .getRowHeaderList(), mTableViewModel.getCellList());


//            mTableFilter = new Filter(mTableView);
        }else{
            ltableview.setVisibility(View.GONE);
        }

        int totalPemanen = 0;
        if(GlobalHelper.dataPemanen != null) {
            for (Pemanen pem : GlobalHelper.dataPemanen.values()) {
                if(setGank.size() > 0) {
                    for (String s : setGank) {
                        if (s.equals(pem.getGank())) {
                            totalPemanen++;
                        }
                    }
                }
            }
        }

        tvStartTime.setText(mulai == 0 ? "" : timeFormat.format(mulai));
        tvFinishTime.setText(sampai == 0 ? "" : timeFormat.format(sampai));
        tvTotalJanjang.setText(String.format("%,d",tJanjang) + "");
        tvTotalBrondolan.setText(String.format("%,d",tBrondolan) + "");
        tvTotalPemanen.setText(String.valueOf(totalPemanen));
        tvPemanenHadir.setText(String.valueOf(tampungPemanen.size()));
        tvTotalTph.setText(String.valueOf(tampungTph.size()));
        tvRestan.setText(String.format("%,d",restan));
        tvUpload.setText(String.valueOf(upload));

        Date d = new Date(System.currentTimeMillis());
        if(sdf.format(d).equals(sdf.format(dateSelected))){
            showNewTranskasi();
        }else{
            llNewData.setVisibility(View.GONE);
        }
    }

    public void startLongOperation(int status){
        new LongOperation().execute(String.valueOf(status));
    }

//    class TphAdapter extends AbstractTableAdapter<TableViewColumnHeader, TableViewRowHeader, TableViewCell> {
//        LayoutInflater mInflater;
//        public TphAdapter(Context context) {
//            super(context);
//            this.mInflater = LayoutInflater.from(mContext);
//        }
//
//        @Override
//        public int getColumnHeaderItemViewType(int position) {
//            return 0;
//        }
//
//        @Override
//        public int getRowHeaderItemViewType(int position) {
//            return 0;
//        }
//
//        @Override
//        public int getCellItemViewType(int position) {
//            return 0;
//        }
//
//        @Override
//        public AbstractViewHolder onCreateCellViewHolder(ViewGroup parent, int viewType) {
//            View layout;
//            // For cells that display a text
//            layout = mInflater.inflate(R.layout.table_view_cell_layout, parent, false);
//
//            // Create a Cell ViewHolder
//            return new TPHCellViewHolder(layout);
//        }
//
//        @Override
//        public void onBindCellViewHolder(AbstractViewHolder holder, Object cellItemModel, int columnPosition, int rowPosition) {
//            TableViewCell cell = (TableViewCell) cellItemModel;
//            TPHCellViewHolder viewHolder = (TPHCellViewHolder) holder;
//            viewHolder.setCell(cell);
//        }
//
//        @Override
//        public AbstractViewHolder onCreateColumnHeaderViewHolder(ViewGroup parent, int viewType) {
//            // TODO: check
//            //Log.e(LOG_TAG, " onCreateColumnHeaderViewHolder has been called");
//            // Get Column Header xml Layout
//            View layout = mInflater.inflate(R.layout.table_view_column_header_layout, parent, false);
//
//            // Create a ColumnHeader ViewHolder
//            return new TPHColumnHeaderViewHolder(layout, getTableView());
//        }
//
//        @Override
//        public void onBindColumnHeaderViewHolder(AbstractViewHolder holder, Object columnHeaderItemModel, int columnPosition) {
//            TableViewColumnHeader columnHeader = (TableViewColumnHeader) columnHeaderItemModel;
//
//            // Get the holder to update cell item text
//            TPHColumnHeaderViewHolder columnHeaderViewHolder = (TPHColumnHeaderViewHolder) holder;
//            columnHeaderViewHolder.setColumnHeader(columnHeader);
//        }
//
//        @Override
//        public AbstractViewHolder onCreateRowHeaderViewHolder(ViewGroup parent, int viewType) {
//            // Get Row Header xml Layout
//            View layout = mInflater.inflate(R.layout.table_view_row_header_layout, parent, false);
//
//            // Create a Row Header ViewHolder
//            return new TPHRowHeaderViewHolder(layout);
//        }
//
//        @Override
//        public void onBindRowHeaderViewHolder(AbstractViewHolder holder, Object rowHeaderItemModel, int rowPosition) {
//            TableViewRowHeader rowHeader = (TableViewRowHeader) rowHeaderItemModel;
//
//            // Get the holder to update row header item text
//            TPHRowHeaderViewHolder rowHeaderViewHolder = (TPHRowHeaderViewHolder) holder;
//            rowHeaderViewHolder.row_header_textview.setText(String.valueOf(rowHeader.getData()));
//        }
//
//        @Override
//        public View onCreateCornerView() {
//            // Get Corner xml layout
//            View corner = mInflater.inflate(R.layout.table_view_corner_layout, null);
//            corner.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    SortState sortState = TphAdapter.this.getTableView()
//                            .getRowHeaderSortingStatus();
//                    if (sortState != SortState.ASCENDING) {
//                        Log.d("TableViewAdapter", "Order Ascending");
//                        TphAdapter.this.getTableView().sortRowHeader(SortState.ASCENDING);
//                    } else {
//                        Log.d("TableViewAdapter", "Order Descending");
//                        TphAdapter.this.getTableView().sortRowHeader(SortState.DESCENDING);
//                    }
//                }
//            });
//            return corner;
//        }
//    }

    private class LongOperation extends AsyncTask<String, Void, String> {
//        private AlertDialog alertDialogAllpoin ;
        String dataString;
        String dataString2;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (getActivity() instanceof BaseActivity && ((BaseActivity) getActivity()).alertDialogBase != null) {
                ((BaseActivity) getActivity()).alertDialogBase.cancel();
            }
            if (getActivity() instanceof BaseActivity ){
                ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(), getResources().getString(R.string.wait));
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])){
                case STATUS_LONG_OPERATION_NONE:{
                    Log.d("TphFragment.java", "10 timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
                    timeScreen = System.currentTimeMillis();

                    getAllUser = GlobalHelper.getAllUser();
                    Log.d("TphFragment.java", "10a timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
                    timeScreen = System.currentTimeMillis();

                    updateDataRv(Integer.parseInt(strings[0]));
                    blockMekanisasi = mekanisasiHelper.setBlockMekanisais();
                    Log.d("TphFragment.java", "10b timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
                    timeScreen = System.currentTimeMillis();

                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    blockMekanisasi = mekanisasiHelper.setBlockMekanisais();
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM:{
                    Gson gson = new Gson();
                    dataString = gson.toJson(selectedTransaksiPanen);

                    NearestBlock nearestBlock = tphHelper.getNearBlock(selectedTransaksiPanen.getLatitude(),selectedTransaksiPanen.getLongitude());
                    if (getActivity() instanceof BaseActivity) {
                        BaseActivity baseActivity = ((BaseActivity) getActivity());
                        if (baseActivity.currentLocationOK()) {
                            nearestBlock.setLocation(baseActivity.currentlocation);
                        }
                    }
                    dataString2 = gson.toJson(nearestBlock);
                    break;
                }
                case STATUS_LONG_OPERATION_CALCULATE_SUMMARY: {
                    Collection<ModelTphFragment> values = origin.values();
                    ArrayList<ModelTphFragment> listOfValues = new ArrayList<ModelTphFragment>(values);
                    summaryTableTransaksis = transaksiPanenHelper.getSummaryTransaksi(headerTableTransaksis, listOfValues);
                    break;
                }
                case STATUS_LONG_OPERATION_NEW_DATA:{
                    if (getActivity() instanceof BaseActivity) {
                        BaseActivity baseActivity = ((BaseActivity) getActivity());
                        if (baseActivity.currentLocationOK()) {
                            Gson gson = new Gson();
                            dataString = gson.toJson(tphHelper.getNearBlock(baseActivity.currentlocation));
                        }
                    }
                    break;
                }
            }
            return strings[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            switch (Integer.parseInt(result)){
                case STATUS_LONG_OPERATION_NONE:{
                    if(getActivity() != null) {
                        updateUIRV(Integer.parseInt(result));
                    }
                    Log.d("TphFragment.java", "11 timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
                    timeScreen = System.currentTimeMillis();
                    if (getActivity() instanceof BaseActivity && ((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateUIRV(Integer.parseInt(result));
                    if (getActivity() instanceof BaseActivity) {
                        if (((BaseActivity) getActivity()).alertDialogBase != null) {
                            ((BaseActivity) getActivity()).alertDialogBase.cancel();
                        }
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM:{
                    Intent intent = new Intent(getActivity(), TphActivity.class);
                    intent.putExtra("transaksiTph",dataString);
                    intent.putExtra("nearestBlock",dataString2);
                    startActivityForResult(intent,GlobalHelper.RESULT_TPHACTIVITY);
                    break;
                }
                case STATUS_LONG_OPERATION_NEW_DATA:{
                    Intent intent = new Intent(getActivity(), TphActivity.class);
                    intent.putExtra("nearestBlock",dataString);
                    intent.putExtra("flgNewHarvest",String.valueOf(flgNewHarvest));
                    startActivityForResult(intent,GlobalHelper.RESULT_TPHACTIVITY);
                    break;
                }
                case STATUS_LONG_OPERATION_CALCULATE_SUMMARY: {
                    modelRecyclerViewHelper.showSummaryTransaksi(summaryTableTransaksis);
                    if (getActivity() instanceof BaseActivity) {
                        if (((BaseActivity) getActivity()).alertDialogBase != null) {
                            ((BaseActivity) getActivity()).alertDialogBase.cancel();
                        }
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_NEW_DATA_MEKANISASI:{
                    Intent intent = new Intent(getActivity(), MekanisasiActivity.class);
                    startActivityForResult(intent, GlobalHelper.RESULT_MEKANISASIACTIVITY);
                    break;
                }
            }

        }
    }
}
