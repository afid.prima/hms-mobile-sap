package id.co.ams_plantation.harvestsap.util;

import android.app.Service;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;

import com.google.gson.Gson;

import net.rehacktive.waspdb.WaspDb;
import net.rehacktive.waspdb.WaspFactory;
import net.rehacktive.waspdb.WaspHash;

import org.apache.commons.io.FileUtils;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.RecordIterable;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import cat.ereza.customactivityoncrash.model.CrashDetailAnalyticInfo;
import id.co.ams_plantation.harvestsap.BuildConfig;
import id.co.ams_plantation.harvestsap.Fragment.AssistensiInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.AssistensiListFragment;
import id.co.ams_plantation.harvestsap.Fragment.ReportListFragment;
import id.co.ams_plantation.harvestsap.Fragment.SettingFragment;
import id.co.ams_plantation.harvestsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestsap.connection.Tag;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.AssistensiRequest;
import id.co.ams_plantation.harvestsap.model.BJRInformation;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.GangList;
import id.co.ams_plantation.harvestsap.model.Gerdang;
import id.co.ams_plantation.harvestsap.model.JanjangSensus;
import id.co.ams_plantation.harvestsap.model.Langsiran;
import id.co.ams_plantation.harvestsap.model.LastSyncTime;
import id.co.ams_plantation.harvestsap.model.OpnameNFC;
import id.co.ams_plantation.harvestsap.model.Pemanen;
import id.co.ams_plantation.harvestsap.model.QcAncak;
import id.co.ams_plantation.harvestsap.model.QcMutuBuah;
import id.co.ams_plantation.harvestsap.model.SensusBjr;
import id.co.ams_plantation.harvestsap.model.SubPemanen;
import id.co.ams_plantation.harvestsap.model.SuperVision;
import id.co.ams_plantation.harvestsap.model.SupervisionList;
import id.co.ams_plantation.harvestsap.model.SyncHistroy;
import id.co.ams_plantation.harvestsap.model.SyncHistroyItem;
import id.co.ams_plantation.harvestsap.model.SyncTime;
import id.co.ams_plantation.harvestsap.model.TPH;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.model.UserAppUsage;
import id.co.ams_plantation.harvestsap.ui.ActiveActivity;
import id.co.ams_plantation.harvestsap.ui.MapActivity;
//import ir.mahdi.mzip.zip.ZipArchive;

import static id.co.ams_plantation.harvestsap.util.GlobalHelper.EXTERNAL_DIR_FILES;
import static id.co.ams_plantation.harvestsap.util.GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2;
import static id.co.ams_plantation.harvestsap.util.GlobalHelper.getDatabasePathHMS;

public class SetUpDataSyncHelper {
    private static final Logger log = LoggerFactory.getLogger(SetUpDataSyncHelper.class);
    Context context;
    String TAG =  this.getClass().getName();

    public SetUpDataSyncHelper(Context context) {
        this.context = context;
    }

    public boolean SetMasterUserByEstate(ServiceResponse responseApi ,Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray jsonArray = new JSONArray(responseApi.getData().toString());

            int  count = 0;
            File fileDb = GlobalHelper.fileDbNitrit(GlobalHelper.TABLE_USER_HARVEST);
            hapusDb(GlobalHelper.TABLE_USER_HARVEST);

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_USER_HARVEST);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            for(int i = 0 ;i < jsonArray.length();i++){
                JSONObject objUserEstate = (JSONObject) jsonArray.get(i);
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", objUserEstate.getString("UserID")));
                DataNitrit dataNitrit = new DataNitrit(objUserEstate.getString("UserID"),String.valueOf(objUserEstate));
                if(cursor.size() ==0) {
                    repository.insert(dataNitrit);
                }

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket",context.getResources().getString(R.string.update_data_user));
                objProgres.put("persen",(count*100/jsonArray.length()));
                objProgres.put("count",count);
                objProgres.put("total",jsonArray.length());
                count++;
                if(longOperation instanceof SettingFragment.LongOperation){
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                }else if(longOperation instanceof ActiveActivity.LongOperation){
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
                //Log.d("SetMasterUserByEstate",String.valueOf(i));
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,jsonArray.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean SetOperatorListByEstateFromTransaksiSpb(ServiceResponse responseApi,Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }

            if(responseApi.getData() != null) {
                JSONObject object = new JSONObject(responseApi.getData().toString());
                JSONArray jsonArray = object.getJSONArray("masterOperator");

                int count = 0;
                hapusDb(GlobalHelper.TABEL_OPERATOR);

                Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_OPERATOR);
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objUserEstate = (JSONObject) jsonArray.get(i);
                    DataNitrit dataNitrit = new DataNitrit(objUserEstate.getString("kodeOperator"), String.valueOf(objUserEstate));
                    repository.insert(dataNitrit);

                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket", context.getResources().getString(R.string.update_data_operator));
                    objProgres.put("persen", jsonArray.length() == 0 ? 0 : (count * 100 / jsonArray.length()));
                    objProgres.put("count", count);
                    objProgres.put("total", jsonArray.length());
                    count++;
                    if (longOperation instanceof SettingFragment.LongOperation) {
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    } else if (longOperation instanceof ActiveActivity.LongOperation) {
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
                db.close();

                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, jsonArray.length());
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
                RestoreMasterHelper.backupTempMasterSAP(GlobalHelper.TABEL_OPERATOR);
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean SetOperatorListByEstate(ServiceResponse responseApi,Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray jsonArray = new JSONArray(responseApi.getData().toString());

            int  count = 0;
            hapusDb(GlobalHelper.TABEL_OPERATOR);

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_OPERATOR);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            for(int i = 0 ;i < jsonArray.length();i++){
                JSONObject objUserEstate = (JSONObject) jsonArray.get(i);
                DataNitrit dataNitrit = new DataNitrit(objUserEstate.getString("kodeOperator"),String.valueOf(objUserEstate));
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket",context.getResources().getString(R.string.update_data_operator));
                objProgres.put("persen",jsonArray.length() == 0 ? 0:(count*100/jsonArray.length()));
                objProgres.put("count",count);
                objProgres.put("total",jsonArray.length());
                count++;
                if(longOperation instanceof SettingFragment.LongOperation){
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                }else if(longOperation instanceof ActiveActivity.LongOperation){
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,jsonArray.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            RestoreMasterHelper.backupTempMasterSAP(GlobalHelper.TABEL_OPERATOR);
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean setVehicleListByEstate(ServiceResponse responseApi,Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray jsonArray = new JSONArray(responseApi.getData().toString());

            int  count = 0;
            hapusDb(GlobalHelper.TABLE_VEHICLE);

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_VEHICLE);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            for(int i = 0 ;i < jsonArray.length();i++){
                JSONObject objUserEstate = (JSONObject) jsonArray.get(i);
                DataNitrit dataNitrit = new DataNitrit(objUserEstate.getString("vraOrderNumber"),String.valueOf(objUserEstate));
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket",context.getResources().getString(R.string.update_data_vehicle));
                objProgres.put("persen",jsonArray.length() == 0 ? 0:(count*100/jsonArray.length()));
                objProgres.put("count",count);
                objProgres.put("total",jsonArray.length());
                count++;
                if(longOperation instanceof SettingFragment.LongOperation){
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                }else if(longOperation instanceof ActiveActivity.LongOperation){
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,jsonArray.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            RestoreMasterHelper.backupTempMasterSAP(GlobalHelper.TABLE_VEHICLE);
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean SetPemanenListByEstate(ServiceResponse responseApi,Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray jsonArray = new JSONArray(responseApi.getData().toString());

            int  count = 0;
            hapusDb(GlobalHelper.TABEL_PEMANEN);

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_PEMANEN);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            for(int i = 0 ;i < jsonArray.length();i++){
                JSONObject objUserEstate = (JSONObject) jsonArray.get(i);
                DataNitrit dataNitrit = new DataNitrit(objUserEstate.getString("kodePemanen"),String.valueOf(objUserEstate));
                Log.d(TAG, "SetPemanenListByEstate: "+ objUserEstate.getString("kodePemanen"));
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket",context.getResources().getString(R.string.update_data_pemanen));
                objProgres.put("persen",jsonArray.length() == 0 ? 0:(count*100/jsonArray.length()));
                objProgres.put("count",count);
                objProgres.put("total",jsonArray.length());
                count++;
                if(longOperation instanceof SettingFragment.LongOperation){
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                }else if(longOperation instanceof ActiveActivity.LongOperation){
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }

            Gson gson = new Gson();
            JSONObject objUserEstate = new JSONObject(gson.toJson(SubPemanen.pemanenSPK));
            DataNitrit dataNitrit = new DataNitrit(objUserEstate.getString("kodePemanen"),String.valueOf(objUserEstate));
            repository.insert(dataNitrit);

            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,jsonArray.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            RestoreMasterHelper.backupTempMasterSAP(GlobalHelper.TABEL_PEMANEN);
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean SetPemanenListByEstateFromTransaksiPanen(ServiceResponse responseApi,Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }

            if(responseApi.getData() != null) {
                JSONObject object = new JSONObject(responseApi.getData().toString());
                JSONArray jsonArray = object.getJSONArray("masterPemanen");

                int count = 0;
                hapusDb(GlobalHelper.TABEL_PEMANEN);

                Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_PEMANEN);
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objUserEstate = (JSONObject) jsonArray.get(i);
                    DataNitrit dataNitrit = new DataNitrit(objUserEstate.getString("kodePemanen"), String.valueOf(objUserEstate));
                    repository.insert(dataNitrit);

                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket", context.getResources().getString(R.string.update_data_pemanen));
                    objProgres.put("persen", jsonArray.length() == 0 ? 0 : (count * 100 / jsonArray.length()));
                    objProgres.put("count", count);
                    objProgres.put("total", jsonArray.length());
                    count++;
                    if (longOperation instanceof SettingFragment.LongOperation) {
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    } else if (longOperation instanceof ActiveActivity.LongOperation) {
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
                Gson gson = new Gson();
                JSONObject objUserEstate = new JSONObject(gson.toJson(SubPemanen.pemanenSPK));
                DataNitrit dataNitrit = new DataNitrit(objUserEstate.getString("kodePemanen"), String.valueOf(objUserEstate));
                repository.insert(dataNitrit);
                db.close();

                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, jsonArray.length());
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
                RestoreMasterHelper.backupTempMasterSAP(GlobalHelper.TABEL_PEMANEN);
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean SetPksListByEstate(ServiceResponse responseApi,Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray jsonArray = new JSONArray(responseApi.getData().toString());

            int  count = 0;
            hapusDb(GlobalHelper.TABLE_PKS);

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_PKS);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            for(int i = 0 ;i < jsonArray.length();i++){
                JSONObject objUserEstate = (JSONObject) jsonArray.get(i);
                if(objUserEstate.has("idPKS")) {
                    DataNitrit dataNitrit = new DataNitrit(objUserEstate.getString("idPKS"), String.valueOf(objUserEstate));
                    repository.insert(dataNitrit);
                }

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket",context.getResources().getString(R.string.update_data_pks));
                objProgres.put("persen",jsonArray.length() == 0 ? 0:(count*100/jsonArray.length()));
                objProgres.put("count",count);
                objProgres.put("total",jsonArray.length());
                count++;
                if(longOperation instanceof SettingFragment.LongOperation){
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                }else if(longOperation instanceof ActiveActivity.LongOperation){
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,jsonArray.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public JSONObject UploadPemanenListByEstate(boolean withProgress,Object longOperation){
        try {
            JSONArray dataPemanen = new JSONArray();
            int count = 1;
            int uploadCount = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_PEMANEN);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(),0);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                JSONObject objPemanen  = new JSONObject(dataNitrit.getValueDataNitrit());
                Gson gson = new Gson();
                Pemanen pemanen = gson.fromJson(dataNitrit.getValueDataNitrit(),Pemanen.class);
                if(pemanen.getStatus() == Pemanen.Pemanen_NotUplaod){
                    objPemanen.put("idx",uploadCount);
                    objPemanen.put("userId",GlobalHelper.getUser().getUserID());
                    objPemanen.put("versionApp",pi.versionName);
                    dataPemanen.put(objPemanen);
                    uploadCount++;
                }

                if(withProgress){

                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_pemanen));
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof MapActivity.LongOperation){
                        ((MapActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();
            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataPemanen);
//            Log.d("objReturn Tph", String.valueOf(objReturn));
            return objReturn;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadTph(boolean withProgress, Object longOperation){
        try {
            JSONArray dataTph = new JSONArray();
            JSONArray dataTphImages = new JSONArray();
//            File dirTPH = new File(GlobalHelper.getDatabasePathHMS() + "/" +Encrypts.encrypt(GlobalHelper.TABEL_TPH));
            int count = 1;
            int uploadCount = 0;

            Set<String> idsHasUpload = GlobalHelper.getIdUpload(GlobalHelper.LIST_FOLDER_TPH);
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                JSONObject objTph = new JSONObject(dataNitrit.getValueDataNitrit());
                boolean masuk = true;
                if(objTph.has("reasonUnharvestTPH")){
                    Log.d(TAG, "UploadTph: :"+objTph.getJSONObject("reasonUnharvestTPH").toString() );
                }
                if (objTph.getInt("status") == TPH.STATUS_TPH_BELUM_UPLOAD || objTph.getInt("status") == TPH.STATUS_TPH_INACTIVE_QC_MOBILE
                    || objTph.getInt("status") == TPH.STATUS_TPH_DELETED || objTph.has("reasonUnharvestTPH")
                ) {

                    if (idsHasUpload.size() > 0) {
                       for (String s : idsHasUpload) {
                           if (s.equals(objTph.getString("noTph"))) {
                               masuk = false;
                               break;
                           }
                       }
                    }
                    if(objTph.getString("block").contains(",")){
                        String [] blok = objTph.getString("block").split(",");
                        objTph.put("block",blok[0]);
                    }

                    if (masuk) {
                       objTph.put("idx", uploadCount);
                       dataTph.put(objTph);
                       uploadCount++;
                    }
                }

                if(withProgress){
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_tph));
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof MapActivity.LongOperation){
                        ((MapActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }

            db.close();
            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataTph);
            return objReturn;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadLangsiran(boolean withProgress, Object longOperation){
        try {
            JSONArray dataLangsiran = new JSONArray();
            int count = 1;
            int uploadCount = 0;

            Set<String> idsHasUpload = GlobalHelper.getIdUpload(GlobalHelper.LIST_FOLDER_LANGSIRAN);
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_LANGSIRAN);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                JSONObject objLangsiran = new JSONObject(dataNitrit.getValueDataNitrit());
                if(objLangsiran.getInt("status") == Langsiran.STATUS_LANGSIRAN_BELUM_UPLOAD || objLangsiran.getInt("status") == Langsiran.STATUS_LANGSIRAN_INACTIVE_QC_MOBILE){
                    boolean masuk = true;
                    if(idsHasUpload.size() > 0) {
                        for (String s : idsHasUpload) {
                            if (s.equals(objLangsiran.getString("idTujuan"))) {
                                masuk = false;
                                break;
                            }
                        }
                    }
                    if(masuk) {
                        objLangsiran.put("idx",uploadCount);
                        dataLangsiran.put(objLangsiran);
                        uploadCount ++;
                    }
                }
                if(withProgress){
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_langsiran));
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof MapActivity.LongOperation){
                        ((MapActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataLangsiran);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadTransaksiPanen(boolean withProgress, Object longOperation){
        try {
            String idSpv = "0";
            if(longOperation instanceof SettingFragment.LongOperation){
                SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.SUPERVISION, Context.MODE_PRIVATE);
                Gson gson= new Gson();
                String stringSuperVision = preferences.getString(HarvestApp.SUPERVISION,null);
                if(stringSuperVision != null) {
                    SuperVision shareSuperVision  = gson.fromJson(stringSuperVision, SuperVision.class);
                    idSpv = shareSuperVision.getIdSupervision();
                }
            }

            JSONArray dataPanen = new JSONArray();

            int count = 1;
            int uploadCount = 0;

            ArrayList<String> allDb = GlobalHelper.getAllFileDB(GlobalHelper.TABLE_TRANSAKSI_TPH,true);
            for(String dbFile : allDb) {
                Nitrite db = GlobalHelper.getTableNitritFullPath(dbFile);
                if (db != null) {
                    ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                    Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
                    for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                        DataNitrit dataNitrit = (DataNitrit) iterator.next();
                        Gson gson = new Gson();
                        TransaksiPanen transaksiPanen = gson.fromJson(dataNitrit.getValueDataNitrit(), TransaksiPanen.class);
                        if (transaksiPanen.getStatus() == TransaksiPanen.SAVE_ONLY || transaksiPanen.getStatus() == TransaksiPanen.TAP || transaksiPanen.getStatus() == TransaksiPanen.DELETED) {
                            JSONObject objPanen = new JSONObject(dataNitrit.getValueDataNitrit());
                            if (idSpv != null) {
                                objPanen.put("idTransactionSupervision", idSpv);
                            }
                            objPanen.put("idx", uploadCount);
                            dataPanen.put(objPanen);
                            uploadCount++;
                        }

                        if (withProgress) {
                            JSONObject objProgres = new JSONObject();
                            objProgres.put("ket", context.getResources().getString(R.string.setup_data_TransaksiPanen));
                            objProgres.put("persen", (count * 100 / ((RecordIterable<DataNitrit>) Iterable).size()));
                            objProgres.put("count", count);
                            objProgres.put("total", ((RecordIterable<DataNitrit>) Iterable).size());
                            count++;
                            if (longOperation instanceof SettingFragment.LongOperation) {
                                ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                            } else if (longOperation instanceof ActiveActivity.LongOperation) {
                                ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                            }
                        }
                    }
                    db.close();
                }
            }

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataPanen);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadTransaksiPanenNfc(boolean withProgress, Object longOperation){
        try {
            JSONArray dataPanen = new JSONArray();

            int count = 1;
            int uploadCount = 0;

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_TPH_NFC);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                if(dataNitrit.getParam1() == null) {
                    JSONObject objPanen = new JSONObject(dataNitrit.getValueDataNitrit());
                    objPanen.put("idx", uploadCount);
                    dataPanen.put(objPanen);
                    uploadCount++;
                }

                if(withProgress){
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_TransaksiPanen_NFC));
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataPanen);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadTransaksiAngkut(boolean withProgress, Object longOperation){
        try {
            JSONArray dataTAngkut = new JSONArray();

            int count = 1;
            int uploadCount = 0;

            Set<String> idsHasUpload = GlobalHelper.getIdUpload(GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT);
            ArrayList<String> allDb = GlobalHelper.getAllFileDB(GlobalHelper.TABLE_TRANSAKSI_ANGKUT, true);
            for (String dbFile : allDb) {
                Nitrite db = GlobalHelper.getTableNitritFullPath(dbFile);
                if (db != null) {
                    ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                    Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
                    for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                        DataNitrit dataNitrit = (DataNitrit) iterator.next();
                        Gson gson = new Gson();
                        TransaksiAngkut transaksiAngkut = gson.fromJson(dataNitrit.getValueDataNitrit(), TransaksiAngkut.class);
                        if (transaksiAngkut.getStatus() == TransaksiAngkut.TAP || transaksiAngkut.getStatus() == TransaksiAngkut.HAPUS) {
                            JSONObject objTAngkut = new JSONObject(dataNitrit.getValueDataNitrit());
                            boolean masuk = true;
                            if (idsHasUpload.size() > 0) {
                                for (String s : idsHasUpload) {
                                    if (s.equals(transaksiAngkut.getIdTAngkut())) {
                                        masuk = false;
                                        break;
                                    }
                                }
                            }
                            if (masuk) {
                                objTAngkut.put("idx", uploadCount);
                                dataTAngkut.put(objTAngkut);
                                uploadCount++;
                            }
                        }

                        if (withProgress) {
                            JSONObject objProgres = new JSONObject();
                            objProgres.put("ket", context.getResources().getString(R.string.setup_data_TransaksiAngkut));
                            objProgres.put("persen", (count * 100 / ((RecordIterable<DataNitrit>) Iterable).size()));
                            objProgres.put("count", count);
                            objProgres.put("total", ((RecordIterable<DataNitrit>) Iterable).size());
                            count++;
                            if (longOperation instanceof SettingFragment.LongOperation) {
                                ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                            } else if (longOperation instanceof ActiveActivity.LongOperation) {
                                ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                            }
                        }
                    }
                    db.close();
                }
            }

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataTAngkut);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


    public JSONObject UploadTransaksiAngkutNfc(boolean withProgress, Object longOperation){
        try {
            JSONArray dataTAngkut = new JSONArray();

            int count = 1;
            int uploadCount = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT_NFC);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                if(dataNitrit.getParam1() == null) {
                    JSONObject objTAngkut = new JSONObject(dataNitrit.getValueDataNitrit());
                    objTAngkut.put("idx", uploadCount);
                    dataTAngkut.put(objTAngkut);
                    uploadCount++;
                }

                if(withProgress){
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_TransaksiAngkut_NFC));
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataTAngkut);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadTransaksiSpb(boolean withProgress, Object longOperation){
        try {
            JSONArray dataTSpb = new JSONArray();

            int count = 1;
            int uploadCount = 0;

            Set<String> idsHasUpload = GlobalHelper.getIdUpload(GlobalHelper.LIST_FOLDER_SPB);
            ArrayList<String> allDb = GlobalHelper.getAllFileDB(GlobalHelper.TABLE_TRANSAKSI_SPB, true);
            for (String dbFile : allDb) {
                Nitrite db = GlobalHelper.getTableNitritFullPath(dbFile);
                if (db != null) {
                    ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                    Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
                    for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                        DataNitrit dataNitrit = (DataNitrit) iterator.next();
                        Gson gson = new Gson();
                        TransaksiSPB transaksiSPB = gson.fromJson(dataNitrit.getValueDataNitrit(), TransaksiSPB.class);
                        if (transaksiSPB.getStatus() == TransaksiSPB.BELUM_UPLOAD || transaksiSPB.getStatus() == TransaksiSPB.HAPUS) {

                            boolean masuk = true;
                            if (idsHasUpload.size() > 0) {
                                for (String s : idsHasUpload) {
                                    if (s.equals(transaksiSPB.getIdSPB())) {
                                        masuk = false;
                                        break;
                                    }
                                }
                            }
                            if (masuk) {
//                                if(transaksiSPB.getTransaksiAngkut().getIdSPB() == null){
//                                    transaksiSPB.getTransaksiAngkut().setIdSPB(transaksiSPB.getIdSPB());
//                                }
//
//                                if(transaksiSPB.getTransaksiAngkut().getApproveBy() == null) {
//                                    transaksiSPB.getTransaksiAngkut().setApproveBy(transaksiSPB.getApproveBy());
//                                }

                                JSONObject objTAngkut = new JSONObject(dataNitrit.getValueDataNitrit());
//                                JSONObject objTAngkut = new JSONObject(gson.toJson(transaksiSPB));
                                objTAngkut.put("idx", uploadCount);
                                dataTSpb.put(objTAngkut);
                                uploadCount++;
                            }
                        }

                        if (withProgress) {
                            JSONObject objProgres = new JSONObject();
                            objProgres.put("ket", context.getResources().getString(R.string.setup_data_TransaksiSPB));
                            objProgres.put("persen", (count * 100 / ((RecordIterable<DataNitrit>) Iterable).size()));
                            objProgres.put("count", count);
                            objProgres.put("total", ((RecordIterable<DataNitrit>) Iterable).size());
                            count++;
                            if (longOperation instanceof SettingFragment.LongOperation) {
                                ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                            } else if (longOperation instanceof ActiveActivity.LongOperation) {
                                ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                            }
                        }
                    }
                    db.close();
                }
            }

            if(dataTSpb.length() == 0){

                count = 1;
                uploadCount = 0;

                ArrayList<String> allJSON = GlobalHelper.getAllPathJSONBackup(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SPB]);
                for(int i = 0 ; i < allJSON.size(); i++){
                    File file = new File(allJSON.get(i));
                    String sJson = GlobalHelper.readFileContent(file.getPath());
                    JSONObject objTAngkut = new JSONObject(sJson);
                    objTAngkut.put("idx", uploadCount);
                    dataTSpb.put(objTAngkut);
                    uploadCount++;

                    if (withProgress) {
                        JSONObject objProgres = new JSONObject();
                        objProgres.put("ket", context.getResources().getString(R.string.setup_data_TransaksiSPB));
                        objProgres.put("persen", (count * 100 / allJSON.size()));
                        objProgres.put("count", count);
                        objProgres.put("total", allJSON.size());
                        count++;
                        if (longOperation instanceof SettingFragment.LongOperation) {
                            ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                        } else if (longOperation instanceof ActiveActivity.LongOperation) {
                            ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                        }
                    }
                }
            }

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataTSpb);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadQcAncak(boolean withProgress, Object longOperation){
        try {
            JSONArray dataQcAncak = new JSONArray();

            int count = 1;
            int uploadCount = 0;

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_ANCAK);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                QcAncak qcAncak = gson.fromJson(dataNitrit.getValueDataNitrit(), QcAncak.class);
                if (qcAncak.getStatus() == QcAncak.Save) {

                    JSONObject obj= new JSONObject(dataNitrit.getValueDataNitrit());

                    JSONObject objTph = obj.getJSONObject("tph");
                    objTph.remove("baris");

                    obj.put("tph",objTph);
                    obj.put("idx",uploadCount);
                    dataQcAncak.put(obj);
                    uploadCount++;

                }

                if(withProgress){
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_Qc_Ancak));
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataQcAncak);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadQcBuah(boolean withProgress, Object longOperation){
        try {
            JSONArray dataPanen = new JSONArray();
            JSONArray dataImagePanen = new JSONArray();

            int count = 1;
            int uploadCount = 0;

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BUAH);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                QcMutuBuah qcMutuBuah = gson.fromJson(dataNitrit.getValueDataNitrit(), QcMutuBuah.class);
                TransaksiPanen transaksiPanen = qcMutuBuah.getTransaksiPanenQc();
                if (transaksiPanen.getStatus() == TransaksiPanen.SAVE_ONLY || transaksiPanen.getStatus() == TransaksiPanen.TAP) {
                    JSONObject objPanen= new JSONObject(dataNitrit.getValueDataNitrit());
                    objPanen.put("idx",uploadCount);
                    dataPanen.put(objPanen);
                    uploadCount++;
                }

                if(withProgress){
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_TransaksiPanen));
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataPanen);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadSensusBjr(boolean withProgress, Object longOperation){
        try {
            JSONArray dataBjr = new JSONArray();

            int count = 1;
            int uploadCount = 0;

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BJR);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                SensusBjr sensusBjr = gson.fromJson(dataNitrit.getValueDataNitrit(),SensusBjr.class);
                if (sensusBjr.getStatus() == SensusBjr.SAVE) {
                    JSONArray allBerat = new JSONArray();
                    for(int i = 0; i < sensusBjr.getBeratJanjang().size();i++){
                        int idx = i + 1;
                        allBerat.put(i,sensusBjr.getBeratJanjang().get(idx).getJanjang());
                    }

                    JSONObject objBjr= new JSONObject(dataNitrit.getValueDataNitrit());
                    objBjr.put("idx",uploadCount);
                    objBjr.put("beratJanjang",allBerat);
                    dataBjr.put(objBjr);
                    uploadCount++;
                }

                if(withProgress){
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_Sensus_Bjr));
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataBjr);
//            Log.d("objReturn Panen", String.valueOf(objReturn));
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadOpnameNFC(boolean withProgress, Object longOperation){
        try {
            JSONArray dataNfc = new JSONArray();

            int count = 1;
            int uploadCount = 0;

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_OPNAME_NFC);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                OpnameNFC opnameNFC = gson.fromJson(dataNitrit.getValueDataNitrit(),OpnameNFC.class);
                if (opnameNFC.getStatus() == OpnameNFC.SAVE) {
                    JSONObject objNfc= new JSONObject(dataNitrit.getValueDataNitrit());
                    objNfc.put("idx",uploadCount);
                    dataNfc.put(objNfc);
                    uploadCount++;
                }

                if(withProgress){
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_Sensus_Nfc));
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataNfc);

            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean SetOpnameNFCByEstate(ServiceResponse responseApi,Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray jsonArray = new JSONArray(responseApi.getData().toString());

            int  count = 0;
            hapusDb(GlobalHelper.TABLE_OPNAME_NFC);

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_OPNAME_NFC);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            for(int i = 0 ;i < jsonArray.length();i++){
                JSONObject objUserEstate = (JSONObject) jsonArray.get(i);
                DataNitrit dataNitrit = new DataNitrit(objUserEstate.getString("uid"),String.valueOf(objUserEstate));
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket",context.getResources().getString(R.string.update_data_opname_nfc));
                objProgres.put("persen",jsonArray.length() == 0 ? 0:(count*100/jsonArray.length()));
                objProgres.put("count",count);
                objProgres.put("total",jsonArray.length());
                count++;
                if(longOperation instanceof SettingFragment.LongOperation){
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                }else if(longOperation instanceof ActiveActivity.LongOperation){
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, jsonArray.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public JSONObject UploadGerdangDetail(boolean withProgress, Object longOperation){
        try {
            JSONArray dataNfc = new JSONArray();

            int count = 1;
            int uploadCount = 0;

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_GERDANG);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                Gerdang gerdang = gson.fromJson(dataNitrit.getValueDataNitrit(),Gerdang.class);
                if (gerdang.getStatus() == Gerdang.Gerdang_NotUplaod) {
                    JSONObject objNfc= new JSONObject(dataNitrit.getValueDataNitrit());
                    objNfc.put("idx",uploadCount);
                    dataNfc.put(objNfc);
                    uploadCount++;
                }

                if(withProgress){
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_Gerdang_Detail));
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataNfc);

            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean SetConfigGerdangByEstate(ServiceResponse responseApi,Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray jsonArray = new JSONArray(responseApi.getData().toString());

            int  count = 0;
            hapusDb(GlobalHelper.TABLE_CONFIG_GERDANG);

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_CONFIG_GERDANG);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            for(int i = 0 ;i < jsonArray.length();i++){
                JSONObject objUserEstate = (JSONObject) jsonArray.get(i);
                DataNitrit dataNitrit = new DataNitrit(objUserEstate.getString("estCode"),String.valueOf(objUserEstate));
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket",context.getResources().getString(R.string.update_data_gerdang));
                objProgres.put("persen",jsonArray.length() == 0 ? 0:(count*100/jsonArray.length()));
                objProgres.put("count",count);
                objProgres.put("total",jsonArray.length());
                count++;
                if(longOperation instanceof SettingFragment.LongOperation){
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                }else if(longOperation instanceof ActiveActivity.LongOperation){
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, jsonArray.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean SetTPHReasonUnharvestMaster(ServiceResponse responseApi,Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray jsonArray = new JSONArray(responseApi.getData().toString());

            int  count = 0;
            hapusDb(GlobalHelper.TABLE_REASON_UNHARVEST);

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_REASON_UNHARVEST);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            for(int i = 0 ;i < jsonArray.length();i++){
                JSONObject objUserEstate = (JSONObject) jsonArray.get(i);
                DataNitrit dataNitrit = new DataNitrit(String.valueOf(objUserEstate.getInt("actionReasonId")),String.valueOf(objUserEstate));
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket",context.getResources().getString(R.string.update_data_reason_unharvest));
                objProgres.put("persen",jsonArray.length() == 0 ? 0:(count*100/jsonArray.length()));
                objProgres.put("count",count);
                objProgres.put("total",jsonArray.length());
                count++;
                if(longOperation instanceof SettingFragment.LongOperation){
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                }else if(longOperation instanceof ActiveActivity.LongOperation){
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, jsonArray.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean SetBlockMekanisasiMaster(ServiceResponse responseApi,Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray jsonArrayX = new JSONArray(responseApi.getData().toString());
            JSONObject jsonObject = jsonArrayX.getJSONObject(0);
            JSONArray jsonArray = jsonObject.getJSONArray("block");

            int  count = 0;
            hapusDb(GlobalHelper.TABLE_BLOCK_MEKANISASI);

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_BLOCK_MEKANISASI);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            for(int i = 0 ;i < jsonArray.length();i++){
                String string = jsonArray.getString(i);
                DataNitrit dataNitrit = new DataNitrit(string,string);
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket",context.getResources().getString(R.string.update_data_block_mekanisasi));
                objProgres.put("persen",jsonArray.length() == 0 ? 0:(count*100/jsonArray.length()));
                objProgres.put("count",count);
                objProgres.put("total",jsonArray.length());
                count++;
                if(longOperation instanceof SettingFragment.LongOperation){
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                }else if(longOperation instanceof ActiveActivity.LongOperation){
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, jsonArray.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public JSONObject UploadLogSPB(boolean withProgress, Object longOperation){
        try {
            JSONArray dataLogs = new JSONArray();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            int count = 1;
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(),0);
            File dir = new File(Environment.getExternalStorageDirectory() +EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SPB]);
            if(dir.isDirectory()){
                for(File file : dir.listFiles()){
                    if (file.getName().toLowerCase().contains(".json") && file.getName().toLowerCase().contains("log_")) {
                        JSONObject obj = new JSONObject();
                        obj.put("data" , GlobalHelper.readFileContent(file.getAbsolutePath()));
                        obj.put("namaFile" , GlobalHelper.getUser().getUserID() +"-" + System.currentTimeMillis() + "-"+file.getName());
                        obj.put("fullPath" , file.getAbsolutePath());
                        obj.put("userId" , GlobalHelper.getUser().getUserID());
                        obj.put("versionApp" , pi.versionName);
                        dataLogs.put(obj);
                    }

                    if(withProgress){
                        JSONObject objProgres = new JSONObject();
                        objProgres.put("ket",context.getResources().getString(R.string.setup_data_Log_SPB));
                        objProgres.put("persen",(count*100/dir.length()));
                        objProgres.put("count",count);
                        objProgres.put("total",(dir.length()));
                        count++;
                        if(longOperation instanceof SettingFragment.LongOperation){
                            ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                        }else if(longOperation instanceof ActiveActivity.LongOperation){
                            ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                        }
                    }
                }
            }

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataLogs);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public JSONObject UploadCrashReport(boolean withProgress, Object longOperation){
        try {
            JSONArray dataCrash = new JSONArray();

            int count = 1;
            int uploadCount = 0;
            WaspDb db = WaspFactory.openOrCreateDatabase(cat.ereza.customactivityoncrash.helper.GlobalHelper.getDatabasePath(),
                    cat.ereza.customactivityoncrash.helper.GlobalHelper.DB_MASTER,
                    cat.ereza.customactivityoncrash.helper.GlobalHelper.getDatabasePass());
            WaspHash crashTbl = db.openOrCreateHash(cat.ereza.customactivityoncrash.helper.GlobalHelper.CrashReport);
            if(crashTbl.getAllValues().size() > 0) {
                for (Object obj : crashTbl.getAllValues()) {
                    CrashDetailAnalyticInfo model = (CrashDetailAnalyticInfo) obj;
                    Gson gson = new Gson();
                    PackageManager manager = HarvestApp.getContext().getPackageManager();
                    PackageInfo info = manager.getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES);

                    JSONObject object = new JSONObject(gson.toJson(model));
                    object.put("VersionCode",info.versionCode );
                    object.put("VersionName",info.versionName);
                    dataCrash.put(object);

                    if(withProgress){
                        JSONObject objProgres = new JSONObject();
                        objProgres.put("ket",context.getResources().getString(R.string.upload_crash));
                        objProgres.put("persen",(count*100/crashTbl.getAllValues().size()));
                        objProgres.put("count",count);
                        objProgres.put("total",(crashTbl.getAllValues().size()));
                        count++;
                        if(longOperation instanceof SettingFragment.LongOperation){
                            ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                        }else if(longOperation instanceof ActiveActivity.LongOperation){
                            ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                        }
                    }
                }
            }

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",dataCrash);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadLogActivity(boolean withProgress, Object longOperation){
        try {
            JSONArray data = new JSONArray();
            int count = 1;
            int uploadCount = 0;

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ContainerLogActivity);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                JSONObject obj = new JSONObject(dataNitrit.getValueDataNitrit());
                obj.put("idx",uploadCount);
                data.put(obj);
                uploadCount++;

                if(withProgress){
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_UAL));
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",data);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject UploadAssistanceRequest(boolean withProgress, Object longOperation){
        try {
            JSONArray data = new JSONArray();
            int count = 1;
            int uploadCount = 0;

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_REQUEST_ASSISTENSI);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                AssistensiRequest assistensiRequest = gson.fromJson(dataNitrit.getValueDataNitrit(),AssistensiRequest.class);
                if (assistensiRequest.getStatus() < AssistensiRequest.PENDING) {

                    JSONObject obj = new JSONObject(dataNitrit.getValueDataNitrit());
                    obj.put("idx",uploadCount);
                    data.put(obj);
                    uploadCount++;
                }

                if(withProgress){
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_Sensus_Bjr));
                    objProgres.put("persen",(count*100/((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count",count);
                    objProgres.put("total",((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if(longOperation instanceof SettingFragment.LongOperation){
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }else if(longOperation instanceof ActiveActivity.LongOperation){
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();

            JSONObject objReturn = new JSONObject();
            objReturn.put("data",data);
            return objReturn;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<GangList> getGangSupervision(boolean withProgress, Object longOperation){
        try {
            int count = 1;
            ArrayList<GangList> gangLists = new ArrayList<>();

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TPANENGANGLIST);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                GangList item = gson.fromJson(dataNitrit.getValueDataNitrit(), GangList.class);
                gangLists.add(item);

                if (withProgress) {
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket", context.getResources().getString(R.string.setup_data_gang_supervision));
                    objProgres.put("persen", (count * 100 / ((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count", count);
                    objProgres.put("total", ((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if (longOperation instanceof SettingFragment.LongOperation) {
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();
            return gangLists;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<SupervisionList> getSupervision(boolean withProgress, Object longOperation){
        try {
            int count = 1;
            ArrayList<SupervisionList> supervisionListsAll = new ArrayList<>();
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TPANENSUPERVISIONLIST);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                SupervisionList item = gson.fromJson(dataNitrit.getValueDataNitrit(), SupervisionList.class);

                supervisionListsAll.add(item);

                if (withProgress) {
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket", context.getResources().getString(R.string.setup_data_supervision));
                    objProgres.put("persen", (count * 100 / ((RecordIterable<DataNitrit>) Iterable).size()));
                    objProgres.put("count", count);
                    objProgres.put("total", ((RecordIterable<DataNitrit>) Iterable).size());
                    count++;
                    if (longOperation instanceof SettingFragment.LongOperation) {
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }
                }
            }
            db.close();
            return supervisionListsAll;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


//    public JSONObject UploadCounter(boolean withProgress, Object longOperation){
//        try {
//            JSONObject objReturn = new JSONObject();
//            int count = 1;
//            for (int i = 0 ; i < GlobalHelper.LIST_FOLDER.length; i++) {
//                File file = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[i] + "/" + "COUNT", GlobalHelper.getUser().getUserID());
//                if(file.exists()){
//                    String value = GlobalHelper.readFileContent(file.getPath());
//                    objReturn.put(GlobalHelper.LIST_FOLDER[i],value);
//                }
//                if(withProgress) {
//                    JSONObject objProgres = new JSONObject();
//                    objProgres.put("ket",context.getResources().getString(R.string.setup_data_Counter));
//                    objProgres.put("persen",(count*100/GlobalHelper.LIST_FOLDER.length));
//                    objProgres.put("count",count);
//                    objProgres.put("total",(GlobalHelper.LIST_FOLDER.length));
//                    count++;
//                    if(longOperation instanceof SettingFragment.LongOperation){
//                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
//                    }else if(longOperation instanceof ActiveActivity.LongOperation){
//                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
//                    }
//                }
//            }
//            objReturn.put("USER_ID",GlobalHelper.getUser().getUserID());
//            SimpleDateFormat sdfDate = new SimpleDateFormat("ddMMyy");
//            objReturn.put("DATE",sdfDate.format(System.currentTimeMillis()));
//            Log.d("objReturn Counter", String.valueOf(objReturn));
//            return objReturn;
//        } catch (JSONException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }

    public boolean downLoadTph(ServiceResponse responseApi, Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());

            hapusDb(GlobalHelper.TABEL_TPH);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objTph = (JSONObject) data.get(i);
                DataNitrit dataNitrit = new DataNitrit(objTph.getString("noTph"),
                        objTph.toString(),
                        objTph.getString("block"),
                        objTph.getString("afdeling"),
                        objTph.get("createBy").toString());
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.update_data_tph));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof MapActivity.LongOperation) {
                    ((MapActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,data.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            RestoreMasterHelper.backupTempMasterSAP(GlobalHelper.TABEL_TPH);
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTph", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadLangsiran(ServiceResponse responseApi, Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(GlobalHelper.TABLE_LANGSIRAN);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_LANGSIRAN);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objLangsiran = (JSONObject) data.get(i);
                DataNitrit dataNitrit = new DataNitrit(objLangsiran.getString("idTujuan"), objLangsiran.toString());
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.update_data_langsiran));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof MapActivity.LongOperation) {
                    ((MapActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,data.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            RestoreMasterHelper.backupTempMasterSAP(GlobalHelper.TABLE_LANGSIRAN);
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadLangsiran", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadTransaksiPanen(ServiceResponse responseApi, Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());

            hapusDb(GlobalHelper.TABLE_TRANSAKSI_TPH);
            hapusDb(GlobalHelper.TABLE_TRANSAKSI_TPH_NFC);
            hapusDb(GlobalHelper.TABLE_GERDANG);
            hapusDb(GlobalHelper.TABLE_OPERATOR_MEKANISASI);

            HashMap<String, Gerdang> listGerdang = new HashMap<>();

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_TPH);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objTransaksiPanen = (JSONObject) data.get(i);
                Log.d(TAG, "downLoadTransaksiPanen: "+objTransaksiPanen.toString());
                JSONObject objTph = objTransaksiPanen.getJSONObject("tph");
                JSONObject objPemanen = null;
                if(objTransaksiPanen.has("pemanen")){
                    objPemanen = objTransaksiPanen.getJSONObject("pemanen");
                }else{
                    JSONObject objKongsi = objTransaksiPanen.getJSONObject("kongsi");
                    JSONArray jaSubPemanens = objKongsi.getJSONArray("subPemanens");
                    for(int j = 0 ; j < jaSubPemanens.length(); j++){
                        JSONObject objSubPemanens = (JSONObject) jaSubPemanens.get(j);
                        if(objSubPemanens.getString("oph").toUpperCase().contains("-A")){
                            objPemanen = objSubPemanens.getJSONObject("pemanen");
                        }
                    }
                }
                DataNitrit dataNitrit = new DataNitrit(objTransaksiPanen.getString("idTPanen"),
                        objTransaksiPanen.toString(),
                        objTph.getString("noTph"),
                        objPemanen.getString("kodePemanen")
                );
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", objTransaksiPanen.getString("idTPanen")));
                if (cursor.size() == 0) {
                    repository.insert(dataNitrit);
                } else {
                    Log.e("idTPanen double", objTransaksiPanen.getString("idTPanen"));
                }

                try {
                    Gson gson = new Gson();
                    TransaksiPanen transaksiPanen = gson.fromJson(objTransaksiPanen.toString(), TransaksiPanen.class);
                    Gerdang gerdang = PemanenGerdangHelper.GerdangToPanen(transaksiPanen);
                    if(gerdang != null) {
                        listGerdang.put(gerdang.getIdGerdang(), gerdang);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.update_data_transaksi_panen));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            PemanenGerdangHelper.savePemanenGerdang(listGerdang);

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,data.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiPanen", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadTransaksiAngkut(ServiceResponse responseApi, Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());

            hapusDb(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
            hapusDb(GlobalHelper.TABLE_TRANSAKSI_ANGKUT_NFC);

            File filePANEN_TAP = new File(getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT) ,"PANEN_TAP");
            if(filePANEN_TAP.exists()){
                filePANEN_TAP.delete();
            }

            int count = 1;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objTransaksiPanen = (JSONObject) data.get(i);

                Gson gson = new Gson();
                TransaksiAngkutHelper.addIdTPanenHasBeenTap(gson.fromJson(objTransaksiPanen.toString(),TransaksiAngkut.class));

                DataNitrit dataNitrit = new DataNitrit(objTransaksiPanen.getString("idTAngkut"), objTransaksiPanen.toString(),String.valueOf(objTransaksiPanen.getBoolean("isManual")));
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.update_data_transaksi_angkut));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,data.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiAngkut", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadTransaksiSpb(ServiceResponse responseApi, Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(GlobalHelper.TABLE_TRANSAKSI_SPB);
            hapusAngkutTap();
            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_SPB);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objTransaksiPanen = (JSONObject) data.get(i);

                Gson gson = new Gson();
                TransaksiSPB transaksiSPB = gson.fromJson(objTransaksiPanen.toString(),TransaksiSPB.class);
                TransaksiAngkutHelper.addIdTPanenHasBeenTap(transaksiSPB.getTransaksiAngkut());

                DataNitrit dataNitrit = new DataNitrit(objTransaksiPanen.getString("idSPB"), objTransaksiPanen.toString());
                Log.d("downloadSPB",objTransaksiPanen.getString("idSPB"));
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.update_data_transaksi_spb));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,data.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadAfdelingAssistant(ServiceResponse responseApi, Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(GlobalHelper.TABLE_AFDELING_ASSISTANT);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_AFDELING_ASSISTANT);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objAfdAss = (JSONObject) data.get(i);
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", objAfdAss.getString("UserID")));
                DataNitrit dataNitrit = new DataNitrit(objAfdAss.getString("UserID"), objAfdAss.toString());
                repository.insert(dataNitrit);
                Log.d(TAG, "downLoadAfdelingAssistant: "+objAfdAss.getString("UserID") );

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.update_data_assisten_afdeling));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,data.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }catch (Exception e){
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadApplicationConfiguration(ServiceResponse responseApi, Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            hapusDb(GlobalHelper.TABLE_APPLICATION_CONFIGURATION);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_APPLICATION_CONFIGURATION);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            JSONObject objAppConfig = new JSONObject(responseApi.getData().toString());
            JSONObject objAppConfigDynamicParameterPenghasilan = objAppConfig.getJSONObject("dynamicParameterPenghasilan");
            DataNitrit dataNitrit = new DataNitrit(objAppConfigDynamicParameterPenghasilan.getString("estCode"), objAppConfigDynamicParameterPenghasilan.toString());
            repository.insert(dataNitrit);

            JSONObject objProgres = new JSONObject();
            objProgres.put("ket", context.getResources().getString(R.string.update_data_aplication_configuration));
            objProgres.put("persen", 100);
            objProgres.put("count", 100);
            objProgres.put("total", 100);
            if (longOperation instanceof SettingFragment.LongOperation) {
                ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
            } else if (longOperation instanceof ActiveActivity.LongOperation) {
                ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),1,1);
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadSupervisionListByEstate(ServiceResponse responseApi, Object longOperation){

        String id = null;

        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONObject responseData = new JSONObject(responseApi.getData().toString());
            hapusDb(GlobalHelper.TABLE_TPANENGANGLIST);
            hapusDb(GlobalHelper.TABLE_TPANENSUPERVISIONLIST);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TPANENGANGLIST );
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            JSONArray dataGangList = responseData.getJSONArray("gangList");
            for (int i = 0; i < dataGangList.length(); i++) {
                JSONObject objGang = (JSONObject) dataGangList.get(i);
                GangList gangList = new GangList(objGang.getString("gank"), objGang.getString("afdeling"));
                id = gangList.getIdGangList();
                Gson gson = new Gson();
                DataNitrit dataNitrit = new DataNitrit(gangList.getIdGangList(), gson.toJson(gangList));
                repository.insert(dataNitrit);
                Log.d(TAG, "downLoadSupervisionListByEstate 1: " +objGang.getString("gank"));
                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.update_data_gang_list));
                objProgres.put("persen", (count * 100 / dataGangList.length()));
                objProgres.put("count", count);
                objProgres.put("total", (dataGangList.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            count = 0;
            db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TPANENSUPERVISIONLIST);
            repository = db.getRepository(DataNitrit.class);

            JSONArray dataSupervisionList = responseData.getJSONArray("supervisionList");
            for (int i = 0; i < dataSupervisionList.length(); i++) {
                JSONObject objSpv = (JSONObject) dataSupervisionList.get(i);
                id = objSpv.getString("nik");
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", objSpv.getString("nik")));
                DataNitrit dataNitrit = new DataNitrit(objSpv.getString("nik"), objSpv.toString());
                if (cursor.size() == 0) {
                    repository.insert(dataNitrit);
                } else {
                    Log.e("nik supervision double", objSpv.getString("nik"));
                }

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.update_data_supervision_list));
                objProgres.put("persen", (count * 100 / dataSupervisionList.length()));
                objProgres.put("count", count);
                objProgres.put("total", (dataSupervisionList.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,dataSupervisionList.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("erroid Supervision",id);
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }catch (Exception e){
            e.printStackTrace();
            Log.e("erroid Supervision",id);
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadBJRInformation(ServiceResponse responseApi, Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(GlobalHelper.TABLE_BJRINFORMATION);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_BJRINFORMATION);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objBjrInformasi = (JSONObject) data.get(i);
                BJRInformation bjrInformation = new BJRInformation(
                        objBjrInformasi.getString("block"),
                        objBjrInformasi.getString("afdeling"),
                        objBjrInformasi.getInt("tahunTanam"),
                        objBjrInformasi.getDouble("bjrAdjustment"),
                        objBjrInformasi.getInt("bulan"),
                        objBjrInformasi.getInt("tahun")
                );
                Gson gson = new Gson();
                DataNitrit dataNitrit = new DataNitrit(bjrInformation.getIdBJRInformation(), gson.toJson(bjrInformation));
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.update_data_bjr_information));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,data.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downloadMasterCages(ServiceResponse responseApi, Object longOperation){
        if (responseApi == null){
            Log.e("DownloadCages", "ServiceCagesResponse is null");
            return false;

        }
        try {
            if (responseApi.getCode() == 304){
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(GlobalHelper.TABLE_CAGES);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_CAGES);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++){
                JSONObject objCages = (JSONObject) data.get(i);

                DataNitrit dataNitrit = new DataNitrit(objCages.getString("assetNo"),objCages.toString(),objCages.getString("assetName")
                );
                repository.insert(dataNitrit);

                JSONObject objProgress = new JSONObject();
                objProgress.put("ket", context.getResources().getString(R.string.update_data_Cages));
                objProgress.put("persen", (count * 100 / data.length()));
                objProgress.put("count", count);
                objProgress.put("total",(data.length()));
                count++;
                        if(longOperation instanceof  SettingFragment.LongOperation){
                            ((SettingFragment.LongOperation) longOperation).publishProgress(objProgress);
                        } else if (longOperation instanceof ActiveActivity.LongOperation) {
                            ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgress);
                        }
            }
            db.close();

        SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,data.length());
        SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

        addGetDataTag(responseApi.getTag());
        return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean downLoadMasterSpk(ServiceResponse responseApi, Object longOperation){
        if (responseApi == null){
            Log.e("DownloadSPK", "ServiceResponse is null");
            return false;
        }
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(GlobalHelper.TABLE_SPK);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_SPK);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objISCC = (JSONObject) data.get(i);
                Gson gson = new Gson();
                DataNitrit dataNitrit = new DataNitrit(objISCC.getString("idSPK"), objISCC.toString(),objISCC.getString("description"));
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.update_data_SPK));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,data.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadISCCInformation(ServiceResponse responseApi, Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(GlobalHelper.TABLE_ISCCINFORMATION);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ISCCINFORMATION);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objISCC = (JSONObject) data.get(i);
                Gson gson = new Gson();
                DataNitrit dataNitrit = new DataNitrit(String.valueOf(objISCC.getInt("idCertificate")), objISCC.toString());
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.update_data_ISCCInformation));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,data.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadSensusBJR(ServiceResponse responseApi, Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(GlobalHelper.TABLE_QC_BJR);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BJR);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objBJR = (JSONObject) data.get(i);

                JSONArray arrayBeratJanjang = objBJR.getJSONArray("beratJanjang");
                HashMap<Integer,JanjangSensus> janjangSensusHashMap = new HashMap<>();
                for(int x = 0 ; x < arrayBeratJanjang.length();x++){
                    int jjgIdx = x + 1;
                    JanjangSensus janjangSensus = new JanjangSensus(jjgIdx,Integer.parseInt(arrayBeratJanjang.get(x).toString()));
                    janjangSensusHashMap.put(janjangSensus.getIdxJanjang(),janjangSensus);
                }
                objBJR.remove("beratJanjang");
                Gson gson = new Gson();
                SensusBjr sensusBjr = gson.fromJson(objBJR.toString(),SensusBjr.class);
                sensusBjr.setBeratJanjang(janjangSensusHashMap);

                DataNitrit dataNitrit = new DataNitrit(sensusBjr.getIdQCSensusBJR(), gson.toJson(sensusBjr),sensusBjr.getIdTPanen());
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.download_sensus_bjr_list));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,data.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadQcBuah(ServiceResponse responseApi, Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(GlobalHelper.TABLE_QC_BUAH);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BUAH);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objBuah = (JSONObject) data.get(i);
                DataNitrit dataNitrit = new DataNitrit(String.valueOf(objBuah.getString("idQCMutuBuah")), objBuah.toString(),String.valueOf(objBuah.getString("idTPanen")));
                repository.insert(dataNitrit);


                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.download_qc_buah_list));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,data.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadQcAncak(ServiceResponse responseApi, Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(GlobalHelper.TABLE_QC_ANCAK);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_ANCAK);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objBuah = (JSONObject) data.get(i);
                DataNitrit dataNitrit = new DataNitrit(String.valueOf(objBuah.getString("idQcAncak")), objBuah.toString());
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.download_qc_ancak_list));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(),count,data.length());
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

            addGetDataTag(responseApi.getTag());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadAssistensiRequest(ServiceResponse responseApi, Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONObject obj = new JSONObject(responseApi.getData().toString());
            JSONArray data = obj.getJSONArray("data");
            hapusDb(GlobalHelper.TABLE_REQUEST_ASSISTENSI);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_REQUEST_ASSISTENSI);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objAssistensi = (JSONObject) data.get(i);
                DataNitrit dataNitrit = new DataNitrit(String.valueOf(objAssistensi.getString("idAssistensi")), objAssistensi.toString());
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.download_assistensi));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            if (longOperation instanceof AssistensiListFragment.LongOperation) {

            }else{
                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, data.length());
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
            }

            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadEstateAndAssistensi(ServiceResponse responseApi, Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(GlobalHelper.TABLE_ESTATE_AND_ASSISTENSI);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ESTATE_AND_ASSISTENSI);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objAssistensi = (JSONObject) data.get(i);
                DataNitrit dataNitrit = new DataNitrit(String.valueOf(objAssistensi.getString("estCode")), objAssistensi.toString());
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.download_assistensi));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof AssistensiInputFragment.LongOperation) {
                    ((AssistensiInputFragment.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            if(longOperation instanceof AssistensiInputFragment.LongOperation){

            }else {
                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, data.length());
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
            }

            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean downLoadLinkReport(String dateParam,ServiceResponse responseApi, Object longOperation){
        try {
            if (responseApi.getCode() == 304) {
                return true;
            }
            JSONArray data = new JSONArray(responseApi.getData().toString());
            hapusDb(GlobalHelper.TABLE_LINK_REPORT,dateParam);

            int count = 0;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_LINK_REPORT,dateParam);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

            for (int i = 0; i < data.length(); i++) {
                JSONObject objLinkReport = (JSONObject) data.get(i);
                String id  = objLinkReport.getString("userId") +"-"+ objLinkReport.getString("nama") +"-"+ objLinkReport.getString("jenisReport");
                DataNitrit dataNitrit = new DataNitrit(id, objLinkReport.toString());
                repository.insert(dataNitrit);

                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", context.getResources().getString(R.string.get_link_download));
                objProgres.put("persen", (count * 100 / data.length()));
                objProgres.put("count", count);
                objProgres.put("total", (data.length()));
                count++;
                if (longOperation instanceof SettingFragment.LongOperation) {
                    ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ActiveActivity.LongOperation) {
                    ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                } else if (longOperation instanceof ReportListFragment.LongOperation) {
                    ((ReportListFragment.LongOperation) longOperation).publishProgress(objProgres);
                }
            }
            db.close();

            if (longOperation instanceof ReportListFragment.LongOperation) {

            }else{
                SyncHistroyItem item = new SyncHistroyItem(responseApi.getTag(), count, data.length());
                SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);

                addGetDataTag(responseApi.getTag());
            }

            return true;
        } catch (JSONException e) {
            e.printStackTrace();
//            Toast.makeText(HarvestApp.getContext(),"SetUpDateSync downLoadTransaksiSPB", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public boolean UpdateTPanenCounterByUserID(ServiceResponse serviceResponse){
        try {
            if (serviceResponse.getCode() == 304) {
                return true;
            }
            JSONObject object = new JSONObject(String.valueOf(serviceResponse.getData()));
            SyncHistroyItem item = new SyncHistroyItem(serviceResponse.getTag(),1,1);
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);
            GlobalHelper.setCountNumber(GlobalHelper.LIST_FOLDER_TRANSAKSI_TPH,object.getInt("nextCounter"));
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean UpdateSPBCounterByUserID(ServiceResponse serviceResponse){
        try {
            if (serviceResponse.getCode() == 304) {
                return true;
            }
            JSONObject object = new JSONObject(String.valueOf(serviceResponse.getData()));
            SyncHistroyItem item = new SyncHistroyItem(serviceResponse.getTag(),1,1);
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);
            GlobalHelper.setCountNumber(GlobalHelper.LIST_FOLDER_SPB,object.getInt("nextCounter"));
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean UpdateTAngkutCounterByUserID(ServiceResponse serviceResponse){
        try {
            if (serviceResponse.getCode() == 304) {
                return true;
            }
            JSONObject object = new JSONObject(String.valueOf(serviceResponse.getData()));
            SyncHistroyItem item = new SyncHistroyItem(serviceResponse.getTag(),1,1);
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);
            GlobalHelper.setCountNumber(GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT,object.getInt("nextCounter"));
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean UpdateTPHCounterByUserID(ServiceResponse serviceResponse){
        try {
            if (serviceResponse.getCode() == 304) {
                return true;
            }
            JSONObject object = new JSONObject(String.valueOf(serviceResponse.getData()));
            SyncHistroyItem item = new SyncHistroyItem(serviceResponse.getTag(),1,1);
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);
            GlobalHelper.setCountNumber(GlobalHelper.LIST_FOLDER_TPH,object.getInt("nextCounter"));
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean UpdateLangsiranCounterByUserID(ServiceResponse serviceResponse){
        try {
            if (serviceResponse.getCode() == 304) {
                return true;
            }
            JSONObject object = new JSONObject(String.valueOf(serviceResponse.getData()));
            SyncHistroyItem item = new SyncHistroyItem(serviceResponse.getTag(),1,1);
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);
            GlobalHelper.setCountNumber(GlobalHelper.LIST_FOLDER_LANGSIRAN,object.getInt("nextCounter"));
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean UpdateTPanenSupevisionCounterByUserID(ServiceResponse serviceResponse){
        try {
            if (serviceResponse.getCode() == 304) {
                return true;
            }
            JSONObject object = new JSONObject(String.valueOf(serviceResponse.getData()));
            SyncHistroyItem item = new SyncHistroyItem(serviceResponse.getTag(),1,1);
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);
            GlobalHelper.setCountNumber(GlobalHelper.LIST_FOLDER_TPanenSupervision,object.getInt("nextCounter"));
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }


    public boolean UpdateQCMutuAncakCounterByUserID(ServiceResponse serviceResponse){
        try {
            if (serviceResponse.getCode() == 304) {
                return true;
            }
            JSONObject object = new JSONObject(String.valueOf(serviceResponse.getData()));
            SyncHistroyItem item = new SyncHistroyItem(serviceResponse.getTag(),1,1);
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);
            GlobalHelper.setCountNumber(GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK,object.getInt("nextCounter"));
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean UpdateAssistensiRequestCounterByUserID(ServiceResponse serviceResponse){
        try {
            if (serviceResponse.getCode() == 304) {
                return true;
            }
            JSONObject object = new JSONObject(String.valueOf(serviceResponse.getData()));
            JSONObject obj = object.getJSONObject("counter");
            GlobalHelper.setCountNumberYear(GlobalHelper.LIST_FOLDER_REQUEST_ASSISTENSI,obj.getInt("nextCounter"));

            SyncHistroyItem item = new SyncHistroyItem(serviceResponse.getTag(),1,1);
            SyncHistoryHelper.updateSyncHistoryTagWithIndex(item);
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void updateStatusUploadTph(String id){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        String [] arrayId = id.split(";");
        for(int i = 0 ; i < arrayId.length;i++) {
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                TPH tph = gson.fromJson(dataNitrit.getValueDataNitrit(),TPH.class);
                tph.setStatus(TPH.STATUS_TPH_UPLOAD);
                tph.setReasonUnharvestTPH(null);
                dataNitrit.setValueDataNitrit(gson.toJson(tph));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();
    }

    public static void updateStatusUploadLangsiran(String id){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_LANGSIRAN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        String [] arrayId = id.split(";");
        for(int i = 0 ; i < arrayId.length;i++) {
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                Langsiran langsiran = gson.fromJson(dataNitrit.getValueDataNitrit(),Langsiran.class);
                langsiran.setStatus(Langsiran.STATUS_LANGSIRAN_UPLOAD);
                dataNitrit.setValueDataNitrit(gson.toJson(langsiran));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();
    }

    public static void updateStatusUploadTransaksiTph(String id){
        ArrayList<String> allDb = GlobalHelper.getAllFileDB(GlobalHelper.TABLE_TRANSAKSI_TPH, true);
        for (String dbFile : allDb) {
            Nitrite db = GlobalHelper.getTableNitritFullPath(dbFile);
            if (db != null) {
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                String[] arrayId = id.split(";");
                for (int i = 0; i < arrayId.length; i++) {
                    Log.d("SetUpDataSyncHelper", "updateStatusUploadTransaksiTph: " + arrayId[i]);
                    try {
                        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
                        for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                            DataNitrit dataNitrit = (DataNitrit) iterator.next();
                            Gson gson = new Gson();
                            TransaksiPanen transaksiPanen = gson.fromJson(dataNitrit.getValueDataNitrit(), TransaksiPanen.class);
                            transaksiPanen.setStatus(TransaksiPanen.UPLOAD);

                            dataNitrit.setValueDataNitrit(gson.toJson(transaksiPanen));
                            repository.update(dataNitrit);
                            break;
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                db.close();
            }
        }
    }

    public static void updateStatusUploadTransaksiAngkut(String id){

        ArrayList<String> allDb = GlobalHelper.getAllFileDB(GlobalHelper.TABLE_TRANSAKSI_ANGKUT, true);
        for (String dbFile : allDb) {
            Nitrite db = GlobalHelper.getTableNitritFullPath(dbFile);
            if (db != null) {
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                String[] arrayId = id.split(";");
                for (int i = 0; i < arrayId.length; i++) {
                    try {
                        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
                        for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                            DataNitrit dataNitrit = (DataNitrit) iterator.next();
                            Gson gson = new Gson();
                            TransaksiAngkut transaksiAngkut = gson.fromJson(dataNitrit.getValueDataNitrit(), TransaksiAngkut.class);
                            if (transaksiAngkut.getStatus() == TransaksiAngkut.TAP) {
                                transaksiAngkut.setStatus(TransaksiAngkut.UPLOAD);
                            } else if (transaksiAngkut.getStatus() == TransaksiAngkut.HAPUS) {
                                transaksiAngkut.setStatus(TransaksiAngkut.UPLOAD_HAPUS);
                            }
                            dataNitrit.setValueDataNitrit(gson.toJson(transaksiAngkut));
                            repository.update(dataNitrit);

                            break;
                        }
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                db.close();
            }
        }
    }

    public static void updateStatusUploadTransaksiSpb(String id){
        ArrayList<String> allDb = GlobalHelper.getAllFileDB(GlobalHelper.TABLE_TRANSAKSI_SPB, true);
        for (String dbFile : allDb) {
            Nitrite db = GlobalHelper.getTableNitritFullPath(dbFile);
            if (db != null) {
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                String[] arrayId = id.split(";");
                for (int i = 0; i < arrayId.length; i++) {
                    try {
                        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
                        for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                            DataNitrit dataNitrit = (DataNitrit) iterator.next();
                            Gson gson = new Gson();
                            TransaksiSPB transaksiSPB = gson.fromJson(dataNitrit.getValueDataNitrit(), TransaksiSPB.class);
                            if (transaksiSPB.getStatus() == TransaksiSPB.BELUM_UPLOAD) {
                                transaksiSPB.setStatus(TransaksiSPB.UPLOAD);
                            } else if (transaksiSPB.getStatus() == TransaksiSPB.HAPUS) {
                                transaksiSPB.setStatus(TransaksiSPB.UPLOAD_HAPUS);
                            }
                            dataNitrit.setValueDataNitrit(gson.toJson(transaksiSPB));
                            repository.update(dataNitrit);
                            break;
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                db.close();
            }
        }

        ArrayList<String> allPathJSONBackup = GlobalHelper.getAllPathJSONBackup(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SPB]);
        for(int i = 0 ; i < allPathJSONBackup.size(); i++){
            try {
                File fileJSON = new File(allPathJSONBackup.get(i));
                String[] arrayId = id.split(";");
                for (int j = 0; j < arrayId.length; j++) {
                    if(fileJSON.getName().toLowerCase().contains(arrayId[j].toLowerCase())){
                        String namaFile = fileJSON.getName();
                        GlobalHelper.moveFile(fileJSON,
                                new File(GlobalHelper.getPathJSONBackupUpload(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SPB])),
                                namaFile
                        );
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public static void updateStatusUploadTransaksiTphNfc(String id){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_TPH_NFC);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        String [] arrayId = id.split(";");
        for(int i = 0 ; i < arrayId.length;i++) {
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                dataNitrit.setParam1(String.valueOf(TransaksiPanen.UPLOAD));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();
    }

    public static void updateStatusUploadTransaksiAngkutNfc(String id){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT_NFC);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        String [] arrayId = id.split(";");
        for(int i = 0 ; i < arrayId.length;i++) {
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                dataNitrit.setParam1(String.valueOf(TransaksiAngkut.UPLOAD));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();
    }

    public static void updateStatusUploadQcBuah(String id){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BUAH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        String [] arrayId = id.split(";");
        for(int i = 0 ; i < arrayId.length;i++) {
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                QcMutuBuah qcMutuBuah = gson.fromJson(dataNitrit.getValueDataNitrit(), QcMutuBuah.class);
                TransaksiPanen transaksiPanen = qcMutuBuah.getTransaksiPanenQc();
                transaksiPanen.setStatus(TransaksiPanen.UPLOAD);
                qcMutuBuah.setTransaksiPanenQc(transaksiPanen);
                dataNitrit.setValueDataNitrit(gson.toJson(qcMutuBuah));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();
    }

    public static void updateStatusUploadSensusBjr(String id){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BJR);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        String [] arrayId = id.split(";");
        for(int i = 0 ; i < arrayId.length;i++) {
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                SensusBjr sensusBjr = gson.fromJson(dataNitrit.getValueDataNitrit(), SensusBjr.class);
                sensusBjr.setStatus(SensusBjr.UPLOAD);
                dataNitrit.setValueDataNitrit(gson.toJson(sensusBjr));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();
    }

    public static void updateStatusUploadQcAncak(String id){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_ANCAK);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        String [] arrayId = id.split(";");
        for(int i = 0 ; i < arrayId.length;i++) {
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                QcAncak qcAncak = gson.fromJson(dataNitrit.getValueDataNitrit(), QcAncak.class);
                qcAncak.setStatus(QcAncak.Upload);
                dataNitrit.setValueDataNitrit(gson.toJson(qcAncak));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();
    }

    public static void updateStatusAssistensiRequest(String id){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_REQUEST_ASSISTENSI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        String [] arrayId = id.split(";");
        for(int i = 0 ; i < arrayId.length;i++) {
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                AssistensiRequest assistensiRequest = gson.fromJson(dataNitrit.getValueDataNitrit(), AssistensiRequest.class);
                assistensiRequest.setStatus(AssistensiRequest.PENDING);
                dataNitrit.setValueDataNitrit(gson.toJson(assistensiRequest));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();
    }

    public static void updateInsetOpname(String id){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_OPNAME_NFC);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        String [] arrayId = id.split(";");
        for(int i = 0 ; i < arrayId.length;i++) {
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                OpnameNFC opnameNFC = gson.fromJson(dataNitrit.getValueDataNitrit(), OpnameNFC.class);
                opnameNFC.setStatus(OpnameNFC.UPLOAD);
                dataNitrit.setValueDataNitrit(gson.toJson(opnameNFC));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();
    }

    public static void updateInsetGerdang(String id){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_GERDANG);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        String [] arrayId = id.split(";");
        for(int i = 0 ; i < arrayId.length;i++) {
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", arrayId[i]));
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                Gerdang gerdang = gson.fromJson(dataNitrit.getValueDataNitrit(), Gerdang.class);
                gerdang.setStatus(Gerdang.Gerdang_Upload);
                dataNitrit.setValueDataNitrit(gson.toJson(gerdang));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();
    }

    public static boolean cekGetDataTag(Tag tag){
        String [] isiLastGetData = GlobalHelper.readFileContent(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getLastGetdataName()).split(";");
        for(int i = 0 ; i < isiLastGetData.length;i++){
            if(isiLastGetData[i].equals(tag.toString())){
                return true;
            }
        }
        if(isiLastGetData.length == 1){
            if(isiLastGetData[0].trim().isEmpty()) {
                return false;
            }
        }

        return SyncTimeHelper.cekSyncTime(tag);
    }

    public static void addGetDataTag(Tag tag){
        GlobalHelper.writeFileContentAppend(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getLastGetdataName(),tag+";");
        SyncTimeHelper.updateSyncTime(tag);
    }

    public static void addPointCounterBadResponse(){
        String isi = GlobalHelper.readFileContent(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getCountBadResponseName());
        if(isi.isEmpty()){
            GlobalHelper.writeFileContent(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getCountBadResponseName(),"1");
        }else{
            int idx = Integer.parseInt(isi);
            idx++;
            GlobalHelper.writeFileContent(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getCountBadResponseName(),String.valueOf(idx));
        }
    }

    public boolean backupDataHMS(Object longOperation){
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, Context.MODE_PRIVATE);
        String sync_time = prefer.getString(HarvestApp.SYNC_TIME,null);
        Gson gson = new Gson();
        LastSyncTime lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
        HashMap<String,SyncTime> syncTimeHashMap = lastSyncTime.getSyncTimeHashMap();
        SyncTime syncTimeTransaksi = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
        SyncTime syncTimeMaster = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);

        if(!syncTimeTransaksi.isSelected() && !syncTimeMaster.isSelected()){
            return true;
        }

        File dbBackup = new File(GlobalHelper.getDatabasePathHMSBackup());
        String[] dirListing = dbBackup.list();
        Arrays.sort(dirListing);

        if (longOperation instanceof ActiveActivity.LongOperation) {
            return true;
        }

        //batasi file restore jika mencapai atas maka dihapus file yang terakhir
        if(dirListing.length > GlobalHelper.SIZE_RESTORE) {
            int selisih = dirListing.length - GlobalHelper.SIZE_RESTORE;
            for (int i = 0; i < selisih; i++){
                File f = new File(dbBackup,dirListing[i]);
                if(f.isDirectory()){
                    try {
                        FileUtils.cleanDirectory(f);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else if (f.isFile()){
                    Log.d("hapus file backup",f.getAbsolutePath());
                    f.delete();
                }
            }
        }

        zipDBHms(longOperation,"SYNC");
        return true;
    }

    public void zipDBHms(Object longOperation,String param){
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, Context.MODE_PRIVATE);
        String sync_time = prefer.getString(HarvestApp.SYNC_TIME,null);
        Gson gson = new Gson();
        LastSyncTime lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
        HashMap<String,SyncTime> syncTimeHashMap = lastSyncTime.getSyncTimeHashMap();
        SyncTime syncTimeTransaksi = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
        SyncTime syncTimeMaster = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);

        String sumber = "";
        if (longOperation instanceof SettingFragment.LongOperation) {
            sumber = "Menu Setting "+ param ;
        } else if (longOperation instanceof ActiveActivity.LongOperation) {
            sumber = "Active "+param;
        } else if (longOperation instanceof MapActivity.LongOperation) {
            sumber = "Map "+param;
        }

        File extStatisticsFolder = new File( BuildConfig.BUILD_VARIANT.equals("dev") ? Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES + "/HMSSAPTESTING":Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES + "/HMSSAP");
        File f = new File(GlobalHelper.getDatabasePathHMSBackup() +"/"+System.currentTimeMillis()+"_"+sumber+".zip");

//        ZipArchive zipArchive = new ZipArchive();
//        ZipArchive.zip(extStatisticsFolder.getPath(),f.getPath(),"");
        ZipManager.zipFolder(extStatisticsFolder.getPath(), f.getPath(), "");

        if(param.equals("SYNC")) {
            try {
                GlobalHelper.copyFile(f,new File(GlobalHelper.getDatabasePathHMSTemp()) ,"HMSTemp.zip");
//                zipArchive.unzip(GlobalHelper.getDatabasePathHMSTemp() + "HMSTemp.zip", GlobalHelper.getDatabasePathHMSTemp(), "");
//                File hmsTemp = new File(GlobalHelper.getDatabasePathHMSTemp() + "HMSTemp.zip");
//                if (hmsTemp.exists()) {
//                    hmsTemp.delete();
//                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        HashMap<Integer,File> fileDbHms = GlobalHelper.getBackUpPathHMS();
        int dbCount = 1;
        for(HashMap.Entry<Integer,File> entry : fileDbHms.entrySet()){
            int count = 0;
            File dir = entry.getValue();
//            boolean hapus = false;
//            if(param.equals("SYNC")) {
//                if(syncTimeTransaksi.isSelected()){
//                    String sDir = dir.getAbsolutePath();
//                    if(sDir.contains(GlobalHelper.EXTERNAL_DIR_FILES+"/HMS/db2")){
//                        hapus = true;
//                    }
//                }
//                if(syncTimeMaster.isSelected()){
//                    String sDir = dir.getAbsolutePath();
//                    if(sDir.contains(GlobalHelper.EXTERNAL_DIR_FILES+"/HMS/db/"+ GlobalHelper.getEstate().getEstCode())){
//                        hapus = true;
//                    }
//                }
//            }
            for (File fileEntry : dir.listFiles()) {
//                if(hapus) {
                    try {
                        if (fileEntry.isDirectory()) {
                            if (param.equals("SYNC")) {
                                String sDir = dir.getAbsolutePath();
                                if (sDir.contains(BuildConfig.BUILD_VARIANT.equals("dev") ? GlobalHelper.EXTERNAL_DIR_FILES + "/HMSSAPTESTING/db/" + GlobalHelper.getEstate().getEstCode() : GlobalHelper.EXTERNAL_DIR_FILES + "/HMSSAP/db/" + GlobalHelper.getEstate().getEstCode())) {
//                                    biar nga keseringan aktive
//                                    if(syncTimeTransaksi.isSelected()){
//                                        if(SyncTimeHelper.cekDbTranskasi(fileEntry.getName())){
//                                            FileUtils.cleanDirectory(fileEntry);
//                                            Log.d(this.getClass() + " hapus dir 1a ", fileEntry.getAbsolutePath() + "("+Encrypts.decrypt(fileEntry.getName()) + ")");
//                                        }
//                                    }
//                                    if(syncTimeMaster.isSelected()){
//                                        if(SyncTimeHelper.cekDbMaster(fileEntry.getName())){
//                                            FileUtils.cleanDirectory(fileEntry);
//                                            Log.d(this.getClass() + " hapus dir 1a M ", fileEntry.getAbsolutePath() + "("+Encrypts.decrypt(fileEntry.getName()) + ")");
//                                        }
//                                    }
                                } else {
                                    if(syncTimeTransaksi.isSelected()){
                                        if(SyncTimeHelper.cekFolderTransaksi(fileEntry.getName())) {
                                            if(SyncTimeHelper.cekFolderTransaksi(fileEntry.getName())) {
                                                FileUtils.cleanDirectory(fileEntry);
                                                Log.d(this.getClass() + " hapus dir 1b ", fileEntry.getAbsolutePath());
                                            }
                                        }
                                    }
                                    if(syncTimeMaster.isSelected()){
                                        if(SyncTimeHelper.cekFolderMaster(fileEntry.getName())) {
                                            if(SyncTimeHelper.cekFolderMaster(fileEntry.getName())) {
                                                FileUtils.cleanDirectory(fileEntry);
                                                Log.d(this.getClass() + " hapus dir 1b M ", fileEntry.getAbsolutePath());
                                            }
                                        }
                                    }
                                }
                            } else {
                                FileUtils.cleanDirectory(fileEntry);
                                Log.d(this.getClass() + " hapus dir 1c ", fileEntry.getAbsolutePath());
                            }
                        } else if (fileEntry.isFile()) {
                            if (param.equals("SYNC")) {
                                String sDir = dir.getAbsolutePath();
                                if (sDir.contains(BuildConfig.BUILD_VARIANT.equals("dev") ? GlobalHelper.EXTERNAL_DIR_FILES + "/HMSSAPTESTING/db/" + GlobalHelper.getEstate().getEstCode() : GlobalHelper.EXTERNAL_DIR_FILES + "/HMSSAP/db/" + GlobalHelper.getEstate().getEstCode())) {
//                                    if(syncTimeTransaksi.isSelected()){
//                                        if(SyncTimeHelper.cekDbTranskasi(dir.getName())){
//                                            fileEntry.delete();
//                                            Log.d(this.getClass() + " hapus file 1a", fileEntry.getAbsolutePath());
//                                        }
//                                    }
                                } else {
                                    fileEntry.delete();
                                    Log.d(this.getClass() + " hapus file 2b", fileEntry.getAbsolutePath());
                                }
                            } else {
                                fileEntry.delete();
                                Log.d(this.getClass() + " hapus file 2c", fileEntry.getAbsolutePath());
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
//                }

                try {
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket", "Backup DB " + dbCount);
                    objProgres.put("persen", (count * 100 / dir.listFiles().length));
                    objProgres.put("count", count);
                    objProgres.put("total", (dir.listFiles().length));
                    count++;
                    if (longOperation instanceof SettingFragment.LongOperation) {
                        ((SettingFragment.LongOperation) longOperation).publishProgress(objProgres);
                    } else if (longOperation instanceof ActiveActivity.LongOperation) {
                        ((ActiveActivity.LongOperation) longOperation).publishProgress(objProgres);
                    } else if (longOperation instanceof MapActivity.LongOperation) {
                        ((MapActivity.LongOperation) longOperation).publishProgress(objProgres);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            try {
//                if(hapus) {
                    if (param.equals("SYNC")) {
                        String sDir = dir.getAbsolutePath();
                        if (sDir.contains(BuildConfig.BUILD_VARIANT.equals("dev") ? GlobalHelper.EXTERNAL_DIR_FILES + "/HMSSAPTESTING/db/" + GlobalHelper.getEstate().getEstCode() : GlobalHelper.EXTERNAL_DIR_FILES + "/HMSSAP/db/" + GlobalHelper.getEstate().getEstCode())) {
//                            if(syncTimeTransaksi.isSelected()){
//                                if(SyncTimeHelper.cekDbTranskasi(dir.getName())){
//                                    FileUtils.cleanDirectory(dir);
//                                    Log.d(this.getClass() + " hapus dir 1a ", dir.getAbsolutePath() + "("+Encrypts.decrypt(dir.getName()) + ")");
//                                }
//                            }
//                            if(syncTimeMaster.isSelected()){
//                                if(SyncTimeHelper.cekDbMaster(dir.getName())){
//                                    FileUtils.cleanDirectory(dir);
//                                    Log.d(this.getClass() + " hapus dir 1a M ", dir.getAbsolutePath() + "("+Encrypts.decrypt(dir.getName()) + ")");
//                                }
//                            }
                        } else {
                            if(syncTimeTransaksi.isSelected()){
                                if(SyncTimeHelper.cekFolderTransaksi(dir.getName())) {
                                    if(SyncTimeHelper.cekFolderTransaksi(dir.getName())) {
                                        FileUtils.cleanDirectory(dir);
                                        Log.d(this.getClass() + " hapus dir 1b ", dir.getAbsolutePath());
                                    }
                                }
                            }
                            if(syncTimeMaster.isSelected()){
                                if(SyncTimeHelper.cekFolderMaster(dir.getName())) {
                                    if(SyncTimeHelper.cekFolderMaster(dir.getName())) {
                                        FileUtils.cleanDirectory(dir);
                                        Log.d(this.getClass() + " hapus dir 1b M ", dir.getAbsolutePath());
                                    }
                                }
                            }
                        }
                    } else {
                        FileUtils.cleanDirectory(dir);
                        Log.d(this.getClass() + " hapus dir 2c", dir.getAbsolutePath());
                    }
//                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            dbCount++;
        }

        GlobalHelper.setupHMSFolder();

    }

    public boolean setUpLastSync(CoordinatorLayout myCoordinatorLayout){
        try {
            File db = new File(getDatabasePathHMS());
            for(File file : db.listFiles()){
                if(!file.isDirectory()){
                    file.delete();
                }
            }

            File file = new File(db,Encrypts.encrypt(GlobalHelper.LAST_SYNC));

            PackageManager manager = HarvestApp.getContext().getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES);

            JSONObject object = new JSONObject();
            object.put("Time",System.currentTimeMillis());
            object.put("PackageName",info.packageName);
            object.put("VersionCode",info.versionCode );
            object.put("VersionName",info.versionName);
            GlobalHelper.writeFileContent(file.getAbsolutePath(), String.valueOf(object));

            SyncHistoryHelper.insertHistorySync(SyncHistroy.Sync_Berhasil);

            clearHmsTemp();

            GlobalHelper.setUpAllData();
            Snackbar snackbar = Snackbar.make(myCoordinatorLayout,"Sync Berhasil",Snackbar.LENGTH_INDEFINITE);
            snackbar.show();
            return true;
        }catch (Exception e){
            Log.e("setUpLastSync",e.toString());
            return false;
        }

    }

    public String getLastSync(){
        File file = new File(getDatabasePathHMS(),Encrypts.encrypt(GlobalHelper.LAST_SYNC));
        String isi = GlobalHelper.readFileContent(file.getAbsolutePath());
        return isi.isEmpty() ? null : isi;
    }

    public void clearHmsTemp(){
        File f = new File(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getLastGetdataName());
        if(f.exists()){
            Log.d(GlobalHelper.getLastGetdataName(),f.getAbsolutePath());
            f.delete();
        }

        File f1 = new File(GlobalHelper.getDatabasePathHMSTemp() + GlobalHelper.getCountBadResponseName());
        if(f1.exists()){
            f1.delete();
        }

        File hmsTemp = new File(GlobalHelper.getDatabasePathHMSTemp() + "HMSTemp.zip");
        if (hmsTemp.exists()) {
            hmsTemp.delete();
        }
    }

    private void hapusDb(String namaTable){
        ArrayList<String> allDb = GlobalHelper.getAllFileDB(namaTable,false);
        for(String dbFile : allDb) {
            File fileDb = new File(dbFile);
            if (fileDb.exists()) {
                fileDb.delete();
            }
        }
    }

    private void hapusDb(String namaTable,String dateParam){
        try {
            Long l = System.currentTimeMillis() - (2 * 24 * 60 * 60 * 1000);
            SimpleDateFormat sdfDateParam = new SimpleDateFormat("yyyyMMdd");
            int tglMin = Integer.parseInt(sdfDateParam.format(l));

            ArrayList<String> allDb = GlobalHelper.getAllFileDB(namaTable, false);
            for (String dbFile : allDb) {
                File fileDb = new File(dbFile);
                String [] aTgl = fileDb.getName().replace(".db","").split("_");
                if(Integer.parseInt(aTgl[2]) < tglMin ){
                    if (fileDb.exists()) {
                        fileDb.delete();
                    }
                }else if (dbFile.contains(dateParam)) {
                    if (fileDb.exists()) {
                        fileDb.delete();
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void hapusAngkutTap(){
        File file = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT) ,"ANGKUT_TAP");
        if(file.exists()){
            file.delete();
            if(file.exists()){
                file.delete();
            }
        }
        File file2 = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT) ,"PANEN_TAP");
        if(file2.exists()){
            file2.delete();
            if(file2.exists()){
                file2.delete();
            }
        }
    }
}
