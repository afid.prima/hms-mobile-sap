package id.co.ams_plantation.harvestsap.model;

import java.util.HashMap;

import id.co.ams_plantation.harvestsap.connection.Tag;

public class SyncHistroy {

    public static final int Sync_Gagal = 0;
    public static final int Sync_Berhasil = 1;
    public static final int Sync_Berjalan = 2;

    public static final String Sync_From_MapActivity = "Map";
    public static final String Sync_From_Setting = "Setting";
    public static final String Sync_From_Active = "Active";

    public static final String Action_Upload = "Upload Dan Sync";
    public static final String Action_Sync = "Sync";

    String idSyncHistory;
    String syncFrom;
    String action;
    int statusSyncHistory;
    HashMap<Tag,SyncHistroyItem> allSyncHistroyItem;
    long updateTime;

    public SyncHistroy(String idSyncHistory, String syncFrom, String action, int statusSyncHistory, HashMap<Tag, SyncHistroyItem> allSyncHistroyItem) {
        this.idSyncHistory = idSyncHistory;
        this.syncFrom = syncFrom;
        this.action = action;
        this.statusSyncHistory = statusSyncHistory;
        this.allSyncHistroyItem = allSyncHistroyItem;
        this.updateTime = System.currentTimeMillis();
    }

    public String getIdSyncHistory() {
        return idSyncHistory;
    }

    public void setIdSyncHistory(String idSyncHistory) {
        this.idSyncHistory = idSyncHistory;
    }

    public String getSyncFrom() {
        return syncFrom;
    }

    public void setSyncFrom(String syncFrom) {
        this.syncFrom = syncFrom;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getStatusSyncHistory() {
        return statusSyncHistory;
    }

    public void setStatusSyncHistory(int statusSyncHistory) {
        this.statusSyncHistory = statusSyncHistory;
    }

    public HashMap<Tag, SyncHistroyItem> getAllSyncHistroyItem() {
        return allSyncHistroyItem;
    }

    public void setAllSyncHistroyItem(HashMap<Tag, SyncHistroyItem> allSyncHistroyItem) {
        this.allSyncHistroyItem = allSyncHistroyItem;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }
}
