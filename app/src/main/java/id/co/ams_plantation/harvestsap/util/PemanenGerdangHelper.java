package id.co.ams_plantation.harvestsap.util;

import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import id.co.ams_plantation.harvestsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.SelectGangAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectPemanenAdapter;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.Gerdang;
import id.co.ams_plantation.harvestsap.model.Pemanen;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.ui.TphActivity;

/**
 * Created on : 05,December,2022
 * Author     : Afid
 */

public class PemanenGerdangHelper {

    private static String TAG = "PemanenGerdangHelper";
    FragmentActivity activity;
    TphActivity tphActivity;
    TphInputFragment tphFragment;
    PemanenHelper pemanenHelper;
    SelectGangAdapter adapterGangGerdang;
    SelectPemanenAdapter adapterPemanenGerdang;
    String gangSelected;

    public PemanenGerdangHelper(FragmentActivity activity) {
        this.activity = activity;

        if(activity instanceof TphActivity) {
            tphActivity = ((TphActivity) activity);
            if (tphActivity.getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof TphInputFragment) {
                tphFragment = (TphInputFragment) tphActivity.getSupportFragmentManager().findFragmentById(R.id.content_container);
            }
            pemanenHelper = new PemanenHelper(tphActivity);
        }
    }

    public boolean cekBawaanInputTph(){
        if(tphActivity == null){
            return false;
        }
        if(tphFragment == null){
            return false;
        }
        if(pemanenHelper == null){
            return false;
        }
        return  true;
    }

    public void setDropdownGangGerdang(){

        if(!cekBawaanInputTph()){
            return;
        }

        HashMap<String, Pemanen> sGangGerdang = new HashMap<>();
        if(GlobalHelper.dataPemanen.size() > 0) {
            for (HashMap.Entry<String, Pemanen> entry : GlobalHelper.dataPemanen.entrySet()) {
                sGangGerdang.put(entry.getValue().getGank(), entry.getValue());
            }
            adapterGangGerdang = new SelectGangAdapter(tphActivity, R.layout.row_setting, new ArrayList<Pemanen>(sGangGerdang.values()));
            tphFragment.etGangGerdang.setThreshold(1);
            tphFragment.etGangGerdang.setAdapter(adapterGangGerdang);
            adapterGangGerdang.notifyDataSetChanged();
        }

        tphFragment.etGangGerdang.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapterGangGerdang = (SelectGangAdapter) parent.getAdapter();
                gangSelected =  adapterGangGerdang.getItemAt(position).getGank();
                setGang(gangSelected);
            }
        });

        tphFragment.etGangGerdang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tphFragment.etGangGerdang.showDropDown();
            }
        });

        tphFragment.etGangGerdang.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if(gangSelected == null){
                        tphFragment.etGangGerdang.setText("");
                    }else{
                        tphFragment.etGangGerdang.setText(gangSelected);
                    }
                }
            }
        });

        tphFragment.etPemanenGerdang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tphFragment.etPemanenGerdang.showDropDown();
            }
        });

        tphFragment.etPemanenGerdang.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                adapterPemanenGerdang = (SelectPemanenAdapter) adapterView.getAdapter();
                Pemanen pemanen = adapterPemanenGerdang.getItemAt(position);
                setPemanen(pemanen);
            }
        });
    }

    public void setGang(String Gang){
        if(!cekBawaanInputTph()){
            return;
        }

        tphFragment.etGangGerdang.setText(Gang);

        setAdapterPemanenGerdang(Gang);
        tphFragment.etPemanenGerdang.requestFocus();
        tphFragment.etPemanenGerdang.callOnClick();
    }

    public void setAdapterPemanenGerdang(String Gang){
        adapterPemanenGerdang = new SelectPemanenAdapter(activity, R.layout.row_record,
                pemanenHelper.getListPemanenByGangWithCompare(GlobalHelper.dataPemanen,
                        null,
                        Gang)
        );
        tphFragment.etPemanenGerdang.setThreshold(1);
        tphFragment.etPemanenGerdang.setAdapter(adapterPemanenGerdang);
        adapterPemanenGerdang.notifyDataSetChanged();
    }

    public void setPemanen(Pemanen value) {
        if (value != null) {
            tphFragment.etPemanenGerdang.setText(value.getNama());
            if (value.getGank() != null) {
                tphFragment.tvGerdang.setText(value.getGank() + " - " + value.getNik() + " - " + value.getNama());
            } else {
                tphFragment.tvGerdang.setText(" - " + value.getNik() + " - " + value.getNama());
            }
            tphFragment.pemanenGerdang = value;
        }
    }

    public HashMap<String,Gerdang> getCurrentGerdang(Long currentDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String tgl = sdf.format(currentDate);
        HashMap<String,Gerdang> currentGerdang = new HashMap<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_GERDANG);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("param1", tgl));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                Gerdang gerdang  = gson.fromJson(dataNitrit.getValueDataNitrit(),Gerdang.class);
                currentGerdang.put(gerdang.getPemanenUtama().getNik(),gerdang);
                Log.d(TAG, "getCurrentGerdang: "+gson.toJson(gerdang));
            }
        }
        db.close();
        return  currentGerdang;
    }

    public void savePemanenGerdang(TransaksiPanen transaksiPanen){
        if(transaksiPanen.getKongsi().getSubPemanens() == null){
            return;
        }
        if(transaksiPanen.getKongsi().getSubPemanens().get(0) == null){
            return;
        }
        if(transaksiPanen.getKongsi().getSubPemanens().get(0).getPemanen() == null){
            return;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdfId = new SimpleDateFormat("yyMMdd");
        Pemanen pemanenUtama = transaksiPanen.getKongsi().getSubPemanens().get(0).getPemanen();
        String tglPanen = sdf.format(transaksiPanen.getCreateDate());
        String tglPanenId = sdfId.format(transaksiPanen.getCreateDate());
        String estCode = GlobalHelper.getEstate().getEstCode();
        if(pemanenUtama.getEstate() != null){
            estCode = pemanenUtama.getEstate();
        }

        Gerdang gerdang = new Gerdang(
                estCode + "-"+tglPanenId+ "-"+pemanenUtama.getNik(),
                estCode,
                tglPanen,pemanenUtama,transaksiPanen.getKongsi().getPemanenGerdang(),Gerdang.Gerdang_NotUplaod,
                transaksiPanen.getCreateDate(),transaksiPanen.getCreateBy()
        );

        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(gerdang.getIdGerdang(),gson.toJson(gerdang),tglPanen);

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_GERDANG);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", gerdang.getIdGerdang()));
        if(cursor.size() > 0) {
            repository.update(dataNitrit);
        }else{
            repository.insert(dataNitrit);
        }
        db.close();
    }

    public static void savePemanenGerdang(HashMap<String,Gerdang> listGerdang){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_GERDANG);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        if(listGerdang.size() > 0 ){
            Gson gson = new Gson();
            try {
                for (String key : listGerdang.keySet()) {
                    Gerdang gerdang = listGerdang.get(key);
                    if(gerdang.getPemanenUtama() != null && gerdang.getPemanenGerdang() != null) {
                        DataNitrit dataNitrit = new DataNitrit(
                                gerdang.getIdGerdang(),
                                gson.toJson(gerdang),
                                gerdang.getDate()
                        );
                        repository.insert(dataNitrit);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        db.close();
    }

    public static Gerdang GerdangToPanen(TransaksiPanen transaksiPanen){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat sdfId = new SimpleDateFormat("yyMMdd");
            Pemanen pemanenUtama = transaksiPanen.getKongsi().getSubPemanens().get(0).getPemanen();
            String tglPanen = sdf.format(transaksiPanen.getCreateDate());
            String tglPanenId = sdfId.format(transaksiPanen.getCreateDate());
            String estCode = GlobalHelper.getEstate().getEstCode();
            if (pemanenUtama.getEstate() != null) {
                estCode = pemanenUtama.getEstate();
            }

            if(transaksiPanen.getKongsi().getPemanenGerdang() != null){
                Gerdang gerdang = new Gerdang(
                        estCode + "-" +tglPanenId + "-" +pemanenUtama.getNik(),
                        estCode,
                        tglPanen, pemanenUtama, transaksiPanen.getKongsi().getPemanenGerdang(), Gerdang.Gerdang_Upload,
                        transaksiPanen.getCreateDate(),transaksiPanen.getCreateBy()
                );

                if(gerdang.getDate().equals("20230103")){
                    Gson gson = new Gson();
                    Log.d(TAG, "GerdangToPanen: "+gson.toJson(gerdang));
                }

                return gerdang;
            }else{
                return null;
            }
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
