package id.co.ams_plantation.harvestsap.TableView.view_model;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewColumnHeader;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestsap.model.Angkut;

public class AngkutInputTableViewModel {
    private final Context mContext;
    private final ArrayList<Angkut> angkuts;
    String [] HeaderColumn = {"OPH","Block","Janjang","Brondolan","Berat Estimasi"};

    public AngkutInputTableViewModel(Context context, ArrayList<Angkut> angkuts) {
        mContext = context;
        this.angkuts = angkuts;
        Collections.sort(angkuts, new Comparator<Angkut>() {
            @Override
            public int compare(Angkut t0, Angkut t1) {
                return (t0.getOph().compareTo(t1.getOph()));
            }
        });
    }

    private List<TableViewRowHeader> getSimpleRowHeaderList(String checked) {
        List<TableViewRowHeader> list = new ArrayList<>();
        //"angkut"
        for (int i = 0; i < angkuts.size(); i++) {
            String id = i + "-" + angkuts.get(i).getOph();
            TableViewRowHeader header = new TableViewRowHeader(id, checked);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<TableViewColumnHeader> getRandomColumnHeaderList() {
        List<TableViewColumnHeader> list = new ArrayList<>();

        for (int i = 0; i < HeaderColumn.length; i++) {
            TableViewColumnHeader header = new TableViewColumnHeader(String.valueOf(i), HeaderColumn[i]);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<List<TableViewCell>> getCellListForSortingTest() {
        List<List<TableViewCell>> list = new ArrayList<>();
        for (int i = 0; i < angkuts.size(); i++) {
            List<TableViewCell> cellList = new ArrayList<>();
            for (int j = 0; j < HeaderColumn.length; j++) {
                // Create dummy id.
                String id = j + "-" + angkuts.get(i).getOph();

                TableViewCell cell = null;
                switch (j){
                    case 0: {
                        String nama = "-";
                        if(angkuts.get(i).getOph() != null){
                            nama = angkuts.get(i).getOph();
                        }
                        cell = new TableViewCell(id, nama);
                        break;
                    }
                    case 1:{
                        cell = new TableViewCell(id, angkuts.get(i).getBlock());
                        break;
                    }
                    case 2: {
                        cell = new TableViewCell(id, angkuts.get(i).getJanjang());
                        break;
                    }
                    case 3: {
                        cell = new TableViewCell(id, angkuts.get(i).getBrondolan());
                        break;
                    }
                    case 4: {
                        cell = new TableViewCell(id, String.format("%.1f", angkuts.get(i).getBerat()));
                        break;
                    }
                }
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }

    private static String getRandomString() {
        Random r = new Random();
        String str = " a ";
        for (int i = 0; i < r.nextInt(); i++) {
            str = str + " a ";
        }

        return str;
    }

    public List<List<TableViewCell>> getCellList() {
        return getCellListForSortingTest();
    }

    public List<TableViewRowHeader> getRowHeaderList(String checked) {
        return getSimpleRowHeaderList(checked);
    }

    public List<TableViewColumnHeader> getColumnHeaderList() {
        return getRandomColumnHeaderList();
    }
}
