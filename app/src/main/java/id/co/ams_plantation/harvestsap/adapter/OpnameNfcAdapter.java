package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.OpnameNFC;

/**
 * Created on : 24,October,2022
 * Author     : Afid
 */

public class OpnameNfcAdapter extends RecyclerView.Adapter<OpnameNfcAdapter.Holder> {
    Context contextApdater;
    ArrayList<OpnameNFC> filter;
    ArrayList<OpnameNFC> origin;
    SimpleDateFormat dateFormat;

    public OpnameNfcAdapter(Context contextApdater, ArrayList<OpnameNFC> origin) {
        this.contextApdater = contextApdater;
        this.origin = origin;
        this.filter = origin;
        dateFormat = new SimpleDateFormat("EEEE, dd MMM yyyy HH:mm:ss");
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(contextApdater).inflate(R.layout.row_record,viewGroup,false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        holder.setValue(filter.get(i));
    }

    @Override
    public int getItemCount() {
        return filter!=null?filter.size():0;
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                filter = new ArrayList<>();
                String string = constraint.toString().toLowerCase();
                int i = 0 ;
                for (OpnameNFC opnameNFC : origin) {
                    if(opnameNFC.getUid().toLowerCase().contains(string) ||
                            opnameNFC.getAfdeling().toLowerCase().contains(string)
                    ){
                        filter.add(opnameNFC);
                    }
                }
                if (filter.size() > 0) {
                    results.count = filter.size();
                    results.values = filter;
                    return results;
                } else {
                    return null;
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null) {
                    filter = (ArrayList<OpnameNFC>) results.values;
                } else {
                    filter.clear();
                }
                notifyDataSetChanged();

            }
        };
    }

    public class Holder extends RecyclerView.ViewHolder {
        RelativeLayout rlImage;
        TextView item_ket;
        TextView item_ket1;
        TextView item_ket2;
        TextView item_ket3;
        CardView card_view;

        public Holder(View itemView) {
            super(itemView);
            card_view = (CardView) itemView.findViewById(R.id.card_view);
            rlImage = (RelativeLayout) itemView.findViewById(R.id.rlImage);
            item_ket = (TextView) itemView.findViewById(R.id.item_ket);
            item_ket1 = (TextView) itemView.findViewById(R.id.item_ket1);
            item_ket2 = (TextView) itemView.findViewById(R.id.item_ket2);
            item_ket3 = (TextView) itemView.findViewById(R.id.item_ket3);
            rlImage.setVisibility(View.GONE);
        }

        public void setValue(OpnameNFC opnameNFC) {
            item_ket.setText(opnameNFC.getUid());
            item_ket1.setText(opnameNFC.getAfdeling());
            item_ket3.setText(dateFormat.format(new Date(opnameNFC.getUpdateDate())));

            switch (opnameNFC.getStatus()){
                case OpnameNFC.SAVE:
                    item_ket2.setText("Belum Upload");
                    break;
                case OpnameNFC.UPLOAD:
                    item_ket2.setText("Sudah Upload");
                    break;
            }
        }
    }
}
