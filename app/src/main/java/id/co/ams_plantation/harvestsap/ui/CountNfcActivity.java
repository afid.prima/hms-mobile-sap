package id.co.ams_plantation.harvestsap.ui;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.io.File;

import butterknife.BindView;
import id.co.ams_plantation.harvestsap.Fragment.CountNfcInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.SpbInputFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.service.BluetoothService;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.NfcHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiSpbHelper;

/**
 * Created on : 11,November,2020
 * Author     : Afid
 */

public class CountNfcActivity extends BaseActivity {

    Activity activity;
    public int TYPE_NFC;
    public String valueNFC;

    @BindView(R.id.myCoordinatorLayout)
    public CoordinatorLayout myCoordinatorLayout;
    public Snackbar searchingGPS;

    /****************************************** NFC ******************************************************************/
    public Tag myTag;
    public NfcAdapter nfcAdapter;
    public PendingIntent pendingIntent;
    public IntentFilter[] writeTagFilters;
    @Override
    protected void initPresenter() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_layout);
        getSupportActionBar().hide();
        toolBarSetup();

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
        }

        File fileDBAngkut = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_COUNTNFC_SPB) + "/" +Encrypts.encrypt(GlobalHelper.TABLE_COUNTNFC_SPB)+ ".db");

        if(fileDBAngkut.exists()){
            fileDBAngkut.delete();
        }

        NfcHelper.readFromIntent(getIntent());
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[] { tagDetected };
//        dataAllUser = GlobalHelper.getAllUser();
        main();
    }

    private void main() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_container, CountNfcInputFragment.getInstance())
                .commit();
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        TextView textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backProses();
            }
        });
        textToolbar.setText(getResources().getString(R.string.count_nfc));
    }

    public void backProses() {
        File fileDBAngkut = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_COUNTNFC_SPB) + "/" +Encrypts.encrypt(GlobalHelper.TABLE_COUNTNFC_SPB)+ ".db");

        if(fileDBAngkut.exists()){
            fileDBAngkut.delete();
        }
        setResult(GlobalHelper.RESULT_COUNTNFCACTIVITY);
        finish();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        }
        valueNFC = NfcHelper.readFromIntent(intent);
        int formatNFC = NfcHelper.cekFormatNFC(valueNFC);
        if (valueNFC != null) {
            if (formatNFC != GlobalHelper.TYPE_NFC_SALAH) {
                if(valueNFC.length() == 5) {
                    Toast.makeText(this, getResources().getString(R.string.nfc_empty) + " " + GlobalHelper.LIST_NFC[formatNFC], Toast.LENGTH_SHORT).show();
                }else{
                    cekDataPassing(valueNFC, this);
                }
                TYPE_NFC = formatNFC;
            } else {
                Toast.makeText(this,R.string.format_not_valid,Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this,R.string.format_not_valid,Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed() {
        backProses();
    }

    @Override
    protected void onResume() {
        super.onResume();
        NfcHelper.WriteModeOn(nfcAdapter,this,pendingIntent,writeTagFilters);
    }

    @Override
    public void onPause(){
        super.onPause();
        NfcHelper.WriteModeOff(nfcAdapter,this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
