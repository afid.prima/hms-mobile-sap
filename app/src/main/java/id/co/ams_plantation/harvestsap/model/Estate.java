package id.co.ams_plantation.harvestsap.model;


public class Estate {

    private String estCode;
    private String estName;
    private String companyShortName;

    public Estate(String estCode, String estName, String companyShortName) {
        this.estCode = estCode;
        this.estName = estName;
        this.companyShortName = companyShortName;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getEstName() {
        return estName;
    }

    public void setEstName(String estName) {
        this.estName = estName;
    }

    public String getCompanyShortName() {
        return companyShortName;
    }

    public void setCompanyShortName(String companyShortName) {
        this.companyShortName = companyShortName;
    }
}
