package id.co.ams_plantation.harvestsap.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ResizeImage {
    public static Bitmap resizeImageSafely(String filePath, String outputPath) {
        // 1. Baca dimensi gambar asli tanpa memuat seluruh gambar
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        int originalWidth = options.outWidth;
        int originalHeight = options.outHeight;

        // 2. Hitung rasio skala untuk batas maksimum
        float scaleWidth = (float) GlobalHelper.maxWidthImages / originalWidth;
        float scaleHeight = (float) GlobalHelper.maxHeightImages / originalHeight;
        float scale = Math.min(scaleWidth, scaleHeight); // Ambil skala terkecil untuk mempertahankan aspek rasio

        int scaledWidth = (int) (originalWidth * scale);
        int scaledHeight = (int) (originalHeight * scale);

        // 3. Periksa apakah dimensi hasil skala masih lebih besar dari batas maksimum
        while (scaledWidth > GlobalHelper.maxWidthImages || scaledHeight > GlobalHelper.maxHeightImages) {
            scaleWidth = (float) GlobalHelper.maxWidthImages / scaledWidth;
            scaleHeight = (float) GlobalHelper.maxHeightImages / scaledHeight;
            scale = Math.min(scaleWidth, scaleHeight);

            scaledWidth = (int) (scaledWidth * scale);
            scaledHeight = (int) (scaledHeight * scale);
        }

        // 4. Tentukan inSampleSize untuk optimalisasi memori
        options.inSampleSize = calculateInSampleSize(options, scaledWidth, scaledHeight);
        options.inJustDecodeBounds = false;

        // 5. Dekode gambar dengan skala
        Bitmap scaledBitmap = BitmapFactory.decodeFile(filePath, options);

        // 6. Sesuaikan ukuran bitmap jika diperlukan
        Bitmap resizedBitmap = null;
        if (scaledBitmap != null && (scaledBitmap.getWidth() != scaledWidth || scaledBitmap.getHeight() != scaledHeight)) {
            resizedBitmap = Bitmap.createScaledBitmap(scaledBitmap, scaledWidth, scaledHeight, true);
            scaledBitmap.recycle(); // Hapus bitmap yang lama untuk menghemat memori
        } else {
            resizedBitmap = scaledBitmap;
        }

        // 7. Simpan gambar hasil resize ke file
        if (resizedBitmap != null) {
            try (FileOutputStream out = new FileOutputStream(new File(outputPath))) {
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out); // Kompresi JPEG kualitas 90%
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                resizedBitmap.recycle(); // Bebaskan memori
                System.gc(); // Paksa garbage collection
            }
        }

        // 8. Kembalikan bitmap yang sesuai
        return resizedBitmap;
    }

    // Helper untuk menghitung inSampleSize
    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) >= reqHeight && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

}
