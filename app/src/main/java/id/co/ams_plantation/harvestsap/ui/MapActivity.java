package id.co.ams_plantation.harvestsap.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.esri.android.map.Callout;
import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.LocationDisplayManager;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISLocalTiledLayer;
import com.esri.android.map.event.OnPanListener;
import com.esri.android.map.event.OnSingleTapListener;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.android.map.event.OnZoomListener;
import com.esri.android.map.ogc.kml.KmlLayer;
import com.esri.android.runtime.ArcGISRuntime;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polygon;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Graphic;
import com.esri.core.runtime.LicenseLevel;
import com.esri.core.runtime.LicenseResult;
import com.esri.core.symbol.CompositeSymbol;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.esri.core.symbol.TextSymbol;
import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.utils.Utils;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestsap.Fragment.SettingFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.SelectedAfdelingBlock;
import id.co.ams_plantation.harvestsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestsap.connection.Tag;
import id.co.ams_plantation.harvestsap.model.Block;
import id.co.ams_plantation.harvestsap.model.Langsiran;
import id.co.ams_plantation.harvestsap.model.SyncTime;
import id.co.ams_plantation.harvestsap.model.TPH;
import id.co.ams_plantation.harvestsap.model.TPHnLangsiran;
import id.co.ams_plantation.harvestsap.presenter.ConnectionPresenter;
import id.co.ams_plantation.harvestsap.util.ActivityLoggerHelper;
import id.co.ams_plantation.harvestsap.util.AfdelingBlockHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.InfoWindow;
import id.co.ams_plantation.harvestsap.util.InfoWindowChose;
import id.co.ams_plantation.harvestsap.util.RestoreHelper;
import id.co.ams_plantation.harvestsap.util.SetUpDataSyncHelper;
import id.co.ams_plantation.harvestsap.util.SyncHistoryHelper;
import id.co.ams_plantation.harvestsap.util.SyncTimeHelper;
import id.co.ams_plantation.harvestsap.util.TphHelper;
import id.co.ams_plantation.harvestsap.util.TujuanHelper;
import id.co.ams_plantation.harvestsap.util.UploadHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import id.co.ams_plantation.harvestsap.view.ApiView;

public class MapActivity extends BaseActivity implements ApiView {

    public static final int LongOperation_Setup_Data_AfdelingBlock = 0;
    public static final int LongOperation_Setup_Poin = 1;
    public static final int LongOperation_Setup_FormTPH = 2;
    public static final int LongOperation_Setup_FormLangsiran = 3;
    public static final int LongOperation_Upload_TPH = 4;
    public static final int LongOperation_Upload_Langsiran = 5;
    public static final int LongOperation_Get_TPH = 6;
    public static final int LongOperation_Get_Langsiran = 7;
    public static final int LongOperation_Download_TPH = 8;
    public static final int LongOperation_Download_Langsiran = 9;
    public static final int LongOperation_BackUp_HMS = 10;
    public static final int LongOperation_Get_MasterUser = 11;
    public static final int LongOperation_Download_MasterUser = 12;
    public static final int LongOperation_Get_Pemanen = 13;
    public static final int LongOperation_Download_Pemanen = 14;
    public static final int LongOperation_Get_PKS = 15;
    public static final int LongOperation_Download_PKS = 16;
    public static final int LongOperation_Counter_TPH = 17;
    public static final int LongOperation_Counter_Langsiran = 18;
    public static final int LongOperation_RestoreHMS = 19;
    public static final int LongOperation_Get_Operator = 20;
    public static final int LongOperation_Download_Operator = 21;
    public static final int LongOperation_SYNC_History = 22;

    public static final int Status_Close_Form_Close = 0;
    public static final int Status_Close_Form_Save = 1;
    public static final int Status_Close_Form_Deleted = 2;
    public static final int Status_Close_Form_Update = 3;

    public MapView mapView;
    public LocationDisplayManager locationDisplayManager;
    ArcGISLocalTiledLayer tiledLayer;
    KmlLayer kmlLayer;

    public FloatingActionButton fabCurrentLocation;
    public FloatingActionButton fabEstateLocation;

    public RelativeLayout container;
    public RelativeLayout rl_main;
    public SlidingUpPanelLayout mLayout;

    public static Long l1;

    RelativeLayout btnShowForm;
    RelativeLayout topLayout;
    CardView cvAfdBlock;
    RecyclerView rvAfdBlock;
    CheckBox checkBoxTphResurvey;
    CheckBox checkBoxTph;
    CheckBox checkBoxLangsir;
    CheckBox checkBoxInputHariIni;
    CheckBox checkBoxTph3Bulan;
    Button btnSelectAfdBlock;
    CardView footerCard;
    LinearLayout llheadumano;
    BoomMenuButton bmb;
    SegmentedButtonGroup segmentedButtonGroup;
    public CoordinatorLayout coor;

    Button btnCancelTph;
    Button btnCancelTransit;

    View tph_new;
    View langsiran_new;
    View pks_new;

    TextView sheetHeader;
    TextView tvDate;
    TextView tvLocation;
    ImageView ivchevronup;
    ImageView ivchevrondown;
    ImageView ivchevronupAfd;
    ImageView ivchevrondownAfd;

    CardView cvToken;
    TextView tvToken;

    Location location;

    TextView footerEstate;
    TextView footerUser;

    Snackbar sbGPS;
    boolean isMapLoaded=false;

    public int countTph =0;
    public int countTphResurvey =0;
    public int countTph3Bulan =0;
    public int countLangsiran =0;
    public int countHariIni =0;

    boolean manual;
    public GraphicsLayer graphicsLayer;
    public GraphicsLayer graphicsLayerTPH;
    public GraphicsLayer graphicsLayerTPHResurvey;
    public GraphicsLayer graphicsLayerTPH3Bulan;
    public GraphicsLayer graphicsLayerLangsiran;
    public GraphicsLayer graphicsLayerText;
    public GraphicsLayer graphicsLayerToday;

    public HashMap<Integer,TPHnLangsiran> hashpoint = new HashMap<>();
    public HashMap<Integer,Integer> hashpointToday = new HashMap<>();
    public HashMap<Integer,Integer> hashText = new HashMap<>();
    public Callout callout;

    public SpatialReference spatialReference;
    public ImageView ivCrosshair;
    public LinearLayout ll_latlon;
    public TextView tv_lat_manual,tv_lon_manual,tv_dis_manual,tv_deg_manual;

    public TphHelper tphHelper;
    public TujuanHelper tujuanHelper;
    public String locKmlMap;
    public ConnectionPresenter connectionPresenter;
    public SetUpDataSyncHelper setUpDataSyncHelper;
    UploadHelper uploadHelper;
    RestoreHelper restoreHelper;
    SyncHistoryHelper syncHistoryHelper;

    public TreeMap<String,ArrayList<Block>> afdelingBlocks;
    ArrayList<Polygon> blocksSelected;
    AfdelingBlockHelper afdelingBlockHelper;

    boolean isTph;
    int idGrapTemp;
    int idGrapSelected;
    TPHnLangsiran tpHnLangsiranNowSelected;

    ArrayList<File> ALselectedImage;

    JSONObject objSetUp;
    ServiceResponse responseApi;
    public AlertDialog alertDialog;
    SimpleDateFormat dateFormat;

    @Override
    protected void initPresenter() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LicenseResult licenseResult = ArcGISRuntime.setClientId("uFfCzKhpVfi0ggjz");
        LicenseLevel licenseLevel = ArcGISRuntime.License.getLicenseLevel();
        Log.e("License Result", licenseResult.toString() + " | " + licenseLevel.toString());
        setContentView(R.layout.map_activity);
        getSupportActionBar().hide();
        startTimeScreen = new Date();

        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        rl_main = (RelativeLayout) findViewById(R.id.rl_main);
        mapView = (MapView) findViewById(R.id.map);
        topLayout = (RelativeLayout) findViewById(R.id.topLayout);
        container = (RelativeLayout) findViewById(R.id.container);
        fabCurrentLocation = (FloatingActionButton) findViewById(R.id.fabZoomToLocation);
        fabEstateLocation = (FloatingActionButton) findViewById(R.id.fabZoomToEstate);
        cvAfdBlock = (CardView) findViewById(R.id.cvAfdBlock);
        rvAfdBlock = (RecyclerView) findViewById(R.id.rvAfdBlock);
        checkBoxTph = (CheckBox) findViewById(R.id.checkBoxTph);
        checkBoxTphResurvey = (CheckBox) findViewById(R.id.checkBoxTphResurvey);
        checkBoxLangsir = (CheckBox) findViewById(R.id.checkBoxLangsir);
        checkBoxInputHariIni = (CheckBox) findViewById(R.id.checkBoxInputHariIni);
        checkBoxTph3Bulan = (CheckBox) findViewById(R.id.checkBoxTph3Bulan);
        btnSelectAfdBlock = (Button) findViewById(R.id.btnSelectAfdBlock);
        footerCard = (CardView) findViewById(R.id.footer_card);
        bmb = (BoomMenuButton) findViewById(R.id.bmb);
        btnShowForm = (RelativeLayout) findViewById(R.id.btnShowForm);
        llheadumano = (LinearLayout) findViewById(R.id.llheadumano);
        coor = (CoordinatorLayout) findViewById(R.id.myCoordinatorLayout);

        tph_new = (View) findViewById(R.id.tph_new);
        langsiran_new = (View) findViewById(R.id.langsiran_new);
        pks_new = (View) findViewById(R.id.pks_new);

        segmentedButtonGroup = (SegmentedButtonGroup) findViewById(R.id.segmentedButtonGroup);
        sheetHeader = (TextView) findViewById(R.id.sheetHeader);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvLocation = (TextView) findViewById(R.id.tvLocation);
        ivchevronup = (ImageView) findViewById(R.id.ivchevronup);
        ivchevrondown = (ImageView) findViewById(R.id.ivchevrondown);
        ivchevronupAfd = (ImageView) findViewById(R.id.ivchevronupAfd);
        ivchevrondownAfd = (ImageView) findViewById(R.id.ivchevrondownAfd);

        btnCancelTph = (Button) findViewById(R.id.btnCancel);
        btnCancelTransit = (Button) findViewById(R.id.btnCancelLangsiran);

        footerEstate = (TextView) findViewById(R.id.footerEstate);
        footerUser = (TextView) findViewById(R.id.footerUser);

        ivCrosshair = (ImageView) findViewById(R.id.iv_crosshair);
        ll_latlon = (LinearLayout) findViewById(R.id.ll_latlon);
        tv_lat_manual = (TextView) findViewById(R.id.tv_lat_manual);
        tv_lon_manual = (TextView) findViewById(R.id.tv_lon_manual);
        tv_deg_manual = (TextView) findViewById(R.id.tv_deg_manual);
        tv_dis_manual = (TextView) findViewById(R.id.tv_dis_manual);

        cvToken = (CardView) findViewById(R.id.cvToken);
        tvToken = (TextView) findViewById(R.id.tvToken);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Long exptDate = sdf.parse(GlobalHelper.getUser().getExpirationDate()).getTime();
            long sisaHari = exptDate - System.currentTimeMillis();
            sisaHari = sisaHari / (24 * 60 * 60 * 1000);
            if(sisaHari <= GlobalHelper.NOTIF_TOKEN_DAYS){
                tvToken.setText(sisaHari+" "+getResources().getString(R.string.days_left));
            }else{
                cvToken.setVisibility(View.GONE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        afdelingBlockHelper = new AfdelingBlockHelper(this);
        dateFormat = new SimpleDateFormat("dd-MMMM-yyyy");

        graphicsLayer = new GraphicsLayer();
        graphicsLayerTPH = new GraphicsLayer();
        graphicsLayerTPHResurvey = new GraphicsLayer();
        graphicsLayerTPH3Bulan = new GraphicsLayer();
        graphicsLayerLangsiran = new GraphicsLayer();
        graphicsLayerText = new GraphicsLayer();
        graphicsLayerToday =new GraphicsLayer();

        manual = false;
        tphHelper = new TphHelper(MapActivity.this);
        tujuanHelper = new TujuanHelper(MapActivity.this);
        setUpDataSyncHelper = new SetUpDataSyncHelper(MapActivity.this);
        uploadHelper = new UploadHelper(MapActivity.this);
        connectionPresenter = new ConnectionPresenter(MapActivity.this);
        restoreHelper = new RestoreHelper(MapActivity.this);
        syncHistoryHelper = new SyncHistoryHelper(MapActivity.this);

        mLayout.setTouchEnabled(false);
        llheadumano.setVisibility(View.GONE);
        ivchevrondownAfd.setVisibility(View.GONE);

        footerUser.setText(GlobalHelper.getUser().getUserFullName());
        footerEstate.setText(GlobalHelper.getEstate().getCompanyShortName() + " - "
                + GlobalHelper.getEstate().getEstName());
        fabEstateLocation.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_landscape).sizeDp(24)
                .colorRes(R.color.White));
        fabCurrentLocation.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_my_location)
                .sizeDp(24)
                .colorRes(R.color.White));
        ivchevronup.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_up)
                .sizeDp(18)
                .colorRes(R.color.Gray));
        ivchevrondown.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_down)
                .sizeDp(18)
                .colorRes(R.color.Gray));
        ivchevronupAfd.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_up)
                .sizeDp(18)
                .colorRes(R.color.Gray));
        ivchevrondownAfd.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_down)
                .sizeDp(18)
                .colorRes(R.color.Gray));
        ivchevronupAfd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rvAfdBlock.setVisibility(View.GONE);
                ivchevronupAfd.setVisibility(View.GONE);
                ivchevrondownAfd.setVisibility(View.VISIBLE);
            }
        });
        ivchevrondownAfd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rvAfdBlock.setVisibility(View.VISIBLE);
                ivchevronupAfd.setVisibility(View.VISIBLE);
                ivchevrondownAfd.setVisibility(View.GONE);
            }
        });
        fabEstateLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mapView.isLoaded()){
                    if(tiledLayer!=null){
                        mapView.setExtent(tiledLayer.getFullExtent());
                    }
                }
            }
        });
        mapView.setOnSingleTapListener(new OnSingleTapListener() {
            @Override
            public void onSingleTap(float v, float v1) {
                if(isMapLoaded){
                    try {
                        defineTapSetup(v, v1);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
        fabCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mapView.isLoaded()){
                    if( locationDisplayManager!=null) {
                        if (locationDisplayManager.getLocation() != null)
                            zoomToLocation(locationDisplayManager.getLocation().getLatitude()
                                    , locationDisplayManager.getLocation().getLongitude(), spatialReference);

                        if (idGrapTemp != 0) {
                            location = locationDisplayManager.getLocation();
                            ((BaseActivity) MapActivity.this).currentlocation = locationDisplayManager.getLocation();
                            addGrapTemp(location.getLatitude(), location.getLongitude());
                        }
                    }
                }

            }
        });
        checkBoxTph.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                graphicsLayerTPH.setVisible(isChecked);
                if(isChecked && checkBoxInputHariIni.isChecked()){
                    checkBoxInputHariIni.setChecked(false);
                }
            }
        });
        checkBoxTphResurvey.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                graphicsLayerTPHResurvey.setVisible(isChecked);
                if(isChecked && checkBoxInputHariIni.isChecked()){
                    checkBoxInputHariIni.setChecked(false);
                }
            }
        });
        checkBoxTph3Bulan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                graphicsLayerTPH3Bulan.setVisible(isChecked);
                if(isChecked && checkBoxInputHariIni.isChecked()){
                    checkBoxInputHariIni.setChecked(false);
                }
            }
        });
        checkBoxLangsir.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                graphicsLayerLangsiran.setVisible(isChecked);
                if(isChecked && checkBoxInputHariIni.isChecked()){
                    checkBoxInputHariIni.setChecked(false);
                }
            }
        });
        checkBoxInputHariIni.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                graphicsLayerToday.setVisible(isChecked);
                if(isChecked){
                    checkBoxLangsir.setChecked(false);
                    checkBoxTphResurvey.setChecked(false);
                    checkBoxTph3Bulan.setChecked(false);
                    checkBoxTph.setChecked(false);
                }else{
                    checkBoxLangsir.setChecked(true);
                    checkBoxTphResurvey.setChecked(true);
                    checkBoxTph3Bulan.setChecked(true);
                    checkBoxTph.setChecked(true);
                }
            }
        });
        btnSelectAfdBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afdelingBlockHelper.setUpTreeeView();
            }
        });
        segmentedButtonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(int position) {
                switch (position){
                    case 0:{
                        manual = false;
                        switchCrossHairUI(false);
                        double lon = 0;
                        double lat = 0;
                        if (isTph) {
                            lat = tphHelper.latitude;
                            lon = tphHelper.longitude;
                        } else {
                            lat = tujuanHelper.latitude;
                            lon = tujuanHelper.longitude;
                        }

                        JSONObject object = new JSONObject();
                        try {
                            object.put("lat", lat);
                            object.put("lon",lon);

                            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.LATLON_PENDAFTARAN, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString(HarvestApp.LATLON_PENDAFTARAN,object.toString());
                            editor.apply();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        addGrapTemp(lat,lon);
                        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                        break;
                    }
                    case 1:{
                        manual = true;
                        setDefaultManual();
                        break;
                    }
                }
            }
        });
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                if(slideOffset == 0){
                    mLayout.setTouchEnabled(true);
                }
                Log.i("MapActivity", "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {

                if(footerCard.getVisibility() == View.VISIBLE){
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
                if(manual && newState ==  SlidingUpPanelLayout.PanelState.EXPANDED){
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.please_stop_manual_point),Toast.LENGTH_SHORT).show();
                }
                if(newState ==  SlidingUpPanelLayout.PanelState.EXPANDED){
                    ivchevronup.setVisibility(View.GONE);
                    ivchevrondown.setVisibility(View.VISIBLE);
                }else if (newState ==  SlidingUpPanelLayout.PanelState.ANCHORED){
                    ivchevronup.setVisibility(View.GONE);
                    ivchevrondown.setVisibility(View.VISIBLE);
                }else if (newState ==  SlidingUpPanelLayout.PanelState.COLLAPSED){
                    ivchevronup.setVisibility(View.VISIBLE);
                    ivchevrondown.setVisibility(View.GONE);
                }
            }
        });
        btnShowForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED){
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }if(mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED){
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }else if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED){
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                }
            }
        });
//        mLayout.setFadeOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//            }
//        });

        menuSetup();
        mapSetup();
        checkBoxInputHariIni.setChecked(false);
        new LongOperation().execute(String.valueOf(LongOperation_Setup_Data_AfdelingBlock));
    }

    public void setUpFormTph(TPH tph){
        if(locationDisplayManager.getLocation() == null) {
            sbGPS = Snackbar.make(coor, "Searching GPS", Snackbar.LENGTH_INDEFINITE);
            sbGPS.show();
            return;
        }
        LogIntervalStep("getLocation");
        llheadumano.setVisibility(View.VISIBLE);
        mLayout.setTouchEnabled(true);
        mLayout.setPanelHeight(Utils.convertDpToPx(this,65));

        location = locationDisplayManager.getLocation();
        footerCard.setVisibility(View.GONE);
        bmb.setVisibility(View.GONE);
        cvAfdBlock.setVisibility(View.GONE);
        segmentedButtonGroup.setVisibility(View.VISIBLE);

        graphicsLayerTPH.setVisible(false);
        graphicsLayerTPHResurvey.setVisible(false);
        graphicsLayerTPH3Bulan.setVisible(false);
        graphicsLayerLangsiran.setVisible(false);
        graphicsLayerText.setVisible(false);
        graphicsLayer.removeAll();
        idGrapTemp = 0;

        isTph = true;
        tph_new.setVisibility(View.VISIBLE);
        langsiran_new.setVisibility(View.GONE);
        pks_new.setVisibility(View.GONE);
        btnCancelTph.setVisibility(View.VISIBLE);

        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd-MMMM-yyyy");

        sheetHeader.setText(tph == null ? getResources().getString(R.string.new_tph): tph.getNamaTph());
        tvDate.setText(sdf.format(System.currentTimeMillis()));
        tvLocation.setText(String.format("Lat : %.5f | Lon : %.5f | Acc : %.0f", location.getLatitude(),
                location.getLongitude(), location.getAccuracy()));
        LogIntervalStep("prepareSetUpUmano");

        // jika dari map popupset latlon seusia denggan tph yang di pilih
        if(tph == null) {
            idGrapSelected = 0;
            tphHelper.latitude = location.getLatitude();
            tphHelper.longitude = location.getLongitude();
            tphHelper.latawal = location.getLatitude();
            tphHelper.longawal = location.getLongitude();
        }else{
            idGrapSelected = getIdGrapSelected(new TPHnLangsiran(tph.getNoTph(), tph));
            tpHnLangsiranNowSelected = new TPHnLangsiran(tph.getNoTph(),tph);
            tphHelper.latitude = tph.getLatitude();
            tphHelper.longitude = tph.getLongitude();
            tphHelper.latawal = tph.getLatitude();
            tphHelper.longawal = tph.getLongitude();
        }
        tphHelper.setupFormInput(tph_new,tph);
        LogIntervalStep("setUpUmanoDone");

        addGrapTemp(tphHelper.latawal,tphHelper.longawal);
        LogIntervalStep("addGrapTemp");

        zoomToLocation(tphHelper.latawal,tphHelper.longawal,spatialReference);

        LogIntervalStep("zoomToLocation");

        btnCancelTph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeForm(Status_Close_Form_Close);
            }
        });
    }

    public void setUpFormLangsiran(Langsiran langsiran){
        if(locationDisplayManager.getLocation() == null) {
            sbGPS = Snackbar.make(coor, "Searching GPS", Snackbar.LENGTH_INDEFINITE);
            sbGPS.show();
            return;
        }
        llheadumano.setVisibility(View.VISIBLE);
        mLayout.setTouchEnabled(true);
        mLayout.setPanelHeight(Utils.convertDpToPx(this,65));

        location = locationDisplayManager.getLocation();
        footerCard.setVisibility(View.GONE);
        bmb.setVisibility(View.GONE);
        cvAfdBlock.setVisibility(View.GONE);
        segmentedButtonGroup.setVisibility(View.VISIBLE);

        graphicsLayerTPH.setVisible(false);
        graphicsLayerTPHResurvey.setVisible(false);
        graphicsLayerTPH3Bulan.setVisible(false);
        graphicsLayerLangsiran.setVisible(false);
        graphicsLayerText.setVisible(false);
        graphicsLayer.removeAll();
        idGrapTemp = 0;

        isTph = false;
        tph_new.setVisibility(View.GONE);
        langsiran_new.setVisibility(View.VISIBLE);
        pks_new.setVisibility(View.GONE);
        btnCancelTransit.setVisibility(View.VISIBLE);

        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd-MMMM-yyyy");

        sheetHeader.setText(langsiran == null ? getResources().getString(R.string.new_langsiran): langsiran.getNamaTujuan());
        tvDate.setText(sdf.format(System.currentTimeMillis()));
        tvLocation.setText(String.format("Lat : %.5f | Lon : %.5f | Acc : %.0f", location.getLatitude(),
                location.getLongitude(), location.getAccuracy()));

        // jika dari map popupset latlon seusia denggan tph yang di pilih
        if(langsiran == null) {
            idGrapSelected = 0;
            tujuanHelper.latitude = location.getLatitude();
            tujuanHelper.longitude = location.getLongitude();
            tujuanHelper.latawal = location.getLatitude();
            tujuanHelper.longawal = location.getLongitude();
        }else{
            idGrapSelected = getIdGrapSelected(new TPHnLangsiran(langsiran.getIdTujuan(), langsiran));
            tpHnLangsiranNowSelected = new TPHnLangsiran(langsiran.getIdTujuan(),langsiran);
            tujuanHelper.latitude = langsiran.getLatitude();
            tujuanHelper.longitude = langsiran.getLongitude();
            tujuanHelper.latawal = langsiran.getLatitude();
            tujuanHelper.longawal = langsiran.getLongitude();
        }
        tujuanHelper.setupFormInput(langsiran_new,langsiran);
        addGrapTemp(tujuanHelper.latawal,tujuanHelper.longawal);
        zoomToLocation(tujuanHelper.latawal,tujuanHelper.longawal,spatialReference);

        btnCancelTransit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeForm(Status_Close_Form_Close);
            }
        });
    }

    public void closeForm(int stat){
        footerCard.setVisibility(View.VISIBLE);
        bmb.setVisibility(View.VISIBLE);
        cvAfdBlock.setVisibility(View.VISIBLE);
        segmentedButtonGroup.setVisibility(View.GONE);
        llheadumano.setVisibility(View.GONE);
        mLayout.setTouchEnabled(false);
        mLayout.setPanelHeight(footerCard.getHeight());
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

        graphicsLayer.removeAll();
        idGrapTemp = 0;
        graphicsLayerLangsiran.setVisible(checkBoxLangsir.isChecked());
        graphicsLayerTPH.setVisible(checkBoxTph.isChecked());
        graphicsLayerTPHResurvey.setVisible(checkBoxTphResurvey.isChecked());
        graphicsLayerTPH3Bulan.setVisible(checkBoxTph3Bulan.isChecked());
        graphicsLayerText.setVisible(true);
        if(stat == Status_Close_Form_Deleted || stat == Status_Close_Form_Update ) {
            if (isTph) {
                graphicsLayerTPH.removeGraphic(idGrapSelected);
            } else {
                graphicsLayerLangsiran.removeGraphic(idGrapSelected);
            }
            graphicsLayerText.removeGraphic(hashText.get(idGrapSelected));
            if(stat == Status_Close_Form_Deleted) {
                Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.dg_message_delete_success_tph), Toast.LENGTH_SHORT).show();
            }
        }else if (stat == Status_Close_Form_Save ){
            if(isTph){
                countTph++;
            }else{
                countLangsiran++;
            }
            countHariIni++;
        }
        updateCounterKet();
        idGrapSelected = 0;
        isTph = false;
    }

    public int getIdGrapSelected(TPHnLangsiran tpHnLangsiran){
        for (HashMap.Entry<Integer, TPHnLangsiran> entry : hashpoint.entrySet()) {
            if(tpHnLangsiran.getTph() != null && entry.getValue().getTph() != null) {
                if (tpHnLangsiran.getTph().getNoTph().equals(entry.getValue().getTph().getNoTph())) {
                    return entry.getKey();
                }
            }else if (tpHnLangsiran.getLangsiran() != null && entry.getValue().getLangsiran() != null){
                if (tpHnLangsiran.getLangsiran().getIdTujuan().equals(entry.getValue().getLangsiran().getIdTujuan())) {
                    return entry.getKey();
                }
            }
        }
        return 0;
    }

    public void setAfdelingBlocksValue(){
        rvAfdBlock.setLayoutManager(new LinearLayoutManager(this));
        SelectedAfdelingBlock adapter = new SelectedAfdelingBlock(this,afdelingBlocks);
        rvAfdBlock.setAdapter(adapter);
        Log.e("setAfdelingBlocksValue","1");
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        new LongOperation().execute(String.valueOf(LongOperation_Setup_Poin));

        Log.e("setAfdelingBlocksValue","2");
    }

    public void addGrapTemp(Double lat, Double lon){
        if(mapView!=null && mapView.isLoaded()){
            Point fromPoint = new Point(lon, lat);
            Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

            CompositeSymbol cms = new CompositeSymbol();
            SimpleMarkerSymbol sms = new SimpleMarkerSymbol(Color.GREEN, Utils.convertDpToPx(this, GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.X);
            SimpleLineSymbol sls = new SimpleLineSymbol(Color.GREEN, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
            sms.setOutline(sls);
            cms.add(sms);
            Graphic pointGraphic = new Graphic(toPoint, cms);
            LogIntervalStep("addGrapTemp 1");
            if(isTph) {
                tphHelper.setupBlokAfdeling(lat, lon);
            }else{
                tujuanHelper.setupBlokAfdeling(lat, lon);
            }
            if(idGrapTemp!= 0){
                graphicsLayer.removeGraphic(idGrapTemp);
            }
            LogIntervalStep("addGrapTemp 2");
            idGrapTemp = graphicsLayer.addGraphic(pointGraphic);
            LogIntervalStep("addGrapTemp 3");
        }else{
            idGrapTemp = 0;
        }
    }


    public void setupPoint(){
        removeAllGraphic();

        countTph =0;
        countLangsiran =0;
        countTphResurvey =0;
        countTph3Bulan =0;
        countHariIni = 0;
        ArrayList<String> list = new ArrayList<>();
        blocksSelected = new ArrayList<>();

        for ( TreeMap.Entry<String,ArrayList<Block>> entry : afdelingBlocks.entrySet() ) {
            for(int i = 0; i < entry.getValue().size();i++){
                if(entry.getValue().get(i).getSelected()){
                    list.add(entry.getValue().get(i).getBlock());
                    for (int j = 0; j < entry.getValue().get(i).getPolygons().size();j++) {
                        blocksSelected.add(entry.getValue().get(i).getPolygons().get(j));
                    }
                }
            }
        }

        ArrayList<TPHnLangsiran> tphArrayList = tphHelper.setDataTphnLangsiran(GlobalHelper.getEstate().getEstCode(),list.toArray(new String[list.size()]));
        JSONArray array = new JSONArray();
        for(TPHnLangsiran tpHnLangsiran: tphArrayList){
            if(tpHnLangsiran.getLangsiran() != null){
                addGraphicLangsiran(tpHnLangsiran);
                countLangsiran++;
                if(tpHnLangsiran.getLangsiran().getStatus() == Langsiran.STATUS_LANGSIRAN_BELUM_UPLOAD){
                    countHariIni++;
                }
            }else if (tpHnLangsiran.getTph() != null){
                addGraphicTph(tpHnLangsiran);
                if(tpHnLangsiran.getTph().getResurvey() == 1){
                    countTphResurvey++;
                }else if(tpHnLangsiran.getTph().getPanen3Bulan() == 0){
                    countTph3Bulan++;
                }else {
                    if(tpHnLangsiran.getTph().getStatus() != TPH.STATUS_TPH_INACTIVE_QC_MOBILE && tpHnLangsiran.getTph().getStatus() != TPH.STATUS_TPH_DELETED) {
                        countTph++;
                    }
                }

                if(tpHnLangsiran.getTph().getStatus() == TPH.STATUS_TPH_BELUM_UPLOAD || tpHnLangsiran.getTph().getReasonUnharvestTPH() != null){
                    countHariIni++;
                }
                Gson gson = new Gson();
                try {
                    array.put(new JSONObject(gson.toJson(tpHnLangsiran.getTph())));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        GlobalHelper.writeFileContent(GlobalHelper.getDatabasePathHMSTemp() + "/maptph.json",array.toString());

        if(blocksSelected.size() == 1) {
            Polygon polygon = new Polygon();
            for(Polygon geometry:blocksSelected) {
                if (geometry != null) {
                    polygon = (Polygon) GeometryEngine.project(blocksSelected.get(0), SpatialReference.create(SpatialReference.WKID_WGS84), mapView.getSpatialReference());
                    break;
                }
            }
            mapView.setExtent(polygon);
        }else if (blocksSelected.size() > 1){
            Polygon[] geometries = new Polygon[blocksSelected.size()];
            for (int i = 0 ; i < blocksSelected.size(); i++) {
                Polygon geometry = blocksSelected.get(i);
                if (geometry != null) {
                    Polygon polygon = (Polygon) GeometryEngine.project(geometry,
                            SpatialReference.create(SpatialReference.WKID_WGS84),
                            mapView.getSpatialReference());
                    geometries[i]= polygon;
                }
            }
            mapView.setExtent(GeometryEngine.union(geometries,mapView.getSpatialReference()));
        }else{
            if(tiledLayer!=null){
                mapView.setExtent(tiledLayer.getFullExtent());
            }
        }
    }

    public void removeResurvey(TPHnLangsiran tpHnLangsiran){
        try {
            if(tpHnLangsiranNowSelected.getIdTPHnLangsiran().equals(tpHnLangsiran.getIdTPHnLangsiran())){
                if(tpHnLangsiranNowSelected.getTph().getResurvey() == 1){
                    graphicsLayerTPHResurvey.removeGraphic(idGrapSelected);
                    countTphResurvey --;
                    countTph++;
                    countHariIni++;
                }
            }
        }catch (Exception e){

        }
        tpHnLangsiranNowSelected = null;
    }

    public void addGraphicTph(TPHnLangsiran tpHnLangsiran){

        Point fromPoint = new Point(tpHnLangsiran.getTph().getLongitude(), tpHnLangsiran.getTph().getLatitude());
        Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

        CompositeSymbol cms = new CompositeSymbol();
        int color = tpHnLangsiran.getTph().getResurvey() == 1 ? TPHnLangsiran.COLOR_TPH_RESURVEY : TPHnLangsiran.COLOR_TPH;
        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(color, Utils.convertDpToPx(this, GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.CIRCLE);
        SimpleLineSymbol sls = new SimpleLineSymbol(TPHnLangsiran.COLOR_BELUMUPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        if(tpHnLangsiran.getTph().getStatus() == TPH.STATUS_TPH_MASTER || tpHnLangsiran.getTph().getStatus() == TPH.STATUS_TPH_UPLOAD){
            sls = new SimpleLineSymbol(TPHnLangsiran.COLOR_UPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        }
        sms.setOutline(sls);
        cms.add(sms);

        Graphic pointGraphic = new Graphic(toPoint, cms);
        int idgrap = 0;
        if(tpHnLangsiran.getTph().getResurvey() == 1) {
            idgrap = graphicsLayerTPHResurvey.addGraphic(pointGraphic);
        }else{
            if(tpHnLangsiran.getTph().getStatus() != TPH.STATUS_TPH_INACTIVE_QC_MOBILE && tpHnLangsiran.getTph().getStatus() != TPH.STATUS_TPH_DELETED) {
                if (tpHnLangsiran.getTph().getPanen3Bulan() == 0) {
                    idgrap = graphicsLayerTPH3Bulan.addGraphic(pointGraphic);
                }else if (tpHnLangsiran.getTph().getReasonUnharvestTPH() != null){
                    idgrap = graphicsLayerTPH.addGraphic(pointGraphic);
                } else {
                    idgrap = graphicsLayerTPH.addGraphic(pointGraphic);
                }
            }else if (tpHnLangsiran.getTph().getReasonUnharvestTPH() != null){
                idgrap = graphicsLayerTPH.addGraphic(pointGraphic);
            }
        }
        hashpoint.put(idgrap,tpHnLangsiran);

        if(tpHnLangsiran.getTph().getStatus() == TPH.STATUS_TPH_BELUM_UPLOAD || tpHnLangsiran.getTph().getReasonUnharvestTPH() != null){
            hashpointToday.put(idgrap,graphicsLayerToday.addGraphic(pointGraphic));
        }

        TextSymbol.HorizontalAlignment horizontalAlignment = TextSymbol.HorizontalAlignment.CENTER;
        TextSymbol.VerticalAlignment verticalAlignment = TextSymbol.VerticalAlignment.BOTTOM;

        int Tcolor = Color.parseColor("#000000");
        TextSymbol txtSymbol = new TextSymbol(13,tpHnLangsiran.getTph().getNamaTph(),Tcolor) ;
        txtSymbol.setHorizontalAlignment(horizontalAlignment);
        txtSymbol.setVerticalAlignment(verticalAlignment);

        pointGraphic = new Graphic(toPoint,txtSymbol);
        if(tpHnLangsiran.getTph().getResurvey() == 1) {
            hashText.put(idgrap,graphicsLayerText.addGraphic(pointGraphic));
        }else{
            if(tpHnLangsiran.getTph().getStatus() != TPH.STATUS_TPH_INACTIVE_QC_MOBILE && tpHnLangsiran.getTph().getStatus() != TPH.STATUS_TPH_DELETED) {
                hashText.put(idgrap,graphicsLayerText.addGraphic(pointGraphic));
            }
        }
    }

    public void addGraphicLangsiran(TPHnLangsiran tpHnLangsiran){
        Point fromPoint = new Point(tpHnLangsiran.getLangsiran().getLongitude(), tpHnLangsiran.getLangsiran().getLatitude());
        Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

        CompositeSymbol cms = new CompositeSymbol();
        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(TPHnLangsiran.COLOR_LANGSIRAN, Utils.convertDpToPx(this, GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.CIRCLE);
        SimpleLineSymbol sls = new SimpleLineSymbol(TPHnLangsiran.COLOR_BELUMUPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        if(tpHnLangsiran.getLangsiran().getStatus() == Langsiran.STATUS_LANGSIRAN_MASTER || tpHnLangsiran.getLangsiran().getStatus() == Langsiran.STATUS_LANGSIRAN_UPLOAD){
            sls = new SimpleLineSymbol(TPHnLangsiran.COLOR_UPLOAD, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        }
        sms.setOutline(sls);
        cms.add(sms);
        Graphic pointGraphic = new Graphic(toPoint, cms);
        int idgrap = graphicsLayerLangsiran.addGraphic(pointGraphic);
        hashpoint.put(idgrap,tpHnLangsiran);

        if(tpHnLangsiran.getLangsiran().getStatus() == Langsiran.STATUS_LANGSIRAN_BELUM_UPLOAD){
            hashpointToday.put(idgrap,graphicsLayerToday.addGraphic(pointGraphic));
        }

        TextSymbol.HorizontalAlignment horizontalAlignment = TextSymbol.HorizontalAlignment.CENTER;
        TextSymbol.VerticalAlignment verticalAlignment = TextSymbol.VerticalAlignment.BOTTOM;

        int Tcolor = Color.parseColor("#000000");
        TextSymbol txtSymbol = new TextSymbol(13,tpHnLangsiran.getLangsiran().getNamaTujuan(),Tcolor) ;
        txtSymbol.setHorizontalAlignment(horizontalAlignment);
        txtSymbol.setVerticalAlignment(verticalAlignment);

        pointGraphic = new Graphic(toPoint,txtSymbol);
        hashText.put(idgrap,graphicsLayerText.addGraphic(pointGraphic));
    }

    private void mapSetup(){
        //
        mapView.removeAll();
        graphicsLayer.removeAll();
        mapView.setMinScale(250000.0d);
        mapView.setMaxScale(1000.0d);
        mapView.enableWrapAround(true);

        String locTiledMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VKM);
        if(locTiledMap != null){
            File file = new File(locTiledMap);
            if(!file.exists()){
                locTiledMap = null;
            }
        }

        if(locTiledMap != null){

            locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
            if (locKmlMap != null) {
                kmlLayer = new KmlLayer(locKmlMap);
//                kmlLayer.setOpacity(0.03f);
                mapView.addLayer(kmlLayer);
                kmlLayer.setVisible(false);
            }
            tiledLayer = new ArcGISLocalTiledLayer(locTiledMap, true);
            mapView.addLayer(tiledLayer);
        }else{
            locTiledMap = GlobalHelper.getFilePath(GlobalHelper.TYPE_VKM, GlobalHelper.getEstate());
            if (locTiledMap != null) {
                locTiledMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VKM);
                tiledLayer = new ArcGISLocalTiledLayer(locTiledMap, true);
                mapView.addLayer(tiledLayer);
            }

            locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
            if(locKmlMap!=null){
                kmlLayer = new KmlLayer(locKmlMap);
//                kmlLayer.setOpacity(0.8f);
                mapView.addLayer(kmlLayer);
                kmlLayer.setVisible(true);
            }
        }

        mapView.setOnStatusChangedListener(new OnStatusChangedListener() {
            @Override
            public void onStatusChanged(Object o, STATUS status) {
                if(o==mapView && status==STATUS.INITIALIZED){
                    spatialReference = mapView.getSpatialReference();
                    isMapLoaded = true;
                    locationListenerSetup();
                    fabEstateLocation.callOnClick();
                }
            }
        });
        mapView.setOnPanListener(new OnPanListener() {
            @Override
            public void prePointerMove(float v, float v1, float v2, float v3) {
                if(mapView.isLoaded()){
                    if(mapView.getSpatialReference()!=null){
                        Point prePoint = mapView.getCenter();
                        Point point = (Point) GeometryEngine.project(prePoint,mapView.getSpatialReference(),SpatialReference.create(SpatialReference.WKID_WGS84));
                    }

                }
            }

            @Override
            public void postPointerMove(float v, float v1, float v2, float v3) {
                if(mapView.isLoaded()){
                    if(mapView.getSpatialReference()!=null){
                        if(manual) {
                            Point prePoint = mapView.getCenter();
                            Point point = (Point) GeometryEngine.project(prePoint, mapView.getSpatialReference(), SpatialReference.create(SpatialReference.WKID_WGS84));
                            double imeter = GlobalHelper.distance(location.getLatitude(), point.getY(), location.getLongitude(), point.getX());
                            double bearing = GlobalHelper.bearing(location.getLatitude(), point.getY(), location.getLongitude(), point.getX());
                            tv_lat_manual.setText(String.format("%.5f", point.getY()));
                            tv_lon_manual.setText(String.format("%.5f", point.getX()));
                            tv_dis_manual.setText(String.format("%.0f m", imeter));
                            tv_deg_manual.setText(String.format("%.0f", bearing));
                            if (point.getY() != 0.0 && point.getX() != 0.0) {
                                if (imeter <= GlobalHelper.MAX_RADIUS_MAPMENU) {
                                    if (isTph) {
                                        tphHelper.latitude = point.getY();
                                        tphHelper.longitude = point.getX();
                                    } else {
                                        tujuanHelper.latitude = point.getY();
                                        tujuanHelper.longitude = point.getX();
                                    }
                                    ll_latlon.setBackgroundColor(Color.WHITE);
                                } else {
                                    ll_latlon.setBackgroundColor(Color.RED);
                                }
                            }
                        }
                    }

                }
            }

            @Override
            public void prePointerUp(float v, float v1, float v2, float v3) {

            }

            @Override
            public void postPointerUp(float v, float v1, float v2, float v3) {

            }
        });
        mapView.setOnZoomListener(new OnZoomListener() {
            @Override
            public void preAction(float v, float v1, double v2) {
                if(mapView.isLoaded()){
                    if(mapView.getSpatialReference()!=null){

                        Point prePoint = mapView.getCenter();
                        Point point = (Point) GeometryEngine.project(prePoint,mapView.getSpatialReference(),SpatialReference.create(SpatialReference.WKID_WGS84));
                    }
                }
            }

            @Override
            public void postAction(float v, float v1, double v2) {
                if(mapView.isLoaded()){
                    if(mapView.getSpatialReference()!=null){

                        Point prePoint = mapView.getCenter();
                        Point point = (Point) GeometryEngine.project(prePoint,mapView.getSpatialReference(),SpatialReference.create(SpatialReference.WKID_WGS84));
                        //ll_txt_latlon.setText(String.format("Lat\t: %.5f\t Lon\t: %.5f",point.getY(),point.getX()));
                    }

                }
            }
        });
        graphicsLayer.setMinScale(150000d);
        graphicsLayer.setMaxScale(1000d);
        graphicsLayerLangsiran.setMinScale(150000d);
        graphicsLayerLangsiran.setMaxScale(1000d);
        graphicsLayerTPHResurvey.setMinScale(150000d);
        graphicsLayerTPHResurvey.setMaxScale(1000d);
        graphicsLayerTPH3Bulan.setMinScale(150000d);
        graphicsLayerTPH3Bulan.setMaxScale(1000d);
        graphicsLayerTPH.setMinScale(150000d);
        graphicsLayerTPH.setMaxScale(1000d);
        graphicsLayerText.setMinScale(15000d);
        graphicsLayerText.setMaxScale(1000d);
        graphicsLayerToday.setMaxScale(1000d);
        graphicsLayerToday.setMaxScale(1000d);
        mapView.addLayer(graphicsLayer);
        mapView.addLayer(graphicsLayerTPHResurvey);
        mapView.addLayer(graphicsLayerTPH3Bulan);
        mapView.addLayer(graphicsLayerTPH);
        mapView.addLayer(graphicsLayerLangsiran);
        mapView.addLayer(graphicsLayerToday);
        mapView.addLayer(graphicsLayerText);
        mapView.invalidate();
        fabEstateLocation.callOnClick();
    }

    public void removeAllGraphic() {
        if(mapView!=null && mapView.isLoaded()){
            graphicsLayerLangsiran.removeAll();
            graphicsLayerTPHResurvey.removeAll();
            graphicsLayerTPH3Bulan.removeAll();
            graphicsLayerTPH.removeAll();
            graphicsLayerText.removeAll();
            graphicsLayerToday.removeAll();
            hashpoint.clear();
            hashText.clear();
            hashpointToday.clear();
        }
    }

    public void locationListenerSetup(){

        if(mapView!=null && mapView.isLoaded()){

            sbGPS = Snackbar.make(coor,"Searching GPS",Snackbar.LENGTH_INDEFINITE);

            sbGPS.show();
            locationDisplayManager = mapView.getLocationDisplayManager();
            locationDisplayManager.setAllowNetworkLocation(false);
            locationDisplayManager.setAccuracyCircleOn(true);
            locationDisplayManager.setLocationListener(new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {

                    ((BaseActivity) MapActivity.this).currentlocation = location;
                    if(sbGPS!=null){
                        if(sbGPS.isShown())sbGPS.dismiss();
                        if(location.getAccuracy()>GlobalHelper.MAX_ACCURACY_MAPMENU){
                            sbGPS = Snackbar.make(coor,"GPS Tidak akurat",Snackbar.LENGTH_INDEFINITE).setActionTextColor(Color.YELLOW);
                            //Log.e("curr acc",locationStation.getAccuracy()+" m");
                            sbGPS.show();
                        }else{
                            if(sbGPS!=null) {
                                if (sbGPS.isShown()) sbGPS.dismiss();
                            }
                        }
                    }
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {
                    if(sbGPS!=null)if(sbGPS.isShown())sbGPS.dismiss();
                }

                @Override
                public void onProviderDisabled(String s) {
                    sbGPS = Snackbar.make(coor,"No GPS Found",Snackbar.LENGTH_INDEFINITE)
                            .setAction("Turn On", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            });
                    sbGPS.show();
                    final AlertDialog.Builder builder = new AlertDialog.Builder(MapActivity.this);
                    builder.setMessage("GPS anda tidak hidup! Apakah anda ingin menghidupkan GPS?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();

                                }
                            });
                    final AlertDialog alert = builder.create();
                    alert.show();

                }


            });
            locationDisplayManager.start();
        }
    }

    public void zoomToLocation(double lat, double lon, SpatialReference spatialReference) {
        Point mapPoint = GeometryEngine.project(lon,lat,spatialReference);
        try {
            mapView.centerAt(mapPoint,true);
            mapView.zoomTo(mapPoint,15000f);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void defineTapSetup(float x,float y){
        List<TPHnLangsiran> tphList = new ArrayList<>();
        List<Integer> grapIdSelected = new ArrayList<>();
        int[] graphicIDs;
        if(graphicsLayerTPH.isVisible()) {
            graphicIDs = graphicsLayerTPH.getGraphicIDs(x, y, Utils.convertDpToPx(this, GlobalHelper.MARKER_TOUCH_SIZE));
            if (graphicIDs.length > 0) {
                for (int gid : graphicIDs) {
                    tphList.add(hashpoint.get(gid));
                    grapIdSelected.add(gid);
//                for (HashMap.Entry<Integer, TPHnLangsiran> entry : hashpoint.entrySet()) {
//                    if(gid == entry.getKey()){
//                        tphList.add(entry.getValue());
//                    }
//                }
                }
            }
        }

        if(graphicsLayerTPHResurvey.isVisible()) {
            graphicIDs = graphicsLayerTPHResurvey.getGraphicIDs(x, y, Utils.convertDpToPx(this, GlobalHelper.MARKER_TOUCH_SIZE));
            if (graphicIDs.length > 0) {
                for (int gid : graphicIDs) {
                    tphList.add(hashpoint.get(gid));
                    grapIdSelected.add(gid);
//                for (HashMap.Entry<Integer, TPHnLangsiran> entry : hashpoint.entrySet()) {
//                    if(gid == entry.getKey()){
//                        tphList.add(entry.getValue());
//                    }
//                }
                }
            }
        }


        if(graphicsLayerTPH3Bulan.isVisible()) {
            graphicIDs = graphicsLayerTPH3Bulan.getGraphicIDs(x, y, Utils.convertDpToPx(this, GlobalHelper.MARKER_TOUCH_SIZE));
            if (graphicIDs.length > 0) {
                for (int gid : graphicIDs) {
                    tphList.add(hashpoint.get(gid));
                    grapIdSelected.add(gid);
//                for (HashMap.Entry<Integer, TPHnLangsiran> entry : hashpoint.entrySet()) {
//                    if(gid == entry.getKey()){
//                        tphList.add(entry.getValue());
//                    }
//                }
                }
            }
        }

        if(graphicsLayerLangsiran.isVisible()) {
            graphicIDs = graphicsLayerLangsiran.getGraphicIDs(x, y, Utils.convertDpToPx(this, GlobalHelper.MARKER_TOUCH_SIZE));
            if (graphicIDs.length > 0) {
                for (int gid : graphicIDs) {
                    tphList.add(hashpoint.get(gid));
                    grapIdSelected.add(gid);
//                for (HashMap.Entry<Integer, TPHnLangsiran> entry : hashpoint.entrySet()) {
//                    if(gid == entry.getKey()){
//                        tphList.add(entry.getValue());
//                    }
//                }
                }
            }
        }

        if(graphicsLayerToday.isVisible()){
            graphicIDs = graphicsLayerToday.getGraphicIDs(x, y, Utils.convertDpToPx(this, GlobalHelper.MARKER_TOUCH_SIZE));
            if (graphicIDs.length > 0) {
                for (int gid : graphicIDs) {
                    for (Map.Entry<Integer, Integer> entry : hashpointToday.entrySet()) {
                        if (gid == entry.getValue()) {
                            tphList.add(hashpoint.get(entry.getKey()));
                            break;
                        }
                    }
                    grapIdSelected.add(gid);
                }
            }
        }

        calloutSetup();
        if (callout.isShowing()) {
            callout.hide();
        } else {
            if (grapIdSelected.size() == 1) {
                //jika dalam satu tap hanya ada satu titik maka kesini
                int idx = -1;
                String graptype = "";
                Graphic graphic = graphicsLayerTPH.getGraphic(grapIdSelected.get(0));
                if(graphic == null){
                    graphic = graphicsLayerTPHResurvey.getGraphic(grapIdSelected.get(0));
                }
                if(graphic == null){
                    graphic = graphicsLayerTPH3Bulan.getGraphic(grapIdSelected.get(0));
                }
                if(graphic == null){
                    graphic = graphicsLayerLangsiran.getGraphic(grapIdSelected.get(0));
                }
                if(graphic == null){
                    graphic = graphicsLayerToday.getGraphic(grapIdSelected.get(0));
                }
                if (graphic.getGeometry() instanceof Point) {
                    idx = grapIdSelected.get(0);
                }
                showInfoWindow(idx,tphList.get(0));
            }else if (grapIdSelected.size() > 1){
                showChoseInfoWindow(grapIdSelected, tphList);
            }
        }
    }

    private void calloutSetup() {
        this.callout = mapView.getCallout();
        this.callout.setStyle(R.xml.statisticspop);
        if (this.callout.isShowing()) {
            this.callout.hide();
        }
    }

    private void showChoseInfoWindow(List<Integer> graphicIDs,List<TPHnLangsiran> tphList){
        ArrayList<TPHnLangsiran> windowChoseArrayList = new ArrayList<>();
        GraphicsLayer layer = new GraphicsLayer();
        for (int i = 0; i < graphicIDs.size(); i++) {
            Graphic graphic = graphicsLayerTPH.getGraphic(graphicIDs.get(i));
            layer = graphicsLayerTPH;
            int idx =0;
            if(graphic == null){
                graphic = graphicsLayerTPHResurvey.getGraphic(graphicIDs.get(i));
                layer = graphicsLayerTPHResurvey;
            }
            if(graphic == null){
                graphic = graphicsLayerTPH3Bulan.getGraphic(graphicIDs.get(i));
                layer = graphicsLayerTPH3Bulan;
            }
            if(graphic == null){
                graphic = graphicsLayerLangsiran.getGraphic(graphicIDs.get(i));
                layer = graphicsLayerLangsiran;
            }
            if(graphic == null){
                return;
            }
            if (graphic.getGeometry() instanceof Point) {
                idx = graphicIDs.get(i);
            }

            if(idx != 0){
//                for (HashMap.Entry<Integer, TPHnLangsiran> entry : hashpoint.entrySet()) {
//                    if(idx == entry.getKey()){
                        windowChoseArrayList.add(hashpoint.get(idx));
//                    }
//                }
            }
        }

        Collections.sort(windowChoseArrayList, new Comparator<TPHnLangsiran>() {
            @Override
            public int compare(TPHnLangsiran o1, TPHnLangsiran o2) {
                return o1.getIdTPHnLangsiran().compareTo(o2.getIdTPHnLangsiran());
            }
        });

        if(windowChoseArrayList.size()>1){
            try {
                ViewGroup content = InfoWindowChose.setupInfoWindowsChoseTphnLangsiran(this, windowChoseArrayList);
                callout.setContent(content);
                GraphicsLayer gl = graphicsLayerTPH;
                Graphic graphic = gl.getGraphic(graphicIDs.get(graphicIDs.size() -1));
                if(graphic == null){
                    gl = graphicsLayerTPHResurvey;
                    graphic = gl.getGraphic(graphicIDs.get(graphicIDs.size() -1));
                }
                if(graphic == null){
                    gl = graphicsLayerTPH3Bulan;
                    graphic = gl.getGraphic(graphicIDs.get(graphicIDs.size() -1));
                }
                if(graphic == null){
                    gl = graphicsLayerLangsiran;
                    graphic = gl.getGraphic(graphicIDs.get(graphicIDs.size() -1));
                }
                if (graphic == null){
                    gl = graphicsLayerToday;
                    graphic = gl.getGraphic(graphicIDs.get(graphicIDs.size() -1));
                }


                int idx =0;
                if (graphic.getGeometry() instanceof Point) {
                    idx = graphicIDs.get(graphicIDs.size() -1);
                }

                Point realpoin = (Point) gl.getGraphic(idx).getGeometry();

                Point wgs84poin = (Point) GeometryEngine.project(realpoin,
                        gl.getSpatialReference(),
                        SpatialReference.create(SpatialReference.WKID_WGS84));
                Point mapPoint = (Point) GeometryEngine.project(wgs84poin,
                        SpatialReference.create(SpatialReference.WKID_WGS84),
                        mapView.getSpatialReference());
                callout.setCoordinates(mapPoint);
                mapView.centerAt(mapPoint,true);
                callout.animatedShow(mapPoint,content);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else if (windowChoseArrayList.size() == 1){
            showInfoWindow(graphicIDs.get(0),windowChoseArrayList.get(0));
        }
    }

    public void showInfoWindow(int idx,TPHnLangsiran tph){

        ViewGroup content = null;
        Point realpoin = null;
        Point mapPoint = null;
        if(tph.getTph() != null){
            if(tph.getTph().getResurvey() == 1 ){
                content = InfoWindow.setupInfoWindows(this, tph.getTph());
                realpoin = (Point) graphicsLayerTPHResurvey.getGraphic(idx).getGeometry();
                Point wgs84poin = (Point) GeometryEngine.project(realpoin,
                        graphicsLayerTPHResurvey.getSpatialReference(),
                        SpatialReference.create(SpatialReference.WKID_WGS84));
                mapPoint = (Point) GeometryEngine.project(wgs84poin,
                        SpatialReference.create(SpatialReference.WKID_WGS84),
                        mapView.getSpatialReference());
            } else if (tph.getTph().getPanen3Bulan() == 0){
                content = InfoWindow.setupInfoWindows(this, tph.getTph());
                realpoin = (Point) graphicsLayerTPH3Bulan.getGraphic(idx).getGeometry();
                Point wgs84poin = (Point) GeometryEngine.project(realpoin,
                        graphicsLayerTPH3Bulan.getSpatialReference(),
                        SpatialReference.create(SpatialReference.WKID_WGS84));
                mapPoint = (Point) GeometryEngine.project(wgs84poin,
                        SpatialReference.create(SpatialReference.WKID_WGS84),
                        mapView.getSpatialReference());
            } else {
                GraphicsLayer layer;
                if(graphicsLayerTPH.isVisible()){
                    layer = graphicsLayerTPH;
                }else{
                    layer = graphicsLayerToday;
                }
                content = InfoWindow.setupInfoWindows(this, tph.getTph());
                realpoin = (Point) layer.getGraphic(idx).getGeometry();
                Point wgs84poin = (Point) GeometryEngine.project(realpoin,
                        layer.getSpatialReference(),
                        SpatialReference.create(SpatialReference.WKID_WGS84));
                mapPoint = (Point) GeometryEngine.project(wgs84poin,
                        SpatialReference.create(SpatialReference.WKID_WGS84),
                        mapView.getSpatialReference());
            }
        }else if (tph.getLangsiran() != null){
            GraphicsLayer layer;
            if(graphicsLayerLangsiran.isVisible()){
                layer = graphicsLayerLangsiran;
            }else{
                layer = graphicsLayerToday;
            }
            content = InfoWindow.setupInfoWindowsLangsiran(this, tph.getLangsiran());
            realpoin = (Point) layer.getGraphic(idx).getGeometry();
            Point wgs84poin = (Point) GeometryEngine.project(realpoin,
                    layer.getSpatialReference(),
                    SpatialReference.create(SpatialReference.WKID_WGS84));
            mapPoint = (Point) GeometryEngine.project(wgs84poin,
                    SpatialReference.create(SpatialReference.WKID_WGS84),
                    mapView.getSpatialReference());
        }

        if(mapPoint!= null) {
            callout.setContent(content);
            callout.setCoordinates(mapPoint);
            mapView.centerAt(mapPoint,true);
            mapView.zoomTo(mapPoint,15000f);
            callout.animatedShow(mapPoint,content);
        }
    }

    private void menuSetup(){
        JSONObject jModule = GlobalHelper.getModule();
        try {
            if ((jModule.getString("mdlAccCode").equals("HMS3")) && (jModule.getString("subMdlAccCode").equals("P5"))) {
                bmb.setButtonEnum(ButtonEnum.TextOutsideCircle);
                bmb.setPiecePlaceEnum(PiecePlaceEnum.DOT_7_3);
                bmb.setButtonPlaceEnum(ButtonPlaceEnum.SC_7_3);
                bmb.setBackgroundEffect(true);
                bmb.addBuilder(newTPHCircle());
                bmb.addBuilder(newLangsiranCircle());
                bmb.addBuilder(sync());
                bmb.addBuilder(restore());
                bmb.addBuilder(syncHistory());
                bmb.addBuilder(selectedNetwork());
                bmb.addBuilder(menuAboutApplication());
            }else{
                bmb.setButtonEnum(ButtonEnum.Ham);
                bmb.setPiecePlaceEnum(PiecePlaceEnum.HAM_2);
                bmb.setButtonPlaceEnum(ButtonPlaceEnum.HAM_2);
                bmb.setBackgroundEffect(true);
                bmb.addBuilder(newTPHHam());
                bmb.addBuilder(newLangsiranHam());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupDataAfdelingBlock(){
        afdelingBlocks = GlobalHelper.getAllAfdelingBlock(new File(locKmlMap));
        if(locationDisplayManager != null && locationDisplayManager.getLocation() != null) {
            String[] blockAfdeling = GlobalHelper.getAndAssignBlock(new File(locKmlMap),
                    locationDisplayManager.getLocation().getLatitude(),
                    locationDisplayManager.getLocation().getLongitude()).split(";");
            for (TreeMap.Entry<String,ArrayList<Block>> entry : afdelingBlocks.entrySet()) {
                String key = entry.getKey();
                ArrayList<Block> blocks = afdelingBlocks.get(key);
                for (int i = 0; i < blocks.size(); i++) {
                    try {
                        if ((key.equals(blockAfdeling[1])) && (blocks.get(i).getBlock().equals(blockAfdeling[2]))) {

                            Block block = new Block(blockAfdeling[2], true,blocks.get(i).getPolygons());
                            blocks.remove(i);
                            blocks.add(block);
                            break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                Collections.sort(blocks, new Comparator<Block>() {
                    @Override
                    public int compare(Block o1, Block o2) {
                        return o1.getBlock().compareToIgnoreCase(o2.getBlock());
                    }
                });

                afdelingBlocks.put(key, blocks);
            }
        }
    }

    private void setDefaultManual(){
        double lon = 0;
        double lat = 0;
        if (isTph) {
            lat = tphHelper.latitude;
            lon = tphHelper.longitude;
        } else {
            lat = tujuanHelper.latitude;
            lon = tujuanHelper.longitude;
        }

        tv_lat_manual.setText(String.format("%.5f",lat));
        tv_lon_manual.setText(String.format("%.5f",lon));
        tv_deg_manual.setText("0");
        tv_dis_manual.setText("0 m");
        ll_latlon.setBackgroundColor(Color.WHITE);
        zoomToLocation(lat,lon,spatialReference);
        GlobalHelper.hideKeyboard(this);

        switchCrossHairUI(true);
//        btnSave.setVisibility(View.VISIBLE);
    }

    private void switchCrossHairUI(boolean visible){
        if(visible){
            ivCrosshair.setVisibility(View.VISIBLE);
            ll_latlon.setVisibility(View.VISIBLE);

            RelativeLayout.LayoutParams params= (RelativeLayout.LayoutParams) ll_latlon.getLayoutParams();
            params.addRule(RelativeLayout.BELOW, R.id.iv_crosshair);
            //ll_latlon.setLayoutParams(params);
        }else{
            ivCrosshair.setVisibility(View.GONE);
            ll_latlon.setVisibility(View.GONE);
        }
    }

    public void updateCounterKet(){
        checkBoxTph.setText(String.format("Total Tph : %,d",countTph));
        checkBoxTphResurvey.setText(String.format("Total Tph Resurvey : %,d",countTphResurvey));
        checkBoxTph3Bulan.setText(String.format("Tph Tidak Panen3 Bulan : %,d",countTph3Bulan));
        checkBoxLangsir.setText(String.format("Total Langsiran : %,d",countLangsiran));
        checkBoxInputHariIni.setText(String.format("Blm Upload : %,d",countHariIni));

        checkBoxInputHariIni.setChecked(false);

        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    private HamButton.Builder newTPHHam(){
        return new HamButton.Builder()
                .normalImageRes(R.drawable.ic_tph)
                .normalText(getResources().getString(R.string.tph))
                .subNormalText("Mendaftarkan Tempat Pengumpulan Hasil")
                .pieceColorRes(R.color.OrangeRed)
                .normalColorRes(R.color.OrangeRed)
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        l1 = System.currentTimeMillis();
                        new LongOperation().execute(String.valueOf(LongOperation_Setup_FormTPH));
                    }
                });
    }

    private HamButton.Builder newLangsiranHam(){
        return new HamButton.Builder()
                .normalImageRes(R.drawable.ic_langsiran)
                .normalText(getResources().getString(R.string.langsiran))
                .subNormalText("Mendaftarkan Tempat Langsiran")
                .pieceColorRes(R.color.Brown)
                .normalColorRes(R.color.Brown)
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        new LongOperation().execute(String.valueOf(LongOperation_Setup_FormLangsiran));
                    }
                });
    }

    private TextOutsideCircleButton.Builder newTPHCircle(){
        return new TextOutsideCircleButton.Builder()
                .normalImageRes(R.drawable.ic_tph)
                .normalText(getResources().getString(R.string.tph))
//                .subNormalText("Mendaftarkan Tempat Pengumpulan Hasil")
                .pieceColorRes(R.color.OrangeRed)
                .normalColorRes(R.color.OrangeRed)
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        l1 = System.currentTimeMillis();
                        new LongOperation().execute(String.valueOf(LongOperation_Setup_FormTPH));
                    }
                });
    }

    private TextOutsideCircleButton.Builder newLangsiranCircle(){
        return new TextOutsideCircleButton.Builder()
                .normalImageRes(R.drawable.ic_langsiran)
                .normalText(getResources().getString(R.string.langsiran))
//                .subNormalText("Mendaftarkan Tempat Langsiran")
                .pieceColorRes(R.color.Brown)
                .normalColorRes(R.color.Brown)
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        new LongOperation().execute(String.valueOf(LongOperation_Setup_FormLangsiran));
                    }
                });
    }

    private TextOutsideCircleButton.Builder sync(){
        return new TextOutsideCircleButton.Builder()
                .normalImageRes(R.drawable.ic_sync)
                .normalText(getResources().getString(R.string.sync))
//                .subNormalText("Sync Data Tph Dan Langsiran")
                .pieceColorRes(R.color.SkyBlue)
                .normalColorRes(R.color.SkyBlue)
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        uploadHelper.SelectUploadSync();
//                        new LongOperation(LongOperation_Upload_TPH).execute(String.valueOf(LongOperation_Upload_TPH));
                    }
                });
    }

    private TextOutsideCircleButton.Builder restore(){
        return new TextOutsideCircleButton.Builder()
                .normalImageRes(R.drawable.ic_backup)
                .normalText(getResources().getString(R.string.restore))
//                .subNormalText("Mengambil Data Tph Dan Langsiran Yang Terhapus Saat Sync Tph Atau Langsiran")
                .pieceColorRes(R.color.DarkSeaGreen)
                .normalColorRes(R.color.DarkSeaGreen)
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        restoreHelper.showAllRestore();
                    }
                });
    }

    private TextOutsideCircleButton.Builder syncHistory(){
        return new TextOutsideCircleButton.Builder()
                .normalImageRes(R.drawable.ic_history)
                .normalText(getResources().getString(R.string.sync_history))
//                .subNormalText(getResources().getString(R.string.sync_history_info))
                .pieceColorRes(R.color.Magenta)
                .normalColorRes(R.color.Magenta)
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        syncHistoryHelper.showAllSyncHistroy();
                    }
                });
    }

    private TextOutsideCircleButton.Builder selectedNetwork(){
        return new TextOutsideCircleButton.Builder()
                .normalImageRes(R.drawable.ic_server)
                .normalText(getResources().getString(R.string.network_use))
//                .subNormalText(getResources().getString(R.string.network_use) + " Untuk Melihat Foto")
                .pieceColorRes(R.color.Blue)
                .normalColorRes(R.color.Blue)
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        viewSelectedNetwork();
                    }
                });
    }

    private TextOutsideCircleButton.Builder menuAboutApplication(){
        return new TextOutsideCircleButton.Builder()
                .normalImageRes(R.drawable.ic_about_application)
                .normalText(getResources().getString(R.string.about_application))
//                .subNormalText(getResources().getString(R.string.network_use) + " Untuk Melihat Foto")
                .pieceColorRes(R.color.Green)
                .normalColorRes(R.color.Green)
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        GlobalHelper.menuAboutApplication(MapActivity.this);
                    }
                });
    }

    public void viewSelectedNetwork(){
        View dialogView = LayoutInflater.from(this).inflate(R.layout.selected_network, null);
        SegmentedButtonGroup sbgNetwork = dialogView.findViewById(R.id.segmentedButtonGroup);
        Button btnClose = dialogView.findViewById(R.id.btnClose);

        final AlertDialog dialog = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                .setView(dialogView)
                .setCancelable(false)
                .create();

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
        if(preferences.getBoolean(HarvestApp.CONNECTION_PREF,false)){
            sbgNetwork.setPosition(SettingFragment.ButtonGroup_Public);
        }else{
            sbgNetwork.setPosition(SettingFragment.ButtonGroup_Lokal);
        }

        sbgNetwork.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(int position) {
                switch (position){
                    case 0: {
                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF, false);
                        editor.apply();
                        break;
                    }
                    case 1: {
                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF, true);
                        editor.apply();
                        break;
                    }
                }
                dialog.dismiss();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void backProses() {
        setResult(GlobalHelper.RESULT_MAPACTIVITY);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        AlertDialog alertDialog = new AlertDialog.Builder(this,R.style.MyAlertDialogStyle)
                .setTitle("Perhatian")
                .setMessage("Yakin keluar Manu Map?")
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        backProses();
                    }
                })
                .create();
        alertDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTimeScreen = new Date();
    }

    @Override
    public void onPause(){
        super.onPause();
        endTimeScreen = new Date();
        if(GlobalHelper.getUser()!=null){
            ActivityLoggerHelper.recordScreenTime(MapActivity.class.getSimpleName(),startTimeScreen, endTimeScreen);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void showLongOperation(TPHnLangsiran tpHnLangsiran,String param){
        new LongOperation(tpHnLangsiran).execute(param);
    }

    @Override
    public void successResponse(ServiceResponse serviceResponse) {
        try {
            switch (serviceResponse.getTag()) {
                // TPH
                case PostTphImage:
                    uploadHelper.uploadData(this,Tag.PostTph,objSetUp.getJSONArray("data"), idxUpload);
                    break;
                case PostTph:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != idxUpload) {
                        idxUpload ++;
                        uploadHelper.uploadImages(this,Tag.PostTphImage,objSetUp.getJSONArray("data"), idxUpload);
                    }else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(HarvestApp.getContext(), "Upload Selesai TPH", Toast.LENGTH_SHORT).show();
                            }
                        });
                        alertDialog.dismiss();
                        new LongOperation(LongOperation_Upload_Langsiran).execute(String.valueOf(LongOperation_Upload_Langsiran));
                    }
                    break;
                case GetTphListByEstate:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                          Toast.makeText(HarvestApp.getContext(), "Get Selesai TPH", Toast.LENGTH_SHORT).show();
                        }
                    });
                    alertDialog.dismiss();
                    responseApi = serviceResponse;
                    new LongOperation(LongOperation_Download_TPH).execute(String.valueOf(LongOperation_Download_TPH));
                    break;
                case GetTPHCounterByUserID:
                    if(setUpDataSyncHelper.UpdateTPHCounterByUserID(serviceResponse)){
                        connectionPresenter.GetLangsiranCounterByUserID();
                    }else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(HarvestApp.getContext(),"Gagal UpdateTPHCounterByUserID",Toast.LENGTH_SHORT).show();
                            }
                        });
                        alertDialog.dismiss();
                    }
                    break;

                // Langsiran
                case PostLangsiranImage:
                    uploadHelper.uploadData(this,Tag.PostLangsiran,objSetUp.getJSONArray("data"), idxUpload);
                    break;
                case PostLangsiran:
                    uploadHelper.listTransaksiDone(this,serviceResponse.getTag(),objSetUp.getJSONArray("data"),idxUpload);
                    if(objSetUp.getJSONArray("data").length() -1 != idxUpload) {
                        idxUpload ++;
                        uploadHelper.uploadImages(this,Tag.PostLangsiranImage,objSetUp.getJSONArray("data"), idxUpload);
                    }else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(HarvestApp.getContext(), "Upload Selesai Langsiran", Toast.LENGTH_SHORT).show();
                            }
                        });
                        alertDialog.dismiss();
                        responseApi = serviceResponse;
                        SyncTimeHelper.updateFinishSyncTime(SyncTime.UPLOAD_TRANSAKSI);
                        new LongOperation(LongOperation_BackUp_HMS).execute(String.valueOf(LongOperation_BackUp_HMS));
                    }
                    break;
                case GetLangsiranListByEstate:
                    runOnUiThread(new Runnable() {
                      @Override
                      public void run() {
                          Toast.makeText(HarvestApp.getContext(), "Get Selesai Langsiran", Toast.LENGTH_SHORT).show();
                      }
                    });
                    alertDialog.dismiss();
                    responseApi = serviceResponse;
                    new LongOperation(LongOperation_Download_Langsiran).execute(String.valueOf(LongOperation_Download_Langsiran));
                    break;
                case GetLangsiranCounterByUserID:
                    if(setUpDataSyncHelper.UpdateLangsiranCounterByUserID(serviceResponse)){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                SyncTimeHelper.updateFinishSyncTime(SyncTime.SYNC_DATA_MASTER);
                                Toast.makeText(HarvestApp.getContext(),"Sync Berhasil",Toast.LENGTH_SHORT).show();
                            }
                        });

                        SyncTimeHelper.updateFinishSyncTime(SyncTime.SYNC_DATA_TRANSAKSI);
                        //selesai sync kondisi ini harus dijalankan agar file penanda sudah sync
                        if(setUpDataSyncHelper.setUpLastSync(coor)) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("setAfdelingBlocksValue","finish GetLangsiranCounterByUserID");
                                    setAfdelingBlocksValue();
                                }
                            });
                        }else{
                            Toast.makeText(HarvestApp.getContext(),"Gagal Sync Harap Sync Ulang",Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                        }
                        alertDialog.dismiss();
                    }else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(HarvestApp.getContext(),"Gagal UpdateTPHCounterByUserID",Toast.LENGTH_SHORT).show();
                            }
                        });
                        alertDialog.dismiss();
                    }
                    break;

                // Master User
                case GetMasterUserByEstate:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(HarvestApp.getContext(), "Get Selesai Master User", Toast.LENGTH_SHORT).show();
                        }
                    });
                    alertDialog.dismiss();
                    responseApi = serviceResponse;
                    new LongOperation(LongOperation_Download_MasterUser).execute(String.valueOf(LongOperation_Download_MasterUser));
                    break;

                // Pemanen
                case GetPemanenListByEstate:
                    runOnUiThread(new Runnable() {
                      @Override
                      public void run() {
                          Toast.makeText(HarvestApp.getContext(), "Get Selesai Pemanen", Toast.LENGTH_SHORT).show();
                      }
                    });
                    alertDialog.dismiss();
                    responseApi = serviceResponse;
                    new LongOperation(LongOperation_Download_Pemanen).execute(String.valueOf(LongOperation_Download_Pemanen));
                    break;

                // Operator
                case GetOperatorListByEstate:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(HarvestApp.getContext(), "Get Selesai Operator", Toast.LENGTH_SHORT).show();
                        }
                    });
                    alertDialog.dismiss();
                    responseApi = serviceResponse;
                    new LongOperation(LongOperation_Download_Operator).execute(String.valueOf(LongOperation_Download_Operator));
                    break;

                // PKS
                case GetListPKSByEstate:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(HarvestApp.getContext(), "Get Selesai PKS", Toast.LENGTH_SHORT).show();
                        }
                    });
                    alertDialog.dismiss();
                    responseApi = serviceResponse;
                    new LongOperation(LongOperation_Download_PKS).execute(String.valueOf(LongOperation_Download_PKS));
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            objSetUp = new JSONObject();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(HarvestApp.getContext(), "Gagal Upload JSONException successResponse", Toast.LENGTH_LONG).show();
                }
            });
            alertDialog.dismiss();
        }
    }

    @Override
    public void badResponse(ServiceResponse serviceResponse) {
        switch (serviceResponse.getTag()) {
            //TPH
            case PostTphImage:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HarvestApp.getContext(), "Upload Gagal Images TPH", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.dismiss();
                uploadHelper.SelectUploadSync();
                break;
            case PostTph:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HarvestApp.getContext(), "Upload Gagal TPH", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.dismiss();
                uploadHelper.SelectUploadSync();
                break;
            case GetTphListByEstate:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HarvestApp.getContext(), "GET Gagal TPH", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.dismiss();
                uploadHelper.SelectUploadSync();
                break;
            case GetTPHCounterByUserID:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HarvestApp.getContext(), "Update Count Gagal TPH", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.dismiss();
                uploadHelper.SelectUploadSync();
                break;

            //Langsiran
            case PostLangsiranImage:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HarvestApp.getContext(), "Upload Gagal Images Langsiran", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.dismiss();
                uploadHelper.SelectUploadSync();
                break;
            case PostLangsiran:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HarvestApp.getContext(), "Upload Gagal Langsiran", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.dismiss();
                uploadHelper.SelectUploadSync();
                break;
            case GetLangsiranListByEstate:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HarvestApp.getContext(), "GET Gagal Langsiran", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.dismiss();
                uploadHelper.SelectUploadSync();
                break;
            case GetLangsiranCounterByUserID:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HarvestApp.getContext(), "Update Count Gagal Langsiran", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.dismiss();
                uploadHelper.SelectUploadSync();
                break;

            // Master User
            case GetMasterUserByEstate:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HarvestApp.getContext(), "GET Gagal Master User", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.dismiss();
                uploadHelper.SelectUploadSync();
                break;

            // Pemanen
            case GetPemanenListByEstate:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HarvestApp.getContext(), "GET Gagal Pemanen", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.dismiss();
                uploadHelper.SelectUploadSync();
                break;

            // Operator
            case GetOperatorListByEstate:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HarvestApp.getContext(), "GET Gagal Operator", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.dismiss();
                uploadHelper.SelectUploadSync();
                break;

            // PKS
            case GetListPKSByEstate:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HarvestApp.getContext(), "GET Gagal PKS", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.dismiss();
                uploadHelper.SelectUploadSync();
                break;
        }
    }

    public void startLongOperation(int statLongOperation,TPHnLangsiran tpHnLangsiranSelected){
        if(tpHnLangsiranSelected != null){
            new LongOperation(tpHnLangsiranSelected).execute(String.valueOf(statLongOperation));
        }else{
            new LongOperation(statLongOperation).execute(String.valueOf(statLongOperation));
        }
    }

    public static void LogIntervalStep(String step){
//        Long lm = System.currentTimeMillis() - l1;
//        l1 = System.currentTimeMillis();
//        Log.d("new tph "+step, String.valueOf((lm / 1000) % 60) + " Second");
    }

    public class LongOperation extends AsyncTask<String, String, String> {
        TPHnLangsiran tpHnLangsiran;
        int statLongOperation;
        boolean skip = false;
        Snackbar snackbar;
        boolean error = false;

        public LongOperation() {
        }

        public LongOperation(int statLongOperation) {
            this.statLongOperation = statLongOperation;
        }

        public LongOperation(TPHnLangsiran tpHnLangsiran) {
            this.tpHnLangsiran = tpHnLangsiran;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if(alertDialog != null){
                if(alertDialog.isShowing()){
                    alertDialog.dismiss();
                }
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    switch (statLongOperation){
                        case LongOperation_Upload_TPH:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.setup_data_tph));
                            break;
                        case LongOperation_Upload_Langsiran:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.setup_data_langsiran));
                            break;
                        case LongOperation_Download_TPH:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.update_data_tph));
                            break;
                        case LongOperation_Download_Langsiran:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.update_data_langsiran));
                            break;
                        case LongOperation_BackUp_HMS:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.backup_data_hms));
                            break;
                        case LongOperation_Get_MasterUser:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.get_master_user));
                            break;
                        case LongOperation_Download_MasterUser:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.download_master_user));
                            break;
                        case LongOperation_Get_Pemanen:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.get_master_pemanen));
                            break;
                        case LongOperation_Download_Pemanen:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.download_master_pemanen));
                            break;
                        case LongOperation_Get_Operator:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.get_master_operator));
                            break;
                        case LongOperation_Download_Operator:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.download_master_operator));
                            break;
                        case LongOperation_Get_PKS:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.get_data_pks));
                            break;
                        case LongOperation_Download_PKS:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.update_data_pks));
                            break;
                        case LongOperation_Counter_TPH:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.setup_data_Counter_tph));
                            break;
                        case LongOperation_Counter_Langsiran:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.setup_data_Counter_langsiran));
                            break;
                        default:
                            alertDialog = WidgetHelper.showWaitingDialog(MapActivity.this, getResources().getString(R.string.wait));
                    }
                }
            });

            objSetUp = new JSONObject();
            skip = false;
            error = false;

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                switch (Integer.parseInt(strings[0])) {
                    case LongOperation_Setup_Data_AfdelingBlock:
                        setupDataAfdelingBlock();
                        break;
                    case LongOperation_Setup_Poin:
                        Log.e("LongOperation","LongOperation_Setup_Poin dobackground");
                        setupPoint();
                        break;

                    //Tph
                    case LongOperation_Upload_TPH:
                        // periapan data untuk upload tph jika tidak ada data yang akan di upload maka skip
                        objSetUp = setUpDataSyncHelper.UploadTph(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadImages(this,Tag.PostTphImage,objSetUp.getJSONArray("data"),0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_TPH:
                        //akses api doang TPH
                        connectionPresenter.GetTphListByEstate();
                        break;
                    case LongOperation_Download_TPH:
                        //update db tph
                        skip = !setUpDataSyncHelper.downLoadTph(responseApi,this);
                        break;
                    case LongOperation_Counter_TPH:
                        //counter
                        connectionPresenter.GetTPHCounterByUserID();
                        break;

                    //Langsiran
                    case LongOperation_Upload_Langsiran:
                        // periapan data untuk upload langsiran jika tidak ada data yang akan di upload maka skip
                        objSetUp = setUpDataSyncHelper.UploadLangsiran(true, this);
                        if(objSetUp != null) {
                            if (objSetUp.getJSONArray("data").length() > 0) {
                                uploadHelper.uploadImages(this,Tag.PostLangsiranImage,objSetUp.getJSONArray("data"),0);
                            } else {
                                skip = true;
                            }
                        }else{
                            error = true;
                        }
                        break;
                    case LongOperation_Get_Langsiran:
                        //akses api doang Langsiran
                        connectionPresenter.GetLangsiranListByEstate();
                        break;
                    case LongOperation_Download_Langsiran:
                        //update db Langsiran
                        skip = !setUpDataSyncHelper.downLoadLangsiran(responseApi,this);
                        break;
                    case LongOperation_Counter_Langsiran:
                        //counter
                        connectionPresenter.GetLangsiranCounterByUserID();
                        break;

                    //Backup
                    case LongOperation_BackUp_HMS:
                        setUpDataSyncHelper.backupDataHMS(this);
                        break;
                    //Restore
                    case LongOperation_RestoreHMS:
                        skip =!restoreHelper.choseRestore(this);
                        break;

                    //Master User
                    case LongOperation_Get_MasterUser:
                        //akses api doang Langsiran
                        connectionPresenter.GetMasterUserByEstate();
                        break;
                    case LongOperation_Download_MasterUser:
                        //update db Langsiran
                        skip = !setUpDataSyncHelper.SetMasterUserByEstate(responseApi,this);
                        break;

                    //Master Pemanen
                    case LongOperation_Get_Pemanen:
                        //akses api doang Langsiran
                        connectionPresenter.GetPemanenListByEstate();
                        break;
                    case LongOperation_Download_Pemanen:
                        //update db Langsiran
                        skip = !setUpDataSyncHelper.SetPemanenListByEstate(responseApi,this);
                        break;

                    //Master Operator
                    case LongOperation_Get_Operator:
                        //akses api doang Langsiran
                        connectionPresenter.GetOperatorListByEstate();
                        break;
                    case LongOperation_Download_Operator:
                        //update db
                        skip = !setUpDataSyncHelper.SetOperatorListByEstate(responseApi,this);
                        break;

                    //Master PKS
                    case LongOperation_Get_PKS:
                        //akses api doang Langsiran
                        connectionPresenter.GetPksListByEstate();
                        break;
                    case LongOperation_Download_PKS:
                        //update db Langsiran
                        skip = !setUpDataSyncHelper.SetPksListByEstate(responseApi,this);
                        break;
                }
                return String.valueOf(Integer.parseInt(strings[0]));
            } catch (JSONException e) {
                e.printStackTrace();
                return "JSONException";
            }
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject objProgres = new JSONObject(String.valueOf(values[0]));

                        if(snackbar== null){
                            snackbar = Snackbar.make(coor,objProgres.getString("ket"),Snackbar.LENGTH_INDEFINITE);
                        }
                        snackbar.setText(String.format(objProgres.getString("ket") + " %d %s [%d/%d] ",
                                objProgres.getInt("persen"), "%", objProgres.getInt("count"), objProgres.getInt("total")));
                        snackbar.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(snackbar != null){
                snackbar.dismiss();
            }
            if(result.equals("JSONException")){
                Toast.makeText(HarvestApp.getContext(),"Gagal Prepare Data Long Operation",Toast.LENGTH_LONG).show();
                alertDialog.dismiss();
            }else {
                switch (Integer.parseInt(result)) {
                    case LongOperation_Setup_Data_AfdelingBlock:
                        setAfdelingBlocksValue();
                        if (alertDialog != null) {
                            alertDialog.dismiss();
                        }
                        break;
                    case LongOperation_Setup_Poin:
                        Log.e("LongOperation","LongOperation_Setup_Poin finish");
                        updateCounterKet();
                        if (alertDialog != null) {
                            if(alertDialog.isShowing()) {
                                alertDialog.dismiss();
                            }
                        }

                        Log.e("LongOperation","LongOperation_Setup_Poin done");
                        break;
                    case LongOperation_Setup_FormTPH:
                        LogIntervalStep("lo");
                        setUpFormTph(tpHnLangsiran != null ? tpHnLangsiran.getTph() : null);
                        if (alertDialog != null) {
                            alertDialog.dismiss();
                        }
                        break;
                    case LongOperation_Setup_FormLangsiran:
                        setUpFormLangsiran(tpHnLangsiran != null ? tpHnLangsiran.getLangsiran() : null);
                        if (alertDialog != null) {
                            alertDialog.dismiss();
                        }
                        break;

                    //TPH
                    case LongOperation_Upload_TPH:
                        if (skip) {
                            alertDialog.dismiss();
                            new LongOperation(LongOperation_Upload_Langsiran).execute(String.valueOf(LongOperation_Upload_Langsiran));
                        } else if(error){
                            alertDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_TPH:
                        if (skip) {
                            alertDialog.dismiss();
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update TPH", Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                            new LongOperation(LongOperation_Get_Langsiran).execute(String.valueOf(LongOperation_Get_Langsiran));
                        }
                        break;

                    //Langsiran
                    case LongOperation_Upload_Langsiran:
                        if (skip) {
                            alertDialog.dismiss();
                            new LongOperation(LongOperation_BackUp_HMS).execute(String.valueOf(LongOperation_BackUp_HMS));
                        } else if(error){
                            alertDialog.dismiss();
                            Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Di Upload", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case LongOperation_Download_Langsiran:
                        if (skip) {
                            alertDialog.dismiss();
                        } else {
                            Toast.makeText(HarvestApp.getContext(), "Berhasil Update Langsiran", Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                            new LongOperation(LongOperation_Counter_TPH).execute(String.valueOf(LongOperation_Counter_TPH));
                        }
                        break;

                     //Backup
                    case LongOperation_BackUp_HMS:
                        alertDialog.dismiss();
                        new LongOperation(LongOperation_Get_MasterUser).execute(String.valueOf(LongOperation_Get_MasterUser));
                        break;
                    //restore
                    case LongOperation_RestoreHMS:
                        restoreHelper.closeRestore();
                        break;

                    //Master User
                    case LongOperation_Download_MasterUser:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetMasterUserByEstate",Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                        }else {
                            alertDialog.dismiss();
                            new LongOperation(LongOperation_Get_Pemanen).execute(String.valueOf(LongOperation_Get_Pemanen));
                        }
                        break;

                    //Pemanen
                    case LongOperation_Download_Pemanen:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetPemanenByEstate",Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                        }else {
                            alertDialog.dismiss();
                            new LongOperation(LongOperation_Get_Operator).execute(String.valueOf(LongOperation_Get_Operator));
                        }
                        break;

                    //Operator
                    case LongOperation_Download_Operator:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetOperatorByEstate",Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                        }else {
                            alertDialog.dismiss();
                            new LongOperation(LongOperation_Get_PKS).execute(String.valueOf(LongOperation_Get_PKS));
                        }
                        break;

                    //PKS
                    case LongOperation_Download_PKS:
                        if(skip){
                            Toast.makeText(HarvestApp.getContext(),"Gagal Insert GetPemanenByEstate",Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                        }else {
                            alertDialog.dismiss();
                            new LongOperation(LongOperation_Get_TPH).execute(String.valueOf(LongOperation_Get_TPH));
                        }
                        break;
                }

            }
        }

        public void publishProgress(JSONObject objProgres){
            onProgressUpdate(String.valueOf(objProgres));
        }
    }
}
