package id.co.ams_plantation.harvestsap.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.CekSPKAdapter;
import id.co.ams_plantation.harvestsap.adapter.CekSPKBlockAdapter;
import id.co.ams_plantation.harvestsap.adapter.RefreshHeaderRecycleView;
import id.co.ams_plantation.harvestsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestsap.model.SPK;
import id.co.ams_plantation.harvestsap.presenter.ConnectionPresenter;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.MekanisasiHelper;
import id.co.ams_plantation.harvestsap.util.SetUpDataSyncHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import id.co.ams_plantation.harvestsap.view.ApiView;

import static id.co.ams_plantation.harvestsap.Fragment.SettingFragment.ButtonGroup_Lokal;
import static id.co.ams_plantation.harvestsap.Fragment.SettingFragment.ButtonGroup_Public;

public class MekanisasiBlockActivity extends BaseActivity implements ApiView {

    CoordinatorLayout myCoordinatorLayout;
    SegmentedButtonGroup segmentedButtonGroup;
    LinearLayout lnoData;
    RecyclerView rv;
    RefreshLayout refreshRv;

    Set<String> blockHashMap = new android.support.v4.util.ArraySet<>();
    CekSPKBlockAdapter adapter;

    final int LongOperation_SetUpData= 0;
    final int LongOperation_SetUpDataDownload = 1;
    final int LongOperation_UpdateData = 2;

    ConnectionPresenter connectionPresenter;
    SetUpDataSyncHelper setUpDataSyncHelper;
    MekanisasiHelper mekanisasiHelper;
    boolean awal;
    ServiceResponse responseApi;

    @Override
    protected void initPresenter() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cek_block_mekanisasi_list_layout);
        getSupportActionBar().hide();
        toolBarSetup();

        myCoordinatorLayout = findViewById(R.id.myCoordinatorLayout);
        segmentedButtonGroup = findViewById(R.id.segmentedButtonGroup);
        lnoData = findViewById(R.id.lnoData);
        rv = findViewById(R.id.rv);
        refreshRv = findViewById(R.id.refreshRv);

        setUpDataSyncHelper = new SetUpDataSyncHelper(this);
        mekanisasiHelper = new MekanisasiHelper(this);
        connectionPresenter = new ConnectionPresenter(this);
        awal = true;

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
        if(preferences.getBoolean(HarvestApp.CONNECTION_PREF,false)){
            segmentedButtonGroup.setPosition(ButtonGroup_Public);
        }else{
            segmentedButtonGroup.setPosition(ButtonGroup_Lokal);
        }

        rv.setLayoutManager(new LinearLayoutManager(this));

        refreshRv.setEnableAutoLoadMore(true);
        refreshRv.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                refresh();
            }
        });
        refreshRv.setRefreshHeader(new RefreshHeaderRecycleView(this));
        refreshRv.setHeaderHeight(60);
        refreshRv.autoRefresh();

        segmentedButtonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(int position) {
                switch (position){
                    case 0: {
                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF, false);
                        editor.apply();
                        break;
                    }
                    case 1: {
                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF, true);
                        editor.apply();
                        break;
                    }
                }
            }
        });
    }

    private void refresh() {
        lnoData.setVisibility(View.GONE);
        refreshRv.getLayout().postDelayed(() -> {
            if(awal) {
                new LongOperation().execute(String.valueOf(LongOperation_SetUpData));
            }else{
                new LongOperation().execute(String.valueOf(LongOperation_SetUpDataDownload));
            }
        }, 2000);
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        TextView textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backProses();
            }
        });
        textToolbar.setText(getResources().getString(R.string.cek_block_mekanisasi));
    }

    private void setupAdapter(){

        rv.setVisibility(View.VISIBLE);
        if(blockHashMap.size() > 0) {
            adapter = new CekSPKBlockAdapter(new ArrayList<>(blockHashMap));
            rv.setLayoutManager(new GridLayoutManager(this, 4));
            rv.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }else{
            rv.setVisibility(View.GONE);
        }

        awal = false;

        if(blockHashMap.size() > 0){
            lnoData.setVisibility(View.GONE);
        }else{
            lnoData.setVisibility(View.VISIBLE);
        }
    }

    public void backProses() {
        setResult(GlobalHelper.RESULT_MEKANISASIBLOCKACTIVITY);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        backProses();
    }

    @Override
    public void successResponse(ServiceResponse serviceResponse) {
        try {
            switch (serviceResponse.getTag()) {
                case GetBlockMekanisasi:
                    //WidgetHelper.showSnackBar(myCoordinatorLayout, "Get Selesai BJRInformation");
                    responseApi = serviceResponse;
                    new LongOperation().execute(String.valueOf(LongOperation_UpdateData));
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void badResponse(ServiceResponse serviceResponse) {
        switch (serviceResponse.getTag()) {
            case GetBlockMekanisasi:
                WidgetHelper.showSnackBar(myCoordinatorLayout, "GET Gagal GetMasterSPKByEstate ");
                new LongOperation().execute(String.valueOf(LongOperation_SetUpData));
                break;
        }
    }

    public class LongOperation extends AsyncTask<String, String, String> {
        boolean skip = false;

        @Override
        protected String doInBackground(String... strings) {
            try {
                switch (Integer.parseInt(strings[0])) {
                    case LongOperation_SetUpData:
                        blockHashMap = mekanisasiHelper.setBlockMekanisais();
                        break;
                    case LongOperation_SetUpDataDownload:
                        connectionPresenter.GetBlockMekanisasi();
                        break;
                    case LongOperation_UpdateData:
                        skip= !setUpDataSyncHelper.SetBlockMekanisasiMaster(responseApi,this);
                        break;
                }
                return strings[0];
            } catch (Exception e) {
                return "Exception";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result.equals("Exception")) {
                WidgetHelper.showSnackBar(myCoordinatorLayout, "Gagal Prepare Data Long Operation");
                refreshRv.finishRefresh();
            }else{
                switch (Integer.parseInt(result)) {
                    case LongOperation_SetUpData:
                        setupAdapter();
                        refreshRv.finishRefresh();
                        break;
                    case LongOperation_UpdateData:
                        if(skip){
                            WidgetHelper.showSnackBar(myCoordinatorLayout, "Gagal Insert BJRInformation");
                            refreshRv.finishRefresh();
                        } else {
                            new LongOperation().execute(String.valueOf(LongOperation_SetUpData));
                        }
                        break;
                    case LongOperation_SetUpDataDownload:
                        rv.setVisibility(View.GONE);
//                        llSearch.setVisibility(View.GONE);
                        break;
                }
            }
        }
    }
}
