package id.co.ams_plantation.harvestsap.presenter;

import id.co.ams_plantation.harvestsap.presenter.base.BasePresenter;
import id.co.ams_plantation.harvestsap.view.RekonsiliasiView;

public class RekonsiliasiPresenter extends BasePresenter {
    public RekonsiliasiPresenter(RekonsiliasiView view){
        attachView(view);
    }
}
