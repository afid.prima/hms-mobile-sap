package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.AssistensiRequest;
import id.co.ams_plantation.harvestsap.model.BJRInformation;
import id.co.ams_plantation.harvestsap.model.OpnameNFC;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;

/**
 * Created on : 13,December,2022
 * Author     : Afid
 */

public class CekBJRAdapter extends RecyclerView.Adapter<CekBJRAdapter.Holder>{

    Context context;
    ArrayList<BJRInformation> originLists;
    ArrayList<BJRInformation> filterLists;

    public CekBJRAdapter(Context context, ArrayList<BJRInformation> originLists) {
        this.context = context;
        this.originLists = originLists;
        this.filterLists = originLists;
    }

    @NonNull
    @Override
    public CekBJRAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_record,viewGroup,false);
        return new CekBJRAdapter.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CekBJRAdapter.Holder holder, int i) {
        holder.setValue(filterLists.get(i));
    }

    @Override
    public int getItemCount() {
        return filterLists.size();
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                filterLists = new ArrayList<>();
                String string = constraint.toString().toLowerCase();
                int i = 0 ;

                for (BJRInformation bjrInformation : originLists) {
                    try {
                        if (bjrInformation.getBlock().toLowerCase().contains(string) ||
                                bjrInformation.getAfdeling().toLowerCase().contains(string) ||
                                GlobalHelper.getNameOfMonth(bjrInformation.getBulan() - 1).toLowerCase().contains(string) ||
                                String.valueOf(bjrInformation.getBjrAdjustment()).toLowerCase().contains(string) ||
                                String.valueOf(bjrInformation.getTahun()).toLowerCase().contains(string)

                        ) {
                            filterLists.add(bjrInformation);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                if (filterLists.size() > 0) {
                    results.count = filterLists.size();
                    results.values = filterLists;
                    return results;
                } else {
                    return null;
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null) {
                    filterLists = (ArrayList<BJRInformation>) results.values;
                } else {
                    filterLists.clear();
                }
                notifyDataSetChanged();

            }
        };
    }

    public class Holder extends RecyclerView.ViewHolder {
        CardView card_view;
        RelativeLayout rlImage;
        TextView item_ket;
        TextView item_ket2;
        TextView item_ket1;
        TextView item_ket3;

        public Holder(View itemView) {
            super(itemView);

            card_view = itemView.findViewById(R.id.card_view);
            rlImage = itemView.findViewById(R.id.rlImage);
            item_ket = itemView.findViewById(R.id.item_ket);
            item_ket1 = itemView.findViewById(R.id.item_ket1);
            item_ket2 = itemView.findViewById(R.id.item_ket2);
            item_ket3 = itemView.findViewById(R.id.item_ket3);

            item_ket1.setVisibility(View.GONE);
            rlImage.setVisibility(View.GONE);
        }

        public void setValue(BJRInformation bjrInformation) {
            item_ket.setText(bjrInformation.getAfdeling() + "  "+bjrInformation.getBlock());
            item_ket2.setText(GlobalHelper.getNameOfMonth(bjrInformation.getBulan() - 1)+ " "+String.valueOf(bjrInformation.getTahun()));
            item_ket3.setText(String.valueOf(bjrInformation.getBjrAdjustment()) + " Kg");
        }
    }
}
