package id.co.ams_plantation.harvestsap.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import id.co.ams_plantation.harvestsap.Fragment.SettingFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.QrItemAdapter;
import id.co.ams_plantation.harvestsap.connection.Tag;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.ModelRecyclerView;
import id.co.ams_plantation.harvestsap.model.SyncHistroy;
import id.co.ams_plantation.harvestsap.model.SyncHistroyItem;
import id.co.ams_plantation.harvestsap.model.SyncTransaksi;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestsap.ui.MapActivity;
import jp.wasabeef.recyclerview.adapters.SlideInLeftAnimationAdapter;

public class SyncHistoryHelper {

    Context context;
    MapActivity mapActivity;
    MainMenuActivity mainMenuActivity;
    SettingFragment settingFragment;
    android.support.v7.app.AlertDialog listrefrence;

    ArrayList<SyncHistroy> filter;
    ArrayList<SyncHistroy> origin;
    SimpleDateFormat dateFormat;

    public SyncHistoryHelper(Context context) {
        this.context = context;
        dateFormat = new SimpleDateFormat("EEEE, dd MMM yyyy HH:mm:ss");
    }

    public void showAllSyncHistroy(){
        if(context instanceof MapActivity){
            mapActivity = ((MapActivity) context);
        }else if (context instanceof MainMenuActivity){
            mainMenuActivity = ((MainMenuActivity) context);
            Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
            if(fragment instanceof SettingFragment) {
                settingFragment = ((SettingFragment) fragment);
            }
        }
        origin = new ArrayList<>();
        filter = new ArrayList<>();
        SyncHistroy syncHistroyX = getSyncHistory();
        if(syncHistroyX != null) {
            origin.add(syncHistroyX);
            filter.add(syncHistroyX);
        }

        Nitrite db = GlobalHelper.getTableNitritSyncHistory();
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            SyncHistroy syncHistroy = gson.fromJson(dataNitrit.getValueDataNitrit(), SyncHistroy.class);
            origin.add(syncHistroy);
            filter.add(syncHistroy);
        }
        db.close();
        Collections.sort(origin, new Comparator<SyncHistroy>() {
            @Override
            public int compare(SyncHistroy o1, SyncHistroy o2) {
                return o1.getUpdateTime() > o2.getUpdateTime() ? -1 : (o1.getUpdateTime() < o2.getUpdateTime()) ? 1 : 0;
            }
        });

        View view = LayoutInflater.from(context).inflate(R.layout.list_refrensi_object,null);
        TextView idHeader = (TextView) view.findViewById(R.id.idHeader);
        EditText etSearch_lso = (EditText) view.findViewById(R.id.et_search_lso);
        RecyclerView rv_usage_lso = (RecyclerView) view.findViewById(R.id.rv_usage_lso);

        idHeader.setText(context.getResources().getString(R.string.sync_history));
        rv_usage_lso.setLayoutManager(new LinearLayoutManager(context));

        final SelectedSyncHistoryAdapter adapter = new SelectedSyncHistoryAdapter(context);
        final SlideInLeftAnimationAdapter adapterLeft = new SlideInLeftAnimationAdapter(adapter);
        adapterLeft.setFirstOnly(false);
//       adapterLeft.setInterpolator(new OvershootInterpolator(1f));
        adapterLeft.setDuration(400);
        etSearch_lso.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s.toString());
            }
        });
        rv_usage_lso.setAdapter(adapterLeft);

        listrefrence = WidgetHelper.showListReference(listrefrence,view,context);
    }

    class SelectedSyncHistoryAdapter extends RecyclerView.Adapter<SelectedSyncHistoryAdapter.Holder> {
        Context contextApdater;

        public SelectedSyncHistoryAdapter(Context contextApdater) {
            this.contextApdater = contextApdater;
        }

        @NonNull
        @Override
        public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(contextApdater).inflate(R.layout.row_infowindow_synchistory,parent,false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull Holder holder, int position) {
            holder.setValue(filter.get(position));
        }

        @Override
        public int getItemCount() {
            return filter!=null?filter.size():0;
        }

        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults results = new FilterResults();
                    filter = new ArrayList<>();
                    String string = constraint.toString().toLowerCase();
                    int i = 0 ;
                    for (SyncHistroy syncHistroy : origin) {
                        if(syncHistroy.getAction().toLowerCase().contains(string) ||
                            syncHistroy.getSyncFrom().toLowerCase().contains(string)||
                            dateFormat.format(new Date(syncHistroy.getUpdateTime())).toLowerCase().contains(string)
                        ){
                            filter.add(syncHistroy);
                        }
                    }
                    if (filter.size() > 0) {
                        results.count = filter.size();
                        results.values = filter;
                        return results;
                    } else {
                        return null;
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null) {
                        filter = (ArrayList<SyncHistroy>) results.values;
                    } else {
                        filter.clear();
                    }
                    notifyDataSetChanged();

                }
            };
        }

        public class Holder extends RecyclerView.ViewHolder {
            RecyclerView rv_item;
            TextView tv_riw_chose1;
            TextView tv_riw_chose;
            TextView tvwaiting;
            ImageView iv_cvdown;
            ImageView iv_cvup;
            CardView cv_riw_chose;
            LinearLayout ll_status;
            public Holder(View itemView) {
                super(itemView);
                rv_item = (RecyclerView) itemView.findViewById(R.id.rv_item);
                tvwaiting = (TextView) itemView.findViewById(R.id.tvwaiting);
                tv_riw_chose = (TextView) itemView.findViewById(R.id.tv_riw_chose);
                tv_riw_chose1 = (TextView) itemView.findViewById(R.id.tv_riw_chose1);
                iv_cvdown = (ImageView) itemView.findViewById(R.id.iv_cvdown);
                iv_cvup = (ImageView) itemView.findViewById(R.id.iv_cvup);
                cv_riw_chose = (CardView) itemView.findViewById(R.id.cv_riw_chose);
                ll_status = (LinearLayout) itemView.findViewById(R.id.ll_status);
                rv_item.setVisibility(View.GONE);
                iv_cvup.setVisibility(View.GONE);
                tvwaiting.setVisibility(View.GONE);
            }

            public void setValue(SyncHistroy syncHistroy) {
                String text = "Action : "+ syncHistroy.getAction() + "\n";
                text += "Menu : "+ syncHistroy.getSyncFrom();
                tv_riw_chose.setText(text);
                tv_riw_chose1.setText(dateFormat.format(new Date(syncHistroy.getUpdateTime())));

                switch (syncHistroy.getStatusSyncHistory()){
                    case SyncHistroy.Sync_Gagal:
                        ll_status.setBackgroundColor(context.getResources().getColor(R.color.Red));
                        break;
                    case SyncHistroy.Sync_Berhasil:
                        ll_status.setBackgroundColor(context.getResources().getColor(R.color.Green));
                        break;
                    case SyncHistroy.Sync_Berjalan:
                        ll_status.setBackgroundColor(context.getResources().getColor(R.color.Blue));
                        break;
                }

                cv_riw_chose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(iv_cvdown.getVisibility() == View.VISIBLE){
                            iv_cvup.setVisibility(View.VISIBLE);
                            iv_cvdown.setVisibility(View.GONE);
                            tvwaiting.setVisibility(View.VISIBLE);
                            new LongOperation(rv_item,tvwaiting,syncHistroy).execute();
                        }else{
                            iv_cvup.setVisibility(View.GONE);
                            iv_cvdown.setVisibility(View.VISIBLE);
                            rv_item.setVisibility(View.GONE);
                        }
                    }
                });
            }
        }
    }

    public void showDetailHistory(ArrayList<ModelRecyclerView> modelRecyclerViews,RecyclerView recyclerView,TextView textView){
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        QrItemAdapter adapter = new QrItemAdapter(context,modelRecyclerViews);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        recyclerView.setVisibility(View.VISIBLE);
        textView.setVisibility(View.GONE);
    }

    public ArrayList<ModelRecyclerView> itemUploadSyncSettingSync(SyncHistroy syncHistroy){
        ArrayList<ModelRecyclerView> arrayList = new ArrayList<>();
        arrayList.add(new ModelRecyclerView("header",syncHistroy.getAllSyncHistroyItem().get(Tag.PostTph).getMenu(),"",""));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.PostSupervisionPanen).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostSupervisionPanen).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostSupervisionPanen).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostSupervisionPanen).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                    syncHistroy.getAllSyncHistroyItem().get(Tag.PostTphImage).getKeterangan() ,
                    String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTph).getCurrentPersen()),
                    String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTph).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTphImage).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostTph).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTph).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTph).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTph).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiranImage).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiran).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiran).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiranImage).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiran).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiran).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiran).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiran).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiPanenImage).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiPanen).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiPanen).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiPanenImage).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiPanen).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiPanen).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiPanen).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiPanen).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiPanenNFC).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiPanenNFC).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiPanenNFC).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiPanenNFC).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiAngkutImage).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiAngkut).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiAngkut).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiAngkutImage).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiAngkut).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiAngkut).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiAngkut).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiAngkut).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiAngkutNFC).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiAngkutNFC).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiAngkutNFC).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiAngkutNFC).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiSPBImage).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiSPB).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiSPB).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiSPBImage).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiSPB).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiSPB).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiSPB).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTransaksiSPB).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostQcBuahImages).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostQcBuah).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostQcBuah).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostQcBuahImages).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostQcBuah).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostQcBuah).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostQcBuah).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostQcBuah).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostSensusBjrImages).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostSensusBjr).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostSensusBjr).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostSensusBjrImages).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostSensusBjr).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostSensusBjr).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostSensusBjr).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostSensusBjr).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostQcAncak).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostQcAncak).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostQcAncak).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostQcAncak).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostPemanen).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostPemanen).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostPemanen).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostPemanen).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.InsertOpnameNFC).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.InsertOpnameNFC).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.InsertOpnameNFC).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.InsertOpnameNFC).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.InsertGerdangDetail).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.InsertGerdangDetail).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.InsertGerdangDetail).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.InsertGerdangDetail).getUpdateTime())
        ));
        return arrayList;
    }

    public ArrayList<ModelRecyclerView> itemDownloadSyncSettingSync(SyncHistroy syncHistroy){
        ArrayList<ModelRecyclerView> arrayList = new ArrayList<>();
        arrayList.add(new ModelRecyclerView("header",syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterUserByEstate).getMenu(),"",""));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterUserByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterUserByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterUserByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterUserByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetOpnameNFCByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetOpnameNFCByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetOpnameNFCByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetOpnameNFCByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetKonfigurasiGerdang).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetKonfigurasiGerdang).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetKonfigurasiGerdang).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetKonfigurasiGerdang).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPHReasonUnharvestMaster).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPHReasonUnharvestMaster).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPHReasonUnharvestMaster).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPHReasonUnharvestMaster).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetBlockMekanisasi).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetBlockMekanisasi).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetBlockMekanisasi).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetBlockMekanisasi).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetPemanenListByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetPemanenListByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetPemanenListByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetPemanenListByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetOperatorListByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetOperatorListByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetOperatorListByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetOperatorListByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetVehicleListByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetVehicleListByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetVehicleListByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetVehicleListByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetAfdelingAssistantByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetAfdelingAssistantByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetAfdelingAssistantByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetAfdelingAssistantByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetApplicationConfiguration).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetApplicationConfiguration).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetApplicationConfiguration).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetApplicationConfiguration).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetSupervisionListByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetSupervisionListByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetSupervisionListByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetSupervisionListByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetBJRInformation).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetBJRInformation).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetBJRInformation).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetBJRInformation).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetISCCInformation).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetISCCInformation).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetISCCInformation).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetISCCInformation).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterSPKByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterSPKByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterSPKByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterSPKByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterCagesByEstate).getKeterangan(),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterCagesByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterCagesByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterCagesByEstate).getUpdateTime())
                ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetListPKSByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetListPKSByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetListPKSByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetListPKSByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetTphListByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTphListByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTphListByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTphListByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranListByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranListByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranListByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranListByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetTransaksiPanenListByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTransaksiPanenListByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTransaksiPanenListByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTransaksiPanenListByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetTransaksiAngkutListByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTransaksiAngkutListByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTransaksiAngkutListByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTransaksiAngkutListByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetTransaksiSpbListByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTransaksiSpbListByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTransaksiSpbListByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTransaksiSpbListByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetQcBuahByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetQcBuahByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetQcBuahByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetQcBuahByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetSensusBJRByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetSensusBJRByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetSensusBJRByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetSensusBJRByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetQCMutuAncakByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetQCMutuAncakByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetQCMutuAncakByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetQCMutuAncakByEstate).getUpdateTime())
        ));

        arrayList.add(new ModelRecyclerView("header",syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPHCounterByUserID).getMenu(),"",""));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPHCounterByUserID).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPHCounterByUserID).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPHCounterByUserID).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPHCounterByUserID).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranCounterByUserID).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranCounterByUserID).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranCounterByUserID).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranCounterByUserID).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPanenCounterByUserID).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPanenCounterByUserID).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPanenCounterByUserID).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPanenCounterByUserID).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetTAngkutCounterByUserID).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTAngkutCounterByUserID).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTAngkutCounterByUserID).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTAngkutCounterByUserID).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetSPBCounterByUserID).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetSPBCounterByUserID).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetSPBCounterByUserID).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetSPBCounterByUserID).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetQCMutuAncakHeaderCounterByUserID).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetQCMutuAncakHeaderCounterByUserID).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetQCMutuAncakHeaderCounterByUserID).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetQCMutuAncakHeaderCounterByUserID).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPanenSupervisionCounterByUserID).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPanenSupervisionCounterByUserID).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPanenSupervisionCounterByUserID).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPanenSupervisionCounterByUserID).getUpdateTime())
        ));
        return arrayList;
    }

    public ArrayList<ModelRecyclerView> itemUploadSyncMapSync(SyncHistroy syncHistroy){
        ArrayList<ModelRecyclerView> arrayList = new ArrayList<>();
        arrayList.add(new ModelRecyclerView("header",syncHistroy.getAllSyncHistroyItem().get(Tag.PostTph).getMenu(),"",""));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.PostTphImage).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTph).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTph).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTphImage).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostTph).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTph).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTph).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostTph).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiranImage).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiran).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiran).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiranImage).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiran).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiran).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiran).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.PostLangsiran).getUpdateTime())
        ));
        return arrayList;
    }

    public ArrayList<ModelRecyclerView> itemDownlaodSyncMapSync(SyncHistroy syncHistroy){
        ArrayList<ModelRecyclerView> arrayList = new ArrayList<>();
        arrayList.add(new ModelRecyclerView("header",syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterUserByEstate).getMenu(),"",""));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterUserByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterUserByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterUserByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetMasterUserByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetPemanenListByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetPemanenListByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetPemanenListByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetPemanenListByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetOperatorListByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetOperatorListByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetOperatorListByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetOperatorListByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetListPKSByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetListPKSByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetListPKSByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetListPKSByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetTphListByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTphListByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTphListByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTphListByEstate).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranListByEstate).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranListByEstate).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranListByEstate).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranListByEstate).getUpdateTime())
        ));

        arrayList.add(new ModelRecyclerView("header",syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPHCounterByUserID).getMenu(),"",""));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPHCounterByUserID).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPHCounterByUserID).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPHCounterByUserID).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetTPHCounterByUserID).getUpdateTime())
        ));
        arrayList.add(new ModelRecyclerView("progress",
                syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranCounterByUserID).getKeterangan() ,
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranCounterByUserID).getCurrentPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranCounterByUserID).getFinishPersen()),
                String.valueOf(syncHistroy.getAllSyncHistroyItem().get(Tag.GetLangsiranCounterByUserID).getUpdateTime())
        ));
        return arrayList;
    }

    public class LongOperation extends AsyncTask<String, String, String> {
        RecyclerView recyclerView;
        TextView tvwaiting;
        SyncHistroy syncHistroy;
        ArrayList<ModelRecyclerView> modelRecyclerViews;

        public LongOperation(RecyclerView recyclerView,TextView textView, SyncHistroy syncHistroy) {
            this.recyclerView = recyclerView;
            this.tvwaiting = textView;
            this.syncHistroy = syncHistroy;
        }

        @Override
        protected String doInBackground(String... strings) {
            modelRecyclerViews = new ArrayList<>();
            if(syncHistroy.getSyncFrom().equalsIgnoreCase(SyncHistroy.Sync_From_Setting) && syncHistroy.getAction().equalsIgnoreCase(SyncHistroy.Action_Upload)){
                modelRecyclerViews = itemUploadSyncSettingSync(syncHistroy);
                ArrayList<ModelRecyclerView> arrayList = itemDownloadSyncSettingSync(syncHistroy);
                for (ModelRecyclerView modelRecyclerView : arrayList){
                    modelRecyclerViews.add(modelRecyclerView);
                }
            }else if(syncHistroy.getSyncFrom().equalsIgnoreCase(SyncHistroy.Sync_From_Setting) && syncHistroy.getAction().equalsIgnoreCase(SyncHistroy.Action_Sync)){
                modelRecyclerViews = itemDownloadSyncSettingSync(syncHistroy);
            }else if(syncHistroy.getSyncFrom().equalsIgnoreCase(SyncHistroy.Sync_From_MapActivity) && syncHistroy.getAction().equalsIgnoreCase(SyncHistroy.Action_Upload)) {
                modelRecyclerViews = itemUploadSyncMapSync(syncHistroy);
                ArrayList<ModelRecyclerView> arrayList = itemDownlaodSyncMapSync(syncHistroy);
                for (ModelRecyclerView modelRecyclerView : arrayList){
                    modelRecyclerViews.add(modelRecyclerView);
                }
            }else if(syncHistroy.getSyncFrom().equalsIgnoreCase(SyncHistroy.Sync_From_MapActivity) && syncHistroy.getAction().equalsIgnoreCase(SyncHistroy.Action_Sync)) {
                modelRecyclerViews = itemDownlaodSyncMapSync(syncHistroy);
            }else if(syncHistroy.getSyncFrom().equalsIgnoreCase(SyncHistroy.Sync_From_Active) && syncHistroy.getAction().equalsIgnoreCase(SyncHistroy.Action_Sync)) {
                modelRecyclerViews = itemDownloadSyncSettingSync(syncHistroy);
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            showDetailHistory(modelRecyclerViews,recyclerView,tvwaiting);
        }
    }

    public static SyncHistroy creaetNewSyncHistory(String syncFrom, String actionSync){
        HashMap<Tag,SyncHistroyItem> itemHashMap = new HashMap<>();
        for(int i =0 ;i <Tag.values().length;i++){
            try {
                JSONObject object = getKeteranganTag(Tag.values()[i]);
                itemHashMap.put(Tag.values()[i], new SyncHistroyItem(0,
                        Tag.values()[i],
                        object.getString("menu"),
                        object.getString("keterangan"),
                        0,
                        0,
                        false,
                        object.getInt("statusMethod")
                ));
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        SyncHistroy syncHistroy = new SyncHistroy(
                String.valueOf(System.currentTimeMillis()),
                syncFrom,
                actionSync,
                SyncHistroy.Sync_Berjalan,
                itemHashMap
        );
        setSyncHistory(syncHistroy);
        return syncHistroy;
    }

    public static SyncTransaksi getSyncTransaksi(){
        SharedPreferences afdelingPref = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TRANSAKSI, Context.MODE_PRIVATE);
        String syncafdeling = afdelingPref.getString(HarvestApp.SYNC_TRANSAKSI, null);
        Gson gson = new Gson();
        if(syncafdeling != null){
            return gson.fromJson(syncafdeling,SyncTransaksi.class);
        }else{
            return null;
        }
    }

    public static SyncHistroy getSyncHistory(){
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_HISTORY, Context.MODE_PRIVATE);
        String preferString = prefer.getString(HarvestApp.SYNC_HISTORY,null);
        Gson gson = new Gson();
        if(preferString != null) {
            try {
                return gson.fromJson(preferString, SyncHistroy.class);
            }catch (Exception e){
                return null;
            }
        }
        return null;
    }

    public static void setSyncHistory(SyncHistroy syncHistory){
        Gson gson = new Gson();
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_HISTORY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(HarvestApp.SYNC_HISTORY,gson.toJson(syncHistory));
        editor.apply();
    }

    public static JSONObject getKeteranganTag(Tag tag){
        JSONObject object = new JSONObject();
        try {
            switch (tag){
                case GetMasterUserByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetMasterUserByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetOpnameNFCByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetOpnameNFCByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetKonfigurasiGerdang:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetKonfigurasiGerdang));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetTPHReasonUnharvestMaster:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetTPHReasonUnharvestMaster));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetBlockMekanisasi:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetBlockMekanisasi));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetPemanenListByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetPemanenListByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetOperatorListByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetOperatorListByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetVehicleListByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetVehicleListByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetTphListByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetTphListByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetLangsiranListByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetLangsiranListByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetListPKSByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetListPKSByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetAfdelingAssistantByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetAfdelingAssistantByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetApplicationConfiguration:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetApplicationConfiguration));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetSupervisionListByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetSupervisionListByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetBJRInformation:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetBJRInformation));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetISCCInformation:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetISCCInformation));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetMasterSPKByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetMasterSPKByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetMasterCagesByEstate:
                    object.put("Keterangan",HarvestApp.getContext().getResources().getString(R.string.GetMasterCagesByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod", SyncHistroyItem.Status_Method_Get);
                    break;
                case GetSensusBJRByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetSensusBJRByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.master_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetTransaksiPanenListByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetTransaksiPanenListByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.data_transaksi));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetTransaksiAngkutListByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetTransaksiAngkutListByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.data_transaksi));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetTransaksiSpbListByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetTransaksiSpbListByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.data_transaksi));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetQcBuahByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetQcBuahByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.data_transaksi));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetQCMutuAncakByEstate:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetQCMutuAncakByEstate));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.data_transaksi));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case PostPemanen:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostPemanen));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case InsertOpnameNFC:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.InsertOpnameNfc));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case InsertGerdangDetail:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.InsertGerdangDetail));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostTphImage:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostTphImage));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostTph:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostTph));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostLangsiranImage:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostLangsiranImage));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostLangsiran:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostLangsiran));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostTransaksiPanenImage:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostTransaksiPanenImage));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostTransaksiPanen:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostTransaksiPanen));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostTransaksiPanenNFC:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostTransaksiPanenNfc));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostTransaksiAngkutImage:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostTransaksiAngkutImage));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostTransaksiAngkut:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostTransaksiAngkut));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostTransaksiAngkutNFC:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostTransaksiAngkutNfc));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostTransaksiSPBImage:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostTransaksiSPBImage));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostTransaksiSPB:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostTransaksiSPB));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostSupervisionPanen:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostSupervisionPanen));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostQcBuahImages:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostQcBuahImages));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostQcBuah:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostQcBuah));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostSensusBjrImages:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostSensusBjrImages));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostSensusBjr:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostSensusBjr));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case PostQcAncak:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.PostQcAncak));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.upload_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Post);
                    break;
                case GetTPanenCounterByUserID:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetTPanenCounterByUserID));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.counter_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetSPBCounterByUserID:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetSPBCounterByUserID));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.counter_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetTAngkutCounterByUserID:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetTAngkutCounterByUserID));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.counter_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetTPHCounterByUserID:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetTPHCounterByUserID));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.counter_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetLangsiranCounterByUserID:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetLangsiranCounterByUserID));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.counter_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetTPanenSupervisionCounterByUserID:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetTPanenSupervisionCounterByUserID));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.counter_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;
                case GetQCMutuAncakHeaderCounterByUserID:
                    object.put("keterangan",HarvestApp.getContext().getResources().getString(R.string.GetQCMutuAncakHeaderCounterByUserID));
                    object.put("menu",HarvestApp.getContext().getResources().getString(R.string.counter_data));
                    object.put("statusMethod",SyncHistroyItem.Status_Method_Get);
                    break;

            }
            return object;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static void updateSyncHistoryTagWithIndex(SyncHistroyItem syncHistroyItem){
        SyncHistroy syncHistroy = SyncHistoryHelper.getSyncHistory();
        if(syncHistroy != null) {
            HashMap<Tag, SyncHistroyItem> allItemHistory = syncHistroy.getAllSyncHistroyItem();
            allItemHistory.put(syncHistroyItem.getTag(), updateSyncHistoryItem(allItemHistory.get(syncHistroyItem.getTag()), syncHistroyItem));
            setSyncHistory(syncHistroy);
        }
    }

    public static SyncHistroyItem updateSyncHistoryItem(SyncHistroyItem syncHistroyItem,SyncHistroyItem syncHistroyItemUpdate){
        if(syncHistroyItem != null) {
            syncHistroyItem.setCurrentPersen(syncHistroyItemUpdate.getCurrentPersen());
            syncHistroyItem.setFinishPersen(syncHistroyItemUpdate.getFinishPersen());
            if (syncHistroyItemUpdate.getCurrentPersen() == syncHistroyItemUpdate.getFinishPersen() - 1) {
                syncHistroyItem.setCurrentPersen(syncHistroyItemUpdate.getFinishPersen());
                syncHistroyItem.setStatusComplated(true);
            }
        }
        return syncHistroyItem;
    }

    public static void insertHistorySync(int statusHistory){
        SyncHistroy syncHistroy = getSyncHistory();
        if(syncHistroy != null) {
            syncHistroy.setUpdateTime(System.currentTimeMillis());
            syncHistroy.setStatusSyncHistory(statusHistory);


            Nitrite db = GlobalHelper.getTableNitritSyncHistory();
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            int batas = GlobalHelper.MAX_HISTORY_SYNC;
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                if (batas <= 0) {
                    repository.remove(dataNitrit);
                }
                batas--;
            }
            Gson gson = new Gson();
            DataNitrit dataNitrit = new DataNitrit(syncHistroy.getIdSyncHistory(), gson.toJson(syncHistroy));
            repository.insert(dataNitrit);
            db.close();
        }

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_HISTORY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(HarvestApp.SYNC_HISTORY);
        editor.apply();
    }
}
