package id.co.ams_plantation.harvestsap.Fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.labo.kaji.fragmentanimations.MoveAnimation;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.adapter.AngkutAdapter;
import id.co.ams_plantation.harvestsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestsap.TableView.popup.RowHeaderLongPressPopup;
import id.co.ams_plantation.harvestsap.TableView.view_model.AngkutTableViewModel;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.DynamicParameterPenghasilan;
import id.co.ams_plantation.harvestsap.model.Langsiran;
import id.co.ams_plantation.harvestsap.model.SubPemanen;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.CountNfcActivity;
import id.co.ams_plantation.harvestsap.ui.SpbActivity;
import id.co.ams_plantation.harvestsap.util.BjrHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.ModelRecyclerViewHelper;
import id.co.ams_plantation.harvestsap.util.TphHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiPanenHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiSpbHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;

/**
 * Created on : 11,November,2020
 * Author     : Afid
 */

public class CountNfcInputFragment extends Fragment {

    public static final int STATUS_LONG_OPERATION_NONE = 0;
    public static final int STATUS_LONG_OPERATION_SEARCH = 1;
    public static final int STATUS_LONG_OPERATION_ADD_TRANSAKSI_PANEN = 2;
    public static final int STATUS_LONG_OPERATION_ADD_TRANSAKSI_ANGKUT = 3;
    public static final int STATUS_LONG_OPERATION_DELETED_ANGKUT = 4;

    BaseActivity baseActivity;
    CountNfcActivity countNfcActivity;
    TransaksiSpbHelper transaksiSpbHelper;
    TransaksiAngkutHelper transaksiAngkutHelper;
    ModelRecyclerViewHelper modelRecyclerViewHelper;

    AlertDialog alertDialog;
    boolean insertDBAngkut;
    String idHapusAngkut;
    TransaksiPanen idPanenAngkut;
    TransaksiAngkut idAngkutAngkut;

    TableView mTableView;
    AppCompatEditText etSearch;
    Button btnSearch;
    RelativeLayout ltableview;

    AngkutAdapter adapter;
    AngkutTableViewModel mTableViewModel;

    TextView tvTotalOph;
    TextView tvTotalTph;
    TextView tvTotalJanjang;
    TextView tvTotalBrondolan;
    TextView tvTotalBerat;

    TransaksiAngkut transaksiAngkutNFC;
    TransaksiPanen transaksiPanenNFC;

    int tJanjang = 0;
    int tBrondolan = 0;
    double tBerat = 0;
    HashMap<String,Angkut> originAngkut;
    HashSet<String> distinctAfdeling;
    HashMap<String,Angkut> angkutView;
    Set<String> idNfc;

    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm");
    SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
    TphHelper tphHelper;

    String TAG = this.getClass().getName();
    public DynamicParameterPenghasilan dynamicParameterPenghasilan;

    public static CountNfcInputFragment getInstance() {
        CountNfcInputFragment inputFragment = new CountNfcInputFragment();
        return inputFragment;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return MoveAnimation.create(MoveAnimation.UP,enter,600);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.countnfc_input_layout, null, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        baseActivity = (BaseActivity) getActivity();
        countNfcActivity = (CountNfcActivity) getActivity();
        transaksiSpbHelper = new TransaksiSpbHelper(getActivity());
        transaksiAngkutHelper = new TransaksiAngkutHelper(getActivity());
        modelRecyclerViewHelper = new ModelRecyclerViewHelper(getActivity());
        tphHelper = new TphHelper(getActivity());

        mTableView = view.findViewById(R.id.tableview);
        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        ltableview = view.findViewById(R.id.ltableview);

        tvTotalOph = view.findViewById(R.id.tvTotalOph);
        tvTotalTph = view.findViewById(R.id.tvTotalTph);
        tvTotalJanjang = view.findViewById(R.id.tvTotalJanjang);
        tvTotalBrondolan = view.findViewById(R.id.tvTotalBrondolan);
        tvTotalBerat = view.findViewById(R.id.tvTotalBerat);

        dynamicParameterPenghasilan = GlobalHelper.getAppConfig().get(GlobalHelper.getEstate().getEstCode());
        new CountNfcInputFragment.LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));

    }

    public void addAngkut(TransaksiPanen transaksiPanen){
        if(baseActivity.currentlocation != null) {
            if(baseActivity.currentlocation.getLongitude() != 0.0) {
                setidNfc();
                transaksiPanenNFC = transaksiPanen;
                if(transaksiPanenNFC.getNfcFlagTap() != null) {
                    if(transaksiPanenNFC.getNfcFlagTap().getUserTap() != null) {
                        if (!transaksiPanenNFC.getNfcFlagTap().getUserTap().equalsIgnoreCase(GlobalHelper.getUser().getUserID())) {
                            countNfcActivity.searchingGPS = Snackbar.make(countNfcActivity.myCoordinatorLayout, "Kartu ini Sudah Pernah Di Tap SPB Kok Di Tap Lagi", Snackbar.LENGTH_INDEFINITE);
                            countNfcActivity.searchingGPS.show();
                            return;
                        }
                    }
                    if (transaksiPanenNFC.getNfcFlagTap().getCountTap() != 0) {
                        if (transaksiPanenNFC.getNfcFlagTap().getCountTap() > 2) {
                            countNfcActivity.searchingGPS = Snackbar.make(countNfcActivity.myCoordinatorLayout, "Kartu ini Sudah Pernah Di Tap " + transaksiPanenNFC.getNfcFlagTap().getCountTap() + " X", Snackbar.LENGTH_INDEFINITE);
                            countNfcActivity.searchingGPS.show();
                            return;
                        }
                    }
                    if(transaksiPanenNFC.getNfcFlagTap().getWaktuTap() != null) {
                        if (transaksiPanenNFC.getNfcFlagTap().getWaktuTap() != 0) {
                            SimpleDateFormat sdfD = new SimpleDateFormat("dd MMM yyyy");
                            String now = sdfD.format(System.currentTimeMillis());
                            String waktuTap = sdfD.format(transaksiPanenNFC.getNfcFlagTap().getWaktuTap());
                            if (!now.equalsIgnoreCase(waktuTap)) {
                                countNfcActivity.searchingGPS = Snackbar.make(countNfcActivity.myCoordinatorLayout, "Kartu Ini Sudah Di Tap Pada " + waktuTap, Snackbar.LENGTH_INDEFINITE);
                                countNfcActivity.searchingGPS.show();
                                return;
                            }
                        }
                    }
                }

//                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//                String now = sdf.format(System.currentTimeMillis());
//                String tPanen = sdf.format(transaksiPanenNFC.getCreateDate());
//                long selisihLong = System.currentTimeMillis() - transaksiPanenNFC.getCreateDate();
//                Log.d(TAG, "addAngkut: " + String.valueOf(TimeUnit.MILLISECONDS.toDays(
//                        System.currentTimeMillis() - (System.currentTimeMillis() - (1000 * 60 * 60 * 24))
//                )));
                long selisih = TimeUnit.MILLISECONDS.toDays(
                        System.currentTimeMillis() - transaksiPanenNFC.getCreateDate()
                );

                int maxRestan = GlobalHelper.MAX_RESTAN;
                try{
                    if(dynamicParameterPenghasilan.getMaxRestan() > 0) {
                        maxRestan = dynamicParameterPenghasilan.getMaxRestan();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                if(selisih > maxRestan){
                    countNfcActivity.searchingGPS = Snackbar.make(countNfcActivity.myCoordinatorLayout, "Kartu Ini Sudah Restan Lebih Dari "+ maxRestan +" Hari", Snackbar.LENGTH_INDEFINITE);
                    countNfcActivity.searchingGPS.show();
                }else if(!transaksiPanenNFC.getTph().getEstCode().equalsIgnoreCase(GlobalHelper.getEstate().getEstCode())){
                    countNfcActivity.searchingGPS = Snackbar.make(countNfcActivity.myCoordinatorLayout, getResources().getString(R.string.diffrent_estate), Snackbar.LENGTH_INDEFINITE);
                    countNfcActivity.searchingGPS.show();
                }else if(!transaksiPanenNFC.getEstCode().equalsIgnoreCase(GlobalHelper.getEstate().getEstCode())){
                    countNfcActivity.searchingGPS = Snackbar.make(countNfcActivity.myCoordinatorLayout, getResources().getString(R.string.diffrent_estate), Snackbar.LENGTH_INDEFINITE);
                    countNfcActivity.searchingGPS.show();
                }else {
                    new CountNfcInputFragment.LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_ADD_TRANSAKSI_PANEN));
                }
            }else{
//                spbActivity.searchingGPS = Snackbar.make(spbActivity.myCoordinatorLayout, getResources().getString(R.string.diffrent_estate), Snackbar.LENGTH_INDEFINITE);
//                spbActivity.searchingGPS.show();
                WidgetHelper.warningFindGps(countNfcActivity,countNfcActivity.myCoordinatorLayout);
            }
        }else{
//            spbActivity.searchingGPS = Snackbar.make(spbActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//            spbActivity.searchingGPS.show();
            WidgetHelper.warningFindGps(countNfcActivity,countNfcActivity.myCoordinatorLayout);
        }
    }

    public void addAngkut(TransaksiAngkut transaksiAngkut){
        if(baseActivity.currentlocation != null) {
            if(baseActivity.currentlocation.getLongitude() != 0.0) {
                setidNfc();
                transaksiAngkutNFC = transaksiAngkut;
                transaksiAngkutNFC.setFullAngkut(true);
                if(!transaksiAngkutNFC.getEstCode().equalsIgnoreCase(GlobalHelper.getEstate().getEstCode())){
                    Langsiran langsiran = tphHelper.getLangsrian(transaksiAngkutNFC.getLangsiran().getIdTujuan());
                    transaksiAngkutNFC.setEstCode(GlobalHelper.getEstate().getEstCode());
                    if(langsiran == null) {
                        Toast.makeText(HarvestApp.getContext(), "Bukan Dari Estate Yang Sama", Toast.LENGTH_SHORT).show();
                    }else {
                        new CountNfcInputFragment.LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_ADD_TRANSAKSI_ANGKUT));
                    }
                }else {
                    new CountNfcInputFragment.LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_ADD_TRANSAKSI_ANGKUT));
                }
            }else{
//                spbActivity.searchingGPS = Snackbar.make(spbActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                spbActivity.searchingGPS.show();
                WidgetHelper.warningFindGps(countNfcActivity,countNfcActivity.myCoordinatorLayout);
            }
        }else{
//            spbActivity.searchingGPS = Snackbar.make(spbActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//            spbActivity.searchingGPS.show();
            WidgetHelper.warningFindGps(countNfcActivity,countNfcActivity.myCoordinatorLayout);
        }
    }

    private void setidNfc(){
        if(countNfcActivity.myTag != null){
            idNfc = new ArraySet<>();
            String idNfcS = GlobalHelper.bytesToHexString(countNfcActivity.myTag.getId());
            if (!idNfcS.isEmpty()) {
                idNfc.add(idNfcS);
            }
            countNfcActivity.myTag = null;
        }
    }


    public void hapusAngkut(String id ){
        idHapusAngkut = id;
        new CountNfcInputFragment.LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_DELETED_ANGKUT));
    }

    private void addAngkutDB(TransaksiPanen transaksiPanen,Object longOperation){
        insertDBAngkut = true;
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_COUNTNFC_SPB);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            Angkut angkutDb = gson.fromJson(dataNitrit.getValueDataNitrit(),Angkut.class);
            if (angkutDb.getTransaksiPanen() != null) {
                if(angkutDb.getTransaksiPanen().getIdTPanen().equals(transaksiPanen.getIdTPanen())){
                    idPanenAngkut = angkutDb.getTransaksiPanen();
                    insertDBAngkut = false;
                    break;
                }
            }

            for(int i = 0 ; i < transaksiPanen.getKongsi().getSubPemanens().size();i++){
                String oph = TransaksiPanenHelper.getOph(transaksiPanen.getIdTPanen(),transaksiPanen.getKongsi().getSubPemanens().get(i).getOph());
                if(angkutDb.getOph().equalsIgnoreCase(oph)){
                    idPanenAngkut = angkutDb.getTransaksiPanen();
                    insertDBAngkut = false;
                    break;
                }
            }
        }

        if(insertDBAngkut){
            if(!transaksiPanen.getTph().getEstCode().equalsIgnoreCase(GlobalHelper.getEstate().getEstCode())){
                insertDBAngkut = false;
            }
        }

        if(insertDBAngkut){
            if(!transaksiPanen.getEstCode().equalsIgnoreCase(GlobalHelper.getEstate().getEstCode())){
                insertDBAngkut = false;
            }
        }

        if(insertDBAngkut){
            idPanenAngkut = TransaksiAngkutHelper.cekIdTPanenHasBeenTap(transaksiPanen.getIdTPanen());
            if(idPanenAngkut != null){
                insertDBAngkut = false;
            }
        }

        if(insertDBAngkut) {
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmm");
            String idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_COUNTNFC_SPB))
                    + sdf.format(System.currentTimeMillis());

            String idTransaksiRef = idTransaksi;
            Date createDate = new Date(transaksiPanen.getCreateDate());
            Calendar cal = Calendar.getInstance();
            cal.setTime(createDate);
            double bjr = BjrHelper.getBJRInformasi(transaksiPanen.getTph().getBlock(), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR));

            int count = 1;
            for (int i = 0; i < transaksiPanen.getKongsi().getSubPemanens().size(); i++) {
                idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_COUNTNFC_SPB))
                        + sdf.format(System.currentTimeMillis());

                SubPemanen subPemanen = transaksiPanen.getKongsi().getSubPemanens().get(i);
                double berat = subPemanen.getHasilPanen().getTotalJanjang() * bjr;
                String oph = TransaksiPanenHelper.getOph(transaksiPanen.getIdTPanen(),subPemanen.getOph());

                if(oph != ""){
                    idTransaksi = "A"+ oph.replace("-","");
                }

                Angkut angkut = new Angkut(idTransaksi,
                        System.currentTimeMillis(),
                        transaksiPanen,
                        null,
                        baseActivity.currentlocation.getLatitude(),
                        baseActivity.currentlocation.getLongitude(),
                        subPemanen.getHasilPanen().getTotalJanjang(),
                        subPemanen.getHasilPanen().getBrondolan(),
                        bjr,
                        berat,
                        transaksiPanen.getTph().getBlock(),
                        oph,
                        0,
                        transaksiPanen.getTph(),
                        null,
                        null);
                angkut.setIdAngkutRef(idTransaksiRef);
                angkut.setIdNfc(idNfc);

                Gson gson = new Gson();
                DataNitrit dataNitrit = new DataNitrit(angkut.getIdAngkut(), gson.toJson(angkut),idTransaksiRef);
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", angkut.getIdAngkut()));
                if(cursor.size() > 0) {
                    repository.update(dataNitrit);
                }else{
                    repository.insert(dataNitrit);
                    GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_COUNTNFC_SPB);
                }

                try {
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket", "Insert Data Oph");
                    objProgres.put("persen", (count * 100 / transaksiPanen.getKongsi().getSubPemanens().size()));
                    objProgres.put("count", count);
                    objProgres.put("total", (transaksiPanen.getKongsi().getSubPemanens().size()));
                    if(longOperation instanceof CountNfcInputFragment.LongOperation){
                        ((CountNfcInputFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }
                    count++;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        db.close();
    }

    private void addAngkutDB(TransaksiAngkut transaksiAngkut,Object longOperation){
        insertDBAngkut = true;
        HashMap<String,Angkut> isiAngkutDb = new HashMap<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_COUNTNFC_SPB);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            Angkut angkutDb = gson.fromJson(dataNitrit.getValueDataNitrit(), Angkut.class);
            isiAngkutDb.put(angkutDb.getOph(),angkutDb);
        }

        for(int i= 0 ; i < transaksiAngkut.getAngkuts().size(); i++){
            if(isiAngkutDb.get(transaksiAngkut.getAngkuts().get(i).getOph()) != null){
                insertDBAngkut = false;
                break;
            }
        }

        if (insertDBAngkut) {
            if (!transaksiAngkut.getEstCode().equalsIgnoreCase(GlobalHelper.getEstate().getEstCode())) {
                insertDBAngkut = false;
            }
        }

        if (insertDBAngkut) {
            idAngkutAngkut = TransaksiAngkutHelper.cekIdTAngkutHasBeenTap(transaksiAngkut);
            if (idAngkutAngkut != null) {
                if(idAngkutAngkut.getAngkuts().size() > 0){
                    transaksiAngkut = idAngkutAngkut;
                }else {
                    insertDBAngkut = false;
                }
            }
        }

        if (insertDBAngkut) {
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmm");
            String idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_COUNTNFC_SPB))
                    + sdf.format(System.currentTimeMillis());
            String idTransaksiRef = idTransaksi;


            Gson gson = new Gson();
            String isiTransaksiAngkut = gson.toJson(transaksiAngkut);
            TransaksiAngkut objTranskasiAngkut = gson.fromJson(isiTransaksiAngkut,TransaksiAngkut.class);
            objTranskasiAngkut.setAngkuts(null);
            objTranskasiAngkut.setTranskasiAngkutVersions(null);


            int count = 1;
            for(int i= 0 ; i < transaksiAngkut.getAngkuts().size();i++){
                idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_COUNTNFC_SPB))
                        + sdf.format(System.currentTimeMillis());
                Angkut angkutX = transaksiAngkut.getAngkuts().get(i);

                if(angkutX.getOph() != ""){
                    idTransaksi = "A"+ angkutX.getOph().replace("-","");
                }

                Angkut angkut = new Angkut(idTransaksi,
                        System.currentTimeMillis(),
                        null,
                        objTranskasiAngkut,
                        baseActivity.currentlocation.getLatitude(),
                        baseActivity.currentlocation.getLongitude(),
                        angkutX.getJanjang(),
                        angkutX.getBrondolan(),
                        angkutX.getBjr(),
                        angkutX.getBerat(),
                        angkutX.getBlock(),
                        angkutX.getOph(),
                        angkutX.getIdxAngkut() + 1,
                        null,
                        objTranskasiAngkut.getLangsiran(),
                        null);
                angkut.setIdAngkutRef(idTransaksiRef + "_" + angkut.getBlock());
                angkut.setIdNfc(idNfc);

                DataNitrit dataNitrit = new DataNitrit(angkut.getIdAngkut(), gson.toJson(angkut));
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", angkut.getIdAngkut()));
                if(cursor.size() > 0) {
                    repository.update(dataNitrit);
                }else{
                    repository.insert(dataNitrit);
                    GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_COUNTNFC_SPB);
                }

                try {
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket", "Insert Data Oph");
                    objProgres.put("persen", (count * 100 / transaksiAngkut.getAngkuts().size()));
                    objProgres.put("count", count);
                    objProgres.put("total", (transaksiAngkut.getAngkuts().size()));
                    if(longOperation instanceof CountNfcInputFragment.LongOperation){
                        ((CountNfcInputFragment.LongOperation) longOperation).publishProgress(objProgres);
                    }
                    count++;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        db.close();
    }


    private void removeAngkutDB(){
        String idRefAngkut = null;
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_COUNTNFC_SPB);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", idHapusAngkut));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                idRefAngkut = dataNitrit.getParam1();
                Log.d(TAG, "removeAngkutDB: idRefAngkut" + idRefAngkut);
                break;
            }
        }

        cursor = repository.find(ObjectFilters.eq("param1", idRefAngkut));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                repository.remove(dataNitrit);
                Log.d(TAG, "removeAngkutDB: idAngkutHapus" + dataNitrit.getIdDataNitrit());
            }
        }
        db.close();
    }

    private void updateDataRv(int status){
        tJanjang = 0;
        tBrondolan = 0;
        tBerat = 0;
        originAngkut = new HashMap<>();
        distinctAfdeling = new HashSet<>();

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_COUNTNFC_SPB);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            Angkut angkut = gson.fromJson(dataNitrit.getValueDataNitrit(),Angkut.class);
            if (status == STATUS_LONG_OPERATION_SEARCH) {
                String tipeAngkut = "Manual";
                if(angkut.getTransaksiPanen() != null){
                    tipeAngkut = angkut.getTransaksiPanen().getTph().getNamaTph();
                }else if (angkut.getTransaksiAngkut() != null){
                    tipeAngkut = angkut.getTransaksiAngkut().getIdTAngkut();
                }
                if (sdfTime.format(angkut.getWaktuAngkut()).contains(etSearch.getText().toString().toLowerCase()) ||
                        tipeAngkut.toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                        String.valueOf(angkut.getJanjang()).contains(etSearch.getText().toString().toLowerCase()) ||
                        String.valueOf(angkut.getBrondolan()).contains(etSearch.getText().toString().toLowerCase())) {
                    addRowTableAngkut(angkut);
                }
            } else if (status == STATUS_LONG_OPERATION_NONE) {
                addRowTableAngkut(angkut);
            }
        }
        db.close();

        angkutView = new HashMap<>();
        if(originAngkut.size() > 0){
            angkutView = transaksiAngkutHelper.setDataView(new ArrayList<Angkut>(originAngkut.values()));
        }

    }

    private void addRowTableAngkut(Angkut angkut){
        tJanjang += angkut.getJanjang();
        tBrondolan += angkut.getBrondolan();
        tBerat += angkut.getBerat();
        originAngkut.put(angkut.getIdAngkut(),angkut);
        distinctAfdeling.add(transaksiSpbHelper.getAfdeling(angkut));
    }

    private void updateUIRV(int status){
        if(angkutView.size() > 0  ) {

            ltableview.setVisibility(View.VISIBLE);
            mTableViewModel = new AngkutTableViewModel(getContext(), angkutView);
            adapter = new AngkutAdapter(getContext(),mTableViewModel);
            mTableView.setAdapter(adapter);
            mTableView.setTableViewListener(new ITableViewListener() {

                @Override
                public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellDoubleClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {
                        // Create Long Press Popup
                        ColumnHeaderPopup popup = new ColumnHeaderPopup(
                                (ColumnHeaderViewHolder) columnHeaderView, mTableView);
                        // Show
                        popup.show();
                    }
                }

                @Override
                public void onColumnHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }

                @Override
                public void onRowHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }

                @Override
                public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    RowHeaderLongPressPopup popup = new RowHeaderLongPressPopup(rowHeaderView, mTableView,countNfcActivity);
                    popup.show();
                }
            });
            adapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel
                    .getRowHeaderList(), mTableViewModel.getCellList());
        }else{
            ltableview.setVisibility(View.GONE);
        }

        tvTotalOph.setText(String.valueOf(originAngkut.size()));
        tvTotalTph.setText(String.valueOf(angkutView.size()));
        tvTotalJanjang.setText(String.format("%,d",tJanjang) + "Jjg");
        tvTotalBrondolan.setText(String.format("%,d",tBrondolan) + "Kg");
        tvTotalBerat.setText(String.format("%.1f",tBerat) + "Kg");
//        etJanjang.setText(String.valueOf(tJanjang));
//        etBrondolan.setText(String.valueOf(tBrondolan));

    }

    private class LongOperation extends AsyncTask<String, String, String> {
        private AlertDialog alertDialogAllpoin;
        Snackbar snackbar;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(getActivity(), getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])){
                case STATUS_LONG_OPERATION_NONE:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_ADD_TRANSAKSI_PANEN:{
                    addAngkutDB(transaksiPanenNFC,this);
                    break;
                }
                case STATUS_LONG_OPERATION_ADD_TRANSAKSI_ANGKUT:{
                    addAngkutDB(transaksiAngkutNFC,this);
                    break;
                }
            }
            return strings[0];
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            countNfcActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject objProgres = new JSONObject(String.valueOf(values[0]));

                        if(snackbar== null) {
                            if (countNfcActivity != null) {
                                snackbar = Snackbar.make(countNfcActivity.myCoordinatorLayout, objProgres.getString("ket"), Snackbar.LENGTH_INDEFINITE);
                            }
                        }
                        snackbar.setText(String.format(objProgres.getString("ket") + " %d %s [%d/%d] ",
                                objProgres.getInt("persen"), "%", objProgres.getInt("count"), objProgres.getInt("total")));
                        snackbar.setDuration(Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (alertDialogAllpoin != null) {
                alertDialogAllpoin.cancel();
            }
            switch (Integer.parseInt(result)) {
                case STATUS_LONG_OPERATION_NONE: {
                    updateUIRV(Integer.parseInt(result));
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH: {
                    updateUIRV(Integer.parseInt(result));
                    break;
                }
                case STATUS_LONG_OPERATION_ADD_TRANSAKSI_ANGKUT:{
                    if(!insertDBAngkut) {
                        if (idAngkutAngkut != null) {
                            modelRecyclerViewHelper.showSummaryTransaksi(TransaksiAngkutHelper.showHasBeenTap(idAngkutAngkut, countNfcActivity));
                        }else{
                            Toast.makeText(HarvestApp.getContext(),"Transaksi Angkut Ini Sudah Di Angkut",Toast.LENGTH_SHORT).show();
                        }
                    }
                    new CountNfcInputFragment.LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
                    break;
                }
                case STATUS_LONG_OPERATION_ADD_TRANSAKSI_PANEN:{
                    if(!insertDBAngkut) {
                        if (idPanenAngkut != null) {
                            modelRecyclerViewHelper.showSummaryTransaksi(TransaksiAngkutHelper.showHasBeenTap(idPanenAngkut, countNfcActivity));
                        }else{
                            Toast.makeText(HarvestApp.getContext(),"Transaksi Panen Ini Sudah Di Angkut",Toast.LENGTH_SHORT).show();
                        }
                    }
                    new CountNfcInputFragment.LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
                    break;
                }
            }
        }

        public void publishProgress(JSONObject objProgres){
            onProgressUpdate(String.valueOf(objProgres));
        }
    }

}
