package id.co.ams_plantation.harvestsap.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created on : 15,November,2021
 * Author     : Afid
 */

public class ReasonTphBayangan {

    public static final int idReasonKesalahanMeletakanBuah = 0;
    public static final int idReasonBanjir = 1;
    public static final int idReasonJalanRusak = 2;
    public static final int idReasonTidakAdaTitiPanen = 3;
    public static final int idReasonOthers = 4;
    public static final int idReasonKosong = 5;

    public static final String nameReasonKesalahanMeletakanBuah = "Kesalahan pemanen meletakkan buah";
    public static final String nameReasonBanjir = "Banjir";
    public static final String nameReasonJalanRusak = "Jalan Rusak";
    public static final String nameReasonTidakAdaTitiPanen = "Tidak ada Titi panen";
    public static final String nameReasonOthers = "Lain - Lain";
    public static final String nameReasonKosong = "Mohon Pilih Alasan";

    public static final List<String> listReason = Arrays.asList(
            nameReasonKesalahanMeletakanBuah,
            nameReasonBanjir,
            nameReasonJalanRusak,
            nameReasonTidakAdaTitiPanen,
            nameReasonOthers,
            nameReasonKosong
    );

    int idReason;
    String nameReason;

    public ReasonTphBayangan() {
    }

    public ReasonTphBayangan(int idReason, String nameReason) {
        this.idReason = idReason;
        this.nameReason = nameReason;
    }

    public int getIdReason() {
        return idReason;
    }

    public void setIdReason(int idReason) {
        this.idReason = idReason;
    }

    public String getNameReason() {
        return nameReason;
    }

    public void setNameReason(String nameReason) {
        this.nameReason = nameReason;
    }
}
