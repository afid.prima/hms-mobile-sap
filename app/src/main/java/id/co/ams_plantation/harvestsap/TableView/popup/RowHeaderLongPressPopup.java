package id.co.ams_plantation.harvestsap.TableView.popup;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.evrencoskun.tableview.TableView;

import id.co.ams_plantation.harvestsap.Fragment.AngkutFragment;
import id.co.ams_plantation.harvestsap.Fragment.CountNfcInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.MekanisasiInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.MekanisasiPanenInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.SpbFragment;
import id.co.ams_plantation.harvestsap.Fragment.SpbInputFragment;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestsap.ui.CountNfcActivity;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestsap.ui.MekanisasiActivity;
import id.co.ams_plantation.harvestsap.ui.SpbActivity;

import static id.co.ams_plantation.harvestsap.R.id.content_container;


public class RowHeaderLongPressPopup extends PopupMenu implements PopupMenu
        .OnMenuItemClickListener {

    // Menu Item constants
    private static final int EDIT_ROW = 2;
    private static final int REMOVE_ROW = 3;

    @NonNull
    private final TableView mTableView;
    private final int mRowPosition;
    private final Context context;

    public RowHeaderLongPressPopup(@NonNull RecyclerView.ViewHolder viewHolder, @NonNull TableView tableView, Context context) {
        super(viewHolder.itemView.getContext(), viewHolder.itemView);

        this.mTableView = tableView;
        this.mRowPosition = viewHolder.getAdapterPosition();
        this.context = context;

        initialize();
    }

    private void initialize() {
        createMenuItem();

        this.setOnMenuItemClickListener(this);
    }

    private void createMenuItem() {
        if(context instanceof MekanisasiActivity){
            this.getMenu().add(Menu.NONE, EDIT_ROW, 2, "Detail");
        }else {
            this.getMenu().add(Menu.NONE, REMOVE_ROW, 2, "Hapus");
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        // Note: item id is index of menu item..

        switch (menuItem.getItemId()) {
            case REMOVE_ROW:
                if(context instanceof SpbActivity){
                    mTableView.getAdapter().removeRow(mRowPosition);
                    SpbActivity spbActivity = ((SpbActivity) context);
                    if (spbActivity.getSupportFragmentManager().findFragmentById(content_container) instanceof SpbInputFragment) {
                        SpbInputFragment fragment = (SpbInputFragment) spbActivity.getSupportFragmentManager().findFragmentById(content_container);
                        TableViewCell cell = ((TableViewCell) mTableView.getAdapter().getCellItem(0,mRowPosition));
                        String [] id = cell.getId().split("-");
                        fragment.hapusAngkut(id[1]);
                    }
//                }else if(context instanceof MainMenuActivity){
//                    android.support.v4.app.Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
//                    TableViewCell cell = ((TableViewCell) mTableView.getAdapter().getCellItem(0,mRowPosition));
//                    String [] id = cell.getId().split("-");
//                    Log.d(String.valueOf(this.getClass()) + " rowHeader ",id.toString());
//                    if(fragment instanceof SpbFragment) {
//                        SpbFragment spbFragment = ((SpbFragment) fragment);
//                        spbFragment.hapusNotif(id[1],mRowPosition);
//                    }else if(fragment instanceof AngkutFragment){
//                        AngkutFragment angkutFragment = ((AngkutFragment) fragment);
//                        angkutFragment.hapusNotif(id[1],mRowPosition);
//                    }
                }else if (context instanceof CountNfcActivity){
                    mTableView.getAdapter().removeRow(mRowPosition);
                    CountNfcActivity countNfcActivity = ((CountNfcActivity) context);
                    if (countNfcActivity.getSupportFragmentManager().findFragmentById(content_container) instanceof CountNfcInputFragment) {
                        CountNfcInputFragment fragment = (CountNfcInputFragment) countNfcActivity.getSupportFragmentManager().findFragmentById(content_container);
                        TableViewCell cell = ((TableViewCell) mTableView.getAdapter().getCellItem(0,mRowPosition));
                        String [] id = cell.getId().split("-");
                        fragment.hapusAngkut(id[1]);
                    }
                }
                break;
            case EDIT_ROW:
                if(context instanceof MekanisasiActivity){
                    MekanisasiActivity mekanisasiActivity = ((MekanisasiActivity) context);
                    if (mekanisasiActivity.getSupportFragmentManager().findFragmentById(content_container) instanceof MekanisasiInputFragment) {
                        MekanisasiInputFragment fragment = (MekanisasiInputFragment) mekanisasiActivity.getSupportFragmentManager().findFragmentById(content_container);
                        TableViewCell cell = ((TableViewCell) mTableView.getAdapter().getCellItem(0,mRowPosition));
                        String [] id = cell.getId().split(";");
                        fragment.pilihIdTpanen(id[1]);
                    }else if (mekanisasiActivity.getSupportFragmentManager().findFragmentById(content_container) instanceof MekanisasiPanenInputFragment) {
                        MekanisasiPanenInputFragment fragment = (MekanisasiPanenInputFragment) mekanisasiActivity.getSupportFragmentManager().findFragmentById(content_container);
                        TableViewCell cell = ((TableViewCell) mTableView.getAdapter().getCellItem(0,mRowPosition));
                        String [] id = cell.getId().split(";");
                        fragment.mekanisasiHelper.showFormMekanisasi(fragment.mekanisasiPanens.get(id[1]),false);
                    }
                }
                break;
        }
        return true;
    }
}