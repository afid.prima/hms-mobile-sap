package id.co.ams_plantation.harvestsap.Fragment;

import android.content.Context;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.labo.kaji.fragmentanimations.MoveAnimation;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.RecordIterable;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.AngkutInputAdapter;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.Langsiran;
import id.co.ams_plantation.harvestsap.model.StampImage;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.service.BluetoothService;
import id.co.ams_plantation.harvestsap.ui.AngkutActivity;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.ModelRecyclerViewHelper;
import id.co.ams_plantation.harvestsap.util.NfcHelper;
import id.co.ams_plantation.harvestsap.util.TphHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import ng.max.slideview.SlideView;

public class LangsiranInputFragment extends Fragment {

    CompleteTextViewHelper etBlock;
    CompleteTextViewHelper etLangsiran;

    AppCompatEditText etSearch;
    Button btnSearch;
    CheckBox cbAll;
    LinearLayout btnFilter;
    TextView txtFilter;
    TextView tvSelected;
    RecyclerView rv;

    TextView tvId;
    TextView tvTotalAngkut;
    TextView tvTotalBrondolan;
    TextView tvTotalJanjang;
    TextView tvTotalBeratEstimasi;

    LinearLayout ketAction1;
    LinearLayout lsave;
    LinearLayout lldeleted;
    LinearLayout lcancel;
    PopupMenu popupMenu;
    MenuItem menuItemSelected;
    TphHelper tphHelper;

    BaseActivity baseActivity;
    AngkutActivity angkutActivity;
    TransaksiAngkutHelper transaksiAngkutHelper;
    Angkut selectedAngkut;
    TransaksiAngkut idAngkutAngkut;
    ModelRecyclerViewHelper modelRecyclerViewHelper;
    TransaksiAngkut transaksiAngkutNFC;
    TransaksiAngkut trnaskasiAngkutNFCSave;
    StampImage stampImage;

    AngkutInputAdapter angkutInputAdapter;
    ArrayList<Angkut> sortAngkut;
    HashMap<String,Angkut> dataAngkut;
    HashMap<String,Angkut> dataAngkutSelected;

    double latitude;
    double longitude;
    String blockName;
    Angkut totalAngkut;
    Angkut totalAngkutAllChecked;
    String idTransaksi;
    AlertDialog alertDialog;

    public final int LongOperation_SetupTableAngkut = 0;
    public final int LongOperation_Save = 1;
    public final int LongOperation_Save_T2_NFC = 2;
    public final int LongOperation_Save_T2_Print = 3;
    public final int LongOperation_Save_T2_Share = 4;
    public final int LongOperation_Deleted = 5;

    final int YesNo_Deleted = 0;
    final int YesNo_Save = 1;
    final int YesNo_Cancel = 2;

    final int MenuItem_Oph = 0;
    final int MenuItem_Block = 1;
    final int MenuItem_Janjang = 2;
    final int MenuItem_Brondolan = 3;
    final int MenuItem_Berat = 4;

    final String [] menuOrder = {
            "OPH","Block","Janjang","Brondolan","Berat"
    };

    String idNfc = "";
    HashMap<String,Angkut> angkutView;
    boolean dariAngkut;

    private static final String TAG = LangsiranInputFragment.class.getSimpleName();

    public static LangsiranInputFragment getInstance() {
        Log.i(TAG, "getInstance: no bundle");
        LangsiranInputFragment langsiranInputFragment = new LangsiranInputFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("dariAngkut", 0);
        langsiranInputFragment.setArguments(bundle);
        return langsiranInputFragment;
    }

    /*
     * Existing param
     * 1 = dariAngkut
     */
    public static LangsiranInputFragment getInstance(int dariAngkut) {
        Log.i(TAG, "getInstance: WITH bundle");
        Bundle bundle = new Bundle();
        bundle.putInt("dariAngkut", dariAngkut);
        LangsiranInputFragment langsiranInputFragment = new LangsiranInputFragment();
        langsiranInputFragment.setArguments(bundle);
        return langsiranInputFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            dariAngkut = getArguments().getInt("dariAngkut") == 1;
        }
        Log.i(TAG, "arguments: "+ getArguments().getInt("dariAngkut"));
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return MoveAnimation.create(MoveAnimation.UP,enter,600);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.langsiran_angkut_layout, null, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        modelRecyclerViewHelper = new ModelRecyclerViewHelper(getActivity());
        tphHelper = new TphHelper(getActivity());

        baseActivity = ((BaseActivity) getActivity());
        if(getActivity() instanceof AngkutActivity) {
            angkutActivity = ((AngkutActivity) getActivity());
            transaksiAngkutHelper = new TransaksiAngkutHelper(getActivity());
            if (angkutActivity.selectedAngkut != null){
                selectedAngkut = angkutActivity.selectedAngkut;
                angkutActivity.selectedAngkut = null;
            }
        }

        etBlock = view.findViewById(R.id.etBlock);
        etLangsiran = view.findViewById(R.id.etLangsiran);
        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        cbAll = view.findViewById(R.id.cbAll);
        btnFilter = view.findViewById(R.id.btnFilter);
        txtFilter = view.findViewById(R.id.txtFilter);
        tvSelected = view.findViewById(R.id.tvSelected);
        rv = view.findViewById(R.id.rv);
        tvId = view.findViewById(R.id.tvId);
        tvTotalAngkut = view.findViewById(R.id.tvTotalAngkut);
        tvTotalBrondolan = view.findViewById(R.id.tvTotalBrondolan);
        tvTotalJanjang = view.findViewById(R.id.tvTotalJanjang);
        tvTotalBeratEstimasi = view.findViewById(R.id.tvTotalBeratEstimasi);
        ketAction1 = view.findViewById(R.id.ketAction1);
        lsave = view.findViewById(R.id.lsave);
        lldeleted = view.findViewById(R.id.lldeleted);
        lcancel = view.findViewById(R.id.lcancel);

        popupMenu = new PopupMenu(getActivity(),btnFilter);
        popupMenu.getMenu().add(0,MenuItem_Oph,0,menuOrder[0]);
        popupMenu.getMenu().add(0,MenuItem_Block,1,menuOrder[1]);
        popupMenu.getMenu().add(0,MenuItem_Janjang,2,menuOrder[2]);
        popupMenu.getMenu().add(0,MenuItem_Brondolan,3,menuOrder[3]);
        popupMenu.getMenu().add(0,MenuItem_Berat,4,menuOrder[4]);

        menuItemSelected = popupMenu.getMenu().getItem(0);
        txtFilter.setText("Sort By:\nOPH");
        cbAll.setChecked(true);

        //cek apakah ini dari tap tableview pada angkutInputFragment Atau bukan
        //jika dari tap tableview pada angkutInputFragment maka selectedAngkut Tidak null
        if(selectedAngkut == null) {
            //cek apakah transaksi panen atau transaksi angkut sudah pernah di input atau belum
            //jika sudah ada di db maka kembali ke fragment angkutInputFragment
            boolean sudahAda = false;
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                Angkut angkut = gson.fromJson(dataNitrit.getValueDataNitrit(), Angkut.class);
                if (angkut.getTransaksiAngkut() != null && angkutActivity.transaksiAngkutFromNFC != null) {
                    if (angkut.getOph().equals(angkutActivity.transaksiAngkutFromNFC.getAngkuts().get(0).getOph())) {
                        Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.transaksiAngkut_has_exists), Toast.LENGTH_LONG).show();
                        sudahAda = true;
                        break;
                    }
                }
            }
            db.close();

            if(angkutActivity.myTag != null) {
                idNfc = GlobalHelper.bytesToHexString(angkutActivity.myTag.getId());
                angkutActivity.myTag = null;
            }

            if(!sudahAda){

                if(angkutActivity.transaksiAngkutFromNFC != null){
                    idAngkutAngkut = TransaksiAngkutHelper.cekIdTAngkutHasBeenTap(angkutActivity.transaksiAngkutFromNFC);
                    if (idAngkutAngkut != null) {
                        if(idAngkutAngkut.getAngkuts().size() > 0){
                            angkutActivity.transaksiAngkutFromNFC = idAngkutAngkut;
                        }else {
                            modelRecyclerViewHelper.showSummaryTransaksi(TransaksiAngkutHelper.showHasBeenTap(idAngkutAngkut, angkutActivity));
                            sudahAda = true;
                        }
                    }

                    if(!angkutActivity.transaksiAngkutFromNFC.getEstCode().equalsIgnoreCase(GlobalHelper.getEstate().getEstCode())){
                        Langsiran langsiran = tphHelper.getLangsrian(angkutActivity.transaksiAngkutFromNFC.getLangsiran().getIdTujuan());
                        angkutActivity.transaksiAngkutFromNFC.setEstCode(GlobalHelper.getEstate().getEstCode());
                        if(langsiran == null) {
                            Toast.makeText(HarvestApp.getContext(), "Bukan Dari Estate Yang Sama", Toast.LENGTH_SHORT).show();
                            sudahAda = true;
                        }
                    }
                }
            }

            if (sudahAda) {
                angkutActivity.backProses();
            }

            lldeleted.setVisibility(View.GONE);
        }else{
            //edit angkut
            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", selectedAngkut.getIdAngkut()));
            if (cursor.size() > 0) {
                for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                    DataNitrit dataNitrit = (DataNitrit) iterator.next();
                    Gson gson = new Gson();
                    Angkut angkut = gson.fromJson(dataNitrit.getValueDataNitrit(), Angkut.class);
                    angkutActivity.transaksiAngkutFromNFC = angkut.getTransaksiAngkut();
                    break;
                }
            }
            db.close();
            cbAll.setChecked(false);
            idTransaksi = selectedAngkut.getIdAngkut();
            if(selectedAngkut.getIdNfc().size() > 0){
                for(String s : selectedAngkut.getIdNfc()) {
                    idNfc = s;
                    break;
                }
            }
        }

        if(baseActivity.currentLocationOK()){
            if (angkutActivity.searchingGPS != null) {
                angkutActivity.searchingGPS.dismiss();
            }
            longitude = baseActivity.currentlocation.getLongitude();
            latitude = baseActivity.currentlocation.getLatitude();
        }

        if(angkutActivity.transaksiAngkutFromNFC != null) {
            setUpTransaksiAngkutNFC();

            etBlock.setAdapter(null);
            etLangsiran.setAdapter(null);

            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etBlock.getWindowToken(), 0);
            InputMethodManager imm2 = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm2.hideSoftInputFromWindow(etLangsiran.getWindowToken(), 0);
        }

        blockName = etBlock.getText().toString();

        cbAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                idTransaksi= null;
                if(cbAll.isChecked()){
                    cbAll.setText("Check All");
                }else {
                    cbAll.setText("Un Check All");
                }
                new LongOperation().execute(String.valueOf(LongOperation_SetupTableAngkut));
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupMenu.show();
            }
        });

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                txtFilter.setText("Sort By:\n"+item.getTitle());
                menuItemSelected = item;
                new LongOperation().execute(String.valueOf(LongOperation_SetupTableAngkut));
                return true;
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                angkutInputAdapter.getFilter().filter(etSearch.getText().toString());
            }
        });

        lsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dataAngkutSelected.size() == 0){
                    Toast.makeText(HarvestApp.getContext(),"Anda Harus Pilih Oph",Toast.LENGTH_SHORT).show();
                    return;
                }

                angkutActivity.batchKartuAngkut = new ArrayList<>();
                angkutActivity.idxTap = 0;

                showYesNo("Apakah Anda Yakin Untuk Simpan Angkut Ini",YesNo_Save);
            }
        });

        lldeleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showYesNo("Apakah Anda Yakin Untuk Hapus Angkut Ini",YesNo_Deleted);
            }
        });

        lcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showYesNo("Apakah Anda Yakin Untuk Keluar Dari Angkut Ini",YesNo_Cancel);
            }
        });
    }

    private void setUpTransaksiAngkutNFC(){
        if(angkutActivity.transaksiAngkutFromNFC.getLangsiran() != null) {
            if(stampImage == null){
                stampImage = new StampImage();
            }
            stampImage.setBlock(angkutActivity.transaksiAngkutFromNFC.getLangsiran().getBlock());
            stampImage.setLangsiran(angkutActivity.transaksiAngkutFromNFC.getLangsiran());

            tvId.setText("Id Deklarasi Angkut : "+angkutActivity.transaksiAngkutFromNFC.getIdTAngkut());
            etBlock.setText(angkutActivity.transaksiAngkutFromNFC.getLangsiran().getBlock());
            etLangsiran.setText(angkutActivity.transaksiAngkutFromNFC.getLangsiran().getNamaTujuan());

            etBlock.setFocusable(false);
            etLangsiran.setFocusable(false);
        }

        transaksiAngkutNFC = angkutActivity.transaksiAngkutFromNFC;
        angkutActivity.transaksiAngkutFromNFC = null;

        new LongOperation().execute(String.valueOf(LongOperation_SetupTableAngkut));
    }

    public void updateDataSelected(Angkut angkut,boolean isChecked){
        int awal = dataAngkutSelected.size();
        if(isChecked) {
            dataAngkutSelected.put(angkut.getOph(), angkut);
            if (awal != dataAngkutSelected.size()) {
                totalAngkut.setJanjang(totalAngkut.getJanjang() + angkut.getJanjang());
                totalAngkut.setBrondolan(totalAngkut.getBrondolan() + angkut.getBrondolan());
                totalAngkut.setBerat(totalAngkut.getBerat() + angkut.getBerat());
            }
            if(dataAngkutSelected.size() == dataAngkut.size()){
                cbAll.setChecked(true);
            }
        }else{
            dataAngkutSelected.remove(angkut.getOph());
            if (awal != dataAngkutSelected.size()) {
                totalAngkut.setJanjang(totalAngkut.getJanjang() - angkut.getJanjang());
                totalAngkut.setBrondolan(totalAngkut.getBrondolan() - angkut.getBrondolan());
                totalAngkut.setBerat(totalAngkut.getBerat() - angkut.getBerat());
            }
            cbAll.setChecked(false);
        }

        if(cbAll.isChecked()){
            dataAngkutSelected = new HashMap<>(dataAngkut);
            totalAngkut = totalAngkutAllChecked;
        }

        tvSelected.setText(String.format("Memilih : %,d OPH",dataAngkutSelected.size()));
        syncTotalUI();
    }

    public void syncTotalUI(){
        int angkut = 0;
        if(dataAngkutSelected.size() > 0 && !dariAngkut ){
            angkut = 1;
        }
        tvTotalAngkut.setText(String.valueOf(angkutActivity.sigmaAngkut.getAngkuts().size() + angkut));
        tvTotalJanjang.setText(String.format("%,d",angkutActivity.sigmaAngkut.getTotalJanjang() + totalAngkut.getJanjang()) + "Jjg");
        tvTotalBrondolan.setText(String.format("%,d",angkutActivity.sigmaAngkut.getTotalBrondolan() + totalAngkut.getBrondolan()) + "Kg");
        tvTotalBeratEstimasi.setText(String.format("%.0f", angkutActivity.sigmaAngkut.getTotalBeratEstimasi() + totalAngkut.getBerat()) + "Kg");
    }

    public boolean cekSudahAngkut(Angkut angkut,boolean isChecked){
        if(idTransaksi != null) {
            if (transaksiAngkutNFC.getAngkuts().size() > 0) {
                for (Angkut angkut1 : transaksiAngkutNFC.getAngkuts()) {
                    if (angkut.getOph().equalsIgnoreCase(angkut1.getOph())) {
                        isChecked = angkut1.isHasBeenAngkut();
                    }

                    if(angkut1.isHasBeenAngkut()){
                        dataAngkutSelected.put(angkut1.getOph(),angkut1);
                    }
                }
            }
        }
        return isChecked;
    }

    private void setupUpdateDataAngkut(){
        if(dataAngkut == null){
            dataAngkut = new HashMap<>();
            for (Angkut angkut : transaksiAngkutNFC.getAngkuts()){
                dataAngkut.put(angkut.getOph(),angkut);
            }
        }

        dataAngkutSelected = new HashMap<>();
        totalAngkut = new Angkut();
        sortAngkut = new ArrayList<>(dataAngkut.values());
        switch (menuItemSelected.getItemId()){
            case MenuItem_Oph:{
                Collections.sort(sortAngkut, new Comparator<Angkut>() {
                    @Override
                    public int compare(Angkut t0, Angkut t1) {
                        return (t0.getOph().compareTo(t1.getOph()));
                    }
                });
                break;
            }
            case MenuItem_Block:{
                Collections.sort(sortAngkut, new Comparator<Angkut>() {
                    @Override
                    public int compare(Angkut t0, Angkut t1) {
                        return (t0.getBlock().compareTo(t1.getBlock()));
                    }
                });
                break;
            }
            case MenuItem_Janjang:{
                Collections.sort(sortAngkut, new Comparator<Angkut>() {
                    @Override
                    public int compare(Angkut t0, Angkut t1) {
                        return Integer.compare(t0.getJanjang(), t1.getJanjang());
                    }
                });
                break;
            }
            case MenuItem_Brondolan:{
                Collections.sort(sortAngkut, new Comparator<Angkut>() {
                    @Override
                    public int compare(Angkut t0, Angkut t1) {
                        return Integer.compare(t0.getBrondolan(), t1.getBrondolan());
                    }
                });
                break;
            }
            case MenuItem_Berat:{
                Collections.sort(sortAngkut, new Comparator<Angkut>() {
                    @Override
                    public int compare(Angkut t0, Angkut t1) {
                        return Double.compare(t0.getBerat(), t1.getBerat());
                    }
                });
                break;
            }
        }

        if(totalAngkutAllChecked == null){
            totalAngkutAllChecked = new Angkut();
            for(Angkut angkut : sortAngkut){
                totalAngkutAllChecked.setJanjang(totalAngkutAllChecked.getJanjang() + angkut.getJanjang());
                totalAngkutAllChecked.setBrondolan(totalAngkutAllChecked.getBrondolan() + angkut.getBrondolan());
                totalAngkutAllChecked.setBerat(totalAngkutAllChecked.getBerat() + angkut.getBerat());
            }
        }

        if(cbAll.isChecked()){
            dataAngkutSelected = new HashMap<>(dataAngkut);
            totalAngkut = new Angkut(totalAngkutAllChecked.getJanjang(),totalAngkutAllChecked.getBrondolan(),totalAngkutAllChecked.getBerat());
        }else{
            dataAngkutSelected = new HashMap<>();
            totalAngkut = new Angkut();
        }
    }

    private void setupUpdateTableAngkut(){
        etSearch.setText("");
        angkutInputAdapter = new AngkutInputAdapter(getActivity(),sortAngkut,cbAll.isChecked());
        rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv.setAdapter(angkutInputAdapter);
    }

    private void deletedAngkut(){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        if(selectedAngkut != null){
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            if(((RecordIterable<DataNitrit>) Iterable).size() > 0) {
                for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                    DataNitrit dataNitrit = (DataNitrit) iterator.next();
                    if(dataNitrit.getParam1().contains(selectedAngkut.getTransaksiAngkut().getIdTAngkut())) {
                        repository.remove(dataNitrit);
                    }
                }
            }
        }
        db.close();
    }

    private void saveAngkut(Object longOperation){
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmm");

        if (selectedAngkut != null) {
            idTransaksi = selectedAngkut.getIdAngkut();
        } else {
            idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_ANGKUT))
                    + sdf.format(System.currentTimeMillis());
        }

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        if(selectedAngkut != null){
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("param1", selectedAngkut.getIdAngkutRef()));
            if(cursor.size() > 0) {
                for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                    DataNitrit dataNitrit = (DataNitrit) iterator.next();
                    repository.remove(dataNitrit);
                }
            }
        }

        ArrayList<Angkut> angkutX = new ArrayList<>();
        for(int i = 0 ; i < transaksiAngkutNFC.getAngkuts().size(); i++){
            Angkut angkut = transaksiAngkutNFC.getAngkuts().get(i);
            Angkut addAngkut = new Angkut(angkut.getOph(),
                    angkut.getBlock(),
                    angkut.getJanjang(),
                    angkut.getBrondolan(),
                    angkut.getBerat(),
                    angkut.getIdxAngkut()
            );
            addAngkut.setHasBeenAngkut(dataAngkutSelected.get(angkut.getOph()) != null);
            angkutX.add(addAngkut);
        }

        int count = 1;
        Gson gson = new Gson();
        String idTransaksiRef = transaksiAngkutNFC.getIdTAngkut();
        String dataTAngkut = gson.toJson(transaksiAngkutNFC);
        TransaksiAngkut tAngkut = gson.fromJson(dataTAngkut,TransaksiAngkut.class);
        tAngkut.setAngkuts(angkutX);
        tAngkut.setTranskasiAngkutVersions(null);
        for (Angkut angkut : dataAngkutSelected.values()) {
            angkut.setIdAngkut(idTransaksi);
            angkut.setWaktuAngkut(System.currentTimeMillis());
            angkut.setIdxAngkut(angkut.getIdxAngkut() + 1);
            angkut.setIdAngkutRef(idTransaksiRef + "_" + angkut.getBlock());
            angkut.setTransaksiAngkut(tAngkut);
            angkut.setLatitude(latitude);
            angkut.setLongitude(longitude);

            Set<String> SidNfc = new ArraySet<>();
            if(!idNfc.isEmpty()) {
                SidNfc.add(idNfc);
                angkut.setIdNfc(SidNfc);
            }

            DataNitrit dataNitrit = new DataNitrit(angkut.getIdAngkut(), gson.toJson(angkut), idTransaksiRef);
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", angkut.getIdAngkut()));
            if(cursor.size() > 0) {
                repository.update(dataNitrit);
            }else {
                repository.insert(dataNitrit);
                GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_ANGKUT);
            }
            idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_ANGKUT))
                    + sdf.format(System.currentTimeMillis());

            try {
                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", "Insert Data Oph");
                objProgres.put("persen", (count * 100 / dataAngkutSelected.values().size()));
                objProgres.put("count", count);
                objProgres.put("total", (dataAngkutSelected.values().size()));
                if(longOperation instanceof LongOperation){
                    ((LongOperation) longOperation).publishProgress(objProgres);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            count++;
        }
        db.close();

        if(dataAngkut.size() != dataAngkutSelected.size()){
            ArrayList<Angkut> angkutSisa = new ArrayList<>();
            Angkut totalAngkut = new Angkut();
            for(Angkut angkut : dataAngkut.values()){
                if(dataAngkutSelected.get(angkut.getOph()) == null){
                    totalAngkut.setJanjang(totalAngkut.getJanjang() + angkut.getJanjang());
                    totalAngkut.setBrondolan(totalAngkut.getBrondolan() + angkut.getBrondolan());
                    totalAngkut.setBerat(totalAngkut.getBerat() + angkut.getBerat());
                    angkutSisa.add(angkut);
                }
            }

            trnaskasiAngkutNFCSave = transaksiAngkutNFC;
            trnaskasiAngkutNFCSave.setTotalJanjang(totalAngkut.getJanjang());
            trnaskasiAngkutNFCSave.setTotalBrondolan(totalAngkut.getBrondolan());
            trnaskasiAngkutNFCSave.setTotalBeratEstimasi(totalAngkut.getBerat());
            trnaskasiAngkutNFCSave.setLatitudeTujuan(latitude);
            trnaskasiAngkutNFCSave.setLongitudeTujuan(longitude);
            trnaskasiAngkutNFCSave.setAngkuts(angkutSisa);

            TransaksiAngkut transaksiAngkut1 = null;

            //tambahkan uikartu agar jika mau kembali edit angkut tidak seluruh transaksiAngkut.angkut() yang muncul
            db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
            repository = db.getRepository(DataNitrit.class);
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", transaksiAngkutNFC.getIdTAngkut()));
            if(cursor.size() > 0) {
                for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                    DataNitrit dataNitrit = (DataNitrit) iterator.next();
                    transaksiAngkut1 = gson.fromJson(dataNitrit.getValueDataNitrit(),TransaksiAngkut.class);
                    if(transaksiAngkut1.getAngkuts().size() > 0){
                        ArrayList<Angkut> angkuts = new ArrayList<>();
                        for (Angkut angkut : transaksiAngkut1.getAngkuts()){
                            if(dataAngkutSelected.get(angkut.getOph()) != null){
                                angkut.setHasBeenAngkut(true);
                            }
                            angkuts.add(angkut);
                        }
                        transaksiAngkut1.setAngkuts(angkuts);

                    }
                    break;
                }
            }

            if(transaksiAngkut1 != null){
                DataNitrit dataNitrit = new DataNitrit(transaksiAngkut1.getIdTAngkut(), gson.toJson(transaksiAngkut1), ""+transaksiAngkut1.isManual());
                repository.update(dataNitrit);
            }
            db.close();
        }
    }

    public void showDialogCloseAngkut(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.metode_penyimpanan_transaksi,null);
        TextView tv_header = view.findViewById(R.id.tv_header);
        LinearLayout lnfc = view.findViewById(R.id.lnfc);
        LinearLayout lprint = view.findViewById(R.id.lprint);
        LinearLayout lshare = view.findViewById(R.id.lshare);
        Button btnClose = view.findViewById(R.id.btnClose);

        tv_header.setText(getResources().getString(R.string.chose_metode_angkut));
        btnClose.setVisibility(View.GONE);

        if(baseActivity.currentlocation == null){
            WidgetHelper.warningFindGps(angkutActivity,angkutActivity.myCoordinatorLayout);
            return;
        }
        latitude = baseActivity.currentlocation.getLatitude();
        longitude = baseActivity.currentlocation.getLongitude();

        lnfc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NfcHelper.showNFCTap(getActivity(),LongOperation_Save_T2_NFC);
                alertDialog.dismiss();
            }
        });

        lprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (baseActivity.bluetoothHelper.mService.getState() == BluetoothService.STATE_CONNECTED) {
                    new LongOperation().execute(String.valueOf(LongOperation_Save_T2_Print));
                }else{
                    baseActivity.bluetoothHelper.showListBluetoothPrinter();
                }
                alertDialog.dismiss();
            }
        });

        lshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new LongOperation().execute(String.valueOf(LongOperation_Save_T2_Share));
                alertDialog.dismiss();
            }
        });

        alertDialog = WidgetHelper.showListReference(alertDialog,view,getActivity());
    }

    public void showYesNo(String text,int stat){
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.yes_no, null);
        TextView tvDialogKet = (TextView) dialogView.findViewById(R.id.ket1);
        SlideView btnCancelDialog = (SlideView) dialogView.findViewById(R.id.btnCancel);
        SlideView btnOkDialog = (SlideView) dialogView.findViewById(R.id.btnOK);

        final AlertDialog dialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle)
                .setView(dialogView)
                .setCancelable(false)
                .create();

        final ForegroundColorSpan fcs = new ForegroundColorSpan(getActivity().getResources().getColor(R.color.Red));
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

        tvDialogKet.setText(text);
        btnOkDialog.setText("Yes");
        btnCancelDialog.setText("No");

        btnCancelDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                dialog.dismiss();
            }
        });
        btnOkDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                switch (stat){
                    case YesNo_Save:{
                        if(dataAngkut.size() != dataAngkutSelected.size()){
                            showDialogCloseAngkut();
                        }else {
                            new LongOperation().execute(String.valueOf(LongOperation_Save));
                        }
                        break;
                    }
                    case YesNo_Deleted:{
                        new LongOperation().execute(String.valueOf(LongOperation_Deleted));
                        break;
                    }
                    case YesNo_Cancel:{
                        angkutActivity.backProses();
                        break;
                    }
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public class LongOperation extends AsyncTask<String, String, String> {
        AlertDialog alertDialog;
        Snackbar snackbar;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialog = WidgetHelper.showWaitingDialog(getActivity(),getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])){
                case LongOperation_SetupTableAngkut: {
                    setupUpdateDataAngkut();
                    break;
                }
                case LongOperation_Save:{
                    saveAngkut(this);
                    break;
                }
                case LongOperation_Save_T2_NFC:{
                    saveAngkut(this);
                    break;
                }
                case LongOperation_Save_T2_Print:{
                    saveAngkut(this);
                    angkutView = new HashMap<>();
                    angkutView = transaksiAngkutHelper.setDataView(trnaskasiAngkutNFCSave.getAngkuts());
                    break;
                }
                case LongOperation_Save_T2_Share:{
                    saveAngkut(this);
                    angkutView = new HashMap<>();
                    angkutView = transaksiAngkutHelper.setDataView(trnaskasiAngkutNFCSave.getAngkuts());
                    break;
                }
                case LongOperation_Deleted:{
                    deletedAngkut();
                    break;
                }
            }
            return strings[0];
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            angkutActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject objProgres = new JSONObject(String.valueOf(values[0]));

                        if(snackbar== null) {
                            if (angkutActivity != null) {
                                snackbar = Snackbar.make(angkutActivity.myCoordinatorLayout, objProgres.getString("ket"), Snackbar.LENGTH_INDEFINITE);
                            }
                        }
                        snackbar.setText(String.format(objProgres.getString("ket") + " %d %s [%d/%d] ",
                                objProgres.getInt("persen"), "%", objProgres.getInt("count"), objProgres.getInt("total")));
                        snackbar.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            switch (Integer.parseInt(result)) {
                case LongOperation_SetupTableAngkut: {
                    setupUpdateTableAngkut();
                    break;
                }
                case LongOperation_Save:{
                    if(snackbar != null) {
                        snackbar.dismiss();
                    }
                    angkutActivity.backProses();
                    break;
                }
                case LongOperation_Save_T2_NFC:{
                    if(snackbar != null) {
                        snackbar.dismiss();
                    }
                    transaksiAngkutHelper.tapNFC(trnaskasiAngkutNFCSave);
                    break;
                }
                case LongOperation_Save_T2_Print:{
                    if(snackbar != null) {
                        snackbar.dismiss();
                    }
                    transaksiAngkutHelper.printTransaksiAngkut(trnaskasiAngkutNFCSave,new ArrayList<>(angkutView.values()));
                    angkutActivity.backProses();
                    break;
                }
                case LongOperation_Save_T2_Share:{
                    if(snackbar != null) {
                        snackbar.dismiss();
                    }
                    transaksiAngkutHelper.showQr(trnaskasiAngkutNFCSave,angkutActivity.qrHelper,new ArrayList<>(angkutView.values()));
                    break;
                }
                case LongOperation_Deleted:{
                    angkutActivity.backProses();
                    break;
                }
            }
            alertDialog.dismiss();
        }

        public void publishProgress(JSONObject objProgres){
            onProgressUpdate(String.valueOf(objProgres));
        }
    }
}
