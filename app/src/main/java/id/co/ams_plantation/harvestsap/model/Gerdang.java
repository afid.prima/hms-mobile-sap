package id.co.ams_plantation.harvestsap.model;

/**
 * Created on : 06,December,2022
 * Author     : Afid
 */

public class Gerdang {

    public static int Gerdang_NotUplaod = 1;
    public static int Gerdang_Upload = 2;

    String idGerdang;
    String estCode;
    String date;
    Pemanen pemanenUtama;
    Pemanen pemanenGerdang;
    int status;
    long createDate;
    String createBy;

    public Gerdang(String idGerdang, String estCode, String date, Pemanen pemanenUtama, Pemanen pemanenGerdang, int status, long createDate, String createBy) {
        this.idGerdang = idGerdang;
        this.estCode = estCode;
        this.date = date;
        this.pemanenUtama = pemanenUtama;
        this.pemanenGerdang = pemanenGerdang;
        this.status = status;
        this.createDate = createDate;
        this.createBy = createBy;
    }

    public String getIdGerdang() {
        return idGerdang;
    }

    public void setIdGerdang(String idGerdang) {
        this.idGerdang = idGerdang;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Pemanen getPemanenUtama() {
        return pemanenUtama;
    }

    public void setPemanenUtama(Pemanen pemanenUtama) {
        this.pemanenUtama = pemanenUtama;
    }

    public Pemanen getPemanenGerdang() {
        return pemanenGerdang;
    }

    public void setPemanenGerdang(Pemanen pemanenGerdang) {
        this.pemanenGerdang = pemanenGerdang;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
}
