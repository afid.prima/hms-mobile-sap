package id.co.ams_plantation.harvestsap.ui;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.OpnameNFC;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.util.ActivityLoggerHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.NfcHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiPanenHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;

public class FormatNfcActivity extends BaseActivity {
    SegmentedButtonGroup segmentedButtonGroup;
    BetterSpinner bsAfdeling;
    ImageView iv_nfc_panen;
    ImageView iv_nfc_angkut;
    String format;

    boolean bisaDaftar;
    AlertDialog alertDialog;
    String idNfc;

    OpnameNFC opnameNFC;

    /****************************************** NFC ******************************************************************/
    Tag myTag;
    NfcAdapter nfcAdapter;
    PendingIntent pendingIntent;
    IntentFilter[] writeTagFilters;
    @Override
    protected void initPresenter() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.formatnfc_layout);
        getSupportActionBar().hide();
        toolBarSetup();
        startTimeScreen = new Date();

//        segmentedButtonGroup = (SegmentedButtonGroup) findViewById(R.id.segmentedButtonGroup);
        bsAfdeling = (BetterSpinner) findViewById(R.id.bsAfdeling);
        iv_nfc_panen = (ImageView) findViewById(R.id.iv_nfc_panen);
        iv_nfc_angkut = (ImageView) findViewById(R.id.iv_nfc_angkut);

        iv_nfc_angkut.setVisibility(View.GONE);
        format = GlobalHelper.FORMAT_NFC_PANEN;

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
        }
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[]{tagDetected};
        bisaDaftar = true;

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.AFDELING_BLOCK, Context.MODE_PRIVATE);
        String afdelingObj = preferences.getString(HarvestApp.AFDELING_BLOCK,null);
        List<String> lAfd = new ArrayList<>();
        lAfd.add("Pilih Afdeling");
        try {
            JSONObject objAfdeling = new JSONObject(afdelingObj);
            if(objAfdeling.names() != null) {
                for (int i = 0; i < objAfdeling.names().length(); i++) {
                    lAfd.add(objAfdeling.names().get(i).toString());
                }
            }else{
                lAfd.add("AFD-1");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, lAfd);
        bsAfdeling.setAdapter(adapter);
        bsAfdeling.setText(lAfd.get(0));

//        segmentedButtonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
//            @Override
//            public void onClickedButton(int position) {
//                switch (position){
//                    case 0:
//                        format = GlobalHelper.FORMAT_NFC_PANEN;
//                        iv_nfc_panen.setVisibility(View.VISIBLE);
//                        iv_nfc_angkut.setVisibility(View.GONE);
//                        break;
//                    case 1:
//                        format = GlobalHelper.FORMAT_NFC_ANGKUT;
//                        iv_nfc_panen.setVisibility(View.GONE);
//                        iv_nfc_angkut.setVisibility(View.VISIBLE);
//                        break;
//                }
//            }
//        });
    }


    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            Log.i("tagNFC", GlobalHelper.bytesToHexString(myTag.getId()));
            Log.i("tagNFC", Arrays.toString(myTag.getTechList()));
        }

        if(bsAfdeling.getText().toString().toLowerCase().equals("pilih afdeling")){
            Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Afdeling Dahulu", Toast.LENGTH_SHORT).show();
            return;
        }

        String valueNFC = NfcHelper.readFromIntent(intent);
        String idX = GlobalHelper.bytesToHexString(myTag.getId());

        opnameNFC = new OpnameNFC(
                idX,
                GlobalHelper.getEstate().getEstCode(),
                bsAfdeling.getText().toString(),
                GlobalHelper.getUser().getUserID(),
                System.currentTimeMillis(),
                GlobalHelper.getUser().getUserID(),
                System.currentTimeMillis(),
                "Panen",
                OpnameNFC.SAVE
        );

        if(!bisaDaftar){
//            String idX = GlobalHelper.bytesToHexString(myTag.getId());
            if(!idNfc.equals(idX)){
                return;
            }else{
                bisaDaftar = true;
                alertDialog.dismiss();
            }
        }
        if (valueNFC.length() > 5) {
            new LongOperation().execute(valueNFC);
        }else{
            new LongOperation().execute("");
        }
        if (NfcHelper.writeNewFormat(format, myTag)) {
            Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_write_success), Toast.LENGTH_SHORT).show();
        } else {
            bisaDaftar = false;
            showErrorNFC();
            Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_write_error), Toast.LENGTH_LONG).show();
        }
    }

    private void showErrorNFC(){
        View view = LayoutInflater.from(this).inflate(R.layout.warning_layout,null);
        TextView keterangan = view.findViewById(R.id.keterangan);
        Button btnClose = view.findViewById(R.id.btnClose);
        idNfc = GlobalHelper.bytesToHexString(myTag.getId());

        keterangan.setText("Gagal Format NFC Id "+ idNfc + " Mohon Tap Ulang NFC Id "+ idNfc );
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bisaDaftar = true;
                alertDialog.dismiss();
            }
        });

        alertDialog = WidgetHelper.showListReference(alertDialog,view,this);
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        TextView textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backProses();
            }
        });
        textToolbar.setText(getResources().getString(R.string.format_nfc));
    }


    public void backProses() {
        setResult(GlobalHelper.RESULT_FORMATNFCACTIVITY);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause(){
        super.onPause();
        NfcHelper.WriteModeOff(nfcAdapter,this);
        endTimeScreen = new Date();
        if(GlobalHelper.getUser()!=null){
            ActivityLoggerHelper.recordScreenTime(FormatNfcActivity.class.getSimpleName(),startTimeScreen, endTimeScreen);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        startTimeScreen = new Date();
        NfcHelper.WriteModeOn(nfcAdapter,this,pendingIntent,writeTagFilters);
    }

    @Override
    public void onBackPressed() {
        backProses();
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        private AlertDialog alertDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialog = WidgetHelper.showWaitingDialog(FormatNfcActivity.this, getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... params) {
            String valueNFC = params[0];

            int formatNFC = GlobalHelper.TYPE_NFC_KOSONG;
            if(!valueNFC.isEmpty()){
                NfcHelper.cekFormatNFC(valueNFC);
            }

            TransaksiPanen transaksiPanen;
            TransaksiAngkut transaksiAngkut;

            try {
                objSeledted = new JSONObject();
                Log.d("formatNFC", String.valueOf(formatNFC));
                switch (formatNFC){
                    case GlobalHelper.TYPE_NFC_KOSONG:
                        NfcHelper.insertOpnameNFC(opnameNFC);
                        break;
                    case GlobalHelper.TYPE_NFC_BIRU:
                        if (valueNFC.length() > 5) {
                            objSeledted = new JSONObject(valueNFC);
                            objSeledted.put("y",idNfc);
                            transaksiPanen = TransaksiPanenHelper.toDecompre(objSeledted);
                            if(transaksiPanen != null) {
                                Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_TPH_NFC);
                                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                                DataNitrit dataNitrit = new DataNitrit(objSeledted.getString("a"), objSeledted.toString());
                                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", dataNitrit.getIdDataNitrit()));
                                if (cursor.size() > 0) {
                                    repository.update(dataNitrit);
                                } else {
                                    repository.insert(dataNitrit);
                                }
                                db.close();
                            }
                        }
                        NfcHelper.insertOpnameNFC(opnameNFC);
                        break;
                    case GlobalHelper.TYPE_NFC_HIJAU:
                        if (valueNFC.length() > 5) {
                            objSeledted = new JSONObject(valueNFC);
                            objSeledted.put("y",idNfc);
                            transaksiAngkut = TransaksiAngkutHelper.toDecompre(objSeledted);
                            if(transaksiAngkut != null) {
                                Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT_NFC);
                                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                                DataNitrit dataNitrit = new DataNitrit(objSeledted.getString("a"), objSeledted.toString());
                                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", dataNitrit.getIdDataNitrit()));
                                if (cursor.size() > 0) {
                                    repository.update(dataNitrit);
                                } else {
                                    repository.insert(dataNitrit);
                                }
                                db.close();
                            }
                        }
                        NfcHelper.insertOpnameNFC(opnameNFC);
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
            return String.valueOf(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            alertDialog.dismiss();
        }
    }
}
