package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import id.co.ams_plantation.harvestsap.Fragment.AngkutInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.TranskasiAngkutVersion;
import id.co.ams_plantation.harvestsap.ui.AngkutActivity;

public class SelectedTransaksiAngkutVersion extends RecyclerView.Adapter<SelectedTransaksiAngkutVersion.Holder> {
    Context contextApdater;
    ArrayList<TranskasiAngkutVersion> filter;
    ArrayList<TranskasiAngkutVersion> origin;
    AngkutActivity angkutActivity;
    AngkutInputFragment angkutInputFragment;
    TphInputFragment tphInputFragment;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm");

    public SelectedTransaksiAngkutVersion(Context contextApdater,ArrayList<TranskasiAngkutVersion> transkasiAngkutVersions) {
        this.contextApdater = contextApdater;
        if(contextApdater instanceof AngkutActivity) {
            angkutActivity = ((AngkutActivity) contextApdater);
            if (angkutActivity.getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof AngkutInputFragment) {
                angkutInputFragment = (AngkutInputFragment) angkutActivity.getSupportFragmentManager().findFragmentById(R.id.content_container);
            }else if (angkutActivity.getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof TphInputFragment) {
                tphInputFragment = (TphInputFragment) angkutActivity.getSupportFragmentManager().findFragmentById(R.id.content_container);

            }
        }
        this.origin = new ArrayList<>();
        this.origin.addAll(transkasiAngkutVersions);
        this.filter = new ArrayList<>();
        this.filter.addAll(transkasiAngkutVersions);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(contextApdater).inflate(R.layout.row_record,parent,false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.setValue(filter.get(position));
    }

    @Override
    public int getItemCount() {
        return filter!=null?filter.size():0;
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                filter = new ArrayList<>();
                if(constraint != null) {
                    filter.clear();
                    for (TranskasiAngkutVersion value : origin) {
                        if(String.valueOf(value.getVersion()).toLowerCase().contains(constraint.toString().toLowerCase())){
                            filter.add(value);
                        }else if (String.valueOf(value.getTransaksiAngkut().getTotalJanjang()).toLowerCase().contains(constraint.toString().toLowerCase())){
                            filter.add(value);
                        }else if (String.valueOf(value.getTransaksiAngkut().getTotalBeratEstimasi()).toLowerCase().contains(constraint.toString().toLowerCase())){
                            filter.add(value);
                        }else if (sdf.format(value.getTransaksiAngkut().getEndDate()).toLowerCase().contains(constraint.toString().toLowerCase())){
                            filter.add(value);
                        }
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = filter;
                    filterResults.count = filter.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null) {
                    filter = (ArrayList<TranskasiAngkutVersion>) results.values;
                } else {
                    filter.clear();
                }
                notifyDataSetChanged();
            }
        };
    }

    public class Holder extends RecyclerView.ViewHolder {
        RelativeLayout rlImage;
        TextView item_ket;
        TextView item_ket1;
        TextView item_ket2;
        TextView item_ket3;
        CardView card_view;
        public Holder(View itemView) {
            super(itemView);
            card_view = (CardView) itemView.findViewById(R.id.card_view);
            rlImage = (RelativeLayout) itemView.findViewById(R.id.rlImage);
            item_ket = (TextView) itemView.findViewById(R.id.item_ket);
            item_ket1 = (TextView) itemView.findViewById(R.id.item_ket1);
            item_ket2 = (TextView) itemView.findViewById(R.id.item_ket2);
            item_ket3 = (TextView) itemView.findViewById(R.id.item_ket3);
            rlImage.setVisibility(View.GONE);
        }

        public void setValue(TranskasiAngkutVersion value) {
            item_ket.setText(String.format("Version %d",value.getVersion()));
            item_ket2.setText(sdf.format(value.getTransaksiAngkut().getEndDate()));

            item_ket1.setText(String.format("%d Janjang",value.getTransaksiAngkut().getTotalJanjang()));
            item_ket3.setText(String.format("Berat Estimasi %d Kg",(int) value.getTransaksiAngkut().getTotalBeratEstimasi()));

            card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(angkutInputFragment != null){

                    }else if (tphInputFragment != null){
                        tphInputFragment.setUpTransaksiAngkutNFCBaseOnHistory(value.getTransaksiAngkut());
                    }
                }
            });
        }
    }
}
