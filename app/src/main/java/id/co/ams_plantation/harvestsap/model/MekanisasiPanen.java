package id.co.ams_plantation.harvestsap.model;

import java.util.ArrayList;

public class MekanisasiPanen {
    String idMekanisasi;
    Operator operator;
    Operator loader;
    Vehicle vehicle;
    String bin;
    String date;
    String createBy;
    String estCode;
    int totalJanjang;
    int totalBrondolan;
    double totalBerat;
    ArrayList<TransaksiPanen> transaksiPanens;

    public MekanisasiPanen() {
    }

    public MekanisasiPanen(String idMekanisasi, Operator operator, Operator loader, Vehicle vehicle, String bin, String date, String createBy, String estCode, int totalJanjang,int totalBrondolan, double totalBerat, ArrayList<TransaksiPanen> transaksiPanens) {
        this.idMekanisasi = idMekanisasi;
        this.operator = operator;
        this.loader = loader;
        this.vehicle = vehicle;
        this.bin = bin;
        this.date = date;
        this.createBy = createBy;
        this.estCode = estCode;
        this.totalJanjang = totalJanjang;
        this.totalBrondolan = totalBrondolan;
        this.totalBerat = totalBerat;
        this.transaksiPanens = transaksiPanens;
    }

    public String getIdMekanisasi() {
        return idMekanisasi;
    }

    public void setIdMekanisasi(String idMekanisasi) {
        this.idMekanisasi = idMekanisasi;
    }

    public Operator getLoader() {
        return loader;
    }

    public void setLoader(Operator loader) {
        this.loader = loader;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public int getTotalJanjang() {
        return totalJanjang;
    }

    public void setTotalJanjang(int totalJanjang) {
        this.totalJanjang = totalJanjang;
    }

    public int getTotalBrondolan() {
        return totalBrondolan;
    }

    public void setTotalBrondolan(int totalBrondolan) {
        this.totalBrondolan = totalBrondolan;
    }

    public double getTotalBerat() {
        return totalBerat;
    }

    public void setTotalBerat(double totalBerat) {
        this.totalBerat = totalBerat;
    }

    public ArrayList<TransaksiPanen> getTransaksiPanens() {
        return transaksiPanens;
    }

    public void setTransaksiPanens(ArrayList<TransaksiPanen> transaksiPanens) {
        this.transaksiPanens = transaksiPanens;
    }
}
