package id.co.ams_plantation.harvestsap.TableView.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;

/**
 * Created by user on 12/24/2018.
 */

public class AngkutCellViewHolder extends AbstractViewHolder {

    public final TextView cell_textview;
    public final LinearLayout cell_container;
    private TableViewCell cell;

    public AngkutCellViewHolder(View itemView) {
        super(itemView);
        cell_textview = (TextView) itemView.findViewById(R.id.cell_data);
        cell_container = (LinearLayout) itemView.findViewById(R.id.cell_container);
    }

    public void setCell(TableViewCell cell) {
        this.cell = cell;
        cell_textview.setText(String.valueOf(cell.getData()));
        // If your TableView should have auto resize for cells & columns.
        // Then you should consider the below lines. Otherwise, you can ignore them.

        // It is necessary to remeasure itself.
        cell_container.getLayoutParams().width = LinearLayout.LayoutParams.WRAP_CONTENT;
        cell_textview.requestLayout();
    }
}
