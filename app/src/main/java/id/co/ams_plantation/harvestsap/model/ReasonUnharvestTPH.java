package id.co.ams_plantation.harvestsap.model;

import java.util.ArrayList;

public class ReasonUnharvestTPH {
    private int actionReasonId;
    private String actionName;
    private String reasonName;
    private int conditionId;
    private String conditionName;
    private String notification;

    public int getActionReasonId() {
        return actionReasonId;
    }

    public void setActionReasonId(int actionReasonId) {
        this.actionReasonId = actionReasonId;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    public int getConditionId() {
        return conditionId;
    }

    public void setConditionId(int conditionId) {
        this.conditionId = conditionId;
    }

    public String getConditionName() {
        return conditionName;
    }

    public void setConditionName(String conditionName) {
        this.conditionName = conditionName;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }
}
