package id.co.ams_plantation.harvestsap.model;

import java.util.HashSet;

/**
 * Created by user on 1/10/2019.
 */

public class Langsiran {

    public static final String[] LIST_TIPE_LANGSIRAN = {"FFB RAMP","BIN","ESTAFET "};


    public static int STATUS_LANGSIRAN_BELUM_UPLOAD = 0;
    public static int STATUS_LANGSIRAN_MASTER = 1;
    public static int STATUS_LANGSIRAN_DELETED = 2;
    public static int STATUS_LANGSIRAN_INACTIVE_QC_MOBILE = 3;
    public static int STATUS_LANGSIRAN_UPLOAD = 4;

    public static final int TIPE_LANGSIRAN_FFB_RAMP = 0;
    public static final int TIPE_LANGSIRAN_BIN = 1;
    public static final int TIPE_LANGSIRAN_ESTAFET = 2;

    String idTujuan;
    String namaTujuan;
    int typeTujuan;
    String afdeling;
    String estCode;
    String block;
    double latitude;
    double longitude;
    long createDate;
    String createBy;
    long updateDate;
    String updateBy;
    int status;
    HashSet<String> foto;

    public Langsiran(){}

    public Langsiran(String idTujuan, String namaTujuan, int typeTujuan,String estCode,String afdeling,  String block, double latitude, double longitude, long createDate, String createBy, long updateDate, String updateBy, int status, HashSet<String> foto) {
        this.idTujuan = idTujuan;
        this.namaTujuan = namaTujuan;
        this.typeTujuan = typeTujuan;
        this.afdeling = afdeling;
        this.estCode = estCode;
        this.block = block;
        this.latitude = latitude;
        this.longitude = longitude;
        this.createDate = createDate;
        this.createBy = createBy;
        this.updateDate = updateDate;
        this.updateBy = updateBy;
        this.status = status;
        this.foto = foto;
    }

    public String getIdTujuan() {
        return idTujuan;
    }

    public void setIdTujuan(String idTujuan) {
        this.idTujuan = idTujuan;
    }

    public String getNamaTujuan() {
        return namaTujuan;
    }

    public void setNamaTujuan(String namaTujuan) {
        this.namaTujuan = namaTujuan;
    }

    public int getTypeTujuan() {
        return typeTujuan;
    }

    public void setTypeTujuan(int typeTujuan) {
        this.typeTujuan = typeTujuan;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }

    public HashSet<String> getFoto() {
        return foto;
    }

    public void setFoto(HashSet<String> foto) {
        this.foto = foto;
    }
}
