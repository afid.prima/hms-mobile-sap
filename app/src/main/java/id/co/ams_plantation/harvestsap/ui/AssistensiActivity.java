package id.co.ams_plantation.harvestsap.ui;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import butterknife.BindView;
import id.co.ams_plantation.harvestsap.Fragment.AssistensiInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.AssistensiListFragment;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestsap.model.AssistensiRequest;
import id.co.ams_plantation.harvestsap.presenter.AssistensiPresenter;
import id.co.ams_plantation.harvestsap.presenter.ConnectionPresenter;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.view.AssistensiView;

public class AssistensiActivity extends BaseActivity implements AssistensiView {
    public AssistensiPresenter presenter;
    public ConnectionPresenter connectionPresenter;

    @BindView(R.id.myCoordinatorLayout)
    public CoordinatorLayout myCoordinatorLayout;
    @BindView(R.id.toolbar_menu)
    public ImageView imgMenu;

    public AssistensiRequest selectedAssistensiRequest;

    @Override
    protected void initPresenter() {
        presenter = new AssistensiPresenter(this);
        connectionPresenter = new ConnectionPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_layout);
        getSupportActionBar().hide();

        toolBarSetup();
        main();
    }

    private void main() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_container, AssistensiListFragment.getInstance())
                .commit();
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = findViewById(R.id.toolbar_icon);
        imgMenu = findViewById(R.id.toolbar_menu);
        TextView textToolbar = findViewById(R.id.toolbar_text);

        textToolbar.setText(getResources().getString(R.string.assistensi));

        imgBack.setVisibility(View.VISIBLE);
        imgMenu.setVisibility(View.VISIBLE);

        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));


        imgMenu.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_plus)
                .colorRes(R.color.White)
                .sizeDp(24));

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backProses();
            }
        });

        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_container, AssistensiInputFragment.getInstance())
//                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    public void backProses() {
        if (getSupportFragmentManager().findFragmentById(R.id.content_container)
                instanceof AssistensiInputFragment) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_container, AssistensiListFragment.getInstance())
                    .commit();
        } else {
            setResult(GlobalHelper.RESULT_ASSISTENSI);
            finish();
        }
    }

    @Override
    public void successResponse(ServiceResponse serviceResponse) {
        try {
            switch (serviceResponse.getTag()){
                case InsertAssistanceRequest:
                    if (getSupportFragmentManager().findFragmentById(R.id.content_container)
                            instanceof AssistensiListFragment) {
                        AssistensiListFragment fragment = (AssistensiListFragment) this.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        fragment.onSuccesRespon(serviceResponse);
                    }
                    break;
                case GetLatestRequestAssistensiByRequestUserId:
                    if (getSupportFragmentManager().findFragmentById(R.id.content_container)
                            instanceof AssistensiListFragment) {
                        AssistensiListFragment fragment = (AssistensiListFragment) this.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        fragment.onSuccesRespon(serviceResponse);
                    }
                    break;
                case GetEstateAndAssistanceList:
                    if (getSupportFragmentManager().findFragmentById(R.id.content_container)
                            instanceof AssistensiInputFragment) {
                        AssistensiInputFragment fragment = (AssistensiInputFragment) this.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        fragment.new LongOperation(serviceResponse).execute(String.valueOf(fragment.LongOperation_UpdateTableEstateAndAssistensi));
                    }
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void badResponse(ServiceResponse serviceResponse) {
        try {
            switch (serviceResponse.getTag()){
                case InsertAssistanceRequest:
                    if (getSupportFragmentManager().findFragmentById(R.id.content_container)
                            instanceof AssistensiListFragment) {
                        AssistensiListFragment fragment = (AssistensiListFragment) this.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        fragment.onBadRespon();
                    }
                    break;
                case GetLatestRequestAssistensiByRequestUserId:
                    if (getSupportFragmentManager().findFragmentById(R.id.content_container)
                            instanceof AssistensiListFragment) {
                        AssistensiListFragment fragment = (AssistensiListFragment) this.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        fragment.onBadRespon();
                    }
                    break;
                case GetEstateAndAssistanceList:
                    if (getSupportFragmentManager().findFragmentById(R.id.content_container)
                            instanceof AssistensiInputFragment) {
                        AssistensiInputFragment fragment = (AssistensiInputFragment) this.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        fragment.onBadRespon();
                    }
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
