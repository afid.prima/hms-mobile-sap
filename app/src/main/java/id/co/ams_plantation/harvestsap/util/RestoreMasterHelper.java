package id.co.ams_plantation.harvestsap.util;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import id.co.ams_plantation.harvestsap.BuildConfig;
import id.co.ams_plantation.harvestsap.Fragment.SettingFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestsap.ui.MapActivity;
//import ir.mahdi.mzip.zip.ZipArchive;
import jp.wasabeef.recyclerview.adapters.SlideInLeftAnimationAdapter;

/**
 * Created on : 12,March,2021
 * Author     : Afid
 */

public class RestoreMasterHelper {

    Context context;
    SettingFragment settingFragment;
    android.support.v7.app.AlertDialog listrefrence;

    ArrayList<String> filter;
    ArrayList<String> origin;
    String namaFileSelected = null;

    public RestoreMasterHelper(Context context) {
        this.context = context;
    }


    public void showRestoreMaster(){
        if (context instanceof MainMenuActivity){
            Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
            if(fragment instanceof SettingFragment) {
                settingFragment = ((SettingFragment) fragment);
            }
        }

        String [] dirListing ={
                GlobalHelper.TABEL_TPH + ";Master Data Titik TPH Untuk Pembuatan Panen",
                GlobalHelper.TABLE_LANGSIRAN + ";Master Data Titik Langsiran Untuk Pembuatan SPB",
                GlobalHelper.TABEL_PEMANEN + ";Master Data Pemanen Untuk Pembuatan Panen",
                GlobalHelper.TABEL_OPERATOR + ";Master Data Operator Dan Loader Untuk Pembuatan SPB",
                GlobalHelper.TABLE_VEHICLE + ";Master Data Vehicle Untuk Pembuatan SPB"
        };
        filter = new ArrayList<String>(Arrays.asList(dirListing));
        origin = new ArrayList<String>(Arrays.asList(dirListing));
        namaFileSelected = null;

        View view = LayoutInflater.from(context).inflate(R.layout.list_refrensi_object,null);
        TextView idHeader = (TextView) view.findViewById(R.id.idHeader);
        EditText etSearch_lso = (EditText) view.findViewById(R.id.et_search_lso);
        RecyclerView rv_usage_lso = (RecyclerView) view.findViewById(R.id.rv_usage_lso);

        idHeader.setText(context.getResources().getString(R.string.chose_restore));
        rv_usage_lso.setLayoutManager(new LinearLayoutManager(context));

        final SelectedRestoreMasterAdapter adapter = new SelectedRestoreMasterAdapter(context);
        final SlideInLeftAnimationAdapter adapterLeft = new SlideInLeftAnimationAdapter(adapter);
        adapterLeft.setFirstOnly(false);
//       adapterLeft.setInterpolator(new OvershootInterpolator(1f));
        adapterLeft.setDuration(400);
        etSearch_lso.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s.toString());
            }
        });
        rv_usage_lso.setAdapter(adapterLeft);

        listrefrence = WidgetHelper.showListReference(listrefrence,view,context);
    }

    class SelectedRestoreMasterAdapter extends RecyclerView.Adapter<SelectedRestoreMasterAdapter.Holder> {
        Context contextApdater;

        public SelectedRestoreMasterAdapter(Context contextApdater) {
            this.contextApdater = contextApdater;
        }

        @NonNull
        @Override
        public SelectedRestoreMasterAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(contextApdater).inflate(R.layout.row_infowindow_chose,parent,false);
            return new SelectedRestoreMasterAdapter.Holder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull SelectedRestoreMasterAdapter.Holder holder, int position) {
            holder.setValue(filter.get(position));
        }

        @Override
        public int getItemCount() {
            return filter!=null?filter.size():0;
        }

        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults results = new FilterResults();
                    filter = new ArrayList<>();
                    String string = constraint.toString().toLowerCase();
                    int i = 0 ;
                    for (String s : origin) {

                        String [] split = s.split(";");

                        if(split[0].toLowerCase().contains(string)){
                            filter.add(s);
                        }
                    }
                    if (filter.size() > 0) {
                        results.count = filter.size();
                        results.values = filter;
                        return results;
                    } else {
                        return null;
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null) {
                        filter = (ArrayList<String>) results.values;
                    } else {
                        filter.clear();
                    }
                    notifyDataSetChanged();

                }
            };
        }

        public class Holder extends RecyclerView.ViewHolder {
            TextView tv_riw_chose;
            TextView tv_riw_chose1;
            CardView cv_riw_chose;
            ImageView iv_status;
            public Holder(View itemView) {
                super(itemView);
                cv_riw_chose = (CardView) itemView.findViewById(R.id.cv_riw_chose);
                tv_riw_chose = (TextView) itemView.findViewById(R.id.tv_riw_chose);
                tv_riw_chose1 = (TextView) itemView.findViewById(R.id.tv_riw_chose1);
                iv_status = (ImageView) itemView.findViewById(R.id.iv_status);
                iv_status.setVisibility(View.GONE);
            }

            public void setValue(String namaFile) {
                String [] namaTbl = namaFile.split(";");
                tv_riw_chose.setText(namaTbl[0]);
                tv_riw_chose1.setText(namaTbl[1]);
                cv_riw_chose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        namaFileSelected = namaTbl[0];
                        if (settingFragment != null){
                            settingFragment.startLongOperation(SettingFragment.LongOperation_RestoreMaster);
                        }
                    }
                });
            }
        }
    }

    public boolean choseRestoreMaster(){

        String tableName = namaFileSelected;
        File dbTemp = new File(GlobalHelper.getDatabasePathHMSTempMaster() + "/" +Encrypts.encrypt(tableName) + "/" +Encrypts.encrypt(tableName)+ ".db");
        File fileDB = new File(GlobalHelper.getDatabasePathHMS() + "/" +Encrypts.encrypt(tableName) + "/" +Encrypts.encrypt(tableName)+ ".db");

        if(!dbTemp.exists()){
            listrefrence.dismiss();
            return false;
        }

        if(!fileDB.exists()){
            fileDB.delete();
        }

        try {
            GlobalHelper.copyFile(dbTemp, new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(tableName)), Encrypts.encrypt(tableName) + ".db");
        }catch (Exception e) {
            e.printStackTrace();
        }

        if(new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(tableName)+ "/" +Encrypts.encrypt(tableName)+ ".db").exists()) {
            listrefrence.dismiss();
            return true;
        }else {
            listrefrence.dismiss();
            return false;
        }
    }

    public void closeRestoreMaster(){
        String tableName = namaFileSelected;
        File dbTemp = new File(GlobalHelper.getDatabasePathHMSTempMaster() + "/" +Encrypts.encrypt(tableName) + "/" +Encrypts.encrypt(tableName)+ ".db");
        File fileDB = new File(GlobalHelper.getDatabasePathHMS() + "/" +Encrypts.encrypt(tableName) + "/" +Encrypts.encrypt(tableName)+ ".db");

        if(!dbTemp.exists()){
            Toast.makeText(HarvestApp.getContext(),"Tidak Ada Data Restore TABEL "+ tableName,Toast.LENGTH_SHORT).show();
        }else if(!fileDB.exists()){
            String folderHMS = BuildConfig.BUILD_VARIANT.equals("dev") ? "HMSSAPTESTING" : "HMSSAP";
            Toast.makeText(HarvestApp.getContext(),"Mohon Kirimkan Folder  "+ folderHMS + " Ke Implementor Lalu Hapus Folder Tersebut",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(HarvestApp.getContext(),"Restore Data Master Table "+ tableName + " Berhasil",Toast.LENGTH_SHORT).show();
        }
    }

    public static void backupTempMasterSAP(String tableName){
        try {
            File fileDB = new File(GlobalHelper.getDatabasePathHMS() + "/" +Encrypts.encrypt(tableName) + "/" +Encrypts.encrypt(tableName)+ ".db");
            if(fileDB.exists()){
                File dirTempMaster = new File(GlobalHelper.getDatabasePathHMSTempMaster());
                if(!dirTempMaster.exists()){
                    dirTempMaster.mkdir();
                }
                File dbTemp = new File(GlobalHelper.getDatabasePathHMSTempMaster() + "/" +Encrypts.encrypt(tableName) + "/" +Encrypts.encrypt(tableName)+ ".db");
                if(dbTemp.exists()){
                    dbTemp.delete();
                }

                GlobalHelper.copyFile(fileDB,new File(GlobalHelper.getDatabasePathHMSTempMaster() + "/" +Encrypts.encrypt(tableName)),Encrypts.encrypt(tableName)+ ".db");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
