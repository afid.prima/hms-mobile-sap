package id.co.ams_plantation.harvestsap.model;

import java.util.Date;

public class UserAppUsage {

    public static final String HIT = "hit";
    public static final String SCREEN_TIME = "screen time";
    public static final String REQUEST_DATA = "request data";

    private int userId;
    private String moduleCode;
    private String appId;
    private String activityType;
    private String platform;
    private String pageId;
    private String elementId;
    private String connectionInfo;
    private String ipAddress;
    private String startTime;
    private String endTime;
    private String remarks;
    private Date createDate;
    private String androidId;
    private String versionApp;
    private String idAndroid;

    public UserAppUsage() {
    }

    public UserAppUsage(int userId, String moduleCode, String appId, String activityType, String platform, String pageId, String elementId, String connectionInfo, String ipAddress, String startTime, String endTime, String remarks, Date createDate, String androidId, String versionApp, String idAndroid) {
        this.userId = userId;
        this.moduleCode = moduleCode;
        this.appId = appId;
        this.activityType = activityType;
        this.platform = platform;
        this.pageId = pageId;
        this.elementId = elementId;
        this.connectionInfo = connectionInfo;
        this.ipAddress = ipAddress;
        this.startTime = startTime;
        this.endTime = endTime;
        this.remarks = remarks;
        this.createDate = createDate;
        this.androidId = androidId;
        this.versionApp = versionApp;
        this.idAndroid = idAndroid;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getElementId() {
        return elementId;
    }

    public void setElementId(String elementId) {
        this.elementId = elementId;
    }

    public String getConnectionInfo() {
        return connectionInfo;
    }

    public void setConnectionInfo(String connectionInfo) {
        this.connectionInfo = connectionInfo;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }

    public String getIdAndroid() {
        return idAndroid;
    }

    public void setIdAndroid(String idAndroid) {
        this.idAndroid = idAndroid;
    }
}
