package id.co.ams_plantation.harvestsap.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import butterknife.BindView;
import id.co.ams_plantation.harvestsap.Fragment.QcMutuAncakFragmet;
import id.co.ams_plantation.harvestsap.Fragment.QcMutuAncakInputEntryFragment;
import id.co.ams_plantation.harvestsap.Fragment.QcMutuAncakInputFragmet;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.Estate;
import id.co.ams_plantation.harvestsap.model.QcAncak;
import id.co.ams_plantation.harvestsap.model.QcAncakPohon;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.MutuAncakHelper;
import id.co.ams_plantation.harvestsap.util.TphHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;

public class QcMutuAncakActivity extends BaseActivity {

    public Estate estate;
    public QcAncak seletedQcAncak;
    public QcAncakPohon seletedQcAncakPohon;
    public MutuAncakHelper mutuAncakHelper;
    public TphHelper tphHelper;
    Activity activity;

    @BindView(R.id.myCoordinatorLayout)
    public CoordinatorLayout myCoordinatorLayout;

    public Snackbar searchingGPS;
    /****************************************** NFC ******************************************************************/
//    public Tag myTag;
//    public NfcAdapter nfcAdapter;
//    public PendingIntent pendingIntent;
//    public IntentFilter writeTagFilters[];
    /****************************************************************************************************************/
//    private static Handler handler;

    @Override
    protected void initPresenter() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_layout);
        getSupportActionBar().hide();

        mutuAncakHelper = new MutuAncakHelper(this);
        tphHelper = new TphHelper(this);
        Intent intent = getIntent();
        if(intent.getExtras() != null) {
            String transaksi = intent.getExtras().getString("qcAncak");
            if (transaksi != null) {

                Gson gson = new Gson();
                seletedQcAncak = gson.fromJson(transaksi, QcAncak.class);

                Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_ANCAK_POHON);
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                for(int i = 0 ; i < seletedQcAncak.getQcAncakPohons().size(); i++){
                    DataNitrit dataNitrit = new DataNitrit(seletedQcAncak.getQcAncakPohons().get(i).getIdPohon(),
                            gson.toJson(seletedQcAncak.getQcAncakPohons().get(i)));
                    repository.insert(dataNitrit);
                }
                db.close();
            }
        }

        activity = this;

        String strEst =  Encrypts.decrypt(GlobalHelper.getFileContent(Encrypts.encrypt(GlobalHelper.SELECTED_ESTATE)));
        Gson gson = new Gson();
        estate = (Estate) gson.fromJson(strEst, Estate.class);
////        set di baseActivity
//        createLocationListener();
        toolBarSetup();

//        dataAllUser = GlobalHelper.getAllUser();
        main();
    }

//    public static Handler getHandler() {
//        return handler;
//    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        switch (resultCode){
//            case GlobalHelper.RESULT_SCAN_QR:{
//                String isiQr = data.getExtras().getString(QRScan.NILAI_QR_SCAN);
//                cekDataPassing(isiQr,this);
//                break;
//            }
//        }
//    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void main() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_container, QcMutuAncakInputFragmet.getInstance())
                .commit();
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        TextView textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backProses();
            }
        });
        textToolbar.setText(getResources().getString(R.string.qc_mutu_ancak));
    }

    public void backProses() {
        if (searchingGPS != null) {
            searchingGPS.dismiss();
        }
        if (getSupportFragmentManager().findFragmentById(R.id.content_container)
                instanceof QcMutuAncakInputEntryFragment) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_container, QcMutuAncakInputFragmet.getInstance())
                    .commit();
        } else {
            closeActivity();
        }
    }

    public void keawalFragment(){
        setResult(GlobalHelper.RESULT_QC_MUTU_ANCAK);
        finish();
    }

    private void createLocationListener() {
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

//        searchingGPS = Snackbar.make(myCoordinatorLayout,getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
//        searchingGPS.show();
        WidgetHelper.warningFindGps(this,myCoordinatorLayout);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                1f, mLocationListener);
    }

    public void closeActivity(){
        mutuAncakHelper.hapusFileQcAncak(true);
        keawalFragment();
    }

//    @Override
//    protected void onNewIntent(Intent intent) {
//        setIntent(intent);
//        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
////            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
////            Log.i("tagNFC", GlobalHelper.bytesToHexString(myTag.getId()));
////            Log.i("tagNFC", Arrays.toString(myTag.getTechList()));
//        }
////        valueNFC = NfcHelper.readFromIntent(intent);
////        int formatNFC = NfcHelper.cekFormatNFC(valueNFC);
////        if (valueNFC != null) {
////            if (formatNFC != GlobalHelper.TYPE_NFC_SALAH) {
////                if(valueNFC.length() == 5) {
////                    if(NfcHelper.stat != NfcHelper.NFC_TIDAK_BISA_WRITE){
//////                        if (getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof QcSensusBjrInputFragment) {
//////                            QcSensusBjrInputFragment fragment = (QcSensusBjrInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
//////                            fragment.startLongOperation(NfcHelper.stat);
//////                        }
////                    }else{
////                        Toast.makeText(this, getResources().getString(R.string.nfc_empty) + " " + GlobalHelper.LIST_NFC[formatNFC], Toast.LENGTH_SHORT).show();
////                    }
////                }else{
////                    cekDataPassing(valueNFC, this);
////                }
////                TYPE_NFC = formatNFC;
////            } else {
////                Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.format_not_valid) + " 999", Toast.LENGTH_SHORT).show();
////            }
////        }else {
////            Toast.makeText(this,R.string.format_not_valid,Toast.LENGTH_SHORT).show();
////        }
//    }

    @Override
    public void onBackPressed() {
        backProses();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        NfcHelper.WriteModeOn(nfcAdapter,this,pendingIntent,writeTagFilters);
    }

    @Override
    public void onPause(){
        super.onPause();
//        NfcHelper.WriteModeOff(nfcAdapter,this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
