package id.co.ams_plantation.harvestsap.Fragment;

import static id.co.ams_plantation.harvestsap.Fragment.SettingFragment.ButtonGroup_Lokal;
import static id.co.ams_plantation.harvestsap.Fragment.SettingFragment.ButtonGroup_Public;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.roughike.swipeselector.OnSwipeItemSelectedListener;
import com.roughike.swipeselector.SwipeItem;
import com.roughike.swipeselector.SwipeSelector;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.RefreshHeaderRecycleView;
import id.co.ams_plantation.harvestsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.LinkReport;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.ReportActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.SetUpDataSyncHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;

public class ReportListFragment extends Fragment {

    ReportActivity reportActivity;
    SetUpDataSyncHelper setUpDataSyncHelper;

    RecyclerView rv;
    RefreshLayout refreshRv;
    CoordinatorLayout myCoordinatorLayout;
    SwipeSelector menuSwipeSelector;
    SegmentedButtonGroup segmentedButtonGroup;
    LinearLayout lnoData;
    CompleteTextViewHelper etSearch;
    TextView footerUser;
    TextView footerEstate;
    Date dateSelected;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
    SimpleDateFormat sdfDateParam = new SimpleDateFormat("yyyyMMdd");
    ServiceResponse serviceResponse;
    ArrayList<LinkReport> requestArrayList;
    ReportListAdapter adapter;

    static String FULLPATH_REPORT = Environment.getExternalStorageDirectory() +GlobalHelper.REPORT_DIR_FILES + "/";
    File folderReport;

    Set<String> listSearch;

    final int LongOperation_GetLinkReport = 0;
    final int LongOperation_SetUpDataDownload = 1;
    final int LongOperation_SetUpData = 2;
    final int LongOperation_DownlaodReport = 3;


    public static ReportListFragment getInstance(){
        ReportListFragment fragment = new ReportListFragment();
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.reportlist_layout, null, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        main();
    }

    public void initView(View view){
        menuSwipeSelector = view.findViewById(R.id.menuSwipeSelector);
        segmentedButtonGroup = view.findViewById(R.id.segmentedButtonGroup);
        footerUser = view.findViewById(R.id.footerUser);
        footerEstate = view.findViewById(R.id.footerEstate);
        lnoData = view.findViewById(R.id.lnoData);
        etSearch = view.findViewById(R.id.etSearch);
        rv = view.findViewById(R.id.rv);
        refreshRv = view.findViewById(R.id.refreshRv);
        myCoordinatorLayout = view.findViewById(R.id.myCoordinatorLayout);
    }

    public void main(){

        reportActivity = ((ReportActivity) getActivity());
        setUpDataSyncHelper = new SetUpDataSyncHelper(getActivity());
        requestArrayList = new ArrayList<>();

        folderReport = new File(FULLPATH_REPORT);
        folderReport.mkdir();

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
        if(preferences.getBoolean(HarvestApp.CONNECTION_PREF,false)){
            segmentedButtonGroup.setPosition(ButtonGroup_Public);
        }else{
            segmentedButtonGroup.setPosition(ButtonGroup_Lokal);
        }

        segmentedButtonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(int position) {
                switch (position){
                    case 0: {
                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF, false);
                        editor.apply();
                        break;
                    }
                    case 1: {
                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF, true);
                        editor.apply();
                        break;
                    }
                }
            }
        });

        dateSelected = new Date();
        etSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.getFilter().filter(etSearch.getText().toString());
            }
        });

        ArrayList<SwipeItem> swipeItems= new ArrayList<>();
        Long now = System.currentTimeMillis();
        for(int i = 0 ; i < GlobalHelper.MAX_LAST_DAY_DATA; i++){
            Long l = now - ( i * 24 * 60 * 60 * 1000);
            swipeItems.add(new SwipeItem(String.valueOf(i),sdf.format(l),""));
        }
        Collections.reverse(swipeItems);

        SwipeItem[] tmpStrSwipe = new SwipeItem[swipeItems.size()];
        tmpStrSwipe = swipeItems.toArray(tmpStrSwipe);
        menuSwipeSelector.setItems(tmpStrSwipe);
        menuSwipeSelector.selectItemAt(swipeItems.size() - 1);
        menuSwipeSelector.setOnItemSelectedListener(new OnSwipeItemSelectedListener() {
            @Override
            public void onItemSelected(SwipeItem item) {
                try {
                    Date date = sdf.parse(item.getTitle());
                    dateSelected = date;
                    new LongOperation().execute(String.valueOf(LongOperation_SetUpData));
                    Log.d("item Date ", date.getTime() + " "+ sdf.format(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        rv.setLayoutManager(new LinearLayoutManager(getActivity()));

        refreshRv.setEnableAutoLoadMore(true);
        refreshRv.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                refresh();
            }
        });
        refreshRv.setRefreshHeader(new RefreshHeaderRecycleView(getActivity()));
        refreshRv.setHeaderHeight(60);
        refreshRv.autoRefresh();

        User user = GlobalHelper.getUser();
        footerUser.setText(user.getUserFullName());
        footerEstate.setText(GlobalHelper.getEstate().getEstCode() + " - " +GlobalHelper.getEstate().getEstName());
    }

    private void refresh() {
        lnoData.setVisibility(View.GONE);
        refreshRv.getLayout().postDelayed(() -> {
            new LongOperation().execute(String.valueOf(LongOperation_GetLinkReport));
        }, 2000);
    }


    private void setupData(String dateParam){
        requestArrayList = new ArrayList<>();
        listSearch = new android.support.v4.util.ArraySet<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_LINK_REPORT,dateParam);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            LinkReport linkReport = gson.fromJson(dataNitrit.getValueDataNitrit(),LinkReport.class);

            if(linkReport.getNamaFile() != null){
                File file = new File(folderReport, linkReport.getNamaFile());
                linkReport.setFullPath(file);
            }

            listSearch.add(linkReport.getNama());
            listSearch.add(linkReport.getJenisReport());

            requestArrayList.add(linkReport);
        }
        db.close();
    }

    private void setupAdapter(){
        etSearch.setText("");

        adapter = new ReportListAdapter(getActivity(),requestArrayList);
        rv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        if(requestArrayList.size() > 0){
            etSearch.setVisibility(View.VISIBLE);
            lnoData.setVisibility(View.GONE);
        }else{
            etSearch.setVisibility(View.GONE);
            lnoData.setVisibility(View.VISIBLE);
        }

        List<String> lSearch = new ArrayList<>();
        if(listSearch != null) {
            lSearch.addAll(listSearch);
            lSearch = GlobalHelper.removeNullUsingIterator(lSearch);
            ArrayAdapter<String> adapterSearch = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_dropdown_item_1line, lSearch);
            etSearch.setAdapter(adapterSearch);
            etSearch.setThreshold(1);
            adapterSearch.notifyDataSetChanged();
        }
    }

    public void onSuccesRespon(ServiceResponse serviceResponse){
        if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
            ((BaseActivity) getActivity()).alertDialogBase.cancel();
        }
        try {
            switch (serviceResponse.getTag()){
                case GetLinkReport:
                    this.serviceResponse = serviceResponse;
                    new LongOperation().execute(String.valueOf(LongOperation_SetUpDataDownload));
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void onBadResponse(ServiceResponse serviceResponse){
        if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
            ((BaseActivity) getActivity()).alertDialogBase.cancel();
        }
        Toast.makeText(HarvestApp.getContext(),"Gagal Get Link Report",Toast.LENGTH_LONG).show();
        new LongOperation().execute(String.valueOf(LongOperation_SetUpData));
    }

    public class LongOperation extends AsyncTask<String, String, String> {
        boolean error = false;
        String message = "Error Get LinkReport";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                ((BaseActivity) getActivity()).alertDialogBase.cancel();
            }
            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(
                    getActivity(),getResources().getString(R.string.wait)
            );
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                switch (Integer.parseInt(strings[0])) {
                    case LongOperation_GetLinkReport:
                        reportActivity.connectionPresenter.GetLinkReport(sdfDateParam.format(dateSelected));
                        break;
                    case LongOperation_SetUpDataDownload:
                        try {
                            JSONArray data = new JSONArray(serviceResponse.getData().toString());
                            String dateParam = "";
                            if(data.length() > 0){
                                JSONObject objDateParam = (JSONObject) data.get(0);
                                if(objDateParam.has("tglReport")){
                                    String [] aDP = objDateParam.getString("tglReport").toString().split("/");
                                    if(aDP.length >= 3){
                                        dateParam = aDP[2] + aDP[0] + aDP[1];
                                    }else{
                                        error = true;
                                        message = "Tgl Report Tidak Benar";
                                    }
                                }
                            }else{
                                error = true;
                                message = "";
                            }

                            if(!error){
                                if(setUpDataSyncHelper.downLoadLinkReport(dateParam,serviceResponse,this)){
                                    setupData(dateParam);
                                }else{
                                    error = true;
                                    message = "Gagal baca tabel";
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            error = true;
                            message = "Error JSON";
                        }
                        break;
                    case LongOperation_SetUpData:
                        setupData(sdfDateParam.format(dateSelected));
                        break;
                    case LongOperation_DownlaodReport:
                        try {
                            if(reportActivity.selectedLinkReport != null) {
                                if(reportActivity.selectedLinkReport.getUrl() != null){
                                    reportActivity.connectionPresenter.downloadReport(reportActivity.selectedLinkReport);
                                    message = "ok";
                                }else{
                                    message = "link download belum tersedia mohon selesaikan qc dan sync ulang";
                                }
                            }else{
                                message = "value tidak di temukan";
                            }
                        } catch (Exception e){
                            message = e.getMessage();
                        }
                        break;
                }
                return strings[0];
            } catch (Exception e) {
                return "Exception";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(error){
                if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                    ((BaseActivity) getActivity()).alertDialogBase.cancel();
                }
                if(!message.isEmpty()) {
                    WidgetHelper.showSnackBar(myCoordinatorLayout, message);
                }
                setupAdapter();
                refreshRv.finishRefresh();
            }else if(result.equals("Exception")){
                if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                    ((BaseActivity) getActivity()).alertDialogBase.cancel();
                }

                WidgetHelper.showSnackBar(myCoordinatorLayout,"Gagal Prepare Data Long Operation");
                refreshRv.finishRefresh();
            }else {
                if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                    ((BaseActivity) getActivity()).alertDialogBase.cancel();
                }

                switch (Integer.parseInt(result)) {
                    case LongOperation_SetUpDataDownload:
                        setupAdapter();
                        refreshRv.finishRefresh();
                        break;
                    case LongOperation_SetUpData:
                        setupAdapter();
                        refreshRv.finishRefresh();
                        break;
                    case LongOperation_DownlaodReport:{
                        if(!message.equals("ok")){
                            Toast.makeText(HarvestApp.getContext(),message,Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(HarvestApp.getContext(),"Berhasil Download",Toast.LENGTH_SHORT).show();
                            reportActivity.openPDF();
                            new LongOperation().execute(String.valueOf(LongOperation_SetUpData));
                        }
                        break;
                    }
                }
            }
        }

        public void publishProgress(JSONObject objProgres){
            onProgressUpdate(String.valueOf(objProgres));
        }
    }

    class ReportListAdapter extends RecyclerView.Adapter<ReportListAdapter.ReportListHolder> implements Filterable {
        private final Context context;
        ArrayList<LinkReport> items;
        ArrayList<LinkReport> originLists;
        String filterText = "";
        public ReportListAdapter(Context context, ArrayList<LinkReport> items){
            this.context = context;
            this.originLists = items;
            this.items = items;
        }

        @NonNull
        @Override
        public ReportListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.row_report,parent,false);
            return new ReportListAdapter.ReportListHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull ReportListHolder holder, int position) {
            holder.setValue(items.get(position));
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults results = new FilterResults();
                    ArrayList<LinkReport> filteredEstates = new ArrayList<>();
                    String string = constraint.toString().toLowerCase();
                    for(LinkReport estate:originLists){
                        if(estate.getNama().toLowerCase().contains(string) ||
                                estate.getJenisReport().toLowerCase().contains(string)){
                            filteredEstates.add(estate);
                        }
                    }
                    filterText = string;
                    if(filteredEstates.size()>0){
                        results.count = filteredEstates.size();
                        results.values = filteredEstates;
                        return results;
                    }else {
                        return null;
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if(results!=null){
                        items = (ArrayList<LinkReport>)results.values;
                    }else{
                        items.clear();
                    }
                    notifyDataSetChanged();
                }
            };
        }

        class ReportListHolder extends RecyclerView.ViewHolder{
            CardView cv;
            TextView tv;
            TextView tv1;
            ImageView ivDownload;
            ImageView ivReDownload;
            ImageView ivOpenReport;
            LinearLayout lDownload;
            LinearLayout lRedownLoad;
            LinearLayout lOpenReport;
            public ReportListHolder(View itemView) {
                super(itemView);
                cv = (CardView) itemView.findViewById(R.id.cv);
                ivDownload = (ImageView) itemView.findViewById(R.id.ivDownload);
                ivReDownload = (ImageView) itemView.findViewById(R.id.ivReDownload);
                ivOpenReport = (ImageView) itemView.findViewById(R.id.ivOpenReport);
                lDownload = (LinearLayout) itemView.findViewById(R.id.lDownload);
                lRedownLoad = (LinearLayout) itemView.findViewById(R.id.lRedownLoad);
                lOpenReport = (LinearLayout) itemView.findViewById(R.id.lOpenReport);
                tv = (TextView) itemView.findViewById(R.id.tv);
                tv1 = (TextView) itemView.findViewById(R.id.tv1);

                ivDownload.setImageDrawable(new
                        IconicsDrawable(reportActivity)
                        .icon(MaterialDesignIconic.Icon.gmi_download)
                        .sizeDp(16)
                        .colorRes(R.color.Green));
                ivReDownload.setImageDrawable(new
                        IconicsDrawable(reportActivity)
                        .icon(MaterialDesignIconic.Icon.gmi_download)
                        .sizeDp(16)
                        .colorRes(R.color.Green));
                ivOpenReport.setImageDrawable(new
                        IconicsDrawable(reportActivity)
                        .icon(MaterialDesignIconic.Icon.gmi_file)
                        .sizeDp(16)
                        .colorRes(R.color.Green));
            }
            public void setValue(LinkReport value) {
                tv.setText(value.getNama());
                tv1.setText(value.getJenisReport());

                if(value.getFullPath().exists()){
                    lDownload.setVisibility(View.GONE);
                    lRedownLoad.setVisibility(View.VISIBLE);
                    lOpenReport.setVisibility(View.VISIBLE);
                }else{
                    lDownload.setVisibility(View.VISIBLE);
                    lRedownLoad.setVisibility(View.GONE);
                    lOpenReport.setVisibility(View.GONE);
                }

                lDownload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        reportActivity.selectedLinkReport = value;
                        new LongOperation().execute(String.valueOf(LongOperation_DownlaodReport));
                    }
                });
                lRedownLoad.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        reportActivity.selectedLinkReport = value;
                        new LongOperation().execute(String.valueOf(LongOperation_DownlaodReport));
                    }
                });
                lOpenReport.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        reportActivity.selectedLinkReport = value;
                        reportActivity.openPDF();
                    }
                });
            }
        }
    }
}
