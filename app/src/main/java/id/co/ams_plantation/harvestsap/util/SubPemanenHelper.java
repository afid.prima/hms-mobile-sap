package id.co.ams_plantation.harvestsap.util;

import id.co.ams_plantation.harvestsap.model.DynamicParameterPenghasilan;
import id.co.ams_plantation.harvestsap.model.Kongsi;

public class SubPemanenHelper {

    public static int getPersentase(DynamicParameterPenghasilan dynamicParameterPenghasilan,
                                    int jumlahPemanen,
                                    int currentPemanen,
                                    int tipeKongsi,
                                    boolean operatorPosition,
                                    int jumlahOperator,
                                    int operatorCount,
                                    int pemanenCount){
        currentPemanen ++;
        switch (tipeKongsi){
            case Kongsi.idNone:
                return 100;
            case Kongsi.idKelompok:
                if(jumlahPemanen == 1){
                    return 100;
                }else if (jumlahPemanen == 2){
                    return 50;
                }else if (jumlahPemanen == 3){
                    switch (currentPemanen){
                        case 1:
                            return 34;
                        case 2:
                            return 33;
                        case 3:
                            return 33;
                    }
                }else if (jumlahPemanen == 4){
                    return 25;
                }else if (jumlahPemanen == 5){
                    return 20;
                }else if (jumlahPemanen == 6){
                    switch (currentPemanen){
                        case 1:
                            return 17;
                        case 2:
                            return 17;
                        case 3:
                            return 17;
                        case 4:
                            return 17;
                        case 5:
                            return 16;
                        case 6:
                            return 16;
                    }
                }
                break;
            case Kongsi.idMekanis:
                if(jumlahPemanen == 1){
                    return 100;
                }else if(jumlahPemanen == 2){
                    if(operatorPosition){
                        return dynamicParameterPenghasilan.getPersenOperator();
                    }else {
                        return dynamicParameterPenghasilan.getPersenPemanen();
                    }
                }else if(jumlahPemanen == 3){
                    if(operatorPosition){
                        if(jumlahOperator == 1){
                            return dynamicParameterPenghasilan.getPersenOperator();
                        }else{
                            double nilai = dynamicParameterPenghasilan.getPersenOperator() / 2.0;
                            switch (currentPemanen) {
                                case 1:
                                    return (int) Math.ceil(Double.parseDouble(String.valueOf(nilai)));
                                case 2:
                                    if(operatorCount == 2) {
                                        return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                                    }else{
                                        return (int) Math.ceil(Double.parseDouble(String.valueOf(nilai)));
                                    }
                                case 3:
                                    return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                            }
                        }
                    }else {
                        if(jumlahOperator == 2) {
                            return dynamicParameterPenghasilan.getPersenPemanen();
                        }else{
                            double nilai = dynamicParameterPenghasilan.getPersenPemanen() / 2.0;
                            switch (currentPemanen) {
                                case 1:
                                    return (int) Math.ceil(Double.parseDouble(String.valueOf(nilai)));
                                case 2:
                                    if(operatorCount == 1) {
                                        return (int) Math.ceil(Double.parseDouble(String.valueOf(nilai)));
                                    }else{
                                        return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                                    }
                                case 3:
                                    return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                            }
                        }
                    }
                }else if(jumlahPemanen == 4){
                    if(operatorPosition){
                        if(jumlahOperator == 1){
                            return dynamicParameterPenghasilan.getPersenOperator();
                        }else if (jumlahOperator == 2){
                            double nilai = dynamicParameterPenghasilan.getPersenOperator() / 2.0;
                            switch (currentPemanen) {
                                case 1:
                                    return (int) Math.ceil(Double.parseDouble(String.valueOf(nilai)));
                                case 2:
                                    if(operatorCount == 2) {
                                        return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                                    }else{
                                        return (int) Math.ceil(Double.parseDouble(String.valueOf(nilai)));
                                    }
                                case 3:
                                    if(operatorCount == 2) {
                                        return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                                    }else{
                                        return (int) Math.ceil(Double.parseDouble(String.valueOf(nilai)));
                                    }
                                case 4:
                                    return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                            }
                        }else if (jumlahOperator == 3){
                            double nilai = dynamicParameterPenghasilan.getPersenOperator() / 3.0;
                            switch (currentPemanen) {
                                case 1:
                                    return (int) Math.ceil(Double.parseDouble(String.valueOf(nilai)));
                                case 2:
                                    if(operatorCount == 2) {
                                        return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                                    }else{
                                        return (int) Math.ceil(Double.parseDouble(String.valueOf(nilai)));
                                    }
                                case 3:
                                    return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                                case 4:
                                    return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                            }
                        }
                    }else {
                        if (jumlahOperator == 1){
                            double nilai = dynamicParameterPenghasilan.getPersenPemanen() / 3.0;
                            switch (currentPemanen) {
                                case 1:
                                    return (int) Math.ceil(Double.parseDouble(String.valueOf(nilai)));
                                case 2:
                                    if(operatorCount == 1) {
                                        return (int) Math.ceil(Double.parseDouble(String.valueOf(nilai)));
                                    }else{
                                        return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                                    }
                                case 3:
                                    return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                                case 4:
                                    return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                            }
                        }else if (jumlahOperator == 2){
                            double nilai = dynamicParameterPenghasilan.getPersenPemanen() / 2.0;
                            switch (currentPemanen) {
                                case 1:
                                    return (int) Math.ceil(Double.parseDouble(String.valueOf(nilai)));
                                case 2:
                                    if(operatorCount == 1) {
                                        return (int) Math.ceil(Double.parseDouble(String.valueOf(nilai)));
                                    }else{
                                        return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                                    }
                                case 3:
                                    if(operatorCount == 1 && pemanenCount == 2 ) {
                                        return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                                    }else{
                                        return (int) Math.ceil(Double.parseDouble(String.valueOf(nilai)));
                                    }
                                case 4:
                                    return (int) Math.floor(Double.parseDouble(String.valueOf(nilai)));
                            }
                        }else{
                            return dynamicParameterPenghasilan.getPersenPemanen();
                        }
                    }
                }
                break;
        }
        return 0;
    }
}
