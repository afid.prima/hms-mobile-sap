package id.co.ams_plantation.harvestsap.model;

public class FilterAfdeling {
    String afdeling;
    Boolean selected;

    public FilterAfdeling(String afdeling, Boolean selected) {
        this.afdeling = afdeling;
        this.selected = selected;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
}
