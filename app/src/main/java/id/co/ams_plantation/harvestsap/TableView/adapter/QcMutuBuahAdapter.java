package id.co.ams_plantation.harvestsap.TableView.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evrencoskun.tableview.adapter.AbstractTableAdapter;
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;
import com.evrencoskun.tableview.sort.SortState;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.holder.QcMutuBuahCellViewHolder;
import id.co.ams_plantation.harvestsap.TableView.holder.QcMutuBuahRowHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewColumnHeader;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestsap.TableView.view_model.QcMutuBuahTableViewModel;

/**
 * Created on : 02,Oktober,2020
 * Author     : Afid
 */

public class QcMutuBuahAdapter extends AbstractTableAdapter<TableViewColumnHeader, TableViewRowHeader, TableViewCell> {

    private final QcMutuBuahTableViewModel mTableViewModel;
    private final LayoutInflater mInflater;

    public QcMutuBuahAdapter(Context context, QcMutuBuahTableViewModel tableViewModel) {
        super();
        this.mInflater = LayoutInflater.from(context);
        this.mTableViewModel = tableViewModel;
    }

    @Override
    public int getColumnHeaderItemViewType(int position) {
        return 0;
    }

    @Override
    public int getRowHeaderItemViewType(int position) {
        return 0;
    }

    @Override
    public int getCellItemViewType(int position) {
        return 0;
    }

    @Override
    public AbstractViewHolder onCreateCellViewHolder(ViewGroup parent, int viewType) {
        View layout;
        // For cells that display a text
        layout = mInflater.inflate(R.layout.table_view_cell_layout, parent, false);

        // Create a Cell ViewHolder
        return new QcMutuBuahCellViewHolder(layout);
    }

    @Override
    public void onBindCellViewHolder(@NonNull AbstractViewHolder holder, @Nullable TableViewCell cellItemModel, int columnPosition, int rowPosition) {
        TableViewCell cell = (TableViewCell) cellItemModel;
        QcMutuBuahCellViewHolder viewHolder = (QcMutuBuahCellViewHolder) holder;
        viewHolder.setCell(cell);
    }

    @Override
    public AbstractViewHolder onCreateColumnHeaderViewHolder(ViewGroup parent, int viewType) {
        // TODO: check
        //Log.e(LOG_TAG, " onCreateColumnHeaderViewHolder has been called");
        // Get Column Header xml Layout
        View corner = mInflater.inflate(R.layout.table_view_column_header_layout, parent, false);
        // Create a ColumnHeader ViewHolder

//        corner.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                SortState sortState = TphAdapter.this.getTableView().getSortingStatus(1);
//                if (sortState != SortState.ASCENDING) {
//                    Log.d("TableViewAdapter", "Order Ascending");
//                    TphAdapter.this.getTableView().sortRowHeader(SortState.ASCENDING);
//                } else {
//                    Log.d("TableViewAdapter", "Order Descending");
//                    TphAdapter.this.getTableView().sortRowHeader(SortState.DESCENDING);
//                }
//            }
//        });
        return new ColumnHeaderViewHolder(corner, getTableView());
    }

    @Override
    public void onBindColumnHeaderViewHolder(@NonNull AbstractViewHolder holder, @Nullable TableViewColumnHeader columnHeaderItemModel, int columnPosition) {
        TableViewColumnHeader columnHeader = (TableViewColumnHeader) columnHeaderItemModel;

        // Get the holder to update cell item text
        ColumnHeaderViewHolder columnHeaderViewHolder = (ColumnHeaderViewHolder) holder;
        columnHeaderViewHolder.setColumnHeader(columnHeader);
    }

    @Override
    public AbstractViewHolder onCreateRowHeaderViewHolder(ViewGroup parent, int viewType) {
        // Get Row Header xml Layout
        View layout = mInflater.inflate(R.layout.table_view_row_header_layout, parent, false);

        // Create a Row Header ViewHolder
        return new QcMutuBuahRowHeaderViewHolder(layout);
    }

    @Override
    public void onBindRowHeaderViewHolder(@NonNull AbstractViewHolder holder, @Nullable TableViewRowHeader rowHeaderItemModel, int rowPosition) {
        TableViewRowHeader rowHeader = (TableViewRowHeader) rowHeaderItemModel;

        // Get the holder to update row header item text
        QcMutuBuahRowHeaderViewHolder rowHeaderViewHolder = (QcMutuBuahRowHeaderViewHolder) holder;
        rowHeaderViewHolder.setCell(rowHeader);
        //rowHeaderViewHolder.row_header_textview.setText(String.valueOf(rowHeader.getData()));
    }

    @NonNull
    @Override
    public View onCreateCornerView(@NonNull ViewGroup parent) {
        // Get Corner xml layout
        View corner = mInflater.inflate(R.layout.table_view_corner_layout, null);
        corner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SortState sortState = QcMutuBuahAdapter.this.getTableView()
                        .getRowHeaderSortingStatus();
                if (sortState != SortState.ASCENDING) {
                    Log.d("TableViewAdapter", "Order Ascending");
                    QcMutuBuahAdapter.this.getTableView().sortRowHeader(SortState.ASCENDING);
                } else {
                    Log.d("TableViewAdapter", "Order Descending");
                    QcMutuBuahAdapter.this.getTableView().sortRowHeader(SortState.DESCENDING);
                }
            }
        });
        return corner;
    }
}
