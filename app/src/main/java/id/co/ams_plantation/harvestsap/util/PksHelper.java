package id.co.ams_plantation.harvestsap.util;

import android.graphics.Color;
import android.os.AsyncTask;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.esri.android.map.Callout;
import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.LocationDisplayManager;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISLocalTiledLayer;
import com.esri.android.map.event.OnPanListener;
import com.esri.android.map.event.OnSingleTapListener;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.android.map.event.OnZoomListener;
import com.esri.android.map.ogc.kml.KmlLayer;
import com.esri.android.runtime.ArcGISRuntime;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Graphic;
import com.esri.core.runtime.LicenseLevel;
import com.esri.core.runtime.LicenseResult;
import com.esri.core.symbol.CompositeSymbol;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.utils.Utils;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.Estate;
import id.co.ams_plantation.harvestsap.model.PKS;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.ui.AngkutActivity;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import pl.aprilapps.easyphotopicker.EasyImage;

public class PksHelper {
    public AlertDialog listrefrence;
    FragmentActivity activity;

    SlidingUpPanelLayout mLayout;
    MapView map;
    LocationDisplayManager locationDisplayManager;
    FloatingActionButton fabZoomToLocation;
    FloatingActionButton fabZoomToEstate;
    SegmentedButtonGroup segmentedButtonGroup;
    RelativeLayout layoutBottomSheet;
    LinearLayout btnShowForm;
    ImageView ivCrosshair;
    LinearLayout ll_latlon;
    TextView tv_lat_manual;
    TextView tv_lon_manual;
    TextView tv_dis_manual;
    TextView tv_deg_manual;

    ImageView ivPhoto;
    ImageView imagecacnel;
    ImageView imagesview;
    RelativeLayout rl_ipf_takepicture;
    SliderLayout sliderLayout;

    LinearLayout lheader;
    AppCompatEditText etNamaPks;
    BetterSpinner etEst;
    CompleteTextViewHelper etBlock;
    CompleteTextViewHelper etAfdeling;
    Button btnSave;
    Button btnClose;
    BottomSheetBehavior sheetBehavior;
    View tph_new;
    View langsiran_new;

    GraphicsLayer graphicsLayer;
    GraphicsLayer graphicsLayerTPH;
    SpatialReference spatialReference;
    boolean isMapLoaded=false;

    ArrayList<File> ALselectedImage;

    ArcGISLocalTiledLayer tiledLayer;
    KmlLayer kmlLayer;
    String locKmlMap;

    BaseActivity baseActivity;
    AngkutActivity angkutActivity;
    int idgraptemp = 0;
    boolean manual;

    public double latawal = 0.0;
    public double longawal = 0.0;
    double latitude = 0.0;
    double longitude = 0.0;

    public HashMap<Integer,PKS> hashpoint;
    public Callout callout;

    PKS selectedPks;

    Boolean popUpForm = false;
    AlertDialog alertDialog2;

    public PksHelper(FragmentActivity activity) {
        this.activity = activity;
    }

    public void showMapPks(){
//        awal = true;
        idgraptemp = 0;
        manual = false;

        if(activity instanceof AngkutActivity) {
            angkutActivity = ((AngkutActivity) activity);
            if (((AngkutActivity) activity).searchingGPS != null) {
                ((AngkutActivity) activity).searchingGPS.dismiss();
            }
        }

        View view = LayoutInflater.from(activity).inflate(R.layout.tph_new,null);
        LicenseResult licenseResult = ArcGISRuntime.setClientId("uFfCzKhpVfi0ggjz");
        LicenseLevel licenseLevel = ArcGISRuntime.License.getLicenseLevel();

        mLayout = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);
        map = (MapView) view.findViewById(R.id.map);
        fabZoomToLocation = (FloatingActionButton) view.findViewById(R.id.fabZoomToLocation);
        fabZoomToEstate = (FloatingActionButton) view.findViewById(R.id.fabZoomToEstate);
        segmentedButtonGroup = (SegmentedButtonGroup) view.findViewById(R.id.segmentedButtonGroup);
//        layoutBottomSheet = (RelativeLayout) view.findViewById(R.id.bottom_sheet);
        btnShowForm = (LinearLayout) view.findViewById(R.id.btnShowForm);

//        lheader = (LinearLayout) view.findViewById(R.id.lheaderPks);

        ll_latlon = (LinearLayout) view.findViewById(R.id.ll_latlon);
        ivCrosshair = (ImageView) view.findViewById(R.id.iv_crosshair);
        tv_lat_manual = (TextView) view.findViewById(R.id.tv_lat_manual);
        tv_lon_manual = (TextView) view.findViewById(R.id.tv_lon_manual);
        tv_dis_manual = (TextView) view.findViewById(R.id.tv_dis_manual);
        tv_deg_manual = (TextView) view.findViewById(R.id.tv_deg_manual);

        etNamaPks = (AppCompatEditText) view.findViewById(R.id.etNamaPks);
        etEst = (BetterSpinner) view.findViewById(R.id.etEstPks);
        etBlock = (CompleteTextViewHelper) view.findViewById(R.id.etBlockPks);
        etAfdeling = (CompleteTextViewHelper) view.findViewById(R.id.etAfdelingPks);
        btnSave = (Button) view.findViewById(R.id.btnSavePks);
        btnClose = (Button) view.findViewById(R.id.btnClosePks);

        ivPhoto = (ImageView) view.findViewById(R.id.ivPhotoPks);
        imagecacnel = (ImageView) view.findViewById(R.id.imagecacnelPks);
        imagesview = (ImageView) view.findViewById(R.id.imagesviewPks);
        rl_ipf_takepicture = (RelativeLayout) view.findViewById(R.id.rl_ipf_takepicturePks);
        sliderLayout = (SliderLayout) view.findViewById(R.id.sliderLayoutPks);

        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.i("TPH helper", "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if(manual && newState ==  SlidingUpPanelLayout.PanelState.EXPANDED){
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.please_stop_manual_point),Toast.LENGTH_SHORT).show();
                }
                Log.i("TPH helper", "onPanelStateChanged " + newState);
            }
        });
        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });

        tph_new = (View) view.findViewById(R.id.tph_new);
        langsiran_new = (View) view.findViewById(R.id.langsiran_new);

        btnShowForm.setVisibility(View.GONE);
//        lheader.setVisibility(View.GONE);
//        layoutBottomSheet.setVisibility(View.GONE);
        segmentedButtonGroup.setVisibility(View.GONE);
//        tph_new.setVisibility(View.GONE);
//        langsiran_new.setVisibility(View.GONE);

        graphicsLayer = new GraphicsLayer();
        graphicsLayerTPH = new GraphicsLayer();
        baseActivity = (BaseActivity) activity;
        ALselectedImage = new ArrayList<>();

        latawal = baseActivity.currentlocation.getLatitude();
        longawal = baseActivity.currentlocation.getLongitude();
        latitude = baseActivity.currentlocation.getLatitude();
        longitude = baseActivity.currentlocation.getLongitude();

        fabZoomToEstate.setImageDrawable(new
                IconicsDrawable(activity)
                .icon(MaterialDesignIconic.Icon.gmi_landscape)
                .sizeDp(24)
                .colorRes(R.color.White));

        fabZoomToLocation.setImageDrawable(new
                IconicsDrawable(activity)
                .icon(MaterialDesignIconic.Icon.gmi_pin)
                .sizeDp(24)
                .colorRes(R.color.White));

        mapSetup();
        switchCrossHairUI(false);
        fabZoomToEstate.callOnClick();
//        setupBlokAfdeling(latawal,longawal);


        List<String> listableObjects = getListEstate(GlobalHelper.getUser(),angkutActivity.estate);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line, listableObjects);
        etEst.setAdapter(adapter);
        etEst.setText(angkutActivity.estate.getEstName());

        setupBlokAfdeling(latitude,longitude);

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ALselectedImage.size() > 3) {
                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.warning_take_foto),Toast.LENGTH_LONG).show();
                }else {
                    GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_NEW_LANGSIRAN;
                    EasyImage.openCamera(activity,1);
                }
            }
        });

        fabZoomToEstate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(map.isLoaded()){
                    if(tiledLayer!=null){
                        map.setExtent(tiledLayer.getFullExtent());
                    }
                    map.invalidate();
                    if(idgraptemp == 0) {
                        idgraptemp = addGrapTemp(latawal, longawal, idgraptemp);
                        pointPksSetup();

                        fabZoomToLocation.callOnClick();
                    }

                    map.setOnSingleTapListener(new OnSingleTapListener() {
                        @Override
                        public void onSingleTap(float v, float v1) {
                            if(map.isLoaded()){
                                defineTapSetup(v,v1);
                            }
                        }
                    });
                }
            }
        });

        fabZoomToLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (angkutActivity.currentLocationOK()) {
                    latawal = angkutActivity.currentlocation.getLatitude();
                    longawal = angkutActivity.currentlocation.getLongitude();
                    latitude = latawal;
                    longitude = longawal;
                    zoomToLocation(latawal,longawal,spatialReference);
                    idgraptemp = addGrapTemp(latawal,longawal,idgraptemp);
                }else{
//                    angkutActivity.searchingGPS = Snackbar.make(angkutActivity.myCoordinatorLayout,activity.getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
//                    angkutActivity.searchingGPS.show();
                    WidgetHelper.warningFindGps(angkutActivity,angkutActivity.myCoordinatorLayout);
                }
            }
        });

//        segmentedButtonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
//            @Override
//            public void onClickedButton(int position) {
//                switch (position){
//                    case 0:{
//                        manual = false;
//                        switchCrossHairUI(false);
//                        idgraptemp = addGrapTemp(latitude,longitude,idgraptemp);
//                        break;
//                    }
//                    case 1:{
//                        manual = true;
//                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                        setDefaultManual();
//                        break;
//                    }
//                }
//            }
//        });
//
//        lheader.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!manual) {
//                    switch (sheetBehavior.getState()) {
//                        case BottomSheetBehavior.STATE_COLLAPSED:
//                            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                            break;
//                        case BottomSheetBehavior.STATE_EXPANDED:
//                            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                            break;
//                    }
//                }else{
//                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.please_stop_manual_point),Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//
//        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
//        /**
//         * bottom sheet state change listener
//         * we are changing button text when sheet changed state
//         * */
//        // set the peek height
////        sheetBehavior.setPeekHeight(190);
//        sheetBehavior.setHideable(false);
//        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                if (manual) {
//                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.please_stop_manual_point),Toast.LENGTH_SHORT).show();
//                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                } else {
//                    switch (newState) {
//                        case BottomSheetBehavior.STATE_HIDDEN:
//                            break;
//                        case BottomSheetBehavior.STATE_EXPANDED:
//                            fabZoomToEstate.hide();
//                            fabZoomToLocation.hide();
//                            segmentedButtonGroup.setVisibility(View.GONE);
//                            Log.v("sheet", "STATE_EXPANDED");
//                            break;
//                        case BottomSheetBehavior.STATE_COLLAPSED:
//                            fabZoomToEstate.show();
//                            fabZoomToLocation.show();
//                            segmentedButtonGroup.setVisibility(View.VISIBLE);
//                            Log.v("sheet", "STATE_COLLAPSED");
//                            break;
//                        case BottomSheetBehavior.STATE_DRAGGING:
//                            break;
//                        case BottomSheetBehavior.STATE_SETTLING:
//                            break;
//                    }
//                }
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//
//            }
//        });
//
//        btnSave.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                new LongOperation(view).execute();
//            }
//        });

        listrefrence = WidgetHelper.showFormDialog(listrefrence,view,activity);
    }

    public void showFormDialog(){
        popUpForm = true;
        new LongOperation().execute();
    }

    private void setupFormDialog() {
        View view = LayoutInflater.from(activity).inflate(R.layout.pks_new_input, null);



        alertDialog2 = WidgetHelper.showFormDialog(alertDialog2,view,activity);
    }

    private void defineTapSetup(float x,float y){
        List<PKS> pksList = new ArrayList<>();
        int[] graphicIDs = graphicsLayerTPH.getGraphicIDs(x,y,Utils.convertDpToPx(activity,GlobalHelper.MARKER_TOUCH_SIZE));

        if(graphicIDs.length > 0) {
            for(int gid : graphicIDs) {
                for (HashMap.Entry<Integer, PKS> entry : hashpoint.entrySet()) {
                    if(gid == entry.getKey()){
                        pksList.add(entry.getValue());
                    }
                }
            }
        }

        calloutSetup();
        if (callout.isShowing()) {
            callout.hide();
        } else {
            if (graphicIDs.length == 1) {
                //jika dalam satu tap hanya ada satu titik maka kesini
                int idx = -1;
                String graptype = "";
                Graphic graphic = graphicsLayerTPH.getGraphic(graphicIDs[0]);
                if (graphic.getGeometry() instanceof Point) {
                    graptype = "point";
                    idx = graphicIDs[0];
                }
                showInfoWindow(idx,pksList.get(0));
            }else if (graphicIDs.length > 1){
                showChoseInfoWindow(graphicIDs, graphicsLayerTPH, pksList);
            }
        }
    }

    private void showChoseInfoWindow(int[] graphicIDs,GraphicsLayer gl,List<PKS> pksList){
        ArrayList<PKS> windowChoseArrayList = new ArrayList<>();

        for (int i = 0; i < graphicIDs.length; i++) {
            Graphic graphic = gl.getGraphic(graphicIDs[i]);
            int idx =0;
            if (graphic.getGeometry() instanceof Point) {
                idx = graphicIDs[i];
            }

            if(idx != 0){
                for (HashMap.Entry<Integer, PKS> entry : hashpoint.entrySet()) {
                    if(idx == entry.getKey()){
                        windowChoseArrayList.add(entry.getValue());
                    }
                }
            }
        }

//        Collections.sort(windowChoseArrayList, new Comparator<TPH>() {
//            @Override
//            public int compare(Langsiran o1, Langsiran o2) {
//                return o1.getNamaTujuan().compareTo(o2.getNamaTujuan());
//            }
//        });

//        if(windowChoseArrayList.size()>1){
//            ViewGroup content = InfoWindowChose.setupInfoWindowsChose(activity, windowChoseArrayList);
//            callout.setContent(content);
//            Graphic graphic = gl.getGraphic(graphicIDs[graphicIDs.length -1]);
//            String graptype = "";
//            int idx =0;
//            if (graphic.getGeometry() instanceof Point) {
//                graptype = "point";
//                idx = graphicIDs[graphicIDs.length -1];
//            }
//
//            Point realpoin = (Point) gl.getGraphic(idx).getGeometry();
//
//            Point wgs84poin = (Point) GeometryEngine.project(realpoin,
//                    gl.getSpatialReference(),
//                    SpatialReference.create(SpatialReference.WKID_WGS84));
//            Point mapPoint = (Point) GeometryEngine.project(wgs84poin,
//                    SpatialReference.create(SpatialReference.WKID_WGS84),
//                    map.getSpatialReference());
//            callout.setCoordinates(mapPoint);
//            map.centerAt(mapPoint,true);
//            callout.animatedShow(mapPoint,content);
//        }else if (windowChoseArrayList.size() == 1){
        showInfoWindow(graphicIDs[0],windowChoseArrayList.get(0));
//        }
    }

    private void calloutSetup() {
        this.callout = map.getCallout();
        this.callout.setStyle(R.xml.statisticspop);
        if (this.callout.isShowing()) {
            this.callout.hide();
        }
    }

    public void showInfoWindow(int idx,PKS pks){
        ViewGroup content = InfoWindow.setupInfoWindowsPks(activity, pks);
        callout.setContent(content);
        Point realpoin = (Point) graphicsLayerTPH.getGraphic(idx).getGeometry();

        if(realpoin!= null) {
            Point wgs84poin = (Point) GeometryEngine.project(realpoin,
                    graphicsLayerTPH.getSpatialReference(),
                    SpatialReference.create(SpatialReference.WKID_WGS84));
            Point mapPoint = (Point) GeometryEngine.project(wgs84poin,
                    SpatialReference.create(SpatialReference.WKID_WGS84),
                    map.getSpatialReference());
            callout.setCoordinates(mapPoint);
            map.centerAt(mapPoint,true);
            callout.animatedShow(mapPoint,content);
        }
    }

    private ArrayList<String> getListEstate(User user, Estate estate){
        ArrayList<String> estateArrayList = new ArrayList<>();
        for(Estate estate1:user.getEstates()) {
            if(estate1.getCompanyShortName().equalsIgnoreCase(estate.getCompanyShortName())){
                estateArrayList.add(estate1.getEstName());
            }
        }
        return estateArrayList;
    }

    private void setupimageslide(){
        Set<File> sf = new HashSet<>();
        sf.addAll(ALselectedImage);
        ALselectedImage.clear();
        ALselectedImage.addAll(sf);

        if(ALselectedImage.size() > 3){
            ivPhoto.setVisibility(View.GONE);
        }else{
            ivPhoto.setVisibility(View.VISIBLE);
        }
        if(ALselectedImage.size() != 0){
            imagecacnel.setVisibility(View.VISIBLE);
            imagesview.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.VISIBLE);
        }else{
            imagecacnel.setVisibility(View.GONE);
            imagesview.setVisibility(View.GONE);
            sliderLayout.setVisibility(View.GONE);
        }
        sliderLayout.removeAllSliders();
        for(File file : ALselectedImage){
            TextSliderView textSliderView = new TextSliderView(activity);
            // initialize a SliderLayout
            if(file.toString().startsWith("http:")){
                String surl = file.toString();
                if(file.toString().startsWith("http://")){

                }else if(file.toString().startsWith("http:/")){
                    surl =surl.replace("http:/","http://");
                }

                textSliderView
                        .image(GlobalHelper.setUrlFoto(surl))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }else {
                textSliderView
                        .image(file)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        imagecacnel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
                try {
                    if (sliderLayout.getCurrentPosition() == 0 && ALselectedImage.size() == 1) {
                        ALselectedImage.clear();
                        sliderLayout.removeAllSliders();
                        sliderLayout.setVisibility(View.GONE);
                        imagecacnel.setVisibility(View.GONE);
                        imagesview.setVisibility(View.GONE);
                    } else {
                        ALselectedImage.remove(sliderLayout.getCurrentPosition());
                        sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
                    }
                }catch (Exception e){
                    Log.e("sliderLayout", e.toString());
                }
            }
        });
        imagesview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WidgetHelper.showImagesZoom(activity,ALselectedImage);
            }
        });
    }

    private void mapSetup(){
        //
        map.removeAll();
        graphicsLayer.removeAll();
        String locTiledMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VKM);
        tiledLayer = new ArcGISLocalTiledLayer(locTiledMap,true);
        tiledLayer.setRenderNativeResolution(true);
        tiledLayer.setMinScale(250000.0d);
        tiledLayer.setMaxScale(1000.0d);
        map.addLayer(tiledLayer);

        locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
        kmlLayer = new KmlLayer(locKmlMap);
        kmlLayer.setOpacity(0.3f);
        map.addLayer(kmlLayer);

        map.setOnStatusChangedListener(new OnStatusChangedListener() {
            @Override
            public void onStatusChanged(Object o, STATUS status) {
                if(o==map && status==STATUS.INITIALIZED){
                    spatialReference = map.getSpatialReference();
                    isMapLoaded = true;
                    fabZoomToEstate.callOnClick();
                }
            }
        });
        map.setOnPanListener(new OnPanListener() {
            @Override
            public void prePointerMove(float v, float v1, float v2, float v3) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Point prePoint = map.getCenter();
                        Point point = (Point) GeometryEngine.project(prePoint,map.getSpatialReference(), SpatialReference.create(SpatialReference.WKID_WGS84));
                    }
                }
            }

            @Override
            public void postPointerMove(float v, float v1, float v2, float v3) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Point prePoint = map.getCenter();
                        Point point = (Point) GeometryEngine.project(prePoint,map.getSpatialReference(),SpatialReference.create(SpatialReference.WKID_WGS84));
                        double imeter = GlobalHelper.distance(latawal,point.getY(),longawal,point.getX());
                        double bearing = GlobalHelper.bearing(latawal,point.getY(),longawal,point.getX());
                        tv_lat_manual.setText(String.format("%.5f",point.getY()));
                        tv_lon_manual.setText(String.format("%.5f",point.getX()));
                        tv_dis_manual.setText(String.format("%.0f m",imeter));
                        tv_deg_manual.setText(String.format("%.0f",bearing));
                        if(point.getY() != 0.0 && point.getX() != 0.0) {
                            if (imeter <= GlobalHelper.RADIUS) {
                                latitude = point.getY();
                                longitude = point.getX();
                                ll_latlon.setBackgroundColor(Color.WHITE);
                            } else {
                                ll_latlon.setBackgroundColor(Color.RED);
                            }
                        }
                    }

                }
            }

            @Override
            public void prePointerUp(float v, float v1, float v2, float v3) {

            }

            @Override
            public void postPointerUp(float v, float v1, float v2, float v3) {

            }
        });
        map.setOnZoomListener(new OnZoomListener() {
            @Override
            public void preAction(float v, float v1, double v2) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Log.e("zoom preAction",v+ " ; "+ v1+" ; "+v2);
                        Log.e("zoom scale",String.format("%.0f",map.getScale()));
                        int scale = (int) map.getScale();
                        int zoomlvl = GlobalHelper.getZoomlvl(map.getScale());
                        Log.v("zoom lvl",String.format(" 1 : %,dm Zl : %,d",scale,zoomlvl));
                        map.getSpatialReference();
                    }

                }
            }

            @Override
            public void postAction(float v, float v1, double v2) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Log.e("zoom postAction",v+ " ; "+ v1+" ; "+v2);
                        int scale = (int) map.getScale();
                        int zoomlvl = GlobalHelper.getZoomlvl(map.getScale());
                        Log.v("zoom lvl",String.format(" 1 : %,dm Zl : %,d",scale,zoomlvl));
                    }

                }
            }
        });

        graphicsLayer.setMinScale(70000d);
        graphicsLayer.setMaxScale(1000d);
        graphicsLayerTPH.setMinScale(70000d);
        graphicsLayerTPH.setMaxScale(1000d);
        map.addLayer(graphicsLayer);
        map.addLayer(graphicsLayerTPH);
        map.invalidate();
    }

    public void pointPksSetup(){
        graphicsLayerTPH.removeAll();
        hashpoint = new HashMap<>();

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_PKS);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> tphIterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = tphIterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            PKS pks = gson.fromJson(dataNitrit.getValueDataNitrit(),PKS.class);
//            double radius = GlobalHelper.distance(latawal,langsiran.getLatitude(),
//                    longawal,langsiran.getLongitude());
            addGraphicPks(pks);
        }

        db.close();
    }

    private void addGraphicPks(PKS pks){
        Point fromPoint = new Point(pks.getLongitude(), pks.getLatitude());
        Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

        CompositeSymbol cms = new CompositeSymbol();
        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(activity.getResources().getColor(R.color.Blue), Utils.convertDpToPx(activity, GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.CIRCLE);
        SimpleLineSymbol sls = new SimpleLineSymbol(activity.getResources().getColor(R.color.Blue), GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        sms.setOutline(sls);
        cms.add(sms);
        Graphic pointGraphic = new Graphic(toPoint, cms);
        int idgrap = graphicsLayerTPH.addGraphic(pointGraphic);
        hashpoint.put(idgrap,pks);
    }

    private int addGrapTemp(Double lat, Double lon, int idlama){
        if(map!=null && map.isLoaded()){
            Point fromPoint = new Point(lon, lat);
            Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

            CompositeSymbol cms = new CompositeSymbol();
            SimpleMarkerSymbol sms = new SimpleMarkerSymbol(Color.RED, Utils.convertDpToPx(activity, GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.X);
            SimpleLineSymbol sls = new SimpleLineSymbol(Color.RED, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
            sms.setOutline(sls);
            cms.add(sms);
            Graphic pointGraphic = new Graphic(toPoint, cms);
            setupBlokAfdeling(lat,lon);

//            try {
//                String [] blockAfdeling = GlobalHelper.getAndAssignBlock(new File(locKmlMap),latitude,longitude).split(";");
//                etAfdeling.setText(blockAfdeling[2]);
//                etBlock.setText(blockAfdeling[1]);
//            }catch (Exception e){
//                e.printStackTrace();
//            }
            if(idlama!= 0){
                removeGrapTemp(idlama);
            }

            return graphicsLayer.addGraphic(pointGraphic);
        }else{
            return 0;
        }
    }

    private void removeGrapTemp(int id){
        graphicsLayer.removeGraphic(id);
    }

    private void setupBlokAfdeling(Double latitude,Double longitude){
        Set<String> allBlockAfdeling = GlobalHelper.getNearBlock(new File(locKmlMap),latitude,longitude);

        Set<String> allBlock = new ArraySet<>();
        Set<String> allAfdeling = new ArraySet<>();

        for(String s : allBlockAfdeling){
            try {
                String [] split = s.split(";");
                allAfdeling.add(split[1]);
                allBlock.add(split[2]);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
                (activity,android.R.layout.select_dialog_item, allBlock.toArray(new String[allBlock.size()]));
        etBlock.setThreshold(1);
        etBlock.setAdapter(adapterBlock);

        ArrayAdapter<String> adapterAfdeling = new ArrayAdapter<String>
                (activity,android.R.layout.select_dialog_item, allAfdeling.toArray(new String[allAfdeling.size()]));
        etAfdeling.setThreshold(1);
        etAfdeling.setAdapter(adapterAfdeling);

        try {
            String [] blockAfdeling = GlobalHelper.getAndAssignBlock(new File(locKmlMap),latitude,longitude).split(";");
            etBlock.setText(blockAfdeling[2]);
            etAfdeling.setText(blockAfdeling[1]);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setDefaultManual(){

        if(selectedPks != null){
            longitude = longawal;
            latitude = latawal;
            selectedPks = null;

            ALselectedImage.clear();
            setupimageslide();
        }

        Point mapPoint = GeometryEngine.project(longitude,latitude,spatialReference);
        map.centerAt(mapPoint,true);
        tv_lat_manual.setText(String.format("%.5f",longitude));
        tv_lon_manual.setText(String.format("%.5f",latitude));
        tv_deg_manual.setText("0");
        tv_dis_manual.setText("0 m");
        ll_latlon.setBackgroundColor(Color.WHITE);
        zoomToLocation(latitude,longitude,spatialReference);
        GlobalHelper.hideKeyboard(activity);

        switchCrossHairUI(true);
        btnSave.setVisibility(View.VISIBLE);
    }

    private void switchCrossHairUI(boolean visible){
        if(visible){
            ivCrosshair.setVisibility(View.VISIBLE);
            ll_latlon.setVisibility(View.VISIBLE);
        }else{
            ivCrosshair.setVisibility(View.GONE);
            ll_latlon.setVisibility(View.GONE);
        }
    }

    private void zoomToLocation(double lat, double lon, SpatialReference spatialReference) {
        Point mapPoint = GeometryEngine.project(lon,lat,spatialReference);
        map.centerAt(mapPoint,true);
        map.zoomTo(mapPoint,15000f);
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        private AlertDialog alertDialogAllpoin;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(activity, activity.getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... params) {
            popUpForm = false;
            return "popup";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.equals("popup")) {
                setupFormDialog();
                if (alertDialogAllpoin != null) {
                    alertDialogAllpoin.cancel();
                }
            }
        }
    }
}
