package id.co.ams_plantation.harvestsap.util;

public class PathUrl {

    public static final String GetMasterUserByEstate = "Master/GetMasterUserByEstate?estCode=";
    public static final String GetPemanenListByEstate = "Master/GetPemanenListByEstate?estCode=";
    public static final String GetOperatorListByEstate = "Master/GetOperatorListByEstate?estCode=";
    public static final String GetVehicleListByEstate = "Master/GetVehicleListByEstate?estCode=";
    public static final String GetListPKSByEstate = "Master/GetListPKSByEstate?estCode=";
    public static final String GetTphListByEstate = "tph/GetAllTPHByEstateSP?estCode=";
    public static final String GetLangsiranListByEstate = "langsiran/GetAllLangsiranByEstate?estCode=";
    public static final String GetTransaksiPanenListByEstate = "harvest/GetLatestTransaksiPanenByEstateView?estCode=";
    public static final String GetTransaksiAngkutListByEstate = "angkut/GetLatestTAngkutByEstateView?estCode=";
    public static final String GetTransaksiSpbListByEstate = "spb/GetLatestSPBByEstateView?estCode=";
    public static final String GetAfdelingAssistantByEstate = "Master/GetAfdelingAssistantByEstate?estCode=";
    public static final String GetApplicationConfiguration = "Master/GetApplicationConfiguration?estCode=";
    public static final String GetBJRInformation = "Master/GetBJRInformation?estCode=";
    public static final String GetSupervisionListByEstate = "Master/GetSupervisionListByEstate?estCode=";
    public static final String GetISCCInformation = "Master/GetISCCInformation?estCode=";
    public static final String GetQCMutuBuahByEstate = "harvest/GetQCMutuBuahByEstate?estCode=";
    public static final String GetSensusBJRByEstate = "harvest/GetSensusBJRByEstate?estCode=";
    public static final String GetQCMutuAncakByEstate = "harvest/GetQCMutuAncakByEstate?estCode=";
    public static final String GetReport = "report/RequestReport";
    public static final String GetEstateAndAssistanceList = "assistance/GetEstateAndAssistanceList";
    public static final String GetLatestRequestAssistensiByRequestUserId = "assistance/GetLatestRequestAssistensiByRequestUserId?userID=";
    public static final String GetMasterSPKByEstate = "master/getMasterSPKByEstate?estCode=";
    public static final String GetMasterCagesByEstate = "master/getMasterCages?estCode=";
    public static final String GetLinkReport = "Report/GetLinkReport?estCode=";
    public static final String GetOpnameNFCByEstate = "harvest/getOpnameNFC?estCode=";
    public static final String GetKonfigurasiGerdang = "Master/GetKonfigurasiGerdang?estCode=";
    public static final String GetTPHReasonUnharvestMaster = "tph/getTPHReasonUnharvestMaster?estCode=";
    public static final String GetBlockMekanisasi = "Master/GetBlockMekanisasi?estCode=";

    public static final String PostPemanen = "master/InsertMasterNomorPemanen?estCode=";
    public static final String PostTphImage = "tph/InsertImageTPH?estCode=";
    public static final String PostTph = "tph/insertTPHnew?estCode=";
    public static final String PostLangsiranImage = "langsiran/InsertImageLangsiran?estCode=";
    public static final String PostLangsiran = "langsiran/insertLangsiranNew?estCode=";
    public static final String PostTransaksiPanenImage = "harvest/InsertImagePanen?estCode=";
    public static final String PostTransaksiPanen = "harvest/insertTransaksiPanen?estCode=";
    public static final String PostTransaksiPanenNFC = "harvest/insertTransaksiPanenDesktopArray?estCode=";
    public static final String PostTransaksiAngkutImage = "angkut/InsertImageTAngkut?estCode=";
    public static final String PostTransaksiAngkut = "angkut/InsertTAngkut?estCode=";
    public static final String PostTransaksiAngkutNFC = "angkut/InsertTAngkutDesktopArray?estCode=";
    public static final String PostTransaksiSPBImage = "spb/InsertImageSPB?estCode=";
    public static final String PostTransaksiSPB = "spb/InsertSPB?estCode=";
    public static final String PostSupervision = "harvest/InsertSupervisionPanen";
    public static final String PostQcBuahImages = "harvest/InsertImageQCMutuBuah";
    public static final String PostQcBuah = "harvest/InsertQCMutuBuah";
    public static final String PostSensusBJRImages = "harvest/InsertImageSensusBJR";
    public static final String PostSensusBJR = "harvest/InsertSensusBJR";
    public static final String PostQcAncak = "harvest/InsertQCMutuAncak";
    public static final String PostTransaksiCounter = "Master/insertCounter?userId=";
    public static final String InsertAssistanceRequest = "assistance/InsertAssistanceRequest?userID=";
    public static final String InsertOpnameNFC = "harvest/insertOpnameNFC?estCode=";
    public static final String InsertGerdangDetail = "harvest/InsertGerdangDetail?estCode=";

    public static final String GetTPanenCounterByUserID = "harvest/GetTPanenCounterByUserID?userID=";
    public static final String GetSPBCounterByUserID = "spb/GetSPBCounterByUserID?userID=";
    public static final String GetTAngkutCounterByUserID = "angkut/GetTAngkutCounterByUserID?userID=";
    public static final String GetTPHCounterByUserID = "tph/GetTPHCounterByUserID?userID=";
    public static final String GetLangsiranCounterByUserID = "langsiran/GetLangsiranCounterByUserID?userID=";
    public static final String GetTPanenSupervisionCounterByUserID = "harvest/GetTPanenSupervisionCounterByUserID?userID=";
    public static final String GetQCMutuAncakHeaderCounterByUserID = "harvest/GetQCMutuAncakHeaderCounterByUserID?userID=";

    public static final String CrashReportList = "user/CrashReportList";
    public static final String UploadLogSPB = "master/insertLogSPB?estCode=";
    public static final String LogActivity = "user/SubmitUserAppUsage";
}
