package id.co.ams_plantation.harvestsap.presenter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import id.co.ams_plantation.harvestsap.connection.Tag;
import id.co.ams_plantation.harvestsap.presenter.base.ApiPresenter;
import id.co.ams_plantation.harvestsap.presenter.base.BasePresenter;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.PathUrl;
import id.co.ams_plantation.harvestsap.view.ApiView;
import id.co.ams_plantation.harvestsap.view.ReportView;

public class ReportPresenter extends BasePresenter {
    public ReportPresenter(ReportView view){attachView(view);}

    public void GetReport(int userId, String emailTo, Date dateParam, String companyName, int reportId){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        HashMap<String, String> queryParam = new HashMap<>();
        queryParam.put("userId", ""+userId);
        queryParam.put("emailTo", emailTo);
        queryParam.put("dateParam",simpleDateFormat.format(dateParam));
        queryParam.put("estCode", GlobalHelper.getEstate().getEstCode());
        queryParam.put("estateName", GlobalHelper.getEstate().getEstName());
        queryParam.put("companyName", companyName);
        queryParam.put("reportId", ""+reportId);
//        get(PathUrl.GetReport + "userId="+userId+"&emailTo="+emailTo
//                +"&dateParam="+dateParam+"&estCode="+ GlobalHelper.getEstate().getEstCode()+
//                "&estateName="+GlobalHelper.getEstate().getEstName()+"&companyName="+companyName+
//                "&reportId="+reportId,new HashMap<>(), Tag.GetReport);
//        get(PathUrl.GetReport, queryParam, Tag.GetReport);
    }
}
