package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import id.co.ams_plantation.harvestsap.Fragment.AssistensiListFragment;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.AssistensiRequest;
import id.co.ams_plantation.harvestsap.ui.AssistensiActivity;

public class AssistensiAdapter extends RecyclerView.Adapter<AssistensiAdapter.Holder>{
    Context context;
    ArrayList<AssistensiRequest> originLists;
    public AssistensiAdapter(Context context, ArrayList<AssistensiRequest> assistensiRequests){
        this.context = context;
        originLists = assistensiRequests;
    }

    @NonNull
    @Override
    public AssistensiAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.assistensi_row,parent,false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AssistensiAdapter.Holder holder, int position) {
        holder.setValue(originLists.get(position));
    }

    @Override
    public int getItemCount() {
        return originLists.size();
    }

    @Override
    public long getItemId(int position) {
//        return super.getItemId(position);
        return position;
    }

    @Override
    public int getItemViewType(int position) {
//        return super.getItemViewType(position);
        return position;
    }

    public class Holder extends RecyclerView.ViewHolder {
        CardView card_view;
        View llStatus;
        TextView item_ket1;
        TextView item_ket2;
        TextView item_ket3;
        TextView item_ket4;
        TextView item_ket5;
        TextView item_ket6;

        public Holder(View itemView) {
            super(itemView);

            card_view = itemView.findViewById(R.id.card_view);
            llStatus = itemView.findViewById(R.id.llStatus);
            item_ket1 = itemView.findViewById(R.id.item_ket1);
            item_ket2 = itemView.findViewById(R.id.item_ket2);
            item_ket3 = itemView.findViewById(R.id.item_ket3);
            item_ket4 = itemView.findViewById(R.id.item_ket4);
            item_ket5 = itemView.findViewById(R.id.item_ket5);
            item_ket6 = itemView.findViewById(R.id.item_ket6);
        }

        public void setValue(final AssistensiRequest value) {
            SimpleDateFormat formatDate = new SimpleDateFormat("dd MMMM yyyy");
            SimpleDateFormat day = new SimpleDateFormat("dd");

            long hari = value.getEndRequest() - value.getStartRequest();

            item_ket1.setText(value.getIdAssistensi().substring(0,4));
            item_ket2.setText(value.getEstateReceive().getEstCode() + " - "+ value.getEstateReceive().getEstName());
            item_ket3.setText("By : "+value.getCreateBy().getUserFullName());
            item_ket4.setText("To : "+value.getAssistenRecive().getUserFullName());
            item_ket5.setText(formatDate.format(value.getStartRequest())
                    + " - " + formatDate.format(value.getEndRequest()));
            item_ket6.setText(day.format(hari) + " Hari         "+ AssistensiRequest.statusName[value.getStatus()]);

            llStatus.setBackgroundColor(AssistensiRequest.statusColor[value.getStatus()]);

            card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(context instanceof AssistensiActivity){
                        if (((AssistensiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof AssistensiListFragment) {
                            AssistensiListFragment fragment = (AssistensiListFragment) ((AssistensiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
                            fragment.setSelectedAssistensiRequest(value);
                        }

                    }
                }
            });
        }
    }
}
