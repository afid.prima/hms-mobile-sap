package id.co.ams_plantation.harvestsap.TableView.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;

/**
 * Created by user on 12/24/2018.
 */

public class TPHCellViewHolder extends AbstractViewHolder {

    public final TextView cell_textview;
    public final LinearLayout cell_container;
    private TableViewCell cell;

    public TPHCellViewHolder(View itemView) {
        super(itemView);
        cell_textview = (TextView) itemView.findViewById(R.id.cell_data);
        cell_container = (LinearLayout) itemView.findViewById(R.id.cell_container);
    }

    public void setCell(TableViewCell cell) {
        this.cell = cell;
        cell_textview.setText(String.valueOf(cell.getData()));
        cell_textview.setBackgroundColor(cell.getColor());
        // If your TableView should have auto resize for cells & columns.
        // Then you should consider the below lines. Otherwise, you can ignore them.

        // It is necessary to remeasure itself.
        cell_container.getLayoutParams().width = LinearLayout.LayoutParams.WRAP_CONTENT;
        cell_container.setBackgroundColor(cell.getColor());
//        String [] id = cell.getId().split("_");
//        switch (Integer.parseInt(id[2])){
//            case TransaksiPanen.SAVE_ONLY:
//                cell_container.setBackgroundColor(TransaksiPanen.COLOR_SAVE_ONLY);
//                break;
//            case TransaksiPanen.TAP:
//                cell_container.setBackgroundColor(TransaksiPanen.COLOR_TAP);
//                break;
//            case TransaksiPanen.UPLOAD:
//                cell_container.setBackgroundColor(TransaksiPanen.COLOR_UPLOAD);
//                break;
//            case TransaksiPanen.SUDAH_DIANGKUT:
//                cell_container.setBackgroundColor(TransaksiPanen.COLOR_SUDAH_DIANGKUT);
//                break;
//            case TransaksiPanen.SUDAH_DIPKS:
//                cell_container.setBackgroundColor(TransaksiPanen.COLOR_SUDAH_DIPKS);
//                break;
//        }

        cell_textview.requestLayout();
    }
}
