package id.co.ams_plantation.harvestsap.util;

import android.util.Log;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.CompressionLevel;
import net.lingala.zip4j.model.enums.CompressionMethod;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ZipManager {
    public static void zipFolder(String sourceFolder, String zipFilePath, String password) {
        try {
            ZipFile zipFile = new ZipFile(zipFilePath, password.toCharArray());
            ZipParameters parameters = new ZipParameters();
            parameters.setCompressionMethod(CompressionMethod.DEFLATE);
            parameters.setCompressionLevel(CompressionLevel.NORMAL);

            File folderToZip = new File(sourceFolder);
            if (folderToZip.isDirectory()) {
                zipFile.addFolder(folderToZip, parameters);
            }
        } catch (ZipException e) {
            e.printStackTrace();
        }
    }

    public static void unzipFolder(String zipFilePath, String destFolder, String password) {
        try {
            ZipFile zipFile = new ZipFile(zipFilePath, password.toCharArray());
            zipFile.extractAll(destFolder);
        } catch (ZipException e) {
            e.printStackTrace();
        }
    }
}
