package id.co.ams_plantation.harvestsap.model;

import java.io.File;

/**
 * Created on : 17,February,2022
 * Author     : Afid
 */

public class LinkReport {

    String userId;
    String nama;
    String jenisReport;
    String tglReport;
    String url;
    String namaFile;
    File fullPath;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenisReport() {
        return jenisReport;
    }

    public void setJenisReport(String jenisReport) {
        this.jenisReport = jenisReport;
    }

    public String getTglReport() {
        return tglReport;
    }

    public void setTglReport(String tglReport) {
        this.tglReport = tglReport;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNamaFile() {
        return namaFile;
    }

    public void setNamaFile(String namaFile) {
        this.namaFile = namaFile;
    }

    public File getFullPath() {
        return fullPath;
    }

    public void setFullPath(File fullPath) {
        this.fullPath = fullPath;
    }
}
