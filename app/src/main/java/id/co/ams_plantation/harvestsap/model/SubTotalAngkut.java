package id.co.ams_plantation.harvestsap.model;

public class SubTotalAngkut {
    String block;
    int janjang;
    int brondolan;
    double berat;

    public SubTotalAngkut(String block, int janjang, int brondolan, double berat) {
        this.block = block;
        this.janjang = janjang;
        this.brondolan = brondolan;
        this.berat = berat;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public int getJanjang() {
        return janjang;
    }

    public void setJanjang(int janjang) {
        this.janjang = janjang;
    }

    public int getBrondolan() {
        return brondolan;
    }

    public void setBrondolan(int brondolan) {
        this.brondolan = brondolan;
    }

    public double getBerat() {
        return berat;
    }

    public void setBerat(double berat) {
        this.berat = berat;
    }
}
