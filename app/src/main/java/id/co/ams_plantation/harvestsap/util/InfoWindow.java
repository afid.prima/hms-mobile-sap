package id.co.ams_plantation.harvestsap.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import id.co.ams_plantation.harvestsap.Fragment.AngkutInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.Langsiran;
import id.co.ams_plantation.harvestsap.model.PKS;
import id.co.ams_plantation.harvestsap.model.TPH;
import id.co.ams_plantation.harvestsap.model.TPHnLangsiran;
import id.co.ams_plantation.harvestsap.ui.AngkutActivity;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.MapActivity;
import id.co.ams_plantation.harvestsap.ui.TphActivity;

/**
 * Created by user on 12/11/2018.
 */

public class InfoWindow {
    static Context context;
    static ViewGroup retView;
    static LinearLayout ll_ipf_takepicture;
    static Button btnChose,btnDetail,btnDelet;
    static TextView tvheader,tvket1_iw,tvket2_iw,tvket3_iw,tvket4_iw;
    static ImageView ivPhoto,ic_eye;
    static SliderLayout sliderLayout;
    static FragmentActivity activity;
    static LinearLayout ldelete;
    static LinearLayout lcheck;
    static int Activity_From;
    final static int Activity_From_TphActivity = 0;
    final static int Activity_From_MapActivity = 1;

    private static void setupInfoWindowsP(final Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        retView = (ViewGroup) inflater.inflate(R.layout.info_window, null);
        InfoWindow.context = context;
    }
    private static void setupInfoWindowsUnharvest(final Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        retView = (ViewGroup) inflater.inflate(R.layout.info_window_unharvest, null);
        InfoWindow.context = context;
    }
    public static ViewGroup setupInfoWindows(final Context context, TPH tph){
        if (tph.getPanen3Bulan() == 0 || tph.getReasonUnharvestTPH() != null) {
            setupInfoWindowsUnharvest(context);
            initBlockDetailUnharvest(retView,tph);
        }else{
            setupInfoWindowsP(context);
            initBlockDetail(retView,tph);
        }
        return retView;
    }
    public static ViewGroup setupInfoWindowsLangsiran(final Context context, Langsiran langsiran){
        setupInfoWindowsP(context);
        initBlockDetailLangsiran(retView,langsiran);
        return retView;
    }
    public static ViewGroup setupInfoWindowsPks(final Context context, PKS pks){
        setupInfoWindowsP(context);
        initBlockDetailPks(retView,pks);
        return retView;
    }
    private static void initBlockDetailUnharvest(final ViewGroup retView, final TPH tph) {

        tvheader = (TextView) retView.findViewById(R.id.tv_header_iw);
        tvket1_iw = (TextView) retView.findViewById(R.id.tvket1_iw);

        ivPhoto = (ImageView) retView.findViewById(R.id.ivPhoto);
        ic_eye = (ImageView) retView.findViewById(R.id.ic_eye);
        sliderLayout = (SliderLayout) retView.findViewById(R.id.sliderLayout);
        ldelete = (LinearLayout) retView.findViewById(R.id.ldelete);
        lcheck = (LinearLayout) retView.findViewById(R.id.lcheck);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy");


        if(context instanceof MapActivity){
            activity = (MapActivity) context;
            Activity_From = Activity_From_MapActivity;
        }else if(context instanceof TphActivity) {
            activity = (TphActivity) context;
            Activity_From = Activity_From_TphActivity;
        }

        tvheader.setText("Inspeksi TPH 3 Bulan Tidak Panen ");
        String ket1 = "TPH : " + tph.getNamaTph().trim()+ "\n";
        ket1 = ket1 + activity.getResources().getString(R.string.create_time)+" : "+ sdf.format(new Date(tph.getCreateDate()));
        tvket1_iw.setText(ket1);
        if(tph.getReasonUnharvestTPH() != null){
            if(tph.getReasonUnharvestTPH().getActionName().toLowerCase().equals("delete")){
                ldelete.setBackground(context.getResources().getDrawable(R.drawable.border_selected));
            }else{
                lcheck.setBackground(context.getResources().getDrawable(R.drawable.border_selected));
            }
        }

        ArrayList<File> files = new ArrayList<>();
        if(tph.getFoto() != null) {
            if (tph.getFoto().size() > 0) {
                for (String s : tph.getFoto()) {
                    File file = new File(s);
                    files.add(file);
                }
            }
        }
        setupSlideshow(files);

        ldelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (Activity_From) {
                    case Activity_From_MapActivity:
                        if(((BaseActivity) context).currentLocationOK()) {
                            double distance = GlobalHelper.distance(((BaseActivity) context).currentlocation.getLatitude(), tph.getLatitude(),
                                    ((BaseActivity) context).currentlocation.getLongitude(), tph.getLongitude());
                            if (distance > GlobalHelper.MAX_RADIUS_MAPMENU) {
                                Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.radius_to_long), Toast.LENGTH_SHORT).show();
                            } else {
                                ((MapActivity) context).tphHelper.yesnoNonAktifTph(tph,true);
                            }
                        }
                        break;
                }
            }
        });

        lcheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (Activity_From) {
                    case Activity_From_MapActivity:
                        if(((BaseActivity) context).currentLocationOK()) {
                            double distance = GlobalHelper.distance(((BaseActivity) context).currentlocation.getLatitude(), tph.getLatitude(),
                                    ((BaseActivity) context).currentlocation.getLongitude(), tph.getLongitude());
                            if (distance > GlobalHelper.MAX_RADIUS_MAPMENU) {
                                Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.radius_to_long), Toast.LENGTH_SHORT).show();
                            } else {
                                ((MapActivity) context).tphHelper.yesnoNonAktifTph(tph,false);
                            }
                        }
                        break;
                }
            }
        });
    }

    private static void initBlockDetail(final ViewGroup retView, final TPH tph) {

        tvheader = (TextView) retView.findViewById(R.id.tv_header_iw);
        tvket1_iw = (TextView) retView.findViewById(R.id.tvket1_iw);
//        tvket2_iw = (TextView) retView.findViewById(R.id.tvket2_iw);
//        tvket3_iw = (TextView) retView.findViewById(R.id.tvket3_iw);
//        tvket4_iw = (TextView) retView.findViewById(R.id.tvket4_iw);

        ivPhoto = (ImageView) retView.findViewById(R.id.ivPhoto);
        ic_eye = (ImageView) retView.findViewById(R.id.ic_eye);
        sliderLayout = (SliderLayout) retView.findViewById(R.id.sliderLayout);
        btnChose = (Button) retView.findViewById(R.id.btnChose_iw);
        btnDetail = (Button) retView.findViewById(R.id.btnDetail_iw);
        btnDelet = (Button) retView.findViewById(R.id.btnDelet_iw);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy");


        if(context instanceof MapActivity){
            activity = (MapActivity) context;
            Activity_From = Activity_From_MapActivity;
            btnChose.setVisibility(View.GONE);
            btnDelet.setVisibility(View.VISIBLE);
            btnDelet.setText("Action");
        }else if(context instanceof TphActivity) {
            activity = (TphActivity) context;
            Activity_From = Activity_From_TphActivity;
            btnChose.setVisibility(View.VISIBLE);
        }

        tvheader.setText(tph.getNamaTph().trim());
        String ket1 = "Ancak : " + tph.getAncak()+ "\n";
        ket1 = ket1 + activity.getResources().getString(R.string.create_time)+" : "+ sdf.format(new Date(tph.getCreateDate()));
        tvket1_iw.setText(ket1);
//        tvket2_iw.setVisibility(View.GONE);
//        tvket3_iw.setVisibility(View.GONE);
//        tvket4_iw.setVisibility(View.GONE);

        ArrayList<File> files = new ArrayList<>();
        if(tph.getFoto() != null) {
            if (tph.getFoto().size() > 0) {
                for (String s : tph.getFoto()) {
                    File file = new File(s);
                    files.add(file);
                }
            }
        }
        setupSlideshow(files);

        btnChose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = (Fragment) activity.getSupportFragmentManager().findFragmentById(R.id.content_container);
                if (fragment instanceof TphInputFragment) {
                    double distance = GlobalHelper.distance(((TphInputFragment) fragment).tphHelper.latawal, tph.getLatitude(), ((TphInputFragment) fragment).tphHelper.longawal, tph.getLongitude());
                    if (distance > GlobalHelper.MAX_RADIUS_MAPMENU) {
                        Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.radius_to_long), Toast.LENGTH_SHORT).show();
                    } else {
                        ((TphInputFragment) fragment).setTph(tph);
                        ((TphInputFragment) fragment).tphHelper.listrefrence.dismiss();
                    }
                }
            }
        });

        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (Activity_From) {
                    case Activity_From_TphActivity:
                        Fragment fragment = (Fragment) activity.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        if(fragment instanceof TphInputFragment){
                            ((TphInputFragment) fragment).tphHelper.callout.hide();
                            ((TphInputFragment) fragment).tphHelper.showValueForm(tph);
                        }
                        break;
                    case Activity_From_MapActivity:
                        ((MapActivity) context).callout.hide();
                        ((MapActivity) context).startLongOperation(MapActivity.LongOperation_Setup_FormTPH,new TPHnLangsiran(tph.getNoTph(),tph));
                        //hapus
//                        ((MapActivity) context).showLongOperation(new TPHnLangsiran(tph.getNoTph(),tph),String.valueOf(MapActivity.LongOperation_Setup_FormTPH));
                        break;
                }
            }
        });

        btnDelet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (Activity_From) {
                    case Activity_From_TphActivity:
                    // function hanya unutk fragment TphNewInputFragment saja
                        Fragment fragment = (Fragment) activity.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        if(fragment instanceof TphInputFragment) {
                            double distance = GlobalHelper.distance(((TphInputFragment) fragment).tphHelper.latawal, tph.getLatitude(), ((TphInputFragment) fragment).tphHelper.longawal, tph.getLongitude());
                            if (distance > GlobalHelper.MAX_RADIUS_MAPMENU) {
                                Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.radius_to_long),Toast.LENGTH_SHORT).show();
                            } else {
                                AlertDialog alertDialog = new AlertDialog.Builder(activity,
                                        R.style.MyAlertDialogStyle)
                                        .setTitle(activity.getResources().getString(R.string.dg_warning_header))
                                        .setMessage(activity.getResources().getString(R.string.warning_deleted_tph)
                                        )
                                        .setPositiveButton(activity.getResources().getString(R.string.dg_confirm_ya), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();

                                                ((TphInputFragment) fragment).tphHelper.callout.hide();
                                                tph.setUpdateDate(System.currentTimeMillis());
                                                tph.setUpdateBy(Integer.parseInt(GlobalHelper.getUser().getUserID()));
                                                tph.setStatus(TPH.STATUS_TPH_INACTIVE_QC_MOBILE);
                                                if (((TphInputFragment) fragment).tphHelper.saveTPH(tph,false)) {
                                                    ((TphInputFragment) fragment).tphHelper.pointTPHSetup(tph.getEstCode(),tph.getBlock());
                                                }
                                            }
                                        })
                                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        }).create();
                                alertDialog.show();
                            }
                        }
                    break;
                    case Activity_From_MapActivity:
                        if(((BaseActivity) context).currentLocationOK()) {
                            double distance = GlobalHelper.distance(((BaseActivity) context).currentlocation.getLatitude(), tph.getLatitude(),
                                    ((BaseActivity) context).currentlocation.getLongitude(), tph.getLongitude());
                            if (distance > GlobalHelper.MAX_RADIUS_MAPMENU) {
                                Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.radius_to_long), Toast.LENGTH_SHORT).show();
                            } else {
                                AlertDialog alertDialog = new AlertDialog.Builder(activity,
                                        R.style.MyAlertDialogStyle)
                                        .setTitle(activity.getResources().getString(R.string.dg_warning_header))
                                        .setMessage(activity.getResources().getString(R.string.warning_deleted_tph))
                                        .setPositiveButton(activity.getResources().getString(R.string.dg_confirm_ya), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();

                                                ((MapActivity) context).callout.hide();
                                                boolean today = false;
                                                if (tph.getStatus() == TPH.STATUS_TPH_BELUM_UPLOAD) {
                                                    today = true;
                                                }

                                                tph.setUpdateDate(System.currentTimeMillis());
                                                tph.setUpdateBy(Integer.parseInt(GlobalHelper.getUser().getUserID()));
                                                tph.setStatus(TPH.STATUS_TPH_INACTIVE_QC_MOBILE);

                                                if (((MapActivity) context).tphHelper.saveTPH(tph, false)) {
                                                    //jika berhasil langsung update variabel yang ada di map main
                                                    int idgrap = ((MapActivity) context).getIdGrapSelected(new TPHnLangsiran(tph.getNoTph(), tph));
                                                    ((MapActivity) context).graphicsLayerTPH.removeGraphic(idgrap);
                                                    ((MapActivity) context).graphicsLayerText.removeGraphic(
                                                            ((MapActivity) context).hashText.get(idgrap)
                                                    );
                                                    Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.dg_message_delete_success_tph), Toast.LENGTH_SHORT).show();
                                                    ((MapActivity) context).countTph--;

                                                    if (today) {
                                                        ((MapActivity) context).graphicsLayerToday.removeGraphic(
                                                                ((MapActivity) context).hashpointToday.get(idgrap)
                                                        );
                                                        ((MapActivity) context).countHariIni--;
                                                    }
                                                    ((MapActivity) context).updateCounterKet();
                                                }
                                            }
                                        })
                                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        }).create();
                                alertDialog.show();
                            }
                        }
                        break;
                }
            }
        });
    }

    private static void initBlockDetailLangsiran(final ViewGroup retView, final Langsiran langsiran) {
        tvheader = (TextView) retView.findViewById(R.id.tv_header_iw);
        tvket1_iw = (TextView) retView.findViewById(R.id.tvket1_iw);
//        tvket2_iw = (TextView) retView.findViewById(R.id.tvket2_iw);
//        tvket3_iw = (TextView) retView.findViewById(R.id.tvket3_iw);
//        tvket4_iw = (TextView) retView.findViewById(R.id.tvket4_iw);

        ivPhoto = (ImageView) retView.findViewById(R.id.ivPhoto);
        ic_eye = (ImageView) retView.findViewById(R.id.ic_eye);
        sliderLayout = (SliderLayout) retView.findViewById(R.id.sliderLayout);
        btnChose = (Button) retView.findViewById(R.id.btnChose_iw);
        btnDetail = (Button) retView.findViewById(R.id.btnDetail_iw);
        btnDelet = (Button) retView.findViewById(R.id.btnDelet_iw);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy");
        if(context instanceof MapActivity){
            activity = (MapActivity) context;
            Activity_From = Activity_From_MapActivity;
            btnChose.setVisibility(View.GONE);
        }else if(context instanceof TphActivity) {
            activity = (TphActivity) context;
            Activity_From = Activity_From_TphActivity;
            btnChose.setVisibility(View.VISIBLE);
        }

        btnChose.setText(activity.getResources().getString(R.string.chose_langsiran));
        tvheader.setText(langsiran.getNamaTujuan());
        String ket1 = "Tipe Langsiran : " + Langsiran.LIST_TIPE_LANGSIRAN[langsiran.getTypeTujuan()] + "\n";
        ket1 = ket1 + activity.getResources().getString(R.string.create_time)+" : "+ sdf.format(new Date(langsiran.getCreateDate()));
        tvket1_iw.setText(ket1);
//        tvket2_iw.setVisibility(View.GONE);
//        tvket3_iw.setVisibility(View.GONE);
//        tvket4_iw.setVisibility(View.GONE);

        ArrayList<File> files = new ArrayList<>();
        if(langsiran.getFoto() != null) {
            if (langsiran.getFoto().size() > 0) {
                for (String s : langsiran.getFoto()) {
                    File file = new File(s);
                    files.add(file);
                }
            }
        }
        setupSlideshow(files);

        btnChose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = (Fragment) activity.getSupportFragmentManager().findFragmentById(R.id.content_container);
                if(fragment instanceof AngkutInputFragment){
                    ((AngkutInputFragment) fragment).setLangsiran(langsiran);
                    ((AngkutInputFragment) fragment).tujuanHelper.listrefrence.dismiss();
                }
            }
        });

        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (Activity_From){
                    case Activity_From_TphActivity:
                        Fragment fragment = (Fragment) activity.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        if(fragment instanceof AngkutInputFragment){
                            ((AngkutInputFragment) fragment).tujuanHelper.callout.hide();
                            ((AngkutInputFragment) fragment).tujuanHelper.showValueForm(langsiran);
                        }
                        break;
                    case Activity_From_MapActivity:
                        ((MapActivity) context).callout.hide();
                        ((MapActivity) context).startLongOperation(MapActivity.LongOperation_Setup_FormLangsiran,new TPHnLangsiran(langsiran.getIdTujuan(),langsiran));
                        //hapus
//                        ((MapActivity) context).showLongOperation(new TPHnLangsiran(langsiran.getIdTujuan(),langsiran),String.valueOf(MapActivity.LongOperation_Setup_FormLangsiran));
                        break;
                }

            }
        });

        btnDelet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (Activity_From) {
                    case Activity_From_TphActivity:
                        // function hanya unutk fragment TphNewInputFragment saja
                        Fragment fragment = (Fragment) activity.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        if (fragment instanceof AngkutInputFragment) {
                            double distance = GlobalHelper.distance(((AngkutInputFragment) fragment).tujuanHelper.latawal,
                                    langsiran.getLatitude(),
                                    ((AngkutInputFragment) fragment).tujuanHelper.longawal,
                                    langsiran.getLongitude());
                            if (distance > GlobalHelper.MAX_RADIUS_MAPMENU) {
                                Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.radius_to_long), Toast.LENGTH_SHORT).show();
                            } else {
                                AlertDialog alertDialog = new AlertDialog.Builder(activity,
                                        R.style.MyAlertDialogStyle)
                                        .setTitle(activity.getResources().getString(R.string.dg_warning_header))
                                        .setMessage(activity.getResources().getString(R.string.warning_deleted_langsiran)
                                        )
                                        .setPositiveButton(activity.getResources().getString(R.string.dg_confirm_ya), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();

                                                ((AngkutInputFragment) fragment).tujuanHelper.callout.hide();
                                                langsiran.setUpdateDate(System.currentTimeMillis());
                                                langsiran.setUpdateBy(GlobalHelper.getUser().getUserID());
                                                langsiran.setStatus(Langsiran.STATUS_LANGSIRAN_INACTIVE_QC_MOBILE);

                                                if (((AngkutInputFragment) fragment).tujuanHelper.saveTujuan(langsiran, false)) {
                                                    ((AngkutInputFragment) fragment).tujuanHelper.pointLangsiranSetup();
                                                }
                                            }
                                        })
                                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        }).create();
                                alertDialog.show();
                            }
                        }
                        break;
                    case Activity_From_MapActivity:
                        double distance = GlobalHelper.distance(((BaseActivity) context).currentlocation.getLatitude(), langsiran.getLatitude(),
                                ((BaseActivity) context).currentlocation.getLongitude(), langsiran.getLongitude());
                        if (distance > GlobalHelper.MAX_RADIUS_MAPMENU) {
                            Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.radius_to_long), Toast.LENGTH_SHORT).show();
                        } else {
                            AlertDialog alertDialog = new AlertDialog.Builder(activity,
                                    R.style.MyAlertDialogStyle)
                                    .setTitle(activity.getResources().getString(R.string.dg_warning_header))
                                    .setMessage(activity.getResources().getString(R.string.warning_deleted_langsiran)
                                    )
                                    .setPositiveButton(activity.getResources().getString(R.string.dg_confirm_ya), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();

                                            ((MapActivity) context).callout.hide();
                                            boolean today = false;
                                            if(langsiran.getStatus() == Langsiran.STATUS_LANGSIRAN_BELUM_UPLOAD){
                                                today = true;
                                            }

                                            langsiran.setUpdateDate(System.currentTimeMillis());
                                            langsiran.setUpdateBy(String.valueOf(Integer.parseInt(GlobalHelper.getUser().getUserID())));
                                            langsiran.setStatus(Langsiran.STATUS_LANGSIRAN_INACTIVE_QC_MOBILE);

                                            if (((MapActivity) context).tujuanHelper.saveTujuan(langsiran, false)) {
                                                //jika berhasil langsung update variabel yang ada di map main
                                                int idgrap  = ((MapActivity) context).getIdGrapSelected(new TPHnLangsiran(langsiran.getIdTujuan(), langsiran));
                                                ((MapActivity) context).graphicsLayerLangsiran.removeGraphic(idgrap);
                                                ((MapActivity) context).graphicsLayerText.removeGraphic(
                                                        ((MapActivity) context).hashText.get(idgrap)
                                                );
                                                Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.dg_message_delete_success_inst2),Toast.LENGTH_SHORT).show();
                                                ((MapActivity) context).countLangsiran--;

                                                if(today){
                                                    ((MapActivity) context).graphicsLayerToday.removeGraphic(
                                                            ((MapActivity) context).hashpointToday.get(idgrap)
                                                    );
                                                    ((MapActivity) context).countHariIni--;
                                                }

                                                ((MapActivity) context).updateCounterKet();
                                            }
                                        }
                                    })
                                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).create();
                            alertDialog.show();
                        }
                        break;
                }
            }
        });
    }

    private static void initBlockDetailPks(final ViewGroup retView, final PKS pks) {
        final AngkutActivity activity = (AngkutActivity) context;

        tvheader = (TextView) retView.findViewById(R.id.tv_header_iw);
        tvket1_iw = (TextView) retView.findViewById(R.id.tvket1_iw);
//        tvket2_iw = (TextView) retView.findViewById(R.id.tvket2_iw);
//        tvket3_iw = (TextView) retView.findViewById(R.id.tvket3_iw);
//        tvket4_iw = (TextView) retView.findViewById(R.id.tvket4_iw);

        ic_eye = (ImageView) retView.findViewById(R.id.ic_eye);
        ll_ipf_takepicture = (LinearLayout) retView.findViewById(R.id.ll_ipf_takepicture);
        btnChose = (Button) retView.findViewById(R.id.btnChose_iw);
        btnDetail = (Button) retView.findViewById(R.id.btnDetail_iw);
        btnDelet = (Button) retView.findViewById(R.id.btnDelet_iw);
        btnChose.setText(activity.getResources().getString(R.string.chose_pks));

        btnDetail.setVisibility(View.GONE);
        btnDelet.setVisibility(View.GONE);
        ic_eye.setVisibility(View.GONE);
        ll_ipf_takepicture.setVisibility(View.GONE);
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy");

        tvheader.setText(pks.getNamaPKS());
        String ket1 = "Block : " + pks.getBlock() + "\n" + "Afdeling : "+ pks.getAfdeling() ;
        tvket1_iw.setText(ket1);
//        tvket2_iw.setVisibility(View.GONE);
//        tvket3_iw.setVisibility(View.GONE);
//        tvket4_iw.setVisibility(View.GONE);


        btnChose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = (Fragment) activity.getSupportFragmentManager().findFragmentById(R.id.content_container);
                if(fragment instanceof AngkutInputFragment){
                    ((AngkutInputFragment) fragment).setPks(pks);
                    ((AngkutInputFragment) fragment).pksHelper.listrefrence.dismiss();
                }
            }
        });
    }

    private static void setupSlideshow(final List<File> ALselectedImage){
        try {
            final Activity activity = (Activity) context;
            Set<File> sf = new HashSet<>();
            sf.addAll(ALselectedImage);
            ALselectedImage.clear();
            ALselectedImage.addAll(sf);

            if (ALselectedImage.size() != 0) {
                sliderLayout.setVisibility(View.VISIBLE);
                ic_eye.setVisibility(View.VISIBLE);
                ivPhoto.setVisibility(View.GONE);
            } else {
                sliderLayout.setVisibility(View.GONE);
                ic_eye.setVisibility(View.GONE);
                ivPhoto.setVisibility(View.VISIBLE);
            }
            sliderLayout.removeAllSliders();
            for (File file : ALselectedImage) {
                TextSliderView textSliderView = new TextSliderView(activity);
                // initialize a SliderLayout
                if (file.toString().startsWith("http:")) {
                    String surl = file.toString();
                    if (file.toString().startsWith("http://")) {

                    } else if (file.toString().startsWith("http:/")) {
                        surl = surl.replace("http:/", "http://");
                    }

                    textSliderView
                            .image(GlobalHelper.setUrlFoto(surl))
                            .setScaleType(BaseSliderView.ScaleType.Fit);
                } else {
                    textSliderView
                            .image(file)
                            .setScaleType(BaseSliderView.ScaleType.Fit);
                }
                sliderLayout.addSlider(textSliderView);
            }
            sliderLayout.stopAutoCycle();
            sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
            sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            sliderLayout.setCustomAnimation(new DescriptionAnimation());
            ic_eye.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    WidgetHelper.showImagesZoom(activity,ALselectedImage);
                    //MainActivity.showZoom(ALselectedImage.get(sliderLayout.getCurrentPosition()).toString(),v,activity);
                }
            });
        }catch (Exception e) {
            Log.e("sliderLayout", e.toString());
        }
    }
}

