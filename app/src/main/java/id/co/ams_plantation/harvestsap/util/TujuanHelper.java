package id.co.ams_plantation.harvestsap.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.esri.android.map.Callout;
import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.LocationDisplayManager;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISLocalTiledLayer;
import com.esri.android.map.event.OnPanListener;
import com.esri.android.map.event.OnSingleTapListener;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.android.map.event.OnZoomListener;
import com.esri.android.map.ogc.kml.KmlLayer;
import com.esri.android.runtime.ArcGISRuntime;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Graphic;
import com.esri.core.runtime.LicenseLevel;
import com.esri.core.runtime.LicenseResult;
import com.esri.core.symbol.CompositeSymbol;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.utils.Utils;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestsap.Fragment.AngkutInputFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.Estate;
import id.co.ams_plantation.harvestsap.model.Langsiran;
import id.co.ams_plantation.harvestsap.model.StampImage;
import id.co.ams_plantation.harvestsap.model.TPHnLangsiran;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.ui.AngkutActivity;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.MapActivity;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by user on 1/10/2019.
 */

public class TujuanHelper {
    public AlertDialog listrefrence;
    FragmentActivity activity;

    SlidingUpPanelLayout mLayout;
    MapView map;
    LocationDisplayManager locationDisplayManager;
    FloatingActionButton fabZoomToLocation;
    FloatingActionButton fabZoomToEstate;
    SegmentedButtonGroup segmentedButtonGroup;
//    RelativeLayout layoutBottomSheet;
    ImageView ivCrosshair;
    LinearLayout ll_latlon;
    TextView tv_lat_manual;
    TextView tv_lon_manual;
    TextView tv_dis_manual;
    TextView tv_deg_manual;

    ImageView ivPhoto;
    ImageView imagecacnel;
    ImageView imagesview;
    RelativeLayout rl_ipf_takepicture;
    SliderLayout sliderLayout;

    LinearLayout lheader;
    LinearLayout btnShowForm;
    AppCompatEditText etNamaTransit;
    BetterSpinner etLangsiran;
    EditText etEst;
    CompleteTextViewHelper etBlock;
    CompleteTextViewHelper etAfdeling;
    Button btnSave;
    Button btnCancel;
//    BottomSheetBehavior sheetBehavior;
    View tph_new;
    View pks_new;

    GraphicsLayer graphicsLayer;
    GraphicsLayer graphicsLayerTPH;
    SpatialReference spatialReference;
    boolean isMapLoaded=false;

    ArrayList<File> ALselectedImage;

    ArcGISLocalTiledLayer tiledLayer;
    KmlLayer kmlLayer;
    String locKmlMap;

    BaseActivity baseActivity;
    AngkutActivity angkutActivity;
    MapActivity mapActivity;
    int idgraptemp = 0;
    boolean manual;
    boolean newData;

    public double latawal = 0.0;
    public double longawal = 0.0;
    public double latitude = 0.0;
    public double longitude = 0.0;

    public HashMap<Integer,Langsiran> hashpoint;
    public Callout callout;

    Langsiran selectedTujuan;
    boolean isNewData;

    public TujuanHelper(FragmentActivity activity) {
        this.activity = activity;
    }

    public void setupFormInput(View view,Langsiran langsiran){
        mapActivity = (MapActivity) activity;
        locKmlMap = mapActivity.locKmlMap;
        ALselectedImage= new ArrayList<>();
        selectedTujuan = langsiran;
        if(ivPhoto == null) {
            etNamaTransit = (AppCompatEditText) view.findViewById(R.id.etNamaTransit);
            etLangsiran = (BetterSpinner) view.findViewById(R.id.etLangsiran);
            etEst = (EditText) view.findViewById(R.id.etEstLangsiran);
            etBlock = (CompleteTextViewHelper) view.findViewById(R.id.etBlockLangsiran);
            etAfdeling = (CompleteTextViewHelper) view.findViewById(R.id.etAfdelingLangsiran);
            btnSave = (Button) view.findViewById(R.id.btnSaveLangsiran);

            ivPhoto = (ImageView) view.findViewById(R.id.ivPhotoLangsiran);
            imagecacnel = (ImageView) view.findViewById(R.id.imagecacnelLangsiran);
            imagesview = (ImageView) view.findViewById(R.id.imagesviewLangsiran);
            rl_ipf_takepicture = (RelativeLayout) view.findViewById(R.id.rl_ipf_takepictureLangsiran);
            sliderLayout = (SliderLayout) view.findViewById(R.id.sliderLayoutLangsiran);
        }else{
            if(langsiran == null) {
                setupimageslide();
                etNamaTransit.setText("");
                etLangsiran.setText(Langsiran.LIST_TIPE_LANGSIRAN[0]);
            }

        }

        ArrayAdapter<String> adapterLangsiran = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line,Langsiran.LIST_TIPE_LANGSIRAN);
        etLangsiran.setAdapter(adapterLangsiran);
        etLangsiran.setText(Langsiran.LIST_TIPE_LANGSIRAN[0]);

        btnSave.setVisibility(View.VISIBLE);
        if(langsiran != null) {
            isNewData = false;
            showValueForm(langsiran);
            if(GlobalHelper.distance(((BaseActivity) activity).currentlocation.getLatitude(),latitude,
                    ((BaseActivity) activity).currentlocation.getLongitude(),longitude) > GlobalHelper.RADIUS){
                btnSave.setVisibility(View.GONE);
            }
        }else{
            isNewData = true;
            Estate estate = langsiran == null ? GlobalHelper.getEstate() : GlobalHelper.getEstateByEstCode(langsiran.getEstCode());
            etEst.setText(estate.getEstName());
        }

        etBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etBlock.showDropDown();
            }
        });
        etBlock.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    etBlock.showDropDown();
                }
            }
        });

        etBlock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                etAfdeling.requestFocus();
            }
        });

        etAfdeling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etAfdeling.showDropDown();
            }
        });
        etAfdeling.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    etBlock.showDropDown();
                }
            }
        });
        etAfdeling.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(ALselectedImage.size() == 0){
                    GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_NEW_TPH_MAP_ACTIVITY;
                    EasyImage.openCamera(activity,1);
                }
                GlobalHelper.hideKeyboard(activity);
            }
        });

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ALselectedImage.size() > 3) {
                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.warning_take_foto),Toast.LENGTH_LONG).show();
                }else {
                    if(etBlock.getText().toString().isEmpty()){
                        etBlock.requestFocus();
                        Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.please_chose_block),Toast.LENGTH_LONG).show();
                        return;
                    }

                    StampImage stampImage = new StampImage();
                    stampImage.setBlock(etBlock.getText().toString());

                    Gson gson  = new Gson();
                    SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(HarvestApp.STAMP_IMAGES,gson.toJson(stampImage));
                    editor.apply();

                    GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_NEW_LANGSIRAN_MAP_ACTIVITY;
                    EasyImage.openCamera(activity,1);
                }
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
                String stringIsi = preferences.getString(HarvestApp.STAMP_IMAGES,null);
                Gson gson  = new Gson();
                StampImage stampImage = gson.fromJson(stringIsi,StampImage.class);
                if(!stampImage.getBlock().equals(etBlock.getText().toString())){
                    stampImage.setBlock(etBlock.getText().toString());
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(HarvestApp.STAMP_IMAGES,gson.toJson(stampImage));
                    editor.apply();

                    GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_NEW_LANGSIRAN_MAP_ACTIVITY;
                    EasyImage.openCamera(activity,1);
                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.warning_take_foto2),Toast.LENGTH_LONG).show();
                    return;
                }

                mapActivity.mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                new LongOperation().execute();
            }
        });
    }

    public void showMapTujuan(){
//        awal = true;
        idgraptemp = 0;
        manual = false;

        if(activity instanceof AngkutActivity) {
            angkutActivity = ((AngkutActivity) activity);
            if (((AngkutActivity) activity).searchingGPS != null) {
                ((AngkutActivity) activity).searchingGPS.dismiss();
            }
        }

        View view = LayoutInflater.from(activity).inflate(R.layout.tph_new,null);
        LicenseResult licenseResult = ArcGISRuntime.setClientId("uFfCzKhpVfi0ggjz");
        LicenseLevel licenseLevel = ArcGISRuntime.License.getLicenseLevel();

        mLayout = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);
        map = (MapView) view.findViewById(R.id.map);
        fabZoomToLocation = (FloatingActionButton) view.findViewById(R.id.fabZoomToLocation);
        fabZoomToEstate = (FloatingActionButton) view.findViewById(R.id.fabZoomToEstate);
        segmentedButtonGroup = (SegmentedButtonGroup) view.findViewById(R.id.segmentedButtonGroup);
//        layoutBottomSheet = (RelativeLayout) view.findViewById(R.id.bottom_sheet);
        btnShowForm = (LinearLayout) view.findViewById(R.id.btnShowForm);

        tph_new = (View) view.findViewById(R.id.tph_new);
        pks_new = (View) view.findViewById(R.id.pks_new);

        ll_latlon = (LinearLayout) view.findViewById(R.id.ll_latlon);
        ivCrosshair = (ImageView) view.findViewById(R.id.iv_crosshair);
        tv_lat_manual = (TextView) view.findViewById(R.id.tv_lat_manual);
        tv_lon_manual = (TextView) view.findViewById(R.id.tv_lon_manual);
        tv_dis_manual = (TextView) view.findViewById(R.id.tv_dis_manual);
        tv_deg_manual = (TextView) view.findViewById(R.id.tv_deg_manual);

        etNamaTransit = (AppCompatEditText) view.findViewById(R.id.etNamaTransit);
        etLangsiran = (BetterSpinner) view.findViewById(R.id.etLangsiran);
        etEst = (EditText) view.findViewById(R.id.etEstLangsiran);
        etBlock = (CompleteTextViewHelper) view.findViewById(R.id.etBlockLangsiran);
        etAfdeling = (CompleteTextViewHelper) view.findViewById(R.id.etAfdelingLangsiran);
        btnSave = (Button) view.findViewById(R.id.btnSaveLangsiran);

        ivPhoto = (ImageView) view.findViewById(R.id.ivPhotoLangsiran);
        imagecacnel = (ImageView) view.findViewById(R.id.imagecacnelLangsiran);
        imagesview = (ImageView) view.findViewById(R.id.imagesviewLangsiran);
        rl_ipf_takepicture = (RelativeLayout) view.findViewById(R.id.rl_ipf_takepictureLangsiran);
        sliderLayout = (SliderLayout) view.findViewById(R.id.sliderLayoutLangsiran);

        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.i("TPH helper", "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                if(manual && newState ==  SlidingUpPanelLayout.PanelState.EXPANDED){
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.please_stop_manual_point),Toast.LENGTH_SHORT).show();
                }
                Log.i("TPH helper", "onPanelStateChanged " + newState);
            }
        });
        mLayout.setFadeOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });

        tph_new.setVisibility(View.GONE);
        pks_new.setVisibility(View.GONE);

        graphicsLayer = new GraphicsLayer();
        graphicsLayerTPH = new GraphicsLayer();
        baseActivity = (BaseActivity) activity;
        ALselectedImage = new ArrayList<>();

        latawal = baseActivity.currentlocation.getLatitude();
        longawal = baseActivity.currentlocation.getLongitude();
        latitude = baseActivity.currentlocation.getLatitude();
        longitude = baseActivity.currentlocation.getLongitude();

//        List<String> listableObjects = getListEstate(GlobalHelper.getUser(),angkutActivity.estate);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
//                android.R.layout.simple_dropdown_item_1line, listableObjects);
//        etEst.setAdapter(adapter);
        etEst.setText(angkutActivity.estate.getEstName());

        ArrayAdapter<String> adapterLangsiran = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line,Langsiran.LIST_TIPE_LANGSIRAN);
        etLangsiran.setAdapter(adapterLangsiran);
        etLangsiran.setText(Langsiran.LIST_TIPE_LANGSIRAN[0]);

        fabZoomToEstate.setImageDrawable(new
                IconicsDrawable(activity)
                .icon(MaterialDesignIconic.Icon.gmi_landscape)
                .sizeDp(24)
                .colorRes(R.color.White));

        fabZoomToLocation.setImageDrawable(new
                IconicsDrawable(activity)
                .icon(MaterialDesignIconic.Icon.gmi_pin)
                .sizeDp(24)
                .colorRes(R.color.White));

        mapSetup();
        switchCrossHairUI(false);
        fabZoomToEstate.callOnClick();
//        setupBlokAfdeling(latawal,longawal);

        fabZoomToEstate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(map.isLoaded()){
                    if(tiledLayer!=null){
                        map.setExtent(tiledLayer.getFullExtent());
                    }
                    map.invalidate();
                    if(idgraptemp == 0) {
                        idgraptemp = addGrapTemp(latawal, longawal, idgraptemp);
                        pointLangsiranSetup();

                        fabZoomToLocation.callOnClick();
                    }

                    map.setOnSingleTapListener(new OnSingleTapListener() {
                        @Override
                        public void onSingleTap(float v, float v1) {
                            if(map.isLoaded()){
                                defineTapSetup(v,v1);
                            }
                        }
                    });
                }
            }
        });

        fabZoomToLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (angkutActivity.currentLocationOK()) {
                    latawal = angkutActivity.currentlocation.getLatitude();
                    longawal = angkutActivity.currentlocation.getLongitude();
                    latitude = latawal;
                    longitude = longawal;
                    zoomToLocation(latawal,longawal,spatialReference);
                    idgraptemp = addGrapTemp(latawal,longawal,idgraptemp);
                }else{
//                    angkutActivity.searchingGPS = Snackbar.make(angkutActivity.myCoordinatorLayout,activity.getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
//                    angkutActivity.searchingGPS.show();
                    WidgetHelper.warningFindGps(angkutActivity,angkutActivity.myCoordinatorLayout);
                }
            }
        });

        segmentedButtonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(int position) {
                switch (position){
                    case 0:{
                        manual = false;
                        switchCrossHairUI(false);
                        idgraptemp = addGrapTemp(latitude,longitude,idgraptemp);
                        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
                        break;
                    }
                    case 1:{
                        manual = true;
//                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        setDefaultManual();
                        break;
                    }
                }
            }
        });

//        btnShowForm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(manual){
//                    Toast.makeText(HarvestApp.getContext(),"Mohon Matikan Dahulu Manual Point",Toast.LENGTH_SHORT).show();
//                }else{showMapTujuan();}
//            }
//        });

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ALselectedImage.size() > 3) {
                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.warning_take_foto),Toast.LENGTH_LONG).show();
                }else {
                    if(etBlock.getText().toString().isEmpty()){
                        etBlock.requestFocus();
                        Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.please_chose_block),Toast.LENGTH_LONG).show();
                        return;
                    }

                    StampImage stampImage = new StampImage();
                    stampImage.setBlock(etBlock.getText().toString());

                    Gson gson  = new Gson();
                    SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(HarvestApp.STAMP_IMAGES,gson.toJson(stampImage));
                    editor.apply();

                    GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_NEW_LANGSIRAN;
                    EasyImage.openCamera(activity,1);
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new LongOperation().execute();
            }
        });

        if(selectedTujuan != null){
            showValueForm(selectedTujuan);
        }
//        lheader.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (!manual) {
//                    switch (sheetBehavior.getState()) {
//                        case BottomSheetBehavior.STATE_COLLAPSED:
//                            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                            break;
//                        case BottomSheetBehavior.STATE_EXPANDED:
//                            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                            break;
//                    }
//                }else{
//                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.please_stop_manual_point),Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

//        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
//        /**
//         * bottom sheet state change listener
//         * we are changing button text when sheet changed state
//         * */
//        // set the peek height
////        sheetBehavior.setPeekHeight(190);
//        sheetBehavior.setHideable(false);
//        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                if (manual) {
//                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.please_stop_manual_point),Toast.LENGTH_SHORT).show();
//                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                } else {
//                    switch (newState) {
//                        case BottomSheetBehavior.STATE_HIDDEN:
//                            break;
//                        case BottomSheetBehavior.STATE_EXPANDED:
//                            fabZoomToEstate.hide();
//                            fabZoomToLocation.hide();
//                            segmentedButtonGroup.setVisibility(View.GONE);
//                            Log.v("sheet", "STATE_EXPANDED");
//                            break;
//                        case BottomSheetBehavior.STATE_COLLAPSED:
//                            fabZoomToEstate.show();
//                            fabZoomToLocation.show();
//                            segmentedButtonGroup.setVisibility(View.VISIBLE);
//                            Log.v("sheet", "STATE_COLLAPSED");
//                            break;
//                        case BottomSheetBehavior.STATE_DRAGGING:
//                            break;
//                        case BottomSheetBehavior.STATE_SETTLING:
//                            break;
//                    }
//                }
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//
//            }
//        });


        listrefrence = WidgetHelper.showFormDialog(listrefrence,view,activity);
    }

    private boolean validasi(){
        HashSet<String> sFoto = new HashSet<>();
        if(ALselectedImage.size() > 0){
            for(File file : ALselectedImage){
                sFoto.add(file.getAbsolutePath());
            }
        }

        String estCode = "";
        for(Estate estate : GlobalHelper.getUser().getEstates()){
            if(estate.getEstName().equalsIgnoreCase(etEst.getText().toString())){
                estCode = estate.getEstCode();
            }
        }

        int tipeLangsiran = 0;
        for(int i = 0; i < Langsiran.LIST_TIPE_LANGSIRAN.length; i++){
            if(etLangsiran.getText().toString().equals(Langsiran.LIST_TIPE_LANGSIRAN[i])){
                tipeLangsiran = i;
                break;
            }
        }

        String idTujuan;
        newData = false;
        if(selectedTujuan == null){
            newData = true;
            SimpleDateFormat sdfD = new SimpleDateFormat("ddMMyy");
            idTujuan = "L"+String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_LANGSIRAN))
                        + sdfD.format(System.currentTimeMillis())
                        + GlobalHelper.getUser().getUserID();
        }else{
            idTujuan = selectedTujuan.getIdTujuan();
        }

        Langsiran langsiran = new Langsiran(idTujuan,
                etNamaTransit.getText().toString(),
                tipeLangsiran,
                estCode,
                etAfdeling.getText().toString(),
                etBlock.getText().toString(),
                latitude,
                longitude,
                System.currentTimeMillis(),
                GlobalHelper.getUser().getUserID(),
                System.currentTimeMillis(),
                GlobalHelper.getUser().getUserID(),
                Langsiran.STATUS_LANGSIRAN_BELUM_UPLOAD,
                sFoto) ;

        if(selectedTujuan != null){
            langsiran.setUpdateBy(GlobalHelper.getUser().getUserID());
            langsiran.setUpdateDate(System.currentTimeMillis());
        }

        if(langsiran.getNamaTujuan().isEmpty()){
            Toast.makeText(HarvestApp.getContext(),HarvestApp.getContext().getResources().getString(R.string.please_input_langsiran_name),Toast.LENGTH_SHORT).show();
            return false;

        }else if (langsiran.getEstCode().isEmpty()){
            Toast.makeText(HarvestApp.getContext(),HarvestApp.getContext().getResources().getString(R.string.please_chose_estate),Toast.LENGTH_SHORT).show();
            return false;
        }else if (langsiran.getFoto().size() == 0){
            Toast.makeText(HarvestApp.getContext(),HarvestApp.getContext().getResources().getString(R.string.please_take_picture),Toast.LENGTH_SHORT).show();
            return false;
        }else if (langsiran.getNamaTujuan().length() >= 21){
            Toast.makeText(HarvestApp.getContext(),"Nama Langsiran Terlalu Panjang",Toast.LENGTH_SHORT).show();
            return false;
        }else if (langsiran.getBlock().length() >= 11){
            Toast.makeText(HarvestApp.getContext(),"Blok Langsiran Terlalu Panjang",Toast.LENGTH_SHORT).show();
            return false;
        }else if (langsiran.getAfdeling().length() >= 11){
            Toast.makeText(HarvestApp.getContext(),"Afdeling Langsiran Terlalu Panjang",Toast.LENGTH_SHORT).show();
            return false;
        }

        selectedTujuan = langsiran;
        return true;
    }

    private boolean savePointLangsiran(){
        if(saveTujuan(selectedTujuan,newData)){
            if(mapActivity != null) {
                mapActivity.addGraphicLangsiran(new TPHnLangsiran(selectedTujuan.getIdTujuan(),selectedTujuan));
            }else {
                addGraphicLangsiran(selectedTujuan);
            }
            return true;
        }else{
            return false;
        }
    }

    public boolean saveTujuan(Langsiran langsiran, boolean insert) {
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_LANGSIRAN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(langsiran.getIdTujuan(),gson.toJson(langsiran));
        if(insert) {
            repository.insert(dataNitrit);
            GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_LANGSIRAN);
        }else{
            repository.update(dataNitrit);
        }
        db.close();
        selectedTujuan = langsiran;
        return true;
    }

//    public void setValueLangsiran(Langsiran tujuan){
//        selectedTujuan = tujuan;
//        showMapTujuan();
//    }

    public void showValueForm(Langsiran tujuan){

        if(mapActivity == null) {
            graphicsLayer.removeAll();
            switchCrossHairUI(false);
            zoomToLocation(latitude,longitude,spatialReference);
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
        }

        String est = "";
        for(Estate estate : GlobalHelper.getUser().getEstates()){
            if(estate.getEstCode().equalsIgnoreCase(tujuan.getEstCode())){
                est = estate.getEstName();
            }
        }

        etNamaTransit.setText(tujuan.getNamaTujuan());
        etEst.setText(est);
        etBlock.setText(tujuan.getBlock());
        etAfdeling.setText(tujuan.getAfdeling());
        etLangsiran.setText(Langsiran.LIST_TIPE_LANGSIRAN[tujuan.getTypeTujuan()]);

        etEst.setFocusable(false);
        etAfdeling.setFocusable(false);
        etBlock.setFocusable(false);

        longitude = tujuan.getLongitude();
        latitude = tujuan.getLatitude();
        ALselectedImage.clear();

        if(tujuan.getFoto() != null) {
            if (tujuan.getFoto().size() > 0) {
                for (String s : tujuan.getFoto()) {
                    File file = new File(s);
                    ALselectedImage.add(file);
                }
            }
        }
        setupimageslide();
        double radius = GlobalHelper.distance(latitude,latawal,longitude,longawal);
        if(radius > GlobalHelper.RADIUS){
            btnSave.setVisibility(View.GONE);
        }else{
            btnSave.setVisibility(View.VISIBLE);
        }
    }

    private void addGraphicLangsiran(Langsiran langsiran){
        Point fromPoint = new Point(langsiran.getLongitude(), langsiran.getLatitude());
        Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

        CompositeSymbol cms = new CompositeSymbol();
        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(activity.getResources().getColor(R.color.Blue), Utils.convertDpToPx(activity, GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.CIRCLE);
        SimpleLineSymbol sls = new SimpleLineSymbol(activity.getResources().getColor(R.color.Blue), GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        sms.setOutline(sls);
        cms.add(sms);
        Graphic pointGraphic = new Graphic(toPoint, cms);
        int idgrap = graphicsLayerTPH.addGraphic(pointGraphic);
        hashpoint.put(idgrap,langsiran);
    }

    private void defineTapSetup(float x,float y){
        List<Langsiran> langsiranList = new ArrayList<>();
        int[] graphicIDs = graphicsLayerTPH.getGraphicIDs(x,y,Utils.convertDpToPx(activity,GlobalHelper.MARKER_TOUCH_SIZE));

        if(graphicIDs.length > 0) {
            for(int gid : graphicIDs) {
                for (HashMap.Entry<Integer, Langsiran> entry : hashpoint.entrySet()) {
                    if(gid == entry.getKey()){
                        langsiranList.add(entry.getValue());
                    }
                }
            }
        }

        calloutSetup();
        if (callout.isShowing()) {
            callout.hide();
        } else {
            if (graphicIDs.length == 1) {
                //jika dalam satu tap hanya ada satu titik maka kesini
                int idx = -1;
                String graptype = "";
                Graphic graphic = graphicsLayerTPH.getGraphic(graphicIDs[0]);
                if (graphic.getGeometry() instanceof Point) {
                    graptype = "point";
                    idx = graphicIDs[0];
                }
                showInfoWindow(idx,langsiranList.get(0));
            }else if (graphicIDs.length > 1){
                showChoseInfoWindow(graphicIDs, graphicsLayerTPH, langsiranList);
            }
        }
    }

    private void showChoseInfoWindow(int[] graphicIDs,GraphicsLayer gl,List<Langsiran> langsiranList){
        ArrayList<Langsiran> windowChoseArrayList = new ArrayList<>();

        for (int i = 0; i < graphicIDs.length; i++) {
            Graphic graphic = gl.getGraphic(graphicIDs[i]);
            int idx =0;
            if (graphic.getGeometry() instanceof Point) {
                idx = graphicIDs[i];
            }

            if(idx != 0){
                for (HashMap.Entry<Integer, Langsiran> entry : hashpoint.entrySet()) {
                    if(idx == entry.getKey()){
                        windowChoseArrayList.add(entry.getValue());
                    }
                }
            }
        }

//        Collections.sort(windowChoseArrayList, new Comparator<TPH>() {
//            @Override
//            public int compare(Langsiran o1, Langsiran o2) {
//                return o1.getNamaTujuan().compareTo(o2.getNamaTujuan());
//            }
//        });

//        if(windowChoseArrayList.size()>1){
//            ViewGroup content = InfoWindowChose.setupInfoWindowsChose(activity, windowChoseArrayList);
//            callout.setContent(content);
//            Graphic graphic = gl.getGraphic(graphicIDs[graphicIDs.length -1]);
//            String graptype = "";
//            int idx =0;
//            if (graphic.getGeometry() instanceof Point) {
//                graptype = "point";
//                idx = graphicIDs[graphicIDs.length -1];
//            }
//
//            Point realpoin = (Point) gl.getGraphic(idx).getGeometry();
//
//            Point wgs84poin = (Point) GeometryEngine.project(realpoin,
//                    gl.getSpatialReference(),
//                    SpatialReference.create(SpatialReference.WKID_WGS84));
//            Point mapPoint = (Point) GeometryEngine.project(wgs84poin,
//                    SpatialReference.create(SpatialReference.WKID_WGS84),
//                    map.getSpatialReference());
//            callout.setCoordinates(mapPoint);
//            map.centerAt(mapPoint,true);
//            callout.animatedShow(mapPoint,content);
//        }else if (windowChoseArrayList.size() == 1){
            showInfoWindow(graphicIDs[0],windowChoseArrayList.get(0));
//        }
    }


    private void calloutSetup() {
        this.callout = map.getCallout();
        this.callout.setStyle(R.xml.statisticspop);
        if (this.callout.isShowing()) {
            this.callout.hide();
        }
    }

    public void showInfoWindow(int idx,Langsiran langsiran){
        ViewGroup content = InfoWindow.setupInfoWindowsLangsiran(activity, langsiran);
        callout.setContent(content);
        Point realpoin = (Point) graphicsLayerTPH.getGraphic(idx).getGeometry();

        if(realpoin!= null) {
            Point wgs84poin = (Point) GeometryEngine.project(realpoin,
                    graphicsLayerTPH.getSpatialReference(),
                    SpatialReference.create(SpatialReference.WKID_WGS84));
            Point mapPoint = (Point) GeometryEngine.project(wgs84poin,
                    SpatialReference.create(SpatialReference.WKID_WGS84),
                    map.getSpatialReference());
            callout.setCoordinates(mapPoint);
            map.centerAt(mapPoint,true);
            callout.animatedShow(mapPoint,content);
        }
    }

    public void setImages(File images,Context context){
//        Glide.with(context)
//                .load(images)
//                .asBitmap()
//                .error(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_camera_alt, null))
//                .centerCrop();
        ALselectedImage.add(images);
        setupimageslide();
    }

    private void setupimageslide(){
        Set<File> sf = new HashSet<>();
        sf.addAll(ALselectedImage);
        ALselectedImage.clear();
        ALselectedImage.addAll(sf);

        if(ALselectedImage.size() > 3){
            ivPhoto.setVisibility(View.GONE);
        }else{
            ivPhoto.setVisibility(View.VISIBLE);
        }
        if(ALselectedImage.size() != 0){
            imagecacnel.setVisibility(View.VISIBLE);
            imagesview.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.VISIBLE);
        }else{
            imagecacnel.setVisibility(View.GONE);
            imagesview.setVisibility(View.GONE);
            sliderLayout.setVisibility(View.GONE);
        }
        sliderLayout.removeAllSliders();
        for(File file : ALselectedImage){
            TextSliderView textSliderView = new TextSliderView(activity);
            // initialize a SliderLayout
            if(file.toString().startsWith("http:")){
                String surl = file.toString();
                if(file.toString().startsWith("http://")){

                }else if(file.toString().startsWith("http:/")){
                    surl =surl.replace("http:/","http://");
                }

                textSliderView
                        .image(GlobalHelper.setUrlFoto(surl))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }else {
                textSliderView
                        .image(file)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        imagecacnel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
                try {
                    if (sliderLayout.getCurrentPosition() == 0 && ALselectedImage.size() == 1) {
                        ALselectedImage.clear();
                        sliderLayout.removeAllSliders();
                        sliderLayout.setVisibility(View.GONE);
                        imagecacnel.setVisibility(View.GONE);
                        imagesview.setVisibility(View.GONE);
                    } else {
                        ALselectedImage.remove(sliderLayout.getCurrentPosition());
                        sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
                    }
                }catch (Exception e){
                    Log.e("sliderLayout", e.toString());
                }
            }
        });
        imagesview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WidgetHelper.showImagesZoom(activity,ALselectedImage);
            }
        });
    }

    private ArrayList<String> getListEstate(User user, Estate estate){
        ArrayList<String> estateArrayList = new ArrayList<>();
        for(Estate estate1:user.getEstates()) {
            if(estate1.getCompanyShortName().equalsIgnoreCase(estate.getCompanyShortName())){
                estateArrayList.add(estate1.getEstName());
            }
        }
        return estateArrayList;
    }

    private void mapSetup(){
        //
        map.removeAll();
        graphicsLayer.removeAll();
        String locTiledMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VKM);
        tiledLayer = new ArcGISLocalTiledLayer(locTiledMap,true);
        tiledLayer.setRenderNativeResolution(true);
        tiledLayer.setMinScale(250000.0d);
        tiledLayer.setMaxScale(1000.0d);
        map.addLayer(tiledLayer);

        locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);
        kmlLayer = new KmlLayer(locKmlMap);
        kmlLayer.setOpacity(0.3f);
        map.addLayer(kmlLayer);

        map.setOnStatusChangedListener(new OnStatusChangedListener() {
            @Override
            public void onStatusChanged(Object o, STATUS status) {
                if(o==map && status==STATUS.INITIALIZED){
                    spatialReference = map.getSpatialReference();
                    isMapLoaded = true;
                    fabZoomToEstate.callOnClick();
                }
            }
        });
        map.setOnPanListener(new OnPanListener() {
            @Override
            public void prePointerMove(float v, float v1, float v2, float v3) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Point prePoint = map.getCenter();
                        Point point = (Point) GeometryEngine.project(prePoint,map.getSpatialReference(), SpatialReference.create(SpatialReference.WKID_WGS84));
                    }
                }
            }

            @Override
            public void postPointerMove(float v, float v1, float v2, float v3) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Point prePoint = map.getCenter();
                        Point point = (Point) GeometryEngine.project(prePoint,map.getSpatialReference(),SpatialReference.create(SpatialReference.WKID_WGS84));
                        double imeter = GlobalHelper.distance(latawal,point.getY(),longawal,point.getX());
                        double bearing = GlobalHelper.bearing(latawal,point.getY(),longawal,point.getX());
                        tv_lat_manual.setText(String.format("%.5f",point.getY()));
                        tv_lon_manual.setText(String.format("%.5f",point.getX()));
                        tv_dis_manual.setText(String.format("%.0f m",imeter));
                        tv_deg_manual.setText(String.format("%.0f",bearing));
                        if(point.getY() != 0.0 && point.getX() != 0.0) {
                            if (imeter <= GlobalHelper.RADIUS) {
                                latitude = point.getY();
                                longitude = point.getX();
                                ll_latlon.setBackgroundColor(Color.WHITE);
                            } else {
                                ll_latlon.setBackgroundColor(Color.RED);
                            }
                        }
                    }

                }
            }

            @Override
            public void prePointerUp(float v, float v1, float v2, float v3) {

            }

            @Override
            public void postPointerUp(float v, float v1, float v2, float v3) {

            }
        });
        map.setOnZoomListener(new OnZoomListener() {
            @Override
            public void preAction(float v, float v1, double v2) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Log.e("zoom preAction",v+ " ; "+ v1+" ; "+v2);
                        Log.e("zoom scale",String.format("%.0f",map.getScale()));
                        int scale = (int) map.getScale();
                        int zoomlvl = GlobalHelper.getZoomlvl(map.getScale());
                        Log.v("zoom lvl",String.format(" 1 : %,dm Zl : %,d",scale,zoomlvl));
                        map.getSpatialReference();
                    }

                }
            }

            @Override
            public void postAction(float v, float v1, double v2) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Log.e("zoom postAction",v+ " ; "+ v1+" ; "+v2);
                        int scale = (int) map.getScale();
                        int zoomlvl = GlobalHelper.getZoomlvl(map.getScale());
                        Log.v("zoom lvl",String.format(" 1 : %,dm Zl : %,d",scale,zoomlvl));
                    }

                }
            }
        });

        graphicsLayer.setMinScale(70000d);
        graphicsLayer.setMaxScale(1000d);
        graphicsLayerTPH.setMinScale(70000d);
        graphicsLayerTPH.setMaxScale(1000d);
        map.addLayer(graphicsLayer);
        map.addLayer(graphicsLayerTPH);
        map.invalidate();
    }

    public void pointLangsiranSetup(){
        graphicsLayerTPH.removeAll();
        hashpoint = new HashMap<>();

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_LANGSIRAN);

        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> tphIterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = tphIterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            Langsiran langsiran = gson.fromJson(dataNitrit.getValueDataNitrit(),Langsiran.class);
//            double radius = GlobalHelper.distance(latawal,langsiran.getLatitude(),
//                    longawal,langsiran.getLongitude());
            if (langsiran.getStatus() != Langsiran.STATUS_LANGSIRAN_DELETED && langsiran.getStatus() != Langsiran.STATUS_LANGSIRAN_INACTIVE_QC_MOBILE) {
                addGraphicLangsiran(langsiran);
            }
        }

        db.close();
    }

    private int addGrapTemp(Double lat, Double lon, int idlama){
        if(map!=null && map.isLoaded()){
            Point fromPoint = new Point(lon, lat);
            Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

            CompositeSymbol cms = new CompositeSymbol();
            SimpleMarkerSymbol sms = new SimpleMarkerSymbol(Color.GREEN, Utils.convertDpToPx(activity, GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.X);
            SimpleLineSymbol sls = new SimpleLineSymbol(Color.GREEN, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
            sms.setOutline(sls);
            cms.add(sms);
            Graphic pointGraphic = new Graphic(toPoint, cms);
            setupBlokAfdeling(lat,lon);

//            try {
//                String [] blockAfdeling = GlobalHelper.getAndAssignBlock(new File(locKmlMap),latitude,longitude).split(";");
//                etAfdeling.setText(blockAfdeling[2]);
//                etBlock.setText(blockAfdeling[1]);
//            }catch (Exception e){
//                e.printStackTrace();
//            }
            if(idlama!= 0){
                removeGrapTemp(idlama);
            }

            return graphicsLayer.addGraphic(pointGraphic);
        }else{
            return 0;
        }
    }

    private void removeGrapTemp(int id){
        graphicsLayer.removeGraphic(id);
    }

    public void setupBlokAfdeling(Double latitude,Double longitude){
        Set<String> allBlockAfdeling = GlobalHelper.getNearBlock(new File(locKmlMap),latitude,longitude);

        Set<String> allBlock = new ArraySet<>();
        Set<String> allAfdeling = new ArraySet<>();

        for(String s : allBlockAfdeling){
            try {
                String [] split = s.split(";");
                if(split.length > 2) {
                    allAfdeling.add(split[1]);
                    allBlock.add(split[2]);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
                (activity,android.R.layout.select_dialog_item, allBlock.toArray(new String[allBlock.size()]));
        etBlock.setThreshold(1);
        etBlock.setAdapter(adapterBlock);

        ArrayAdapter<String> adapterAfdeling = new ArrayAdapter<String>
                (activity,android.R.layout.select_dialog_item, allAfdeling.toArray(new String[allAfdeling.size()]));
        etAfdeling.setThreshold(1);
        etAfdeling.setAdapter(adapterAfdeling);

        try {
            String [] blockAfdeling = GlobalHelper.getAndAssignBlock(new File(locKmlMap),latitude,longitude).split(";");

            if(selectedTujuan == null){
                etBlock.setText(blockAfdeling[2]);
                etAfdeling.setText(blockAfdeling[1]);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setDefaultManual(){

        if(selectedTujuan != null){
            longitude = longawal;
            latitude = latawal;
            selectedTujuan = null;

            ALselectedImage.clear();
            setupimageslide();
        }

        Point mapPoint = GeometryEngine.project(longitude,latitude,spatialReference);
        map.centerAt(mapPoint,true);
        tv_lat_manual.setText(String.format("%.5f",longitude));
        tv_lon_manual.setText(String.format("%.5f",latitude));
        tv_deg_manual.setText("0");
        tv_dis_manual.setText("0 m");
        ll_latlon.setBackgroundColor(Color.WHITE);
        zoomToLocation(latitude,longitude,spatialReference);
        GlobalHelper.hideKeyboard(activity);

        switchCrossHairUI(true);
//        btnSave.setVisibility(View.VISIBLE);
    }

    private void switchCrossHairUI(boolean visible){
        if(visible){
            ivCrosshair.setVisibility(View.VISIBLE);
            ll_latlon.setVisibility(View.VISIBLE);
        }else{
            ivCrosshair.setVisibility(View.GONE);
            ll_latlon.setVisibility(View.GONE);
        }
    }

    private void zoomToLocation(double lat, double lon, SpatialReference spatialReference) {
        Point mapPoint = GeometryEngine.project(lon,lat,spatialReference);
        map.centerAt(mapPoint,true);
        map.zoomTo(mapPoint,15000f);
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        private AlertDialog alertDialogAllpoin ;
        boolean validasi;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            validasi = validasi();
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(activity,activity.getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... params) {
            if(validasi) {
                if (savePointLangsiran()) {
//                pointLangsiranSetup();
                    return String.valueOf(1);
                } else {
                    return String.valueOf(0);
                }
            }else {
                return String.valueOf(0);
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (alertDialogAllpoin != null) {
                alertDialogAllpoin.cancel();
            }
            switch (Integer.parseInt(result)) {
                case 0: {
                    Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.save_failed), Toast.LENGTH_SHORT).show();
                    break;
                }
                case 1: {
                    //graphicsLayer.removeAll();
                    //gambar titik tph
                    GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_TPH);
//                    fabZoomToEstate.show();
//                    fabZoomToLocation.show();
                    Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.save_done), Toast.LENGTH_SHORT).show();
//                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    if(activity instanceof MapActivity){
                        int stat = 0;
                        if (isNewData) {
                            stat = MapActivity.Status_Close_Form_Save;
                        }else{
                            stat = MapActivity.Status_Close_Form_Update;
                        }
                        mapActivity.closeForm(stat);
                    }else {
                        Fragment fragment = (Fragment) activity.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        if (fragment instanceof AngkutInputFragment) {
                            ((AngkutInputFragment) fragment).setLangsiran(selectedTujuan);
                            ((AngkutInputFragment) fragment).tujuanHelper.listrefrence.dismiss();
                        }
                    }
                    break;
                }
            }
        }
    }
}
