package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.Langsiran;
import id.co.ams_plantation.harvestsap.model.TPHnLangsiran;
import id.co.ams_plantation.harvestsap.ui.MapActivity;

public class RowInfoWindowChoseTphnLangsiran extends RecyclerView.Adapter<RowInfoWindowChoseTphnLangsiran.Holder> {
    Context context;
    private final ArrayList<TPHnLangsiran> tphArrayList;

    public RowInfoWindowChoseTphnLangsiran(Context context, ArrayList<TPHnLangsiran> tphs) {
        this.context = context;
        this.tphArrayList = tphs;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_infowindow_chose_tphnlangsiran, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.setValue(tphArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return tphArrayList != null ? tphArrayList.size() : 0;
    }

    class Holder extends RecyclerView.ViewHolder {
        LinearLayout llcolor;
        CardView cv_riw_chose;
        TextView tv_riw_chose;
        TextView tv_riw_chose1;
        TextView tv_riw_chose2;
        ImageView iv_status;

        public Holder(View view) {
            super(view);
            llcolor = (LinearLayout) view.findViewById(R.id.llcolor);
            cv_riw_chose = (CardView) view.findViewById(R.id.cv_riw_chose);
            tv_riw_chose = (TextView) view.findViewById(R.id.tv_riw_chose);
            tv_riw_chose1 = (TextView) view.findViewById(R.id.tv_riw_chose1);
            tv_riw_chose2 = (TextView) view.findViewById(R.id.tv_riw_chose2);
            iv_status = (ImageView) view.findViewById(R.id.iv_status);
        }

        public void setValue(final TPHnLangsiran value) {
//            if(context instanceof TphActivity) {
//                TphActivity activity = ((TphActivity) context);
//            }
            if(value.getTph() != null){
                llcolor.setBackgroundColor(TPHnLangsiran.COLOR_TPH);
                tv_riw_chose.setText(value.getTph().getNamaTph());
                tv_riw_chose1.setText("TPH");
                tv_riw_chose2.setVisibility(View.GONE);
            }else if(value.getLangsiran() != null){
                llcolor.setBackgroundColor(TPHnLangsiran.COLOR_LANGSIRAN);
                tv_riw_chose.setText(value.getLangsiran().getNamaTujuan());
                tv_riw_chose1.setText("LANGSIRAN");
                tv_riw_chose2.setText(Langsiran.LIST_TIPE_LANGSIRAN[value.getLangsiran().getTypeTujuan()]);
            }

//            iv_status = activity.newRecording.setupivstatus(modelInfoWindowChose.getStatus(),iv_status);
//
//            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(100, 100);
//            iv_status.setLayoutParams(layoutParams);

            cv_riw_chose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MapActivity) context).callout.animatedHide();
                    int idx = 0;
                    for (HashMap.Entry<Integer, TPHnLangsiran> entry : ((MapActivity) context).hashpoint.entrySet()) {
                        if(value.getIdTPHnLangsiran().equals(entry.getValue().getIdTPHnLangsiran())){
                            idx = entry.getKey();
                            break;
                        }
                    }
                    ((MapActivity) context).showInfoWindow(idx,value);
                }
            });
        }
    }
}