package id.co.ams_plantation.harvestsap.Fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.mikepenz.iconics.utils.Utils;
import com.roughike.swipeselector.OnSwipeItemSelectedListener;
import com.roughike.swipeselector.SwipeItem;
import com.roughike.swipeselector.SwipeSelector;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.adapter.AncakAdapter;
import id.co.ams_plantation.harvestsap.TableView.holder.AncakRowHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestsap.TableView.view_model.AncakTableViewModel;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.QcAncak;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestsap.ui.QcMutuAncakActivity;
import id.co.ams_plantation.harvestsap.ui.TphActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;

/**
 * Created by user on 12/4/2018.
 */

public class QcMutuAncakFragmet extends Fragment {

    public static final int STATUS_LONG_OPERATION_NONE = 0;
    public static final int STATUS_LONG_OPERATION_SEARCH = 1;
    public static final int STATUS_LONG_OPERATION_SELECT_ITEM = 2;
    public static final int STATUS_LONG_OPERATION_NEW_DATA = 3;

    int totalPohonDiQc;
    HashMap<String,QcAncak> origin;
    AncakAdapter adapter;
    TableView mTableView;
//    Filter mTableFilter;
    AncakTableViewModel mTableViewModel;
    CompleteTextViewHelper etSearch;
    Button btnSearch;
    TextView tvQcTph;
    TextView tvQcPokok;
    RelativeLayout ltableview;

    LinearLayout llNewData;

    boolean isVisibleToUser;
    MainMenuActivity mainMenuActivity;
    Set<String> listSearch;
    Date dateSelected;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
    QcAncak seletedQcAncak;

    public static QcMutuAncakFragmet getInstance(){
        QcMutuAncakFragmet fragment = new QcMutuAncakFragmet();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qc_ancak_layout,null,false);
        mainMenuActivity = (MainMenuActivity) getActivity();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        main();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SwipeSelector menuSwipeSelector = view.findViewById(R.id.menuSwipeSelector);
        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        tvQcTph = view.findViewById(R.id.tvQcTph);
        tvQcPokok = view.findViewById(R.id.tvQcPokok);
        ltableview = view.findViewById(R.id.ltableview);
        mTableView = view.findViewById(R.id.tableview);
        llNewData = view.findViewById(R.id.llNewData);

        totalPohonDiQc = 0;
        dateSelected = new Date();
        origin = new HashMap<>();

        ArrayList<SwipeItem> swipeItems= new ArrayList<>();
        Long now = System.currentTimeMillis();
        for(int i = 0 ; i < GlobalHelper.MAX_LAST_DAY_DATA; i++){
            Long l = now - ( i * 24 * 60 * 60 * 1000);
            swipeItems.add(new SwipeItem(String.valueOf(i),sdf.format(l),""));
        }
        Collections.reverse(swipeItems);

        SwipeItem[] tmpStrSwipe = new SwipeItem[swipeItems.size()];
        tmpStrSwipe = swipeItems.toArray(tmpStrSwipe);
        menuSwipeSelector.setItems(tmpStrSwipe);
        menuSwipeSelector.selectItemAt(swipeItems.size() - 1);
        menuSwipeSelector.setOnItemSelectedListener(new OnSwipeItemSelectedListener() {
            @Override
            public void onItemSelected(SwipeItem item) {
                try {
                    Date date = sdf.parse(item.getTitle());
                    dateSelected = date;
                    startLongOperation(STATUS_LONG_OPERATION_NONE);
                    Log.d("item Date ", date.getTime() + " "+ sdf.format(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        llNewData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seletedQcAncak = null;
                startLongOperation(STATUS_LONG_OPERATION_NEW_DATA);
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etSearch.getText().toString().isEmpty()){
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
                }else{
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SEARCH));
                }
            }
        });

        startLongOperation(STATUS_LONG_OPERATION_NONE);
        showNewTranskasi();
    }

    private void main(){
        if (isVisibleToUser && getActivity() != null) {
            mainMenuActivity.ivLogo.setVisibility(View.VISIBLE);
            mainMenuActivity.btnFilter.setVisibility(View.GONE);
            startLongOperation(STATUS_LONG_OPERATION_NONE);
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    private void updateDataRv(int status){
        origin = new HashMap<>();
        listSearch = new android.support.v4.util.ArraySet<>();
        totalPohonDiQc = 0;

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_ANCAK);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            QcAncak qcAncak = gson.fromJson(dataNitrit.getValueDataNitrit(),QcAncak.class);
            if(GlobalHelper.dataAllUser != null){
                User user =  GlobalHelper.dataAllUser.get(qcAncak.getCreateBy());
                if (sdf.format(dateSelected).equals(sdf.format(new Date(qcAncak.getStartTime())))) {
                    if (status == STATUS_LONG_OPERATION_SEARCH) {
                        if (qcAncak.getTph().getNamaTph().equalsIgnoreCase(etSearch.getText().toString()) ||
                                qcAncak.getTph().getBlock().equalsIgnoreCase(etSearch.getText().toString()) ||
                                user.getUserFullName().equalsIgnoreCase(etSearch.getText().toString()) ||
                                String.valueOf(qcAncak.getTotalJanjangPanen()).equals(etSearch.getText().toString().toLowerCase()) ||
                                String.valueOf(qcAncak.getTotalBuahTinggal()).equals(etSearch.getText().toString().toLowerCase()) ||
                                String.valueOf(qcAncak.getBrondolanDiTph()).equals(etSearch.getText().toString().toLowerCase())) {
                            addRowTable(qcAncak);
                        }
                    } else if (status == STATUS_LONG_OPERATION_NONE) {
                        addRowTable(qcAncak);
                    }
                    if(user != null) {
                        listSearch.add(user.getUserFullName());
                    }
                    listSearch.add(qcAncak.getTph().getNamaTph());
                    listSearch.add(qcAncak.getTph().getBlock());
                    listSearch.add(String.valueOf(qcAncak.getTotalJanjangPanen()));
                    listSearch.add(String.valueOf(qcAncak.getTotalBuahTinggal()));
                    listSearch.add(String.valueOf(qcAncak.getBrondolanDiTph()));
                }
            }

        }
        db.close();

    }

    private void addRowTable(QcAncak qcAncak){
        totalPohonDiQc += qcAncak.getQcAncakPohons().size();
        origin.put(qcAncak.getIdQcAncak(), qcAncak);
    }

    private void updateUIRV(int status){
        if(status == STATUS_LONG_OPERATION_NONE){
            etSearch.setText("");
        }

        List<String> lSearch = new ArrayList<>();
        lSearch.addAll(listSearch);
        ArrayAdapter<String> adapterSearch = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, lSearch);
        etSearch.setAdapter(adapterSearch);
        etSearch.setThreshold(1);
        adapterSearch.notifyDataSetChanged();

        if(origin.size() > 0 ) {
            ArrayList<QcAncak> list = new ArrayList<>(origin.values());

            Collections.sort(list, new Comparator<QcAncak>() {
                @Override
                public int compare(QcAncak o1, QcAncak o2) {
                    return o1.getStartTime() > o2.getStartTime() ? -1 : (o1.getStartTime() < o2.getStartTime()) ? 1 : 0;
                }
            });

            ltableview.setVisibility(View.VISIBLE);
            mTableViewModel = new AncakTableViewModel(getContext(), list);
            // Create TableView Adapter
            adapter = new AncakAdapter(getContext(), mTableViewModel);
            mTableView.setAdapter(adapter);
            mTableView.setRowHeaderWidth(Utils.convertDpToPx(getActivity(),100));
            mTableView.setTableViewListener(new ITableViewListener() {
                @Override
                public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellDoubleClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {
                        // Create Long Press Popup
                        ColumnHeaderPopup popup = new ColumnHeaderPopup(
                                (ColumnHeaderViewHolder) columnHeaderView, mTableView);
                        // Show
                        popup.show();
                    }
                }

                @Override
                public void onColumnHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    if (rowHeaderView != null && rowHeaderView instanceof AncakRowHeaderViewHolder) {

                        String [] sid = ((AncakRowHeaderViewHolder) rowHeaderView).getCellId().split("-");
                        seletedQcAncak = origin.get(sid[1]);
                        startLongOperation(STATUS_LONG_OPERATION_SELECT_ITEM);

                    }

                }

                @Override
                public void onRowHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }

                @Override
                public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }
            });
            adapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel
                    .getRowHeaderList(), mTableViewModel.getCellList());


//            mTableFilter = new Filter(mTableView);
        }else{
            ltableview.setVisibility(View.GONE);
        }


        tvQcTph.setText(String.valueOf(origin.size()));
        tvQcPokok.setText(String.valueOf(totalPohonDiQc));
        Date d = new Date(System.currentTimeMillis());
        if(sdf.format(d).equals(sdf.format(dateSelected))){
            showNewTranskasi();
        }else{
            llNewData.setVisibility(View.GONE);
        }
    }


    private void showNewTranskasi(){
//        if(GlobalHelper.enableToNewTransaksi()){
//            llNewData.setVisibility(View.VISIBLE);
//        }else{
//            llNewData.setVisibility(View.GONE);
//        }
    }

    public void startLongOperation(int status){
        new LongOperation().execute(String.valueOf(status));
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
//        private AlertDialog alertDialogAllpoin ;
        String dataString;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (((BaseActivity) getActivity()).alertDialogBase != null) {
                ((BaseActivity) getActivity()).alertDialogBase.cancel();
            }
            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])){
                case STATUS_LONG_OPERATION_NONE:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM:{
                    Gson gson = new Gson();
                    dataString = gson.toJson(seletedQcAncak);
//                    Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_ANCAK_POHON);
//                    ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
//                    for(int i = 0; i < seletedQcAncak.getQcAncakPohons().size(); i++){
//                        Gson gson = new Gson();
//                        DataNitrit dataNitrit = new DataNitrit(seletedQcAncak.getQcAncakPohons().get(i).getIdQcAncakPohon(),
//                                gson.toJson(seletedQcAncak.getQcAncakPohons().get(i)));
//                        repository.insert(dataNitrit);
//                    }
//                    db.close();
                    break;
                }
            }
            return strings[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            switch (Integer.parseInt(result)){
                case STATUS_LONG_OPERATION_NONE:{
                    updateUIRV(Integer.parseInt(result));

                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateUIRV(Integer.parseInt(result));

                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM:{
                    Intent intent = new Intent(getActivity(), QcMutuAncakActivity.class);
                    intent.putExtra("qcAncak",dataString);
                    startActivityForResult(intent,GlobalHelper.RESULT_QC_MUTU_ANCAK);
                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_NEW_DATA:{
                    Intent intent = new Intent(getActivity(), QcMutuAncakActivity.class);
                    startActivityForResult(intent,GlobalHelper.RESULT_QC_MUTU_ANCAK);
                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
            }

        }
    }
}
