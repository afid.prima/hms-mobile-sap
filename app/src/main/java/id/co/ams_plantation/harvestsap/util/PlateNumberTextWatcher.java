package id.co.ams_plantation.harvestsap.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class PlateNumberTextWatcher implements TextWatcher {
    private static final String PLATE_NUMBER_REGEX = "^[A-Z]{1,2}\\d{1,4}[A-Z]{0,3}$";

    private EditText editText;

    public PlateNumberTextWatcher(EditText editText) {
        this.editText = editText;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int start, int before, int count) {
        // Not used in this example
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        // Not used in this example
    }

    @Override
    public void afterTextChanged(Editable editable) {
        String input = editable.toString().toUpperCase();

        if (!input.matches(PLATE_NUMBER_REGEX)) {
            editText.setError("Format nomor plat tidak valid");
        } else {
            editText.setError(null);
        }

        // Implement your desired formatting logic here, for example: AB 1234 CD
        if (input.length() == 1 || input.length() == 3 || input.length() == 7) {
            editText.setText(String.format("%s", editText.getText().toString()));
            editText.setSelection(editText.getText().length());
        }
    }
}