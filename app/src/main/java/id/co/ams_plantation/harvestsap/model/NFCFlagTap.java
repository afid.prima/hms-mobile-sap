package id.co.ams_plantation.harvestsap.model;

/**
 * Created on : 29,Oktober,2020
 * Author     : Afid
 */

public class NFCFlagTap {
    String userTap;
    Long WaktuTap;
    int CountTap;

    public String getUserTap() {
        return userTap;
    }

    public void setUserTap(String userTap) {
        this.userTap = userTap;
    }

    public Long getWaktuTap() {
        return WaktuTap;
    }

    public void setWaktuTap(Long waktuTap) {
        WaktuTap = waktuTap;
    }

    public int getCountTap() {
        return CountTap;
    }

    public void setCountTap(int countTap) {
        CountTap = countTap;
    }
}
