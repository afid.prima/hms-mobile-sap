package id.co.ams_plantation.harvestsap.model;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;

public class TPHnLangsiran {

    public static final int COLOR_TPH = HarvestApp.getContext().getResources().getColor(R.color.OrangeRed);
    public static final int COLOR_TPH_RESURVEY = HarvestApp.getContext().getResources().getColor(R.color.Gold);
    public static final int COLOR_LANGSIRAN = HarvestApp.getContext().getResources().getColor(R.color.Brown);
    public static final int COLOR_UPLOAD = HarvestApp.getContext().getResources().getColor(R.color.SkyBlue);
    public static final int COLOR_BELUMUPLOAD = HarvestApp.getContext().getResources().getColor(R.color.SeaGreen);

    String idTPHnLangsiran;
    TPH tph;
    Langsiran langsiran;

    public TPHnLangsiran(String idTPHnLangsiran, TPH tph) {
        this.idTPHnLangsiran = idTPHnLangsiran;
        this.tph = tph;
    }

    public TPHnLangsiran(String idTPHnLangsiran, Langsiran langsiran) {
        this.idTPHnLangsiran = idTPHnLangsiran;
        this.langsiran = langsiran;
    }

    public String getIdTPHnLangsiran() {
        return idTPHnLangsiran;
    }

    public void setIdTPHnLangsiran(String idTPHnLangsiran) {
        this.idTPHnLangsiran = idTPHnLangsiran;
    }

    public TPH getTph() {
        return tph;
    }

    public void setTph(TPH tph) {
        this.tph = tph;
    }

    public Langsiran getLangsiran() {
        return langsiran;
    }

    public void setLangsiran(Langsiran langsiran) {
        this.langsiran = langsiran;
    }
}
