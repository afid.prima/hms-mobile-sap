package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.SPK;

public class CekSPKAdapter extends RecyclerView.Adapter<CekSPKAdapter.Holder>{

    Context context;
    ArrayList<SPK> originLists;
    ArrayList<SPK> filterLists;

    public CekSPKAdapter(Context context, ArrayList<SPK> originLists) {
        this.context = context;
        this.originLists = originLists;
        this.filterLists = originLists;
    }

    @NonNull
    @Override
    public CekSPKAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_record_with_item,viewGroup,false);
        return new CekSPKAdapter.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CekSPKAdapter.Holder holder, int i) {
        holder.setValue(filterLists.get(i));
    }

    @Override
    public int getItemCount() {
        return filterLists.size();
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                filterLists = new ArrayList<>();
                String string = constraint.toString().toLowerCase();
                int i = 0 ;

                for (SPK spk : originLists) {
                    try {
                        if (spk.getIdSPK().toLowerCase().contains(string) ||
                                spk.getDescription().toLowerCase().contains(string) ||
                                spk.getVendorCode().toLowerCase().contains(string) ||
                                spk.getMaterialGroupDesc().toLowerCase().contains(string)

                                ) {
                            filterLists.add(spk);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                if (filterLists.size() > 0) {
                    results.count = filterLists.size();
                    results.values = filterLists;
                    return results;
                } else {
                    return null;
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null) {
                    filterLists = (ArrayList<SPK>) results.values;
                } else {
                    filterLists.clear();
                }
                notifyDataSetChanged();

            }
        };
    }

    public class Holder extends RecyclerView.ViewHolder {
        CardView card_view;
        LinearLayout lketerangan;
        RelativeLayout rlImage;
        ImageView ivchevronup;
        ImageView ivchevrondown;
        TextView item_ket;
        TextView item_ket2;
        TextView item_ket1;
        TextView item_ket3;
        TextView item_ket4;
        TextView item_ket_sub;
        TextView item_ket_sub2;
        RecyclerView rvRef;

        public Holder(View itemView) {
            super(itemView);

            lketerangan = itemView.findViewById(R.id.lketerangan);
            card_view = itemView.findViewById(R.id.card_view);
            rlImage = itemView.findViewById(R.id.rlImage);
            ivchevronup = itemView.findViewById(R.id.ivchevronup);
            ivchevrondown = itemView.findViewById(R.id.ivchevrondown);
            item_ket = itemView.findViewById(R.id.item_ket);
            item_ket1 = itemView.findViewById(R.id.item_ket1);
            item_ket2 = itemView.findViewById(R.id.item_ket2);
            item_ket3 = itemView.findViewById(R.id.item_ket3);
            item_ket4 = itemView.findViewById(R.id.item_ket4);
            item_ket_sub = itemView.findViewById(R.id.item_ket_sub);
            item_ket_sub2 = itemView.findViewById(R.id.item_ket_sub2);
            rvRef = itemView.findViewById(R.id.rvRef);

            ivchevronup.setImageDrawable(new
                    IconicsDrawable(context)
                    .icon(MaterialDesignIconic.Icon.gmi_chevron_up)
                    .sizeDp(18)
                    .colorRes(R.color.Gray));
            ivchevrondown.setImageDrawable(new
                    IconicsDrawable(context)
                    .icon(MaterialDesignIconic.Icon.gmi_chevron_down)
                    .sizeDp(18)
                    .colorRes(R.color.Gray));

            ivchevronup.setVisibility(View.VISIBLE);
            ivchevrondown.setVisibility(View.GONE);
            rlImage.setVisibility(View.GONE);
            rvRef.setVisibility(View.GONE);

            lketerangan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ivchevrondown.getVisibility() == View.VISIBLE){
                        ivchevronup.setVisibility(View.VISIBLE);
                        ivchevrondown.setVisibility(View.GONE);
                        rvRef.setVisibility(View.GONE);
                    }else {
                        ivchevronup.setVisibility(View.GONE);
                        ivchevrondown.setVisibility(View.VISIBLE);
                        rvRef.setVisibility(View.VISIBLE);
                    }
                }
            });
        }

        public void setValue(SPK value) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd MMM yyyy HH:mm:ss");

                item_ket.setText(value.getIdSPK() + " - " + value.getVendorCode());
                item_ket2.setText(value.getMaterialGroupDesc());
                item_ket1.setText(value.getDescription());
                item_ket3.setText(value.getShortText());
                item_ket4.setText("Total Block " + value.getBlock().size());
                item_ket_sub.setText(sdf.format(value.getValidFrom()));
                item_ket_sub2.setText(sdf.format(value.getValidTo()));

                if(value.getBlock().size() > 0) {
                    rvRef.setLayoutManager(new GridLayoutManager(context, 4));
                    CekSPKBlockAdapter adapter = new CekSPKBlockAdapter(value.getBlock());
                    rvRef.setAdapter(adapter);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
