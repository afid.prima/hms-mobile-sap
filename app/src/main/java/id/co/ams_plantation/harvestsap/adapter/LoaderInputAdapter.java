package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;

import id.co.ams_plantation.harvestsap.Fragment.AngkutInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.MekanisasiInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.SpbInputFragment;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.SwipeItemRecycleView.SwipeControllerInterface;
import id.co.ams_plantation.harvestsap.model.Operator;
import id.co.ams_plantation.harvestsap.ui.AngkutActivity;
import id.co.ams_plantation.harvestsap.ui.MekanisasiActivity;
import id.co.ams_plantation.harvestsap.ui.SpbActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;

public class LoaderInputAdapter extends RecyclerView.Adapter<LoaderInputAdapter.Holder> implements SwipeControllerInterface {
    Context context;
    ArrayList<Operator> operators;
    public ArrayList<Operator> loader;
    SelectOperatorAdapter adapterOperator;

    public LoaderInputAdapter(Context context, ArrayList<Operator> operators, ArrayList<Operator> loader) {
        this.context = context;
        this.operators = operators;
        this.loader = loader;
        Log.d("LoaderInputAdapter", "loader size: " + operators.size());
    }

    @NonNull
    @Override
    public LoaderInputAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.loader_transaksi_item, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull LoaderInputAdapter.Holder holder, int position) {
        holder.setValue(loader.get(position));
        Log.d("LoaderInputAdapter", "listCages size (onBindViewHolder): " + operators.size());
    }

    @Override
    public int getItemCount() {
        return loader != null ? loader.size() : 0;
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        Collections.swap(operators, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        updateLoader();
    }

    public void updateLoader(){
        if(context instanceof AngkutActivity) {
            if (((AngkutActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof AngkutInputFragment) {
                AngkutInputFragment fragment = (AngkutInputFragment) ((AngkutActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
                fragment.updateLoader();
            }
        }else if(context instanceof SpbActivity){
            if (((SpbActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof SpbInputFragment) {
                SpbInputFragment fragment = (SpbInputFragment) ((SpbActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
                fragment.updateLoader();
            }
        }else if(context instanceof MekanisasiActivity){
            if (((MekanisasiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof MekanisasiInputFragment) {
                MekanisasiInputFragment fragment = (MekanisasiInputFragment) ((MekanisasiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
                fragment.updateLoader();
            }
        }
    }

    public class Holder extends RecyclerView.ViewHolder {
        CompleteTextViewHelper etLoader;
        TextView tvNik;
        TextView tvEstCode;
        TextView tvValue;

        public Holder(View v) {
            super(v);
            etLoader = v.findViewById(R.id.etLoader);
            tvNik = v.findViewById(R.id.tvNik);
            tvEstCode = v.findViewById(R.id.tvEstCode);
            tvValue = v.findViewById(R.id.tvValue);
        }

        public void setValue(Operator operator) {
            Log.d("LoaderInputAdapter", "listCages size (Holder - setValue): " + operators.size());
            updateLoaderUI(operator);

            adapterOperator = new SelectOperatorAdapter(context, R.layout.row_setting,operators);
            etLoader.setThreshold(1);
            etLoader.setAdapter(adapterOperator);
            adapterOperator.notifyDataSetChanged();

            etLoader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etLoader.showDropDown();
                }
            });

            etLoader.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Operator loader = ((SelectOperatorAdapter) parent.getAdapter()).getItemAt(position);
                    updateLoaderUI(loader);
                    updateLoader();
                }
            });
        }

        private void updateLoaderUI(Operator value) {
            if (value.getKodeOperator() != null) {
                Gson gson = new Gson();
                etLoader.setText(value.getNama());
                tvNik.setText(value.getNik());
                tvEstCode.setText(value.getEstCodeSAP());
                tvValue.setText(gson.toJson(value));
            }
        }
    }
}
