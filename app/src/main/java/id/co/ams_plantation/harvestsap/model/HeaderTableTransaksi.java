package id.co.ams_plantation.harvestsap.model;

public class HeaderTableTransaksi {
    public static final int Status_Hitung_Nilai = 0;
    public static final int Status_Hitung_Banyak_Data = 1;

    public static final int Status_Calculated_In_Summary_Is_Show = 1;
    public static final int Status_Calculated_In_Summary_Not_Show = 0;

    String namaAtribut;
    String viewAtribut;
    int statusHitung;
    int calculateInSummary;

    public HeaderTableTransaksi(String namaAtribut, String viewAtribut, int statusHitung, int calculateInSummary) {
        this.namaAtribut = namaAtribut;
        this.viewAtribut = viewAtribut;
        this.statusHitung = statusHitung;
        this.calculateInSummary = calculateInSummary;
    }

    public String getNamaAtribut() {
        return namaAtribut;
    }

    public void setNamaAtribut(String namaAtribut) {
        this.namaAtribut = namaAtribut;
    }

    public String getViewAtribut() {
        return viewAtribut;
    }

    public void setViewAtribut(String viewAtribut) {
        this.viewAtribut = viewAtribut;
    }

    public int getStatusHitung() {
        return statusHitung;
    }

    public void setStatusHitung(int statusHitung) {
        this.statusHitung = statusHitung;
    }

    public int getCalculateInSummary() {
        return calculateInSummary;
    }

    public void setCalculateInSummary(int calculateInSummary) {
        this.calculateInSummary = calculateInSummary;
    }
}
