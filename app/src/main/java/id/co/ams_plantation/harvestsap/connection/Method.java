package id.co.ams_plantation.harvestsap.connection;

import java.io.Serializable;


public enum Method implements Serializable {
    GET,
    POST,
    UPLOAD,
    PATCH,
    DELETE
}
