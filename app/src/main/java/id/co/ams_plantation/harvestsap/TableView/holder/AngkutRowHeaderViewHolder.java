package id.co.ams_plantation.harvestsap.TableView.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;

/**
 * Created by user on 12/24/2018.
 */

public class AngkutRowHeaderViewHolder extends AbstractViewHolder {

    private final TextView row_header_textview;
    private final LinearLayout row_header_container;
    private TableViewCell cell;

    public AngkutRowHeaderViewHolder(View layout) {
        super(layout);
        row_header_textview = (TextView) itemView.findViewById(R.id.row_header_textview);
        row_header_container = (LinearLayout) itemView.findViewById(R.id.row_header_container);
    }

    public void setCell(TableViewRowHeader cell) {
        String [] id = cell.getId().split("-");
        this.cell = cell;
        row_header_textview.setText(String.valueOf(cell.getData()));
        switch (id[2].toLowerCase()){
            case "angkut":{
                row_header_container.setBackgroundColor(TransaksiAngkut.COLOR_FROM_ANGKUT);
                break;
            }
            case "transit":{
                row_header_container.setBackgroundColor(TransaksiAngkut.COLOR_FROM_TRANSIT);
                break;
            }
            case "manual":{
                row_header_container.setBackgroundColor(TransaksiAngkut.COLOR_FROM_MANUAL);
                break;
            }
        }
    }

    public String getCellId(){
        return this.cell.getId();
    }
}
