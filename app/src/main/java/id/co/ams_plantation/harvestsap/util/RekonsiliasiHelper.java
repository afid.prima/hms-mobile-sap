package id.co.ams_plantation.harvestsap.util;

import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;

public class RekonsiliasiHelper {

    public ArrayList<TransaksiAngkut> getAllRekonsiliasi(Date selectedDate, HashMap<String, TransaksiAngkut> originData){
        ArrayList<TransaksiAngkut> rekonsiliasiList = new ArrayList<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> allRekonsiliasi = repository.find(ObjectFilters.eq("param1", ""+true));
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");

       for(Iterator iterator = allRekonsiliasi.iterator(); iterator.hasNext();){
           DataNitrit dataNitrit = (DataNitrit) iterator.next();
           Gson gson = new Gson();
           TransaksiAngkut transaksiAngkut = gson.fromJson(dataNitrit.getValueDataNitrit(),TransaksiAngkut.class);
           if(sdf.format(selectedDate).equals(sdf.format(transaksiAngkut.getStartDate()))){
               rekonsiliasiList.add(transaksiAngkut);
               originData.put(transaksiAngkut.getIdTAngkut(), transaksiAngkut);
           }
       }
        db.close();
        return rekonsiliasiList;
    }
}
