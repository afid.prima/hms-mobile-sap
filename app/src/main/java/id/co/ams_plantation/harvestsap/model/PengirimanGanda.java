package id.co.ams_plantation.harvestsap.model;

public class PengirimanGanda {

    int idx;
    int kendaraanDari;
    Operator operator;
    Vehicle vehicle;
    SPK spk;
    Langsiran langsiran;

    public PengirimanGanda(int idx,int kendaraanDari, Operator operator, Vehicle vehicle, Langsiran langsiran) {
        this.idx = idx;
        this.kendaraanDari = kendaraanDari;
        this.operator = operator;
        this.vehicle = vehicle;
        this.langsiran = langsiran;
    }

    public PengirimanGanda(int idx, int kendaraanDari, Operator operator, Vehicle vehicle, SPK spk) {
        this.idx = idx;
        this.kendaraanDari = kendaraanDari;
        this.operator = operator;
        this.vehicle = vehicle;
        this.spk = spk;
    }

    public PengirimanGanda(int idx, int kendaraanDari, Operator operator, Vehicle vehicle) {
        this.idx = idx;
        this.kendaraanDari = kendaraanDari;
        this.operator = operator;
        this.vehicle = vehicle;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public int getKendaraanDari() {
        return kendaraanDari;
    }

    public void setKendaraanDari(int kendaraanDari) {
        this.kendaraanDari = kendaraanDari;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public SPK getSpk() {
        return spk;
    }

    public void setSpk(SPK spk) {
        this.spk = spk;
    }

    public Langsiran getLangsiran() {
        return langsiran;
    }

    public void setLangsiran(Langsiran langsiran) {
        this.langsiran = langsiran;
    }
}
