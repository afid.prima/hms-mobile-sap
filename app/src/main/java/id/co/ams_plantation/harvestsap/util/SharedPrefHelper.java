package id.co.ams_plantation.harvestsap.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.HashMap;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.model.UserAppUsage;

public class SharedPrefHelper {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "AMS";

    public static final String KEY_GENGSELECTED = "gang";
    public static final String KEY_TipeKongsi = "tipeKongsi";
    public static final String KEY_DoubleQrSPB = "qrspb";
    // Constructor
    public SharedPrefHelper(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void commit(String key,String name){
        // Storing name in pref
        editor.putString(key, name);
        // commit changes
        editor.commit();
    }

    public HashMap<String, String> getSharePref(){
        HashMap<String, String> Hses = new HashMap<String, String>();
        // user name
        Hses.put(KEY_GENGSELECTED, pref.getString(KEY_GENGSELECTED, null));
        // return user
        return Hses;
    }

    public String getName(String key){
        return  pref.getString(key, null);
    }


    public static void applySharepref(String Key,String Value){
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(Key, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Key,Value);
        editor.apply();
    }

    public static String getSharePref(String Key){
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(Key, Context.MODE_PRIVATE);
        return preferences.getString(Key, null);
    }

    public static TransaksiPanen getSharePrefTransaksiPanen(){
        String sharePref = getSharePref(HarvestApp.TRANSAKSI_PANEN_PREF);
        if(sharePref == null){
            return null;
        }
        Gson gson = new Gson();
        return gson.fromJson(sharePref,TransaksiPanen.class);
    }

    public static UserAppUsage getSharePrefUserAppUsage(){
        String sharePref = getSharePref(HarvestApp.USER_APP_PREF);
        if(sharePref == null){
            return null;
        }
        Gson gson = new Gson();
        return gson.fromJson(sharePref,UserAppUsage.class);
    }
}
