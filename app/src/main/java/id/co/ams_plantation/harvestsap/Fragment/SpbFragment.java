package id.co.ams_plantation.harvestsap.Fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.mikepenz.iconics.utils.Utils;
import com.roughike.swipeselector.OnSwipeItemSelectedListener;
import com.roughike.swipeselector.SwipeItem;
import com.roughike.swipeselector.SwipeSelector;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.adapter.TSpbAdapter;
import id.co.ams_plantation.harvestsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.holder.TSpbRowHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.listener.TSpbTableViewListener;
import id.co.ams_plantation.harvestsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestsap.TableView.popup.RowHeaderLongPressPopup;
import id.co.ams_plantation.harvestsap.TableView.view_model.TSpbTableViewModel;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.Operator;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestsap.ui.MekanisasiActivity;
import id.co.ams_plantation.harvestsap.ui.SpbActivity;
import id.co.ams_plantation.harvestsap.util.ActivityLoggerHelper;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.FilterHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.MainMenuHelper;
import id.co.ams_plantation.harvestsap.util.MekanisasiHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiSpbHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;

/**
 * Created by user on 12/4/2018.
 */

public class SpbFragment extends Fragment {
    public static final int STATUS_LONG_OPERATION_NONE = 0;
    public static final int STATUS_LONG_OPERATION_SEARCH = 1;
    public static final int STATUS_LONG_OPERATION_SELECT_ITEM = 2;
    public static final int STATUS_LONG_OPERATION_NEW_DATA = 3;
    public static final int STATUS_LONG_OPERATION_SHOW_CONFIRM_DELETED = 4;
    public static final int STATUS_LONG_OPERATION_DELETED = 5;
    public static final int STATUS_LONG_OPERATION_NEW_DATA_MEKANISASI = 6;

    HashMap<String,TransaksiSPB> origin;
    HashMap<String ,User> getAllUser;
    Set<String> listSearch;
    Set<String> blockMekanisasi;

    TSpbTableViewModel mTableViewModel;
    TSpbAdapter adapter;

    LinearLayout llNewData;
    CompleteTextViewHelper etSearch;
    Button btnSearch;
    RelativeLayout ltableview;
    TableView mTableView;
    TextView tvStartTime;
    TextView tvFinishTime;
    TextView tvTrip;
    TextView tvUpload;
    TextView tvTotalJanjang;
    TextView tvTotalBrondolan;

    Date dateSelected;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    Long mulai = Long.valueOf(0);
    Long sampai = Long.valueOf(0);
    int tJanjang = 0;
    int tBrondolan = 0;
    int upload= 0;
    int trip = 0;

    String idSelected;
    int posisi;

    MainMenuActivity mainMenuActivity;
    TransaksiSPB selectTransaksiSPB;
    boolean isVisibleToUser;
    MainMenuHelper mainMenuHelper;
    MekanisasiHelper mekanisasiHelper;

    String TAG = this.getClass().toString();

    public static SpbFragment getInstance() {
        SpbFragment fragment = new SpbFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.spb_layout, null, false);
        mainMenuActivity = (MainMenuActivity) getActivity();
        return view;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        main();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SwipeSelector menuSwipeSelector = view.findViewById(R.id.menuSwipeSelector);
        llNewData = view.findViewById(R.id.llNewData);
        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        ltableview = view.findViewById(R.id.ltableview);
        mTableView = view.findViewById(R.id.tableview);
        tvStartTime = view.findViewById(R.id.tvStartTime);
        tvFinishTime = view.findViewById(R.id.tvFinishTime);
        tvTrip = view.findViewById(R.id.tvTrip);
        tvUpload = view.findViewById(R.id.tvUpload);
        tvTotalJanjang = view.findViewById(R.id.tvTotalJanjang);
        tvTotalBrondolan = view.findViewById(R.id.tvTotalBrondolan);

        dateSelected = new Date();
        origin = new HashMap<>();
        getAllUser = new HashMap<>();
        blockMekanisasi = new android.support.v4.util.ArraySet<>();
        mainMenuHelper = new MainMenuHelper(mainMenuActivity);
        mekanisasiHelper = new MekanisasiHelper(mainMenuActivity);

        ArrayList<SwipeItem> swipeItems= new ArrayList<>();
        Long now = System.currentTimeMillis();
        for(int i = 0 ; i < GlobalHelper.MAX_LAST_DAY_DATA; i++){
            Long l = now - ( i * 24 * 60 * 60 * 1000);
            swipeItems.add(new SwipeItem(String.valueOf(i),sdf.format(l),""));
        }
        Collections.reverse(swipeItems);

        SwipeItem[] tmpStrSwipe = new SwipeItem[swipeItems.size()];
        tmpStrSwipe = swipeItems.toArray(tmpStrSwipe);
        menuSwipeSelector.setItems(tmpStrSwipe);
        menuSwipeSelector.selectItemAt(swipeItems.size() - 1);
        menuSwipeSelector.setOnItemSelectedListener(new OnSwipeItemSelectedListener() {
            @Override
            public void onItemSelected(SwipeItem item) {
                try {
                    Date date = sdf.parse(item.getTitle());
                    dateSelected = date;
                    startLongOperation(STATUS_LONG_OPERATION_NONE);
                    Log.d("item Date ", date.getTime() + " "+ sdf.format(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        llNewData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (blockMekanisasi.size() > 0) {
                    mekanisasiHelper.popupNewSPB();
                } else {
                    Intent intent = new Intent(getActivity(), SpbActivity.class);
                    startActivityForResult(intent, GlobalHelper.RESULT_SPBACTIVITY);
                }
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLongOperation(STATUS_LONG_OPERATION_SEARCH);
            }
        });
        main();
//        showNewTranskasi();
    }

    public void chosenSPB(int flag){
        if(flag == 1){
            ActivityLoggerHelper.recordHitTime(SpbFragment.class.getSimpleName(),"llNewData",new Date(),new Date());
            startLongOperation(STATUS_LONG_OPERATION_NEW_DATA);
        }else{
            ActivityLoggerHelper.recordHitTime(SpbFragment.class.getSimpleName(),"llNewData",new Date(),new Date());
            startLongOperation(STATUS_LONG_OPERATION_NEW_DATA_MEKANISASI);
        }
    }

    private void showNewTranskasi(){
        if(GlobalHelper.enableToNewTransaksi(getAllUser)){
            llNewData.setVisibility(View.VISIBLE);
        }else{
            llNewData.setVisibility(View.GONE);
        }
    }

    private void main(){
        if (isVisibleToUser && getActivity() != null) {
            mainMenuActivity.ivLogo.setVisibility(View.GONE);
            mainMenuActivity.btnFilter.setVisibility(View.VISIBLE);
            mainMenuActivity.filterHelper.setFilterView(this);
            Log.d("setUserVisibleHint","SpbFragment");
            startLongOperation(STATUS_LONG_OPERATION_NONE);
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    private void updateDataRv(int status) {
        mulai = Long.valueOf(0);
        sampai = Long.valueOf(0);
        tJanjang = 0;
        tBrondolan = 0;
        upload = 0;
        trip = 0;
        origin = new HashMap<>();
        listSearch = new android.support.v4.util.ArraySet<>();

        String userId = GlobalHelper.getUser().getUserID();
        String filterAfdeling = mainMenuActivity.tvFilterAfdeling.getText().toString();
        String filterCreateBy = mainMenuActivity.tvFilterDataInputBy.getText().toString();

        ArrayList<String> allDb = GlobalHelper.getAllFileDB(GlobalHelper.TABLE_TRANSAKSI_SPB, true);
        for (String dbFile : allDb) {
            Nitrite db = GlobalHelper.getTableNitritFullPath(dbFile);
            if (db != null) {
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
                for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                    DataNitrit dataNitrit = (DataNitrit) iterator.next();
                    Gson gson = new Gson();
                    TransaksiSPB transaksi = gson.fromJson(dataNitrit.getValueDataNitrit(), TransaksiSPB.class);
                    if (sdf.format(dateSelected).equals(sdf.format(new Date(transaksi.getCreateDate())))) {

                        if (transaksi.getStatus() == TransaksiSPB.BELUM_UPLOAD || transaksi.getStatus() == TransaksiSPB.UPLOAD) {
                            TransaksiSPB addTransaksi = null;
                            if (status == STATUS_LONG_OPERATION_SEARCH) {
                                if ((transaksi.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS) && (transaksi.getTransaksiAngkut().getPks().getNamaPKS().toLowerCase().contains(etSearch.getText().toString().toLowerCase()))) {
                                    addTransaksi = transaksi;
                                } else if ((transaksi.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_KELUAR) && TransaksiAngkut.KELAUR.contains(etSearch.getText().toString().toUpperCase())) {
                                    addTransaksi = transaksi;
                                } else if (TransaksiSpbHelper.converIdSPBToNoSPB(transaksi.getIdSPB()).toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                                        timeFormat.format(new Date(transaksi.getCreateDate())).contains(etSearch.getText().toString().toLowerCase()) ||
                                        String.format("%,d", transaksi.getTotalJanjang()).contains(etSearch.getText().toString().toLowerCase()) ||
                                        String.format("%,d", transaksi.getTotalBrondolan()).contains(etSearch.getText().toString().toLowerCase())) {
                                    addTransaksi = transaksi;
                                }
                                if (addTransaksi == null) {
                                    User user = GlobalHelper.dataAllUser.get(transaksi.getCreateBy());
                                    if (user != null) {
                                        if (user.getUserFullName().toLowerCase().contains(etSearch.getText().toString().toLowerCase())) {
                                            addTransaksi = transaksi;
                                        }
                                    }
                                }
                                if (addTransaksi == null) {
                                    User user = GlobalHelper.dataAllUser.get(transaksi.getApproveBy());
                                    if (user != null) {
                                        if (user.getUserFullName().toLowerCase().contains(etSearch.getText().toString().toLowerCase())) {
                                            addTransaksi = transaksi;
                                        }
                                    }
                                }
                                if (addTransaksi == null) {
                                    Operator operator = transaksi.getTransaksiAngkut().getSupir();
                                    if (operator != null) {
                                        if (operator.getNama().toLowerCase().contains(etSearch.getText().toString().toLowerCase())) {
                                            addTransaksi = transaksi;
                                        }
                                    }
                                }
                            } else if (status == STATUS_LONG_OPERATION_NONE) {
                                addTransaksi = transaksi;
                            }

                            if (!filterAfdeling.equals(FilterHelper.No_Filter_Afdeling) && addTransaksi != null) {
                                boolean ada = false;
                                String[] fafd = filterAfdeling.split(",");
                                for (int i = 0; i < fafd.length; i++) {
                                    for (int j = 0; j < addTransaksi.getTransaksiAngkut().getAngkuts().size(); j++) {
                                        Angkut angkut = addTransaksi.getTransaksiAngkut().getAngkuts().get(j);
                                        if (angkut.getTph() != null) {
                                            if (angkut.getTph().getAfdeling() != null) {
                                                if (fafd[i].equalsIgnoreCase(angkut.getTph().getAfdeling())) {
                                                    ada = true;
                                                    break;
                                                }
                                            }
                                        } else if (angkut.getLangsiran() != null) {
                                            if (angkut.getLangsiran().getAfdeling() != null) {
                                                if (fafd[i].equalsIgnoreCase(angkut.getLangsiran().getAfdeling())) {
                                                    ada = true;
                                                    break;
                                                }
                                            }
                                        } else if (angkut.getTransaksiPanen() != null) {
                                            if (angkut.getTransaksiPanen().getTph() != null) {
                                                if (angkut.getTransaksiPanen().getTph().getAfdeling() != null) {
                                                    if (fafd[i].equalsIgnoreCase(angkut.getTransaksiPanen().getTph().getAfdeling())) {
                                                        ada = true;
                                                        break;

                                                    }
                                                }
                                            }
                                        } else if (angkut.getTransaksiAngkut() != null) {
                                            if (angkut.getTransaksiAngkut().getLangsiran() != null) {
                                                if (angkut.getTransaksiAngkut().getLangsiran().getAfdeling() != null) {
                                                    if (fafd[i].equalsIgnoreCase(angkut.getTransaksiAngkut().getLangsiran().getAfdeling())) {
                                                        ada = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (!ada) {
                                    addTransaksi = null;
                                }
                            }

                            if (!filterCreateBy.equals(FilterHelper.No_Filter_User) && addTransaksi != null) {
                                if (!addTransaksi.getCreateBy().equals(userId)) {
                                    addTransaksi = null;
                                }
                            }

                            if (addTransaksi != null) {
                                addRowTable(addTransaksi);
                            }
                        }
                    }
                }
                db.close();
            }
        }
    }

    private void addRowTable(TransaksiSPB transaksi){
        if (mulai == 0) {
            mulai = transaksi.getCreateDate();
        } else if (mulai > transaksi.getCreateDate()) {
            mulai = transaksi.getCreateDate();
        }

        if (sampai < transaksi.getCreateDate()) {
            sampai = transaksi.getCreateDate();
        }

        tJanjang += transaksi.getTotalJanjang();
        tBrondolan += transaksi.getTotalBrondolan();

        if (transaksi.getStatus() == TransaksiSPB.UPLOAD) {
            upload++;
        }

        trip++;
        origin.put(transaksi.getIdSPB(), transaksi);

        listSearch.add(TransaksiSpbHelper.converIdSPBToNoSPB(transaksi.getIdSPB()));
        User user = GlobalHelper.dataAllUser.get(transaksi.getCreateBy());
        if(user != null){
            listSearch.add(user.getUserFullName());
        }
        user = GlobalHelper.dataAllUser.get(transaksi.getApproveBy());
        if(user != null){
            listSearch.add(user.getUserFullName());
        }
        Operator operator = transaksi.getTransaksiAngkut().getSupir();
        if(operator != null){
            listSearch.add(operator.getNama());
        }
        listSearch.add(transaksi.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS ? transaksi.getTransaksiAngkut().getPks().getNamaPKS() :
                transaksi.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR ? transaksi.getTransaksiAngkut().getLangsiran().getNamaTujuan():
                        transaksi.getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_KELUAR ? TransaksiAngkut.KELAUR : ""
        );
        listSearch.add(timeFormat.format(transaksi.getCreateDate()));
        listSearch.add(String.valueOf(transaksi.getTotalJanjang()));
        listSearch.add(String.valueOf(transaksi.getTotalBrondolan()));
    }

    private void updateUIRV(int status){
        List<String> lSearch = new ArrayList<>();
        lSearch.addAll(listSearch);
        lSearch = GlobalHelper.removeNullUsingIterator(lSearch);
        ArrayAdapter<String> adapterSearch = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, lSearch);
        etSearch.setAdapter(adapterSearch);
        etSearch.setThreshold(1);
        adapterSearch.notifyDataSetChanged();

        if(status == STATUS_LONG_OPERATION_NONE){
//            etSearch.setText("");
        }

        if(origin.size() > 0 ) {
            ltableview.setVisibility(View.VISIBLE);
            mTableViewModel = new TSpbTableViewModel(getContext(), origin);
            // Create TableView Adapter
            adapter = new TSpbAdapter(getContext(), mTableViewModel);
            mTableView.setAdapter(adapter);
            mTableView.setRowHeaderWidth(Utils.convertDpToPx(getActivity(),100));
//            mTableView.setTableViewListener(new TSpbTableViewListener(mTableView));
            mTableView.setTableViewListener(new ITableViewListener() {
                @Override
                public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {
                    Log.d(TAG, "onCellClicked: "+ String.valueOf(column) + " " + String.valueOf(row));
//                    Toast.makeText(getContext(), "onCellClicked", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCellDoubleClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {
                    Log.d(TAG, "onCellDoubleClicked: "+ String.valueOf(column) + " " + String.valueOf(row));
//                    Toast.makeText(getContext(), "onCellDoubleClicked", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {
                    Log.d(TAG, "onCellLongPressed: "+ String.valueOf(column) + " " + String.valueOf(row));
//                    Toast.makeText(getContext(), "onCellLongPressed", Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    Log.d(TAG, "onColumnHeaderClicked: "+ String.valueOf(column) );
//                    Toast.makeText(getContext(), "onColumnHeaderClicked", Toast.LENGTH_SHORT).show();
                    if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {
                        // Create Long Press Popup
                        ColumnHeaderPopup popup = new ColumnHeaderPopup(
                                (ColumnHeaderViewHolder) columnHeaderView, mTableView);
                        // Show
                        popup.show();
                    }
                }

                @Override
                public void onColumnHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    Log.d(TAG, "onColumnHeaderDoubleClicked: "+ String.valueOf(column) );
//                    Toast.makeText(getContext(), "onColumnHeaderDoubleClicked", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    Log.d(TAG, "onColumnHeaderLongPressed: "+ String.valueOf(column) );
//                    Toast.makeText(getContext(), "onColumnHeaderLongPressed", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    Log.d(TAG, "onRowHeaderClicked: "+ String.valueOf(row) );
//                    Toast.makeText(getContext(), "onRowHeaderClicked", Toast.LENGTH_SHORT).show();
                    if (rowHeaderView != null && rowHeaderView instanceof TSpbRowHeaderViewHolder) {

                        String [] sid = ((TSpbRowHeaderViewHolder) rowHeaderView).getCellId().split("-");
                        selectTransaksiSPB = origin.get(sid[1]);

                        startLongOperation(STATUS_LONG_OPERATION_SELECT_ITEM);

                    }
                }

                @Override
                public void onRowHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    Log.d(TAG, "onRowHeaderDoubleClicked: "+ String.valueOf(row) );
//                    Toast.makeText(getContext(), "onRowHeaderDoubleClicked", Toast.LENGTH_SHORT).show();
                    if (rowHeaderView != null && rowHeaderView instanceof TSpbRowHeaderViewHolder) {

                        String [] sid = ((TSpbRowHeaderViewHolder) rowHeaderView).getCellId().split("-");
                        selectTransaksiSPB = origin.get(sid[1]);

                        startLongOperation(STATUS_LONG_OPERATION_SELECT_ITEM);

                    }
                }

                @Override
                public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    Log.d(TAG, "onRowHeaderLongPressed: "+ String.valueOf(row) );
//                    Toast.makeText(getContext(), "onRowHeaderLongPressed", Toast.LENGTH_SHORT).show();
                    // Create Long Press Popup
//                    RowHeaderLongPressPopup popup = new RowHeaderLongPressPopup(rowHeaderView, mTableView,mainMenuActivity);
                    // Show
//                    popup.show();
                }
            });

            adapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel
                    .getRowHeaderList(), mTableViewModel.getCellList());


//            mTableFilter = new Filter(mTableView);
        }else{
            ltableview.setVisibility(View.GONE);
        }


        tvStartTime.setText(mulai == 0 ? "" : timeFormat.format(mulai));
        tvFinishTime.setText(sampai == 0 ? "" : timeFormat.format(sampai));
        tvTotalJanjang.setText(String.format("%,d",tJanjang) + " JJg");
        tvTotalBrondolan.setText(String.format("%,d",tBrondolan) + " Kg");
        tvTrip.setText(String.valueOf(trip));
        tvUpload.setText(String.valueOf(upload));

        Date d = new Date(System.currentTimeMillis());
        if(sdf.format(d).equals(sdf.format(dateSelected))){
            showNewTranskasi();
        }else{
            llNewData.setVisibility(View.GONE);
        }
    }

    private void cekHapusSpb(){
        selectTransaksiSPB = null;
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_SPB);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", idSelected));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                selectTransaksiSPB = gson.fromJson(dataNitrit.getValueDataNitrit(),TransaksiSPB.class);
                break;
            }
        }
        db.close();
    }

    private void hapuSpb(){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_SPB);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", idSelected));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                selectTransaksiSPB = gson.fromJson(dataNitrit.getValueDataNitrit(),TransaksiSPB.class);

                for(int i = 0 ; i < selectTransaksiSPB.getTransaksiAngkut().getAngkuts().size();i++){
                    Angkut angkut = selectTransaksiSPB.getTransaksiAngkut().getAngkuts().get(i);
                    if(angkut.getTransaksiPanen() != null){
                        TransaksiAngkutHelper.removeIdTPanenHasBeenTap(angkut.getTransaksiPanen().getIdTPanen());
                    }else if(angkut.getTransaksiAngkut() != null){
                        TransaksiAngkutHelper.removeIdTAngkutHasBeenTap(angkut.getTransaksiAngkut().getIdTAngkut());
                    }
                }
                TransaksiAngkut transaksiAngkut = selectTransaksiSPB.getTransaksiAngkut();
                transaksiAngkut.setStatus(TransaksiAngkut.HAPUS);
                selectTransaksiSPB.setTransaksiAngkut(transaksiAngkut);
                selectTransaksiSPB.setStatus(TransaksiSPB.HAPUS);
                dataNitrit.setValueDataNitrit(gson.toJson(selectTransaksiSPB));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();

        db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
        repository = db.getRepository(DataNitrit.class);
        cursor = repository.find(ObjectFilters.eq("idDataNitrit", selectTransaksiSPB.getTransaksiAngkut().getIdTAngkut()));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                TransaksiAngkut selectTransaksiAngkut = gson.fromJson(dataNitrit.getValueDataNitrit(),TransaksiAngkut.class);

                selectTransaksiAngkut.setStatus(TransaksiAngkut.HAPUS);
                dataNitrit.setValueDataNitrit(gson.toJson(selectTransaksiAngkut));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();
    }

    public void hapusNotif(String id,int position){
        idSelected = id;
        posisi = position;
        startLongOperation(STATUS_LONG_OPERATION_SHOW_CONFIRM_DELETED);
    }

    public void startLongOperation(int status){
        new LongOperation().execute(String.valueOf(status));
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        String dataString;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                ((BaseActivity) getActivity()).alertDialogBase.cancel();
            }
            ((BaseActivity) getActivity()).alertDialogBase  = WidgetHelper.showWaitingDialog(getActivity(),getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])) {
                case STATUS_LONG_OPERATION_NONE: {
                    getAllUser = GlobalHelper.getAllUser();
                    updateDataRv(Integer.parseInt(strings[0]));
                    blockMekanisasi = mekanisasiHelper.setBlockMekanisais();
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM:{
                    Gson gson = new Gson();
                    dataString = gson.toJson(selectTransaksiSPB);
                    break;
                }
                case STATUS_LONG_OPERATION_SHOW_CONFIRM_DELETED:{
                    cekHapusSpb();
                    break;
                }
                case STATUS_LONG_OPERATION_DELETED:{
                    hapuSpb();
                    break;
                }
            }

            return strings[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            switch (Integer.parseInt(result)) {
                case STATUS_LONG_OPERATION_NONE: {
                    updateUIRV(Integer.parseInt(result));
                    if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateUIRV(Integer.parseInt(result));
                    if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM:{
                    if(selectTransaksiSPB.getTransaksiAngkut().getMekanisasiPanens() == null){
                        Intent intent = new Intent(getActivity(), SpbActivity.class);
                        intent.putExtra("transaksiSpb","1");
                        GlobalHelper.writeFileContent(GlobalHelper.TRANSFER_TRANSAKSI_SPB, dataString);
                        GlobalHelper.writeFileContent(GlobalHelper.TRANSFER_TRANSAKSI_SPB, dataString);
                        startActivityForResult(intent,GlobalHelper.RESULT_SPBACTIVITY);
                    }else{
                        if(selectTransaksiSPB.getTransaksiAngkut().getMekanisasiPanens().size() >= 1){
                            Intent intent = new Intent(getActivity(), MekanisasiActivity.class);
                            intent.putExtra("transaksiSpb","1");
                            GlobalHelper.writeFileContent(GlobalHelper.TRANSFER_TRANSAKSI_SPB, dataString);
                            startActivityForResult(intent,GlobalHelper.RESULT_MEKANISASIACTIVITY);
                        }else{
                            Intent intent = new Intent(getActivity(), SpbActivity.class);
                            intent.putExtra("transaksiSpb","1");
                            GlobalHelper.writeFileContent(GlobalHelper.TRANSFER_TRANSAKSI_SPB, dataString);
                            startActivityForResult(intent,GlobalHelper.RESULT_SPBACTIVITY);
                        }
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_NEW_DATA:{
                    Intent intent = new Intent(getActivity(), SpbActivity.class);
                    startActivityForResult(intent, GlobalHelper.RESULT_SPBACTIVITY);
                    break;
                }
                case STATUS_LONG_OPERATION_NEW_DATA_MEKANISASI:{
                    Intent intent = new Intent(getActivity(), MekanisasiActivity.class);
                    startActivityForResult(intent, GlobalHelper.RESULT_MEKANISASIACTIVITY);
                    break;
                }
                case STATUS_LONG_OPERATION_SHOW_CONFIRM_DELETED:{
                    if(selectTransaksiSPB != null){
                        if(selectTransaksiSPB.getStatus() == TransaksiSPB.BELUM_UPLOAD){
                            mainMenuHelper.showYesNoQuestion(selectTransaksiSPB);
                        }else{
                            Toast.makeText(HarvestApp.getContext(),"SPB ini tidak bisa dihapus",Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_DELETED:{
                    Objects.requireNonNull(mTableView.getAdapter()).removeRow(posisi);
                    if (((BaseActivity) Objects.requireNonNull(getActivity())).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
            }
        }
    }
}