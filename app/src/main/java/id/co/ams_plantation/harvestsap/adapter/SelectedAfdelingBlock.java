package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.TreeMap;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.AfdelingBlock;
import id.co.ams_plantation.harvestsap.model.Block;

public class SelectedAfdelingBlock extends RecyclerView.Adapter<SelectedAfdelingBlock.Holder>{

    Context context;
    ArrayList<AfdelingBlock> originLists;

    public SelectedAfdelingBlock(Context context, TreeMap<String, ArrayList<Block>> afdelingBlocks){
        this.context = context;
        originLists = new ArrayList<>();
        for ( String key : afdelingBlocks.keySet() ) {
            ArrayList<Block> block = new ArrayList<>();
            for(int i = 0; i < afdelingBlocks.get(key).size();i++){
                if(afdelingBlocks.get(key).get(i).getSelected()){
                    block.add(afdelingBlocks.get(key).get(i));
                }
            }
            if(block.size() > 0){
                AfdelingBlock afdelingBlock = new AfdelingBlock(key,block);
                originLists.add(afdelingBlock);
            }
        }
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.selected_afdelingblock,parent,false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(SelectedAfdelingBlock.Holder holder, int position) {
        holder.setValue(originLists.get(position));
    }

    @Override
    public int getItemCount() {
        return originLists.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView tvAfd;
        TextView tvBlock;

        public Holder(View itemView) {
            super(itemView);
            tvAfd = (TextView) itemView.findViewById(R.id.tvAfd);
            tvBlock = (TextView) itemView.findViewById(R.id.tvBlock);
        }

        public void setValue(final AfdelingBlock value) {
            String bloks = "";
            for(int i = 0 ; i < value.getBlock().size();i++){
                if(i == 0){
                    bloks = value.getBlock().get(i).getBlock();
                }else{
                    bloks += " ," +value.getBlock().get(i).getBlock();
                }
            }
            tvAfd.setText(value.getAfdeling());
            tvBlock.setText(bloks);
        }
    }
}
