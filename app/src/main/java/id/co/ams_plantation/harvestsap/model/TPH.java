package id.co.ams_plantation.harvestsap.model;

import org.dizitart.no2.Document;
import org.dizitart.no2.mapper.Mappable;
import org.dizitart.no2.mapper.NitriteMapper;
import org.dizitart.no2.objects.Id;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by user on 12/3/2018.
 */

public class TPH implements Mappable {

    public static int STATUS_TPH_BELUM_UPLOAD = 0;
    public static int STATUS_TPH_MASTER = 1;
    public static int STATUS_TPH_DELETED = 2;
    public static int STATUS_TPH_INACTIVE_QC_MOBILE = 3;
    public static int STATUS_TPH_UPLOAD = 4;

    //jika createBY = usergissystem maka tph tidak perlu di munculkan
    public static int USER_GIS_SYSTEM = 286;

    public static final String [] LIST_REASON = {
            "Pilih reason",
//            "Dipanen warga",
            "TPH memang tidak dipakai",
            "Kesalahan Pemetaan",
//            "Daerah enclave",
//            "Kondisi tanaman tidak homogen",
            "Lain - lain"
    };

    @Id
    String noTph;
    String namaTph;
    //param1
    String block;
    //param2
    String afdeling;
    String estCode;
    String ancak;
    Double Longitude;
    Double Latitude;
    //param3
    int createBy;
    long createDate;
    int updateBy;
    long updateDate;
    int status;
    int resurvey;
    int panen3Bulan;
    String remarks;
    ReasonUnharvestTPH reasonUnharvestTPH;
    ArrayList<Integer> baris;
    HashSet<String> foto;

    public TPH(){}

    public TPH(String noTph, String namaTph, String block, String afdeling, String estCode,String ancak, Double longitude, Double latitude, int createBy, long createDate, int updateBy, long updateDate, int status, HashSet<String> foto,int resurvey) {
        this.noTph = noTph;
        this.namaTph = namaTph;
        this.block = block;
        this.afdeling = afdeling;
        this.estCode = estCode;
        this.ancak = ancak;
        Longitude = longitude;
        Latitude = latitude;
        this.createBy = createBy;
        this.createDate = createDate;
        this.updateBy = updateBy;
        this.updateDate = updateDate;
        this.status = status;
        this.foto = foto;
        this.resurvey = resurvey;
    }

    public int getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(int updateBy) {
        this.updateBy = updateBy;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public HashSet<String> getFoto() {
        return foto;
    }

    public void setFoto(HashSet<String> foto) {
        this.foto = foto;
    }

    public String getNamaTph() {
        return namaTph;
    }

    public void setNamaTph(String namaTph) {
        this.namaTph = namaTph;
    }

    public int getCreateBy() {
        return createBy;
    }

    public void setCreateBy(int createBy) {
        this.createBy = createBy;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public String getNoTph() {
        return noTph;
    }

    public void setNoTph(String noTph) {
        this.noTph = noTph;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getAncak() {
        return ancak;
    }

    public void setAncak(String ancak) {
        this.ancak = ancak;
    }

    public ArrayList<Integer> getBaris() {
        return baris;
    }

    public void setBaris(ArrayList<Integer> baris) {
        this.baris = baris;
    }

    public int getResurvey() {
        return resurvey;
    }

    public void setResurvey(int resurvey) {
        this.resurvey = resurvey;
    }

    public int getPanen3Bulan() {
        return panen3Bulan;
    }

    public void setPanen3Bulan(int panen3Bulan) {
        this.panen3Bulan = panen3Bulan;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public ReasonUnharvestTPH getReasonUnharvestTPH() {
        return reasonUnharvestTPH;
    }

    public void setReasonUnharvestTPH(ReasonUnharvestTPH reasonUnharvestTPH) {
        this.reasonUnharvestTPH = reasonUnharvestTPH;
    }

    @Override
    public Document write(NitriteMapper mapper) {
        Document document = new Document();
        document.put("noTph", noTph);
        document.put("namaTph", namaTph);
        document.put("block", block);
        document.put("afdeling", afdeling);
        document.put("estCode", estCode);
        document.put("Longitude", Longitude);
        document.put("Latitude", Latitude);
        document.put("createBy", createBy);
        document.put("createDate", createDate);
        document.put("updateBy", updateBy);
        document.put("updateDate", updateDate);
        document.put("status", status);
        document.put("foto", foto);
        return document;
    }

    @Override
    public void read(NitriteMapper mapper, Document document) {
        noTph = (String) document.get("noTph");
        namaTph = (String) document.get("namaTph");
        block = (String) document.get("block");
        afdeling = (String) document.get("afdeling");
        estCode = (String) document.get("estCode");
        Longitude = (Double) document.get("Longitude");
        Latitude = (Double) document.get("Latitude");
        createBy = (int) document.get("createBy");
        updateBy = (int) document.get("updateBy");
        updateDate = (long) document.get("updateDate");
        status = (int) document.get("status");
        foto = (HashSet<String>) document.get("foto");
    }

//    public HashSet<String> getListAncak() {
//        return listAncak;
//    }
//
//    public void setListAncak(HashSet<String> listAncak) {
//        this.listAncak = listAncak;
//    }
}
