package id.co.ams_plantation.harvestsap.ui;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import butterknife.BindView;
import id.co.ams_plantation.harvestsap.Fragment.AngkutInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.LangsiranInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.Estate;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.model.TranskasiAngkutVersion;
import id.co.ams_plantation.harvestsap.presenter.AngkutPresenter;
import id.co.ams_plantation.harvestsap.service.BluetoothService;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.NfcHelper;
import id.co.ams_plantation.harvestsap.util.QrHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import id.co.ams_plantation.harvestsap.view.AngkutView;

import static id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper.createNewAngkut;

/**
 * Created by user on 12/26/2018.
 */

public class AngkutActivity extends BaseActivity implements AngkutView {

    public AngkutPresenter presenter;
    public Estate estate;
    public TransaksiAngkut selectedTransaksiAngkut;
    public TransaksiAngkut transaksiAngkutFromNFC;
    public TransaksiPanen selectedTransaksiPanen;
    public Angkut selectedAngkut;
    public TransaksiAngkut sigmaAngkut;
    public QrHelper qrHelper;
    public HashMap<String,Angkut> originAngkut;
    public HashMap<String, Angkut> originRekonsilasi;
    public boolean closeTransaksi;
    public ArrayList<TransaksiAngkut> batchKartuAngkut;
    public int idxTap = 0;
    public boolean dariAngkut;

//    public ArrayList<Langsiran> angkutLangsiran;

    Activity activity;
    public int TYPE_NFC;
    public String valueNFC;

//    public LocationManager mLocationManager;
//    public Location currentlocation;

    @BindView(R.id.myCoordinatorLayout)
    public CoordinatorLayout myCoordinatorLayout;
    public Snackbar searchingGPS;

    /****************************************** NFC ******************************************************************/
    public Tag myTag;
    public NfcAdapter nfcAdapter;
    public PendingIntent pendingIntent;
    public IntentFilter[] writeTagFilters;
    private boolean isRekonsiliasi=false;

//    public HashMap<String,User> dataAllUser;
//    public HashMap<String,PKS> dataAllPks;
//    public HashMap<String,Langsiran> dataAllLagsiran;
//    public HashMap<String,Operator> dataOperator;
//    public HashMap<String,Vehicle> dataVehicle;
//    public ArrayList<User> users;
//    public ArrayList<PKS> pkss;
//    public ArrayList<Langsiran> langsirans;
//    public ArrayList<Operator> operators;
//    public ArrayList<Vehicle> vehicles;

    @Override
    protected void initPresenter() {
        presenter = new AngkutPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_layout);
        getSupportActionBar().hide();

        Intent intent = getIntent();
        if(intent.getExtras() != null) {
            String transaksi = intent.getExtras().getString("transaksiAngkut");
            if (transaksi != null) {
                File file = new File(GlobalHelper.TRANSFER_TRANSAKSI_ANGKUT);
                if(file.exists()){
                    transaksi = GlobalHelper.readFileContent(GlobalHelper.TRANSFER_TRANSAKSI_ANGKUT);
                    file.delete();
                }
                Log.d("transaksiAngkut",transaksi);
                TransaksiAngkutHelper.hapusFileAngkut(false);

                Gson gson = new Gson();

                selectedTransaksiAngkut = gson.fromJson(transaksi, TransaksiAngkut.class);
                createNewAngkut(selectedTransaksiAngkut);

                if(selectedTransaksiAngkut.getTranskasiAngkutVersions() != null && selectedTransaksiAngkut.getStatus() == TransaksiAngkut.TAP){
                    boolean bisaEdit = true;
                    for(int i =0;i < selectedTransaksiAngkut.getTranskasiAngkutVersions().size();i++){
                        TranskasiAngkutVersion transkasiAngkutVersion = selectedTransaksiAngkut.getTranskasiAngkutVersions().get(i);
                        if(transkasiAngkutVersion.getNextIdTransaksi() != null){
                            bisaEdit = false;
                            break;
                        }
                    }

                    if(!bisaEdit){
                        selectedTransaksiAngkut.setStatus(TransaksiAngkut.UPLOAD);
                    }
                }
                if (selectedTransaksiAngkut.getAngkuts() != null) {
                    selectedTransaksiAngkut.setAngkuts(TransaksiAngkutHelper.cekAngkutUniq(selectedTransaksiAngkut.getAngkuts()));
                    Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT);
                    ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                    for(int i = 0 ; i < selectedTransaksiAngkut.getAngkuts().size(); i++){
                        if(selectedTransaksiAngkut.getAngkuts().get(i).getIdAngkut() == null){
                            break;
                        }
                        DataNitrit dataNitrit = new DataNitrit(selectedTransaksiAngkut.getAngkuts().get(i).getIdAngkut(),
                                gson.toJson(selectedTransaksiAngkut.getAngkuts().get(i)));
                        repository.insert(dataNitrit);
                    }
                    db.close();
                }

//                if(cekDb){
//                    boolean adaDiDb = false;
//                    db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
//                    repository = db.getRepository(DataNitrit.class);
//                    Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", selectedTransaksiAngkut.getIdTAngkut()));
//                    if(cursor.size() > 0) {
//                        for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
//                            DataNitrit dataNitrit = (DataNitrit) iterator.next();
//                            selectedTransaksiAngkut = gson.fromJson(dataNitrit.getValueDataNitrit(),TransaksiAngkut.class);
//                            adaDiDb = true;
//                            break;
//                        }
//                    }
//                    db.close();
//
//                    if(adaDiDb) {
//                        db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT);
//                        repository = db.getRepository(DataNitrit.class);
//                        for (int i = 0; i < selectedTransaksiAngkut.getAngkuts().size(); i++) {
//                            DataNitrit dataNitrit = new DataNitrit(selectedTransaksiAngkut.getAngkuts().get(i).getIdAngkut(),
//                                    gson.toJson(selectedTransaksiAngkut.getAngkuts().get(i)));
//                            repository.insert(dataNitrit);
//                        }
//                        db.close();
//                    }else{
//                        Toast.makeText(HarvestApp.getContext(),"Transkasi Angkut Ini Tidak Ada Di DB",Toast.LENGTH_SHORT).show();
//                        closeActivity();
//                    }
//                }
            }

            isRekonsiliasi = intent.getExtras().getBoolean("rekonsiliasiOnly");
        }

//        dataAllUser = GlobalHelper.getAllUser();pemanetph
//        users = new ArrayList<>();
//        for(Map.Entry<String,User> e : dataAllUser.entrySet()) {
//            String key = e.getKey();
//            User value = e.getValue();
//            users.add(value);
//        }
//
//        dataAllPks = GlobalHelper.getAllPks();
//        pkss = new ArrayList<>();
//        for(Map.Entry<String,PKS> e : dataAllPks.entrySet()) {
//            String key = e.getKey();
//            PKS value = e.getValue();
//            pkss.add(value);
//        }
//
//        dataAllLagsiran = GlobalHelper.getAllLangsiran();
//        langsirans = new ArrayList<>();
//        for(Map.Entry<String,Langsiran> e : dataAllLagsiran.entrySet()) {
//            String key = e.getKey();
//            Langsiran value = e.getValue();
//            langsirans.add(value);
//        }
//
//        dataOperator = GlobalHelper.getAllOperator();
//        operators = new ArrayList<>();
//        for(Map.Entry<String,Operator> e : dataOperator.entrySet()) {
//            String key = e.getKey();
//            Operator value = e.getValue();
//            operators.add(value);
//        }
//
//        dataVehicle = GlobalHelper.getAllVehicle();
//        vehicles = new ArrayList<>();
//        for(Map.Entry<String,Vehicle> e : dataVehicle.entrySet()) {
//            String key = e.getKey();
//            Vehicle value = e.getValue();
//            vehicles.add(value);
//        }

        qrHelper = new QrHelper(this);
        activity = this;

        String strEst =  Encrypts.decrypt(GlobalHelper.getFileContent(Encrypts.encrypt(GlobalHelper.SELECTED_ESTATE)));
        Gson gson = new Gson();
        estate = (Estate) gson.fromJson(strEst, Estate.class);
////      set di baseActivity
//        createLocationListener();
        toolBarSetup();

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
        }
        NfcHelper.readFromIntent(getIntent());
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[] { tagDetected };
//        dataAllUser = GlobalHelper.getAllUser();
        main();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        closeTransaksi = false;
    }

    private void main() {
        if(isRekonsiliasi){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_container, AngkutInputFragment.getInstance(isRekonsiliasi))
                    .commit();
        }else{
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_container, AngkutInputFragment.getInstance())
                    .commit();
        }

    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        TextView textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backProses();
            }
        });
        if(isRekonsiliasi){
            textToolbar.setText(getResources().getString(R.string.rekonsiliasi));
        }else{
            textToolbar.setText(getResources().getString(R.string.angkut));
        }

    }

    public void backProses() {
        Log.i("BackPRoses", "backProses: BackProses click");
        if (getSupportFragmentManager().findFragmentById(R.id.content_container)
                instanceof TphInputFragment) {
            getSupportFragmentManager().popBackStack();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_container, AngkutInputFragment.getInstance())
                    .commit();
            Log.d(this.getClass() + " backProses", "Dari TphInputFragment");
        }else if (getSupportFragmentManager().findFragmentById(R.id.content_container)
                instanceof LangsiranInputFragment) {
            getSupportFragmentManager().popBackStack();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_container, AngkutInputFragment.getInstance())
                    .commit();
            Log.d(this.getClass() + " backProses", "Dari LangsiranInputFragment");
        } else if (getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof  AngkutInputFragment){
            AngkutInputFragment fragment = (AngkutInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
            fragment.closeAngkut();
            Log.d(this.getClass() + " backProses", "Dari AngkutInputFragment");
        }
    }

    public void closeActivity(){
        TransaksiAngkutHelper.hapusFileAngkut(true);
        setResult(GlobalHelper.RESULT_ANGKUTACTIVITY);
        finish();
    }

    private void createLocationListener() {
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

//        searchingGPS = Snackbar.make(myCoordinatorLayout,getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
//        searchingGPS.show();
        WidgetHelper.warningFindGps(this,myCoordinatorLayout);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                1f, mLocationListener);
    }

//    public final LocationListener mLocationListener = new LocationListener() {
//        @Override
//        public void onLocationChanged(final Location location) {
//            currentlocation = location;
//            if(searchingGPS!=null){
//                if(currentlocation.getAccuracy()>MAX_ACCURACY){
//                    if (!searchingGPS.isShown()) {
//                        searchingGPS = Snackbar.make(myCoordinatorLayout, getResources().getString(R.string.gps_not_accurate), Snackbar.LENGTH_INDEFINITE).setActionTextColor(Color.YELLOW);
//                        //Log.e("curr acc",location.getAccuracy()+" m");
//                        searchingGPS.show();
//                    }
//                }else{
//                    if (searchingGPS.isShown()) {
//                        searchingGPS.dismiss();
//                    }
//                }
//            }
//            Log.v("change Location",currentlocation.getLongitude()+","+currentlocation.getLatitude());
//        }
//
//        @Override
//        public void onStatusChanged(String s, int i, Bundle bundle) {
//            Log.v("onStatusChanged L",s);
//        }
//
//        @Override
//        public void onProviderEnabled(String s) {
//            Log.v("onProviderEnabled L",s);
//        }
//
//        @Override
//        public void onProviderDisabled(String s) {
//            Log.v("onProviderDisabled L",s);
//        }
//    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode){
            case GlobalHelper.RESULT_SCAN_QR:{
                String isiQr = data.getExtras().getString(QRScan.NILAI_QR_SCAN);
                cekDataPassing(isiQr,this);
                break;
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // If Bluetooth is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!bluetoothHelper.mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, GlobalHelper.REQUEST_ENABLE_BT);
            // Otherwise, setup the session
        } else {
            if (bluetoothHelper.mService == null)
                bluetoothHelper.mService = new BluetoothService(this, bluetoothHelper.mHandler);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        }
        valueNFC = NfcHelper.readFromIntent(intent);
        int formatNFC = NfcHelper.cekFormatNFC(valueNFC);
        if (valueNFC != null) {
            if (formatNFC != GlobalHelper.TYPE_NFC_SALAH) {
                if(valueNFC.length() == 5) {
                    if(NfcHelper.stat != NfcHelper.NFC_TIDAK_BISA_WRITE){
                        if (getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof AngkutInputFragment) {
                            AngkutInputFragment fragment = (AngkutInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                            fragment.startLongOperation(NfcHelper.stat);
                        }else if(getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof  LangsiranInputFragment){
                            LangsiranInputFragment fragment = (LangsiranInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                            fragment.new LongOperation().execute(String.valueOf(fragment.LongOperation_Save_T2_NFC));
                        }
                    }
                    Toast.makeText(this, getResources().getString(R.string.nfc_empty) + " " + GlobalHelper.LIST_NFC[formatNFC], Toast.LENGTH_SHORT).show();
                }else{
                    if(getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof LangsiranInputFragment) {
                        LangsiranInputFragment fragment = (LangsiranInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                        if (NfcHelper.stat == fragment.LongOperation_Save_T2_NFC) {
                            fragment.new LongOperation().execute(String.valueOf(fragment.LongOperation_Save_T2_NFC));
                        }
                    }else if (!closeTransaksi) {
                        cekDataPassing(valueNFC, this);
                    } else {
                        try {
                            if(NfcHelper.stat == 999){
                                cekDataPassing(valueNFC, this);
                            }else {
                                TransaksiAngkut transaksiAngkut = TransaksiAngkutHelper.toDecompre(new JSONObject(valueNFC));
                                if (TransaksiAngkutHelper.cekDataLangsiranUpdate(this, transaksiAngkut)) {
                                    AngkutInputFragment fragment = (AngkutInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                    fragment.startLongOperation(NfcHelper.stat);
//                                    NfcHelper.write(GlobalHelper.FORMAT_NFC_ANGKUT, myTag,true);
//                                    fragment.startLongOperation(fragment.STATUS_LONG_OPERATION_SAVE_DATA_TAP_NFC);
                                } else {
                                    Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.diffrent_angkut), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }catch (Exception e){
                            Log.d(this.getClass()+ " update kartu angkut",e.toString());
                            Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_not_empty), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                TYPE_NFC = formatNFC;
            }else {
                Toast.makeText(this,R.string.format_not_valid,Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this,R.string.format_not_valid,Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed() {
        backProses();
    }

    @Override
    protected void onResume() {
        super.onResume();
        NfcHelper.WriteModeOn(nfcAdapter,this,pendingIntent,writeTagFilters);
        if(bluetoothHelper != null) {
            if (bluetoothHelper.mService != null) {

                if (bluetoothHelper.mService.getState() == BluetoothService.STATE_NONE) {
                    // Start the Bluetooth services
                    bluetoothHelper.mService.start();
                }
            }
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        NfcHelper.WriteModeOff(nfcAdapter,this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth services
        if (bluetoothHelper != null) {
            if (bluetoothHelper.mService != null)
                bluetoothHelper.mService.stop();
        }
    }
}