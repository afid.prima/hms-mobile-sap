package id.co.ams_plantation.harvestsap.util;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.SelectCagesAdapter;
import id.co.ams_plantation.harvestsap.model.Cages;

public class CagesHelper {

    Context context;
    SelectCagesAdapter adapterCages;
    ArrayList<Cages> listCages;

    public CagesHelper(Context context, ArrayList<Cages> listCages) {
        this.context = context;
        this.listCages = listCages;
        this.adapterCages = new SelectCagesAdapter(context, R.layout.row_setting, listCages);
    }

    public void setNewCages(Cages dataCages) {

        View baseView = LayoutInflater.from(context).inflate(R.layout.cages_layout, null);
        CompleteTextViewHelper assetNo = baseView.findViewById(R.id.assetNo);
        TextView cagesName = baseView.findViewById(R.id.cagesNames);

        assetNo.setThreshold(1);
        assetNo.setAdapter(adapterCages);
        adapterCages.notifyDataSetChanged();

        assetNo.setOnClickListener(view -> assetNo.showDropDown());

        assetNo.setOnItemClickListener((parent, view, position, id) -> {
            Cages selectedCage = adapterCages.getItem(position);
            cagesName.setText(selectedCage.getAssetName());
        });
    }
}
