package id.co.ams_plantation.harvestsap.model;

public class DynamicParameterPenghasilan {
    public static final int isCagesHide = 0;
    public static final int isCagesShow = 1;
    public static final int isCagesQrOnly = 2;

    private String estCode;
    private Long updateBy;
    private Double janjangNormal;
    private Double buahMentah;
    private Double busukNJangkos;
    private Double buahLewatMatang;
    private Double buahAbnormal;
    private Double tangkaiPanjang;
    private Double buahDimakanTikus;
    private Double brondolan;
    private int persenOperator;
    private int persenPemanen;
    private String lastUpdate;
    private String uploadFotoTransaksi;
    private int maxRestan;

    private int typeVehicleMekanisasi;
    private int isCages;

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Double getJanjangNormal() {
        return janjangNormal;
    }

    public void setJanjangNormal(Double janjangNormal) {
        this.janjangNormal = janjangNormal;
    }

    public Double getBuahMentah() {
        return buahMentah;
    }

    public void setBuahMentah(Double buahMentah) {
        this.buahMentah = buahMentah;
    }

    public Double getBusukNJangkos() {
        return busukNJangkos;
    }

    public void setBusukNJangkos(Double busukNJangkos) {
        this.busukNJangkos = busukNJangkos;
    }

    public Double getBuahLewatMatang() {
        return buahLewatMatang;
    }

    public void setBuahLewatMatang(Double buahLewatMatang) {
        this.buahLewatMatang = buahLewatMatang;
    }

    public Double getBuahAbnormal() {
        return buahAbnormal;
    }

    public void setBuahAbnormal(Double buahAbnormal) {
        this.buahAbnormal = buahAbnormal;
    }

    public Double getTangkaiPanjang() {
        return tangkaiPanjang;
    }

    public void setTangkaiPanjang(Double tangkaiPanjang) {
        this.tangkaiPanjang = tangkaiPanjang;
    }

    public Double getBuahDimakanTikus() {
        return buahDimakanTikus;
    }

    public void setBuahDimakanTikus(Double buahDimakanTikus) {
        this.buahDimakanTikus = buahDimakanTikus;
    }

    public Double getBrondolan() {
        return brondolan;
    }

    public void setBrondolan(Double brondolan) {
        this.brondolan = brondolan;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public int getPersenOperator() {
        return persenOperator;
    }

    public void setPersenOperator(int persenOperator) {
        this.persenOperator = persenOperator;
    }

    public int getPersenPemanen() {
        return persenPemanen;
    }

    public void setPersenPemanen(int persenPemanen) {
        this.persenPemanen = persenPemanen;
    }

    public String getUploadFotoTransaksi() {
        return uploadFotoTransaksi;
    }

    public void setUploadFotoTransaksi(String uploadFotoTransaksi) {
        this.uploadFotoTransaksi = uploadFotoTransaksi;
    }

    public int getMaxRestan() {
        return maxRestan;
    }

    public void setMaxRestan(int maxRestan) {
        this.maxRestan = maxRestan;
    }

    public int getTypeVehicleMekanisasi() {
        return typeVehicleMekanisasi;
    }

    public void setTypeVehicleMekanisasi(int typeVehicleMekanisasi) {
        this.typeVehicleMekanisasi = typeVehicleMekanisasi;
    }

    public int getIsCages() {
        return isCages;
    }

    public void setIsCages(int isCages) {
        this.isCages = isCages;
    }
}
