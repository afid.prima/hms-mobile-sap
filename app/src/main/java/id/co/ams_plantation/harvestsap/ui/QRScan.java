package id.co.ams_plantation.harvestsap.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.Result;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by user on 11/30/2018.
 */

public class QRScan extends Activity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;
    public static String  NILAI_QR_SCAN = "nilaiQrScan";

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result result) {
        Intent intent = new Intent();
        String nilai = GlobalHelper.decompress(result.getText());
        if(nilai == null){
            Toast.makeText(this,R.string.format_not_valid,Toast.LENGTH_SHORT).show();
        }else {
            Log.d("Isi Qr",nilai);
            intent.putExtra(NILAI_QR_SCAN, nilai);
            setResult(GlobalHelper.RESULT_SCAN_QR, intent);
            finish();
        }
    }
}
