package id.co.ams_plantation.harvestsap.TableView.view_model;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewColumnHeader;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestsap.model.Angkut;

/**
 * Created by user on 12/27/2018.
 */

public class AngkutTableViewModel {
    private final Context mContext;
    private final ArrayList<Angkut> angkuts;
    String [] HeaderColumn = {"TPH / Langsiran","Block","Janjang","Brondolan","Berat Estimasi","BJR","OPH","ID NFC"};
    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

    public AngkutTableViewModel(Context context, HashMap<String,Angkut> hAngkut) {
        mContext = context;
        angkuts = new ArrayList<>();
        angkuts.addAll(hAngkut.values());
        Collections.sort(angkuts, new Comparator<Angkut>() {
            @Override
            public int compare(Angkut t0, Angkut t1) {
                return t0.getIdAngkut().compareTo(t1.getIdAngkut());
            }
        });
    }

    private List<TableViewRowHeader> getSimpleRowHeaderList() {
        List<TableViewRowHeader> list = new ArrayList<>();
        //"angkut"
        for (int i = 0; i < angkuts.size(); i++) {
            String nama = "-manual";
            if(angkuts.get(i).getTransaksiPanen() != null){
                nama = "-angkut";
            }else if (angkuts.get(i).getTransaksiAngkut() != null){
                nama = "-transit";
            }
            String id = i + "-" + angkuts.get(i).getIdAngkut() + nama;
            int no = i + 1;
            TableViewRowHeader header = new TableViewRowHeader(id, String.valueOf(no));
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<TableViewColumnHeader> getRandomColumnHeaderList() {
        List<TableViewColumnHeader> list = new ArrayList<>();

        for (int i = 0; i < HeaderColumn.length; i++) {
            TableViewColumnHeader header = new TableViewColumnHeader(String.valueOf(i), HeaderColumn[i]);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<List<TableViewCell>> getCellListForSortingTest() {
        List<List<TableViewCell>> list = new ArrayList<>();
        for (int i = 0; i < angkuts.size(); i++) {
            List<TableViewCell> cellList = new ArrayList<>();
            for (int j = 0; j < HeaderColumn.length; j++) {
                // Create dummy id.
                String id = j + "-" + angkuts.get(i).getIdAngkut() + "-Angkut";

                TableViewCell cell = null;
                switch (j){
                    case 0: {
                        String nama = "Manual";
                        if (angkuts.get(i).getTransaksiPanen() != null) {
                            nama = angkuts.get(i).getTransaksiPanen().getTph().getNamaTph();
                            if (nama == null) {
                                nama = "TPH";
                            }
                        } else if (angkuts.get(i).getTph() != null) {
                            if (angkuts.get(i).getTph().getNamaTph() != null) {
                                nama = angkuts.get(i).getTph().getNamaTph();
                            }
                        } else if (angkuts.get(i).getTransaksiAngkut() != null) {
                            if (angkuts.get(i).getTransaksiAngkut().getLangsiran() != null) {
                                nama = angkuts.get(i).getTransaksiAngkut().getLangsiran().getNamaTujuan() + " - " + angkuts.get(i).getTransaksiAngkut().getLangsiran().getBlock();
                            } else {
                                nama = "Langsiran";
                            }
                        } else if (angkuts.get(i).getLangsiran() != null) {
                            if (angkuts.get(i).getLangsiran().getNamaTujuan() != null) {
                                nama = angkuts.get(i).getLangsiran().getNamaTujuan() + " - " + angkuts.get(i).getLangsiran().getBlock();
                            }
                        }
                        cell = new TableViewCell(id, nama);
                        break;
                    }
                    case 1: {
                        String nama = "-";
                        if (angkuts.get(i).getTransaksiPanen() != null) {
                            if (angkuts.get(i).getTransaksiPanen().getTph() != null) {
                                if (angkuts.get(i).getTransaksiPanen().getTph().getBlock() != null) {
                                    nama = angkuts.get(i).getTransaksiPanen().getTph().getBlock();
                                }
                            }
                        } else if (angkuts.get(i).getTph() != null) {
                            if (angkuts.get(i).getTph().getBlock() != null) {
                                nama = angkuts.get(i).getTph().getBlock();
                            }
                        } else if (angkuts.get(i).getTransaksiAngkut() != null) {
                            if (angkuts.get(i).getTransaksiAngkut().getLangsiran() != null) {
                                if (angkuts.get(i).getBlock() != null) {
                                    nama = angkuts.get(i).getBlock();
                                }
                            }
                        } else if (angkuts.get(i).getLangsiran() != null) {
                            if (angkuts.get(i).getLangsiran().getBlock() != null) {
                                nama = angkuts.get(i).getLangsiran().getBlock();
                            }
                        }
                        cell = new TableViewCell(id, nama);
                        break;
                    }
                    case 2: {
                        cell = new TableViewCell(id, angkuts.get(i).getJanjang());
                        break;
                    }
                    case 3: {
                        cell = new TableViewCell(id, angkuts.get(i).getBrondolan());
                        break;
                    }
                    case 4: {
                        cell = new TableViewCell(id, String.format("%.1f", angkuts.get(i).getBerat()));
                        break;
                    }
                    case 5: {
                        cell = new TableViewCell(id, String.format("%.1f", angkuts.get(i).getBjr()));
                        break;
                    }
                    case 6: {
                        cell = new TableViewCell(id, angkuts.get(i).getOphCount() != 0 ? angkuts.get(i).getOphCount() : "");
                        break;
                    }
                    case 7: {
                        String idNfc = "-";
                        if(angkuts.get(i).getIdNfc() != null) {
                            if (angkuts.get(i).getIdNfc().size() > 0) {
                                for (String s : angkuts.get(i).getIdNfc()) {
                                    idNfc = s;
                                }
                            }
                        }
                        cell = new TableViewCell(id, idNfc);
                        break;
                    }
                }
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }

    private static String getRandomString() {
        Random r = new Random();
        String str = " a ";
        for (int i = 0; i < r.nextInt(); i++) {
            str = str + " a ";
        }

        return str;
    }

    public List<List<TableViewCell>> getCellList() {
        return getCellListForSortingTest();
    }

    public List<TableViewRowHeader> getRowHeaderList() {
        return getSimpleRowHeaderList();
    }

    public List<TableViewColumnHeader> getColumnHeaderList() {
        return getRandomColumnHeaderList();
    }

}
