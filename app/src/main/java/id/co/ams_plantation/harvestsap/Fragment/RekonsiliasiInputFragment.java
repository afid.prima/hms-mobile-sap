package id.co.ams_plantation.harvestsap.Fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.labo.kaji.fragmentanimations.MoveAnimation;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.adapter.RekonsilasiAdapter;
import id.co.ams_plantation.harvestsap.TableView.holder.AngkutRowHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestsap.TableView.view_model.RekonsilasiTableViewModel;
import id.co.ams_plantation.harvestsap.adapter.SelectLangsiranAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectOperatorAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectPksAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectUserAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectVehicleAdapter;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.Langsiran;
import id.co.ams_plantation.harvestsap.model.Operator;
import id.co.ams_plantation.harvestsap.model.PKS;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.model.Vehicle;
import id.co.ams_plantation.harvestsap.service.BluetoothService;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.RekonsilasiActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.PksHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiSpbHelper;
import id.co.ams_plantation.harvestsap.util.TujuanHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;

public class RekonsiliasiInputFragment extends Fragment {
    public static final int STATUS_LONG_OPERATION_NONE = 0;
    public static final int STATUS_LONG_OPERATION_SEARCH = 1;

    public TujuanHelper tujuanHelper;
    public PksHelper pksHelper;
    TransaksiAngkutHelper transaksiAngkutHelper;
    TransaksiSpbHelper transaksiSpbHelper;
    BaseActivity baseActivity;
    RekonsilasiActivity rekonsilasiActivity;

    RekonsilasiAdapter rekonsilasiAdapter;
    RekonsilasiTableViewModel mRekonsilasiTableViewModel;
    TableView mTableView;
    //    Filter mTableFilter;
    CompleteTextViewHelper etSearch;
    Button btnSearch;
    Button btnClsoe;
    RelativeLayout ltableview;

    //    TextView tvAngkutId;
    TextView tvCreateTime;
    BetterSpinner lsTujuan;
    BetterSpinner lsKendaraan;
    CompleteTextViewHelper etVehicle;
    CompleteTextViewHelper etDriver;
    //tambahan zendi
    TextView tvJanjangTitle;
    TextView tvBrondolanTitle;
    //
    TextView tvTotalOph;
    TextView tvTotalTph;
    TextView tvTotalJanjang;
    TextView tvTotalBrondolan;
    TextView tvTotalBeratEstimasi;
    Button btnAddTujuanPoint;
    Button btnAddPksPoint;
    //tambahan zendi
    LinearLayout lLangsiran;
    LinearLayout lPks;
    LinearLayout lKeluar;
    TextView tvSpbID;
    TextView tvSpbIDKeluar;
    CompleteTextViewHelper tvLangsiran;
    CompleteTextViewHelper tvPks;
    CompleteTextViewHelper etApprove;
    CompleteTextViewHelper etApproveKeluar;

    Vehicle vehicleUse;
    User approveBy;
    int tJanjang = 0;
    int tBrondolan = 0;
    double tBeratEstimasi = 0.0;
    int tKartuHilangDef=0;
    int tKartuRusakDef=0;
    int tKartuHilang=0;
    int tKartuRusak=0;
    int tujuanAngkut=0;
    int kendaraanDari=0;
    double latitude;
    double longitude;

    String idSpb;

    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm");
    SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");

//    JSONObject objTAngkut;
    TransaksiAngkut selectedTransaksiAngkut;
    TransaksiSPB selectedTransaksiSPB;
    Langsiran selectedLangsiran;
    PKS selectedPks;
    Operator selectedOperator;
    SelectUserAdapter adapterAssAfd;
    SelectPksAdapter adapterPks;
    SelectLangsiranAdapter adapterLangsiran;
    SelectOperatorAdapter adapterOperator;
    SelectVehicleAdapter adapterVehicle;
    AlertDialog alertDialog;
    boolean popupTPH;
    List<String> bAlas;
    private ArrayList<Angkut> rekonsiliasiAngkuts;
    Set<String> searchList;
    Set<String> listSearch;
    private final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    HashMap<String,Angkut> angkutView;

    public static RekonsiliasiInputFragment getInstance() {
        RekonsiliasiInputFragment inputFragment = new RekonsiliasiInputFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("rekonsiliasiOnly", true);
        inputFragment.setArguments(bundle);
        return inputFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        searchList = new android.support.v4.util.ArraySet<>();
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return MoveAnimation.create(MoveAnimation.UP,enter,600);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rekonsiliasi_layout, null, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        baseActivity = (BaseActivity) getActivity();
        rekonsilasiActivity = (RekonsilasiActivity) getActivity();

        tujuanHelper = new TujuanHelper(getActivity());
        pksHelper = new PksHelper(getActivity());

//        tvAngkutId = view.findViewById(R.id.tvAngkutId);
        tvCreateTime = view.findViewById(R.id.tvCreateTime);
        etVehicle = view.findViewById(R.id.etVehicle);
        lsTujuan = view.findViewById(R.id.lsTujuan);
        lsKendaraan = view.findViewById(R.id.lsKendaraan);
        etDriver = view.findViewById(R.id.etDriver);
        lLangsiran = view.findViewById(R.id.lLangsiran);
        lPks = view.findViewById(R.id.lPks);
        lKeluar = view.findViewById(R.id.lKeluar);
        tvLangsiran = view.findViewById(R.id.tvLangsiran);
        tvPks = view.findViewById(R.id.tvPks);
        btnAddTujuanPoint = view.findViewById(R.id.btnAddTujuanPoint);
        btnAddPksPoint = view.findViewById(R.id.btnAddPksPoint);
        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        btnClsoe = view.findViewById(R.id.btnClsoe);
        ltableview = view.findViewById(R.id.ltableview);
        mTableView = view.findViewById(R.id.tableview);
        tvBrondolanTitle = view.findViewById(R.id.tvBrondolTitle);
        tvJanjangTitle = view.findViewById(R.id.tvJanjangTitle);

        tvTotalOph = view.findViewById(R.id.tvTotalOph);
        tvTotalTph = view.findViewById(R.id.tvTotalTph);
        tvTotalJanjang = view.findViewById(R.id.tvTotalJanjang);
        tvTotalBrondolan = view.findViewById(R.id.tvTotalBrondolan);
        tvTotalBeratEstimasi = view.findViewById(R.id.tvTotalBeratEstimasi);

        etApprove = view.findViewById(R.id.etApprove);
        etApproveKeluar = view.findViewById(R.id.etApproveKeluar);
        tvSpbID = view.findViewById(R.id.tvSpbID);
        tvSpbIDKeluar = view.findViewById(R.id.tvSpbIDKeluar);

        transaksiAngkutHelper = new TransaksiAngkutHelper(getActivity());
        transaksiSpbHelper = new TransaksiSpbHelper(getActivity());
        lLangsiran.setVisibility(View.GONE);
        lPks.setVisibility(View.GONE);

        //karena untuk mendaftarkan langsiran dan pks butuh team khusu dan terpisah
        btnAddTujuanPoint.setVisibility(View.GONE);
        btnAddPksPoint.setVisibility(View.GONE);

        popupTPH= false;

        bAlas = new ArrayList<>();
        bAlas.add(TransaksiAngkut.PKS);
        bAlas.add(TransaksiAngkut.LANGSIR);
        bAlas.add(TransaksiAngkut.KELAUR);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, bAlas);
        lsTujuan.setAdapter(adapter);
        lsTujuan.setText(bAlas.get(TransaksiAngkut.TUJUAN_KELUAR));

        List<String> bKendaraan = new ArrayList<>();
        bKendaraan.add(TransaksiAngkut.KEBUN);
        bKendaraan.add(TransaksiAngkut.LUAR);
        ArrayAdapter<String> adapterKendaran = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, bKendaraan);
        lsKendaraan.setAdapter(adapterKendaran);
        lsKendaraan.setText(bKendaraan.get(TransaksiAngkut.KENDARAAN_KEBUN));

        if(((BaseActivity) getActivity()).currentlocation != null){
            latitude = ((BaseActivity) getActivity()).currentlocation.getLatitude();
            longitude = ((BaseActivity) getActivity()).currentlocation.getLongitude();

        }

        Collection<User> assAfd = GlobalHelper.dataAllAsstAfd.values();
        Collection<PKS> pkss = GlobalHelper.dataAllPks.values();
        Collection<Langsiran> langsirans = GlobalHelper.dataAllLagsiran.values();
        Collection<Operator> operators = GlobalHelper.dataOperator.values();
        Collection<Vehicle> vehicles = GlobalHelper.dataVehicle.values();

        adapterAssAfd = new SelectUserAdapter(getActivity(),R.layout.row_object, new ArrayList<User>(assAfd));
        adapterPks = new SelectPksAdapter(getActivity(),R.layout.row_object, new ArrayList<PKS>(pkss));
        adapterLangsiran = new SelectLangsiranAdapter(getActivity(),R.layout.row_setting, new ArrayList<Langsiran>(langsirans));
        adapterOperator = new SelectOperatorAdapter(getActivity(),R.layout.row_setting, new ArrayList<Operator>(operators));
        adapterVehicle= new SelectVehicleAdapter(getActivity(),R.layout.row_setting, new ArrayList<Vehicle>(vehicles));

        SimpleDateFormat sdfDate = new SimpleDateFormat("base");
        idSpb = "S" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT)) + sdfDate.format(System.currentTimeMillis()) + GlobalHelper.getUser().getUserID();
        tvSpbID.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));
        tvSpbIDKeluar.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));
        etApprove.setAdapter(adapterAssAfd);
        etApprove.setThreshold(1);
        etApproveKeluar.setAdapter(adapterAssAfd);
        etApproveKeluar.setThreshold(1);
        adapterAssAfd.notifyDataSetChanged();

        approveBy = (User) assAfd.toArray()[0];
        etApprove.setText(approveBy.getUserFullName());
        etApproveKeluar.setText(approveBy.getUserFullName());

        etVehicle.setAdapter(adapterVehicle);
        etVehicle.setThreshold(1);
        adapterVehicle.notifyDataSetChanged();

        etDriver.setAdapter(adapterOperator);
        etDriver.setThreshold(1);
        adapterOperator.notifyDataSetChanged();


        if(rekonsilasiActivity.selectedTransaksiAngkut != null){
            selectedTransaksiAngkut = rekonsilasiActivity.selectedTransaksiAngkut;

            tvCreateTime.setText(sdf.format(selectedTransaksiAngkut.getStartDate()));
            lsTujuan.setText(bAlas.get(selectedTransaksiAngkut.getTujuan()));
            lsKendaraan.setText(bKendaraan.get(selectedTransaksiAngkut.getKendaraanDari()));

            String vehicle = selectedTransaksiAngkut.getVehicle().getVehicleCode();
            String supir =selectedTransaksiAngkut.getSupir().getNama();

            if(selectedTransaksiAngkut.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN &&
                    !supir.isEmpty()){
                selectedOperator = GlobalHelper.dataOperator.get(supir);
                if (selectedOperator != null) {
                    setOperator(selectedOperator);
                }else{
                    selectedOperator = new Operator();
                    selectedOperator.setNama(supir);
                    setOperator(selectedOperator);
                }
            }else if (selectedTransaksiAngkut.getKendaraanDari() == TransaksiAngkut.KENDARAAN_LUAR){
                selectedOperator = new Operator();
                selectedOperator.setNama(supir);
                setOperator(selectedOperator);
            }else{
                selectedOperator = (Operator) GlobalHelper.dataOperator.values().toArray()[0];
                setOperator(selectedOperator);
            }

            if(selectedTransaksiAngkut.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN &&
                    !vehicle.isEmpty()){
                vehicleUse = GlobalHelper.dataVehicle.get(vehicle);
                if (vehicleUse != null) {
                    setVehicle(vehicleUse);
                }else{
                    vehicleUse = new Vehicle();
                    vehicleUse.setVehicleName(vehicle);
                    setVehicle(vehicleUse);
                }
            }else if (selectedTransaksiAngkut.getKendaraanDari() == TransaksiAngkut.KENDARAAN_LUAR ){
                vehicleUse = new Vehicle();
                vehicleUse.setVehicleName(vehicle);
                setVehicle(vehicleUse);
            }else{
                if(GlobalHelper.dataVehicle.values().size() == 0){
                    rekonsilasiActivity.closeActivity();
//                        if(rekonsilasiActivity!=null) rekonsilasiActivity.closeActivity();
                    Toast.makeText(HarvestApp.getContext(), "Mohon Sync Data", Toast.LENGTH_LONG).show();
                    return;
                }
                vehicleUse = (Vehicle) GlobalHelper.dataVehicle.values().toArray()[0];
                setVehicle(vehicleUse);
            }


            if(lsTujuan.getText().toString().equals(TransaksiAngkut.LANGSIR)){
                lPks.setVisibility(View.GONE);
                lKeluar.setVisibility(View.GONE);
                lLangsiran.setVisibility(View.VISIBLE);
                if(!selectedTransaksiAngkut.getLangsiran().getIdTujuan().isEmpty()){
                    tvLangsiran.setAdapter(adapterLangsiran);
                    tvLangsiran.setThreshold(1);
                    adapterLangsiran.notifyDataSetChanged();

                    Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_LANGSIRAN);
                    ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                    Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", selectedTransaksiAngkut.getLangsiran().getIdTujuan()));
                    if(cursor.size() > 0){
                        for (Iterator iterator = cursor.iterator(); iterator.hasNext();) {
                            DataNitrit dataNitrit = (DataNitrit) iterator.next();
                            Gson gson = new Gson();
                            Langsiran langsiran = gson.fromJson(dataNitrit.getValueDataNitrit(),Langsiran.class);
                            if(langsiran.getIdTujuan().equals(selectedTransaksiAngkut.getLangsiran().getIdTujuan())){
                                tvLangsiran.setText(langsiran.getNamaTujuan());
                                selectedLangsiran = langsiran;
                                break;
                            }
                        }
                    }
                    db.close();
                }else {
                    Toast.makeText(HarvestApp.getContext(), "Mohon Info Ini ada Bug Tujuan Langsiran langsir", Toast.LENGTH_LONG).show();
                }
            }else if (lsTujuan.getText().toString().equals(TransaksiAngkut.PKS)){
                lPks.setVisibility(View.VISIBLE);
                lKeluar.setVisibility(View.GONE);
                lLangsiran.setVisibility(View.GONE);
                if(!selectedTransaksiAngkut.getPks().getIdPKS().isEmpty()){
                    tvPks.setAdapter(adapterPks);
                    tvPks.setThreshold(1);
                    adapterPks.notifyDataSetChanged();

                    Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_PKS);
                    ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                    Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", selectedTransaksiAngkut.getPks().getIdPKS()));
                    if(cursor.size() > 0){
                        for (Iterator iterator = cursor.iterator(); iterator.hasNext();) {
                            DataNitrit dataNitrit = (DataNitrit) iterator.next();
                            Gson gson = new Gson();
                            PKS pks = gson.fromJson(dataNitrit.getValueDataNitrit(),PKS.class);
                            if(pks.getIdPKS().equals(selectedTransaksiAngkut.getPks().getIdPKS())){
                                tvPks.setText(pks.getNamaPKS());
                                selectedPks = pks;
                                break;
                            }
                        }
                    }
                    db.close();
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Mohon Info Ini ada Bug Tujuan PKS pks", Toast.LENGTH_LONG).show();
                }
                if(!selectedTransaksiAngkut.getApproveBy().isEmpty()){
                    User user = GlobalHelper.dataAllAsstAfd.get(selectedTransaksiAngkut.getApproveBy());
                    if(user == null){
                        user = (User) assAfd.toArray()[0];
                    }
                    approveBy = user;
                    etApprove.setText(user.getUserFullName());
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Mohon Info Ini ada Bug Tujuan PKS approveBy", Toast.LENGTH_LONG).show();
                }

                if(!selectedTransaksiAngkut.getIdSPB().isEmpty()){
                    idSpb = selectedTransaksiAngkut.getIdSPB();
                    tvSpbID.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));
                    tvSpbIDKeluar.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Mohon Info Ini ada Bug Tujuan PKS idSpb", Toast.LENGTH_LONG).show();
                }

            }else{
                if(!selectedTransaksiAngkut.getIdSPB().isEmpty()){
                    idSpb = selectedTransaksiAngkut.getIdSPB();
                    tvSpbID.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));
                    tvSpbIDKeluar.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Mohon Info Ini ada Bug Tujuan Keluar idSpb", Toast.LENGTH_LONG).show();
                }
                if(!selectedTransaksiAngkut.getApproveBy().isEmpty()){
                    User user = GlobalHelper.dataAllAsstAfd.get(selectedTransaksiAngkut.getApproveBy());
                    if(user == null){
                        user = (User) assAfd.toArray()[0];
                    }
                    approveBy = user;
                    etApproveKeluar.setText(user.getUserFullName());
                }else{
                    Toast.makeText(HarvestApp.getContext(),"Mohon Info Ini ada Bug Tujuan Keluar approveBy", Toast.LENGTH_LONG).show();
                }
            }
        }
        //jika selectedTransaksi sudah ada di database
        if(rekonsilasiActivity.selectedTransaksiAngkut != null) {
            selectedTransaksiAngkut = rekonsilasiActivity.selectedTransaksiAngkut;
            lsTujuan.setText(bAlas.get(selectedTransaksiAngkut.getTujuan()));

            if(selectedTransaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_PKS || selectedTransaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_KELUAR){
                Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_SPB);
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", selectedTransaksiAngkut.getIdSPB()));
                if(cursor.size() > 0){
                    for(DataNitrit dataNitrit : cursor){
                        Gson gson = new Gson();
                        selectedTransaksiSPB = gson.fromJson(dataNitrit.getValueDataNitrit(),TransaksiSPB.class);
                        idSpb = selectedTransaksiSPB.getIdSPB();
                        tvSpbID.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));
                        tvSpbIDKeluar.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));

                        approveBy = GlobalHelper.dataAllUser.get(selectedTransaksiSPB.getApproveBy());
                        etApprove.setText(approveBy.getUserFullName());
                        break;
                    }
                }
                db.close();
            }
            TransaksiAngkutHelper.hapusFileAngkut(false);
        }

        //rekonsilasi
        lsKendaraan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    if(i == TransaksiAngkut.KENDARAAN_KEBUN){
                        etDriver.setAdapter(adapterOperator);
                        etDriver.setThreshold(1);
                        adapterOperator.notifyDataSetChanged();
                        setOperator((Operator) GlobalHelper.dataOperator.values().toArray()[0]);

                        etVehicle.setAdapter(adapterVehicle);
                        etVehicle.setThreshold(1);
                        adapterVehicle.notifyDataSetChanged();
                        setVehicle((Vehicle) GlobalHelper.dataVehicle.values().toArray()[0]);
                    }else if(i == TransaksiAngkut.KENDARAAN_LUAR){
                        etDriver.setText("");
                        etDriver.setAdapter(null);
                        etDriver.setThreshold(0);
                        setOperator(new Operator());

                        etVehicle.setText("");
                        etVehicle.setAdapter(null);
                        etVehicle.setThreshold(0);
                        setVehicle(new Vehicle());
                    }
                }
            }
        });

        lsTujuan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    //try {
                    if(i == TransaksiAngkut.TUJUAN_PKS){
                        lLangsiran.setVisibility(View.GONE);
                        lPks.setVisibility(View.VISIBLE);
                        lKeluar.setVisibility(View.GONE);
                        selectedLangsiran = null;
//                            objTAngkut.remove("langsiran");
//                            objTAngkut.put("idSpb",idSpb);

                        tvPks.setAdapter(adapterPks);
                        tvPks.setThreshold(1);
                        adapterPks.notifyDataSetChanged();
                        if(baseActivity.currentLocationOK()){
                            PKS pksX = null;
                            double dis = 0.0;
                            for(Map.Entry<String,PKS> e : GlobalHelper.dataAllPks.entrySet()) {
                                PKS value = e.getValue();
                                if(pksX == null){
                                    pksX = value;
                                    dis = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(),pksX.getLatitude(),
                                            baseActivity.currentlocation.getLongitude(),pksX.getLongitude());
                                }else{
                                    double disX = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(),value.getLatitude(),
                                            baseActivity.currentlocation.getLongitude(),value.getLongitude());

                                    if(dis >= disX){
                                        dis = disX;
                                        pksX = value;
                                    }
                                }
                            }
                            setPks(pksX);
                        }else{
                        }

                    }else if(i == TransaksiAngkut.TUJUAN_LANGSIR){
                        lLangsiran.setVisibility(View.VISIBLE);
                        lPks.setVisibility(View.GONE);
                        lKeluar.setVisibility(View.GONE);
//                            objTAngkut.remove("pks");
//                            objTAngkut.remove("idSpb");
                        selectedPks = null;

                        tvLangsiran.setAdapter(adapterLangsiran);
                        tvLangsiran.setThreshold(1);
                        adapterLangsiran.notifyDataSetChanged();
                        if(baseActivity.currentLocationOK()){
                            Langsiran langsiranX = null;
                            double dis = 0.0;
                            for(Map.Entry<String,Langsiran> e : GlobalHelper.dataAllLagsiran.entrySet()) {
                                Langsiran value = e.getValue();
                                if(langsiranX == null){
                                    langsiranX = value;
                                    dis = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(),langsiranX.getLatitude(),
                                            baseActivity.currentlocation.getLongitude(),langsiranX.getLongitude());
                                }else{
                                    double disX = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(),value.getLatitude(),
                                            baseActivity.currentlocation.getLongitude(),value.getLongitude());

                                    if(dis >= disX){
                                        dis = disX;
                                        langsiranX = value;
                                    }
                                }
                            }
                            setLangsiran(langsiranX);
                        }else{

                        }
                    }else if (i == TransaksiAngkut.TUJUAN_KELUAR){
                        lLangsiran.setVisibility(View.GONE);
                        lPks.setVisibility(View.GONE);
                        lKeluar.setVisibility(View.VISIBLE);
                        selectedPks = null;
                        selectedLangsiran = null;
//                            objTAngkut.remove("pks");
//                            objTAngkut.remove("langsiran");
//                            objTAngkut.put("idSpb",idSpb);
                    }
//                        objTAngkut.put("tujuan", i);
//                        TransaksiAngkutHelper.createNewAngkut(objTAngkut);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
                }
            }
        });

        etVehicle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etVehicle.showDropDown();
            }
        });

        etVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etVehicle.setText("");
                etVehicle.showDropDown();
            }
        });

        etVehicle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    adapterVehicle = (SelectVehicleAdapter) parent.getAdapter();
                    setVehicle(adapterVehicle.getItemAt(position));
                }
            }
        });

        etVehicle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    if (lsKendaraan.getText().toString().equals(TransaksiAngkut.LUAR)) {
                        Vehicle veh = new Vehicle();
                        veh.setVehicleName(etVehicle.getText().toString());
                        vehicleUse = veh;
//                        try {
////                            objTAngkut.put("vehicle", vehicleUse.getVehicleName());
////                            TransaksiAngkutHelper.createNewAngkut(objTAngkut);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    }
                }
            }
        });

        etApprove.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etApprove.showDropDown();
            }
        });

        etApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etApprove.setText("");
                etApprove.showDropDown();
            }
        });

        etApprove.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    adapterAssAfd = (SelectUserAdapter) parent.getAdapter();
                    User user = adapterAssAfd.getItemAt(position);
                    etApprove.setText(user.getUserFullName());
                    etApproveKeluar.setText(user.getUserFullName());
                    approveBy = user;
//                    try {
//                        objTAngkut.put("approveBy", user.getUserID());
////                        TransaksiAngkutHelper.createNewAngkut(objTAngkut);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
                }
            }
        });

        etApproveKeluar.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etApproveKeluar.showDropDown();
            }
        });

        etApproveKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etApproveKeluar.setText("");
                etApproveKeluar.showDropDown();
            }
        });

        etApproveKeluar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    adapterAssAfd = (SelectUserAdapter) parent.getAdapter();
                    User user = adapterAssAfd.getItemAt(position);
                    etApprove.setText(user.getUserFullName());
                    etApproveKeluar.setText(user.getUserFullName());
                    approveBy = user;
//                    try {
//                        objTAngkut.put("approveBy", user.getUserID());
////                        TransaksiAngkutHelper.createNewAngkut(objTAngkut);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
                }
            }
        });

        etDriver.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etDriver.showDropDown();
            }
        });

        etDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etDriver.setText("");
                etDriver.showDropDown();
            }
        });
        etDriver.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    adapterOperator = (SelectOperatorAdapter) parent.getAdapter();
                    Operator operator = adapterOperator.getItemAt(position);
                    setOperator(operator);
                }
            }
        });

        etDriver.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    if (lsKendaraan.getText().toString().equals(TransaksiAngkut.LUAR)) {
                        Operator ope = new Operator();
                        ope.setNama(etDriver.getText().toString());
                        selectedOperator = ope;
                    }
                }
            }
        });

        tvPks.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    tvPks.showDropDown();
            }
        });

        tvPks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPks.setText("");
                tvPks.showDropDown();
            }
        });

        tvPks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    adapterPks = (SelectPksAdapter) parent.getAdapter();
                    PKS pks = adapterPks.getItemAt(position);
                    setPks(pks);
                }
            }
        });

        tvLangsiran.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    tvLangsiran.showDropDown();
            }
        });

        tvLangsiran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvLangsiran.setText("");
                tvLangsiran.showDropDown();
            }
        });

        tvLangsiran.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    adapterLangsiran = (SelectLangsiranAdapter) parent.getAdapter();
                    Langsiran langsiran = adapterLangsiran.getItemAt(position);
                    setLangsiran(langsiran);
                }
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SEARCH));
            }
        });
        btnClsoe.setText(HarvestApp.getContext().getResources().getString(R.string.print));

        btnClsoe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogCloseTransaksiAngkut();
            }
        });
        new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
    }



    private void updateDataRv(int status){
        tJanjang = 0;
        tBrondolan = 0;
        tBeratEstimasi = 0;
        tKartuHilang=0;
        tKartuRusak=0;
        rekonsilasiActivity.selectedAngkut = null;
        rekonsilasiActivity.selectedTransaksiPanen = null;
        if(rekonsilasiActivity.originRekonsilasi== null)rekonsilasiActivity.originRekonsilasi = new HashMap<>();
        rekonsilasiActivity.originSearch = new HashMap<>();
//        if(searchList.size()>0) searchList.clear();

        if(selectedTransaksiAngkut!=null){
            for(Angkut angkut: selectedTransaksiAngkut.getAngkuts()){
                if(angkut.getManualSPBData()!=null){
                    if (status == STATUS_LONG_OPERATION_SEARCH) {
                        String tipeAngkut = "Manual";
                        if(angkut.getTransaksiPanen() != null){
                            tipeAngkut = angkut.getTransaksiPanen().getTph().getNamaTph();
                        }else if (angkut.getTransaksiAngkut() != null){
                            tipeAngkut = angkut.getTransaksiAngkut().getIdTAngkut();
                        }
                        if (sdfTime.format(angkut.getWaktuAngkut()).contains(etSearch.getText().toString().toLowerCase()) ||
                                tipeAngkut.toLowerCase().contains(etSearch.getText().toString().toLowerCase())) {
                            if(angkut.getManualSPBData()!=null){
                                addRowTableRekonsiliasi(angkut, status);
                            }
                        }else if(angkut.getTph().getNoTph().contains(etSearch.getText().toString())){
                            addRowTableRekonsiliasi(angkut, status);
                        }else if(angkut.getManualSPBData().getTipe().toUpperCase().contains(etSearch.getText().toString().toUpperCase())){
                            addRowTableRekonsiliasi(angkut, status);
                        }
                    } else if (status == STATUS_LONG_OPERATION_NONE) {
                        //tambahan zendi
                        searchList.add(angkut.getBlock());
                        if(angkut.getTph() !=null)searchList.add(angkut.getTph().getNoTph());
                        if(angkut.getManualSPBData()!=null)searchList.add(angkut.getManualSPBData().getTipe());
                        searchList.add(timeFormat.format(selectedTransaksiAngkut.getStartDate()));
                        if(angkut.getManualSPBData()!=null){
                            addRowTableRekonsiliasi(angkut, status);
                        }
                    }
                }
            }

            angkutView = new HashMap<>();
            if(selectedTransaksiAngkut.getAngkuts().size() > 0){
                angkutView = transaksiAngkutHelper.setDataView(selectedTransaksiAngkut.getAngkuts());
            }

            Log.i("updateDataRv", "selectedTransaksiAngkut: "+selectedTransaksiAngkut.getAngkuts().size());
        }else{
            Log.i("updateDataRv", "selectedTransaksiAngkut NULL");
        }


    }

    private void addRowTableRekonsiliasi(Angkut angkut, int status){
        if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
            if(status == STATUS_LONG_OPERATION_NONE) tKartuRusakDef++;
            tKartuRusak++;
        }else if(angkut.getManualSPBData().getTipe().equals("Kartu Hilang")){
            if(status == STATUS_LONG_OPERATION_NONE) tKartuHilangDef++;
            tKartuHilang++;
        }
        if(status== STATUS_LONG_OPERATION_NONE) rekonsilasiActivity.originRekonsilasi.put(angkut.getIdAngkut(),angkut);
        if(status == STATUS_LONG_OPERATION_SEARCH) rekonsilasiActivity.originSearch.put(angkut.getIdAngkut(), angkut);
    }

    private void updateUIRV(int status){
        List<String> lSearch = new ArrayList<>();
        lSearch.addAll(searchList);
        lSearch = GlobalHelper.removeNullUsingIterator(lSearch);
        ArrayAdapter<String> adapterSearch = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, lSearch);
        etSearch.setAdapter(adapterSearch);
        etSearch.setThreshold(1);
        HashMap<String, Angkut> hashRekonsiliasi;
        Log.i("updateUIRV", "updateUIRV: ");


        if(status == STATUS_LONG_OPERATION_NONE){
            hashRekonsiliasi = rekonsilasiActivity.originRekonsilasi;
        }else{
            if(rekonsilasiActivity.originSearch.size()>0){
                hashRekonsiliasi = rekonsilasiActivity.originSearch;
            }else{
                Toast.makeText(HarvestApp.getContext(), "Mohon maaf data yang anda cari tidak ada", Toast.LENGTH_SHORT).show();
                hashRekonsiliasi = rekonsilasiActivity.originRekonsilasi;
                tKartuHilang = tKartuHilangDef;
                tKartuRusak = tKartuRusakDef;
            }
        }
        if(hashRekonsiliasi.size() > 0  ) {
            ltableview.setVisibility(View.VISIBLE);
            mRekonsilasiTableViewModel = new RekonsilasiTableViewModel(getContext(), hashRekonsiliasi);
            rekonsilasiAdapter = new RekonsilasiAdapter(getContext(),mRekonsilasiTableViewModel);
            mTableView.setAdapter(rekonsilasiAdapter);

            mTableView.setTableViewListener(new ITableViewListener() {
                @Override
                public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellDoubleClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {
                        // Create Long Press Popup
                        ColumnHeaderPopup popup = new ColumnHeaderPopup(
                                (ColumnHeaderViewHolder) columnHeaderView, mTableView);
                        // Show
                        popup.show();
                    }
                }

                @Override
                public void onColumnHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    String[] sid = ((AngkutRowHeaderViewHolder) rowHeaderView).getCellId().split("-");
                    if (sid[2].equalsIgnoreCase("angkut") || sid[2].equalsIgnoreCase("manual") || sid[2].equalsIgnoreCase("transit")) {
                        Angkut value = hashRekonsiliasi.get(sid[1]);
                        rekonsilasiActivity.selectedAngkut = value;
                        if (value.getTransaksiPanen() != null) {
                            rekonsilasiActivity.selectedTransaksiPanen = value.getTransaksiPanen();
                        }
                    }
                }

                @Override
                public void onRowHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }

                @Override
                public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }
            });

            rekonsilasiAdapter.setAllItems(mRekonsilasiTableViewModel.getColumnHeaderList(),
                    mRekonsilasiTableViewModel.getRowHeaderList(),
                    mRekonsilasiTableViewModel.getCellList());

        }else{
            ltableview.setVisibility(View.GONE);
        }

        tvJanjangTitle.setText(HarvestApp.getContext().getResources().getString(R.string.total_kartu_hilang));
        tvBrondolanTitle.setText(HarvestApp.getContext().getResources().getString(R.string.total_kartu_rusak));

        tvTotalOph.setText(String.valueOf(hashRekonsiliasi.size()));
        tvTotalTph.setText(String.valueOf(hashRekonsiliasi.size()));
        tvTotalJanjang.setText(String.valueOf(tKartuHilang));
        tvTotalBrondolan.setText(String.valueOf(tKartuRusak));
    }

    public void showDialogCloseTransaksiAngkut(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.metode_penyimpanan_transaksi,null);
        LinearLayout lnfc = view.findViewById(R.id.lnfc);
        LinearLayout lprint = view.findViewById(R.id.lprint);
        LinearLayout lshare = view.findViewById(R.id.lshare);
        Button btnClose = view.findViewById(R.id.btnClose);
        lnfc.setVisibility(View.GONE);

        btnClose.setVisibility(View.VISIBLE);

        lprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((BaseActivity) getActivity()).bluetoothHelper.mService.getState() == BluetoothService.STATE_CONNECTED) {
                    if(selectedTransaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR) {
                        transaksiAngkutHelper.printRekonsilasi(selectedTransaksiAngkut, tKartuRusak, tKartuHilang,new ArrayList<>(angkutView.values()));
                    }else{
                        transaksiSpbHelper.printRekonsilasiSPB(selectedTransaksiSPB, tKartuRusak, tKartuHilang,new ArrayList<>(angkutView.values()));
                    }
//                    transaksiAngkutHelper.printRekonsilasi(selectedTransaksiAngkut, tKartuRusak, tKartuHilang);
                }else{
                    ((BaseActivity) getActivity()).bluetoothHelper.showListBluetoothPrinter();
                }
            }
        });

        lshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedTransaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR) {
                    if(rekonsilasiActivity!=null) transaksiAngkutHelper.showQrRekonsilasi(selectedTransaksiAngkut, rekonsilasiActivity.qrHelper,new ArrayList<>(angkutView.values()));
                }else{
                    if(rekonsilasiActivity!=null) transaksiSpbHelper.showRekonsiliasiSPBQr(selectedTransaksiSPB, rekonsilasiActivity.qrHelper,new ArrayList<>(angkutView.values()));
                }
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                objTAngkut = new JSONObject();
                if(rekonsilasiActivity!=null)rekonsilasiActivity.closeActivity();

            }
        });

        alertDialog = WidgetHelper.showListReference(alertDialog,view,getActivity());
    }

    public void setLangsiran(Langsiran langsiran){
        lLangsiran.setVisibility(View.VISIBLE);
        lPks.setVisibility(View.GONE);
        lKeluar.setVisibility(View.GONE);
        if(langsiran != null) {
            tvLangsiran.setText(langsiran.getNamaTujuan());
            selectedLangsiran = langsiran;
//            try {
//                objTAngkut.remove("pks");
//                objTAngkut.remove("idSpb");
//                objTAngkut.put("langsiran", selectedLangsiran.getIdTujuan());
////                TransaksiAngkutHelper.createNewAngkut(objTAngkut);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
        }
    }

    public void setPks(PKS pks){
        lLangsiran.setVisibility(View.GONE);
        lPks.setVisibility(View.VISIBLE);
        lKeluar.setVisibility(View.GONE);
        tvPks.setText(pks.getNamaPKS());
        selectedPks = pks;
//        try {
//            objTAngkut.remove("langsiran");
//            objTAngkut.put("pks",selectedPks.getIdPKS());
//            objTAngkut.put("idSpb",idSpb);
////            TransaksiAngkutHelper.createNewAngkut(objTAngkut);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

    public void setOperator(Operator operator){
        etDriver.setText(operator.getNama());
        selectedOperator = operator;
//        try {
//            objTAngkut.put("supir",operator.getKodeOperator() != null ? operator.getKodeOperator() : operator.getNama());
////            TransaksiAngkutHelper.createNewAngkut(objTAngkut);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

    public void setVehicle(Vehicle vehicle){
        etVehicle.setText(vehicle.getVehicleCode() == null ? "" : vehicle.getVehicleCode());
        vehicleUse = vehicle;
//        try {
////            objTAngkut.put("vehicle",vehicle.getVehicle() != null ? vehicle.getVehicle() : vehicle.getVehicleName());
////            TransaksiAngkutHelper.createNewAngkut(objTAngkut);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

    public void startLongOperation(int longOperation){
        new LongOperation().execute(String.valueOf(longOperation));
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        AlertDialog alertDialogAllpoin;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(getActivity(),getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])){
                case STATUS_LONG_OPERATION_NONE:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
            }
            return strings[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(popupTPH){
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        if(lsTujuan.getText().toString().equals(TransaksiAngkut.PKS)){
                            pksHelper.showMapPks();
                        }else if (lsTujuan.getText().toString().equals(TransaksiAngkut.LANGSIR)){
                            tujuanHelper.showMapTujuan();
                        }
                        popupTPH = false;
                        if (alertDialogAllpoin != null) {
                            alertDialogAllpoin.cancel();
                        }
                    }
                });
            }else {
                if (alertDialogAllpoin != null) {
                    alertDialogAllpoin.cancel();
                }
                switch (Integer.parseInt(result)) {
//                    STATUS_LONG_OPERATION_NONE
                    case STATUS_LONG_OPERATION_NONE: {
                        updateUIRV(Integer.parseInt(result));
                        break;
                    }
                    case STATUS_LONG_OPERATION_SEARCH: {
                        updateUIRV(Integer.parseInt(result));
                        break;
                    }
                }
            }
        }
    }
}