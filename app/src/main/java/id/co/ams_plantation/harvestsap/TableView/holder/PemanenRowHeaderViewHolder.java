package id.co.ams_plantation.harvestsap.TableView.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestsap.model.Pemanen;

/**
 * Created by user on 12/24/2018.
 */

public class PemanenRowHeaderViewHolder extends AbstractViewHolder {

    private final TextView row_header_textview;
    private final LinearLayout row_header_container;
    private TableViewCell cell;

    public PemanenRowHeaderViewHolder(View layout) {
        super(layout);
        row_header_textview = (TextView) itemView.findViewById(R.id.row_header_textview);
        row_header_container = (LinearLayout) itemView.findViewById(R.id.row_header_container);
    }

    public void setCell(TableViewRowHeader cell) {
        String [] id = cell.getId().split("-");
        this.cell = cell;
        row_header_textview.setText(String.valueOf(cell.getData()));
        try {
            int urut = Integer.parseInt(id[2]);
            row_header_container.setBackgroundColor(Pemanen.COLOR_DEFIND);
        }catch (Exception e){
            row_header_container.setBackgroundColor(Pemanen.COLOR_UNDEFIND);
        }
    }

    public String getCellId(){
        return this.cell.getId();
    }
}
