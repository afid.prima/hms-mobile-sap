package id.co.ams_plantation.harvestsap.TableView.view_model;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewColumnHeader;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestsap.model.Pemanen;

/**
 * Created by user on 12/24/2018.
 */

public class PemanenTableViewModel {
    private final Context mContext;
    private ArrayList<Pemanen> pemanens;
    String [] HeaderColumn = {"Kemandoran","NIK","Nama","Gang","Afdeling"};

    public PemanenTableViewModel(Context context, ArrayList<Pemanen> hPemanens) {
        mContext = context;
        pemanens = new ArrayList<>();
        pemanens = hPemanens;
    }

    private List<TableViewRowHeader> getSimpleRowHeaderList() {
        List<TableViewRowHeader> list = new ArrayList<>();
        for (int i = 0; i < pemanens.size(); i++) {
            String id = i + "-" + pemanens.get(i).getNik() + "-"+ pemanens.get(i).getNoUrut();
            TableViewRowHeader header = new TableViewRowHeader(id, pemanens.get(i).getNoUrut() == 0 ? "999": String.valueOf(pemanens.get(i).getNoUrut()));
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<TableViewColumnHeader> getRandomColumnHeaderList() {
        List<TableViewColumnHeader> list = new ArrayList<>();

        for (int i = 0; i < HeaderColumn.length; i++) {
            TableViewColumnHeader header = new TableViewColumnHeader(String.valueOf(i), HeaderColumn[i]);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<List<TableViewCell>> getCellListForSortingTest() {
        List<List<TableViewCell>> list = new ArrayList<>();
        for (int i = 0; i < pemanens.size(); i++) {
            List<TableViewCell> cellList = new ArrayList<>();
            for (int j = 0; j < HeaderColumn.length; j++) {
                // Create dummy id.
                String id = j + "-" + pemanens.get(i).getNik() + "-"+ pemanens.get(i).getNoUrut();

                TableViewCell cell = null;
                switch (j){
                    case 0:{
                        cell = new TableViewCell(id, pemanens.get(i).getKemandoran() == null?  "" : pemanens.get(i).getKemandoran() );
                        break;
                    }
                    case 1:{
                        cell = new TableViewCell(id, pemanens.get(i).getNik());
                        break;
                    }
                    case 2:{
                        cell = new TableViewCell(id, pemanens.get(i).getNama());
                        break;
                    }
                    case 3:{
                        cell = new TableViewCell(id, pemanens.get(i).getGank());
                        break;
                    }
                    case 4:{
                        cell = new TableViewCell(id, pemanens.get(i).getAfdeling());
                        break;
                    }
                }
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }


    private static String getRandomString() {
        Random r = new Random();
        String str = " a ";
        for (int i = 0; i < r.nextInt(); i++) {
            str = str + " a ";
        }

        return str;
    }

    public List<List<TableViewCell>> getCellList() {
        return getCellListForSortingTest();
    }

    public List<TableViewRowHeader> getRowHeaderList() {
        return getSimpleRowHeaderList();
    }

    public List<TableViewColumnHeader> getColumnHeaderList() {
        return getRandomColumnHeaderList();
    }

}