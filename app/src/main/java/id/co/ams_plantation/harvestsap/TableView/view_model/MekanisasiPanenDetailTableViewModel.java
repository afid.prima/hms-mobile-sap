package id.co.ams_plantation.harvestsap.TableView.view_model;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewColumnHeader;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;

public class MekanisasiPanenDetailTableViewModel {
    private final Context mContext;
    private final ArrayList<TransaksiPanen> panens;
    String [] HeaderColumn = {"Block","Ancak","Pemanen","Janjang","Brondolan","Berat Estimasi","BJR"};

    public MekanisasiPanenDetailTableViewModel(Context context, HashMap<String,TransaksiPanen> hPanen) {
        mContext = context;
        panens = new ArrayList<>();
        panens.addAll(hPanen.values());
        Collections.sort(panens, new Comparator<TransaksiPanen>() {
            @Override
            public int compare(TransaksiPanen t0, TransaksiPanen t1) {
                return t0.getIdTPanen().compareTo(t1.getIdTPanen());
            }
        });
    }

    private List<TableViewRowHeader> getSimpleRowHeaderList() {
        List<TableViewRowHeader> list = new ArrayList<>();
        //"angkut"
        for (int i = 0; i < panens.size(); i++) {
            String id = i + ";" + panens.get(i).getIdTPanen();
            int no = i + 1;
            TableViewRowHeader header = new TableViewRowHeader(id, String.valueOf(no));
            list.add(header);
        }

        return list;
    }
    /**
     * This is a dummy model list test some cases.
     */
    private List<TableViewColumnHeader> getRandomColumnHeaderList() {
        List<TableViewColumnHeader> list = new ArrayList<>();

        for (int i = 0; i < HeaderColumn.length; i++) {
            TableViewColumnHeader header = new TableViewColumnHeader(String.valueOf(i), HeaderColumn[i]);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<List<TableViewCell>> getCellListForSortingTest() {
        List<List<TableViewCell>> list = new ArrayList<>();
        for (int i = 0; i < panens.size(); i++) {
            List<TableViewCell> cellList = new ArrayList<>();
            for (int j = 0; j < HeaderColumn.length; j++) {
                // Create dummy id.
                String id = j + ";" + panens.get(i).getIdTPanen();
                int color = panens.get(i).getColorBackground();
                TableViewCell cell = null;
                switch (j){
                    case 0: {
                        String nama = "";
                        if (panens.get(i).getTph() != null) {
                            nama = panens.get(i).getTph().getBlock();
                        }
                        cell = new TableViewCell(id, nama);
                        break;
                    }
                    case 1: {
                        String nama = "";
                        if (panens.get(i).getTph() != null) {
                            nama = panens.get(i).getTph().getAncak();
                        }
                        cell = new TableViewCell(id, nama);
                        break;
                    }
                    case 2: {
                        String nama = "";
                        if (panens.get(i).getPemanen() != null) {
                            nama = panens.get(i).getPemanen().getKodePemanen() + "-" + panens.get(i).getPemanen().getNama();
                        }
                        cell = new TableViewCell(id, nama);
                        break;
                    }
                    case 3: {
                        if (panens.get(i).getHasilPanen() != null) {
                            cell = new TableViewCell(id, panens.get(i).getHasilPanen().getTotalJanjang());
                        }else {
                            cell = new TableViewCell(id, "");
                        }
                        break;
                    }
                    case 4: {
                        if (panens.get(i).getHasilPanen() != null) {
                            cell = new TableViewCell(id, panens.get(i).getHasilPanen().getBrondolan());
                        }else {
                            cell = new TableViewCell(id, "");
                        }
                        break;
                    }
                    case 5: {
                        if (panens.get(i).getHasilPanen() != null && panens.get(i).getBjrMekanisasi() > 0.0) {
                            int beratEstimasi = (int) (panens.get(i).getHasilPanen().getTotalJanjang() * panens.get(i).getBjrMekanisasi());
                            cell = new TableViewCell(id,  beratEstimasi);
                        }else {
                            cell = new TableViewCell(id, "");
                        }
                        break;
                    }
                    case 6: {
                        cell = new TableViewCell(id, String.format("%.1f", panens.get(i).getBjrMekanisasi()));
                        break;
                    }
                }
                cell.setColor(color);
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }

    private static String getRandomString() {
        Random r = new Random();
        String str = " a ";
        for (int i = 0; i < r.nextInt(); i++) {
            str = str + " a ";
        }

        return str;
    }

    public List<List<TableViewCell>> getCellList() {
        return getCellListForSortingTest();
    }

    public List<TableViewRowHeader> getRowHeaderList() {
        return getSimpleRowHeaderList();
    }

    public List<TableViewColumnHeader> getColumnHeaderList() {
        return getRandomColumnHeaderList();
    }

}
