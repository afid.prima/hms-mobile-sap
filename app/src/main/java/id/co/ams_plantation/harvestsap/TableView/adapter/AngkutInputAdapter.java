package id.co.ams_plantation.harvestsap.TableView.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import com.evrencoskun.tableview.adapter.AbstractTableAdapter;
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.holder.AngkutCellViewHolder;
import id.co.ams_plantation.harvestsap.TableView.holder.AngkutRowHeaderCheckBoxViewHolder;
import id.co.ams_plantation.harvestsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewColumnHeader;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestsap.TableView.view_model.AngkutInputTableViewModel;

public class AngkutInputAdapter extends AbstractTableAdapter<TableViewColumnHeader, TableViewRowHeader, TableViewCell> {
    private final AngkutInputTableViewModel mTableViewModel;
    private final LayoutInflater mInflater;

    public AngkutInputAdapter(Context context, AngkutInputTableViewModel mTableViewModel) {
        super();
        this.mInflater = LayoutInflater.from(context);
        this.mTableViewModel = mTableViewModel;
    }

    @Override
    public int getColumnHeaderItemViewType(int position) {
        return 0;
    }

    @Override
    public int getRowHeaderItemViewType(int position) {
        return 0;
    }

    @Override
    public int getCellItemViewType(int position) {
        return 0;
    }

    @Override
    public AbstractViewHolder onCreateCellViewHolder(ViewGroup parent, int viewType) {
        View layout;
        // For cells that display a text
        layout = mInflater.inflate(R.layout.table_view_cell_layout, parent, false);

        // Create a Cell ViewHolder
        return new AngkutCellViewHolder(layout);
    }

    @Override
    public void onBindCellViewHolder(@NonNull AbstractViewHolder holder, @Nullable TableViewCell cellItemModel, int columnPosition, int rowPosition) {
        TableViewCell cell = (TableViewCell) cellItemModel;
        AngkutCellViewHolder viewHolder = (AngkutCellViewHolder) holder;
        viewHolder.setCell(cell);
    }

    @Override
    public AbstractViewHolder onCreateColumnHeaderViewHolder(ViewGroup parent, int viewType) {
        View corner = mInflater.inflate(R.layout.table_view_column_header_layout, parent, false);
        return new ColumnHeaderViewHolder(corner, getTableView());
    }

    @Override
    public void onBindColumnHeaderViewHolder(@NonNull AbstractViewHolder holder, @Nullable TableViewColumnHeader columnHeaderItemModel, int columnPosition) {
        TableViewColumnHeader columnHeader = (TableViewColumnHeader) columnHeaderItemModel;

        // Get the holder to update cell item text
        ColumnHeaderViewHolder columnHeaderViewHolder = (ColumnHeaderViewHolder) holder;
        columnHeaderViewHolder.setColumnHeader(columnHeader);
    }

    @Override
    public AbstractViewHolder onCreateRowHeaderViewHolder(ViewGroup parent, int viewType) {
        // Get Row Header xml Layout
        View layout = mInflater.inflate(R.layout.table_view_row_header_checkbox_layout, parent, false);

        // Create a Row Header ViewHolder
        return new AngkutRowHeaderCheckBoxViewHolder(layout);
    }

    @Override
    public void onBindRowHeaderViewHolder(@NonNull AbstractViewHolder holder, @Nullable TableViewRowHeader rowHeaderItemModel, int rowPosition) {
        TableViewRowHeader rowHeader = (TableViewRowHeader) rowHeaderItemModel;

        // Get the holder to update row header item text
        AngkutRowHeaderCheckBoxViewHolder rowHeaderViewHolder = (AngkutRowHeaderCheckBoxViewHolder) holder;
        rowHeaderViewHolder.setCell(rowHeader);
    }

    @NonNull
    @Override
    public View onCreateCornerView(@NonNull ViewGroup parent) {
        View corner = mInflater.inflate(R.layout.table_view_corner_checkbox_layout, null);
        RelativeLayout rHeader = corner.findViewById(R.id.rHeader);
        CheckBox cbCorner = corner.findViewById(R.id.cbCorner);
        cbCorner.setChecked(true);
        return corner;
    }
}
