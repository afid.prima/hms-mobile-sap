package id.co.ams_plantation.harvestsap.Fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.JanjangSensus;
import id.co.ams_plantation.harvestsap.model.SensusBjr;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.QcSensusBjrActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.SensusBjrHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import ng.max.slideview.SlideView;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by user on 12/4/2018.
 */

public class QcSensusBjrInputFragment extends Fragment {

    View tphphoto;
    ImageView ivPhoto;
    ImageView imagecacnel;
    ImageView imagesview;
    SliderLayout sliderLayout;
    RelativeLayout rl_ipf_takepicture;

    LinearLayout lInfoTph;
    LinearLayout lGang;
    LinearLayout lPemanen;
    LinearLayout lTph;
    LinearLayout lBlock;
    LinearLayout lAncak;
    CompleteTextViewHelper etNotph;
    CompleteTextViewHelper etBlock;
    AppCompatEditText etAncak;

    LayoutInflater vi;
    ViewGroup lSensusBjr;
    LinearLayout lsave;
    LinearLayout lldeleted;
    LinearLayout lcancel;

    TransaksiPanen selectedTransaksiPanen;
    SensusBjr selectedSensusBjr;

    QcSensusBjrActivity qcSensusBjrActivity;
    BaseActivity baseActivity;
    SensusBjrHelper sensusBjrHelper;

    ArrayList<File> ALselectedImage;
    ArrayList<View> alv;
    public HashMap<String,Integer> hmdynamicqc;
    int totalJanjang;
    boolean newData;

    public double latitude;
    public double longitude;

    final int YesNo_Simpan = 0;
    final int YesNo_Batal = 1;
    final int YesNo_Hapus = 2;
    final int YesNo_UpdateQc = 3;

    public static QcSensusBjrInputFragment getInstance(){
        QcSensusBjrInputFragment fragment = new QcSensusBjrInputFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qc_sensus_bjr_input,null,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        baseActivity = ((BaseActivity) getActivity());
        sensusBjrHelper = new SensusBjrHelper(getActivity());

        if (getActivity() instanceof QcSensusBjrActivity) {
            qcSensusBjrActivity = ((QcSensusBjrActivity) getActivity());
            if(qcSensusBjrActivity.selectedTransaksiPanen != null) {
                newData = true;
                selectedTransaksiPanen = qcSensusBjrActivity.selectedTransaksiPanen;
                qcSensusBjrActivity.selectedTransaksiPanen = null;
                if(selectedTransaksiPanen.getTph().getNoTph() == null){
                    qcSensusBjrActivity.backProses();
                    qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout,"Anda Tidak Punya Master TPH Ini",Snackbar.LENGTH_INDEFINITE);
                    qcSensusBjrActivity.searchingGPS.show();
                    return;
                }
            }else if (qcSensusBjrActivity.selectedSensusBjr != null){
                newData = false;
                selectedSensusBjr = qcSensusBjrActivity.selectedSensusBjr;
                qcSensusBjrActivity.selectedSensusBjr = null;
                if(selectedSensusBjr.getTph().getNoTph() == null){
                    qcSensusBjrActivity.backProses();
                    qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout,"Anda Tidak Punya Master TPH Ini",Snackbar.LENGTH_INDEFINITE);
                    qcSensusBjrActivity.searchingGPS.show();
                    return;
                }
            }else{
                //kenapa transaksi panen karena jika data transaksi panen berarti berasaldari tap kartu atau scan qr
                qcSensusBjrActivity.backProses();
                qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout,"Transaksi Panen Tidak Diketahui",Snackbar.LENGTH_INDEFINITE);
                qcSensusBjrActivity.searchingGPS.show();
                return;
            }

//            if(!qcSensusBjrActivity.currentLocationOK()){
//                qcSensusBjrActivity.backProses();
//                qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout,
//                        qcSensusBjrActivity.getResources().getString(R.string.gps_not_accurate),
//                        Snackbar.LENGTH_INDEFINITE);
//                qcSensusBjrActivity.searchingGPS.show();
//                return;
//            }
        }

        tphphoto = (View) view.findViewById(R.id.tphphoto);
        ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
        imagecacnel = (ImageView) view.findViewById(R.id.imagecacnel);
        imagesview = (ImageView) view.findViewById(R.id.imagesview);
        rl_ipf_takepicture = (RelativeLayout) view.findViewById(R.id.rl_ipf_takepicture);
        sliderLayout = (SliderLayout) view.findViewById(R.id.sliderLayout);

        lInfoTph = (LinearLayout) view.findViewById(R.id.lInfoTph);
//        lGang = (LinearLayout) view.findViewById(R.id.lGang);
//        lPemanen = (LinearLayout) view.findViewById(R.id.lPemanen);
        lTph = (LinearLayout) view.findViewById(R.id.lTph);
        lBlock = (LinearLayout) view.findViewById(R.id.lBlock);
        lAncak = (LinearLayout) view.findViewById(R.id.lAncak);
        etNotph = (CompleteTextViewHelper) view.findViewById(R.id.etNotph);
        etBlock = (CompleteTextViewHelper) view.findViewById(R.id.etBlock);
        etAncak = (AppCompatEditText) view.findViewById(R.id.etAncak);

        lSensusBjr = (ViewGroup) view.findViewById(R.id.lSensusBjr);
        lsave = (LinearLayout) view.findViewById(R.id.lsave);
        lldeleted = (LinearLayout) view.findViewById(R.id.lldeleted);
        lcancel = (LinearLayout) view.findViewById(R.id.lcancel);

//        lGang.setVisibility(View.GONE);
//        lPemanen.setVisibility(View.GONE);
        etNotph.setFocusable(false);
        etBlock.setFocusable(false);
        etAncak.setFocusable(false);

        main();

        if(selectedSensusBjr != null){
            if (selectedSensusBjr.getStatus() == SensusBjr.UPLOAD){
                lsave.setVisibility(View.GONE);
                lldeleted.setVisibility(View.GONE);
            }
        }

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (baseActivity.currentLocationOK()) {
                    if(ALselectedImage.size() > 3) {
                        Toast.makeText(HarvestApp.getContext(),getActivity().getResources().getString(R.string.warning_take_foto),Toast.LENGTH_LONG).show();
                    }else {
                        GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_QC_SENSUSBJR;
                        EasyImage.openCamera(getActivity(),1);
                    }
                } else {
                    qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
                    qcSensusBjrActivity.searchingGPS.show();
                }

            }
        });

        lsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showYesNo("Apakah Anda Yakin Untuk Simpan Data Ini",YesNo_Simpan);
            }
        });
        lcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showYesNo("Apakah Anda Yakin Ingin Keluar Form",YesNo_Batal);
            }
        });
        lldeleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showYesNo("Apakah Anda Yakin Untuk Hapus Data Ini",YesNo_Hapus);
            }
        });
    }

    public void showYesNo(String text,int stat){
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.yes_no, null);
        TextView tvDialogKet = (TextView) dialogView.findViewById(R.id.ket1);
        SlideView btnCancelDialog = (SlideView) dialogView.findViewById(R.id.btnCancel);
        SlideView btnOkDialog = (SlideView) dialogView.findViewById(R.id.btnOK);

        final AlertDialog dialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle)
                .setView(dialogView)
                .setCancelable(false)
                .create();

        final ForegroundColorSpan fcs = new ForegroundColorSpan(getActivity().getResources().getColor(R.color.Red));
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

        tvDialogKet.setText(text);
        btnOkDialog.setText("Yes");
        btnCancelDialog.setText("No");

        btnCancelDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                switch (stat) {
                    case YesNo_UpdateQc:
                        qcSensusBjrActivity.backProses();
                        dialog.dismiss();
                        break;
                    default:
                        dialog.dismiss();
                        break;
                }
            }
        });
        btnOkDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                switch (stat){
                    case YesNo_Simpan:
                        new LongOperation(YesNo_Simpan).execute(String.valueOf(YesNo_Simpan));
                        break;
                    case YesNo_Hapus:
                        new LongOperation(YesNo_Hapus).execute(String.valueOf(YesNo_Hapus));
                        break;
                    case YesNo_Batal:
                        qcSensusBjrActivity.backProses();
                        break;
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void main(){

        ALselectedImage = new ArrayList<>();
        ALselectedImage.clear();

        lSensusBjr.removeAllViews();
        vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        alv = new ArrayList<>();
        hmdynamicqc = new HashMap<>();

        if(selectedTransaksiPanen != null) {
            lldeleted.setVisibility(View.GONE);
            if(selectedTransaksiPanen.getFoto() != null) {
                for (String s : selectedTransaksiPanen.getFoto()) {
                    File file = new File(s);
                    ALselectedImage.add(file);
                }
            }

            totalJanjang = GlobalHelper.getTotalJanjang(selectedTransaksiPanen.getHasilPanen());
            etNotph.setText(selectedTransaksiPanen.getTph().getNamaTph());
            etBlock.setText(selectedTransaksiPanen.getTph().getBlock());
            etAncak.setText(selectedTransaksiPanen.getTph().getAncak());

            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BJR);
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", selectedTransaksiPanen.getIdTPanen()));
            if(cursor.size() > 0) {
                newData = false;
                showYesNo("Apakah Anda Akan Melakukan Update Qc Buah pada Tph "+ selectedTransaksiPanen.getTph().getNamaTph(),YesNo_UpdateQc);
            }
            db.close();
        }else if (selectedSensusBjr != null){
            lldeleted.setVisibility(View.VISIBLE);
            if(selectedSensusBjr.getFoto() != null) {
                for (String s : selectedSensusBjr.getFoto()) {
                    File file = new File(s);
                    ALselectedImage.add(file);
                }
            }

            totalJanjang = selectedSensusBjr.getBeratJanjang().size();
            etNotph.setText(selectedSensusBjr.getTph().getNamaTph());
            etBlock.setText(selectedSensusBjr.getTph().getBlock());
            etAncak.setText(selectedSensusBjr.getTph().getAncak());
        }
        setupimageslide();

        for(int i = 0 ; i < totalJanjang + 1; i ++){
            int no = i + 1;
            String id = "Janjang "+ no;
            if (no > totalJanjang){
                id = "Brondolan";
            }
            if(selectedTransaksiPanen != null) {
                alv.add(addBjrSensus(id, 0));
            }else if (selectedSensusBjr != null){
                if(id.contains("Janjang")){
                    alv.add(addBjrSensus(id, selectedSensusBjr.getBeratJanjang().get(no).getJanjang()));
                }else{
                    alv.add(addBjrSensus(id, selectedSensusBjr.getBeratBrondolan() == null ? 0 : selectedSensusBjr.getBeratBrondolan()));
                }
            }
        }

        for(int i = alv.size() - 1 ; i >= 0; i -- ){
            lSensusBjr.addView(alv.get(i), 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
        }

    }

    public View addBjrSensus(String id,Integer nilai){
        View v = vi.inflate(R.layout.sensus_bjr, null);
        bjrSensus(v,id,String.valueOf(nilai));
        hmdynamicqc.put(id, nilai);
        return v;
    }

    private boolean validasiSensusBJR(){
        if(baseActivity.currentLocationOK()){
            longitude = baseActivity.currentlocation.getLongitude();
            latitude = baseActivity.currentlocation.getLatitude();
            //jika dari nfc biru ada maka cek apa kah jarak lokasi sekarang denggan tph masih sesuai toleransi
            //jika dari nfc hijau ada maka cek apa kah jarak lokasi sekarang denggan tph langsiran masih sesuai toleransi
            if(selectedTransaksiPanen != null){
                double distance = GlobalHelper.distance(selectedTransaksiPanen.getLatitude(),latitude,
                        selectedTransaksiPanen.getLongitude(),longitude);
                if(distance > GlobalHelper.RADIUS){
                    WidgetHelper.warningRadiusTph(qcSensusBjrActivity,qcSensusBjrActivity.myCoordinatorLayout,distance);
//                    qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout, getResources().getString(R.string.radius_to_long_with_tph), Snackbar.LENGTH_INDEFINITE);
//                    qcSensusBjrActivity.searchingGPS.show();
                    return false;
                }
            }
        }else{
            WidgetHelper.warningFindGps(qcSensusBjrActivity,qcSensusBjrActivity.myCoordinatorLayout);
//            qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//            qcSensusBjrActivity.searchingGPS.show();
            return false;
        }

        HashMap<Integer,JanjangSensus> beratJanjang = new HashMap<>();
        boolean lanjut = false;
        int jmlKg = 0;
        for(TreeMap.Entry<String,Integer> entry :hmdynamicqc.entrySet()){
            if(!entry.getKey().equals("Brondolan")) {
                if (entry.getValue() != 0) {
                    lanjut = true;
                }
                jmlKg += entry.getValue();
                int idx = Integer.parseInt(entry.getKey().replace("Janjang","").trim());
                JanjangSensus janjangSensus = new JanjangSensus(idx,hmdynamicqc.get("Janjang "+idx));
                if(janjangSensus.getJanjang() == 0){
                    Toast.makeText(HarvestApp.getContext(),"Mohon Timbang Seluruh Janjang ",Toast.LENGTH_SHORT).show();
                    return false;
                }
                beratJanjang.put(idx,janjangSensus);
            }
        }

        if(!lanjut){
            qcSensusBjrActivity.searchingGPS = Snackbar.make(qcSensusBjrActivity.myCoordinatorLayout, "Mohon Isi Berat Janjang", Snackbar.LENGTH_INDEFINITE);
            qcSensusBjrActivity.searchingGPS.show();
            return false;
        }

        HashSet<String> sFoto = new HashSet<>();
        if(ALselectedImage.size() > 0){
            for(File file : ALselectedImage){
                sFoto.add(file.getAbsolutePath());
            }
        }
        String idPanen = selectedSensusBjr != null ? selectedSensusBjr.getIdTPanen() : selectedTransaksiPanen.getIdTPanen();
        String idQCSensusBJR = "QC2_"+ GlobalHelper.getUser().getUserID()+"_"+ idPanen;
        selectedSensusBjr = new SensusBjr(idQCSensusBJR,
                idPanen,
                latitude,
                longitude,
                hmdynamicqc.get("Brondolan") == null ? 0 :  hmdynamicqc.get("Brondolan"),
                Double.parseDouble(String.valueOf(jmlKg)) / Double.parseDouble(String.valueOf((hmdynamicqc.size() - 1))),
                GlobalHelper.getUser().getUserID(),
                System.currentTimeMillis(),
                SensusBjr.SAVE,
                selectedSensusBjr != null ? selectedSensusBjr.getTph() : selectedTransaksiPanen.getTph(),
                beratJanjang,
                sFoto);
        return true;
    }

    public void bjrSensus(final View v, final String id,  String values){
        TextView tv = (TextView) v.findViewById(R.id.tvKet);
        AppCompatEditText et = (AppCompatEditText) v.findViewById(R.id.etKet);
        if(id.toLowerCase().contains("janjang")){
            tv.setText("Berat" + id);
        }else{
            tv.setText("BeratBrondolan");
        }

        if(values.equals("0")){
            et.setText("");
        }else{
            et.setText(values);
        }

        et.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!et.getText().toString().isEmpty()) {
                    hmdynamicqc.put(id, Integer.parseInt(et.getText().toString()));
                }
            }
        });
    }

    public void setImages(File images){
        ALselectedImage.add(images);
        setupimageslide();
    }

    private void setupimageslide(){
        Set<File> sf = new HashSet<>();
        sf.addAll(ALselectedImage);
        ALselectedImage.clear();
        ALselectedImage.addAll(sf);

        if(ALselectedImage.size() > 3){
            ivPhoto.setVisibility(View.GONE);
        }else{
            ivPhoto.setVisibility(View.VISIBLE);
        }
        if(ALselectedImage.size() != 0){
//            tphphoto.setVisibility(View.VISIBLE);
            imagecacnel.setVisibility(View.VISIBLE);
            imagesview.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.VISIBLE);
        }else{
//            tphphoto.setVisibility(View.GONE);
            imagecacnel.setVisibility(View.GONE);
            imagesview.setVisibility(View.GONE);
            sliderLayout.setVisibility(View.GONE);
        }
        sliderLayout.removeAllSliders();
        for(File file : ALselectedImage){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            if(file.toString().startsWith("http:")){
                String surl = file.toString();
                if(file.toString().startsWith("http://")){

                }else if(file.toString().startsWith("http:/")){
                    surl =surl.replace("http:/","http://");
                }

                textSliderView
                        .image(GlobalHelper.setUrlFoto(surl))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }else {
                textSliderView
                        .image(file)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        imagecacnel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
                try {
                    if (sliderLayout.getCurrentPosition() == 0 && ALselectedImage.size() == 1) {
                        ALselectedImage.clear();
                        sliderLayout.removeAllSliders();
                        sliderLayout.setVisibility(View.GONE);
                        imagecacnel.setVisibility(View.GONE);
                        imagesview.setVisibility(View.GONE);
                    } else {
                        ALselectedImage.remove(sliderLayout.getCurrentPosition());
                        sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
                    }
                }catch (Exception e){
                    Log.e("sliderLayout", e.toString());
                }
            }
        });
        imagesview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WidgetHelper.showImagesZoom(getActivity(),ALselectedImage);
            }
        });
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        private AlertDialog alertDialogAllpoin;
        private Boolean validasi;
        int statLongOperation;

        public LongOperation(int statLongOperation) {
            this.statLongOperation = statLongOperation;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(getActivity(), getResources().getString(R.string.wait));
            if(statLongOperation == YesNo_Simpan) {
                validasi = validasiSensusBJR();
            }else{
                validasi = true;
            }
        }

        @Override
        protected String doInBackground(String... params) {
            if (validasi) {
                switch (Integer.parseInt(params[0])) {
                    case YesNo_Simpan:
                        validasi = sensusBjrHelper.save(selectedSensusBjr);
                    break;
                    case YesNo_Hapus:
                        validasi = sensusBjrHelper.hapus(selectedSensusBjr);
                    break;
                }
            }
            return String.valueOf(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            alertDialogAllpoin.dismiss();
            switch (Integer.parseInt(result)) {
                case YesNo_Simpan:
                    if (validasi) {
                        Toast.makeText(HarvestApp.getContext(), "Data Berhasil Disimpan", Toast.LENGTH_SHORT).show();
                        qcSensusBjrActivity.backProses();
                    } else {
                        Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Disimpan", Toast.LENGTH_SHORT).show();
                    }
                break;
                case YesNo_Hapus:
                    if (validasi) {
                        Toast.makeText(HarvestApp.getContext(), "Data Berhasil Dihapus", Toast.LENGTH_SHORT).show();
                        qcSensusBjrActivity.backProses();
                    } else {
                        Toast.makeText(HarvestApp.getContext(), "Data Tidak Bisa Dihapups", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }
}
