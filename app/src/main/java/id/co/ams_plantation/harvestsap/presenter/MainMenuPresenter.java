package id.co.ams_plantation.harvestsap.presenter;

import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.presenter.base.BasePresenter;
import id.co.ams_plantation.harvestsap.view.MainMenuView;

/**
 * Created by user on 11/21/2018.
 */

/*
    RULES :
    1. Always Extend ApiPresenter
    2. Always Attach Corresponding View
    3. Create method / data transaction logic between
    4. You may use Model directly/indirectly from this class
    5. You may use another helper class as long as its logic only
    6. This is logic only class.

    DO NOT :
    1. Update UI / set UI on this class
    2. Calling any View class.
 */

public class MainMenuPresenter extends BasePresenter {
    public User user;
    public MainMenuPresenter(MainMenuView view){attachView(view);}
}