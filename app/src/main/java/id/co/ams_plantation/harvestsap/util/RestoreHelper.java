package id.co.ams_plantation.harvestsap.util;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import id.co.ams_plantation.harvestsap.Fragment.SettingFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestsap.ui.MapActivity;
//import ir.mahdi.mzip.zip.ZipArchive;
import jp.wasabeef.recyclerview.adapters.SlideInLeftAnimationAdapter;

public class RestoreHelper {
    Context context;
    MapActivity mapActivity;
    SettingFragment settingFragment;
    android.support.v7.app.AlertDialog listrefrence;

    ArrayList<String> filter;
    ArrayList<String> origin;
    String namaFileSelected;

    public RestoreHelper(Context context) {
        this.context = context;
    }

    public void showAllRestore(){
        if(context instanceof MapActivity){
            mapActivity = ((MapActivity) context);
        }else if (context instanceof MainMenuActivity){
            Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
            if(fragment instanceof SettingFragment) {
                settingFragment = ((SettingFragment) fragment);
            }
        }

        File dbBackup = new File(GlobalHelper.getDatabasePathHMSBackup());
        String[] dirListing = dbBackup.list();
        Arrays.sort(dirListing);
        filter = new ArrayList<String>(Arrays.asList(dirListing));
        origin = new ArrayList<String>(Arrays.asList(dirListing));
        namaFileSelected = null;

        View view = LayoutInflater.from(context).inflate(R.layout.list_refrensi_object,null);
        TextView idHeader = (TextView) view.findViewById(R.id.idHeader);
        EditText etSearch_lso = (EditText) view.findViewById(R.id.et_search_lso);
        RecyclerView rv_usage_lso = (RecyclerView) view.findViewById(R.id.rv_usage_lso);

        idHeader.setText(context.getResources().getString(R.string.chose_restore));
        rv_usage_lso.setLayoutManager(new LinearLayoutManager(context));

        final SelectedRestoreAdapter adapter = new SelectedRestoreAdapter(context);
        final SlideInLeftAnimationAdapter adapterLeft = new SlideInLeftAnimationAdapter(adapter);
        adapterLeft.setFirstOnly(false);
//       adapterLeft.setInterpolator(new OvershootInterpolator(1f));
        adapterLeft.setDuration(400);
        etSearch_lso.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s.toString());
            }
        });
        rv_usage_lso.setAdapter(adapterLeft);

        listrefrence = WidgetHelper.showListReference(listrefrence,view,context);
    }

    class SelectedRestoreAdapter extends RecyclerView.Adapter<SelectedRestoreAdapter.Holder> {
        Context contextApdater;

        public SelectedRestoreAdapter(Context contextApdater) {
            this.contextApdater = contextApdater;
        }

        @NonNull
        @Override
        public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(contextApdater).inflate(R.layout.row_infowindow_chose,parent,false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull Holder holder, int position) {
            holder.setValue(filter.get(position));
        }

        @Override
        public int getItemCount() {
            return filter!=null?filter.size():0;
        }

        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults results = new FilterResults();
                    filter = new ArrayList<>();
                    String string = constraint.toString().toLowerCase();
                    int i = 0 ;
                    for (String s : origin) {

                        String [] split = s.split("_");
                        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd-MMMM-yyyy HH:mm");

                        if(s.toLowerCase().contains(string) || sdf.format(Long.parseLong(split[0].trim())).toLowerCase().contains(string)){
                            filter.add(s);
                        }
                    }
                    if (filter.size() > 0) {
                        results.count = filter.size();
                        results.values = filter;
                        return results;
                    } else {
                        return null;
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null) {
                        filter = (ArrayList<String>) results.values;
                    } else {
                        filter.clear();
                    }
                    notifyDataSetChanged();

                }
            };
        }

        public class Holder extends RecyclerView.ViewHolder {
            TextView tv_riw_chose;
            TextView tv_riw_chose1;
            CardView cv_riw_chose;
            ImageView iv_status;
            public Holder(View itemView) {
                super(itemView);
                cv_riw_chose = (CardView) itemView.findViewById(R.id.cv_riw_chose);
                tv_riw_chose = (TextView) itemView.findViewById(R.id.tv_riw_chose);
                tv_riw_chose1 = (TextView) itemView.findViewById(R.id.tv_riw_chose1);
                iv_status = (ImageView) itemView.findViewById(R.id.iv_status);
                iv_status.setVisibility(View.GONE);
            }

            public void setValue(String namaFile) {
                String [] split = namaFile.split("_");
                SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd-MMMM-yyyy HH:mm");
                tv_riw_chose.setText(sdf.format(Long.parseLong(split[0].trim())));
                tv_riw_chose1.setText(split[1].trim().replace(".zip",""));
                cv_riw_chose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        namaFileSelected = namaFile;
                        if(mapActivity != null){
                            mapActivity.startLongOperation(MapActivity.LongOperation_RestoreHMS,null);
                        }else if (settingFragment != null){
                            settingFragment.startLongOperation(SettingFragment.LongOperation_Restore_HMS);
                        }
                    }
                });
            }
        }
    }

    public boolean choseRestore(Object longOperation){
        if(mapActivity != null) {
            mapActivity.setUpDataSyncHelper.zipDBHms(longOperation, "Restore");
        }else if (settingFragment != null){
            settingFragment.setUpDataSyncHelper.zipDBHms(longOperation, "Restore");
        }else{
            return false;
        }

//        ZipArchive zipArchive = new ZipArchive();
//        ZipArchive.unzip(GlobalHelper.getDatabasePathHMSBackup() + namaFileSelected,Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES ,"");
        ZipManager.unzipFolder(GlobalHelper.getDatabasePathHMSBackup() + namaFileSelected,Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES,"");

        File f = new File(GlobalHelper.getDatabasePathHMSBackup(),namaFileSelected);
        //cek dahulu jika file last sync ada maka berhasil unzip
        File cekFileLastSync = new File(GlobalHelper.getDatabasePathHMS(),Encrypts.encrypt(GlobalHelper.LAST_SYNC));
        return cekFileLastSync.exists();
    }

    public void closeRestore(){
        File f = new File(GlobalHelper.getDatabasePathHMSBackup(),namaFileSelected);
        File cekFileLastSync = new File(GlobalHelper.getDatabasePathHMS(),Encrypts.encrypt(GlobalHelper.LAST_SYNC));
        if(cekFileLastSync.exists()) {
            listrefrence.dismiss();
            if (f.isFile()) {
                f.delete();
            }

            Toast.makeText(HarvestApp.getContext(),"Restore Berhasil",Toast.LENGTH_SHORT).show();
            if(mapActivity != null) {
                mapActivity.setAfdelingBlocksValue();
            }
        }else {
            Toast.makeText(HarvestApp.getContext(),"Restore Gagal",Toast.LENGTH_LONG).show();
            listrefrence.dismiss();
            if (mapActivity.alertDialog != null) {
                mapActivity.alertDialog.dismiss();
            }
        }
    }

}
