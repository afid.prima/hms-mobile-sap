package id.co.ams_plantation.harvestsap.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.utils.Utils;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.adapter.PemanenAdapter;
import id.co.ams_plantation.harvestsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.holder.PemanenRowHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestsap.TableView.view_model.PemanenTableViewModel;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.Pemanen;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.PemanenHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;

public class PemanenActivity extends BaseActivity {

    public static final int STATUS_LONG_OPERATION_NONE = 0;
    public static final int STATUS_LONG_OPERATION_SEARCH = 1;
    public static final int STATUS_LONG_OPERATION_SELECT_ITEM = 2;

    PemanenAdapter pemanenAdapter;
    PemanenTableViewModel mTableViewModel;
    TableView mTableView;
    SlidingUpPanelLayout mLayout;
    CompleteTextViewHelper etSearch;
    CardView footerCard;
    View btnShowForm;
    LinearLayout llheadumano;
    TextView footerEstate;
    TextView footerUser;
    TextView tvEstate;
    TextView tvAfdeling;
    ImageView ivchevronup;
    ImageView ivchevrondown;

    AlertDialog alertDialogBase;
    HashMap<String, Pemanen> origin;
    Pemanen selectedPemanen;
    Set<String> listSearch;
    PemanenHelper pemanenHelper;
    @Override
    protected void initPresenter() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pemanen_layout);
        getSupportActionBar().hide();

        mLayout = findViewById(R.id.sliding_layout);
        etSearch = findViewById(R.id.etSearch);
        mTableView = findViewById(R.id.tableview);
        footerCard = findViewById(R.id.footer_card);
        btnShowForm = findViewById(R.id.btnShowForm);
        llheadumano = findViewById(R.id.llheadumano);
        footerEstate = findViewById(R.id.footerEstate);
        footerUser = findViewById(R.id.footerUser);
        tvEstate = findViewById(R.id.tvEstate);
        tvAfdeling = findViewById(R.id.tvAfdeling);
        ivchevronup = findViewById(R.id.ivchevronup);
        ivchevrondown = findViewById(R.id.ivchevrondown);

        pemanenHelper = new PemanenHelper(this);
        toolBarSetup();
        mLayout.setTouchEnabled(false);
        llheadumano.setVisibility(View.GONE);

        footerUser.setText(GlobalHelper.getUser().getUserFullName());
        footerEstate.setText(GlobalHelper.getEstate().getCompanyShortName() + " - "
                + GlobalHelper.getEstate().getEstName());
        tvEstate.setText(GlobalHelper.getEstate().getCompanyShortName() + " - "
                + GlobalHelper.getEstate().getEstName());
        ivchevronup.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_up)
                .sizeDp(18)
                .colorRes(R.color.Gray));
        ivchevrondown.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_down)
                .sizeDp(18)
                .colorRes(R.color.Gray));
        etSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SEARCH));
            }
        });
        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                if(slideOffset == 0){
                    mLayout.setTouchEnabled(true);
                }
                Log.i("MapActivity", "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {

                if(footerCard.getVisibility() == View.VISIBLE){
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
                if(newState ==  SlidingUpPanelLayout.PanelState.EXPANDED){
                    ivchevronup.setVisibility(View.GONE);
                    ivchevrondown.setVisibility(View.VISIBLE);
                }else if (newState ==  SlidingUpPanelLayout.PanelState.ANCHORED){
                    ivchevronup.setVisibility(View.GONE);
                    ivchevrondown.setVisibility(View.VISIBLE);
                }else if (newState ==  SlidingUpPanelLayout.PanelState.COLLAPSED){
                    ivchevronup.setVisibility(View.VISIBLE);
                    ivchevrondown.setVisibility(View.GONE);
                }
            }
        });
        new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        TextView textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backProses();
            }
        });
        textToolbar.setText(getResources().getString(R.string.my_pemanen));
    }

    private void updateDataRv(int status) {
        origin = new HashMap<>();
        listSearch = new android.support.v4.util.ArraySet<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_PEMANEN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            Pemanen pemanen = gson.fromJson(dataNitrit.getValueDataNitrit(), Pemanen.class);
            if (status == STATUS_LONG_OPERATION_SEARCH) {
                if(pemanen.getKemandoran() != null){
                    if(pemanen.getKemandoran().toLowerCase().contains(etSearch.getText().toString().toLowerCase())){
                        origin.put(pemanen.getNik(), pemanen);
                    }else if (pemanen.getNama().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                            pemanen.getAfdeling().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                            pemanen.getGank().toLowerCase().contains(etSearch.getText().toString().toLowerCase())) {
                        origin.put(pemanen.getNik(), pemanen);
                    }
                }else if (pemanen.getNama().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                        pemanen.getAfdeling().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                        pemanen.getNik().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                        pemanen.getGank().toLowerCase().contains(etSearch.getText().toString().toLowerCase())) {
                    origin.put(pemanen.getNik(), pemanen);
                }
            } else if (status == STATUS_LONG_OPERATION_NONE) {
                origin.put(pemanen.getNik(), pemanen);
            }

            listSearch.add(pemanen.getAfdeling());
            listSearch.add(pemanen.getNik());
            listSearch.add(pemanen.getNama());
            listSearch.add(pemanen.getGank());
            if(pemanen.getKemandoran() != null) {
                listSearch.add(pemanen.getKemandoran());
            }
            if(pemanen.getNoUrut() == 0){
                listSearch.add("-");
            }else{
                listSearch.add(String.valueOf(pemanen.getNoUrut()));
            }
        }
        db.close();

    }

    private void updateUIRV(int status) {
        if (status == STATUS_LONG_OPERATION_NONE) {
            etSearch.setText("");
        }
        List<String> lSearch = new ArrayList<>();
        lSearch.addAll(listSearch);
        ArrayAdapter<String> adapterSearch = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, lSearch);
        etSearch.setAdapter(adapterSearch);
        etSearch.setThreshold(1);
        adapterSearch.notifyDataSetChanged();

        if (origin.size() > 0) {
            ArrayList<Pemanen> list = new ArrayList<>(origin.values());

            Collections.sort(list, new Comparator<Pemanen>() {
                @Override
                public int compare(Pemanen o1, Pemanen o2) {
                    return o1.getNoUrut() < o2.getNoUrut() ? -1 : (o1.getNoUrut() > o2.getNoUrut()) ? 1 : 0;
                }
            });

            mTableViewModel = new PemanenTableViewModel(this, list);
            // Create TableView Adapter
            pemanenAdapter = new PemanenAdapter(this, mTableViewModel);
            mTableView.setAdapter(pemanenAdapter);
            mTableView.setRowHeaderWidth(Utils.convertDpToPx(this, 100));
            mTableView.setTableViewListener(new ITableViewListener() {
                @Override
                public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellDoubleClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {
                        // Create Long Press Popup
                        ColumnHeaderPopup popup = new ColumnHeaderPopup(
                                (ColumnHeaderViewHolder) columnHeaderView, mTableView);
                        // Show
                        popup.show();
                    }
                }

                @Override
                public void onColumnHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    if (rowHeaderView != null && rowHeaderView instanceof PemanenRowHeaderViewHolder) {

                        String[] sid = ((PemanenRowHeaderViewHolder) rowHeaderView).getCellId().split("-");
                        selectedPemanen = origin.get(sid[1]);
//                        showFormPemanen();

                    }

                }

                @Override
                public void onRowHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }

                @Override
                public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }
            });
            pemanenAdapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel
                    .getRowHeaderList(), mTableViewModel.getCellList());

        }
    }

    private void showFormPemanen(){
        llheadumano.setVisibility(View.VISIBLE);
        footerCard.setVisibility(View.GONE);
        mLayout.setPanelHeight(Utils.convertDpToPx(this,65));
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        tvAfdeling.setText(selectedPemanen.getAfdeling());
        new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SELECT_ITEM));
    }

    public void closeForm(boolean withUpdateTable){
        llheadumano.setVisibility(View.GONE);
        footerCard.setVisibility(View.VISIBLE);
        mLayout.setPanelHeight(footerCard.getHeight());
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        if(withUpdateTable){
            startLongOperation();
        }
    }


    public void startLongOperation(){
        if(etSearch.getText().toString().isEmpty()){
            new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
        }else{
            new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SEARCH));
        }
    }

    public void backProses() {
        setResult(GlobalHelper.RESULT_PEMANENACTIVITY);
        finish();
    }

    @Override
    public void onBackPressed() {
        backProses();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (alertDialogBase != null) {
                alertDialogBase.cancel();
            }
            alertDialogBase = WidgetHelper.showWaitingDialog(PemanenActivity.this, getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])) {
                case STATUS_LONG_OPERATION_NONE:
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                case STATUS_LONG_OPERATION_SEARCH:
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
            }
            return strings[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            switch (Integer.parseInt(result)) {
                case STATUS_LONG_OPERATION_NONE:
                    updateUIRV(Integer.parseInt(result));
                    if (alertDialogBase != null) {
                        alertDialogBase.cancel();
                    }
                    break;

                case STATUS_LONG_OPERATION_SEARCH:
                    updateUIRV(Integer.parseInt(result));
                    if (alertDialogBase != null) {
                        alertDialogBase.cancel();
                    }
                    break;

                case STATUS_LONG_OPERATION_SELECT_ITEM:
                    Toast.makeText(HarvestApp.getContext(), selectedPemanen.getNama(), Toast.LENGTH_SHORT).show();
                    pemanenHelper.showFormPemanen(selectedPemanen,btnShowForm);
                    if (alertDialogBase != null) {
                        alertDialogBase.cancel();
                    }
                    break;

            }
        }
    }
}
