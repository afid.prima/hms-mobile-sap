package id.co.ams_plantation.harvestsap.model;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;

public class Cages {
    private String assetNo;
    private String assetName;
    private String estCode;
    private String assetCategory;
    private String assetType;
    private String assetCondition;
    private String assetStatus;
    private int createBy;
    private String createDate;
    private int updateBy;
    private String updateDate;
    private String generateId;
    private int inputType;

    public static final int inputTypeQr = 0 ;//=> qr = >hijau
    public static final int inputTypeMaster = 1 ;//=> master = >kuning
    public static final int inputTypeNoMaster = 2 ;//=> tidak ada di master = >merah

    public static final int COLOR_INPUTTYPEQR = HarvestApp.getContext().getResources().getColor(R.color.Green);
    public static final int COLOR_INPUTTYPEMASTER = HarvestApp.getContext().getResources().getColor(R.color.md_yellow_500);
    public static final int COLOR_INPUTTYPENOMASTER = HarvestApp.getContext().getResources().getColor(R.color.red_color);

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getAssetCategory() {
        return assetCategory;
    }

    public void setAssetCategory(String assetCategory) {
        this.assetCategory = assetCategory;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public String getAssetCondition() {
        return assetCondition;
    }

    public void setAssetCondition(String assetCondition) {
        this.assetCondition = assetCondition;
    }

    public String getAssetStatus() {
        return assetStatus;
    }

    public void setAssetStatus(String assetStatus) {
        this.assetStatus = assetStatus;
    }

    public int getCreateBy() {
        return createBy;
    }

    public void setCreateBy(int createBy) {
        this.createBy = createBy;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(int updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public void setGenerateId(String generateId){
        this.generateId = generateId;
    }

    public String getGenerateId() {
        return generateId;
    }

    public int getInputType() {
        return inputType;
    }

    public void setInputType(int inputType) {
        this.inputType = inputType;
    }
}
