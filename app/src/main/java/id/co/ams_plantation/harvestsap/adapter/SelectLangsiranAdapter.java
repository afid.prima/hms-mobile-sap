package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.Langsiran;

public class SelectLangsiranAdapter extends ArrayAdapter<Langsiran> {
    public ArrayList<Langsiran> items;
    public ArrayList<Langsiran> itemsAll;
    public ArrayList<Langsiran> suggestions;
    private final int viewResourceId;

    public SelectLangsiranAdapter(Context context, int viewResourceId, ArrayList<Langsiran> data){
        super(context, viewResourceId, data);
        this.items = new ArrayList<>();
        this.items.addAll(data);
        this.itemsAll = (ArrayList<Langsiran>)this.items.clone();
        this.suggestions = new ArrayList<>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        if(items.size()>position){
            Langsiran langsiran = items.get(position);
            if (langsiran != null) {
                ImageView iv = (ImageView) v.findViewById(R.id.iv);
                TextView tv = (TextView) v.findViewById(R.id.tv);
                TextView tv1 = (TextView) v.findViewById(R.id.tv1);
                iv.setVisibility(View.GONE);
                tv.setText(langsiran.getNamaTujuan());
                tv1.setText(langsiran.getBlock());
            }
        }
        return v;
    }

    public Langsiran getItemAt(int position){
        return items.get(position);
    }

    @Override
    public int getCount(){
        return items!=null?items.size():0;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((Langsiran)(resultValue)).getNamaTujuan();
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (Langsiran langsiran : itemsAll) {
                    if(langsiran.getNamaTujuan().toLowerCase().contains(constraint.toString().toLowerCase())){
                        suggestions.add(langsiran);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Langsiran> filteredList = (ArrayList<Langsiran>) results.values;
//            clear();
            items.clear();
//            itemsAll.clear();
            if(results != null && results.count > 0) {
//                addAll(filteredList);
//                itemsAll.addAll(filteredList);
//                for (Anggota anggota : filteredList){
//                    items.add(anggota);
//                }
                items.addAll(filteredList);
            }else{
//                clear();
                items.clear();
//                itemsAll.clear();
            }
            notifyDataSetChanged();
        }
    };
}
