package id.co.ams_plantation.harvestsap.model;

import java.util.ArrayList;

public class EstateAndAssistance {
    String companyShortName;
    String estCode;
    String estName;
    ArrayList<User> assistenAfdeling;

    public EstateAndAssistance(String companyShortName, String estCode, String estName, ArrayList<User> assistenAfdeling) {
        this.companyShortName = companyShortName;
        this.estCode = estCode;
        this.estName = estName;
        this.assistenAfdeling = assistenAfdeling;
    }

    public String getCompanyShortName() {
        return companyShortName;
    }

    public void setCompanyShortName(String companyShortName) {
        this.companyShortName = companyShortName;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getEstName() {
        return estName;
    }

    public void setEstName(String estName) {
        this.estName = estName;
    }

    public ArrayList<User> getAssistenAfdeling() {
        return assistenAfdeling;
    }

    public void setAssistenAfdeling(ArrayList<User> assistenAfdeling) {
        this.assistenAfdeling = assistenAfdeling;
    }
}
