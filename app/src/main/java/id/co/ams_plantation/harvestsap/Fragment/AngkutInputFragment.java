package id.co.ams_plantation.harvestsap.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.labo.kaji.fragmentanimations.MoveAnimation;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.SwipeItemRecycleView.SwipeController;
import id.co.ams_plantation.harvestsap.SwipeItemRecycleView.SwipeControllerActions;
import id.co.ams_plantation.harvestsap.TableView.adapter.AngkutAdapter;
import id.co.ams_plantation.harvestsap.TableView.adapter.RekonsilasiAdapter;
import id.co.ams_plantation.harvestsap.TableView.holder.AngkutRowHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.holder.RekonsilasiRowHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestsap.TableView.view_model.AngkutTableViewModel;
import id.co.ams_plantation.harvestsap.TableView.view_model.RekonsilasiTableViewModel;
import id.co.ams_plantation.harvestsap.adapter.CagesInputAdapter;
import id.co.ams_plantation.harvestsap.adapter.LoaderInputAdapter;
import id.co.ams_plantation.harvestsap.adapter.PengirimanGandaAdapater;
import id.co.ams_plantation.harvestsap.adapter.SelectLangsiranAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectOperatorAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectPksAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectSpkAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectSupervisionAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectUserAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectVehicleAdapter;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.Langsiran;
import id.co.ams_plantation.harvestsap.model.Operator;
import id.co.ams_plantation.harvestsap.model.Cages;
import id.co.ams_plantation.harvestsap.model.PKS;
import id.co.ams_plantation.harvestsap.model.PengirimanGanda;
import id.co.ams_plantation.harvestsap.model.SPK;
import id.co.ams_plantation.harvestsap.model.StampImage;
import id.co.ams_plantation.harvestsap.model.SupervisionList;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.model.Vehicle;
import id.co.ams_plantation.harvestsap.service.BluetoothService;
import id.co.ams_plantation.harvestsap.ui.AngkutActivity;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.NfcHelper;
import id.co.ams_plantation.harvestsap.util.PksHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiSpbHelper;
import id.co.ams_plantation.harvestsap.util.TujuanHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import ng.max.slideview.SlideView;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by user on 12/26/2018.
 */

public class AngkutInputFragment extends Fragment {

    public static final int STATUS_LONG_OPERATION_NONE = 0;
    public static final int STATUS_LONG_OPERATION_SEARCH = 1;
    public static final int STATUS_LONG_OPERATION_SAVE_DATA_SHARE = 2;
    public static final int STATUS_LONG_OPERATION_SAVE_DATA_PRINT = 3;
    public static final int STATUS_LONG_OPERATION_SAVE_DATA_TAP_NFC = 4;
    public static final int STATUS_LONG_OPERATION_TAP_NFC_ONLY = 5;
    public static final int STATUS_LONG_OPERATION_UPDATE_TOTAL = 6;
    public static final int STATUS_LONG_OPERATION_TAP_NFC = 7;
    public static final int STATUS_LONG_OPERATION_POPUP_MAP = 999;

    public TujuanHelper tujuanHelper;
    public PksHelper pksHelper;
    TransaksiAngkutHelper transaksiAngkutHelper;
    TransaksiSpbHelper transaksiSpbHelper;
    BaseActivity baseActivity;
    AngkutActivity angkutActivity;

//    LinearLayout historyTransaksi;
//    LinearLayout scanQr;
//    LinearLayout newTransaksiPanen;

    RelativeLayout lShowHide;
    RelativeLayout lShow;
    RelativeLayout lHide;
    LinearLayout lShowHideIsi;
    ImageView ivShow;
    ImageView ivHide;

    AngkutAdapter adapter;
    AngkutTableViewModel mTableViewModel;
    RekonsilasiAdapter rekonsilasiAdapter;
    RekonsilasiTableViewModel mRekonsilasiTableViewModel;
    TableView mTableView;
    //    Filter mTableFilter;
    AppCompatEditText etSearch;
    Button btnSearch;
    Button btnClsoe;
    RelativeLayout ltableview;

//    TextView tvAngkutId;
    TextView tvCreateTime;
    BetterSpinner lsTujuan;
    BetterSpinner lsKendaraan;
    CompleteTextViewHelper etVehicle;
    CompleteTextViewHelper etDriver;
    //tambahan zendi
    TextView tvJanjangTitle;
    TextView tvBrondolanTitle;
    //
    TextView tvTotalOph;
    TextView tvTotalTph;
    TextView tvTotalJanjang;
    TextView tvTotalBrondolan;
    TextView tvTotalBeratEstimasi;
    //tambahan zendi
    LinearLayout lTotalBerat;
    //
    LinearLayout lLangsiran;
    LinearLayout lPks;
    LinearLayout lKeluar;
    LinearLayout lSPK;
    TextView tvSpbID;
    TextView tvSpbIDKeluar;
    CompleteTextViewHelper tvLangsiran;
    CompleteTextViewHelper etSpk;
//    CompleteTextViewHelper etBin;
    CompleteTextViewHelper tvPks;
    CompleteTextViewHelper etApprove;
    CompleteTextViewHelper etApproveKeluar;
    CompleteTextViewHelper etSubstitute;
    CheckBox cbKraniSubstitute;
    CheckBox cbPengirimanGanda;
//    CheckBox cbBin;
    RecyclerView rvLoader;
    RecyclerView rvCages;
    RecyclerView rvPengirimanGanda;

    View tphphoto;
    ImageView ivPhoto;
    ImageView imagecacnel;
    ImageView imagesview;
    SliderLayout sliderLayout;
    RelativeLayout rl_ipf_takepicture;

    ArrayList<Operator> loaders;
    ArrayList<Cages> listCages;
    ArrayList<PengirimanGanda> pengirimanGandas;
    Vehicle vehicleUse;
    SPK spkUse;
    User approveBy;
    SupervisionList substituteSelected;
    int tJanjang = 0;
    int tBrondolan = 0;
    double tBeratEstimasi = 0.0;
    int tKartuHilang;
    int tKartuRusak;
    int tujuanAngkut=0;
    int kendaraanDari=0;
    double latitude;
    double longitude;

    HashMap<String,SPK> allSpkAngkut;
    HashMap<String,Operator> loaderSave;
    HashMap<String,PengirimanGanda> pengirimanGandaSave;
    HashSet<String> distinctAfdeling;

    String idSpb;

    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm");
    SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");

    TransaksiAngkut headerTransaksiAngkut;
    TransaksiAngkut selectedTransaksiAngkut;
    TransaksiSPB selectedTransaksiSPB;
    Langsiran selectedLangsiran;
    PKS selectedPks;
    Operator selectedOperator;
    ArrayList<Cages> cages;
    SelectUserAdapter adapterAssAfd;

    SelectPksAdapter adapterPks;
    SelectLangsiranAdapter adapterLangsiran;
    SelectOperatorAdapter adapterOperator;
    SelectVehicleAdapter adapterVehicle;
    SelectSupervisionAdapter adapterSupervision;
    SelectSpkAdapter adapterSpk;
    LoaderInputAdapter adapterLoader;
    CagesInputAdapter adapterCages;
    PengirimanGandaAdapater adapterPengirimanGanda;

    SwipeController swipeController;
    SwipeController swipeControllerPengiriman;

    AlertDialog alertDialog;
    boolean popupTPH;
    boolean bisaEdit;
    List<String> bAlas;

    private boolean isRekonsilasi=false;

    StampImage stampImage;
    ArrayList<File> ALselectedImage;
    HashMap<String,Angkut> angkutView;

    public static AngkutInputFragment getInstance() {
        AngkutInputFragment inputFragment = new AngkutInputFragment();
        return inputFragment;
    }

    public static AngkutInputFragment getInstance(boolean isRekonsilasi) {
        AngkutInputFragment inputFragment = new AngkutInputFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("rekonsiliasiOnly", isRekonsilasi);
        inputFragment.setArguments(bundle);
        return inputFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            isRekonsilasi = getArguments().getBoolean("rekonsiliasiOnly");
        }
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return MoveAnimation.create(MoveAnimation.UP,enter,600);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.angkut_input_layout, null, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        baseActivity = (BaseActivity) getActivity();
        angkutActivity = (AngkutActivity) getActivity();
        tujuanHelper = new TujuanHelper(getActivity());
        pksHelper = new PksHelper(getActivity());

//        tvAngkutId = view.findViewById(R.id.tvAngkutId);
        lShowHide = view.findViewById(R.id.lShowHide);
        lShowHideIsi = view.findViewById(R.id.lShowHideIsi);
        lShow = view.findViewById(R.id.lShow);
        lHide = view.findViewById(R.id.lHide);
        ivShow = view.findViewById(R.id.ivShow);
        ivHide = view.findViewById(R.id.ivHide);
        ltableview = view.findViewById(R.id.ltableview);
        tvCreateTime = view.findViewById(R.id.tvCreateTime);
        etVehicle = view.findViewById(R.id.etVehicle);
        lsTujuan = view.findViewById(R.id.lsTujuan);
        lsKendaraan = view.findViewById(R.id.lsKendaraan);
        etDriver = view.findViewById(R.id.etDriver);
        lTotalBerat = view.findViewById(R.id.lTotalBerat);
        lLangsiran = view.findViewById(R.id.lLangsiran);
        lPks = view.findViewById(R.id.lPks);
        lKeluar = view.findViewById(R.id.lKeluar);
        lSPK = view.findViewById(R.id.lSPK);

        tvLangsiran = view.findViewById(R.id.tvLangsiran);
        etSpk = view.findViewById(R.id.etSpk);
//        etBin = view.findViewById(R.id.etBin);
        tvPks = view.findViewById(R.id.tvPks);
        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        btnClsoe = view.findViewById(R.id.btnClsoe);
        mTableView = view.findViewById(R.id.tableview);
//        historyTransaksi = view.findViewById(R.id.historyTransaksi);
//        scanQr = view.findViewById(R.id.scanQr);
//        newTransaksiPanen = view.findViewById(R.id.newTransaksiPanen);
        tvBrondolanTitle = view.findViewById(R.id.tvBrondolTitle);
        tvJanjangTitle = view.findViewById(R.id.tvJanjangTitle);

        tvTotalOph = view.findViewById(R.id.tvTotalOph);
        tvTotalTph = view.findViewById(R.id.tvTotalTph);
        tvTotalJanjang = view.findViewById(R.id.tvTotalJanjang);
        tvTotalBrondolan = view.findViewById(R.id.tvTotalBrondolan);
        tvTotalBeratEstimasi = view.findViewById(R.id.tvTotalBeratEstimasi);

//        cbBin = view.findViewById(R.id.cb_bin);
        cbPengirimanGanda = view.findViewById(R.id.cb_pengiriman_ganda);
        cbKraniSubstitute = view.findViewById(R.id.cb_krani_substitute);
        etSubstitute = view.findViewById(R.id.etSubstitute);

        rvPengirimanGanda = view.findViewById(R.id.rvPengirimanGanda);
        rvLoader = view.findViewById(R.id.rvLoader);
        etApprove = view.findViewById(R.id.etApprove);
        etApproveKeluar = view.findViewById(R.id.etApproveKeluar);
        tvSpbID = view.findViewById(R.id.tvSpbID);
        tvSpbIDKeluar = view.findViewById(R.id.tvSpbIDKeluar);

        tphphoto = (View) view.findViewById(R.id.tphphoto);
        ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
        imagecacnel = (ImageView) view.findViewById(R.id.imagecacnel);
        imagesview = (ImageView) view.findViewById(R.id.imagesview);
        rl_ipf_takepicture = (RelativeLayout) view.findViewById(R.id.rl_ipf_takepicture);
        sliderLayout = (SliderLayout) view.findViewById(R.id.sliderLayout);

        transaksiAngkutHelper = new TransaksiAngkutHelper(getActivity());
        transaksiSpbHelper = new TransaksiSpbHelper(getActivity());
        lKeluar.setVisibility(View.VISIBLE);
        lLangsiran.setVisibility(View.GONE);
//        lSPK.setVisibility(View.GONE);
        lPks.setVisibility(View.GONE);
//        etBin.setVisibility(View.GONE);
        lShowHideIsi.setVisibility(View.GONE);
        rvPengirimanGanda.setVisibility(View.GONE);
        lShow.setVisibility(View.VISIBLE);
        lHide.setVisibility(View.GONE);
        btnClsoe.setVisibility(View.GONE);

        ivShow.setImageDrawable(new
                IconicsDrawable(getActivity())
                .icon(MaterialDesignIconic.Icon.gmi_chevron_down).sizeDp(24)
                .colorRes(R.color.Black));

        ivHide.setImageDrawable(new
                IconicsDrawable(getActivity())
                .icon(MaterialDesignIconic.Icon.gmi_chevron_up).sizeDp(24)
                .colorRes(R.color.Black));

//        historyTransaksi.setVisibility(View.GONE);
        popupTPH= false;
        ALselectedImage = new ArrayList<>();

        Collection<User> assAfd = GlobalHelper.dataAllAsstAfd.values();
        Collection<PKS> pkss = GlobalHelper.dataAllPks.values();
        Collection<Langsiran> langsirans = GlobalHelper.dataAllLagsiran.values();
        Collection<Operator> operators = GlobalHelper.dataOperator.values();
        Collection<Cages> listCages = GlobalHelper.dataOperatorCages.values();
        Collection<Vehicle> vehicles = GlobalHelper.dataVehicle.values();
        Collection<SupervisionList> sSupervisionList = GlobalHelper.dataAllSupervision.values();
        allSpkAngkut = GlobalHelper.getDataAllSPKAngkut();

        bAlas = new ArrayList<>();
        bAlas.add(TransaksiAngkut.PKS);
        bAlas.add(TransaksiAngkut.LANGSIR);
        bAlas.add(TransaksiAngkut.KELAUR);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, bAlas);
        lsTujuan.setAdapter(adapter);
        lsTujuan.setText(bAlas.get(TransaksiAngkut.TUJUAN_KELUAR));

        List<String> bKendaraan = new ArrayList<>();
        bKendaraan.add(TransaksiAngkut.KEBUN);
        bKendaraan.add(TransaksiAngkut.LUAR);
        ArrayAdapter<String> adapterKendaran = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, bKendaraan);
        lsKendaraan.setAdapter(adapterKendaran);
        if(vehicles.size() > 0 && operators.size() > 0 ){
            lsKendaraan.setText(bKendaraan.get(TransaksiAngkut.KENDARAAN_KEBUN));
        }else{
            lsKendaraan.setText(bKendaraan.get(TransaksiAngkut.KENDARAAN_LUAR));
//            lSPK.setVisibility(View.VISIBLE);
        }

        if(((BaseActivity) getActivity()).currentlocation != null){
            latitude = ((BaseActivity) getActivity()).currentlocation.getLatitude();
            longitude = ((BaseActivity) getActivity()).currentlocation.getLongitude();
            if(angkutActivity.searchingGPS != null){
                angkutActivity.searchingGPS.dismiss();
            }
        }

//        HashMap<String,User> dataAllUser = GlobalHelper.dataAllUser;
//        ArrayList<User> users = angkutActivity.users;
//        for(Map.Entry<String,User> e : dataAllUser.entrySet()) {
//            String key = e.getKey();
//            User value = e.getValue();
//            users.add(value);
//        }

//        HashMap<String,PKS> dataAllPks = GlobalHelper.dataAllPks;
//        ArrayList<PKS> pkss = angkutActivity.pkss;
//        for(Map.Entry<String,PKS> e : dataAllPks.entrySet()) {
//            String key = e.getKey();
//            PKS value = e.getValue();
//            pkss.add(value);
//        }

//        HashMap<String,Langsiran> dataAllLagsiran = GlobalHelper.dataAllLagsiran;
//        ArrayList<Langsiran> langsirans = angkutActivity.langsirans;
//        for(Map.Entry<String,Langsiran> e : dataAllLagsiran.entrySet()) {
//            String key = e.getKey();
//            Langsiran value = e.getValue();
//            langsirans.add(value);
//        }

//        HashMap<String,Operator> dataOperator = GlobalHelper.dataOperator;
//        ArrayList<Operator> operators = angkutActivity.operators;
//        for(Map.Entry<String,Operator> e : dataOperator.entrySet()) {
//            String key = e.getKey();
//            Operator value = e.getValue();
//            operators.add(value);
//        }

//        HashMap<String,Vehicle> dataVehicle = GlobalHelper.dataVehicle;
//        ArrayList<Vehicle> vehicles = angkutActivity.vehicles;

        adapterAssAfd = new SelectUserAdapter(getActivity(),R.layout.row_object, new ArrayList<User>(assAfd));
        adapterPks = new SelectPksAdapter(getActivity(),R.layout.row_object, new ArrayList<PKS>(pkss));
        adapterLangsiran = new SelectLangsiranAdapter(getActivity(),R.layout.row_setting, new ArrayList<Langsiran>(langsirans));
        adapterOperator = new SelectOperatorAdapter(getActivity(),R.layout.row_setting, new ArrayList<Operator>(operators));
        adapterVehicle= new SelectVehicleAdapter(getActivity(),R.layout.row_setting, new ArrayList<Vehicle>(vehicles));
        adapterSupervision = new SelectSupervisionAdapter(getActivity(), R.layout.row_setting, new ArrayList<SupervisionList>(sSupervisionList));
        adapterSpk = new SelectSpkAdapter(getActivity(), R.layout.row_setting, new ArrayList<SPK>(allSpkAngkut.values()));

        SimpleDateFormat sdfDate = new SimpleDateFormat("ddMMyy");
        idSpb = "S" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT)) + sdfDate.format(System.currentTimeMillis()) + GlobalHelper.getUser().getUserID();
        tvSpbID.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));
        tvSpbIDKeluar.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));
        etApprove.setAdapter(adapterAssAfd);
        etApprove.setThreshold(1);
        etApproveKeluar.setAdapter(adapterAssAfd);
        etApproveKeluar.setThreshold(1);
        adapterAssAfd.notifyDataSetChanged();

        approveBy = (User) assAfd.toArray()[0];
        etApprove.setText(approveBy.getUserFullName());
        etApproveKeluar.setText(approveBy.getUserFullName());

        etVehicle.setAdapter(adapterVehicle);
        etVehicle.setThreshold(1);
        adapterVehicle.notifyDataSetChanged();

        etDriver.setAdapter(adapterOperator);
        etDriver.setThreshold(1);
        adapterOperator.notifyDataSetChanged();

        etSpk.setAdapter(adapterSpk);
        etSpk.setThreshold(1);
        adapterSpk.notifyDataSetChanged();

        etSubstitute.setAdapter(adapterSupervision);
        etSubstitute.setThreshold(1);
        adapterSupervision.notifyDataSetChanged();

        if(angkutActivity.selectedTransaksiAngkut == null){
            bisaEdit = true;
        }else bisaEdit = angkutActivity.selectedTransaksiAngkut.getStatus() != TransaksiAngkut.UPLOAD;

        Gson gson = new Gson();
        headerTransaksiAngkut = gson.fromJson(TransaksiAngkutHelper.readAngkutSelected(),TransaksiAngkut.class);
        //pertama laod kesini
        if(headerTransaksiAngkut != null){
            tvCreateTime.setText(angkutActivity.getResources().getString(R.string.create_time)+" "+sdf.format(headerTransaksiAngkut.getStartDate()));
            if(headerTransaksiAngkut.getSupir() != null){
                if(headerTransaksiAngkut.getSupir().getNama() != null){
                    etDriver.setText(headerTransaksiAngkut.getSupir().getNama());
                    selectedOperator = headerTransaksiAngkut.getSupir();
                }
            }

            if(headerTransaksiAngkut.getVehicle() != null){
                etVehicle.setText(TransaksiAngkutHelper.showVehicle(headerTransaksiAngkut.getVehicle()));
                vehicleUse = headerTransaksiAngkut.getVehicle();
            }

            if(headerTransaksiAngkut.getKendaraanDari() == TransaksiAngkut.KENDARAAN_LUAR) {
                etVehicle.setAdapter(null);
                etVehicle.setThreshold(0);
                etDriver.setAdapter(null);
                etDriver.setThreshold(0);

                //lSPK.setVisibility(View.VISIBLE);
            }

            if(headerTransaksiAngkut.getSpk() != null){
                etSpk.setText(headerTransaksiAngkut.getSpk().getIdSPK());
                spkUse = headerTransaksiAngkut.getSpk();
            }

            approveBy = GlobalHelper.dataAllAsstAfd.get(headerTransaksiAngkut.getApproveBy());
            if(approveBy != null) {
                etApproveKeluar.setText(approveBy.getUserFullName());
                etApprove.setText(approveBy.getUserFullName());
            }else{
                etApproveKeluar.setText(headerTransaksiAngkut.getApproveBy());
                etApprove.setText(headerTransaksiAngkut.getApproveBy());
            }
            lsKendaraan.setText(bKendaraan.get(headerTransaksiAngkut.getKendaraanDari()));

            if(headerTransaksiAngkut.getSubstitute() != null){
                substituteSelected = headerTransaksiAngkut.getSubstitute();
                cbKraniSubstitute.setChecked(true);
                etSubstitute.setVisibility(View.VISIBLE);
                etSubstitute.setText(substituteSelected.getNama());
            }else{
                substituteSelected = null;
                cbKraniSubstitute.setChecked(false);
                etSubstitute.setVisibility(View.GONE);
            }

            if(headerTransaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_PKS){
                lsTujuan.setText(bAlas.get(TransaksiAngkut.TUJUAN_PKS));
                lPks.setVisibility(View.VISIBLE);
                lKeluar.setVisibility(View.GONE);
                lLangsiran.setVisibility(View.GONE);
                tvPks.setAdapter(adapterPks);
                tvPks.setThreshold(1);
                adapterPks.notifyDataSetChanged();
                if(headerTransaksiAngkut.getPks() != null){
                    lLangsiran.setVisibility(View.GONE);
                    lPks.setVisibility(View.VISIBLE);
                    lKeluar.setVisibility(View.GONE);
                    tvPks.setText(headerTransaksiAngkut.getPks().getNamaPKS());
                    selectedPks = headerTransaksiAngkut.getPks();
                }else{
                    selectedPks = (PKS) GlobalHelper.dataAllPks.values().toArray()[0];
                    if(selectedPks != null){
                        lLangsiran.setVisibility(View.GONE);
                        lPks.setVisibility(View.VISIBLE);
                        lKeluar.setVisibility(View.GONE);
                        tvPks.setText(selectedPks.getNamaPKS());
                    }
                }
            }else if(headerTransaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR) {
                lsTujuan.setText(bAlas.get(TransaksiAngkut.TUJUAN_LANGSIR));
                lPks.setVisibility(View.GONE);
                lKeluar.setVisibility(View.GONE);
                lLangsiran.setVisibility(View.VISIBLE);
                tvLangsiran.setAdapter(adapterLangsiran);
                tvLangsiran.setThreshold(1);
                adapterLangsiran.notifyDataSetChanged();

                if(headerTransaksiAngkut.getLangsiran() != null) {
                    lLangsiran.setVisibility(View.VISIBLE);
                    lPks.setVisibility(View.GONE);
                    lKeluar.setVisibility(View.GONE);
                    tvLangsiran.setText(headerTransaksiAngkut.getLangsiran().getNamaTujuan());
                    selectedLangsiran = headerTransaksiAngkut.getLangsiran();
                }
            }else if(headerTransaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_KELUAR){
                lsTujuan.setText(bAlas.get(TransaksiAngkut.TUJUAN_KELUAR));
                lPks.setVisibility(View.GONE);
                lLangsiran.setVisibility(View.GONE);
                lKeluar.setVisibility(View.VISIBLE);
            }

            if(headerTransaksiAngkut.getLoader() != null){
                loaders = headerTransaksiAngkut.getLoader();
            }

            if(headerTransaksiAngkut.getPengirimanGandas() != null){
                cbPengirimanGanda.setChecked(true);
                rvPengirimanGanda.setVisibility(View.VISIBLE);
                pengirimanGandas = headerTransaksiAngkut.getPengirimanGandas();
            }

//            if(headerTransaksiAngkut.getBin() != null){
//                cbBin.setChecked(true);
//                etBin.setVisibility(View.VISIBLE);
//                etBin.setText(headerTransaksiAngkut.getBin());
//            }

            if(headerTransaksiAngkut.getFoto() != null){
                if(headerTransaksiAngkut.getFoto().size() > 0){
                    ALselectedImage = new ArrayList<>();
                    for(String file : headerTransaksiAngkut.getFoto()) {
                        ALselectedImage.add(new File(file));
                    }
                    setupimageslide();
                }
            }

            boolean adaDiDBSPB = false;
            if(headerTransaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_PKS || headerTransaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_KELUAR){
                Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_SPB);
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", headerTransaksiAngkut.getIdSPB()));
                if(cursor.size() > 0){
                    for(DataNitrit dataNitrit : cursor){
                        selectedTransaksiSPB = gson.fromJson(dataNitrit.getValueDataNitrit(),TransaksiSPB.class);
                        idSpb = selectedTransaksiSPB.getIdSPB();
                        tvSpbID.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));
                        tvSpbIDKeluar.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));

                        approveBy = GlobalHelper.dataAllUser.get(selectedTransaksiSPB.getApproveBy());
                        if(approveBy != null) {
                            etApprove.setText(approveBy.getUserFullName());
                        }else{
                            etApprove.setText(selectedTransaksiSPB.getApproveBy());
                        }
                        adaDiDBSPB = true;
                        break;
                    }
                }
                db.close();
            }


            if(headerTransaksiAngkut.getIdSPB() != null && !adaDiDBSPB){
                idSpb = headerTransaksiAngkut.getIdSPB();
                tvSpbID.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));
                tvSpbIDKeluar.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));
            }

        }else if (headerTransaksiAngkut == null){
            SimpleDateFormat sdfD = new SimpleDateFormat("ddMMyy");
            headerTransaksiAngkut = new TransaksiAngkut();
            headerTransaksiAngkut.setIdTAngkut("D" +String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT))
            + sdfD.format(System.currentTimeMillis())
            + GlobalHelper.getUser().getUserID());
            headerTransaksiAngkut.setIdSPB(idSpb);
            headerTransaksiAngkut.setStartDate(System.currentTimeMillis());
            headerTransaksiAngkut.setTujuan(2);
            headerTransaksiAngkut.setApproveBy(approveBy.getUserID());

            tvCreateTime.setText(angkutActivity.getResources().getString(R.string.create_time)+" "+sdf.format(headerTransaksiAngkut.getStartDate()));
            if(vehicles.size() > 0 && operators.size() > 0){
                lsKendaraan.setText(bKendaraan.get(TransaksiAngkut.KENDARAAN_KEBUN));
            }else {
                headerTransaksiAngkut.setKendaraanDari(TransaksiAngkut.KENDARAAN_LUAR);
                lsKendaraan.setText(bKendaraan.get(TransaksiAngkut.KENDARAAN_LUAR));
                //lSPK.setVisibility(View.VISIBLE);
                etVehicle.setAdapter(null);
                etVehicle.setThreshold(0);
                etDriver.setAdapter(null);
                etDriver.setThreshold(0);
            }
            lPks.setVisibility(View.GONE);
            lKeluar.setVisibility(View.VISIBLE);
            lsTujuan.setText(bAlas.get(2));

            updateHeaderTransaksiAngkut();
        }

        if(!bisaEdit){
//            scanQr.setVisibility(View.GONE);
//            newTransaksiPanen.setVisibility(View.GONE);
            TransaksiAngkutHelper.hapusFileAngkut(false);
            if(angkutActivity.selectedTransaksiAngkut.getStatus() != TransaksiAngkut.UPLOAD) {
                TransaksiAngkutHelper.createNewAngkut(angkutActivity.selectedTransaksiAngkut);
            }
        }

//        cbBin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    etBin.setVisibility(View.VISIBLE);
//                }else{
//                    etBin.setVisibility(View.GONE);
//                    headerTransaksiAngkut.setBin(null);
//                }
//                updateHeaderTransaksiAngkut();
//            }
//        });

        if(loaders == null){
            loaders = new ArrayList<>();
            loaders.add(new Operator());
        }else{
            if(loaders.size() == 0){
                loaders = new ArrayList<>();
                loaders.add(new Operator());
            }
        }
        adapterLoader = new LoaderInputAdapter(getActivity(),new ArrayList<Operator>(operators),loaders);
        rvLoader.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvLoader.setAdapter(adapterLoader);



        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                if (loaders.size() > 1) {
                    adapterLoader.loader.remove(position);

                    RecyclerView.ViewHolder viewHolder = rvLoader.findViewHolderForAdapterPosition(position);
                    View v = viewHolder.itemView;
                    TextView tvValue = v.findViewById(R.id.tvValue);

                    if(!tvValue.getText().toString().isEmpty()) {
                        Gson gson = new Gson();
                        Operator loaderHapus = gson.fromJson(tvValue.getText().toString(),Operator.class);
                        for (int i = 0; i < loaders.size(); i++) {
                            if(loaders.get(i).getKodeOperator().equalsIgnoreCase(loaderHapus.getKodeOperator())){
                                loaders.remove(i);
                                break;
                            }
                        }
                    }

                    adapterLoader.notifyItemRemoved(position);
                }
            }



            @Override
            public void onLeftClicked(int position) {
                if(adapterLoader.loader.size() < GlobalHelper.MAX_LOADER){
                    for (int i = 0 ; i < rvLoader.getAdapter().getItemCount();i++) {
                        RecyclerView.ViewHolder viewHolder = rvLoader.findViewHolderForAdapterPosition(i);
                        if (viewHolder != null) {
                            View v = viewHolder.itemView;
                            CompleteTextViewHelper etLoader = view.findViewById(R.id.etLoader);
                            TextView tvValue = v.findViewById(R.id.tvValue);

                            if (tvValue.getText().toString().isEmpty()) {
                                Toast.makeText(HarvestApp.getContext(), getActivity().getResources().getString(R.string.chose_loader), Toast.LENGTH_SHORT).show();
                                etLoader.requestFocus();
                                return;
                            }
                        }
                    }


                    adapterLoader.loader.add(new Operator());
                    adapterLoader.notifyItemInserted(adapterLoader.loader.size());
                }else{
                    Toast.makeText(HarvestApp.getContext(), "Max Loader 15", Toast.LENGTH_SHORT).show();
                }
            }
        },adapterLoader);

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(rvLoader);

        if (this.listCages == null){
            this.listCages = new ArrayList<>();
            this.listCages.add(new Cages());
        } else {
            if(this.listCages.size() == 0){
                this.listCages = new ArrayList<>();
                this.listCages.add(new Cages());
            }
        }

        adapterCages = new CagesInputAdapter(getActivity(), new ArrayList<Cages>(listCages), this.listCages);
        rvCages.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvCages.setAdapter(adapterCages);

        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                if (AngkutInputFragment.this.listCages.size() > 1) {
                    adapterCages.Cages.remove(position);
                    RecyclerView.ViewHolder viewHolder = rvCages.findViewHolderForAdapterPosition(position);
                    View v = viewHolder.itemView;
                    TextView tvValue = v.findViewById(R.id.tvValue);

                    if (!tvValue.getText().toString().isEmpty()) {
                        Gson gson = new Gson();
                        Cages cagesHapus = gson.fromJson(tvValue.getText().toString(), Cages.class);
                        for (int i = 0; i < AngkutInputFragment.this.listCages.size(); i++) {
                            if (AngkutInputFragment.this.listCages.get(i).getAssetNo().equalsIgnoreCase(cagesHapus.getAssetNo())) {

                                AngkutInputFragment.this.listCages.remove(i);
                                break;
                            }
                        }
                    }
                    adapterCages.notifyItemRemoved(position);

                }

            }

            @Override
            public void onLeftClicked(int position){
                if(adapterCages.Cages.size() < GlobalHelper.MAX_CAGES){
                    for (int i = 0; i < Objects.requireNonNull(rvCages.getAdapter()).getItemCount(); i++){
                        RecyclerView.ViewHolder viewHolder = rvCages.findViewHolderForAdapterPosition(i);
                        if(viewHolder != null){
                            View v = viewHolder.itemView;
                            CompleteTextViewHelper etCages = view.findViewById(R.id.etCages);
                            TextView tvValue = v.findViewById(R.id.tvValue);

                            if(tvValue.getText().toString().isEmpty()) {
                                Toast.makeText(HarvestApp.getContext(), Objects.requireNonNull(getActivity()).getResources().getString(R.string.chose_cages), Toast.LENGTH_SHORT ).show();
                                etCages.requestFocus();
                                return;
                            }
                            }
                        }
                        adapterCages.Cages.add(new Cages());
                    adapterCages.notifyItemInserted(adapterCages.Cages.size());
                    } else {
                    Toast.makeText(HarvestApp.getContext(),"Max Cages 5",Toast.LENGTH_SHORT).show();
                }
                }
        },adapterCages);




        cbPengirimanGanda.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    rvPengirimanGanda.setVisibility(View.VISIBLE);
                    headerTransaksiAngkut.setPengirimanGandas(pengirimanGandas);
                }else{
                    rvPengirimanGanda.setVisibility(View.GONE);
                    headerTransaksiAngkut.setPengirimanGandas(null);
                }
                updateHeaderTransaksiAngkut();
            }
        });

        if(pengirimanGandas == null){
            pengirimanGandas = new ArrayList<>();
            pengirimanGandas.add(new PengirimanGanda(0,TransaksiAngkut.KENDARAAN_KEBUN,new Operator(),new Vehicle()));
        }else{
            if(pengirimanGandas.size() == 0){
                pengirimanGandas = new ArrayList<>();
                pengirimanGandas.add(new PengirimanGanda(0,TransaksiAngkut.KENDARAAN_KEBUN,new Operator(),new Vehicle()));
            }
        }
        adapterPengirimanGanda = new PengirimanGandaAdapater(getActivity(),pengirimanGandas,adapterOperator,adapterVehicle,adapterSpk,adapterLangsiran);
        rvPengirimanGanda.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvPengirimanGanda.setAdapter(adapterPengirimanGanda);

        swipeControllerPengiriman = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                if(adapterPengirimanGanda.pengirimanGandas.size() > 1){
                    adapterPengirimanGanda.pengirimanGandas.remove(position);
                    adapterLoader.notifyItemRemoved(position);
                }
            }

            @Override
            public void onLeftClicked(int position) {
                if(adapterPengirimanGanda.pengirimanGandas.size() < 3){
                    for (int i = 0 ; i < rvPengirimanGanda.getAdapter().getItemCount();i++) {
                        RecyclerView.ViewHolder viewHolder = rvPengirimanGanda.findViewHolderForAdapterPosition(i);
                        if (viewHolder != null) {
                            View v = viewHolder.itemView;
                            CompleteTextViewHelper etVehicle = v.findViewById(R.id.etVehicle);
                            CompleteTextViewHelper etDriver = v.findViewById(R.id.etDriver);
                            TextView tvValueUnit = v.findViewById(R.id.tvValueUnit);
                            TextView tvValueOperator = v.findViewById(R.id.tvValueOperator);

                            if (tvValueUnit.getText().toString().isEmpty()) {
                                Toast.makeText(HarvestApp.getContext(), "Kendaraan", Toast.LENGTH_SHORT).show();
                                etVehicle.requestFocus();
                                return;
                            }
                            if (tvValueOperator.getText().toString().isEmpty()) {
                                Toast.makeText(HarvestApp.getContext(), "Operator", Toast.LENGTH_SHORT).show();
                                etDriver.requestFocus();
                                return;
                            }
                        }
                    }

                    adapterPengirimanGanda.pengirimanGandas.add(new PengirimanGanda(0,TransaksiAngkut.KENDARAAN_KEBUN,new Operator(),new Vehicle()));
                    adapterPengirimanGanda.notifyItemInserted(adapterPengirimanGanda.pengirimanGandas.size());
                }
            }
        },adapterPengirimanGanda);

        ItemTouchHelper itemTouchhelperPengiriman = new ItemTouchHelper(swipeControllerPengiriman);
        itemTouchhelperPengiriman.attachToRecyclerView(rvPengirimanGanda);

        lShowHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lShowHideIsi.getVisibility() == View.GONE){
                    lShowHideIsi.setVisibility(View.VISIBLE);
                    lHide.setVisibility(View.VISIBLE);
                    btnClsoe.setVisibility(View.VISIBLE);
                    lShow.setVisibility(View.GONE);
                }else{
                    lShowHideIsi.setVisibility(View.GONE);
                    lHide.setVisibility(View.GONE);
                    btnClsoe.setVisibility(View.GONE);
                    lShow.setVisibility(View.VISIBLE);
                }
            }
        });

        lsKendaraan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                headerTransaksiAngkut.setKendaraanDari(i);
                if(i == TransaksiAngkut.KENDARAAN_KEBUN){
                    if(vehicles.toArray().length > 0 && operators.toArray().length > 0) {
                        etDriver.setAdapter(adapterOperator);
                        etDriver.setThreshold(1);
                        adapterOperator.notifyDataSetChanged();
                        setOperator((Operator) GlobalHelper.dataOperator.values().toArray()[0]);

                        etVehicle.setAdapter(adapterVehicle);
                        etVehicle.setThreshold(1);
                        adapterVehicle.notifyDataSetChanged();
                        setVehicle((Vehicle) GlobalHelper.dataVehicle.values().toArray()[0]);
                        setSpk(null);
                        //lSPK.setVisibility(View.GONE);
                    }else{
                        //lSPK.setVisibility(View.VISIBLE);
                        etDriver.setAdapter(null);
                        etVehicle.setAdapter(null);
                        headerTransaksiAngkut.setKendaraanDari(TransaksiAngkut.KENDARAAN_LUAR);
                        Toast.makeText(HarvestApp.getContext(),"Hanya Bisa Set Kontrak Karena TABEL Operator atau Vehicle Kosong",Toast.LENGTH_SHORT).show();
                    }
                }else if(i == TransaksiAngkut.KENDARAAN_LUAR){
                    etDriver.setText("");
                    etDriver.setAdapter(null);
                    etDriver.setThreshold(0);
                    setOperator(new Operator());

                    etVehicle.setText("");
                    etVehicle.setAdapter(null);
                    etVehicle.setThreshold(0);
                    setVehicle(new Vehicle());
                    //lSPK.setVisibility(View.VISIBLE);
                }

                updateHeaderTransaksiAngkut();
            }
        });

        lsTujuan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                headerTransaksiAngkut.setTujuan(i);
                if(i == TransaksiAngkut.TUJUAN_PKS){

                    lLangsiran.setVisibility(View.GONE);
                    lPks.setVisibility(View.VISIBLE);
                    lKeluar.setVisibility(View.GONE);
                    selectedLangsiran = null;
                    headerTransaksiAngkut.setLangsiran(selectedLangsiran);
                    headerTransaksiAngkut.setIdSPB(idSpb);

                    tvPks.setAdapter(adapterPks);
                    tvPks.setThreshold(1);
                    adapterPks.notifyDataSetChanged();
                    if(baseActivity.currentLocationOK()){
                        PKS pksX = null;
                        double dis = 0.0;
                        for(Map.Entry<String,PKS> e : GlobalHelper.dataAllPks.entrySet()) {
                            PKS value = e.getValue();
                            if(pksX == null){
                                pksX = value;
                                dis = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(),pksX.getLatitude(),
                                        baseActivity.currentlocation.getLongitude(),pksX.getLongitude());
                            }else{
                                double disX = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(),value.getLatitude(),
                                        baseActivity.currentlocation.getLongitude(),value.getLongitude());

                                if(dis >= disX){
                                    dis = disX;
                                    pksX = value;
                                }
                            }
                        }
                        if(pksX == null){

                            lLangsiran.setVisibility(View.GONE);
                            lPks.setVisibility(View.GONE);
                            lKeluar.setVisibility(View.VISIBLE);
                            selectedPks = null;
                            selectedLangsiran = null;
                            headerTransaksiAngkut.setPks(selectedPks);
                            headerTransaksiAngkut.setLangsiran(selectedLangsiran);
                            headerTransaksiAngkut.setIdSPB(idSpb);

                            angkutActivity.searchingGPS = Snackbar.make(angkutActivity.myCoordinatorLayout, "Tidak Ada Master Pks Di " + GlobalHelper.getEstate().getEstCode() + " - " + GlobalHelper.getEstate().getEstName(), Snackbar.LENGTH_INDEFINITE);
                            angkutActivity.searchingGPS.show();
                        }else {
                            setPks(pksX);
                        }
//                                popupTPH = true;
//                                new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_POPUP_MAP));
                    }
//                            else{
//                                angkutActivity.searchingGPS = Snackbar.make(angkutActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                                angkutActivity.searchingGPS.show();
//                            }

                }else if(i == TransaksiAngkut.TUJUAN_LANGSIR){
                    lLangsiran.setVisibility(View.VISIBLE);
                    lPks.setVisibility(View.GONE);
                    lKeluar.setVisibility(View.GONE);
                    selectedPks = null;
                    headerTransaksiAngkut.setPks(selectedPks);
                    headerTransaksiAngkut.setIdSPB(null);

                    tvLangsiran.setAdapter(adapterLangsiran);
                    tvLangsiran.setThreshold(1);
                    adapterLangsiran.notifyDataSetChanged();
                    if(baseActivity.currentLocationOK()){
                        Langsiran langsiranX = null;
                        double dis = 0.0;
                        for(Map.Entry<String,Langsiran> e : GlobalHelper.dataAllLagsiran.entrySet()) {
                            Langsiran value = e.getValue();
                            if(langsiranX == null){
                                langsiranX = value;
                                dis = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(),langsiranX.getLatitude(),
                                        baseActivity.currentlocation.getLongitude(),langsiranX.getLongitude());
                            }else{
                                double disX = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(),value.getLatitude(),
                                        baseActivity.currentlocation.getLongitude(),value.getLongitude());

                                if(dis >= disX){
                                    dis = disX;
                                    langsiranX = value;
                                }
                            }
                        }
                        setLangsiran(langsiranX);
//                                popupTPH = true;
//                                new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_POPUP_MAP));
                    }
//                            else{
//                                angkutActivity.searchingGPS = Snackbar.make(angkutActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                                angkutActivity.searchingGPS.show();
//                            }
                }else if (i == TransaksiAngkut.TUJUAN_KELUAR){
                    lLangsiran.setVisibility(View.GONE);
                    lPks.setVisibility(View.GONE);
                    lKeluar.setVisibility(View.VISIBLE);
                    selectedPks = null;
                    selectedLangsiran = null;
                    headerTransaksiAngkut.setPks(selectedPks);
                    headerTransaksiAngkut.setLangsiran(selectedLangsiran);
                    headerTransaksiAngkut.setIdSPB(idSpb);

                }

                updateHeaderTransaksiAngkut();
            }
        });

        etVehicle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etVehicle.showDropDown();
            }
        });

        etVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etVehicle.setText("");
                etVehicle.showDropDown();
            }
        });

        etVehicle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    setVehicle(((SelectVehicleAdapter) parent.getAdapter()).getItemAt(position));
                }
            }
        });

        etVehicle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (lsKendaraan.getText().toString().equals(TransaksiAngkut.LUAR)) {
                    Vehicle veh = new Vehicle();
                    veh.setVehicleCode(etVehicle.getText().toString());
                    vehicleUse = veh;
                    updateHeaderTransaksiAngkut();
                }
            }
        });

        etApprove.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etApprove.showDropDown();
            }
        });

        etApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etApprove.setText("");
                etApprove.showDropDown();
            }
        });

        etApprove.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = ((SelectUserAdapter) parent.getAdapter()).getItemAt(position);
                etApprove.setText(user.getUserFullName());
                etApproveKeluar.setText(user.getUserFullName());
                approveBy = user;

                headerTransaksiAngkut.setApproveBy(user.getUserID());
                updateHeaderTransaksiAngkut();
            }
        });

        etApproveKeluar.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etApproveKeluar.showDropDown();
            }
        });

        etApproveKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etApproveKeluar.setText("");
                etApproveKeluar.showDropDown();
            }
        });

        etApproveKeluar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = ((SelectUserAdapter) parent.getAdapter()).getItemAt(position);
                etApprove.setText(user.getUserFullName());
                etApproveKeluar.setText(user.getUserFullName());
                approveBy = user;

                headerTransaksiAngkut.setApproveBy(user.getUserID());
                updateHeaderTransaksiAngkut();
            }
        });

        etDriver.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etDriver.showDropDown();
            }
        });

        etDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etDriver.setText("");
                etDriver.showDropDown();
            }
        });
        etDriver.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Operator operator = ((SelectOperatorAdapter) parent.getAdapter()).getItemAt(position);
                setOperator(operator);
            }
        });

        etDriver.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    if (lsKendaraan.getText().toString().equals(TransaksiAngkut.LUAR)) {
                        Operator ope = new Operator();
                        ope.setNama(etDriver.getText().toString());
                        selectedOperator = ope;
                        updateHeaderTransaksiAngkut();
                    }
                }
            }
        });

        etSpk.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etSpk.showDropDown();
            }
        });

        etSpk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSpk.setText("");
                etSpk.showDropDown();
            }
        });

        etSpk.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    SPK spk = ((SelectSpkAdapter) parent.getAdapter()).getItemAt(position);
                    setSpk(spk);
                }
            }
        });

        etSpk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    spkUse = null;
                    updateHeaderTransaksiAngkut();
                }
            }
        });

//        etBin.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                headerTransaksiAngkut.setBin(s.toString());
//                updateHeaderTransaksiAngkut();
//            }
//        });

        cbKraniSubstitute.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etSubstitute.setVisibility(View.VISIBLE);
                }else{
                    headerTransaksiAngkut.setSubstitute(null);
                    etSubstitute.setVisibility(View.GONE);
                    substituteSelected = null;
                }
                updateHeaderTransaksiAngkut();
            }
        });

        etSubstitute.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    etSubstitute.showDropDown();
            }
        });

        etSubstitute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSubstitute.setText("");
                etSubstitute.showDropDown();
            }
        });

        etSubstitute.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    substituteSelected = ((SelectSupervisionAdapter) parent.getAdapter()).getItemAt(position);
                    etSubstitute.setText(substituteSelected.getNama());
                    headerTransaksiAngkut.setSubstitute(substituteSelected);
                    updateHeaderTransaksiAngkut();
                }
            }
        });

        tvPks.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    tvPks.showDropDown();
            }
        });

        tvPks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPks.setText("");
                tvPks.showDropDown();
            }
        });

        tvPks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    adapterPks = (SelectPksAdapter) parent.getAdapter();
                    PKS pks = adapterPks.getItemAt(position);
                    setPks(pks);
                }
            }
        });

        tvLangsiran.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    tvLangsiran.showDropDown();
            }
        });

        tvLangsiran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvLangsiran.setText("");
                tvLangsiran.showDropDown();
            }
        });

        tvLangsiran.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(fileAngkutSelected.exists()) {
                    Langsiran langsiran = ((SelectLangsiranAdapter) parent.getAdapter()).getItemAt(position);
                    setLangsiran(langsiran);
                }
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SEARCH));
            }
        });

//        historyTransaksi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                transaksiAngkutHelper.showHistoryTransaksi(selectedTransaksiAngkut);
//            }
//        });

//        scanQr.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getActivity(), QRScan.class);
//                startActivityForResult(intent, GlobalHelper.RESULT_SCAN_QR);
//            }
//        });
//
//        newTransaksiPanen.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getActivity().getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.content_container, TphInputFragment.getInstance(1))
////                        .addToBackStack(null)
//                        .commit();
//            }
//        });

        if(isRekonsilasi){
            btnClsoe.setText(HarvestApp.getContext().getResources().getString(R.string.print));
        }else{
            btnClsoe.setText(HarvestApp.getContext().getResources().getString(R.string.done));
        }

        btnClsoe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isRekonsilasi){
                    showDialogCloseTransaksiAngkut();
                }else{
                    closeAngkut();
                }

            }
        });

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });

        new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
    }

    public void updateLoader(){
        try {
            loaders = new ArrayList<>();
            JSONArray loaderArray = new JSONArray();
            for (int i = 0; i < rvLoader.getAdapter().getItemCount(); i++) {
                RecyclerView.ViewHolder viewHolder = rvLoader.findViewHolderForAdapterPosition(i);
                if (viewHolder != null) {
                    Gson gson = new Gson();
                    View v = viewHolder.itemView;
                    TextView tvValue = v.findViewById(R.id.tvValue);
                    if (!tvValue.getText().toString().isEmpty()) {
                        loaders.add(gson.fromJson(tvValue.getText().toString(), Operator.class));
                        loaderArray.put(new JSONObject(tvValue.getText().toString()));
                    }
                }
            }
            headerTransaksiAngkut.setLoader(loaders);
            updateHeaderTransaksiAngkut();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void updateCages(){
        try {
            listCages = new ArrayList<>();
            JSONArray cagesArray = new JSONArray();
            for (int i = 0; i < Objects.requireNonNull(rvCages.getAdapter()).getItemCount(); i++){
                RecyclerView.ViewHolder viewHolder = rvCages.findViewHolderForAdapterPosition(i);
                if (viewHolder != null){
                    Gson gson  = new Gson();
                    View v = viewHolder.itemView;
                    TextView tvValue = v.findViewById(R.id.tvValue);
                    if(!tvValue.getText().toString().isEmpty()){
                        listCages.add(gson.fromJson(tvValue.getText().toString(), Cages.class));
                        cagesArray.put(new JSONObject(tvValue.getText().toString()));
                    }
                }
            }
            headerTransaksiAngkut.setCages(listCages);
            updateHeaderTransaksiAngkut();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void updatePengirimanGanda(){
        pengirimanGandas = new ArrayList<>();
        for(int i = 0 ;i< rvPengirimanGanda.getAdapter().getItemCount();i++){
            RecyclerView.ViewHolder viewHolder = rvPengirimanGanda.findViewHolderForAdapterPosition(i);
            if (viewHolder != null) {
                Gson gson = new Gson();
                View v = viewHolder.itemView;
                BetterSpinner lsKendaraan = v.findViewById(R.id.lsKendaraan);
                TextView tvValueUnit = v.findViewById(R.id.tvValueUnit);
                TextView tvValueOperator = v.findViewById(R.id.tvValueOperator);
                TextView tvValueSpk = v.findViewById(R.id.tvValueSpk);
                int kendaraanDari = 0;
                SPK spk = new SPK();
                switch (lsKendaraan.getText().toString().toUpperCase()){
                    case TransaksiAngkut.KEBUN:
                        kendaraanDari = TransaksiAngkut.KENDARAAN_KEBUN;
                        break;
                    case TransaksiAngkut.LUAR:
                        kendaraanDari = TransaksiAngkut.KENDARAAN_LUAR;
                        break;
                }
                if(!tvValueSpk.getText().toString().isEmpty()) {
                    spk = gson.fromJson(tvValueSpk.getText().toString(), SPK.class);
                }
                PengirimanGanda ganda = new PengirimanGanda(i,
                        kendaraanDari,
                        gson.fromJson(tvValueOperator.getText().toString(),Operator.class),
                        gson.fromJson(tvValueUnit.getText().toString(),Vehicle.class)
                );

                ganda.setSpk(spk);
                pengirimanGandas.add(ganda);
            }
        }
        headerTransaksiAngkut.setPengirimanGandas(pengirimanGandas);
        updateHeaderTransaksiAngkut();
    }

    private void updateInfoStamp(){
        if(stampImage == null){
            stampImage = new StampImage();
        }
        if (selectedLangsiran != null) {
            stampImage.setLangsiran(selectedLangsiran);
            stampImage.setBlock(selectedLangsiran.getBlock());
        } else if (selectedPks != null) {
            stampImage.setPks(selectedPks);
            stampImage.setBlock(selectedPks.getBlock());
        }

        if (selectedOperator.getNama() != null) {
            stampImage.setOperator(selectedOperator);
        } else {
            Operator X = new Operator();
            X.setNama(etDriver.getText().toString());
            stampImage.setOperator(X);
        }


        if (vehicleUse.getVehicleCode() != null) {
            stampImage.setVehicle(vehicleUse);
        } else {
            Vehicle X = new Vehicle();
            X.setVehicleName(etVehicle.getText().toString());
            stampImage.setVehicle(X);
        }

//        if(etBin.getVisibility() == View.VISIBLE){
//            stampImage.setBin(etBin.getText().toString());
//        }

        Gson gson  = new Gson();
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(HarvestApp.STAMP_IMAGES,gson.toJson(stampImage));
        editor.apply();
    }

    private void takePicture() {
        updateInfoStamp();
        GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_TRANSAKSI_ANGKUT;
        EasyImage.openCamera(getActivity(),1);
    }

    public void addImages(File newImages){
        try {
            boolean pernahAda = false;
            if (headerTransaksiAngkut.getFoto() != null) {
                if(headerTransaksiAngkut.getFoto().size() > 0) {
                    pernahAda = true;
                }
            }

            if(pernahAda){
                headerTransaksiAngkut.getFoto().add(newImages.getAbsolutePath());
            }else{
                HashSet<String> hFoto = new HashSet<>();
                hFoto.add(newImages.getAbsolutePath());
                headerTransaksiAngkut.setFoto(hFoto);
            }

            ALselectedImage.add(newImages);

            updateHeaderTransaksiAngkut();
            setupimageslide();
        }catch ( Exception e){
            e.printStackTrace();
        }
    }

    private void setupimageslide(){

        Set<File> sf = new HashSet<>();
        sf.addAll(ALselectedImage);
        ALselectedImage.clear();
        ALselectedImage.addAll(sf);

        if(ALselectedImage.size() > 3){
            ivPhoto.setVisibility(View.GONE);
        }else{
            ivPhoto.setVisibility(View.VISIBLE);
        }
        if(ALselectedImage.size() != 0){
//            tphphoto.setVisibility(View.VISIBLE);
            imagecacnel.setVisibility(View.VISIBLE);
            imagesview.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.VISIBLE);
        }else{
//            tphphoto.setVisibility(View.GONE);
            imagecacnel.setVisibility(View.GONE);
            imagesview.setVisibility(View.GONE);
            sliderLayout.setVisibility(View.GONE);
        }
        sliderLayout.removeAllSliders();
        for(File file : ALselectedImage){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            if(file.toString().startsWith("http:")){
                String surl = file.toString();
                if(file.toString().startsWith("http://")){

                }else if(file.toString().startsWith("http:/")){
                    surl =surl.replace("http:/","http://");
                }

                textSliderView
                        .image(GlobalHelper.setUrlFoto(surl))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }else {
                textSliderView
                        .image(file)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        imagecacnel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
                try {
                    if (sliderLayout.getCurrentPosition() == 0 && ALselectedImage.size() == 1) {
                        ALselectedImage.clear();
                        sliderLayout.removeAllSliders();
                        sliderLayout.setVisibility(View.GONE);
                        imagecacnel.setVisibility(View.GONE);
                        imagesview.setVisibility(View.GONE);
                    } else {
                        String fileSelected = ALselectedImage.get(sliderLayout.getCurrentPosition()).getAbsolutePath();
                        ALselectedImage = new ArrayList();
                        if (headerTransaksiAngkut.getFoto() != null) {
                            if (headerTransaksiAngkut.getFoto().size() > 0) {
                                for (String file : headerTransaksiAngkut.getFoto()) {
                                    if (!file.startsWith("http:")) {
                                        if (fileSelected.equalsIgnoreCase(file)) {
                                            headerTransaksiAngkut.getFoto().remove(file);
                                            sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
                                        } else {
                                            ALselectedImage.add(new File(file));
                                        }
                                    }
                                }
                            }
                        }
                        updateHeaderTransaksiAngkut();
                    }
                }catch (Exception e){
                    Log.e("sliderLayout", e.toString());
                }
            }
        });
        imagesview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WidgetHelper.showImagesZoom(getActivity(),ALselectedImage);
            }
        });
    }


    public void closeAngkut(){
        //untuk data angkut baru saja yang bisa masuk ke sini
        //jika data transaksi angkut yang lama maka bisa share print dan tap nfc saja
        if (angkutActivity.originAngkut.size() == 0) {
            angkutActivity.closeActivity();
        }else if(bisaEdit && angkutActivity.originAngkut.size() > 0) {

            if(isRekonsilasi){
                showDialogCloseTransaksiAngkut();
            }else{
                if(baseActivity.currentLocationOK()) {
//                // unutk proses angkut radius tidak di ikutkan karena tidak memungkinkan tap nfc pada tahap ini
//                if (selectedLangsiran != null) {
//                    double distance = GlobalHelper.distance(selectedLangsiran.getLatitude(),baseActivity.currentlocation.getLatitude(),
//                            selectedLangsiran.getLongitude(),baseActivity.currentlocation.getLongitude());
//                    if(distance > GlobalHelper.RADIUS){
//                        Toast.makeText(HarvestApp.getContext(),getActivity().getResources().getString(R.string.radius_to_long_with_langsiran),Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//                }
                }else{
//                // jika angkut tidak perlu cek gps
//                angkutActivity.searchingGPS = Snackbar.make(angkutActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                angkutActivity.searchingGPS.show();
//                return;
                }

                if(etVehicle.getText().toString().trim().isEmpty()){
                    etVehicle.requestFocus();
                    Toast.makeText(HarvestApp.getContext(),getActivity().getResources().getString(R.string.plase_input_vehicle),Toast.LENGTH_SHORT).show();
                    return;
                }

                if(etDriver.getText().toString().trim().isEmpty()){
                    etDriver.requestFocus();
                    Toast.makeText(HarvestApp.getContext(),getActivity().getResources().getString(R.string.plase_input_driver),Toast.LENGTH_SHORT).show();
                    return;
                }

                if(lsTujuan.getText().toString().equals(TransaksiAngkut.LANGSIR)){
                    if(selectedLangsiran == null){
                        tvLangsiran.requestFocus();
                        Toast.makeText(HarvestApp.getContext(),"Mohon pilih Langsiran",Toast.LENGTH_SHORT).show();
                        return;
                    }
                }else if (lsTujuan.getText().toString().equals(TransaksiAngkut.PKS)){
                    if(selectedPks == null){
                        tvPks.requestFocus();
                        Toast.makeText(HarvestApp.getContext(),"Mohon pilih Pks",Toast.LENGTH_SHORT).show();
//                        new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_POPUP_MAP));
//                        popupTPH = true;
                        return;
                    }
                }

                View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.yes_no, null);
                TextView tvDialogKet = (TextView) dialogView.findViewById(R.id.ket1);
                SlideView btnCancelDialog = (SlideView) dialogView.findViewById(R.id.btnCancel);
                SlideView btnOkDialog = (SlideView) dialogView.findViewById(R.id.btnOK);

                final AlertDialog dialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle)
                        .setView(dialogView)
                        .setCancelable(false)
                        .create();

                final SpannableStringBuilder sb = new SpannableStringBuilder(lsTujuan.getText().toString());
                final ForegroundColorSpan fcs = new ForegroundColorSpan(getActivity().getResources().getColor(R.color.Red));
                final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
                sb.setSpan(fcs, 0, lsTujuan.getText().toString().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                sb.setSpan(bss, 0, lsTujuan.getText().toString().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                tvDialogKet.setText("Apakah Anda Sudah Sampai " + sb);
                btnOkDialog.setText("Sudah");
                btnCancelDialog.setText("Belum");

                btnCancelDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                    @Override
                    public void onSlideComplete(SlideView slideView) {
                        dialog.dismiss();
                    }
                });
                btnOkDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
                    @Override
                    public void onSlideComplete(SlideView slideView) {
                        dialog.dismiss();
                        showDialogCloseTransaksiAngkut();
                    }
                });
                dialog.show();
            }

        }else{
            showDialogCloseTransaksiAngkut();
        }
    }

    private boolean validasiDeklarasiAngkut(){

        if(((BaseActivity) getActivity()).currentlocation != null){
            latitude = ((BaseActivity) getActivity()).currentlocation.getLatitude();
            longitude = ((BaseActivity) getActivity()).currentlocation.getLongitude();
            if(angkutActivity.searchingGPS != null){
                angkutActivity.searchingGPS.dismiss();
            }
        }
//        else{
//            angkutActivity.searchingGPS = Snackbar.make(angkutActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//            angkutActivity.searchingGPS.show();
//            return false;
//        }

        String s = headerTransaksiAngkut.getIdTAngkut();
        Long l = headerTransaksiAngkut.getStartDate();

        if (lsTujuan.getText().toString().equalsIgnoreCase(TransaksiAngkut.PKS) && selectedPks == null) {
            Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.chose_pks), Toast.LENGTH_SHORT).show();
            return false;
        }

        if(angkutActivity.originAngkut.size() == 0 ){
            Toast.makeText(HarvestApp.getContext(),getActivity().getResources().getString(R.string.mohon_angkut),Toast.LENGTH_SHORT).show();
            return false;
        }

        boolean adaManual = false;
        for(Angkut dataAngkut : angkutActivity.originAngkut.values()){
            if(dataAngkut.getManualSPBData()!=null){
                adaManual = true;
                break;
            }
        }

        if(adaManual){
            if(ALselectedImage.size() == 0){
                Toast.makeText(HarvestApp.getContext(),"Mohon Foto List Angkut Dari Supir",Toast.LENGTH_SHORT).show();
                takePicture();
                return false;
            }
        }

        Set<String> distincOperator = new HashSet<>();
        Set<String> distincVehicle = new HashSet<>();
        Set<String> distincPengirimanGandaSPK = new HashSet<>();
        ArrayList<String> arrayListOperator = new ArrayList<>();
        ArrayList<String> arrayListVehicle = new ArrayList<>();
        ArrayList<String> arrayListSpk = new ArrayList<>();

        if(selectedOperator == null){
            Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Supir",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            distincOperator.add(selectedOperator.getKodeOperator());
            arrayListOperator.add(selectedOperator.getKodeOperator());
        }

        if(vehicleUse == null){
            Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Vehicle",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            distincVehicle.add(TransaksiAngkutHelper.showVehicle(vehicleUse));
            arrayListVehicle.add(TransaksiAngkutHelper.showVehicle(vehicleUse));
        }

        if(spkUse != null){
            distincPengirimanGandaSPK.add(spkUse.getIdSPK());
            arrayListSpk.add(spkUse.getIdSPK());
        }


        if(cbPengirimanGanda.isChecked()) {
            pengirimanGandaSave = new HashMap<>();
            ArrayList<SPK> pengirimanGandaSPk = new ArrayList<>();
            for (int i = 0; i < rvPengirimanGanda.getAdapter().getItemCount(); i++) {
                RecyclerView.ViewHolder viewHolder = rvPengirimanGanda.findViewHolderForAdapterPosition(i);
                if (viewHolder != null) {
                    View v = viewHolder.itemView;
                    BetterSpinner lsKendaraan = v.findViewById(R.id.lsKendaraan);
                    CompleteTextViewHelper etVehicle = v.findViewById(R.id.etVehicle);
                    CompleteTextViewHelper etDriver = v.findViewById(R.id.etDriver);
                    TextView tvValueUnit = v.findViewById(R.id.tvValueUnit);
                    TextView tvValueOperator = v.findViewById(R.id.tvValueOperator);
                    TextView tvValueSpk = v.findViewById(R.id.tvValueSpk);

                    if (tvValueUnit.getText().toString().isEmpty()) {
                        etVehicle.requestFocus();
                        Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Vehicle", Toast.LENGTH_SHORT).show();
                        return false;
                    } else if (tvValueOperator.getText().toString().isEmpty()) {
                        etDriver.requestFocus();
                        Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Operator", Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    Gson gson = new Gson();
                    Vehicle vehicle = gson.fromJson(tvValueUnit.getText().toString(), Vehicle.class);
                    Operator operator = gson.fromJson(tvValueOperator.getText().toString(), Operator.class);
                    SPK spk = null;

                    distincOperator.add(operator.getKodeOperator());
                    arrayListOperator.add(operator.getKodeOperator());

                    distincVehicle.add(TransaksiAngkutHelper.showVehicle(vehicle));
                    arrayListVehicle.add(TransaksiAngkutHelper.showVehicle(vehicle));

                    int kendaraanDari = 0;
                    switch (lsKendaraan.getText().toString().toUpperCase()) {
                        case TransaksiAngkut.KEBUN:
                            kendaraanDari = TransaksiAngkut.KENDARAAN_KEBUN;
                            break;
                        case TransaksiAngkut.LUAR:
                            kendaraanDari = TransaksiAngkut.KENDARAAN_LUAR;
                            break;
                    }

                    if(!tvValueSpk.getText().toString().isEmpty()){
                        spk = gson.fromJson(tvValueSpk.getText().toString(),SPK.class);
                        pengirimanGandaSPk.add(spk);
                        distincPengirimanGandaSPK.add(spk.getIdSPK());
                        arrayListSpk.add(spk.getIdSPK());
                    }

                    PengirimanGanda ganda = new PengirimanGanda(i, kendaraanDari, operator, vehicle,spk);
                    pengirimanGandaSave.put(TransaksiAngkutHelper.showVehicle(vehicle) , ganda);
                } else if (i < pengirimanGandas.size()) {
                    PengirimanGanda ganda = pengirimanGandas.get(i);
                    if (ganda.getVehicle().getVehicleCode().isEmpty()) {
                        Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Vehicle", Toast.LENGTH_SHORT).show();
                        return false;
                    } else if (ganda.getOperator().getNama().isEmpty()) {
                        Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Operator", Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    if(ganda.getOperator() != null){
                        distincOperator.add(ganda.getOperator().getKodeOperator());
                        arrayListOperator.add(ganda.getOperator().getKodeOperator());
                    }
                    if(ganda.getVehicle() != null){
                        distincVehicle.add(TransaksiAngkutHelper.showVehicle(ganda.getVehicle()));
                        arrayListVehicle.add(TransaksiAngkutHelper.showVehicle(ganda.getVehicle()));
                    }
                    if(ganda.getSpk() != null){
                        if(ganda.getSpk().getIdSPK() != null){
                            pengirimanGandaSPk.add(ganda.getSpk());
                            distincPengirimanGandaSPK.add(ganda.getSpk().getIdSPK());
                            arrayListSpk.add(ganda.getSpk().getIdSPK());
                        }
                    }

                    pengirimanGandaSave.put(TransaksiAngkutHelper.showVehicle(ganda.getVehicle()), ganda);
                }
            }

            if (pengirimanGandaSave.size() != rvPengirimanGanda.getAdapter().getItemCount()) {
                Toast.makeText(HarvestApp.getContext(), "Tidak Boleh Ada Unit Yang Sama Dalam Pengiriman Ganda", Toast.LENGTH_SHORT).show();
                return false;
            }

            if (pengirimanGandaSave.get(TransaksiAngkutHelper.showVehicle(vehicleUse)) != null) {
                Toast.makeText(HarvestApp.getContext(), "Unit Yang Sudah Dipilih Tidak Boleh Ada Dalam Pengiriman Ganda", Toast.LENGTH_SHORT).show();
                return false;
            }

        }

        if(distincVehicle.size() != arrayListVehicle.size()){
            Toast.makeText(HarvestApp.getContext(), "Ada Vehicle Yang Double Mohon Cek", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(distincPengirimanGandaSPK.size() != arrayListSpk.size()){
            Toast.makeText(HarvestApp.getContext(), "Ada Spk Yang Double Mohon Cek", Toast.LENGTH_SHORT).show();
            return false;
        }

        loaderSave = new HashMap<>();
        for(int i = 0 ; i < rvLoader.getAdapter().getItemCount();i++){
            RecyclerView.ViewHolder viewHolder = rvLoader.findViewHolderForAdapterPosition(i);
            if (viewHolder != null) {
                View v = viewHolder.itemView;
                CompleteTextViewHelper etLoader = v.findViewById(R.id.etLoader);
                TextView tvValue = v.findViewById(R.id.tvValue);

//                if(distincPengirimanGandaSPK.size() > 0){
                    if(!tvValue.getText().toString().isEmpty()){
                        Gson gson = new Gson();
                        Operator loader = gson.fromJson(tvValue.getText().toString(), Operator.class);
                        loaderSave.put(loader.getKodeOperator(), loader);
                        distincOperator.add(loader.getKodeOperator());
                        arrayListOperator.add(loader.getKodeOperator());
                    }
//                } else if(tvValue.getText().toString().isEmpty()){
//                    etLoader.requestFocus();
//                    Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Loader",Toast.LENGTH_SHORT).show();
//                    return false;
//                } else {
//                    Gson gson = new Gson();
//                    Operator loader = gson.fromJson(tvValue.getText().toString(), Operator.class);
//                    loaderSave.put(loader.getKodeOperator(), loader);
//                    distincOperator.add(loader.getKodeOperator());
//                    arrayListOperator.add(loader.getKodeOperator());
//                }
            } else if(i < loaders.size()){
                Operator loader = loaders.get(i);
//                if(distincPengirimanGandaSPK.size() > 0){
                    if(loader != null) {
                        distincOperator.add(loader.getKodeOperator());
                        arrayListOperator.add(loader.getKodeOperator());
                        loaderSave.put(loader.getKodeOperator(), loader);
                    }
//                }else if(loader == null) {
//                    Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Loader", Toast.LENGTH_SHORT).show();
//                    return false;
//                }else if (loader.getKodeOperator().isEmpty()) {
//                    Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Loader", Toast.LENGTH_SHORT).show();
//                    return false;
//                }
            }
        }

        if(loaderSave.size() != rvLoader.getAdapter().getItemCount()){
//            if(distincPengirimanGandaSPK.size() > 0){
                if(loaders.size() > 1){
                    Toast.makeText(HarvestApp.getContext(), "Tidak Boleh Ada Loader Yang Sama Dalam Info Loader", Toast.LENGTH_SHORT).show();
                    return false;
                }
//            }else if(loaders.size() > 1){
//                Toast.makeText(HarvestApp.getContext(), "Tidak Boleh Ada Loader Yang Sama Dalam Info Loader", Toast.LENGTH_SHORT).show();
//                return false;
//            }
        }

        if(distincOperator.size() != arrayListOperator.size()){
            Toast.makeText(HarvestApp.getContext(), "Tidak Boleh Ada Operator Yang Double", Toast.LENGTH_SHORT).show();
            return false;
        }

//        if(cbBin.isChecked() && etBin.getText().toString().isEmpty()){
//            etBin.requestFocus();
//            Toast.makeText(HarvestApp.getContext(),"Mohon Bin Disi",Toast.LENGTH_SHORT).show();
//            return false;
//        }

        if(ALselectedImage.size() > 0){
            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
            String stringIsi = preferences.getString(HarvestApp.STAMP_IMAGES,null);
            Gson gson  = new Gson();
            StampImage stampImage = gson.fromJson(stringIsi,StampImage.class);

            if (lsTujuan.getText().toString().equalsIgnoreCase(TransaksiAngkut.LANGSIR)) {
                if(stampImage.getLangsiran() == null){
                    Toast.makeText(HarvestApp.getContext(), "Langsiran yang Di pilih dan Watermark Berbeda Harap Foto Kembali", Toast.LENGTH_LONG).show();
                    takePicture();
                    return false;
                }else if(!stampImage.getLangsiran().getIdTujuan().equalsIgnoreCase(selectedLangsiran.getIdTujuan())) {
                    Toast.makeText(HarvestApp.getContext(), "Langsiran yang Di pilih dan Watermark Berbeda Harap Foto Kembali", Toast.LENGTH_LONG).show();
                    takePicture();
                    return false;
                }
            } else if (lsTujuan.getText().toString().equalsIgnoreCase(TransaksiAngkut.PKS)) {
                if(stampImage.getPks() == null){
                    Toast.makeText(HarvestApp.getContext(), "PKS yang Di pilih dan Watermark Berbeda Harap Foto Kembali", Toast.LENGTH_LONG).show();
                    takePicture();
                    return false;
                }else if(!stampImage.getPks().getIdPKS().equalsIgnoreCase(selectedPks.getIdPKS())) {
                    Toast.makeText(HarvestApp.getContext(), "PKS yang Di pilih dan Watermark Berbeda Harap Foto Kembali", Toast.LENGTH_LONG).show();
                    takePicture();
                    return false;
                }
            } else if (lsTujuan.getText().toString().equalsIgnoreCase(TransaksiAngkut.KELAUR)) {
                if(stampImage.getPks() != null && stampImage.getLangsiran() != null){
                    Toast.makeText(HarvestApp.getContext(), "Tujuan yang Di pilih dan Watermark Berbeda Harap Foto Kembali", Toast.LENGTH_LONG).show();
                    takePicture();
                    return false;
                }
            }
        }

        return  true;
    }

    private void saveDataDeklarasiAngkut(){
        String bin = null;
//        if(cbBin.isChecked()){
//            bin = etBin.getText().toString();
//        }

        switch (lsTujuan.getText().toString().toUpperCase()){
            case TransaksiAngkut.LANGSIR:
                tujuanAngkut = TransaksiAngkut.TUJUAN_LANGSIR;
                selectedPks = null;
                break;
            case TransaksiAngkut.PKS:
                tujuanAngkut = TransaksiAngkut.TUJUAN_PKS;
                selectedLangsiran = null;
                break;
            case TransaksiAngkut.KELAUR:
                tujuanAngkut = TransaksiAngkut.TUJUAN_KELUAR;
                selectedLangsiran = null;
                selectedPks = null;
                break;
        }

        switch (lsKendaraan.getText().toString().toUpperCase()){
            case TransaksiAngkut.KEBUN:
                kendaraanDari = TransaksiAngkut.KENDARAAN_KEBUN;
                break;
            case TransaksiAngkut.LUAR:
                kendaraanDari = TransaksiAngkut.KENDARAAN_LUAR;
                break;
        }

        HashSet<String> sFoto = new HashSet<>();
        if(ALselectedImage.size() > 0){
            for(File file : ALselectedImage){
                sFoto.add(file.getAbsolutePath());
            }
        }

        loaders = new ArrayList<>(loaderSave.values());
        pengirimanGandas = null;
        if(pengirimanGandaSave != null) {
            if(pengirimanGandaSave.values().size() > 0) {
                pengirimanGandas = new ArrayList<>(pengirimanGandaSave.values());
                Collections.sort(pengirimanGandas, new Comparator<PengirimanGanda>() {
                    @Override
                    public int compare(PengirimanGanda o1, PengirimanGanda o2) {
                        return o1.getIdx() < o2.getIdx() ? -1 : (o1.getIdx() > o2.getIdx()) ? 1 : 0;
                    }
                });
            }
        }

        ArrayList<Angkut> angkuts = new ArrayList<>();
        angkuts.addAll(angkutActivity.originAngkut.values());

        TransaksiAngkut transaksiAngkut = new TransaksiAngkut(
                headerTransaksiAngkut.getIdTAngkut(),
                GlobalHelper.getUser().getUserID(),
                headerTransaksiAngkut.getStartDate(),
                System.currentTimeMillis(),
                tJanjang,
                tBrondolan,
                tBeratEstimasi,
                latitude,
                longitude,
                GlobalHelper.getEstate().getEstCode(),
                kendaraanDari,
                selectedOperator,
                vehicleUse,
                tujuanAngkut,
                selectedPks,
                selectedLangsiran,
                spkUse,
                bin,
                TransaksiAngkut.TAP,
                angkuts,
                loaders,
                cages,
                pengirimanGandas,
                sFoto);

        transaksiAngkut.setIdSPB(idSpb);

        if(transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_PKS){
            transaksiAngkut.setIdSPB(idSpb);
            transaksiAngkut.setApproveBy(approveBy.getUserID());
        }else if (transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_KELUAR) {
            transaksiAngkut.setIdSPB(idSpb);
            transaksiAngkut.setApproveBy(approveBy.getUserID());
            if (angkutActivity.myTag != null) {
                transaksiAngkut.setIdNfc(GlobalHelper.bytesToHexString(angkutActivity.myTag.getId()));
            }
        }

        if(substituteSelected != null) {
            transaksiAngkut.setSubstitute(substituteSelected);
        }
        transaksiAngkut.setSubTotalAngkuts(TransaksiAngkutHelper.getSubTotalAngkut(transaksiAngkut.getAngkuts()));

//            if(selectedTransaksiAngkut != null){
//                ArrayList<TranskasiAngkutVersion> transkasiAngkutVersions = new ArrayList<>();
//                if(selectedTransaksiAngkut.getTranskasiAngkutVersions() != null) {
//                    transkasiAngkutVersions = selectedTransaksiAngkut.getTranskasiAngkutVersions();
//                    selectedTransaksiAngkut.setTranskasiAngkutVersions(null);
//                }
//                transkasiAngkutVersions.add(new TranskasiAngkutVersion(transkasiAngkutVersions.size() +  1 , selectedTransaksiAngkut));
//                transaksiAngkut.setTranskasiAngkutVersions(transkasiAngkutVersions);
//            }
        //tambahan zendi, check transaksi panen bila terdapat data angkut manual
        if(transaksiAngkut.getAngkuts()!=null){
            for(Angkut dataAngkut : transaksiAngkut.getAngkuts()){
                if(dataAngkut.getManualSPBData()!=null){
                    transaksiAngkut.setManual(true);
                    break;
                }
            }
        }

        Gson gson = new Gson();
        String sTAngkut =  gson.toJson(transaksiAngkut);

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        DataNitrit dataNitrit = new DataNitrit(transaksiAngkut.getIdTAngkut(), sTAngkut, ""+transaksiAngkut.isManual());
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", dataNitrit.getIdDataNitrit()));
        if(cursor.size() > 0){
            repository.update(dataNitrit);
        }else{
            repository.insert(dataNitrit);
            GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT);
        }
        db.close();

        TransaksiAngkutHelper.addIdTPanenHasBeenTap(transaksiAngkut);
        TransaksiAngkutHelper.hapusFileAngkut(true);

        //udh nga di pakai lagi
        //update ke transaksiangkutversion agar tidak bisa di select untuk tap kartu lagi
        //update history angkut ke table tangkutnya jika angkut sebagian
//            db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
//            repository = db.getRepository(DataNitrit.class);
//            for (int i = 0 ; i < transaksiAngkut.getAngkuts().size();i++){
//                //cek apakah angkutnya ada yang dari transaksi angkut
//                //jika ada maka transaksi angkut tsb harus di update transaksiangkutversion
//                //isi atribut nextIdTransaksiAngkut = idTranskasiAngkut saat ini
//                if(transaksiAngkut.getAngkuts().get(i).getTransaksiAngkut() != null){
//                    TransaksiAngkut tAngkut = transaksiAngkut.getAngkuts().get(i).getTransaksiAngkut();
//                    Cursor<DataNitrit> cursorX = repository.find(ObjectFilters.eq("idDataNitrit", tAngkut.getIdTAngkut()));
//                    if(cursorX.size() > 0) {
//                        TransaksiAngkut tAngkutSelected= null;
//                        for (Iterator iteratorX = cursorX.iterator(); iteratorX.hasNext(); ) {
//                            DataNitrit dataNitritX = (DataNitrit) iteratorX.next();
//                            tAngkutSelected = gson.fromJson(dataNitritX.getValueDataNitrit(),TransaksiAngkut.class);
//                            ArrayList<TranskasiAngkutVersion> transkasiAngkutVersions = new ArrayList<>();
//                            if(tAngkutSelected.getTranskasiAngkutVersions() != null) {
//                                for (int j = 0; j < tAngkutSelected.getTranskasiAngkutVersions().size(); j++) {
//                                    TranskasiAngkutVersion angkutVersion = tAngkutSelected.getTranskasiAngkutVersions().get(j);
//                                    if (angkutVersion.getNextIdTransaksi() == null) {
//                                        angkutVersion.setNextIdTransaksi(transaksiAngkut.getIdTAngkut());
//                                    }
//                                    transkasiAngkutVersions.add(angkutVersion);
//                                }
//                                tAngkutSelected.setTranskasiAngkutVersions(transkasiAngkutVersions);
//                            }
//                            break;
//                        }
//                        if(tAngkutSelected != null){
//                            DataNitrit dataNitritX = new DataNitrit(tAngkutSelected.getIdTAngkut(), gson.toJson(tAngkutSelected), ""+tAngkutSelected.isManual());
//                            repository.update(dataNitritX);
//                        }
//                    }
//                }
//            }
//            db.close();

        selectedTransaksiAngkut = gson.fromJson(sTAngkut,TransaksiAngkut.class);
        if(selectedTransaksiSPB != null){
            db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_SPB);
            repository = db.getRepository(DataNitrit.class);
            cursor = repository.find(ObjectFilters.eq("idDataNitrit", selectedTransaksiSPB.getIdSPB()));
            if(cursor.size() > 0){
                for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                    dataNitrit = (DataNitrit) iterator.next();
                    repository.remove(dataNitrit);
                    break;
                }
            }
            db.close();
            selectedTransaksiSPB = null;
        }

        if(selectedTransaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_PKS || selectedTransaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_KELUAR){
            //langsung create SPB
            TransaksiSPB transaksiSPB = new TransaksiSPB(idSpb,
                    tvSpbID.getText().toString(),
                    GlobalHelper.getUser().getUserID(),
                    approveBy.getUserID(),
                    GlobalHelper.getEstate().getEstCode(),
                    System.currentTimeMillis(),
                    selectedTransaksiAngkut,
                    latitude,
                    longitude,
                    selectedTransaksiAngkut.getTotalJanjang(),
                    selectedTransaksiAngkut.getTotalBrondolan(),
                    TransaksiSPB.BELUM_UPLOAD,
                    distinctAfdeling,
                    sFoto
            );

            if(substituteSelected != null){
                transaksiSPB.setSubstitute(substituteSelected);
            }

            db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_SPB);
            repository = db.getRepository(DataNitrit.class);
            gson = new Gson();
            dataNitrit = new DataNitrit(transaksiSPB.getIdSPB(),gson.toJson(transaksiSPB));
            cursor = repository.find(ObjectFilters.eq("idDataNitrit", dataNitrit.getIdDataNitrit()));
            if(cursor.size() > 0){
                repository.update(dataNitrit);
            }else{
                repository.insert(dataNitrit);
                if(Integer.parseInt(idSpb.substring(1,5)) >= GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_SPB)) {
                    GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_SPB);
                }
            }
            db.close();
            selectedTransaksiSPB = transaksiSPB;
        }

    }

    private void updateDataRv(int status){

        tJanjang = 0;
        tBrondolan = 0;
        tBeratEstimasi = 0;
        angkutActivity.originAngkut = new HashMap<>();
        angkutActivity.selectedAngkut = null;
        angkutActivity.selectedTransaksiPanen = null;
        angkutActivity.originRekonsilasi = new HashMap<>();
        distinctAfdeling = new HashSet<>();
//        angkutActivity.angkutLangsiran = new ArrayList<>();

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            Angkut angkut = gson.fromJson(dataNitrit.getValueDataNitrit(),Angkut.class);
//            if(angkut.getLangsiran() != null){
//                angkutActivity.angkutLangsiran.add(angkut.getLangsiran());
//            }else if (angkut.getTransaksiAngkut() != null) {
//                if (angkut.getTransaksiAngkut().getLangsiran() != null) {
//                    angkutActivity.angkutLangsiran.add(angkut.getTransaksiAngkut().getLangsiran());
//                }
//            }
            if (status == STATUS_LONG_OPERATION_SEARCH) {
                String tipeAngkut = "Manual";
                if(angkut.getTransaksiPanen() != null){
                    tipeAngkut = angkut.getTransaksiPanen().getTph().getNamaTph();
                }else if (angkut.getTransaksiAngkut() != null){
                    tipeAngkut = angkut.getTransaksiAngkut().getIdTAngkut();
                }
                if (sdfTime.format(angkut.getWaktuAngkut()).contains(etSearch.getText().toString().toLowerCase()) ||
                        tipeAngkut.toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                        String.valueOf(angkut.getJanjang()).contains(etSearch.getText().toString().toLowerCase()) ||
                        String.valueOf(angkut.getBrondolan()).contains(etSearch.getText().toString().toLowerCase())) {
                    addRowTableAngkut(angkut);
                    //tambahan zendi
                    if(angkut.getManualSPBData()!=null){
                        addRowTableRekonsiliasi(angkut);
                    }
                }
            } else if (status == STATUS_LONG_OPERATION_NONE) {
                addRowTableAngkut(angkut);
                //tambahan zendi
                if(angkut.getManualSPBData()!=null){
                    addRowTableRekonsiliasi(angkut);
                }
            }
        }
        db.close();


        angkutView = new HashMap<>();
        if(angkutActivity.originAngkut.size() > 0){
            angkutView = transaksiAngkutHelper.setDataView(new ArrayList<Angkut>(angkutActivity.originAngkut.values()));
            headerTransaksiAngkut.setAngkuts(new ArrayList<Angkut>(angkutActivity.originAngkut.values()));
            updateHeaderTransaksiAngkut();
        }else if(angkutActivity.originRekonsilasi.size() > 0){
            angkutView = transaksiAngkutHelper.setDataView(new ArrayList<Angkut>(angkutActivity.originRekonsilasi.values()));
        }
    }

    private void addRowTableAngkut(Angkut angkut){
        tJanjang += angkut.getJanjang();
        tBrondolan += angkut.getBrondolan();
        tBeratEstimasi += angkut.getBerat();
        angkutActivity.originAngkut.put(angkut.getIdAngkut(),angkut);
        distinctAfdeling.add(transaksiSpbHelper.getAfdeling(angkut));
    }

    private void addRowTableRekonsiliasi(Angkut angkut){
        if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
            tKartuRusak++;
        }else if(angkut.getManualSPBData().getTipe().equals("Kartu Hilang")){
            tKartuHilang++;
        }

        angkutActivity.originRekonsilasi.put(angkut.getIdAngkut(),angkut);
    }



    public void synTotalData(){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            Angkut angkut = gson.fromJson(dataNitrit.getValueDataNitrit(),Angkut.class);
            if(angkut.getIdAngkutRef().equalsIgnoreCase(angkutActivity.selectedAngkut.getIdAngkutRef())){
                angkutActivity.originAngkut.remove(angkut.getIdAngkut());
                angkutActivity.sigmaAngkut.setTotalJanjang(angkutActivity.sigmaAngkut.getTotalJanjang() - angkut.getJanjang());
                angkutActivity.sigmaAngkut.setTotalBrondolan(angkutActivity.sigmaAngkut.getTotalBrondolan() - angkut.getBrondolan());
                angkutActivity.sigmaAngkut.setTotalBeratEstimasi(angkutActivity.sigmaAngkut.getTotalBeratEstimasi() - angkut.getBerat());
            }
        }
        db.close();
    }

    private void updateUIRV(int status){
        if(angkutView.size() > 0  ) {

            if(isRekonsilasi){
                mRekonsilasiTableViewModel = new RekonsilasiTableViewModel(getContext(), angkutView);
                rekonsilasiAdapter = new RekonsilasiAdapter(getContext(),mRekonsilasiTableViewModel);
                mTableView.setAdapter(rekonsilasiAdapter);
            }else{
                mTableViewModel = new AngkutTableViewModel(getContext(), angkutView);
                adapter = new AngkutAdapter(getContext(),mTableViewModel);
                mTableView.setAdapter(adapter);
            }

            mTableView.setTableViewListener(new ITableViewListener() {
                @Override
                public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellDoubleClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {
                        // Create Long Press Popup
                        ColumnHeaderPopup popup = new ColumnHeaderPopup(
                                (ColumnHeaderViewHolder) columnHeaderView, mTableView);
                        // Show
                        popup.show();
                    }
                }

                @Override
                public void onColumnHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    if(bisaEdit){
                        if (rowHeaderView != null && rowHeaderView instanceof AngkutRowHeaderViewHolder) {
                            String[] sid = ((AngkutRowHeaderViewHolder) rowHeaderView).getCellId().split("-");
                            if (sid[2].equalsIgnoreCase("angkut") || sid[2].equalsIgnoreCase("manual") || sid[2].equalsIgnoreCase("transit")) {
                                Angkut value = angkutActivity.originAngkut.get(sid[1]);
                                angkutActivity.selectedAngkut = value;
                                if (value.getTransaksiAngkut() != null) {
                                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_UPDATE_TOTAL));
                                }else{
                                    angkutActivity.selectedTransaksiPanen = value.getTransaksiPanen();
                                    getActivity().getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.content_container, TphInputFragment.getInstance())
                                            .commit();
                                }

                            }
                        }

                        if(rowHeaderView!=null && rowHeaderView instanceof RekonsilasiRowHeaderViewHolder){
                            String[] sid = ((AngkutRowHeaderViewHolder) rowHeaderView).getCellId().split("-");
                            if (sid[2].equalsIgnoreCase("angkut") || sid[2].equalsIgnoreCase("manual") || sid[2].equalsIgnoreCase("transit")) {
                                Angkut value = angkutActivity.originAngkut.get(sid[1]);
                                angkutActivity.selectedAngkut = value;
                                if (value.getTransaksiPanen() != null) {
                                    angkutActivity.selectedTransaksiPanen = value.getTransaksiPanen();
                                } else if (value.getTransaksiAngkut() != null) {
                                    angkutActivity.transaksiAngkutFromNFC = value.getTransaksiAngkut();
                                }

//                                getActivity().getSupportFragmentManager().beginTransaction()
//                                        .replace(R.id.content_container, TphInputFragment.getInstance())
////                                        .addToBackStack(null)
//                                        .commit();
                            }
                        }
                    }else{
                        return;
                    }
                }

                @Override
                public void onRowHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }

                @Override
                public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }
            });
            if(isRekonsilasi){
                rekonsilasiAdapter.setAllItems(mRekonsilasiTableViewModel.getColumnHeaderList(),
                                                mRekonsilasiTableViewModel.getRowHeaderList(),
                                                mRekonsilasiTableViewModel.getCellList());
            }else{
                adapter.setAllItems(mTableViewModel.getColumnHeaderList(),
                                    mTableViewModel.getRowHeaderList(),
                                    mTableViewModel.getCellList());
            }

        }

        if(isRekonsilasi){
            tvJanjangTitle.setText(HarvestApp.getContext().getResources().getString(R.string.total_kartu_hilang));
            tvBrondolanTitle.setText(HarvestApp.getContext().getResources().getString(R.string.total_kartu_rusak));

            tvTotalOph.setText(String.valueOf(angkutActivity.originAngkut.size()));
            tvTotalTph.setText(String.valueOf(angkutView.size()));
            tvTotalJanjang.setText(String.valueOf(tKartuHilang));
            tvTotalBrondolan.setText(String.valueOf(tKartuRusak));
//            tvTotalBeratEstimasi.setText(String.format("%.0f",tBeratEstimasi) + "Kg");
            lTotalBerat.setVisibility(View.GONE);
        }else{
            tvTotalOph.setText(String.valueOf(angkutActivity.originAngkut.size()));
            tvTotalTph.setText(String.valueOf(angkutView.size()));
            tvTotalJanjang.setText(String.format("%,d",tJanjang) + "Jjg");
            tvTotalBrondolan.setText(String.format("%,d",tBrondolan) + "Kg");
            tvTotalBeratEstimasi.setText(String.format("%.0f",tBeratEstimasi) + "Kg");

            angkutActivity.sigmaAngkut = new TransaksiAngkut(tJanjang,tBrondolan,tBeratEstimasi,new ArrayList<>(angkutView.values()));
        }

    }

    public void showDialogCloseTransaksiAngkut(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.metode_penyimpanan_transaksi,null);
        LinearLayout lnfc = view.findViewById(R.id.lnfc);
        LinearLayout lprint = view.findViewById(R.id.lprint);
        LinearLayout lshare = view.findViewById(R.id.lshare);
        Button btnClose = view.findViewById(R.id.btnClose);

        if(lsTujuan.getText().toString().equals(TransaksiAngkut.PKS)){
            lnfc.setVisibility(View.GONE);
            if(etApprove.getText().toString().isEmpty()) {
                etApprove.requestFocus();
                Toast.makeText(HarvestApp.getContext(),"Mohon Input "+ getActivity().getResources().getString(R.string.approveby),Toast.LENGTH_SHORT).show();
                return;
            }
        }else if (lsTujuan.getText().toString().equals(TransaksiAngkut.KELAUR)){
            lnfc.setVisibility(View.GONE);
            if(etApproveKeluar.getText().toString().isEmpty()) {
                etApproveKeluar.requestFocus();
                Toast.makeText(HarvestApp.getContext(),"Mohon Input "+ getActivity().getResources().getString(R.string.approveby),Toast.LENGTH_SHORT).show();
                return;
            }
        }

        angkutActivity.closeTransaksi = true;
        File file = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
        if(file.exists()) {
            if(angkutActivity.selectedTransaksiAngkut == null){
                btnClose.setVisibility(View.GONE);
            }else{
                btnClose.setVisibility(View.VISIBLE);
            }
        }else{
            btnClose.setVisibility(View.VISIBLE);
        }

        lnfc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(!fileAngkutSelected.exists() && selectedTransaksiAngkut.getAngkuts().size() > 0 && selectedTransaksiAngkut.getStatus() == TransaksiAngkut.UPLOAD){
                    //transaksiAngkutHelper.tapNFC(selectedTransaksiAngkut);
                    NfcHelper.showNFCTap(getActivity(),STATUS_LONG_OPERATION_TAP_NFC_ONLY);
                }else {
                    if (validasiDeklarasiAngkut()) {
                        NfcHelper.showNFCTap(getActivity(),STATUS_LONG_OPERATION_SAVE_DATA_TAP_NFC);
                        //new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SAVE_DATA_TAP_NFC));
                    }else{
                        alertDialog.dismiss();
                    }
                }
            }
        });

        lprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((BaseActivity) getActivity()).bluetoothHelper.mService.getState() == BluetoothService.STATE_CONNECTED) {
                    Log.i("isRekonsilasi", "onClick: here or not");

                    File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                    if(!fileAngkutSelected.exists() && selectedTransaksiAngkut.getAngkuts().size() > 0 && selectedTransaksiAngkut.getStatus() == TransaksiAngkut.UPLOAD){

                        if(selectedTransaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR) {
                            if(isRekonsilasi){
//                                Log.i("we'reHere", "isRekonsilasi: Transaksi Angkut"+isRekonsilasi);
                                Log.i("we'reHere", "isRekonsilasi: "+isRekonsilasi);
                                transaksiAngkutHelper.printRekonsilasi(selectedTransaksiAngkut, tKartuRusak, tKartuHilang,new ArrayList<>(angkutView.values()));
                                return ;
                            }
                            transaksiAngkutHelper.printTransaksiAngkut(selectedTransaksiAngkut,new ArrayList<>(angkutView.values()));
                        }else{
                            if(selectedTransaksiSPB.getAfdeling() == null){
                                selectedTransaksiSPB.setAfdeling(distinctAfdeling);
                            }
                            if(isRekonsilasi){
                                Log.i("we'reHere", "isRekonsilasi: Transaksi SPB"+isRekonsilasi);
                                transaksiSpbHelper.printRekonsilasiSPB(selectedTransaksiSPB, tKartuRusak, tKartuHilang,new ArrayList<>(angkutView.values()));
                                return ;
                            }
                            transaksiSpbHelper.printTransaksiSPB(selectedTransaksiSPB,new ArrayList<>(angkutView.values()),true,null);
                        }
                    }else {
                        if (validasiDeklarasiAngkut()) {
                            if(isRekonsilasi){
                                if(selectedTransaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR) {
                                    transaksiAngkutHelper.printRekonsilasi(selectedTransaksiAngkut, tKartuRusak, tKartuHilang,new ArrayList<>(angkutView.values()));
                                }else{
                                    if(selectedTransaksiSPB.getAfdeling() == null){
                                        selectedTransaksiSPB.setAfdeling(distinctAfdeling);
                                    }
                                    transaksiSpbHelper.printRekonsilasiSPB(selectedTransaksiSPB, tKartuRusak, tKartuHilang,new ArrayList<>(angkutView.values()));
                                }
                                return ;
                            }
                            new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SAVE_DATA_PRINT));
                        }else{
                            alertDialog.dismiss();
                        }
                    }
                }else{
                    ((BaseActivity) getActivity()).bluetoothHelper.showListBluetoothPrinter();
                }
            }
        });

        lshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //jika data tidak ada file angkut selected berarti
                //data sudah di hapus karena ini data yang sebelumnya sudah pernah di create
                if(isRekonsilasi){
                    transaksiAngkutHelper.showQrRekonsilasi(selectedTransaksiAngkut, angkutActivity.qrHelper,new ArrayList<>(angkutView.values()));
                 return ;
                }

                File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
                if(!fileAngkutSelected.exists() && selectedTransaksiAngkut.getAngkuts().size() > 0 && selectedTransaksiAngkut.getStatus() == TransaksiAngkut.UPLOAD){
                    if (selectedTransaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR) {

                        transaksiAngkutHelper.showQr(selectedTransaksiAngkut, angkutActivity.qrHelper,new ArrayList<>(angkutView.values()));
                    } else {
                        if(selectedTransaksiSPB.getAfdeling() == null){
                            selectedTransaksiSPB.setAfdeling(distinctAfdeling);
                        }
                        transaksiSpbHelper.showQr(selectedTransaksiSPB, angkutActivity.qrHelper,new ArrayList<>(angkutView.values()),true,null);
                    }
                }else {
                    if (validasiDeklarasiAngkut()) {
                        new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SAVE_DATA_SHARE));
                    }else{
                        alertDialog.dismiss();
                    }
                }
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angkutActivity.closeActivity();
            }
        });

        alertDialog = WidgetHelper.showListReference(alertDialog,view,getActivity());
    }

    public void nextNfc(){
        NfcHelper.stat = STATUS_LONG_OPERATION_TAP_NFC;
    }

    public void setLangsiran(Langsiran langsiran){
        lLangsiran.setVisibility(View.VISIBLE);
        lPks.setVisibility(View.GONE);
        lKeluar.setVisibility(View.GONE);
        if(langsiran != null) {
            tvLangsiran.setText(langsiran.getNamaTujuan());
            selectedLangsiran = langsiran;
        }else{
            angkutActivity.searchingGPS = Snackbar.make(angkutActivity.myCoordinatorLayout, getResources().getString(R.string.langsiran_not_found), Snackbar.LENGTH_INDEFINITE);
            angkutActivity.searchingGPS.show();
        }

        updateHeaderTransaksiAngkut();
    }

    public void setPks(PKS pks){
        lLangsiran.setVisibility(View.GONE);
        lPks.setVisibility(View.VISIBLE);
        lKeluar.setVisibility(View.GONE);
        if(pks != null) {
            tvPks.setText(pks.getNamaPKS());
            selectedPks = pks;
        }else{
            tvPks.setText("");
            selectedPks = new PKS();
        }
        updateHeaderTransaksiAngkut();
    }

    public void setOperator(Operator operator){
        if(operator != null) {
            if(lsKendaraan.getText().toString().equalsIgnoreCase(TransaksiAngkut.KEBUN)) {
                etDriver.setText(operator.getNama());
            }
            selectedOperator = operator;
        }else{
            etDriver.setText("");
            selectedOperator = new Operator();
        }
        updateHeaderTransaksiAngkut();
    }

    public void setVehicle(Vehicle vehicle){
        if(vehicle !=null) {
            if(lsKendaraan.getText().toString().equalsIgnoreCase(TransaksiAngkut.KEBUN)) {
                etVehicle.setText(TransaksiAngkutHelper.showVehicle(vehicle));
            }
            vehicleUse = vehicle;
        }else{
            etVehicle.setText("");
            vehicleUse = new Vehicle();
        }
        updateHeaderTransaksiAngkut();
    }

    public void setSpk(SPK spk){
        if(spk != null){
            etSpk.setText(spk.getIdSPK());
            //lSPK.setVisibility(View.VISIBLE);
        }else{
            etSpk.setText("");
        }
        spkUse = spk;
        updateHeaderTransaksiAngkut();
    }

    public void updateHeaderTransaksiAngkut(){
        if(headerTransaksiAngkut != null){
            if (selectedOperator != null) {
                headerTransaksiAngkut.setSupir(selectedOperator);
            }

            if (vehicleUse != null) {
                headerTransaksiAngkut.setVehicle(vehicleUse);
            }

            if(spkUse != null){
                headerTransaksiAngkut.setSpk(spkUse);
            }

            if(approveBy != null) {
                headerTransaksiAngkut.setApproveBy(approveBy.getUserID());
            }

            headerTransaksiAngkut.setSubstitute(substituteSelected);
            headerTransaksiAngkut.setPks(selectedPks);
            headerTransaksiAngkut.setLangsiran(selectedLangsiran);

            TransaksiAngkutHelper.createNewAngkut(headerTransaksiAngkut);
        }
    }

    public void startLongOperation(int longOperation){
        new LongOperation().execute(String.valueOf(longOperation));
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        AlertDialog alertDialogAllpoin;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(getActivity(),getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])){
                case STATUS_LONG_OPERATION_NONE:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SAVE_DATA_SHARE:{
                    saveDataDeklarasiAngkut();
                    break;
                }
                case STATUS_LONG_OPERATION_SAVE_DATA_PRINT:{
                    saveDataDeklarasiAngkut();
                    break;
                }
                case STATUS_LONG_OPERATION_SAVE_DATA_TAP_NFC:{
                    saveDataDeklarasiAngkut();
                    angkutActivity.batchKartuAngkut = transaksiAngkutHelper.setupBatchKartuAngkut(selectedTransaksiAngkut);
                    angkutActivity.idxTap = 0;
                    break;
                }
                case STATUS_LONG_OPERATION_TAP_NFC_ONLY:{
                    angkutActivity.batchKartuAngkut = transaksiAngkutHelper.setupBatchKartuAngkut(selectedTransaksiAngkut);
                    angkutActivity.idxTap = 0;
                    break;
                }
                case STATUS_LONG_OPERATION_POPUP_MAP:{
                    popupTPH = true;
                    break;
                }
                case STATUS_LONG_OPERATION_UPDATE_TOTAL:{
                    synTotalData();
                    break;
                }
            }
            return strings[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(popupTPH){
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        if(lsTujuan.getText().toString().equals(TransaksiAngkut.PKS)){
                            pksHelper.showMapPks();
                        }else if (lsTujuan.getText().toString().equals(TransaksiAngkut.LANGSIR)){
                            tujuanHelper.showMapTujuan();
                        }
                        popupTPH = false;
                        if (alertDialogAllpoin != null) {
                            if(alertDialogAllpoin.isShowing()) {
                                alertDialogAllpoin.dismiss();
                            }
                        }
                    }
                });
            }else {
                if (alertDialogAllpoin != null) {
                    if(alertDialogAllpoin.isShowing()) {
                        alertDialogAllpoin.dismiss();
                    }
                }
                switch (Integer.parseInt(result)) {
//                    STATUS_LONG_OPERATION_NONE
                    case STATUS_LONG_OPERATION_NONE: {
                        updateUIRV(Integer.parseInt(result));
                        break;
                    }
                    case STATUS_LONG_OPERATION_SEARCH: {
                        updateUIRV(Integer.parseInt(result));
                        break;
                    }
                    case STATUS_LONG_OPERATION_SAVE_DATA_SHARE:{
                        if(selectedTransaksiSPB != null) {
                            transaksiSpbHelper.showQr(selectedTransaksiSPB,angkutActivity.qrHelper,new ArrayList<>(angkutView.values()),true,null);
                        }else{
                            transaksiAngkutHelper.showQr(selectedTransaksiAngkut, angkutActivity.qrHelper,new ArrayList<>(angkutView.values()));
                        }
                        break;
                    }
                    case STATUS_LONG_OPERATION_SAVE_DATA_PRINT:{
                        if(selectedTransaksiSPB != null){
                            transaksiSpbHelper.printTransaksiSPB(selectedTransaksiSPB,new ArrayList<>(angkutView.values()),false,null);
                        }else {
                            transaksiAngkutHelper.printTransaksiAngkut(selectedTransaksiAngkut,new ArrayList<>(angkutView.values()));
                        }
                        break;
                    }
                    case STATUS_LONG_OPERATION_SAVE_DATA_TAP_NFC:{
                        WidgetHelper.showSnackBar(angkutActivity.myCoordinatorLayout,String.format("Tap [ %,d / %,d]",angkutActivity.idxTap,angkutActivity.batchKartuAngkut.size()));
                        transaksiAngkutHelper.tapNFC(angkutActivity.batchKartuAngkut.get(angkutActivity.idxTap));
                        break;
                    }
                    case STATUS_LONG_OPERATION_TAP_NFC_ONLY:{
                        WidgetHelper.showSnackBar(angkutActivity.myCoordinatorLayout,String.format("Tap [ %,d / %,d]",angkutActivity.idxTap,angkutActivity.batchKartuAngkut.size()));
                        transaksiAngkutHelper.tapNFC(angkutActivity.batchKartuAngkut.get(angkutActivity.idxTap));
                        break;
                    }
                    case STATUS_LONG_OPERATION_TAP_NFC:{
                        WidgetHelper.showSnackBar(angkutActivity.myCoordinatorLayout,String.format("Tap [ %,d / %,d]",angkutActivity.idxTap,angkutActivity.batchKartuAngkut.size()));
                        transaksiAngkutHelper.tapNFC(angkutActivity.batchKartuAngkut.get(angkutActivity.idxTap));
                        break;
                    }
                    case STATUS_LONG_OPERATION_UPDATE_TOTAL:{
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.content_container, LangsiranInputFragment.getInstance(1))
                                .commit();
                        break;
                    }
                }
            }
        }
    }
}
