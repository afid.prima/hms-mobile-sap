package id.co.ams_plantation.harvestsap.model;

/**
 * Created on : 26,November,2021
 * Author     : Afid
 */

public class InfoRestan {
    int jjgH2;
    int lamaRestan;

    public int getJjgH2() {
        return jjgH2;
    }

    public void setJjgH2(int jjgH2) {
        this.jjgH2 = jjgH2;
    }

    public int getLamaRestan() {
        return lamaRestan;
    }

    public void setLamaRestan(int lamaRestan) {
        this.lamaRestan = lamaRestan;
    }
}
