package id.co.ams_plantation.harvestsap.model;

/**
 * Created on : 21,October,2022
 * Author     : Afid
 */

public class OpnameNFC {
    public static final int SAVE = 1;
    public static final int UPLOAD = 2;

    String uid;
    String estCode;
    String afdeling;
    String createBy;
    long createDate;
    String updateBy;
    long updateDate;
    String type;
    int status;

    public OpnameNFC(String uid, String estCode, String afdeling, String createBy, long createDate, String updateBy, long updateDate, String type, int status) {
        this.uid = uid;
        this.estCode = estCode;
        this.afdeling = afdeling;
        this.createBy = createBy;
        this.createDate = createDate;
        this.updateBy = updateBy;
        this.updateDate = updateDate;
        this.type = type;
        this.status = status;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
