package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.Cages;

public class CekCagesAdapter extends RecyclerView.Adapter<CekCagesAdapter.Holder>{

    Context context;
    ArrayList<Cages> originLists;
    ArrayList<Cages> filterLists;

    public CekCagesAdapter(Context context, ArrayList<Cages> originLists) {
        this.context = context;
        this.originLists = originLists;
        this.filterLists = originLists;
    }

    @NonNull
    @Override
    public CekCagesAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.row_record,viewGroup,false);
        return new CekCagesAdapter.Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CekCagesAdapter.Holder holder, int i) {
        holder.setValue(filterLists.get(i));
    }

    @Override
    public int getItemCount() {
        return filterLists.size();
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                filterLists = new ArrayList<>();
                String string = constraint.toString().toLowerCase();
                int i = 0 ;

                for (Cages Cages : originLists) {
                    try {
                        if (Cages.getAssetNo().toLowerCase().contains(string) ||
                                Cages.getAssetName().toLowerCase().contains(string)

                        ) {
                            filterLists.add(Cages);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                if (filterLists.size() > 0) {
                    results.count = filterLists.size();
                    results.values = filterLists;
                    return results;
                } else {
                    return null;
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null) {
                    filterLists = (ArrayList<Cages>) results.values;
                } else {
                    filterLists.clear();
                }
                notifyDataSetChanged();

            }
        };
    }

    public class Holder extends RecyclerView.ViewHolder {
        CardView card_view;
        LinearLayout lketerangan;
        RelativeLayout rlImage;
        TextView item_ket;
        TextView item_ket2;
        TextView item_ket1;
        TextView item_ket3;

        public Holder(View itemView) {
            super(itemView);

            lketerangan = itemView.findViewById(R.id.lketerangan);
            card_view = itemView.findViewById(R.id.card_view);
            rlImage = itemView.findViewById(R.id.rlImage);
            item_ket = itemView.findViewById(R.id.item_ket);
            item_ket1 = itemView.findViewById(R.id.item_ket1);
            item_ket2 = itemView.findViewById(R.id.item_ket2);
            item_ket3 = itemView.findViewById(R.id.item_ket3);

            rlImage.setVisibility(View.GONE);
            item_ket3.setVisibility(View.GONE);
        }

        public void setValue(Cages value) {
            try {

                item_ket.setText(value.getAssetNo());
                item_ket1.setText(value.getAssetName());
                item_ket2.setText(value.getGenerateId());

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
