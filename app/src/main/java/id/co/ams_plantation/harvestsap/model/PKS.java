package id.co.ams_plantation.harvestsap.model;

public class PKS {
    private String idPKS;
    private String namaPKS;
    private String estCode;
    private String afdeling;
    private String block;
    private Double latitude;
    private Double longitude;
    private Integer createBy;
    private Long createDate;
    private String customerCode;
    private String estNew;
    private Double landDistance;
    private Double waterDistance;

    public String getIdPKS() {
        return idPKS;
    }

    public void setIdPKS(String idPKS) {
        this.idPKS = idPKS;
    }

    public String getNamaPKS() {
        return namaPKS;
    }

    public void setNamaPKS(String namaPKS) {
        this.namaPKS = namaPKS;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getEstNew() {
        return estNew;
    }

    public void setEstNew(String estNew) {
        this.estNew = estNew;
    }

    public Double getLandDistance() {
        return landDistance;
    }

    public void setLandDistance(Double landDistance) {
        this.landDistance = landDistance;
    }

    public Double getWaterDistance() {
        return waterDistance;
    }

    public void setWaterDistance(Double waterDistance) {
        this.waterDistance = waterDistance;
    }
}
