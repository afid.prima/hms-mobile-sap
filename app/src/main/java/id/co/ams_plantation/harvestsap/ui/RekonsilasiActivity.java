package id.co.ams_plantation.harvestsap.ui;

import android.app.Activity;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.util.HashMap;

import butterknife.BindView;
import id.co.ams_plantation.harvestsap.Fragment.RekonsiliasiInputFragment;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.Estate;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.service.BluetoothService;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.NfcHelper;
import id.co.ams_plantation.harvestsap.util.QrHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;

public class RekonsilasiActivity extends BaseActivity {
    public Estate estate;
    public TransaksiAngkut selectedTransaksiAngkut;
    public Angkut selectedAngkut;
    public TransaksiPanen selectedTransaksiPanen;
//    public HashMap<String,Angkut> originAngkut;
    public HashMap<String, Angkut> originRekonsilasi;
    public HashMap<String, Angkut> originSearch;
    public boolean closeTransaksi;

    public QrHelper qrHelper;

    Activity activity;



//    public LocationManager mLocationManager;
//    public Location currentlocation;

    @BindView(R.id.myCoordinatorLayout)
    public CoordinatorLayout myCoordinatorLayout;

    public NfcAdapter nfcAdapter;
    public PendingIntent pendingIntent;
    public IntentFilter[] writeTagFilters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_layout);
        Intent intent = getIntent();
        if(intent.getExtras() != null) {
            String transaksi = intent.getExtras().getString("transaksiAngkut");
            if (transaksi != null) {
                TransaksiAngkutHelper.hapusFileAngkut(false);
                Gson gson = new Gson();
                selectedTransaksiAngkut = gson.fromJson(transaksi, TransaksiAngkut.class);
//                createNewAngkut(selectedTransaksiAngkut);

//                Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT);
//                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
//                for(int i = 0 ; i < selectedTransaksiAngkut.getAngkuts().size(); i++){
//                    DataNitrit dataNitrit = new DataNitrit(selectedTransaksiAngkut.getAngkuts().get(i).getIdAngkut(),
//                            gson.toJson(selectedTransaksiAngkut.getAngkuts().get(i)));
//                    repository.insert(dataNitrit);
//                }
//                db.close();
            }
        }
        qrHelper = new QrHelper(this);
        activity = this;
        String strEst =  Encrypts.decrypt(GlobalHelper.getFileContent(Encrypts.encrypt(GlobalHelper.SELECTED_ESTATE)));
        Gson gson = new Gson();
        estate = (Estate) gson.fromJson(strEst, Estate.class);
        toolBarSetup();
        main();

        closeTransaksi = false;

        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);

    }

    private void main() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_container, RekonsiliasiInputFragment.getInstance())
                .commit();
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        TextView textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        textToolbar.setText(getResources().getString(R.string.rekonsiliasi));

    }

    public void closeActivity(){
        setResult(GlobalHelper.RESULT_REKONSILIASI);
        finish();
    }

    @Override
    protected void initPresenter() {

    }

    @Override
    public void onStart() {
        super.onStart();

        // If Bluetooth is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!bluetoothHelper.mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, GlobalHelper.REQUEST_ENABLE_BT);
            // Otherwise, setup the session
        } else {
            if (bluetoothHelper.mService == null)
                bluetoothHelper.mService = new BluetoothService(this, bluetoothHelper.mHandler);
        }
    }



    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        NfcHelper.WriteModeOn(nfcAdapter,this,pendingIntent,writeTagFilters);
        if(bluetoothHelper != null) {
            if (bluetoothHelper.mService != null) {
                if (bluetoothHelper.mService.getState() == BluetoothService.STATE_NONE) {
                    // Start the Bluetooth services
                    bluetoothHelper.mService.start();
                }
            }
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        NfcHelper.WriteModeOff(nfcAdapter,this); //disable nfc read
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth services
        if (bluetoothHelper != null) {
            if (bluetoothHelper.mService != null)
                bluetoothHelper.mService.stop();
        }
    }
}
