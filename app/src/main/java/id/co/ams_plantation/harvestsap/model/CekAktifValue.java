package id.co.ams_plantation.harvestsap.model;

public class CekAktifValue {
    boolean aktifKembali;
    String message;

    public CekAktifValue(boolean aktifKembali, String message) {
        this.aktifKembali = aktifKembali;
        this.message = message;
    }

    public boolean isAktifKembali() {
        return aktifKembali;
    }

    public void setAktifKembali(boolean aktifKembali) {
        this.aktifKembali = aktifKembali;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
