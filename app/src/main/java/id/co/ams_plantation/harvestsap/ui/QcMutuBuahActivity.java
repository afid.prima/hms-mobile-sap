package id.co.ams_plantation.harvestsap.ui;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.util.Arrays;

import butterknife.BindView;
import id.co.ams_plantation.harvestsap.Fragment.QcMutuBuahFragment;
import id.co.ams_plantation.harvestsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.Estate;
import id.co.ams_plantation.harvestsap.model.QcMutuBuah;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.NfcHelper;
import id.co.ams_plantation.harvestsap.util.QrHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;

public class QcMutuBuahActivity extends BaseActivity {

    public Estate estate;
    public int TYPE_NFC;
    public String valueNFC;
    public QrHelper qrHelper;
    public QcMutuBuah qcMutuBuah;
    Activity activity;

    @BindView(R.id.myCoordinatorLayout)
    public CoordinatorLayout myCoordinatorLayout;

    public Snackbar searchingGPS;
    /****************************************** NFC ******************************************************************/
    public Tag myTag;
    public NfcAdapter nfcAdapter;
    public PendingIntent pendingIntent;
    public IntentFilter[] writeTagFilters;

    /****************************************************************************************************************/
    private static Handler handler;

    @Override
    protected void initPresenter() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_layout);
        getSupportActionBar().hide();

        qrHelper = new QrHelper(this);
        activity = this;

        String strEst =  Encrypts.decrypt(GlobalHelper.getFileContent(Encrypts.encrypt(GlobalHelper.SELECTED_ESTATE)));
        Gson gson = new Gson();
        estate = (Estate) gson.fromJson(strEst, Estate.class);

////        set di baseActivity
//        createLocationListener();

        toolBarSetup();

        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            String SqcMutuBuah = intent.getExtras().getString("qcMutuBuah");
            if (SqcMutuBuah != null) {
                qcMutuBuah = gson.fromJson(SqcMutuBuah, QcMutuBuah.class);
            }
        }

//        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
//        if (nfcAdapter == null) {
//            // Stop here, we definitely need NFC
//            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
//        }
//        NfcHelper.readFromIntent(getIntent());
//        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
//        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
//        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
//        writeTagFilters = new IntentFilter[] { tagDetected };
//        dataAllUser = GlobalHelper.getAllUser();
        main();
    }

//    public static Handler getHandler() {
//        return handler;
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        switch (resultCode){
//            case GlobalHelper.RESULT_SCAN_QR:{
//                String isiQr = data.getExtras().getString(QRScan.NILAI_QR_SCAN);
//                cekDataPassing(isiQr,this);
//                break;
//            }
//        }
//    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void main() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_container, TphInputFragment.getInstance())
                .commit();
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        TextView textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backProses();
            }
        });
        textToolbar.setText(getResources().getString(R.string.qc_mutu_buah));
    }

    public void backProses() {
        if (searchingGPS != null) {
            searchingGPS.dismiss();
        }
        setResult(GlobalHelper.RESULT_QC_MUTU_BUAH);
        finish();
    }

    private void createLocationListener() {
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

//        searchingGPS = Snackbar.make(myCoordinatorLayout,getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
//        searchingGPS.show();
        WidgetHelper.warningFindGps(this,myCoordinatorLayout);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                1f, mLocationListener);
    }

//    @Override
//    protected void onNewIntent(Intent intent) {
//        setIntent(intent);
//        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
//            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
//            Log.i("tagNFC", GlobalHelper.bytesToHexString(myTag.getId()));
//            Log.i("tagNFC", Arrays.toString(myTag.getTechList()));
//        }
//        valueNFC = NfcHelper.readFromIntent(intent);
//        int formatNFC = NfcHelper.cekFormatNFC(valueNFC);
//        if (valueNFC != null) {
//            if (formatNFC != GlobalHelper.TYPE_NFC_SALAH) {
//                if(valueNFC.length() == 5) {
//                    if(NfcHelper.stat != NfcHelper.NFC_TIDAK_BISA_WRITE){
//                        if (getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof TphInputFragment) {
//                            TphInputFragment fragment = (TphInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
//                            fragment.startLongOperation(NfcHelper.stat);
//                        }
//                    }else{
//                        Toast.makeText(this, getResources().getString(R.string.nfc_empty) + " " + GlobalHelper.LIST_NFC[formatNFC], Toast.LENGTH_SHORT).show();
//                    }
//                }else{
//                    cekDataPassing(valueNFC, this);
//                }
//                TYPE_NFC = formatNFC;
//            } else {
//                Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.format_not_valid) + " 999", Toast.LENGTH_SHORT).show();
//            }
//        }else {
//            Toast.makeText(this,R.string.format_not_valid,Toast.LENGTH_SHORT).show();
//        }
//    }

    @Override
    public void onBackPressed() {
        backProses();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        NfcHelper.WriteModeOn(nfcAdapter,this,pendingIntent,writeTagFilters);
    }

    @Override
    public void onPause(){
        super.onPause();
//        NfcHelper.WriteModeOff(nfcAdapter,this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
