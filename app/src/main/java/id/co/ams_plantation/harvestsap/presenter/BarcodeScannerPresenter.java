package id.co.ams_plantation.harvestsap.presenter;

import id.co.ams_plantation.harvestsap.presenter.base.BasePresenter;
import id.co.ams_plantation.harvestsap.view.BarcodeScannerView;

public class BarcodeScannerPresenter extends BasePresenter {

    public BarcodeScannerPresenter(BarcodeScannerView view){
        attachView(view);
    }

}