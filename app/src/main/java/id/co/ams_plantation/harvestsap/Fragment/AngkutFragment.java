package id.co.ams_plantation.harvestsap.Fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.mikepenz.iconics.utils.Utils;
import com.roughike.swipeselector.OnSwipeItemSelectedListener;
import com.roughike.swipeselector.SwipeItem;
import com.roughike.swipeselector.SwipeSelector;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.adapter.TAngkutAdapter;
import id.co.ams_plantation.harvestsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.holder.TAngkutRowHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestsap.TableView.popup.RowHeaderLongPressPopup;
import id.co.ams_plantation.harvestsap.TableView.view_model.TAngkutTableViewModel;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.model.TranskasiAngkutVersion;
import id.co.ams_plantation.harvestsap.ui.AngkutActivity;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestsap.ui.RekonsiliasiListActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.FilterHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.MainMenuHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;

/**
 * Created by user on 12/4/2018.
 */

public class AngkutFragment extends Fragment {
    public static final int STATUS_LONG_OPERATION_NONE = 0;
    public static final int STATUS_LONG_OPERATION_SEARCH = 1;
    public static final int STATUS_LONG_OPERATION_SELECT_ITEM = 2;
    public static final int STATUS_LONG_OPERATION_NEW_DATA = 3;
    public static final int STATUS_LONG_OPERATION_REKONSILIASI = 4;
    public static final int STATUS_LONG_OPERATION_SHOW_CONFIRM_DELETED = 5;
    public static final int STATUS_LONG_OPERATION_DELETED = 6;

    HashMap<String,TransaksiAngkut> origin;
    Set<String> listSearch;

    TAngkutTableViewModel mTableViewModel;
    TAngkutAdapter adapter;

    LinearLayout llNewData;
//    LinearLayout llRekonsiliasi;
    CompleteTextViewHelper etSearch;
    Button btnSearch;
    RelativeLayout ltableview;
    TableView mTableView;
    TextView tvStartTime;
    TextView tvFinishTime;
    TextView tvTrip;
    TextView tvUpload;
    TextView tvTotalJanjang;
    TextView tvTotalBrondolan;

    Date dateSelected;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

    Long mulai = Long.valueOf(0);
    Long sampai = Long.valueOf(0);
    int tJanjang = 0;
    int tBrondolan = 0;
    int upload= 0;
    int trip = 0;

    HashMap<Long,TransaksiAngkut> arrayLog;

    String idSelected;
    int posisi;

    TransaksiAngkut selectedTransaksiAngkut;
    boolean isVisibleToUser;
    MainMenuActivity mainMenuActivity;
    MainMenuHelper mainMenuHelper;

    public static AngkutFragment getInstance(){
        AngkutFragment fragment = new AngkutFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.angkut_layout,null,false);
        mainMenuActivity = (MainMenuActivity) getActivity();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        this.isVisibleToUser = isVisibleToUser;
        main();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SwipeSelector menuSwipeSelector = view.findViewById(R.id.menuSwipeSelector);
        llNewData = view.findViewById(R.id.llNewData);
//        llRekonsiliasi = view.findViewById(R.id.llRekonsiliasi);
        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        ltableview = view.findViewById(R.id.ltableview);
        mTableView = view.findViewById(R.id.tableview);
        tvStartTime = view.findViewById(R.id.tvStartTime);
        tvFinishTime = view.findViewById(R.id.tvFinishTime);
        tvTrip = view.findViewById(R.id.tvTrip);
        tvUpload = view.findViewById(R.id.tvUpload);
        tvTotalJanjang = view.findViewById(R.id.tvTotalJanjang);
        tvTotalBrondolan = view.findViewById(R.id.tvTotalBrondolan);

        dateSelected = new Date();
        origin = new HashMap<>();
        mainMenuHelper = new MainMenuHelper(mainMenuActivity);

        ArrayList<SwipeItem> swipeItems= new ArrayList<>();
        Long now = System.currentTimeMillis();
        for(int i = 0 ; i < GlobalHelper.MAX_LAST_DAY_DATA; i++){
            Long l = now - ( i * 24 * 60 * 60 * 1000);
            swipeItems.add(new SwipeItem(String.valueOf(i),sdf.format(l),""));
        }
        Collections.reverse(swipeItems);

        SwipeItem[] tmpStrSwipe = new SwipeItem[swipeItems.size()];
        tmpStrSwipe = swipeItems.toArray(tmpStrSwipe);
        menuSwipeSelector.setItems(tmpStrSwipe);

        menuSwipeSelector.selectItemAt(swipeItems.size() - 1);
        menuSwipeSelector.setOnItemSelectedListener(new OnSwipeItemSelectedListener() {
            @Override
            public void onItemSelected(SwipeItem item) {
                try {
                    Date date = sdf.parse(item.getTitle());
                    dateSelected = date;
                    startLongOperation(STATUS_LONG_OPERATION_NONE);
                    Log.d("item Date ", date.getTime() + " "+ sdf.format(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        llNewData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLongOperation(STATUS_LONG_OPERATION_NEW_DATA);
//                TransaksiAngkutHelper.createNewAngkut();
//                Intent intent = new Intent(getActivity(), AngkutActivity.class);
//                startActivityForResult(intent, GlobalHelper.RESULT_ANGKUTACTIVITY);
            }
        });

//        llRekonsiliasi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startLongOperation(STATUS_LONG_OPERATION_REKONSILIASI);
//            }
//        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLongOperation(STATUS_LONG_OPERATION_SEARCH);
            }
        });
        main();
        showNewTranskasi();
    }

    private void showNewTranskasi(){
//        if(GlobalHelper.enableToNewTransaksi()){
//            llNewData.setVisibility(View.VISIBLE);
//        }else{
//            llNewData.setVisibility(View.GONE);
//        }
    }

    private void main(){
        if (isVisibleToUser && getActivity() != null) {
            mainMenuActivity.ivLogo.setVisibility(View.GONE);
            mainMenuActivity.btnFilter.setVisibility(View.VISIBLE);
            mainMenuActivity.filterHelper.setFilterView(this);
            startLongOperation(STATUS_LONG_OPERATION_NONE);
        }
    }

    private void updateDataRv(int status){
        mulai = Long.valueOf(0);
        sampai = Long.valueOf(0);
        tJanjang = 0;
        tBrondolan = 0;
        upload= 0;
        trip = 0;
        origin = new HashMap<>();
        listSearch = new android.support.v4.util.ArraySet<>();

        String userId = GlobalHelper.getUser().getUserID();
        String filterAfdeling = mainMenuActivity.tvFilterAfdeling.getText().toString();
        String filterCreateBy = mainMenuActivity.tvFilterDataInputBy.getText().toString();

        arrayLog = new HashMap<>();
        String isiLog = GlobalHelper.readFileContent(GlobalHelper.getLogTransaksi(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT], sdf.format(dateSelected)));
        try {
            if(isiLog.isEmpty()) {
                JSONObject objIsiLog = new JSONObject(isiLog);
                Iterator<String> keys = objIsiLog.keys();

                while (keys.hasNext()) {
                    String key = keys.next();
                    if (objIsiLog.get(key) instanceof JSONObject) {
                        Gson gson = new Gson();
                        TransaksiAngkut transaksiAngkut = gson.fromJson(objIsiLog.toString(), TransaksiAngkut.class);
                        arrayLog.put(transaksiAngkut.getStartDate(), transaksiAngkut);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayList<String> allDb = GlobalHelper.getAllFileDB(GlobalHelper.TABLE_TRANSAKSI_ANGKUT,true);
        for(String dbFile : allDb) {
            Nitrite db = GlobalHelper.getTableNitritFullPath(dbFile);
            if (db != null) {
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
                for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                    DataNitrit dataNitrit = (DataNitrit) iterator.next();
                    Gson gson = new Gson();
                    TransaksiAngkut transaksi = gson.fromJson(dataNitrit.getValueDataNitrit(), TransaksiAngkut.class);
                    if (sdf.format(dateSelected).equals(sdf.format(new Date(transaksi.getStartDate())))) {

                        if (transaksi.getStatus() == TransaksiAngkut.TAP) {
                            arrayLog.put(transaksi.getStartDate(), transaksi);
                        }

                        if (transaksi.getStatus() == TransaksiAngkut.TAP || transaksi.getStatus() == TransaksiAngkut.UPLOAD || transaksi.getStatus() == TransaksiAngkut.SPB) {
                            TransaksiAngkut addTransaksi = null;
                            if (status == STATUS_LONG_OPERATION_SEARCH) {
                                if ((transaksi.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR) && (transaksi.getLangsiran().getNamaTujuan().toLowerCase().contains(etSearch.getText().toString().toLowerCase()))) {
                                    addTransaksi = transaksi;
                                } else if ((transaksi.getTujuan() == TransaksiAngkut.TUJUAN_PKS) && (transaksi.getPks().getNamaPKS().toLowerCase().contains(etSearch.getText().toString().toLowerCase()))) {
                                    addTransaksi = transaksi;
                                } else if ((transaksi.getTujuan() == TransaksiAngkut.TUJUAN_KELUAR) && TransaksiAngkut.KELAUR.contains(etSearch.getText().toString().toUpperCase())) {
                                    addTransaksi = transaksi;
                                } else if (transaksi.getVehicle().getVehicleCode().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                                        transaksi.getIdTAngkut().toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                                        timeFormat.format(transaksi.getStartDate()).contains(etSearch.getText().toString().toLowerCase()) ||
                                        String.valueOf(transaksi.getAngkuts().size()).contains(etSearch.getText().toString().toLowerCase()) ||
                                        String.format("%,d", transaksi.getTotalJanjang()).contains(etSearch.getText().toString().toLowerCase()) ||
                                        String.format("%,d", transaksi.getTotalBrondolan()).contains(etSearch.getText().toString().toLowerCase())) {
                                    addTransaksi = transaksi;
                                }
                            } else if (status == STATUS_LONG_OPERATION_NONE) {
                                addTransaksi = transaksi;
                            }

                            if (!filterAfdeling.equals(FilterHelper.No_Filter_Afdeling) && addTransaksi != null) {
                                boolean ada = false;
                                String[] fafd = filterAfdeling.split(",");
                                for (int i = 0; i < fafd.length; i++) {
                                    for (int j = 0; j < addTransaksi.getAngkuts().size(); j++) {
                                        Angkut angkut = addTransaksi.getAngkuts().get(j);
                                        if (angkut.getTph() != null) {
                                            if (angkut.getTph().getAfdeling() != null) {
                                                if (fafd[i].equalsIgnoreCase(angkut.getTph().getAfdeling())) {
                                                    ada = true;
                                                    break;
                                                }
                                            }
                                        } else if (angkut.getLangsiran() != null) {
                                            if (angkut.getLangsiran().getAfdeling() != null) {
                                                if (angkut.getLangsiran().getAfdeling() != null) {
                                                    if (fafd[i].equalsIgnoreCase(angkut.getLangsiran().getAfdeling())) {
                                                        ada = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        } else if (angkut.getTransaksiPanen() != null) {
                                            if (angkut.getTransaksiPanen().getTph() != null) {
                                                if (angkut.getTransaksiPanen().getTph().getAfdeling() != null) {
                                                    if (fafd[i].equalsIgnoreCase(angkut.getTransaksiPanen().getTph().getAfdeling())) {
                                                        ada = true;
                                                        break;

                                                    }
                                                }
                                            }
                                        } else if (angkut.getTransaksiAngkut() != null) {
                                            if (angkut.getTransaksiAngkut().getLangsiran() != null) {
                                                if (angkut.getTransaksiAngkut().getLangsiran().getAfdeling() != null) {
                                                    if (fafd[i].equalsIgnoreCase(angkut.getTransaksiAngkut().getLangsiran().getAfdeling())) {
                                                        ada = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (!ada) {
                                    addTransaksi = null;
                                }
                            }

                            if (!filterCreateBy.equals(FilterHelper.No_Filter_User) && addTransaksi != null) {
                                if (!addTransaksi.getCreateBy().equals(userId)) {
                                    addTransaksi = null;
                                }
                            }

                            if (addTransaksi != null) {
                                addRowTable(addTransaksi);
                            }
                        }
                    }
                }
                db.close();
            }
        }
    }

    public void hapusNotif(String id,int position){
        idSelected = id;
        posisi = position;
        startLongOperation(STATUS_LONG_OPERATION_SHOW_CONFIRM_DELETED);
    }

    private void cekHapusAngkut(){
        selectedTransaksiAngkut = null;
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", idSelected));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                selectedTransaksiAngkut = gson.fromJson(dataNitrit.getValueDataNitrit(),TransaksiAngkut.class);
                break;
            }
        }
        db.close();

        if(selectedTransaksiAngkut.getStatus() == TransaksiAngkut.UPLOAD){
            selectedTransaksiAngkut = null;
            return;
        }

        if(selectedTransaksiAngkut.getTranskasiAngkutVersions() != null){
            if(selectedTransaksiAngkut.getTranskasiAngkutVersions().size() > 0){
                for (int i = 0 ; i < selectedTransaksiAngkut.getTranskasiAngkutVersions().size();i++){
                    TranskasiAngkutVersion angkutVersion = selectedTransaksiAngkut.getTranskasiAngkutVersions().get(i);
                    if(angkutVersion.getNextIdTransaksi() != null){
                        selectedTransaksiAngkut = null;
                        return;
                    }
                }
            }
        }
    }

    private void hapuAngkut(){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", idSelected));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                selectedTransaksiAngkut = gson.fromJson(dataNitrit.getValueDataNitrit(),TransaksiAngkut.class);

                for(int i = 0 ; i < selectedTransaksiAngkut.getAngkuts().size();i++){
                    Angkut angkut = selectedTransaksiAngkut.getAngkuts().get(i);
                    if(angkut.getTransaksiPanen() != null){
                        TransaksiAngkutHelper.removeIdTPanenHasBeenTap(angkut.getTransaksiPanen().getIdTPanen());
                    }else if (angkut.getTransaksiAngkut() != null){
                        TransaksiAngkutHelper.removeIdTAngkutHasBeenTap(angkut.getTransaksiAngkut().getIdTAngkut());
                    }
                }
                selectedTransaksiAngkut.setStatus(TransaksiAngkut.HAPUS);
                dataNitrit.setValueDataNitrit(gson.toJson(selectedTransaksiAngkut));
                repository.update(dataNitrit);
                break;
            }
        }
        db.close();

        db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_SPB);
        repository = db.getRepository(DataNitrit.class);
        cursor = repository.find(ObjectFilters.eq("idDataNitrit", selectedTransaksiAngkut.getIdSPB()));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                TransaksiSPB transaksiSPB = gson.fromJson(dataNitrit.getValueDataNitrit(), TransaksiSPB.class);
                TransaksiAngkut transaksiAngkut = transaksiSPB.getTransaksiAngkut();
                if (transaksiAngkut.getIdTAngkut().equalsIgnoreCase(selectedTransaksiAngkut.getIdTAngkut())) {

                    transaksiAngkut.setStatus(TransaksiAngkut.HAPUS);
                    transaksiSPB.setTransaksiAngkut(transaksiAngkut);
                    transaksiSPB.setStatus(TransaksiSPB.HAPUS);
                    dataNitrit.setValueDataNitrit(gson.toJson(transaksiSPB));
                    repository.update(dataNitrit);
                    break;
                }
            }
        }
        db.close();
    }

    private void addRowTable(TransaksiAngkut transaksi){
        if (mulai == 0) {
            mulai = transaksi.getStartDate();
        } else if (mulai > transaksi.getStartDate()) {
            mulai = transaksi.getStartDate();
        }

        if (sampai < transaksi.getEndDate()) {
            sampai = transaksi.getEndDate();
        }

        tJanjang += transaksi.getTotalJanjang();
        tBrondolan += transaksi.getTotalBrondolan();

        if (transaksi.getStatus() == TransaksiAngkut.UPLOAD) {
            upload++;
        }

        trip++;
        origin.put(transaksi.getIdTAngkut(), transaksi);

        listSearch.add(transaksi.getIdTAngkut());
        listSearch.add(transaksi.getVehicle().getVehicleCode());
        listSearch.add(transaksi.getTujuan() == TransaksiAngkut.TUJUAN_PKS ? transaksi.getPks().getNamaPKS() :
                transaksi.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR ? transaksi.getLangsiran().getNamaTujuan():
                transaksi.getTujuan() == TransaksiAngkut.TUJUAN_KELUAR ? TransaksiAngkut.KELAUR : ""
        );
        listSearch.add(timeFormat.format(transaksi.getStartDate()));
        listSearch.add(timeFormat.format(transaksi.getEndDate()));
        listSearch.add(String.valueOf(transaksi.getAngkuts().size()));
        listSearch.add(String.valueOf(transaksi.getTotalJanjang()));
        listSearch.add(String.valueOf(transaksi.getTotalBrondolan()));
    }

    private void updateUIRV(int status){
        List<String> lSearch = new ArrayList<>();
        lSearch.addAll(listSearch);
        lSearch = GlobalHelper.removeNullUsingIterator(lSearch);
        ArrayAdapter<String> adapterSearch = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, lSearch);
        etSearch.setAdapter(adapterSearch);
        etSearch.setThreshold(1);
        adapterSearch.notifyDataSetChanged();

        if(status == STATUS_LONG_OPERATION_NONE){
//            etSearch.setText("");
        }

        if (arrayLog.size() > 0) {
            Gson gson = new Gson();
            GlobalHelper.writeFileContent(GlobalHelper.getLogTransaksi(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT], sdf.format(dateSelected)), gson.toJson(arrayLog));
        }

        if(origin.size() > 0 ) {

            ArrayList<TransaksiAngkut> list = new ArrayList<>(origin.values());

            Collections.sort(list, new Comparator<TransaksiAngkut>() {
                @Override
                public int compare(TransaksiAngkut o1, TransaksiAngkut o2) {
                    return o1.getEndDate() > o2.getEndDate() ? -1 : (o1.getEndDate() < o2.getEndDate()) ? 1 : 0;
                }
            });


            ltableview.setVisibility(View.VISIBLE);
            mTableViewModel = new TAngkutTableViewModel(getContext(), list);
            // Create TableView Adapter
            adapter = new TAngkutAdapter(getContext(), mTableViewModel);
            mTableView.setAdapter(adapter);
            mTableView.setRowHeaderWidth(Utils.convertDpToPx(getActivity(),100));
            mTableView.setTableViewListener(new ITableViewListener() {
                @Override
                public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellDoubleClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {
                        // Create Long Press Popup
                        ColumnHeaderPopup popup = new ColumnHeaderPopup(
                                (ColumnHeaderViewHolder) columnHeaderView, mTableView);
                        // Show
                        popup.show();
                    }
                }

                @Override
                public void onColumnHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    if (rowHeaderView != null && rowHeaderView instanceof TAngkutRowHeaderViewHolder) {

                        String [] sid = ((TAngkutRowHeaderViewHolder) rowHeaderView).getCellId().split("-");
                        selectedTransaksiAngkut = origin.get(sid[1]);
                        startLongOperation(STATUS_LONG_OPERATION_SELECT_ITEM);
                    }

                }

                @Override
                public void onRowHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }

                @Override
                public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    // Create Long Press Popup
                    RowHeaderLongPressPopup popup = new RowHeaderLongPressPopup(rowHeaderView, mTableView,mainMenuActivity);
                    // Show
                    popup.show();
                }
            });
            adapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel
                    .getRowHeaderList(), mTableViewModel.getCellList());


//            mTableFilter = new Filter(mTableView);
        }else{
            ltableview.setVisibility(View.GONE);
        }


        tvStartTime.setText(mulai == 0 ? "" : timeFormat.format(mulai));
        tvFinishTime.setText(sampai == 0 ? "" : timeFormat.format(sampai));
        tvTotalJanjang.setText(String.format("%,d",tJanjang) + " JJg");
        tvTotalBrondolan.setText(String.format("%,d",tBrondolan) + " Kg");
        tvTrip.setText(String.valueOf(trip));
        tvUpload.setText(String.valueOf(upload));

        Date d = new Date(System.currentTimeMillis());
        if(sdf.format(d).equals(sdf.format(dateSelected))){
            showNewTranskasi();
        }else{
            llNewData.setVisibility(View.GONE);
        }
    }

    public void startLongOperation(int status){
        new LongOperation().execute(String.valueOf(status));
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        String dataString;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (((BaseActivity) getActivity()).alertDialogBase != null) {
                ((BaseActivity) getActivity()).alertDialogBase.cancel();
            }
            ((BaseActivity) getActivity()).alertDialogBase = WidgetHelper.showWaitingDialog(getActivity(),getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])){
                case STATUS_LONG_OPERATION_NONE:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM:{
                    Gson gson = new Gson();
                    dataString = gson.toJson(selectedTransaksiAngkut);
                    break;
                }
                case STATUS_LONG_OPERATION_NEW_DATA:{
                    TransaksiAngkutHelper.hapusFileAngkut(true);
//                    TransaksiAngkutHelper.createNewAngkut();
                    break;
                }
                case STATUS_LONG_OPERATION_REKONSILIASI:{
                    break;
                }
                case STATUS_LONG_OPERATION_SHOW_CONFIRM_DELETED:{
                    cekHapusAngkut();
                    break;
                }
                case STATUS_LONG_OPERATION_DELETED:{
                    hapuAngkut();
                    break;
                }

            }

            return strings[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            switch (Integer.parseInt(result)){
                case STATUS_LONG_OPERATION_NONE:{
                    updateUIRV(Integer.parseInt(result));
                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateUIRV(Integer.parseInt(result));
                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_SELECT_ITEM:{
                    Intent intent = new Intent(getActivity(), AngkutActivity.class);
                    intent.putExtra("transaksiAngkut","1");
                    GlobalHelper.writeFileContent(GlobalHelper.TRANSFER_TRANSAKSI_ANGKUT, dataString);
                    startActivityForResult(intent,GlobalHelper.RESULT_ANGKUTACTIVITY);
                    break;
                }
                case STATUS_LONG_OPERATION_NEW_DATA:{
                    Intent intent = new Intent(getActivity(), AngkutActivity.class);
                    startActivityForResult(intent, GlobalHelper.RESULT_ANGKUTACTIVITY);
                    break;
                }

                case STATUS_LONG_OPERATION_REKONSILIASI:{
                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    Intent intent = new Intent(getActivity(), RekonsiliasiListActivity.class);
                    intent.putExtra("selectedDate", dateSelected.getTime());
//                    startActivity(intent);
                    startActivityForResult(intent, GlobalHelper.RESULT_REKONSILIASI);
                    break;
                }
                case STATUS_LONG_OPERATION_SHOW_CONFIRM_DELETED:{
                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    if(selectedTransaksiAngkut != null){
                        mainMenuHelper.showYesNoQuestion(selectedTransaksiAngkut);
                    }else{
                        Toast.makeText(HarvestApp.getContext(),"Data Yang Sudah DiUpload Dan Sudah Diangkut Tidak Bisa Di Hapus",Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                case STATUS_LONG_OPERATION_DELETED:{
                    mTableView.getAdapter().removeRow(posisi);
                    if (((BaseActivity) getActivity()).alertDialogBase != null) {
                        ((BaseActivity) getActivity()).alertDialogBase.cancel();
                    }
                    break;
                }
            }
        }
    }

}