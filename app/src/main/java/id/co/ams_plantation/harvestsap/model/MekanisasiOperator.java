package id.co.ams_plantation.harvestsap.model;

public class MekanisasiOperator {

    public static int MekanisasiOperator_NotUplaod = 1;
    public static int MekanisasiOperator_Upload = 2;

    public static int masterIdx = 0;
    public static int freeTextIdx = 1;

    String idMekanisasiOperator;
    String estCode;
    String date;
    Vehicle vehicle;
    Pemanen pemanen;
    int typeVehicleMekanisasi;
    int status;
    long createDate;
    String createBy;

    public MekanisasiOperator(String idMekanisasiOperator, String estCode, String date, Vehicle vehicle, Pemanen pemanen,int typeVehicleMekanisasi, int status, long createDate, String createBy) {
        this.idMekanisasiOperator = idMekanisasiOperator;
        this.estCode = estCode;
        this.date = date;
        this.vehicle = vehicle;
        this.pemanen = pemanen;
        this.typeVehicleMekanisasi = typeVehicleMekanisasi;
        this.status = status;
        this.createDate = createDate;
        this.createBy = createBy;
    }

    public String getIdMekanisasiOperator() {
        return idMekanisasiOperator;
    }

    public void setIdMekanisasiOperator(String idMekanisasiOperator) {
        this.idMekanisasiOperator = idMekanisasiOperator;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Pemanen getPemanen() {
        return pemanen;
    }

    public void setPemanen(Pemanen pemanen) {
        this.pemanen = pemanen;
    }

    public int getTypeVehicleMekanisasi() {
        return typeVehicleMekanisasi;
    }

    public void setTypeVehicleMekanisasi(int typeVehicleMekanisasi) {
        this.typeVehicleMekanisasi = typeVehicleMekanisasi;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
}
