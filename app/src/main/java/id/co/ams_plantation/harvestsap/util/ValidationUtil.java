package id.co.ams_plantation.harvestsap.util;

import android.text.TextUtils;

import java.util.regex.Matcher;

import static java.util.regex.Pattern.compile;


public class ValidationUtil {
    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
    public static boolean isValidNfc(String value){
        final Matcher matcher = compile("^[0-9A-F]+$").matcher(value.toUpperCase().replace(" ",""));
        return matcher.matches();
    }
}
