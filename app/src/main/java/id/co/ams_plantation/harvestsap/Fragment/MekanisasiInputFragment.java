package id.co.ams_plantation.harvestsap.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.esri.core.internal.util.LocalGDBUtil;
import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.labo.kaji.fragmentanimations.MoveAnimation;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.SwipeItemRecycleView.SwipeController;
import id.co.ams_plantation.harvestsap.SwipeItemRecycleView.SwipeControllerActions;
import id.co.ams_plantation.harvestsap.TableView.adapter.MekanisasiPanenAdapter;
import id.co.ams_plantation.harvestsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestsap.TableView.popup.RowHeaderLongPressPopup;
import id.co.ams_plantation.harvestsap.TableView.view_model.MekanisasiPanenTableViewModel;
import id.co.ams_plantation.harvestsap.adapter.CagesInputAdapter;
import id.co.ams_plantation.harvestsap.adapter.LoaderInputAdapter;
import id.co.ams_plantation.harvestsap.adapter.PengirimanGandaAdapater;
import id.co.ams_plantation.harvestsap.adapter.SelectCagesAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectLangsiranAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectOperatorAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectPksAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectSpkAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectSupervisionAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectUserAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectVehicleAdapter;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.DynamicParameterPenghasilan;
import id.co.ams_plantation.harvestsap.model.Langsiran;
import id.co.ams_plantation.harvestsap.model.ManualSPB;
import id.co.ams_plantation.harvestsap.model.MekanisasiPanen;
import id.co.ams_plantation.harvestsap.model.Operator;
import id.co.ams_plantation.harvestsap.model.Cages;
import id.co.ams_plantation.harvestsap.model.PKS;
import id.co.ams_plantation.harvestsap.model.PengirimanGanda;
import id.co.ams_plantation.harvestsap.model.SPK;
import id.co.ams_plantation.harvestsap.model.StampImage;
import id.co.ams_plantation.harvestsap.model.SubPemanen;
import id.co.ams_plantation.harvestsap.model.SubTotalAngkutMekanisasi;
import id.co.ams_plantation.harvestsap.model.SupervisionList;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.model.Vehicle;
import id.co.ams_plantation.harvestsap.service.BluetoothService;
import id.co.ams_plantation.harvestsap.ui.BarcodeScannerActivity;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.MekanisasiActivity;
import id.co.ams_plantation.harvestsap.util.BjrHelper;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.ModelRecyclerViewHelper;
import id.co.ams_plantation.harvestsap.util.TphHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiPanenHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiSpbHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import ng.max.slideview.SlideView;
import pl.aprilapps.easyphotopicker.EasyImage;

public class MekanisasiInputFragment extends Fragment {

    public static final int STATUS_LONG_OPERATION_NONE = 0;
    public static final int STATUS_LONG_OPERATION_SEARCH = 1;
    public static final int STATUS_LONG_OPERATION_SAVE_DATA_PRINT = 2;
    public static final int STATUS_LONG_OPERATION_SAVE_DATA_SHARE = 3;
    public static final int STATUS_LONG_OPERATION_PRINT = 4;
    public static final int STATUS_LONG_OPERATION_SHARE = 5;
    public static final int STATUS_LONG_OPERATION_DETAIL = 6;
    public static final int STATUS_LONG_OPERATION_ADD_TRANSAKSI_PANEN_MAX_RESTAN = 7;
    public static final int STATUS_LONG_OPERATION_ADD_TRANSAKSI_PANEN = 8;
    public static final int STATUS_LONG_OPERATION_DELETED_ANGKUT = 9;

    private static final String TAG = MekanisasiInputFragment.class.getSimpleName();

    BaseActivity baseActivity;
    MekanisasiActivity mekanisasiActivity;
    TransaksiSpbHelper transaksiSpbHelper;
    TransaksiAngkutHelper transaksiAngkutHelper;
    ModelRecyclerViewHelper modelRecyclerViewHelper;
    TphHelper tphHelper;

    RelativeLayout lShowHide;
    RelativeLayout lShow;
    RelativeLayout lHide;
    LinearLayout lShowHideIsi;
    LinearLayout lVehicle;
    ImageView ivShow;
    ImageView ivHide;

//    CheckBox cbPengirimanGanda;
//    CheckBox cbBin;
    RadioGroup rgSelectDirectType;
    RadioButton rbPKS;
    RadioButton rbTransitPoint;
    RecyclerView rvLoader;
    RecyclerView rvCages;
    RecyclerView rvPengirimanGanda;

    TextView tvUnit;
    TextView tvSpbId;
    TextView tvSpbIDKeluar;
    TextView tvCreateTime;
    TextView tvOa;
    CompleteTextViewHelper etSpk;
//    CompleteTextViewHelper etBin;
    CompleteTextViewHelper etVehicle;
    CompleteTextViewHelper etDriver;
    CompleteTextViewHelper etSubstitute;
    BetterSpinner lsKendaraan;
    BetterSpinner lsTujuan;
    CompleteTextViewHelper tvPks;
    CompleteTextViewHelper etApproveKeluar;
    CompleteTextViewHelper etApprove;
    LinearLayout lPks;
    LinearLayout lKeluar;
    LinearLayout lSPK;
    LinearLayout llCages;
    Button btnDone;

    SelectSpkAdapter adapterSpk;
    SelectLangsiranAdapter adapterLangsiran;
    SelectSupervisionAdapter adapterSubtitute;
    SelectVehicleAdapter adapterVehicle;
    SelectPksAdapter adapterPks;
    MekanisasiPanenAdapter adapter;
    MekanisasiPanenTableViewModel mTableViewModel;
    TableView mTableView;
    AppCompatEditText etSearch;
    Button btnSearch;
    RelativeLayout ltableview;
    LinearLayout newMekanisasi;
    LinearLayout newTransaksiPanen;
    LinearLayout llket1;

    TextView tvTotalOph;
    TextView tvTotalTph;
    TextView tvTotalJanjang;
    TextView tvTotalBerat;
    TextView tvTotalPemanen;
//    TextView tvQuickTruck;
//    TextView tvOperatorQT;
//    TextView tvKelompok;
//    TextView tvBlock;
    CheckBox cbKraniSubstitute;

    View tphphoto;
    ImageView ivPhoto;
    ImageView imagecacnel;
    ImageView imagesview;
    TextView driverScanQr;
    ImageView ivScanQr;
    SliderLayout sliderLayout;
    RelativeLayout rl_ipf_takepicture;

    String idSpb;
    int tJanjang = 0;
    int tBrondolan = 0;
    double tBerat = 0;
    Set<String> distinctQT;
    Set<String> distinctOperatorQT;
    Set<String> distinctLoaderQT;
    Set<String> distinctMekanisasi;
    Set<String> distinctBlock;
    Set<String> distinctTPH;
    Set<String> distinctPemanen;
    HashMap<String,TransaksiPanen>  originAngkut;
    HashMap<String,Angkut> originAngkutKartu;
    HashSet<String> distinctAfdeling;

    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm");
    SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
    SelectOperatorAdapter adapterOperator;
    SelectUserAdapter adapterAssAfd;

    TransaksiSPB selectedTransaksiSPB;
    TransaksiSPB headerTransaksiSPB;
    TransaksiPanen transaksiPanenNFC;

    AlertDialog alertDialog;
    String idPanenSelected;
    String idHapusAngkut;
    String messageError;

    boolean insertDBAngkut;
    TransaksiPanen idPanenAngkut;
    double latitude;
    double longitude;

    HashMap<String,SPK> allSpkAngkut;
    HashMap<String,Operator> loaderSave;
    HashMap<String,PengirimanGanda> pengirimanGandaSave;
    HashMap<String, Cages> masterCages;
    HashMap<String, Vehicle> masterVehicle;
    HashMap<String, Cages> cagesSave;

    LoaderInputAdapter adapterLoader;
    CagesInputAdapter adapterCages;
    PengirimanGandaAdapater adapterPengirimanGanda;

    SelectCagesAdapter adapterCagesPopUp;

    SwipeController swipeController;
    SwipeController swipeControllerCages;
    SwipeController swipeControllerPengiriman;

    ArrayList<Operator> loaders;
    ArrayList<PengirimanGanda> pengirimanGandas;
    Operator selectedOperator;
    ArrayList<Cages> cages;
    PKS selectedPks;
    User approveBy;
    Vehicle vehicleUse;
    SPK spkUse;
    SupervisionList substituteSelected;

    Cages selectedCagesPopUp;

    String initialDropdownValue = null;

    public DynamicParameterPenghasilan dynamicParameterPenghasilan;

    StampImage stampImage;
    ArrayList<File> ALselectedImage;
    Set<String> idNfc;

    HashMap<String,Integer> blockWarna = new HashMap<>();
    int [] warnaBlok = {
            HarvestApp.getContext().getResources().getColor(R.color.DeepSkyBlue),
            HarvestApp.getContext().getResources().getColor(R.color.MediumSpringGreen),
            HarvestApp.getContext().getResources().getColor(R.color.Aqua),
            HarvestApp.getContext().getResources().getColor(R.color.CornflowerBlue),
            HarvestApp.getContext().getResources().getColor(R.color.Chartreuse),
            HarvestApp.getContext().getResources().getColor(R.color.DarkSeaGreen),
            HarvestApp.getContext().getResources().getColor(R.color.GreenYellow),
            HarvestApp.getContext().getResources().getColor(R.color.PowderBlue),
            HarvestApp.getContext().getResources().getColor(R.color.Tan),
            HarvestApp.getContext().getResources().getColor(R.color.Plum)
    };

    public static MekanisasiInputFragment getInstance() {
        Log.i(TAG, "getInstance: no bundle");
        MekanisasiInputFragment mekanisasiInputFragment = new MekanisasiInputFragment();
        return mekanisasiInputFragment;
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return MoveAnimation.create(MoveAnimation.UP, enter, 600);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mekanisasi_input_layout, null, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: ");

        baseActivity = (BaseActivity) getActivity();
        mekanisasiActivity = (MekanisasiActivity) getActivity();
        transaksiSpbHelper = new TransaksiSpbHelper(getActivity());
        transaksiAngkutHelper = new TransaksiAngkutHelper(getActivity());
        modelRecyclerViewHelper = new ModelRecyclerViewHelper(getActivity());
        tphHelper = new TphHelper(getActivity());

        lShowHide = view.findViewById(R.id.lShowHide);
        lShowHideIsi = view.findViewById(R.id.lShowHideIsi);
        lVehicle = view.findViewById(R.id.lVehicle);
        lShow = view.findViewById(R.id.lShow);
        lHide = view.findViewById(R.id.lHide);
        ivShow = view.findViewById(R.id.ivShow);
        ivHide = view.findViewById(R.id.ivHide);
        tvUnit = view.findViewById(R.id.tvUnit);
        tvSpbId = view.findViewById(R.id.tvSpbID);
        tvSpbIDKeluar = view.findViewById(R.id.tvSpbIDKeluar);
        tvCreateTime = view.findViewById(R.id.tvCreateTime);
        etVehicle = view.findViewById(R.id.etVehicle);
        etDriver = view.findViewById(R.id.etDriver);
        lsKendaraan = view.findViewById(R.id.lsKendaraan);
        lsTujuan = view.findViewById(R.id.lsTujuan);
        tvPks = view.findViewById(R.id.tvPks);
        etApprove = view.findViewById(R.id.etApprove);
        etApproveKeluar = view.findViewById(R.id.etApproveKeluar);
        btnDone = view.findViewById(R.id.btnClsoe);

        cbKraniSubstitute = view.findViewById(R.id.cb_krani_substitute);
        etSubstitute = view.findViewById(R.id.etSubstitute);

//        cbBin = view.findViewById(R.id.cb_bin);
//        cbPengirimanGanda = view.findViewById(R.id.cb_pengiriman_ganda);
        rvPengirimanGanda = view.findViewById(R.id.rvPengirimanGanda);
        rvLoader = view.findViewById(R.id.rvLoader);
        rvCages = view.findViewById(R.id.rvCages);
//        etBin = view.findViewById(R.id.etBin);

        rgSelectDirectType = view.findViewById(R.id.rgSelectDirectType);
        rbPKS = view.findViewById(R.id.rbPKS);
        rbTransitPoint = view.findViewById(R.id.rbTransitPoint);

        etSpk = view.findViewById(R.id.etSpk);
        tvOa = view.findViewById(R.id.tvOa);
        mTableView = view.findViewById(R.id.tableview);
        etSearch = view.findViewById(R.id.etSearch);
        btnSearch = view.findViewById(R.id.btnSearch);
        ltableview = view.findViewById(R.id.ltableview);
        lPks = view.findViewById(R.id.lPks);
        lKeluar = view.findViewById(R.id.lKeluar);
        lSPK = view.findViewById(R.id.lSPK);
        llCages = view.findViewById(R.id.llCages);

        tvTotalOph = view.findViewById(R.id.tvTotalOph);
        tvTotalTph = view.findViewById(R.id.tvTotalTph);
        tvTotalJanjang = view.findViewById(R.id.tvTotalJanjang);
        tvTotalBerat = view.findViewById(R.id.tvTotalBerat);
        tvTotalPemanen = view.findViewById(R.id.tvTotalPemanen);
//        tvQuickTruck = view.findViewById(R.id.tvQuickTruck);
//        tvOperatorQT = view.findViewById(R.id.tvOperatorQT);
//        tvLoaderQT = view.findViewById(R.id.tvLoaderQT);
//        tvKelompok = view.findViewById(R.id.tvKelompok);
//        tvBlock = view.findViewById(R.id.tvBlock);

//        scanQr = view.findViewById(R.id.scanQr);
        newMekanisasi = view.findViewById(R.id.newMekanisasi);
        newTransaksiPanen = view.findViewById(R.id.newTransaksiPanen);
        llket1 = view.findViewById(R.id.llket1);

        tphphoto = view.findViewById(R.id.tphphoto);
        ivPhoto = view.findViewById(R.id.ivPhoto);
        imagecacnel = view.findViewById(R.id.imagecacnel);
        imagesview = view.findViewById(R.id.imagesview);
        ivScanQr = view.findViewById(R.id.scanQrCages);
        driverScanQr = view.findViewById(R.id.driverScanQr);
        rl_ipf_takepicture = view.findViewById(R.id.rl_ipf_takepicture);
        sliderLayout = view.findViewById(R.id.sliderLayout);

        ALselectedImage = new ArrayList<>();
        loaders = new ArrayList<>();
        cages = new ArrayList<>();
        pengirimanGandas = new ArrayList<>();

//        etBin.setVisibility(View.GONE);
        lShowHideIsi.setVisibility(View.GONE);
        rvPengirimanGanda.setVisibility(View.GONE);
        lVehicle.setVisibility(View.GONE);
        lShow.setVisibility(View.VISIBLE);
        lHide.setVisibility(View.GONE);
        lSPK.setVisibility(View.GONE);
        btnDone.setVisibility(View.GONE);
        driverScanQr.setVisibility(View.VISIBLE);

        ivShow.setImageDrawable(new
                IconicsDrawable(getActivity())
                .icon(MaterialDesignIconic.Icon.gmi_chevron_down).sizeDp(24)
                .colorRes(R.color.Black));

        ivHide.setImageDrawable(new
                IconicsDrawable(getActivity())
                .icon(MaterialDesignIconic.Icon.gmi_chevron_up).sizeDp(24)
                .colorRes(R.color.Black));

        Collection<User> users = GlobalHelper.dataAllAsstAfd.values();
        Collection<PKS> pkss = GlobalHelper.dataAllPks.values();
        Collection<Langsiran> langsirans = GlobalHelper.dataAllLagsiran.values();
        Collection<Operator> operators = GlobalHelper.dataOperator.values();
        masterCages = GlobalHelper.dataOperatorCages;
        masterVehicle = GlobalHelper.dataVehicle;
        Collection<Cages> listCages = masterCages.values();
        Collection<Vehicle> vehicles = GlobalHelper.dataVehicle.values();
        Collection<SupervisionList> supervisionLists = GlobalHelper.dataAllSupervision.values();
        allSpkAngkut = GlobalHelper.getDataAllSPKAngkut();

        mekanisasiActivity.mekanisasiPanenSelected = null;

        adapterAssAfd = new SelectUserAdapter(getActivity(),R.layout.row_object, new ArrayList<User>(users));
        adapterPks = new SelectPksAdapter(getActivity(),R.layout.row_object, new ArrayList<PKS>(pkss));
        adapterOperator = new SelectOperatorAdapter(getActivity(),R.layout.row_setting, new ArrayList<Operator>(operators));
        adapterVehicle= new SelectVehicleAdapter(getActivity(),R.layout.row_setting, new ArrayList<Vehicle>(vehicles));
        adapterSubtitute = new SelectSupervisionAdapter(getActivity(),R.layout.row_setting, new ArrayList<SupervisionList>(supervisionLists));
        adapterSpk = new SelectSpkAdapter(getActivity(),R.layout.row_setting, new ArrayList<SPK>(allSpkAngkut.values()));
        adapterLangsiran = new SelectLangsiranAdapter(getActivity(),R.layout.row_setting, new ArrayList<Langsiran>(langsirans));

        dynamicParameterPenghasilan = GlobalHelper.getAppConfig().get(GlobalHelper.getEstate().getEstCode());
        SimpleDateFormat sdfDate = new SimpleDateFormat("ddMMyy");
        idSpb = "S" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT))+ sdfDate.format(System.currentTimeMillis()) + GlobalHelper.getUser().getUserID();
        tvSpbId.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));
        tvSpbIDKeluar.setText(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));
        tvCreateTime.setText(sdf.format(System.currentTimeMillis()));

        etVehicle.setAdapter(adapterVehicle);
        etVehicle.setThreshold(1);

        etDriver.setAdapter(adapterOperator);
        etDriver.setThreshold(1);

        etApproveKeluar.setAdapter(adapterAssAfd);
        etApproveKeluar.setThreshold(1);

        etApprove.setAdapter(adapterAssAfd);
        etApprove.setThreshold(1);

        etSpk.setAdapter(adapterSpk);
        etSpk.setThreshold(1);

        etSubstitute.setAdapter(adapterSubtitute);
        etSubstitute.setThreshold(1);

        List<String> bKendaraan = new ArrayList<>();
        bKendaraan.add(TransaksiAngkut.KEBUN);
        bKendaraan.add(TransaksiAngkut.LUAR);
        ArrayAdapter<String> adapterKendaran = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, bKendaraan);
        lsKendaraan.setAdapter(adapterKendaran);

        List<String> bAlas = new ArrayList<>();
        bAlas.add(TransaksiAngkut.PKS);
        bAlas.add(TransaksiAngkut.KELAUR);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_dropdown_item_1line, bAlas);
        lsTujuan.setAdapter(adapter);

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.SUBSTITUTE_KRANI_ANGKUT, Context.MODE_PRIVATE);
        String stringSubstituteKrani = preferences.getString(HarvestApp.SUBSTITUTE_KRANI_ANGKUT, null);
        if(stringSubstituteKrani == null){
            cbKraniSubstitute.setChecked(false);
            etSubstitute.setVisibility(View.GONE);
            substituteSelected = null;
        }else{
            Gson gson = new Gson();
            cbKraniSubstitute.setChecked(true);
            substituteSelected = gson.fromJson(stringSubstituteKrani,SupervisionList.class);
            if(substituteSelected != null) {
                etSubstitute.setText(substituteSelected.getNama());
            }else{
                if(adapterSubtitute != null) {
                    Log.d(this.getClass()+"stringSubstituteKrani", "count "+ adapterSubtitute.getCount());
                    if(adapterSubtitute.getCount() > 0) {
                        substituteSelected = adapterSubtitute.getItemAt(0);
                        etSubstitute.setText(substituteSelected.getNama());
                    }
                }else{
                    etSubstitute.setText("");
                }
            }
            etSubstitute.setVisibility(View.VISIBLE);
            etSubstitute.setText(substituteSelected.getNama());
        }

        if(mekanisasiActivity.selectedTransaksiSPB != null){
            idSpb = mekanisasiActivity.selectedTransaksiSPB.getIdSPB();
            selectedTransaksiSPB = mekanisasiActivity.selectedTransaksiSPB;
            tvSpbId.setText(TransaksiSpbHelper.converIdSPBToNoSPB(selectedTransaksiSPB.getIdSPB()));
            tvSpbIDKeluar.setText(TransaksiSpbHelper.converIdSPBToNoSPB(selectedTransaksiSPB.getIdSPB()));
            tvCreateTime.setText(sdf.format(selectedTransaksiSPB.getCreateDate()));

            if(selectedTransaksiSPB.getStatus() == TransaksiSPB.UPLOAD || selectedTransaksiSPB.getStatus() == TransaksiSPB.BELUM_UPLOAD ) {
//                scanQr.setVisibility(View.GONE);
                newMekanisasi.setVisibility(View.GONE);
                newTransaksiPanen.setVisibility(View.GONE);
                llket1.setVisibility(View.GONE);
            }
        }

        Gson gson = new Gson();
        headerTransaksiSPB =  gson.fromJson(TransaksiSpbHelper.readFileSpb(),TransaksiSPB.class);
        if(headerTransaksiSPB != null){
            if(headerTransaksiSPB.getTransaksiAngkut().getSupir() != null){
                if(headerTransaksiSPB.getTransaksiAngkut().getSupir().getNama() != null){
                    etDriver.setText(headerTransaksiSPB.getTransaksiAngkut().getSupir().getNama());
                    selectedOperator = headerTransaksiSPB.getTransaksiAngkut().getSupir();
                }
            }

            if(headerTransaksiSPB.getTransaksiAngkut().getVehicle() != null){
                if(headerTransaksiSPB.getTransaksiAngkut().getVehicle().getVehicleCode() != null){
                    vehicleUse = headerTransaksiSPB.getTransaksiAngkut().getVehicle();
                    lVehicle.setVisibility(View.VISIBLE);
                    rbPKS.setChecked(true);
                    etVehicle.setText(TransaksiAngkutHelper.showVehicle(vehicleUse));
                }
            }

            if(headerTransaksiSPB.getTransaksiAngkut().getKendaraanDari() == TransaksiAngkut.KENDARAAN_LUAR) {
                etVehicle.setAdapter(null);
                etVehicle.setThreshold(0);
                etDriver.setAdapter(null);
                etDriver.setThreshold(0);
                tvUnit.setText("Nomor Plat");
                lSPK.setVisibility(View.VISIBLE);
                driverScanQr.setVisibility(View.GONE);
            }

            if(headerTransaksiSPB.getTransaksiAngkut().getSpk() != null){
                etSpk.setText(headerTransaksiSPB.getTransaksiAngkut().getSpk() .getIdSPK());
                tvOa.setText(TransaksiPanenHelper.showSPK(headerTransaksiSPB.getTransaksiAngkut().getSpk()));
                spkUse = headerTransaksiSPB.getTransaksiAngkut().getSpk();
            }

            approveBy = GlobalHelper.dataAllAsstAfd.get(headerTransaksiSPB.getApproveBy());
            if(approveBy != null) {
                etApproveKeluar.setText(approveBy.getUserFullName());
                etApprove.setText(approveBy.getUserFullName());
            }else{
                etApproveKeluar.setText(headerTransaksiSPB.getApproveBy());
                etApprove.setText(headerTransaksiSPB.getApproveBy());
            }
            lsKendaraan.setText(bKendaraan.get(headerTransaksiSPB.getTransaksiAngkut().getKendaraanDari()));

            if(headerTransaksiSPB.getSubstitute() != null){
                substituteSelected = headerTransaksiSPB.getSubstitute();
                cbKraniSubstitute.setChecked(true);
                etSubstitute.setVisibility(View.VISIBLE);
                etSubstitute.setText(substituteSelected.getNama());
            }else{
                cbKraniSubstitute.setChecked(false);
                etSubstitute.setVisibility(View.GONE);
            }

            if(headerTransaksiSPB.getTransaksiAngkut().getTujuan() == 0){
                lsTujuan.setText(bAlas.get(0));
                lPks.setVisibility(View.VISIBLE);
                lKeluar.setVisibility(View.GONE);
                tvPks.setAdapter(adapterPks);
                tvPks.setThreshold(1);
                adapterPks.notifyDataSetChanged();
                if(headerTransaksiSPB.getTransaksiAngkut().getPks() != null){
                    lPks.setVisibility(View.VISIBLE);
                    lKeluar.setVisibility(View.GONE);
                    selectedPks = headerTransaksiSPB.getTransaksiAngkut().getPks();
                    tvPks.setText(selectedPks.getNamaPKS());
                }else{
                    if(GlobalHelper.dataAllPks.values().size() == 0 ){
                        lsTujuan.setText(bAlas.get(1));
                        selectedPks= null;
                        lPks.setVisibility(View.GONE);
                        lKeluar.setVisibility(View.VISIBLE);
                    }else{
                        selectedPks = (PKS) GlobalHelper.dataAllPks.values().toArray()[0];
                        if (selectedPks != null) {
                            lPks.setVisibility(View.VISIBLE);
                            lKeluar.setVisibility(View.GONE);
                            tvPks.setText(selectedPks.getNamaPKS());
                        }
                    }
                }
            }else{
//                hide dulu karena tujaun external di hide
                lsTujuan.setText(bAlas.get(1));
                lPks.setVisibility(View.GONE);
                lKeluar.setVisibility(View.VISIBLE);
            }

            if(headerTransaksiSPB.getTransaksiAngkut().getLoader() != null){
                loaders = headerTransaksiSPB.getTransaksiAngkut().getLoader();
            }

            if (headerTransaksiSPB.getTransaksiAngkut().getCages() != null) {
                cages = headerTransaksiSPB.getTransaksiAngkut().getCages();
            }

            if(headerTransaksiSPB.getTransaksiAngkut().getPengirimanGandas() != null){
//                cbPengirimanGanda.setChecked(true);
                rbTransitPoint.setChecked(true);
                rvPengirimanGanda.setVisibility(View.VISIBLE);
                pengirimanGandas = headerTransaksiSPB.getTransaksiAngkut().getPengirimanGandas();
            }

//            if(headerTransaksiSPB.getTransaksiAngkut().getBin() != null){
//                cbBin.setChecked(true);
//                etBin.setVisibility(View.VISIBLE);
//                etBin.setText(headerTransaksiSPB.getTransaksiAngkut().getBin());
//            }


            if(headerTransaksiSPB.getFoto() != null){
                if(headerTransaksiSPB.getFoto().size() > 0){
                    ALselectedImage = new ArrayList<>();
                    for(String file : headerTransaksiSPB.getFoto()) {
                        ALselectedImage.add(new File(file));
                    }
                    setupimageslide();
                }
            }
        }else{
            headerTransaksiSPB = new TransaksiSPB();
            headerTransaksiSPB.setIdSPB(idSpb);
            headerTransaksiSPB.setNoSpb(TransaksiSpbHelper.converIdSPBToNoSPB(idSpb));
            headerTransaksiSPB.setCreateDate(System.currentTimeMillis());
            headerTransaksiSPB.setTransaksiAngkut( new TransaksiAngkut());
            headerTransaksiSPB.getTransaksiAngkut().setTujuan(0);
            headerTransaksiSPB.setStatus(TransaksiSPB.BELUM_CLOSE);

//            selectedOperator = (Operator) GlobalHelper.dataOperator.values().toArray()[0];
//            vehicleUse = (Vehicle) GlobalHelper.dataVehicle.values().toArray()[0];
            approveBy = (User) users.toArray()[0];

//            setOperator((Operator) GlobalHelper.dataOperator.values().toArray()[0]);
//            setVehicle((Vehicle) GlobalHelper.dataVehicle.values().toArray()[0]);
            etApproveKeluar.setText(approveBy.getUserFullName());
            etApprove.setText(approveBy.getUserFullName());
            adapterAssAfd.notifyDataSetChanged();
            if(vehicles.size() > 0 && operators.size() > 0){
                lsKendaraan.setText(bKendaraan.get(TransaksiAngkut.KENDARAAN_KEBUN));
            }else {
                headerTransaksiSPB.getTransaksiAngkut().setKendaraanDari(TransaksiAngkut.KENDARAAN_LUAR);
                lsKendaraan.setText(bKendaraan.get(TransaksiAngkut.KENDARAAN_LUAR));
                lSPK.setVisibility(View.VISIBLE);
                driverScanQr.setVisibility(View.GONE);
                tvUnit.setText("Nomor Plat");
                etVehicle.setAdapter(null);
                etVehicle.setThreshold(0);
                etDriver.setAdapter(null);
                etDriver.setThreshold(0);
            }
            lsTujuan.setText(bAlas.get(0));
            lPks.setVisibility(View.VISIBLE);
            lKeluar.setVisibility(View.GONE);
            tvPks.setAdapter(adapterPks);
            tvPks.setThreshold(1);
            adapterPks.notifyDataSetChanged();
            if(headerTransaksiSPB.getTransaksiAngkut().getPks() != null){
                lPks.setVisibility(View.VISIBLE);
                lKeluar.setVisibility(View.GONE);
                selectedPks = headerTransaksiSPB.getTransaksiAngkut().getPks();
                tvPks.setText(selectedPks.getNamaPKS());
            }else{
                if(GlobalHelper.dataAllPks.values().size() == 0 ){
                    lsTujuan.setText(bAlas.get(1));
                    selectedPks= null;
                    lPks.setVisibility(View.GONE);
                    lKeluar.setVisibility(View.VISIBLE);
                }else{
                    selectedPks = (PKS) GlobalHelper.dataAllPks.values().toArray()[0];
                    if (selectedPks != null) {
                        lPks.setVisibility(View.VISIBLE);
                        lKeluar.setVisibility(View.GONE);
                        tvPks.setText(selectedPks.getNamaPKS());
                    }
                }
            }
        }

//        cbBin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    etBin.setVisibility(View.VISIBLE);
//                }else{
//                    etBin.setVisibility(View.GONE);
//                    headerTransaksiSPB.getTransaksiAngkut().setBin(null);
//                }
//                updateHeaderTransaksiSpb();
//            }
//        });

        if(loaders == null){
            loaders = new ArrayList<>();
            loaders.add(new Operator());
        }else{
            if(loaders.size() == 0){
                loaders = new ArrayList<>();
                loaders.add(new Operator());
            }
        }
        adapterLoader = new LoaderInputAdapter(getActivity(),new ArrayList<Operator>(operators),loaders);
        rvLoader.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvLoader.setAdapter(adapterLoader);

        rgSelectDirectType.setOnCheckedChangeListener((group, checkedId) -> {
            try {
                if (checkedId == R.id.rbPKS ) {
                    lVehicle.setVisibility(View.VISIBLE);
                    rvPengirimanGanda.setVisibility(View.GONE);
                    headerTransaksiSPB.getTransaksiAngkut().setPengirimanGandas(null);
                } else if (checkedId == R.id.rbTransitPoint) {
                    lVehicle.setVisibility(View.VISIBLE);
                    rvPengirimanGanda.setVisibility(View.VISIBLE);
                    JSONArray array = new JSONArray();
                    for (int i = 0; i < pengirimanGandas.size(); i++) {
                        array.put(gson.toJson(pengirimanGandas.get(i)));
                    }
                    headerTransaksiSPB.getTransaksiAngkut().setPengirimanGandas(pengirimanGandas);
                }
                updateHeaderTransaksiSpb();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        swipeController = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                if (loaders.size() > 1) {
                    adapterLoader.loader.remove(position);

                    RecyclerView.ViewHolder viewHolder = rvLoader.findViewHolderForAdapterPosition(position);
                    View v = viewHolder.itemView;
                    TextView tvValue = v.findViewById(R.id.tvValue);

                    if(!tvValue.getText().toString().isEmpty()) {
                        Gson gson = new Gson();
                        Operator loaderHapus = gson.fromJson(tvValue.getText().toString(),Operator.class);
                        for (int i = 0; i < loaders.size(); i++) {
                            if(loaders.get(i).getKodeOperator().equalsIgnoreCase(loaderHapus.getKodeOperator())){
                                loaders.remove(i);
                                break;
                            }
                        }
                    }

                    adapterLoader.notifyItemRemoved(position);
                }
            }

            @Override
            public void onLeftClicked(int position) {
                if(adapterLoader.loader.size() < GlobalHelper.MAX_LOADER){
                    for (int i = 0 ; i < rvLoader.getAdapter().getItemCount();i++) {
                        RecyclerView.ViewHolder viewHolder = rvLoader.findViewHolderForAdapterPosition(i);
                        if (viewHolder != null) {
                            View v = viewHolder.itemView;
                            CompleteTextViewHelper etLoader = view.findViewById(R.id.etLoader);
                            TextView tvValue = v.findViewById(R.id.tvValue);

                            if (tvValue.getText().toString().isEmpty()) {
                                Toast.makeText(HarvestApp.getContext(), getActivity().getResources().getString(R.string.chose_loader), Toast.LENGTH_SHORT).show();
                                etLoader.requestFocus();
                                return;
                            }
                        }
                    }
                    adapterLoader.loader.add(new Operator());
                    adapterLoader.notifyItemInserted(adapterLoader.loader.size());
                }else{
                    Toast.makeText(HarvestApp.getContext(), "Max Loader 15", Toast.LENGTH_SHORT).show();
                }
            }
        },adapterLoader);

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(rvLoader);


        if (dynamicParameterPenghasilan.getIsCages() == DynamicParameterPenghasilan.isCagesHide) {
            llCages.setVisibility(View.GONE);
        }

        if (this.cages == null) {
            this.cages = new ArrayList<>();
            this.cages.add(new Cages());
        } else {
            if (this.cages.size() == 0) {
                this.cages = new ArrayList<>();
                this.cages.add(new Cages());
            }
        }

        adapterCages = new CagesInputAdapter(getActivity(), new ArrayList<Cages>(listCages), this.cages);
        adapterCagesPopUp = new SelectCagesAdapter(getActivity(), R.layout.row_setting, new ArrayList<Cages>(listCages));

        rvCages.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvCages.setAdapter(adapterCages);

        swipeControllerCages = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                adapterCages.Cages.remove(position);

                RecyclerView.ViewHolder viewHolder = rvCages.findViewHolderForAdapterPosition(position);
                View v = viewHolder.itemView;
                TextView tvValue = v.findViewById(R.id.tvValue);

                if (!tvValue.getText().toString().isEmpty()) {
                    Gson gson = new Gson();
                    Cages cagesHapus = gson.fromJson(tvValue.getText().toString(), Cages.class);
                    for (int i = 0; i < cages.size(); i++) {

                        Cages cage = cages.get(i);

                        if (cage.getAssetNo() != null && cagesHapus.getAssetNo() != null &&
                                cage.getAssetNo().equalsIgnoreCase(cagesHapus.getAssetNo())) {
                            cages.remove(i);
                            break;
                        }
                    }
                }
                updateHeaderTransaksiSpb();
                adapterCages.notifyItemRemoved(position);
            }

            @Override
            public void onLeftClicked(int position) {

            }
        }, adapterCages);

        ItemTouchHelper itemTouchHelperCages = new ItemTouchHelper(swipeControllerCages);
        itemTouchHelperCages.attachToRecyclerView(rvCages);

//        cbPengirimanGanda.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                try {
//                    if (isChecked) {
//                        rvPengirimanGanda.setVisibility(View.VISIBLE);
//                        JSONArray array = new JSONArray();
//                        Gson gson = new Gson();
//                        for(int i = 0 ; i < pengirimanGandas.size();i++){
//                            array.put(gson.toJson(pengirimanGandas.get(i)));
//                        }
//                        headerTransaksiSPB.getTransaksiAngkut().setPengirimanGandas(pengirimanGandas);
//                    }else{
//                        rvPengirimanGanda.setVisibility(View.GONE);
//                        headerTransaksiSPB.getTransaksiAngkut().setPengirimanGandas(null);
//                    }
//                    updateHeaderTransaksiSpb();
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//            }
//        });

        if(pengirimanGandas == null){
            pengirimanGandas = new ArrayList<>();
            pengirimanGandas.add(new PengirimanGanda(0,TransaksiAngkut.KENDARAAN_KEBUN,new Operator(),new Vehicle()));
        }else{
            if(pengirimanGandas.size() == 0){
                pengirimanGandas = new ArrayList<>();
                pengirimanGandas.add(new PengirimanGanda(0,TransaksiAngkut.KENDARAAN_KEBUN,new Operator(),new Vehicle()));
            }
        }
        adapterPengirimanGanda = new PengirimanGandaAdapater(getActivity(),pengirimanGandas,adapterOperator,adapterVehicle,adapterSpk,adapterLangsiran);
        rvPengirimanGanda.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvPengirimanGanda.setAdapter(adapterPengirimanGanda);

        swipeControllerPengiriman = new SwipeController(new SwipeControllerActions() {
            @Override
            public void onRightClicked(int position) {
                if(adapterPengirimanGanda.pengirimanGandas.size() > 1){
                    adapterPengirimanGanda.pengirimanGandas.remove(position);
                    adapterPengirimanGanda.notifyItemRemoved(position);
                }
            }

            @Override
            public void onLeftClicked(int position) {
                if(adapterPengirimanGanda.pengirimanGandas.size() < 3){
                    for (int i = 0 ; i < rvPengirimanGanda.getAdapter().getItemCount();i++) {
                        RecyclerView.ViewHolder viewHolder = rvPengirimanGanda.findViewHolderForAdapterPosition(i);
                        if (viewHolder != null) {
                            View v = viewHolder.itemView;
                            CompleteTextViewHelper etVehicle = v.findViewById(R.id.etVehicle);
                            CompleteTextViewHelper etDriver = v.findViewById(R.id.etDriver);
                            TextView tvValueUnit = v.findViewById(R.id.tvValueUnit);
                            TextView tvValueOperator = v.findViewById(R.id.tvValueOperator);

                            if (tvValueUnit.getText().toString().isEmpty()) {
                                Toast.makeText(HarvestApp.getContext(), "Kendaraan", Toast.LENGTH_SHORT).show();
                                etVehicle.requestFocus();
                                return;
                            }
                            if (tvValueOperator.getText().toString().isEmpty()) {
                                Toast.makeText(HarvestApp.getContext(), "Operator", Toast.LENGTH_SHORT).show();
                                etDriver.requestFocus();
                                return;
                            }
                        }
                    }

                    adapterPengirimanGanda.pengirimanGandas.add(new PengirimanGanda(0,TransaksiAngkut.KENDARAAN_KEBUN,new Operator(),new Vehicle()));
                    adapterPengirimanGanda.notifyItemInserted(adapterPengirimanGanda.pengirimanGandas.size());
                }
            }
        },adapterPengirimanGanda);

        ItemTouchHelper itemTouchhelperPengiriman = new ItemTouchHelper(swipeControllerPengiriman);
        itemTouchhelperPengiriman.attachToRecyclerView(rvPengirimanGanda);

        lShowHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lShowHideIsi.getVisibility() == View.GONE){
                    lShowHideIsi.setVisibility(View.VISIBLE);
                    lHide.setVisibility(View.VISIBLE);
                    btnDone.setVisibility(View.VISIBLE);
                    lShow.setVisibility(View.GONE);
                }else{
                    lShowHideIsi.setVisibility(View.GONE);
                    lHide.setVisibility(View.GONE);
                    btnDone.setVisibility(View.GONE);
                    lShow.setVisibility(View.VISIBLE);
                }
            }
        });

//        etBin.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                headerTransaksiSPB.getTransaksiAngkut().setBin(s.toString());
//                updateHeaderTransaksiSpb();
//            }
//        });

        lsKendaraan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                headerTransaksiSPB.getTransaksiAngkut().setKendaraanDari(i);
                if(i == TransaksiAngkut.KENDARAAN_KEBUN){
                    if(vehicles.size() > 0 && operators.size() > 0) {
                        etDriver.setAdapter(adapterOperator);
                        etDriver.setThreshold(1);
                        adapterOperator.notifyDataSetChanged();
                        setOperator((Operator) GlobalHelper.dataOperator.values().toArray()[0]);

                        etVehicle.setAdapter(adapterVehicle);
                        etVehicle.setThreshold(1);
                        adapterVehicle.notifyDataSetChanged();
                        Vehicle vehicle = (Vehicle) GlobalHelper.dataVehicle.values().toArray()[0];
                        vehicle.setInputType(Vehicle.inputTypeMaster);
                        setVehicle(vehicle);
                        tvUnit.setText("Unit");
                        lSPK.setVisibility(View.GONE);
                        driverScanQr.setVisibility(View.VISIBLE);
                    }else{
                        headerTransaksiSPB.getTransaksiAngkut().setKendaraanDari(TransaksiAngkut.KENDARAAN_LUAR);
                        lsKendaraan.setText(bKendaraan.get(TransaksiAngkut.KENDARAAN_LUAR));
                        tvUnit.setText("Nomor Plat");
                        lSPK.setVisibility(View.VISIBLE);
                        driverScanQr.setVisibility(View.GONE);
                        Toast.makeText(HarvestApp.getContext(),"Hanya Bisa Pilih Kontrak Karena TABEL Operator u Vehicle Kosong",Toast.LENGTH_SHORT).show();
                    }
                }else if(i == TransaksiAngkut.KENDARAAN_LUAR){
                    etDriver.setText("");
                    etDriver.setAdapter(null);
                    etDriver.setThreshold(0);
                    setOperator(new Operator());

                    etVehicle.setText("");
                    etVehicle.setAdapter(null);
                    etVehicle.setThreshold(0);
                    setVehicle(new Vehicle());
                    tvUnit.setText("Nomor Plat");
                    lSPK.setVisibility(View.VISIBLE);
                    driverScanQr.setVisibility(View.GONE);
                }
            }
        });

        lsTujuan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){
                    headerTransaksiSPB.getTransaksiAngkut().setTujuan(i);

                    lPks.setVisibility(View.VISIBLE);
                    lKeluar.setVisibility(View.GONE);
                    tvPks.setAdapter(adapterPks);
                    tvPks.setThreshold(1);
                    adapterPks.notifyDataSetChanged();
                    if(baseActivity.currentLocationOK()){
                        PKS pksX = null;
                        double dis = 0.0;
                        if(GlobalHelper.dataAllPks.values().size() > 0 ) {
                            for (Map.Entry<String, PKS> e : GlobalHelper.dataAllPks.entrySet()) {
                                PKS value = e.getValue();
                                if (pksX == null) {
                                    pksX = value;
                                    dis = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(), pksX.getLatitude(),
                                            baseActivity.currentlocation.getLongitude(), pksX.getLongitude());
                                } else {
                                    double disX = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(), value.getLatitude(),
                                            baseActivity.currentlocation.getLongitude(), value.getLongitude());

                                    if (dis >= disX) {
                                        dis = disX;
                                        pksX = value;
                                    }
                                }
                            }
                        }
                        if(pksX == null){
                            Toast.makeText(HarvestApp.getContext(),"Tidak Ada Master Pks Di " + GlobalHelper.getEstate().getEstCode() + " - " + GlobalHelper.getEstate().getEstName(),Toast.LENGTH_LONG);
                            lsTujuan.setText(bAlas.get(1));
                            lPks.setVisibility(View.GONE);
                            lKeluar.setVisibility(View.VISIBLE);
                            selectedPks = null;
                            return;
                        }
                        setPks(pksX);
                    }else{
//                        mekanisasiActivity.searchingGPS = Snackbar.make(mekanisasiActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//                        mekanisasiActivity.searchingGPS.show();
                    }

                }else if (i == 1){
                    headerTransaksiSPB.getTransaksiAngkut().setTujuan(i);
                    lPks.setVisibility(View.GONE);
                    lKeluar.setVisibility(View.VISIBLE);
                    selectedPks = null;
                    updateHeaderTransaksiSpb();
                }
            }
        });

        etVehicle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etVehicle.showDropDown();
            }
        });

        etVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etVehicle.setText("");
                etVehicle.showDropDown();
            }
        });

        etVehicle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Vehicle vehicle = ((SelectVehicleAdapter) parent.getAdapter()).getItemAt(position);
                vehicle.setInputType(Vehicle.inputTypeMaster);
                setVehicle(vehicle);
            }
        });

        etVehicle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(lsKendaraan.getText().toString().equalsIgnoreCase(TransaksiAngkut.LUAR)){
                    Vehicle vehicle = new Vehicle();
                    vehicle.setVehicleCode(etVehicle.getText().toString());
                    vehicle.setInputType(Vehicle.inputTypeNoMaster);
                    setVehicle(vehicle);
                }
            }
        });

//        etDriver.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean b) {
//                if(b)
//                    etDriver.showDropDown();
//            }
//        });

        etDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etDriver.setText("");
                etDriver.showDropDown();
            }
        });

        etDriver.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Operator operator = ((SelectOperatorAdapter) parent.getAdapter()).getItemAt(position);
                setOperator(operator);
            }
        });

        etDriver.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(lsKendaraan.getText().toString().equalsIgnoreCase(TransaksiAngkut.LUAR)){
                    Operator operator = new Operator();
                    operator.setNama(etDriver.getText().toString());
                    setOperator(operator);
                }
            }
        });

        tvPks.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    tvPks.showDropDown();
            }
        });

        tvPks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPks.setText("");
                tvPks.showDropDown();
            }
        });

        tvPks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PKS pks = ((SelectPksAdapter) parent.getAdapter()).getItemAt(position);
                setPks(pks);
            }
        });

        etApproveKeluar.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etApproveKeluar.showDropDown();
            }
        });

        etApproveKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etApproveKeluar.setText("");
                etApproveKeluar.showDropDown();
            }
        });

        etApproveKeluar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = ((SelectUserAdapter) parent.getAdapter()).getItemAt(position);
                etApproveKeluar.setText(user.getUserFullName());
                approveBy = user;

                updateHeaderTransaksiSpb();
            }
        });

        etApprove.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etApprove.showDropDown();
            }
        });

        etApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etApprove.setText("");
                etApprove.showDropDown();
            }
        });

        etApprove.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = ((SelectUserAdapter) parent.getAdapter()).getItemAt(position);
                etApprove.setText(user.getUserFullName());
                approveBy = user;

                updateHeaderTransaksiSpb();
            }
        });

        etSpk.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etSpk.showDropDown();
            }
        });

        etSpk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSpk.setText("");
                tvOa.setText("");
                etSpk.showDropDown();
            }
        });

        etSpk.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SPK spk = ((SelectSpkAdapter) parent.getAdapter()).getItemAt(position);
                setSpk(spk);
            }
        });

        etSpk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().isEmpty()){
                    spkUse = null;
                    updateHeaderTransaksiSpb();
                }
            }
        });

        cbKraniSubstitute.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etSubstitute.setVisibility(View.VISIBLE);
                }else{
                    etSubstitute.setVisibility(View.GONE);
                    substituteSelected = null;
                    headerTransaksiSPB.setSubstitute(null);
                }

                updateHeaderTransaksiSpb();
            }
        });

        etSubstitute.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etSubstitute.showDropDown();
            }
        });

        etSubstitute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSubstitute.setText("");
                etSubstitute.showDropDown();
            }
        });

        etSubstitute.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                substituteSelected = ((SelectSupervisionAdapter) parent.getAdapter()).getItemAt(position);
                etSubstitute.setText(substituteSelected.getNama());

                updateHeaderTransaksiSpb();
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeAngkut();
            }
        });

        newMekanisasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_container, MekanisasiPanenInputFragment.getInstance())
                        .commit();
            }
        });

        newTransaksiPanen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_container, TphInputFragment.getInstance(1))
//                        .addToBackStack(null)
                        .commit();
            }
        });

        driverScanQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), BarcodeScannerActivity.class);
                intent.putExtra("requestCode", BarcodeScannerActivity.STATUS_LONG_SCAN_VEHICLE_REQUEST);
                startActivityForResult(intent, BarcodeScannerActivity.STATUS_LONG_SCAN_VEHICLE_REQUEST);
            }
        });


        ivScanQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (adapterCages != null && adapterCages.Cages != null && adapterCages.Cages.size() >= GlobalHelper.MAX_CAGES) {
                    Toast.makeText(HarvestApp.getContext(), "Max Cages 2", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (dynamicParameterPenghasilan.getIsCages() == DynamicParameterPenghasilan.isCagesShow) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Konfirmasi")
                            .setMessage("Apakah Anda yakin ingin scan QR?")
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(getActivity(), BarcodeScannerActivity.class);
                                    intent.putExtra("requestCode", BarcodeScannerActivity.STATUS_LONG_SCAN_CAGES_REQUEST);
                                    startActivityForResult(intent, BarcodeScannerActivity.STATUS_LONG_SCAN_CAGES_REQUEST);
                                }
                            })
                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    showAddCagesPopup("", "Silahkan Input Cages");
                                }
                            });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }else if (dynamicParameterPenghasilan.getIsCages() == DynamicParameterPenghasilan.isCagesQrOnly) {
                    Intent intent = new Intent(getActivity(), BarcodeScannerActivity.class);
                    intent.putExtra("requestCode", BarcodeScannerActivity.STATUS_LONG_SCAN_CAGES_REQUEST);
                    startActivityForResult(intent, BarcodeScannerActivity.STATUS_LONG_SCAN_CAGES_REQUEST);
                }
            }
        });
        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });

        new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == BarcodeScannerActivity.STATUS_LONG_SCAN_CAGES_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            if (adapterCages != null && adapterCages.Cages != null && adapterCages.Cages.size() >= GlobalHelper.MAX_CAGES) {
                Toast.makeText(HarvestApp.getContext(), "Max Cages 2", Toast.LENGTH_SHORT).show();
                return;
            }
            String scanResult = data.getStringExtra(BarcodeScannerActivity.SCAN_RESULT);
            Cages cagesSelected = null;

            if (!masterCages.isEmpty()) {
                for (Cages cages1 : masterCages.values()) {
                    if (cages1.getGenerateId() != null && cages1.getGenerateId().equalsIgnoreCase(scanResult)) {
                        cagesSelected = cages1;
                        cagesSelected.setInputType(Cages.inputTypeQr);
                        break;
                    }
                }
            }

            if (cagesSelected != null) {
                Log.d(TAG, "onActivityResult: " + cagesSelected);
                if (cagesSelected.getAssetNo() != null) {
                    boolean isAlreadyExist = false;

                    for (Cages existingCages : cages) {
                        if (existingCages.getAssetNo() != null && cagesSelected.getAssetNo() != null
                                && existingCages.getAssetNo().equals(cagesSelected.getAssetNo())) {
                            isAlreadyExist = true;
                            break;
                        }
                    }

                    ArrayList<Cages> cagesX = new ArrayList<>();
                    for(int i = 0 ; i < cages.size(); i++){
                        Cages c = cages.get(i);
                        if(c.getAssetNo() != null ){
                            if(!c.getAssetNo().isEmpty()){
                                cagesX.add(c);
                            }
                        }
                    }


                    if (!isAlreadyExist) {
                        cages = cagesX;
                        cages.add(cagesSelected);
                        updateRecyclerView();

                        headerTransaksiSPB.getTransaksiAngkut().setCages(cages);
                        updateHeaderTransaksiSpb();
                    }
                }
            } else {
                Toast.makeText(HarvestApp.getContext(), "QR Cages tidak ditemukan", Toast.LENGTH_SHORT).show();
                showAddCagesPopup(scanResult, "Silahkan Input Cages");
            }
        }

        if (requestCode == BarcodeScannerActivity.STATUS_LONG_SCAN_VEHICLE_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            Vehicle vehicleSelected = null;

            String scanResult = data.getStringExtra(BarcodeScannerActivity.SCAN_RESULT);

            if (!masterVehicle.isEmpty()) {
                for (Vehicle vehicles : masterVehicle.values()) {
                    if (vehicles.getQRCode() != null && vehicles.getQRCode().equalsIgnoreCase(scanResult)) {
                        vehicleSelected = vehicles;
                        break;
                    }
                    Gson gson = new Gson();
                    Log.d(TAG, "onActivityResult: allvehicle" +gson.toJson(vehicles) );

                }
            }

            if (vehicleSelected != null){
                vehicleSelected.setInputType(Vehicle.inputTypeQr);
                setVehicle(vehicleSelected);
            }else {
                Toast.makeText(HarvestApp.getContext(), "QR Vehicle tidak ditemukan", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void updateRecyclerView() {
        adapterCages = new CagesInputAdapter(getActivity(), new ArrayList<>(masterCages.values()), cages);
        rvCages.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvCages.setAdapter(adapterCages);
        adapterCages.notifyDataSetChanged();
    }

    private void showAddCagesPopup(String scanResult, String title) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title);

        LayoutInflater layoutInflater = getLayoutInflater();
        View dialogView = layoutInflater.inflate(R.layout.cages_layout, null);

        RadioGroup rgSelectInputCages = dialogView.findViewById(R.id.rgSelectInputCages);
        RadioButton rbMaster = dialogView.findViewById(R.id.rbMaster);
        RadioButton rbFreeText = dialogView.findViewById(R.id.rbFreeText);
        CompleteTextViewHelper assetNo = dialogView.findViewById(R.id.assetNo);
        TextView cagesName = dialogView.findViewById(R.id.cagesNames);

        rbFreeText.setVisibility(View.GONE);

        selectedCagesPopUp = new Cages();
        final String[] selectedInputType = {TransaksiAngkut.MASTER};

        Collection<Cages> listCages = masterCages.values();
        ArrayList<Cages> listOfCages = new ArrayList<>(listCages);
        adapterCagesPopUp = new SelectCagesAdapter(getActivity(), R.layout.row_setting, new ArrayList<>(listOfCages));

        rbMaster.setChecked(true);

        rgSelectInputCages.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.rbMaster ) {
                selectedInputType[0] = TransaksiAngkut.MASTER;
                assetNo.setAdapter(adapterCagesPopUp);
                assetNo.setThreshold(1);
                assetNo.setOnFocusChangeListener((v, hasFocus) -> {
                    if (hasFocus){
                        assetNo.showDropDown();
                    }
                });
                assetNo.setOnClickListener(view -> assetNo.showDropDown());
            } else if (checkedId == R.id.rbFreeText){
                selectedInputType[0] = TransaksiAngkut.FREE_TEXT;
                assetNo.setAdapter(null);
                assetNo.setThreshold(0);
                assetNo.setOnFocusChangeListener(null);
                assetNo.setOnClickListener(null);

                if (initialDropdownValue != null){
                    selectedCagesPopUp.setGenerateId(initialDropdownValue);
                }
            }
        });

    /*    lsSelectInputCages.setOnItemClickListener((parent, view, position, id) -> {
            selectedInputType[0] = bCages.get(position);
            if (selectedInputType[0].equals(TransaksiAngkut.MASTER)) {
                assetNo.setAdapter(adapterCagesPopUp);
                assetNo.setThreshold(1);
                assetNo.setOnFocusChangeListener((v, hasFocus) -> {
                    if (hasFocus) {
                        assetNo.showDropDown();
                    }
                });
                assetNo.setOnClickListener(v -> assetNo.showDropDown());
            } else if (selectedInputType[0].equals(TransaksiAngkut.FREE_TEXT)) {
                assetNo.setAdapter(null);
                assetNo.setThreshold(0);
                assetNo.setOnFocusChangeListener(null);
                assetNo.setOnClickListener(null);

                if (initialDropdownValue != null) {
                    selectedCagesPopUp.setGenerateId(initialDropdownValue);
                }
            }
        });*/

        if (!listOfCages.isEmpty()) {
            for (Cages cagesX : listOfCages) {
                if (cagesX.getGenerateId().toLowerCase().equals(scanResult)) {
                    selectedCagesPopUp = cagesX;
                    selectedCagesPopUp.setInputType(Cages.inputTypeQr);
                    assetNo.setText(scanResult);
                    assetNo.setAdapter(adapterCagesPopUp);
                    assetNo.setThreshold(1);
                }
            }
        }

        //assetNo.setText(scanResult);
        assetNo.setAdapter(adapterCagesPopUp);
        assetNo.setThreshold(1);

        assetNo.setOnItemClickListener((parent, view, position, id) -> {
            Cages cages1 = ((SelectCagesAdapter) parent.getAdapter()).getItemAt(position);
            cagesName.setText(cages1.getAssetName());
            assetNo.setText(cages1.getAssetNo());
            selectedCagesPopUp = cages1;
            selectedCagesPopUp.setInputType(Cages.inputTypeMaster);

            initialDropdownValue = cages1.getAssetNo();
            assetNo.setTag("selected");
        });

        assetNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                assetNo.setTag(null);
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        });

        alertDialog.setView(dialogView);

        // Cancel button
        alertDialog.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        alertDialog.setPositiveButton("Save", null);
        AlertDialog dialog = alertDialog.create();

        dialog.setOnShowListener(d -> {
            Button saveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            saveButton.setOnClickListener(v -> {
                if (selectedInputType[0] == null) {
                    Toast.makeText(getActivity(), "Tolong pilih tipe input", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TransaksiAngkut.MASTER.equals(selectedInputType[0])) {
                    if ("selected".equals(assetNo.getTag())) {
                        saveCages();
                        dialog.dismiss();
                    } else {
                        Toast.makeText(getActivity(), "Tolong pilih cages yang benar", Toast.LENGTH_SHORT).show();
                    }
                } else if (TransaksiAngkut.FREE_TEXT.equals(selectedInputType[0])) {
                    if (assetNo.getText().toString().isEmpty()) {
                        Toast.makeText(getActivity(), "Tolong isi cages", Toast.LENGTH_SHORT).show();
                    } else {
                        selectedCagesPopUp.setAssetNo(assetNo.getText().toString());
                        selectedCagesPopUp.setAssetName(assetNo.getText().toString());
                        selectedCagesPopUp.setInputType(Cages.inputTypeNoMaster);
                        saveCages();
                        dialog.dismiss();
                    }
                }
            });
        });

        dialog.show();
    }

    private void saveCages() {
        boolean isAlreadyExist = false;

        for (Cages existingCages : cages) {
            if (existingCages.getAssetNo() != null && selectedCagesPopUp.getAssetNo() != null
                    && existingCages.getAssetNo().equals(selectedCagesPopUp.getAssetNo())) {
                isAlreadyExist = true;
                break;
            }
        }

        ArrayList<Cages> cagesX = new ArrayList<>();
        for (Cages c : cages) {
            if (c.getAssetNo() != null && !c.getAssetNo().isEmpty()) {
                cagesX.add(c);
            }
        }

        if (!isAlreadyExist) {
            cages = cagesX;
            cages.add(selectedCagesPopUp);
            updateRecyclerView();

            headerTransaksiSPB.getTransaksiAngkut().setCages(cages);
            updateHeaderTransaksiSpb();
        }
    }

    public void pilihIdTpanen(String id ){
        Gson gson = new Gson();
        idPanenSelected = id;
        Log.d(TAG, "pilihIdTPANEN: "+ idPanenSelected);
        Log.d(TAG, "selectedTransaksiSPB: "+ gson.toJson(selectedTransaksiSPB));

        if(selectedTransaksiSPB == null){
            nextidTPanenStep();
        }else if(selectedTransaksiSPB.getStatus() == TransaksiSPB.BELUM_UPLOAD || selectedTransaksiSPB.getStatus() == TransaksiSPB.BELUM_CLOSE){
            nextidTPanenStep();
        }else{
            Toast.makeText(HarvestApp.getContext(),"Angkut Tidak Bisa DiHapus",Toast.LENGTH_SHORT).show();
            new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
        }
    }

    private void nextidTPanenStep(){
        if(idPanenSelected.toLowerCase().contains("-angkut")){
            if(idPanenSelected.toLowerCase().contains("ah")){
                Toast.makeText(HarvestApp.getContext(),"Hasil dari tap kartu tidak bisa di edit",Toast.LENGTH_SHORT).show();
                return;
            }else{
                AlertDialog alertDialog = new AlertDialog.Builder(mekanisasiActivity, R.style.MyAlertDialogStyle)
                        .setTitle("Perhatian")
                        .setMessage("Apakah Anda Yakin Untuk Hapus Data Kartu Hilang dan Kartu Rusak Ini")
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                hapusAngkut(idPanenSelected);
                            }
                        })
                        .create();
                alertDialog.show();
            }
        }else{
            new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_DETAIL));
        }
    }

    public void hapusAngkut(String id ){
        idHapusAngkut = id;
        if(selectedTransaksiSPB == null){
            new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_DELETED_ANGKUT));
        }else if(selectedTransaksiSPB.getStatus() == TransaksiSPB.BELUM_UPLOAD){
            new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_DELETED_ANGKUT));
        }
    }

    public void updateLoader(){
        loaders = new ArrayList<>();
        JSONArray loaderArray = new JSONArray();
        for (int i = 0; i < rvLoader.getAdapter().getItemCount(); i++) {
            RecyclerView.ViewHolder viewHolder = rvLoader.findViewHolderForAdapterPosition(i);
            if (viewHolder != null) {
                Gson gson = new Gson();
                View v = viewHolder.itemView;
                TextView tvValue = v.findViewById(R.id.tvValue);
                if (!tvValue.getText().toString().isEmpty()) {
                    loaders.add(gson.fromJson(tvValue.getText().toString(), Operator.class));
                }
            }
        }
        headerTransaksiSPB.getTransaksiAngkut().setLoader(loaders);
        updateHeaderTransaksiSpb();
    }

    public void updatePengirimanGanda(){
        pengirimanGandas = new ArrayList<>();
        for(int i = 0 ;i< rvPengirimanGanda.getAdapter().getItemCount();i++){
            RecyclerView.ViewHolder viewHolder = rvPengirimanGanda.findViewHolderForAdapterPosition(i);
            if (viewHolder != null) {
                Gson gson = new Gson();
                View v = viewHolder.itemView;
                BetterSpinner lsKendaraan = v.findViewById(R.id.lsKendaraan);
                TextView tvValueUnit = v.findViewById(R.id.tvValueUnit);
                TextView tvValueOperator = v.findViewById(R.id.tvValueOperator);
                TextView tvValueSpk = v.findViewById(R.id.tvValueSpk);
                TextView tvValueLangsiran = v.findViewById(R.id.tvValueLangsiran);
                int kendaraanDari = 0;
                SPK spk = new SPK();
                switch (lsKendaraan.getText().toString().toUpperCase()){
                    case TransaksiAngkut.KEBUN:
                        kendaraanDari = TransaksiAngkut.KENDARAAN_KEBUN;
                        break;
                    case TransaksiAngkut.LUAR:
                        kendaraanDari = TransaksiAngkut.KENDARAAN_LUAR;
                        spk = gson.fromJson(tvValueSpk.getText().toString(),SPK.class);
                        break;
                }
                PengirimanGanda ganda = new PengirimanGanda(i,
                        kendaraanDari,
                        gson.fromJson(tvValueOperator.getText().toString(),Operator.class),
                        gson.fromJson(tvValueUnit.getText().toString(),Vehicle.class)
                );

                ganda.setSpk(spk);

                if(!tvValueLangsiran.getText().toString().isEmpty()){
                    Langsiran langsiran = gson.fromJson(tvValueLangsiran.getText().toString(),Langsiran.class);
                    ganda.setLangsiran(langsiran);
                }

                pengirimanGandas.add(ganda);
            }
        }
        headerTransaksiSPB.getTransaksiAngkut().setPengirimanGandas(pengirimanGandas);
        updateHeaderTransaksiSpb();
    }


    private void takePicture() {
        if(stampImage == null){
            stampImage = new StampImage();
        }
        if (selectedPks != null) {
            stampImage.setPks(selectedPks);
            stampImage.setBlock(selectedPks.getBlock());
        }

        if (selectedOperator != null) {
            stampImage.setOperator(selectedOperator);
        } else {
            Operator X = new Operator();
            X.setNama(etDriver.getText().toString());
            stampImage.setOperator(X);
        }


        if (vehicleUse != null) {
            stampImage.setVehicle(vehicleUse);
        } else {
            Vehicle X = new Vehicle();
            X.setVehicleName(etVehicle.getText().toString());
            stampImage.setVehicle(X);
        }


        Gson gson  = new Gson();
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(HarvestApp.STAMP_IMAGES,gson.toJson(stampImage));
        editor.apply();

        GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_TRANSAKSI_SPB_MEKANISASI;
        EasyImage.openCamera(getActivity(),1);
    }

    public void addImages(File newImages){
        try {
            boolean pernahAda = false;
            if (headerTransaksiSPB.getFoto() != null) {
                if(headerTransaksiSPB.getFoto().size() > 0) {
                    pernahAda = true;
                }
            }

            if(pernahAda){
                headerTransaksiSPB.getFoto().add(newImages.getAbsolutePath());
            }else{
                HashSet<String> hFoto = new HashSet<>();
                hFoto.add(newImages.getAbsolutePath());
                headerTransaksiSPB.setFoto(hFoto);
            }

            ALselectedImage.add(newImages);

            Gson gson = new Gson();
            TransaksiSpbHelper.createFileSpb(gson.toJson(headerTransaksiSPB));
            setupimageslide();
        }catch ( Exception e){
            e.printStackTrace();
        }
    }

    private void setupimageslide(){

        Set<File> sf = new HashSet<>();
        sf.addAll(ALselectedImage);
        ALselectedImage.clear();
        ALselectedImage.addAll(sf);

        if(ALselectedImage.size() > 3){
            ivPhoto.setVisibility(View.GONE);
        }else{
            ivPhoto.setVisibility(View.VISIBLE);
        }
        if(ALselectedImage.size() != 0){
//            tphphoto.setVisibility(View.VISIBLE);
            imagecacnel.setVisibility(View.VISIBLE);
            imagesview.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.VISIBLE);
        }else{
//            tphphoto.setVisibility(View.GONE);
            imagecacnel.setVisibility(View.GONE);
            imagesview.setVisibility(View.GONE);
            sliderLayout.setVisibility(View.GONE);
        }
        sliderLayout.removeAllSliders();
        for(File file : ALselectedImage){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            if(file.toString().startsWith("http:")){
                String surl = file.toString();
                if(file.toString().startsWith("http://")){

                }else if(file.toString().startsWith("http:/")){
                    surl =surl.replace("http:/","http://");
                }

                textSliderView
                        .image(GlobalHelper.setUrlFoto(surl))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }else {
                textSliderView
                        .image(file)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        imagecacnel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
                try {
                    if (sliderLayout.getCurrentPosition() == 0 && ALselectedImage.size() == 1) {
                        headerTransaksiSPB.getFoto().remove(ALselectedImage.get(sliderLayout.getCurrentPosition()));
                        ALselectedImage.clear();
                        sliderLayout.removeAllSliders();
                        sliderLayout.setVisibility(View.GONE);
                        imagecacnel.setVisibility(View.GONE);
                        imagesview.setVisibility(View.GONE);
                    } else {
                        String fileSelected = ALselectedImage.get(sliderLayout.getCurrentPosition()).getAbsolutePath();
                        ALselectedImage = new ArrayList();
                        if (headerTransaksiSPB.getFoto() != null) {
                            if (headerTransaksiSPB.getFoto().size() > 0) {
                                for (String file : headerTransaksiSPB.getFoto()) {
                                    if (!file.startsWith("http:")) {
                                        if (fileSelected.equalsIgnoreCase(file)) {
                                            headerTransaksiSPB.getFoto().remove(file);
                                            sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
                                        } else {
                                            ALselectedImage.add(new File(file));
                                        }
                                    }
                                }
                            }
                        }
                    }

                    Gson gson = new Gson();
                    TransaksiSpbHelper.createFileSpb(gson.toJson(headerTransaksiSPB));
                }catch (Exception e){
                    Log.e("sliderLayout", e.toString());
                }
            }
        });
        imagesview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WidgetHelper.showImagesZoom(getActivity(),ALselectedImage);
            }
        });
    }

    public void updateHeaderTransaksiSpb(){
        if(headerTransaksiSPB != null){
            if (selectedOperator != null) {
                headerTransaksiSPB.getTransaksiAngkut().setSupir(selectedOperator);
            }

            if (vehicleUse != null) {
                headerTransaksiSPB.getTransaksiAngkut().setVehicle(vehicleUse);
            }
            if(spkUse != null){
                headerTransaksiSPB.getTransaksiAngkut().setSpk(spkUse);
            }

            if(approveBy != null) {
                headerTransaksiSPB.setApproveBy(approveBy.getUserID());
            }

            if (cages != null && !cages.isEmpty()){
                headerTransaksiSPB.getTransaksiAngkut().setCages(cages);
            }


            headerTransaksiSPB.setSubstitute(substituteSelected);
            headerTransaksiSPB.getTransaksiAngkut().setPks(selectedPks);

            Gson gson = new Gson();
            TransaksiSpbHelper.createFileSpb(gson.toJson(headerTransaksiSPB));
        }
    }

    public void closeAngkut(){
        File fileDBAngkut = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI) + "/" +Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI)+ ".db");

        int angka = 0;
        if(originAngkut.size() > 0 ){
            angka += originAngkut.size();
        }

        if(originAngkutKartu.size() > 0 ){
            angka += originAngkutKartu.size();
        }

        if(angka == 0 ){
            mekanisasiActivity.closeActivity();
        }else if (!fileDBAngkut.exists()){
            mekanisasiActivity.closeActivity();
        }else{
            if(selectedTransaksiSPB != null) {
                if (selectedTransaksiSPB.getStatus() == TransaksiSPB.BELUM_UPLOAD || selectedTransaksiSPB.getStatus() == TransaksiSPB.UPLOAD) {
                    showDialogCloseTransaksiSPB();
                } else {
                    yesnoQuestion();
                }
            }else{
                yesnoQuestion();
            }
        }
    }

    private void yesnoQuestion(){
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.yes_no, null);
        TextView tvDialogKet = (TextView) dialogView.findViewById(R.id.ket1);
        SlideView btnCancelDialog = (SlideView) dialogView.findViewById(R.id.btnCancel);
        SlideView btnOkDialog = (SlideView) dialogView.findViewById(R.id.btnOK);

        final AlertDialog dialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle)
                .setView(dialogView)
                .setCancelable(false)
                .create();
        String mesage = "Tujuan : "+ lsTujuan.getText().toString();
        if(selectedPks != null) {
            mesage += "\nPKS : " + selectedPks.getNamaPKS();
        }
        mesage += "\nApakah Anda Yakin Data Yang Sudah Di Simpan Tidak Bisa Di Edit";

        tvDialogKet.setText(mesage);
        btnOkDialog.setText("Simpan");
        btnCancelDialog.setText("Tidak");

        btnCancelDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                dialog.dismiss();
            }
        });
        btnOkDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                dialog.dismiss();
                showDialogCloseTransaksiSPB();
            }
        });
        dialog.show();
    }

    public void setVehicle(Vehicle vehicle){
        if(vehicle !=null) {
            if(lsKendaraan.getText().toString().equalsIgnoreCase(TransaksiAngkut.KEBUN)) {
                etVehicle.setText(TransaksiAngkutHelper.showVehicle(vehicle));
            }
            vehicleUse = vehicle;
        }else{
            etVehicle.setText("");
            vehicleUse = new Vehicle();
        }

        updateHeaderTransaksiSpb();
    }

    public void setOperator(Operator operator){
        if(operator != null) {
            if(lsKendaraan.getText().toString().equalsIgnoreCase(TransaksiAngkut.KEBUN)) {
                etDriver.setText(operator.getNama());
            }
            selectedOperator = operator;
        }else{
            etDriver.setText("");
            selectedOperator = new Operator();
        }

        updateHeaderTransaksiSpb();
    }

    public void setSpk(SPK spk){
        if(spk != null){
            etSpk.setText(spk.getIdSPK());
            tvOa.setText(TransaksiPanenHelper.showSPK(spk));
            //lSPK.setVisibility((View.VISIBLE);
        }else{
            etSpk.setText("");
            tvOa.setText("");
        }
        spkUse = spk;
        updateHeaderTransaksiSpb();
    }

    public void setPks(PKS pks){
        lPks.setVisibility(View.VISIBLE);
        if(pks != null) {
            tvPks.setText(pks.getNamaPKS());
            selectedPks = pks;
        }else{
            tvPks.setText("");
            selectedPks = new PKS();
        }
        updateHeaderTransaksiSpb();
    }

    public void showDialogCloseTransaksiSPB(){
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.metode_penyimpanan_transaksi,null);
        TextView tv_header = view.findViewById(R.id.tv_header);
        LinearLayout lnfc = view.findViewById(R.id.lnfc);
        LinearLayout lprint = view.findViewById(R.id.lprint);
        LinearLayout lshare = view.findViewById(R.id.lshare);
        Button btnClose = view.findViewById(R.id.btnClose);
        lnfc.setVisibility(View.GONE);
        tv_header.setText(getActivity().getResources().getString(R.string.chose_metode_spb));


        if(mekanisasiActivity.selectedTransaksiSPB == null || selectedTransaksiSPB.getStatus() == TransaksiSPB.BELUM_CLOSE){
            btnClose.setVisibility(View.GONE);
        }

        lprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(GlobalHelper.isDoubleClick()){
                    alertDialog.dismiss();
                    return;
                }
                if (((BaseActivity) getActivity()).bluetoothHelper.mService.getState() == BluetoothService.STATE_CONNECTED) {
                    if(mekanisasiActivity.selectedTransaksiSPB != null
                            && (mekanisasiActivity.selectedTransaksiSPB.getStatus() == TransaksiSPB.UPLOAD || mekanisasiActivity.selectedTransaksiSPB.getStatus() == TransaksiSPB.BELUM_UPLOAD)) {
                        if (selectedTransaksiSPB.getAfdeling() == null) {
                            selectedTransaksiSPB.setAfdeling(distinctAfdeling);
                        }

                        //tambahan revisi selagi update arcgis
                        boolean adaRevisi = false;
                        if(mekanisasiActivity.selectedTransaksiSPB.getStatus() == TransaksiSPB.BELUM_UPLOAD){
                            adaRevisi = cekAdaRevisi();
                        }

                        if(!adaRevisi){
                            new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_PRINT));
                            alertDialog.dismiss();
                        }else{
                            if(mekanisasiActivity.selectedTransaksiSPB.getTransaksiAngkut() != null){
                                mekanisasiActivity.selectedTransaksiSPB.getTransaksiAngkut().setRevisi(true);
                            }
                            if(selectedTransaksiSPB.getTransaksiAngkut() != null){
                                selectedTransaksiSPB.getTransaksiAngkut().setRevisi(true);
                            }
                            if (validasiDeklarasiSPB()) {
                                new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SAVE_DATA_PRINT));
                                alertDialog.dismiss();
                            }
                        }
                    }else {
                        if (validasiDeklarasiSPB()) {
                            new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SAVE_DATA_PRINT));
                            alertDialog.dismiss();
                        }
                    }
                }else{
                    ((BaseActivity) getActivity()).bluetoothHelper.showListBluetoothPrinter();
                }
            }
        });

        lshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(GlobalHelper.isDoubleClick()){
                    alertDialog.dismiss();
                    return;
                }
                if(mekanisasiActivity.selectedTransaksiSPB != null
                        && (mekanisasiActivity.selectedTransaksiSPB.getStatus() == TransaksiSPB.UPLOAD || mekanisasiActivity.selectedTransaksiSPB.getStatus() == TransaksiSPB.BELUM_UPLOAD)
                        ){
                    if(selectedTransaksiSPB.getAfdeling() == null){
                        selectedTransaksiSPB.setAfdeling(distinctAfdeling);
                    }

                    //tambahan revisi selagi update arcgis
                    boolean adaRevisi = false;
                    if(mekanisasiActivity.selectedTransaksiSPB.getStatus() == TransaksiSPB.BELUM_UPLOAD){
                        adaRevisi = cekAdaRevisi();
                    }

                    if(!adaRevisi){
                        new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SHARE));
                    }else{
                        if(mekanisasiActivity.selectedTransaksiSPB.getTransaksiAngkut() != null){
                            mekanisasiActivity.selectedTransaksiSPB.getTransaksiAngkut().setRevisi(true);
                        }
                        if(selectedTransaksiSPB.getTransaksiAngkut() != null){
                            selectedTransaksiSPB.getTransaksiAngkut().setRevisi(true);
                        }
                        if (validasiDeklarasiSPB()) {
                            new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SAVE_DATA_SHARE));
                        }
                    }
                }else {
                    if (validasiDeklarasiSPB()) {
                        new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_SAVE_DATA_SHARE));
                    }
                }
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mekanisasiActivity.closeActivity();
            }
        });

        alertDialog = WidgetHelper.showListReference(alertDialog,view,getActivity());
    }

    private boolean cekAdaRevisi(){
        TransaksiAngkut transaksiAngkutX = null;
        Vehicle vehicleX = null;

        if(mekanisasiActivity.selectedTransaksiSPB != null){
            if(mekanisasiActivity.selectedTransaksiSPB.getTransaksiAngkut() != null){
                transaksiAngkutX = mekanisasiActivity.selectedTransaksiSPB.getTransaksiAngkut();
            }
        }

        if(transaksiAngkutX.getVehicle() != null){
            vehicleX = transaksiAngkutX.getVehicle();
        }

        if(vehicleUse != null && vehicleX != null){
            if(!vehicleX.getVehicleCode().toUpperCase().equals(vehicleUse.getVehicleCode().toUpperCase())){
                Log.d(TAG, "cekAdaRevisi: Vehicle");
                return true;
            }
        }

        Operator supirX = null;
        if(transaksiAngkutX.getSupir() != null){
            supirX = transaksiAngkutX.getSupir();
        }

        if(selectedOperator != null && supirX != null){
            if(!supirX.getNama().toUpperCase().equals(selectedOperator.getNama().toUpperCase())){
                Log.d(TAG, "cekAdaRevisi: Operator");
                return true;
            }
        }

        if(spkUse != null && transaksiAngkutX.getSpk() != null){
            if(!spkUse.getIdSPK().toUpperCase().equals(transaksiAngkutX.getSpk().getIdSPK().toUpperCase())){
                Log.d(TAG, "cekAdaRevisi: SPK");
                return true;
            }
        }else if (spkUse == null && transaksiAngkutX.getSpk() != null){
            Log.d(TAG, "cekAdaRevisi: SPK 1");
            return true;
        }else if (spkUse != null && transaksiAngkutX.getSpk() == null){
            Log.d(TAG, "cekAdaRevisi: SPK 2");
            return true;
        }

        if(selectedPks != null && transaksiAngkutX.getPks() != null){
            if(!selectedPks.getIdPKS().toLowerCase().equals(transaksiAngkutX.getPks().getIdPKS().toLowerCase())){
                Log.d(TAG, "cekAdaRevisi: PKS");
                return true;
            }
        }else if (selectedPks == null && transaksiAngkutX.getPks() != null){
            return true;
        }else if (selectedPks != null && transaksiAngkutX.getPks() == null){
            return true;
        }

        if(cages != null && transaksiAngkutX.getCages() != null){
            ArrayList<Cages> cages1 = new ArrayList<>();
            if(cages != null){
                for(int i = 0 ; i < cages.size(); i++){
                    Cages c = cages.get(i);
                    if(c.getAssetNo() != null){
                        cages1.add(c);
                    }
                }
            }
            if(cages1.size() != transaksiAngkutX.getCages().size()){
                Log.d(TAG, "cekAdaRevisi: Cages");
                return true;
            }
        }

        return false;
    }

    private boolean validasiDeklarasiSPB(){
        if(baseActivity.currentLocationOK()){
            latitude = baseActivity.currentlocation.getLatitude();
            longitude = baseActivity.currentlocation.getLongitude();
        }else{
            WidgetHelper.warningFindGps(mekanisasiActivity,mekanisasiActivity.myCoordinatorLayout);
            return false;
        }

        boolean rbValid = false;
        if(rbTransitPoint.isChecked()){
            rbValid = true;
        }else if (rbPKS.isChecked()){
            rbValid = true;
        }

        if(!rbValid){
            Toast.makeText(HarvestApp.getContext(), "Mohon tentukan jenis pengiriman", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(etVehicle.getText().toString().isEmpty()){
            etVehicle.requestFocus();
            Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.plase_input_vehicle),Toast.LENGTH_SHORT).show();
            return false;
        }

        if(etDriver.getText().toString().isEmpty()){
            etDriver.requestFocus();
            Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.plase_input_driver),Toast.LENGTH_SHORT).show();
            return false;
        }

        if(originAngkut.size() == 0 && originAngkutKartu.size() == 0){
            Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.mohon_angkut), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (lsTujuan.getText().toString().equalsIgnoreCase(TransaksiAngkut.PKS) && selectedPks == null) {
            Toast.makeText(HarvestApp.getContext(), getResources().getString(R.string.chose_pks), Toast.LENGTH_SHORT).show();
            return false;
        }

        boolean adaMekanisasi = false;
        if(originAngkut.size() > 0) {
            for (TransaksiPanen transaksiPanen : originAngkut.values()) {
                if (transaksiPanen.getIdTPanen() != null) {
                    adaMekanisasi = true;
                    break;
                }
            }
        }

        if(adaMekanisasi){
            if(ALselectedImage.size() == 0){
                Toast.makeText(HarvestApp.getContext(),"Mohon Foto Tiket Panen Quik",Toast.LENGTH_SHORT).show();
                takePicture();
                return false;
            }
        }

        Set<String> distinctCages = new HashSet<>();
        Set<String> distincOperator = new HashSet<>();
        Set<String> distincVehicle = new HashSet<>();
        Set<String> distincPengirimanGandaSPK = new HashSet<>();
        Set<String> distincPengirimanGandaLangsiran = new HashSet<>();
        ArrayList<String> arrayListOperator = new ArrayList<>();
        ArrayList<String> arrayListCages = new ArrayList<>();
        ArrayList<String> arrayListVehicle = new ArrayList<>();
        ArrayList<String> arrayListSpk = new ArrayList<>();
        ArrayList<String> arrayListLangsiran = new ArrayList<>();


        if(selectedOperator == null){
            Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Supir",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            distincOperator.add(selectedOperator.getKodeOperator());
            arrayListOperator.add(selectedOperator.getKodeOperator());
        }

        if(vehicleUse == null){
            Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Vehicle",Toast.LENGTH_SHORT).show();
            return false;
        }else{
            distincVehicle.add(TransaksiAngkutHelper.showVehicle(vehicleUse));
            arrayListVehicle.add(TransaksiAngkutHelper.showVehicle(vehicleUse));
        }

        if(spkUse != null){
            distincPengirimanGandaSPK.add(spkUse.getIdSPK());
            arrayListSpk.add(spkUse.getIdSPK());
        }

        if(!etSpk.getText().toString().trim().isEmpty()){
            if(spkUse == null){
                Toast.makeText(HarvestApp.getContext(), "Mohon pilih SPK", Toast.LENGTH_LONG).show();
                return false;
            }else if(! spkUse.getIdSPK().trim().toUpperCase().equals(etSpk.getText().toString().trim().toUpperCase())){
                Toast.makeText(HarvestApp.getContext(), "Mohon pilih SPK dengan benar", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if(lsKendaraan.getText().toString().toUpperCase().equals(TransaksiAngkut.KEBUN.toUpperCase())){
            spkUse = null;

//            if(spkUse != null){
//                if(spkUse.getIdSPK() != null){
//                    if(!etSpk.getText().toString().trim().isEmpty()) {
//                        Toast.makeText(HarvestApp.getContext(), "Jika SPK Maka tidak boleh  kepemilikan sendiri", Toast.LENGTH_LONG).show();
//                        return false;
//                    }
//                }
//            }
        }

        if(lsKendaraan.getText().toString().toUpperCase().equals(TransaksiAngkut.LUAR.toUpperCase())){
            if(!TransaksiAngkutHelper.cekPlatno(etVehicle.getText().toString())){
                Toast.makeText(HarvestApp.getContext(), "Input vehicle dengan plat no contoh AA1234XXX", Toast.LENGTH_LONG).show();
                etVehicle.setFocusable(true);
                return false;
            }
            //nant jika oa sudah di pakai semua maka ini diunculkan
            else if(spkUse == null){
                Toast.makeText(HarvestApp.getContext(), "Mohon pilih OA ", Toast.LENGTH_LONG).show();
                return false;
            }else if(! spkUse.getIdSPK().trim().toUpperCase().equals(etSpk.getText().toString().trim().toUpperCase())){
                Toast.makeText(HarvestApp.getContext(), "Mohon pilih OA", Toast.LENGTH_LONG).show();
                return false;
            }
        }

//        if(cbPengirimanGanda.isChecked()) {
        if(rbTransitPoint.isChecked()) {
            pengirimanGandaSave = new HashMap<>();
            ArrayList<SPK> pengirimanGandaSPk = new ArrayList<>();
            ArrayList<Langsiran> pengirimanGandaLangsiran = new ArrayList<>();
            for (int i = 0; i < rvPengirimanGanda.getAdapter().getItemCount(); i++) {
                RecyclerView.ViewHolder viewHolder = rvPengirimanGanda.findViewHolderForAdapterPosition(i);
                if (viewHolder != null) {
                    View v = viewHolder.itemView;
                    BetterSpinner lsKendaraan = v.findViewById(R.id.lsKendaraan);
                    CompleteTextViewHelper etVehicle = v.findViewById(R.id.etVehicle);
                    CompleteTextViewHelper etDriver = v.findViewById(R.id.etDriver);
                    TextView tvValueUnit = v.findViewById(R.id.tvValueUnit);
                    TextView tvValueOperator = v.findViewById(R.id.tvValueOperator);
                    TextView tvValueSpk = v.findViewById(R.id.tvValueSpk);
                    TextView tvValueLangsiran = v.findViewById(R.id.tvValueLangsiran);

                    if (tvValueUnit.getText().toString().isEmpty()) {
                        etVehicle.requestFocus();
                        Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Vehicle", Toast.LENGTH_SHORT).show();
                        return false;
                    } else if (tvValueOperator.getText().toString().isEmpty()) {
                        etDriver.requestFocus();
                        Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Operator", Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    Gson gson = new Gson();
                    Vehicle vehicle = gson.fromJson(tvValueUnit.getText().toString(), Vehicle.class);
                    Operator operator = gson.fromJson(tvValueOperator.getText().toString(), Operator.class);
                    SPK spk = null;
                    Langsiran langsiran = null;

                    distincVehicle.add(TransaksiAngkutHelper.showVehicle(vehicle));
                    arrayListVehicle.add(TransaksiAngkutHelper.showVehicle(vehicle));

                    int kendaraanDari = 0;
                    switch (lsKendaraan.getText().toString().toUpperCase()) {
                        case TransaksiAngkut.KEBUN:
                            distincOperator.add(operator.getKodeOperator());
                            arrayListOperator.add(operator.getKodeOperator());
                            kendaraanDari = TransaksiAngkut.KENDARAAN_KEBUN;
                            break;
                        case TransaksiAngkut.LUAR:
                            kendaraanDari = TransaksiAngkut.KENDARAAN_LUAR;
                            distincOperator.add(operator.getNama());
                            arrayListOperator.add(operator.getNama());
                            break;
                    }

                    if(!tvValueSpk.getText().toString().isEmpty()){
                        spk = gson.fromJson(tvValueSpk.getText().toString(),SPK.class);
                        pengirimanGandaSPk.add(spk);
                        distincPengirimanGandaSPK.add(spk.getIdSPK());
                        arrayListSpk.add(spk.getIdSPK());
                    }

                    if(!tvValueLangsiran.getText().toString().isEmpty()){
                        langsiran = gson.fromJson(tvValueLangsiran.getText().toString(),Langsiran.class);
                        pengirimanGandaLangsiran.add(langsiran);
                        distincPengirimanGandaLangsiran.add(langsiran.getIdTujuan());
                        arrayListLangsiran.add(langsiran.getIdTujuan());
                    }

                    PengirimanGanda ganda = new PengirimanGanda(i, kendaraanDari, operator, vehicle,langsiran);
                    if(spk != null){
                        ganda.setSpk(spk);
                    }
                    pengirimanGandaSave.put(TransaksiAngkutHelper.showVehicle(vehicle), ganda);
                } else if (i < pengirimanGandas.size()) {
                    PengirimanGanda ganda = pengirimanGandas.get(i);
                    if(ganda != null) {
                        if(ganda.getVehicle() != null && ganda.getOperator() != null) {
                            if (ganda.getVehicle().getVehicleCode().isEmpty()) {
                                Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Vehicle", Toast.LENGTH_SHORT).show();
                                return false;
                            } else if (ganda.getOperator().getNama().isEmpty()) {
                                Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Operator", Toast.LENGTH_SHORT).show();
                                return false;
                            }
                        }else if (ganda.getVehicle() == null){
                            Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Vehicle", Toast.LENGTH_SHORT).show();
                            return false;
                        }else if (ganda.getOperator() == null){
                            Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Vehicle", Toast.LENGTH_SHORT).show();
                            return false;
                        }

                        if (ganda.getOperator() != null) {
                            distincOperator.add(ganda.getOperator().getKodeOperator());
                            arrayListOperator.add(ganda.getOperator().getKodeOperator());
                        }
                        if (ganda.getVehicle() != null) {
                            distincVehicle.add(TransaksiAngkutHelper.showVehicle(ganda.getVehicle()));
                            arrayListVehicle.add(TransaksiAngkutHelper.showVehicle(ganda.getVehicle()));
                        }
                        if (ganda.getSpk() != null) {
                            if (ganda.getSpk().getIdSPK() != null) {
                                pengirimanGandaSPk.add(ganda.getSpk());
                                distincPengirimanGandaSPK.add(ganda.getSpk().getIdSPK());
                                arrayListSpk.add(ganda.getSpk().getIdSPK());
                            }
                        }
                        if (ganda.getLangsiran() != null) {
                            if (ganda.getLangsiran().getIdTujuan() != null) {
                                pengirimanGandaLangsiran.add(ganda.getLangsiran());
                                distincPengirimanGandaLangsiran.add(ganda.getLangsiran().getIdTujuan());
                                arrayListLangsiran.add(ganda.getLangsiran().getIdTujuan());
                            }
                        }
                        pengirimanGandaSave.put(TransaksiAngkutHelper.showVehicle(ganda.getVehicle()), ganda);
                    }
                }
            }

            if (pengirimanGandaSave.size() != rvPengirimanGanda.getAdapter().getItemCount()) {
                Toast.makeText(HarvestApp.getContext(), "Tidak Boleh Ada Unit Yang Sama Dalam Pengiriman Ganda", Toast.LENGTH_SHORT).show();
                return false;
            }

            if (pengirimanGandaSave.get(TransaksiAngkutHelper.showVehicle(vehicleUse)) != null) {
                Toast.makeText(HarvestApp.getContext(), "Unit Yang Sudah Dipilih Tidak Boleh Ada Dalam Pengiriman Ganda", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        if(distincVehicle.size() != arrayListVehicle.size()){
            Toast.makeText(HarvestApp.getContext(), "Ada Vehicle Yang Double Mohon Cek", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(distincPengirimanGandaSPK.size() != arrayListSpk.size()){
            Toast.makeText(HarvestApp.getContext(), "Ada Spk Yang Double Mohon Cek", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(distincPengirimanGandaLangsiran.size() != arrayListLangsiran.size()){
            Toast.makeText(HarvestApp.getContext(), "Ada Langsiran Yang Double Mohon Cek", Toast.LENGTH_SHORT).show();
            return false;
        }

        loaderSave = new HashMap<>();
        for(int i = 0 ; i < rvLoader.getAdapter().getItemCount();i++){
            RecyclerView.ViewHolder viewHolder = rvLoader.findViewHolderForAdapterPosition(i);
            if (viewHolder != null) {
                View v = viewHolder.itemView;
                CompleteTextViewHelper etLoader = v.findViewById(R.id.etLoader);
                TextView tvValue = v.findViewById(R.id.tvValue);

                if(!tvValue.getText().toString().isEmpty()){
                    Gson gson = new Gson();
                    Operator loader = gson.fromJson(tvValue.getText().toString(), Operator.class);
                    loaderSave.put(loader.getKodeOperator(), loader);
                    distincOperator.add(loader.getKodeOperator());
                    arrayListOperator.add(loader.getKodeOperator());
                }

            }else if(i < loaders.size()){
                Operator loader = loaders.get(i);

                if(loader != null){
                    distincOperator.add(loader.getKodeOperator());
                    arrayListOperator.add(loader.getKodeOperator());
                    loaderSave.put(loader.getKodeOperator(),loader);
                }

            }
        }

        if(loaderSave.size() != rvLoader.getAdapter().getItemCount()){
            if (loaders.size() > 1) {
                Toast.makeText(HarvestApp.getContext(), "Tidak Boleh Ada Loader Yang Sama Dalam Info Loader", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        cagesSave = new HashMap<>();

        for (int i = 0; i < cages.size(); i++) {
            Cages cage = cages.get(i);

            if (cage != null) {
                if(cage.getAssetNo() != null) {
                    distinctCages.add(cage.getAssetNo());
                    arrayListCages.add(cage.getAssetNo());
                    cagesSave.put(cage.getAssetNo(), cage);
                }
            }
        }

        if (cagesSave.size() != rvCages.getAdapter().getItemCount()){
            if (cages.size() > 1){
                Toast.makeText(HarvestApp.getContext(), "Tidak Boleh Ada Cages Yang Sama Dalam Info Cages", Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        if (distinctCages.size() !=  arrayListCages.size()){
            Toast.makeText(HarvestApp.getContext(), "Tidak Boleh Ada Cages Yang Double", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(distincOperator.size() != arrayListOperator.size()){
            Toast.makeText(HarvestApp.getContext(), "Tidak Boleh Ada Operator Yang Double", Toast.LENGTH_SHORT).show();
            return false;
        }

        // harus ada cages
        if (vehicleUse != null) {
            if(vehicleUse.getGroupCode() != null) {
                if (vehicleUse.getGroupCode().toUpperCase().equals("PTN") ||
                        vehicleUse.getGroupCode().toUpperCase().equals("PCI") ||
                        vehicleUse.getGroupCode().toUpperCase().equals("PPG") ||
                        vehicleUse.getGroupCode().toUpperCase().equals("BGS")
                ) {
                    if ((dynamicParameterPenghasilan.getIsCages() == DynamicParameterPenghasilan.isCagesShow
                            || dynamicParameterPenghasilan.getIsCages() == DynamicParameterPenghasilan.isCagesQrOnly)
                            && cagesSave.size() == 0) {
                        Toast.makeText(HarvestApp.getContext(), "Untuk Kendaraan PTN, PCI, PPG, BGS Harus Input Cages", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                }
            }
        }

        if(ALselectedImage.size() > 0){
            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
            String stringIsi = preferences.getString(HarvestApp.STAMP_IMAGES,null);
            Gson gson  = new Gson();
            StampImage stampImage = gson.fromJson(stringIsi,StampImage.class);

            if (lsTujuan.getText().toString().equalsIgnoreCase(TransaksiAngkut.PKS)) {
                if(stampImage.getPks() == null){
                    Toast.makeText(HarvestApp.getContext(), "PKS yang Di pilih dan Watermark Berbeda Harap Foto Kembali", Toast.LENGTH_LONG).show();
                    takePicture();
                    return false;
                }else if(!stampImage.getPks().getIdPKS().equalsIgnoreCase(selectedPks.getIdPKS())) {
                    Toast.makeText(HarvestApp.getContext(), "PKS yang Di pilih dan Watermark Berbeda Harap Foto Kembali", Toast.LENGTH_LONG).show();
                    takePicture();
                    return false;
                }
            } else if (lsTujuan.getText().toString().equalsIgnoreCase(TransaksiAngkut.KELAUR)) {
                if(stampImage.getPks() != null && stampImage.getLangsiran() != null){
                    Toast.makeText(HarvestApp.getContext(), "Tujuan yang Di pilih dan Watermark Berbeda Harap Foto Kembali", Toast.LENGTH_LONG).show();
                    takePicture();
                    return false;
                }
            }
        }
        return true;
    }

    private void saveDataDeklarasiSPB(){
        String bin = null;
        messageError = null;
//        if(cbBin.isChecked()){
//            bin = etBin.getText().toString();
//        }

        SimpleDateFormat sdfD = new SimpleDateFormat("ddMMyy");
        HashMap<String,SubTotalAngkutMekanisasi> angkuts = new HashMap<>();

        ArrayList<Angkut> angkutsTap = new ArrayList<>();
        if(originAngkut.size() > 0){
            for (TransaksiPanen value : originAngkut.values()) {
                SubTotalAngkutMekanisasi subTotalAngkutMekanisasi = new SubTotalAngkutMekanisasi(value.getIdTPanen(),value,null);
                angkuts.put(subTotalAngkutMekanisasi.getIdKey(),subTotalAngkutMekanisasi);
            }
        }

        if(originAngkutKartu.size() > 0){
            for (Angkut value : originAngkutKartu.values()) {
                angkutsTap.add(value);
                SubTotalAngkutMekanisasi subTotalAngkutMekanisasi = new SubTotalAngkutMekanisasi(value.getIdAngkut(),null,value);
                angkuts.put(subTotalAngkutMekanisasi.getIdKey(),subTotalAngkutMekanisasi);
            }
        }


        int kendaraanDari = 0;
        switch (lsKendaraan.getText().toString().toUpperCase()){
            case TransaksiAngkut.KEBUN:
                kendaraanDari = TransaksiAngkut.KENDARAAN_KEBUN;
                break;
            case TransaksiAngkut.LUAR:
                kendaraanDari = TransaksiAngkut.KENDARAAN_LUAR;
                break;
        }

        String idTransaksiAngkut = null;
        if(selectedTransaksiSPB != null) {
            idTransaksiAngkut = selectedTransaksiSPB.getTransaksiAngkut().getIdTAngkut();
        }

        String noSPB = tvSpbId.getText().toString();
        if(idTransaksiAngkut == null){
            idTransaksiAngkut = "D" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT))
                    + sdfD.format(System.currentTimeMillis())
                    + GlobalHelper.getUser().getUserID();

            if(!idSpb.replace("S","").equals(idTransaksiAngkut.replace("D",""))){
                idSpb = "S"+idTransaksiAngkut.replace("D","");
                noSPB = TransaksiSpbHelper.converIdSPBToNoSPB(idSpb);
            }
        }
//        if(ALselectedImage.size() > 0){
//            for(File file : ALselectedImage){
//                sFoto.add(file.getAbsolutePath());
//            }
//        }

        loaders = new ArrayList<>(loaderSave.values());
        cages = new ArrayList<>(cagesSave.values());
        pengirimanGandas = null;
        if(pengirimanGandaSave != null) {
            if(pengirimanGandaSave.values().size() > 0) {
                pengirimanGandas = new ArrayList<>(pengirimanGandaSave.values());
                Collections.sort(pengirimanGandas, new Comparator<PengirimanGanda>() {
                    @Override
                    public int compare(PengirimanGanda o1, PengirimanGanda o2) {
                        return o1.getIdx() < o2.getIdx() ? -1 : (o1.getIdx() > o2.getIdx()) ? 1 : 0;
                    }
                });
            }
        }

        TransaksiAngkut transaksiAngkut = new TransaksiAngkut(idTransaksiAngkut,
                GlobalHelper.getUser().getUserID(),
                System.currentTimeMillis(),
                System.currentTimeMillis(),
                tJanjang,
                tBrondolan,
                tBerat,
                latitude,
                longitude,
                GlobalHelper.getEstate().getEstCode(),
                kendaraanDari,
                selectedOperator,
                vehicleUse,
                lsTujuan.getText().toString().equals(TransaksiAngkut.PKS) ? TransaksiAngkut.TUJUAN_PKS: TransaksiAngkut.TUJUAN_KELUAR,
                selectedPks,
                null,
                spkUse,
                bin,
                TransaksiAngkut.TAP,
                angkutsTap,
                loaders,
                cages,
                pengirimanGandas,
                headerTransaksiSPB.getFoto());

        if(substituteSelected!= null){
            transaksiAngkut.setSubstitute(substituteSelected);
        }

        //tambahan revisi selagi update arcgis
        if(selectedTransaksiSPB != null) {
            if (selectedTransaksiSPB.getTransaksiAngkut() != null) {
                if (selectedTransaksiSPB.getTransaksiAngkut().isRevisi()) {
                    transaksiAngkut.setRevisi(true);
                }
            }
        }

        transaksiAngkut.setSubTotalAngkuts(TransaksiAngkutHelper.getSubTotalMekanisasi(new ArrayList<SubTotalAngkutMekanisasi>(angkuts.values())));
        transaksiAngkut.setMekanisasiPanens(TransaksiAngkutHelper.getMekanisaiPanen());
        if(transaksiAngkut.getMekanisasiPanens().size() == 0){
            transaksiAngkut.setMekanisasiPanens(TransaksiAngkutHelper.getMekanisasiPanenFromAngkuts(originAngkut));
        }

        TransaksiSPB transaksiSPB = new TransaksiSPB(idSpb,
                noSPB,
                GlobalHelper.getUser().getUserID(),
                approveBy.getUserID(),
                GlobalHelper.getEstate().getEstCode(),
                System.currentTimeMillis(),
                transaksiAngkut,
                latitude,
                longitude,
                tJanjang,
                tBrondolan,
                TransaksiSPB.BELUM_UPLOAD,
                distinctAfdeling,
                headerTransaksiSPB.getFoto());

        Gson gson = new Gson();
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.SUBSTITUTE_KRANI_ANGKUT, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        if(substituteSelected!= null){
            transaksiSPB.setSubstitute(substituteSelected);
            editor.putString(HarvestApp.SUBSTITUTE_KRANI_ANGKUT,gson.toJson(transaksiAngkut.getSubstitute()));
        }else{
            editor.remove(HarvestApp.SUBSTITUTE_KRANI_ANGKUT);
        }
        editor.apply();

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_SPB);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        DataNitrit dataNitrit = new DataNitrit(transaksiSPB.getIdSPB(),gson.toJson(transaksiSPB));
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
            DataNitrit dataNitritX = (DataNitrit) iterator.next();
            TransaksiSPB transaksi = gson.fromJson(dataNitritX.getValueDataNitrit(), TransaksiSPB.class);
            if(transaksi.getStatus() == TransaksiSPB.BELUM_UPLOAD || transaksi.getStatus() == TransaksiSPB.BELUM_CLOSE){
                if(transaksi.getIdSPB() != null && transaksi.getTransaksiAngkut() != null ) {
                    if (transaksi.getIdSPB().equalsIgnoreCase(transaksiSPB.getIdSPB())) {
                        repository.update(dataNitrit);
                        messageError = "Update SPB";
                    }
//                    else if (transaksi.getTotalJanjang() == transaksiSPB.getTotalJanjang()
//                            && transaksi.getTotalBrondolan() == transaksiSPB.getTotalBrondolan()
//                            && transaksi.getTransaksiAngkut().getAngkuts().size() == transaksiSPB.getTransaksiAngkut().getAngkuts().size()
//                    ) {
//                        messageError = "Indikasi Doubel";
//                    }
                }
            }
        }

        if(messageError == null){
            repository.insert(dataNitrit);
            GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_SPB);
            GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT);

            String pathJSONBackup = GlobalHelper.getPathJSONBackup(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SPB],transaksiSPB.getIdSPB());
            File fJSONBackup = new File(pathJSONBackup);
            if(fJSONBackup.exists()){
                fJSONBackup.delete();
            }
            GlobalHelper.writeFileContent(pathJSONBackup,gson.toJson(transaksiSPB));
        }
        db.close();

        TransaksiSpbHelper.hapusFileAngkut(true);
        TransaksiAngkutHelper.addIdTPanenHasBeenTap(transaksiAngkut);

        selectedTransaksiSPB = transaksiSPB;
        if(selectedTransaksiSPB.getStatus() == TransaksiSPB.BELUM_UPLOAD) {
            SimpleDateFormat sdfJSON = new SimpleDateFormat("dd MMM yyyy");
            GlobalHelper.writeFileContentAppendNoToast(GlobalHelper.getLogTransaksi(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SPB], sdfJSON.format(selectedTransaksiSPB.getCreateDate())), "\n"+gson.toJson(selectedTransaksiSPB));
        }
    }

    private MekanisasiPanen selectedMekanisasi(){
        MekanisasiPanen mekanisasiPanenX = new MekanisasiPanen();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            try {
                MekanisasiPanen mekanisasiPanen = gson.fromJson(dataNitrit.getValueDataNitrit(), MekanisasiPanen.class);
                for (int i = 0; i < mekanisasiPanen.getTransaksiPanens().size(); i++) {
                    if (mekanisasiPanen.getTransaksiPanens().get(i).getIdTPanen().toLowerCase().equals(idPanenSelected.toLowerCase())) {
                        mekanisasiPanenX = mekanisasiPanen;
                        break;
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        db.close();
        return  mekanisasiPanenX;
    }

    private void updateDataRv(int status){
        tJanjang = 0;
        tBrondolan = 0;
        tBerat = 0;
        originAngkut = new HashMap<>();
        originAngkutKartu = new HashMap<>();
        distinctAfdeling = new HashSet<>();
        distinctTPH = new HashSet<>();
        distinctPemanen = new HashSet<>();
        distinctQT = new HashSet<>();
        distinctOperatorQT = new HashSet<>();
        distinctLoaderQT = new HashSet<>();
        distinctMekanisasi = new HashSet<>();
        distinctBlock = new HashSet<>();

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            boolean mekanisasi = false;
            try {
                JSONObject obj = new JSONObject(dataNitrit.getValueDataNitrit());
                if(obj.has("idMekanisasi")){
                    mekanisasi = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(mekanisasi) {
                MekanisasiPanen mekanisasiPanen = gson.fromJson(dataNitrit.getValueDataNitrit(), MekanisasiPanen.class);

                int color = warnaBlok[0];
                if (blockWarna.size() < 10) {
                    color = warnaBlok[blockWarna.size()];
                } else if (blockWarna.size() < 20) {
                    color = warnaBlok[blockWarna.size() - 10];
                } else if (blockWarna.size() < 30) {
                    color = warnaBlok[blockWarna.size() - 20];
                }

                if (blockWarna.get(mekanisasiPanen.getIdMekanisasi()) != null) {
                    color = blockWarna.get(mekanisasiPanen.getIdMekanisasi());
                } else {
                    blockWarna.put(mekanisasiPanen.getIdMekanisasi(), color);
                }

                if (status == STATUS_LONG_OPERATION_SEARCH) {
                    if (mekanisasiPanen != null) {
                        for (int i = 0; i < mekanisasiPanen.getTransaksiPanens().size(); i++) {
                            boolean masuk = false;
                            if (mekanisasiPanen.getTransaksiPanens().get(i).getTph() != null) {
                                if (mekanisasiPanen.getTransaksiPanens().get(i).getTph().getBlock().toLowerCase().contains(etSearch.getText().toString().toLowerCase())
                                        || mekanisasiPanen.getTransaksiPanens().get(i).getTph().getAncak().toLowerCase().contains(etSearch.getText().toString().toLowerCase())
                                        ) {
                                    masuk = true;
                                }
                            }

                            if (mekanisasiPanen.getTransaksiPanens().get(i).getPemanen() != null) {
                                if (mekanisasiPanen.getTransaksiPanens().get(i).getPemanen().getKodePemanen().toLowerCase().contains(etSearch.getText().toString().toLowerCase())
                                        || mekanisasiPanen.getTransaksiPanens().get(i).getPemanen().getGank().toLowerCase().contains(etSearch.getText().toString().toLowerCase())
                                        || mekanisasiPanen.getTransaksiPanens().get(i).getPemanen().getNama().toLowerCase().contains(etSearch.getText().toString().toLowerCase())
                                        ) {
                                    masuk = true;
                                }
                            }

                            if (masuk) {
                                addRowTableAngkut(mekanisasiPanen, color);
                            }
                        }
                    }
                } else if (status == STATUS_LONG_OPERATION_NONE) {
                    addRowTableAngkut(mekanisasiPanen, color);
                }
            }else{
                Angkut angkut = gson.fromJson(dataNitrit.getValueDataNitrit(),Angkut.class);
                if (status == STATUS_LONG_OPERATION_SEARCH) {
                    String tipeAngkut = "Manual";
                    if(angkut.getTransaksiPanen() != null){
                        tipeAngkut = angkut.getTransaksiPanen().getTph().getNamaTph();
                    }else if (angkut.getTransaksiAngkut() != null){
                        tipeAngkut = angkut.getTransaksiAngkut().getIdTAngkut();
                    }
                    if (sdfTime.format(angkut.getWaktuAngkut()).contains(etSearch.getText().toString().toLowerCase()) ||
                            tipeAngkut.toLowerCase().contains(etSearch.getText().toString().toLowerCase()) ||
                            String.valueOf(angkut.getJanjang()).contains(etSearch.getText().toString().toLowerCase()) ||
                            String.valueOf(angkut.getBrondolan()).contains(etSearch.getText().toString().toLowerCase())) {
                        addRowTableAngkut(angkut);
                    }
                } else if (status == STATUS_LONG_OPERATION_NONE) {
                    addRowTableAngkut(angkut);
                }
            }
        }
        db.close();
    }

    private void addRowTableAngkut(MekanisasiPanen mekanisasiPanen,int color){
        tJanjang += mekanisasiPanen.getTotalJanjang();
        tBrondolan += mekanisasiPanen.getTotalBrondolan();
        tBerat += mekanisasiPanen.getTotalBerat();

        if(mekanisasiPanen.getVehicle() != null) {
            distinctQT.add(mekanisasiPanen.getVehicle().getVraOrderNumber());
        }

        if(mekanisasiPanen.getOperator() != null) {
            distinctOperatorQT.add(mekanisasiPanen.getOperator().getNik());
        }
        if(mekanisasiPanen.getLoader() != null) {
            distinctLoaderQT.add(mekanisasiPanen.getLoader().getNik());
        }
        distinctMekanisasi.add(mekanisasiPanen.getIdMekanisasi());

        if(mekanisasiPanen.getTransaksiPanens().size() > 0){
            for(TransaksiPanen transaksiPanen : mekanisasiPanen.getTransaksiPanens()){
                transaksiPanen.setRemarks(mekanisasiPanen.getIdMekanisasi());
                transaksiPanen.setColorBackground(color);
                originAngkut.put(transaksiPanen.getIdTPanen(),transaksiPanen);
                distinctAfdeling.add(transaksiPanen.getTph().getAfdeling());
                distinctBlock.add(transaksiPanen.getTph().getBlock());
                distinctTPH.add(transaksiPanen.getTph().getNoTph());
                distinctPemanen.add(transaksiPanen.getPemanen().getKodePemanen());
            }
        }
    }

    private void addRowTableAngkut(Angkut angkut) {
        tJanjang += angkut.getJanjang();
        tBrondolan += angkut.getBrondolan();
        tBerat += angkut.getBerat();
        originAngkutKartu.put(angkut.getIdAngkut(), angkut);
        distinctAfdeling.add(transaksiSpbHelper.getAfdeling(angkut));
        if (angkut.getTph() != null) {
            distinctTPH.add(angkut.getTph().getNoTph());
            distinctBlock.add(angkut.getTph().getBlock());
        }
    }

    private void updateUIRV(int status){
        if(originAngkut.size() > 0   || originAngkutKartu.size() > 0) {

            ltableview.setVisibility(View.VISIBLE);
            mTableViewModel = new MekanisasiPanenTableViewModel(mekanisasiActivity, originAngkut,originAngkutKartu);
            adapter = new MekanisasiPanenAdapter(mekanisasiActivity,mTableViewModel);
            mTableView.setAdapter(adapter);
            mTableView.setTableViewListener(new ITableViewListener() {

                @Override
                public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellDoubleClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {

                }

                @Override
                public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {
                        // Create Long Press Popup
                        ColumnHeaderPopup popup = new ColumnHeaderPopup(
                                (ColumnHeaderViewHolder) columnHeaderView, mTableView);
                        // Show
                        popup.show();
                    }
                }

                @Override
                public void onColumnHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {

                }

                @Override
                public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }

                @Override
                public void onRowHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {

                }

                @Override
                public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    if(newMekanisasi.getVisibility() == View.VISIBLE) {
                        RowHeaderLongPressPopup popup = new RowHeaderLongPressPopup(rowHeaderView, mTableView, mekanisasiActivity);
                        popup.show();
                    }
                }
            });
            adapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel
                    .getRowHeaderList(), mTableViewModel.getCellList());
        }else{
            ltableview.setVisibility(View.GONE);
        }

        tvTotalOph.setText(String.valueOf(originAngkut.size() + originAngkutKartu.size()));
        tvTotalTph.setText(String.valueOf(distinctTPH.size()));
        tvTotalPemanen.setText(String.valueOf(distinctPemanen.size()));
        tvTotalJanjang.setText(String.format("%,d",tJanjang) + "Jjg");
        tvTotalBerat.setText(String.format("%.1f",tBerat) + "Kg");

//        tvQuickTruck.setText(String.valueOf(distinctQT.size()));
//        tvOperatorQT.setText(String.valueOf(distinctOperatorQT.size()));
//        tvLoaderQT.setText(String.valueOf(distinctLoaderQT.size()));
//        tvKelompok.setText(String.valueOf(distinctMekanisasi.size()));
//        tvBlock.setText(String.valueOf(distinctBlock.size()));

    }

    public void addAngkut(TransaksiPanen transaksiPanen){
        if(selectedTransaksiSPB != null) {
            if (selectedTransaksiSPB.getStatus() == TransaksiSPB.UPLOAD || selectedTransaksiSPB.getStatus() == TransaksiSPB.BELUM_UPLOAD) {
                mekanisasiActivity.searchingGPS = Snackbar.make(mekanisasiActivity.myCoordinatorLayout, "SPB ini sudah close tidak bisa lagi tap kartu", Snackbar.LENGTH_INDEFINITE);
                mekanisasiActivity.searchingGPS.show();
                return;
            }
        }

        if(baseActivity.currentlocation != null) {
            if(baseActivity.currentlocation.getLongitude() != 0.0) {
                setidNfc();
                transaksiPanenNFC = transaksiPanen;
                if(transaksiPanenNFC.getNfcFlagTap() != null) {
                    if(transaksiPanenNFC.getNfcFlagTap().getUserTap() != null) {
                        if (!transaksiPanenNFC.getNfcFlagTap().getUserTap().equalsIgnoreCase(GlobalHelper.getUser().getUserID())) {
                            mekanisasiActivity.searchingGPS = Snackbar.make(mekanisasiActivity.myCoordinatorLayout, "Kartu ini Sudah Pernah Di Tap SPB Kok Di Tap Lagi", Snackbar.LENGTH_INDEFINITE);
                            mekanisasiActivity.searchingGPS.show();
                            return;
                        }
                    }
                    if (transaksiPanenNFC.getNfcFlagTap().getCountTap() != 0) {
                        if (transaksiPanenNFC.getNfcFlagTap().getCountTap() > 2) {
                            mekanisasiActivity.searchingGPS = Snackbar.make(mekanisasiActivity.myCoordinatorLayout, "Kartu ini Sudah Pernah Di Tap " + transaksiPanenNFC.getNfcFlagTap().getCountTap() + " X", Snackbar.LENGTH_INDEFINITE);
                            mekanisasiActivity.searchingGPS.show();
                            return;
                        }
                    }
                    if(transaksiPanenNFC.getNfcFlagTap().getWaktuTap() != null) {
                        if (transaksiPanenNFC.getNfcFlagTap().getWaktuTap() != 0) {
                            SimpleDateFormat sdfD = new SimpleDateFormat("dd MMM yyyy");
                            String now = sdfD.format(System.currentTimeMillis());
                            String waktuTap = sdfD.format(transaksiPanenNFC.getNfcFlagTap().getWaktuTap());
                            if (!now.equalsIgnoreCase(waktuTap)) {
                                mekanisasiActivity.searchingGPS = Snackbar.make(mekanisasiActivity.myCoordinatorLayout, "Kartu Ini Sudah Di Tap Pada " + waktuTap, Snackbar.LENGTH_INDEFINITE);
                                mekanisasiActivity.searchingGPS.show();
                                return;
                            }
                        }
                    }
                }

//                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//                String now = sdf.format(System.currentTimeMillis());
//                String tPanen = sdf.format(transaksiPanenNFC.getCreateDate());
//                Log.d(TAG, "addAngkut: " + String.valueOf(TimeUnit.MILLISECONDS.toDays(
//                        System.currentTimeMillis() - transaksiPanenNFC.getCreateDate()
//                )));
                long selisih = TimeUnit.MILLISECONDS.toDays(
                        System.currentTimeMillis() - transaksiPanenNFC.getCreateDate()
                );

                int maxRestan = GlobalHelper.MAX_RESTAN;
                try{
                    if(dynamicParameterPenghasilan.getMaxRestan() > 0) {
                        maxRestan = dynamicParameterPenghasilan.getMaxRestan();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                if(!transaksiPanenNFC.getTph().getEstCode().equalsIgnoreCase(GlobalHelper.getEstate().getEstCode())){
                    mekanisasiActivity.searchingGPS = Snackbar.make(mekanisasiActivity.myCoordinatorLayout, getResources().getString(R.string.diffrent_estate), Snackbar.LENGTH_INDEFINITE);
                    mekanisasiActivity.searchingGPS.show();
                }else if(!transaksiPanenNFC.getEstCode().equalsIgnoreCase(GlobalHelper.getEstate().getEstCode())){
                    mekanisasiActivity.searchingGPS = Snackbar.make(mekanisasiActivity.myCoordinatorLayout, getResources().getString(R.string.diffrent_estate), Snackbar.LENGTH_INDEFINITE);
                    mekanisasiActivity.searchingGPS.show();
                }else if (transaksiPanenNFC.getTph().getNoTph() == null){
                    mekanisasiActivity.searchingGPS = Snackbar.make(mekanisasiActivity.myCoordinatorLayout, "TPH Tidak Di Temukan", Snackbar.LENGTH_INDEFINITE);
                    mekanisasiActivity.searchingGPS.show();
                }else if(selisih > maxRestan){
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_ADD_TRANSAKSI_PANEN_MAX_RESTAN));
                }else {
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_ADD_TRANSAKSI_PANEN));
                }
            }else{
//                spbActivity.searchingGPS = Snackbar.make(spbActivity.myCoordinatorLayout, getResources().getString(R.string.diffrent_estate), Snackbar.LENGTH_INDEFINITE);
//                spbActivity.searchingGPS.show();
                WidgetHelper.warningFindGps(mekanisasiActivity,mekanisasiActivity.myCoordinatorLayout);
            }
        }else{
//            spbActivity.searchingGPS = Snackbar.make(spbActivity.myCoordinatorLayout, getResources().getString(R.string.search_gps), Snackbar.LENGTH_INDEFINITE);
//            spbActivity.searchingGPS.show();
            WidgetHelper.warningFindGps(mekanisasiActivity,mekanisasiActivity.myCoordinatorLayout);
        }
    }

    private void setidNfc(){
        if(mekanisasiActivity.myTag != null){
            idNfc = new ArraySet<>();
            String idNfcS = GlobalHelper.bytesToHexString(mekanisasiActivity.myTag.getId());
            if (!idNfcS.isEmpty()) {
                idNfc.add(idNfcS);
            }
        }
    }

    private void addAngkutDB(TransaksiPanen transaksiPanen,Object longOperation){
        insertDBAngkut = true;
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            try {
                Gson gson = new Gson();
                Angkut angkutDb = gson.fromJson(dataNitrit.getValueDataNitrit(), Angkut.class);
                if (angkutDb.getTransaksiPanen() != null) {
                    if (angkutDb.getTransaksiPanen().getIdTPanen().equals(transaksiPanen.getIdTPanen())) {
                        idPanenAngkut = angkutDb.getTransaksiPanen();
                        insertDBAngkut = false;
                        break;
                    }
                }

                for (int i = 0; i < transaksiPanen.getKongsi().getSubPemanens().size(); i++) {
                    String oph = TransaksiPanenHelper.getOph(transaksiPanen.getIdTPanen(), transaksiPanen.getKongsi().getSubPemanens().get(i).getOph());
                    if (angkutDb.getOph().equalsIgnoreCase(oph)) {
                        idPanenAngkut = angkutDb.getTransaksiPanen();
                        insertDBAngkut = false;
                        break;
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        if(insertDBAngkut){
            if(!transaksiPanen.getTph().getEstCode().equalsIgnoreCase(GlobalHelper.getEstate().getEstCode())){
                insertDBAngkut = false;
            }
        }

        if(insertDBAngkut){
            if(!transaksiPanen.getEstCode().equalsIgnoreCase(GlobalHelper.getEstate().getEstCode())){
                insertDBAngkut = false;
            }
        }

        if(insertDBAngkut){
            idPanenAngkut = TransaksiAngkutHelper.cekIdTPanenHasBeenTap(transaksiPanen.getIdTPanen());
            if(idPanenAngkut != null){
                insertDBAngkut = false;
            }
        }

        if(insertDBAngkut) {
            if (baseActivity != null) {
                if (!((BaseActivity) baseActivity).formatNfcPanen((MekanisasiActivity) mekanisasiActivity, GlobalHelper.TYPE_NFC_BIRU)) {
                    insertDBAngkut = false;
                    messageError = "Gagal Format Kartu Tap Kartu Kembali";
                }
            }
        }

        if(insertDBAngkut) {

            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmm");
            String idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_ANGKUT_SPB))
                    + sdf.format(System.currentTimeMillis());

            String idTransaksiRef = idTransaksi;
            Date createDate = new Date(transaksiPanen.getCreateDate());
            Calendar cal = Calendar.getInstance();
            cal.setTime(createDate);
            double bjr = BjrHelper.getBJRInformasi(transaksiPanen.getTph().getBlock(), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR));

            int count = 1;
            for (int i = 0; i < transaksiPanen.getKongsi().getSubPemanens().size(); i++) {
                idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_ANGKUT_SPB))
                        + sdf.format(System.currentTimeMillis());

                SubPemanen subPemanen = transaksiPanen.getKongsi().getSubPemanens().get(i);
                double berat = subPemanen.getHasilPanen().getTotalJanjang() * bjr;
                String oph = TransaksiPanenHelper.getOph(transaksiPanen.getIdTPanen(),subPemanen.getOph());

                if(oph != ""){
                    idTransaksi = "A"+ oph.replace("-","");
                }

                Angkut angkut = new Angkut(idTransaksi,
                        System.currentTimeMillis(),
                        transaksiPanen,
                        null,
                        baseActivity.currentlocation.getLatitude(),
                        baseActivity.currentlocation.getLongitude(),
                        subPemanen.getHasilPanen().getTotalJanjang(),
                        subPemanen.getHasilPanen().getBrondolan(),
                        bjr,
                        berat,
                        transaksiPanen.getTph().getBlock(),
                        oph,
                        0,
                        transaksiPanen.getTph(),
                        null,
                        null);
                angkut.setIdAngkutRef(idTransaksiRef);
                angkut.setIdNfc(idNfc);

                Gson gson = new Gson();
                DataNitrit dataNitrit = new DataNitrit(angkut.getIdAngkut(), gson.toJson(angkut),idTransaksiRef);
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", angkut.getIdAngkut()));
                if(cursor.size() > 0) {
                    repository.update(dataNitrit);
                }else{
                    repository.insert(dataNitrit);
                    GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_ANGKUT_SPB);
                }

                try {
                    JSONObject objProgres = new JSONObject();
                    objProgres.put("ket", "Insert Data Oph");
                    objProgres.put("persen", (count * 100 / transaksiPanen.getKongsi().getSubPemanens().size()));
                    objProgres.put("count", count);
                    objProgres.put("total", (transaksiPanen.getKongsi().getSubPemanens().size()));
                    if(longOperation instanceof LongOperation){
                        ((LongOperation) longOperation).publishProgress(objProgres);
                    }
                    count++;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        db.close();

        if (transaksiPanen != null) {

            transaksiPanen.setStatus(TransaksiPanen.TAP);
//            if (baseActivity != null) {
//                ((BaseActivity) baseActivity).formatNfcPanen((SpbActivity) spbActivity, GlobalHelper.TYPE_NFC_BIRU);
//            }
        }
    }

    private void addAngkutDBMaxRestan(TransaksiPanen transaksiPanen, Object longOperation){
        insertDBAngkut = true;
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            try {
                Gson gson = new Gson();
                Angkut angkutDb = gson.fromJson(dataNitrit.getValueDataNitrit(), Angkut.class);
                if (angkutDb.getTransaksiPanen() != null) {
                    if (angkutDb.getTransaksiPanen().getIdTPanen().equals(transaksiPanen.getIdTPanen())) {
                        idPanenAngkut = angkutDb.getTransaksiPanen();
                        insertDBAngkut = false;
                        break;
                    }
                }

                for (int i = 0; i < transaksiPanen.getKongsi().getSubPemanens().size(); i++) {
                    String oph = TransaksiPanenHelper.getOph(transaksiPanen.getIdTPanen(), transaksiPanen.getKongsi().getSubPemanens().get(i).getOph());
                    if (angkutDb.getOph().equalsIgnoreCase(oph)) {
                        idPanenAngkut = angkutDb.getTransaksiPanen();
                        insertDBAngkut = false;
                        break;
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        if(insertDBAngkut){
            if(!transaksiPanen.getTph().getEstCode().equalsIgnoreCase(GlobalHelper.getEstate().getEstCode())){
                insertDBAngkut = false;
            }
        }

        if(insertDBAngkut){
            if(!transaksiPanen.getEstCode().equalsIgnoreCase(GlobalHelper.getEstate().getEstCode())){
                insertDBAngkut = false;
            }
        }

        if(insertDBAngkut){
            idPanenAngkut = TransaksiAngkutHelper.cekIdTPanenHasBeenTap(transaksiPanen.getIdTPanen());
            if(idPanenAngkut != null){
                insertDBAngkut = false;
            }
        }

        if(insertDBAngkut) {
            if (baseActivity != null) {
                if (!((BaseActivity) baseActivity).formatNfcPanen((MekanisasiActivity) mekanisasiActivity, GlobalHelper.TYPE_NFC_BIRU)) {
                    insertDBAngkut = false;
                    messageError = "Gagal Format Kartu Tap Kartu Kembali";
                }
            }
        }

        if(insertDBAngkut) {

            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmm");
            String idTransaksi = "A" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_ANGKUT_SPB))
                    + sdf.format(System.currentTimeMillis());

            String idTransaksiRef = idTransaksi;

            SubPemanen subPemanen = transaksiPanen.getKongsi().getSubPemanens().get(0);
            String oph = TransaksiPanenHelper.getOph(transaksiPanen.getIdTPanen(),subPemanen.getOph());

            if(oph != ""){
                idTransaksi = "A"+ oph.replace("H","").replace("-","");
            }

            long waktuAngkut = System.currentTimeMillis();
            String idManualAngkut = idTransaksi+";manual_angkut";
            String nfcId = "";
            if(idNfc.size() > 0) {
                for (String nfc : idNfc) {
                    nfcId = nfc;
                }
            }

            ManualSPB manualSPB = new ManualSPB(  idManualAngkut,
                    GlobalHelper.getUser().getUserID(),
                    GlobalHelper.getEstate().getEstCode(),
                    ManualSPB.TYPE_KARTU_MAX_RESTAN,
                    ManualSPB.TYPE_KARTU_MAX_RESTAN_NAME,
                    transaksiPanen.getTph().getBlock(),
                    transaksiPanen.getTph(),
                    waktuAngkut,
                    waktuAngkut,
                    nfcId);
            manualSPB.setTransaksiPanen(transaksiPanen);

            Angkut angkut = new Angkut( idTransaksi,
                    waktuAngkut,
                    null,
                    null,
                    baseActivity.currentlocation.getLatitude(),
                    baseActivity.currentlocation.getLongitude(),
                    transaksiPanen.getTph().getBlock(),
                    transaksiPanen.getTph(),
                    null,
                    null,
                    manualSPB);
            angkut.setIdAngkutRef(idTransaksi);
            angkut.setIdNfc(idNfc);

            Gson gson = new Gson();
            DataNitrit dataNitrit = new DataNitrit(angkut.getIdAngkut(), gson.toJson(angkut),idTransaksiRef);
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", angkut.getIdAngkut()));
            if(cursor.size() > 0) {
                repository.update(dataNitrit);
            }else{
                repository.insert(dataNitrit);
                GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_ANGKUT_SPB);
            }

            try {
                JSONObject objProgres = new JSONObject();
                objProgres.put("ket", "Insert Data Oph MaxRestan");
                objProgres.put("persen", 100);
                objProgres.put("count", 1);
                objProgres.put("total", 1);
                if(longOperation instanceof LongOperation){
                    ((LongOperation) longOperation).publishProgress(objProgres);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        db.close();

        if (transaksiPanen != null) {

            transaksiPanen.setStatus(TransaksiPanen.TAP);
//            if (baseActivity != null) {
//                ((BaseActivity) baseActivity).formatNfcPanen((SpbActivity) spbActivity, GlobalHelper.TYPE_NFC_BIRU);
//            }
        }
    }

    private void removeAngkutDB(){
        String idRefAngkut = null;
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", idHapusAngkut));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                idRefAngkut = dataNitrit.getParam1();
                Log.d(TAG, "removeAngkutDB: idRefAngkut" + idRefAngkut);
                break;
            }
        }

        cursor = repository.find(ObjectFilters.eq("param1", idRefAngkut));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                repository.remove(dataNitrit);
                Log.d(TAG, "removeAngkutDB: idAngkutHapus" + dataNitrit.getIdDataNitrit());
            }
        }
        db.close();
    }

    private class LongOperation extends AsyncTask<String, String, String> {
        private AlertDialog alertDialogAllpoin;
        Snackbar snackbar;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(getActivity(), getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])){
                case STATUS_LONG_OPERATION_NONE:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH:{
                    updateDataRv(Integer.parseInt(strings[0]));
                    break;
                }
                case STATUS_LONG_OPERATION_SAVE_DATA_PRINT:{
                    saveDataDeklarasiSPB();
                    break;
                }
                case STATUS_LONG_OPERATION_SAVE_DATA_SHARE:{
                    saveDataDeklarasiSPB();
                    break;
                }
                case STATUS_LONG_OPERATION_DETAIL:{
                    mekanisasiActivity.mekanisasiPanenSelected = selectedMekanisasi();
                    break;
                }
                case STATUS_LONG_OPERATION_ADD_TRANSAKSI_PANEN:{
                    addAngkutDB(transaksiPanenNFC,this);
                    break;
                }
                case STATUS_LONG_OPERATION_ADD_TRANSAKSI_PANEN_MAX_RESTAN:{
                    addAngkutDBMaxRestan(transaksiPanenNFC,this);
                    break;
                }
                case STATUS_LONG_OPERATION_DELETED_ANGKUT:{
                    removeAngkutDB();
                    break;
                }
            }
            return strings[0];
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            mekanisasiActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject objProgres = new JSONObject(String.valueOf(values[0]));

                        if(snackbar== null) {
                            if (mekanisasiActivity != null) {
                                snackbar = Snackbar.make(mekanisasiActivity.myCoordinatorLayout, objProgres.getString("ket"), Snackbar.LENGTH_INDEFINITE);
                            }
                        }
                        snackbar.setText(String.format(objProgres.getString("ket") + " %d %s [%d/%d] ",
                                objProgres.getInt("persen"), "%", objProgres.getInt("count"), objProgres.getInt("total")));
                        snackbar.setDuration(Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            switch (Integer.parseInt(result)) {
                case STATUS_LONG_OPERATION_NONE: {
                    updateUIRV(Integer.parseInt(result));
                    break;
                }
                case STATUS_LONG_OPERATION_SEARCH: {
                    updateUIRV(Integer.parseInt(result));
                    break;
                }
                case STATUS_LONG_OPERATION_SAVE_DATA_PRINT:{
                    if(messageError != null){
                        Toast.makeText(HarvestApp.getContext(),messageError,Toast.LENGTH_LONG).show();
                    }
                    transaksiSpbHelper.printTransaksiSPBMekanisasi(selectedTransaksiSPB,new ArrayList<>(originAngkut.values()),new ArrayList<>(originAngkutKartu.values()),false,dynamicParameterPenghasilan);
                    break;
                }
                case STATUS_LONG_OPERATION_PRINT:{
                    transaksiSpbHelper.printTransaksiSPBMekanisasi(mekanisasiActivity.selectedTransaksiSPB, new ArrayList<>(originAngkut.values()),new ArrayList<>(originAngkutKartu.values()), true,dynamicParameterPenghasilan);
                    break;
                }
                case STATUS_LONG_OPERATION_SAVE_DATA_SHARE:{
                    if(messageError != null){
                        Toast.makeText(HarvestApp.getContext(),messageError,Toast.LENGTH_LONG).show();
                    }
                    transaksiSpbHelper.showQrMekanisasi(selectedTransaksiSPB,mekanisasiActivity.qrHelper,new ArrayList<>(originAngkut.values()),new ArrayList<>(originAngkutKartu.values()),false,dynamicParameterPenghasilan);
                    break;
                }
                case STATUS_LONG_OPERATION_SHARE:{
                    transaksiSpbHelper.showQrMekanisasi(mekanisasiActivity.selectedTransaksiSPB, mekanisasiActivity.qrHelper, new ArrayList<>(originAngkut.values()),new ArrayList<>(originAngkutKartu.values()), true,dynamicParameterPenghasilan);
                    break;
                }
                case STATUS_LONG_OPERATION_DETAIL:{
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_container, MekanisasiPanenInputFragment.getInstance())
                            .commit();
                    break;
                }
                case STATUS_LONG_OPERATION_ADD_TRANSAKSI_PANEN:{
                    if(!insertDBAngkut) {
                        if (idPanenAngkut != null) {
                            modelRecyclerViewHelper.showSummaryTransaksi(TransaksiAngkutHelper.showHasBeenTap(idPanenAngkut, mekanisasiActivity));
                        }else if (messageError != null){
                            Toast.makeText(HarvestApp.getContext(),messageError,Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(HarvestApp.getContext(),"Transaksi Panen Ini Sudah Di Angkut",Toast.LENGTH_SHORT).show();
                        }
                    }
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
                    break;
                }
                case STATUS_LONG_OPERATION_ADD_TRANSAKSI_PANEN_MAX_RESTAN:{
                    if(!insertDBAngkut) {
                        if (idPanenAngkut != null) {
                            modelRecyclerViewHelper.showSummaryTransaksi(TransaksiAngkutHelper.showHasBeenTap(idPanenAngkut, mekanisasiActivity));
                        }else if (messageError != null){
                            Toast.makeText(HarvestApp.getContext(),messageError,Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(HarvestApp.getContext(),"Transaksi Panen Ini Sudah Di Angkut",Toast.LENGTH_SHORT).show();
                        }
                    }
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
                    break;
                }
                case STATUS_LONG_OPERATION_DELETED_ANGKUT:{
                    new LongOperation().execute(String.valueOf(STATUS_LONG_OPERATION_NONE));
                    break;
                }
            }

            if (alertDialogAllpoin != null) {
                alertDialogAllpoin.cancel();
            }
            messageError = null;
        }

        public void publishProgress(JSONObject objProgres){
            onProgressUpdate(String.valueOf(objProgres));
        }
    }
}
