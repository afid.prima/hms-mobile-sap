package id.co.ams_plantation.harvestsap.model;

import java.util.ArrayList;

public class FilterTransaction {
    int showInputBy;
    ArrayList<FilterMenu> filterMenus;
    ArrayList<FilterAfdeling> filterAfdelings;

    public FilterTransaction(int showInputBy, ArrayList<FilterMenu> filterMenus, ArrayList<FilterAfdeling> filterAfdelings) {
        this.showInputBy = showInputBy;
        this.filterMenus = filterMenus;
        this.filterAfdelings = filterAfdelings;
    }

    public int getShowInputBy() {
        return showInputBy;
    }

    public void setShowInputBy(int showInputBy) {
        this.showInputBy = showInputBy;
    }

    public ArrayList<FilterMenu> getFilterMenus() {
        return filterMenus;
    }

    public void setFilterMenus(ArrayList<FilterMenu> filterMenus) {
        this.filterMenus = filterMenus;
    }

    public ArrayList<FilterAfdeling> getFilterAfdelings() {
        return filterAfdelings;
    }

    public void setFilterAfdelings(ArrayList<FilterAfdeling> filterAfdelings) {
        this.filterAfdelings = filterAfdelings;
    }
}
