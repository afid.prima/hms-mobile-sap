package id.co.ams_plantation.harvestsap.model;

public class SyncTransaksi {
    String afdeling;
    String userId;

    public SyncTransaksi() {
    }

    public SyncTransaksi(String afdeling, String userId) {
        this.afdeling = afdeling;
        this.userId = userId;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
