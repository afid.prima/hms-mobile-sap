package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import id.co.ams_plantation.harvestsap.Fragment.AngkutInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.MekanisasiInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.SpbInputFragment;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.SwipeItemRecycleView.SwipeControllerInterface;
import id.co.ams_plantation.harvestsap.model.Langsiran;
import id.co.ams_plantation.harvestsap.model.Operator;
import id.co.ams_plantation.harvestsap.model.PengirimanGanda;
import id.co.ams_plantation.harvestsap.model.SPK;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.Vehicle;
import id.co.ams_plantation.harvestsap.ui.AngkutActivity;
import id.co.ams_plantation.harvestsap.ui.MekanisasiActivity;
import id.co.ams_plantation.harvestsap.ui.SpbActivity;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;

public class PengirimanGandaAdapater extends RecyclerView.Adapter<PengirimanGandaAdapater.Holder> implements SwipeControllerInterface {
    Context context;
    public ArrayList<PengirimanGanda> pengirimanGandas;
    SelectOperatorAdapter adapterOperator;
    SelectVehicleAdapter adapterVehicle;
    SelectSpkAdapter adapterSpk;
    SelectLangsiranAdapter adapterLangsiran;

    public PengirimanGandaAdapater(Context context, ArrayList<PengirimanGanda> pengirimanGandas, SelectOperatorAdapter adapterOperator, SelectVehicleAdapter adapterVehicle,SelectSpkAdapter adapterSpk,SelectLangsiranAdapter adapterLangsiran) {
        this.context = context;
        this.pengirimanGandas = pengirimanGandas;
        this.adapterOperator = adapterOperator;
        this.adapterVehicle = adapterVehicle;
        this.adapterSpk = adapterSpk;
        this.adapterLangsiran = adapterLangsiran;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.pengiriman_ganda_item, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.setValue(pengirimanGandas.get(position),position);
    }

    @Override
    public int getItemCount() {
        return pengirimanGandas != null ? pengirimanGandas.size() : 0;
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    public void updatePengirimanGanda(){
        if(context instanceof AngkutActivity) {
            if (((AngkutActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof AngkutInputFragment) {
                AngkutInputFragment fragment = (AngkutInputFragment) ((AngkutActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
                fragment.updatePengirimanGanda();
            }
        }else if(context instanceof SpbActivity) {
            if (((SpbActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof SpbInputFragment) {
                SpbInputFragment fragment = (SpbInputFragment) ((SpbActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
                fragment.updatePengirimanGanda();
            }
        }else if(context instanceof MekanisasiActivity) {
            if (((MekanisasiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof MekanisasiInputFragment) {
                MekanisasiInputFragment fragment = (MekanisasiInputFragment) ((MekanisasiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
                fragment.updatePengirimanGanda();
            }
        }
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView tvUnit;
        TextView tvOperator;
        BetterSpinner lsKendaraan;
        CompleteTextViewHelper etLangsiran;
        CompleteTextViewHelper etVehicle;
        CompleteTextViewHelper etDriver;
        CompleteTextViewHelper etSpk;
        TextView tvValueUnit;
        TextView tvValueOperator;
        TextView tvValueSpk;
        TextView tvValueLangsiran;
        LinearLayout lSPK;
        List<String> bKendaraan;

        public Holder(View v) {
            super(v);
            tvUnit = v.findViewById(R.id.tvUnit);
            tvOperator = v.findViewById(R.id.tvOperator);
            lsKendaraan = v.findViewById(R.id.lsKendaraan);
            etVehicle = v.findViewById(R.id.etVehicle);
            etDriver = v.findViewById(R.id.etDriver);
            etSpk = v.findViewById(R.id.etSpk);
            etLangsiran = v.findViewById(R.id.etLangsiran);
            lSPK = v.findViewById(R.id.lSPK);
            tvValueUnit = v.findViewById(R.id.tvValueUnit);
            tvValueOperator = v.findViewById(R.id.tvValueOperator);
            tvValueSpk = v.findViewById(R.id.tvValueSpk);
            tvValueLangsiran = v.findViewById(R.id.tvValueLangsiran);

            lSPK.setVisibility(View.GONE);

            bKendaraan = new ArrayList<>();
            bKendaraan.add(TransaksiAngkut.KEBUN);
            bKendaraan.add(TransaksiAngkut.LUAR);
            ArrayAdapter<String> adapterKendaran = new ArrayAdapter<String>(context,
                    android.R.layout.simple_dropdown_item_1line, bKendaraan);
            lsKendaraan.setAdapter(adapterKendaran);
            lsKendaraan.setText(bKendaraan.get(TransaksiAngkut.KENDARAAN_KEBUN));

            etSpk.setAdapter(adapterSpk);
            etSpk.setThreshold(1);
            adapterSpk.notifyDataSetChanged();

            etLangsiran.setAdapter(adapterLangsiran);
            etLangsiran.setThreshold(1);
            adapterLangsiran.notifyDataSetChanged();
        }

        public void setValue(PengirimanGanda value,int position) {
            Gson gson = new Gson();
            int ke = position + 1;
            tvUnit.setText(String.format("Unit Ke - %,d",ke));
            tvOperator.setText(String.format("Operator Ke - %,d",ke));
            lsKendaraan.setText(bKendaraan.get(value.getKendaraanDari()));
            etDriver.setText(value.getOperator() != null ? value.getOperator().getNama() : "");
            etVehicle.setText(value.getVehicle() == null ? "" :  value.getVehicle().getVehicleName() != null ? value.getVehicle().getVehicleName() :
                    value.getVehicle().getVraOrderNumber() != null ? value.getVehicle().getVraOrderNumber() :
                    value.getVehicle().getVehicleCode() != null ? value.getVehicle().getVehicleCode() :
                            ""
            );

            if(lsKendaraan.getText().toString().equalsIgnoreCase(bKendaraan.get(TransaksiAngkut.KENDARAAN_KEBUN))){
                etDriver.setAdapter(adapterOperator);
                etDriver.setThreshold(1);
                adapterOperator.notifyDataSetChanged();

                etVehicle.setAdapter(adapterVehicle);
                etVehicle.setThreshold(1);
                adapterVehicle.notifyDataSetChanged();
            }else{
                lSPK.setVisibility(View.VISIBLE);
            }

            if(value.getVehicle() != null) {
                if (value.getVehicle().getVehicleCode() != null) {
                    etVehicle.setText(
                            TransaksiAngkutHelper.showVehicle(value.getVehicle())
                    );
                    tvValueUnit.setText(gson.toJson(value.getVehicle()));
                }
            }

            if(value.getOperator() != null) {
                if (value.getOperator().getNama() != null) {
                    etDriver.setText(value.getOperator().getNama());
                    tvValueOperator.setText(gson.toJson(value.getOperator()));
                }
            }

            if(value.getSpk() != null){
                if(value.getSpk().getIdSPK() != null){
                    etSpk.setText(value.getSpk().getIdSPK());
                    tvValueSpk.setText(gson.toJson(value.getSpk()));
                }
            }

            if(value.getLangsiran() != null){
                if(value.getLangsiran().getIdTujuan() != null){
                    etLangsiran.setText(value.getLangsiran().getIdTujuan());
                    tvValueLangsiran.setText(gson.toJson(value.getLangsiran()));
                }
            }

            lsKendaraan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                    if(i == TransaksiAngkut.KENDARAAN_KEBUN){
                        etDriver.setAdapter(adapterOperator);
                        etDriver.setThreshold(1);
                        adapterOperator.notifyDataSetChanged();
                        if(((SelectOperatorAdapter) adapterOperator).getCount() > 0) {
                            tvValueOperator.setText(gson.toJson(((SelectOperatorAdapter) adapterOperator).getItemAt(0)));
                        }else{
                            tvValueOperator.setText("");
                        }

                        etVehicle.setAdapter(adapterVehicle);
                        etVehicle.setThreshold(1);
                        adapterVehicle.notifyDataSetChanged();
                        if(((SelectVehicleAdapter) adapterVehicle).getCount() > 0) {
                            tvValueUnit.setText(gson.toJson(((SelectVehicleAdapter) adapterVehicle).getItemAt(0)));
                        }else{
                            tvValueUnit.setText("");
                        }

                        etSpk.setText("");
                        tvValueSpk.setText("");
                        lSPK.setVisibility(View.GONE);
                    }else if(i == TransaksiAngkut.KENDARAAN_LUAR){
                        etDriver.setText("");
                        etDriver.setAdapter(null);
                        etDriver.setThreshold(0);
                        tvValueOperator.setText("");

                        etVehicle.setText("");
                        etVehicle.setAdapter(null);
                        etVehicle.setThreshold(0);
                        tvValueUnit.setText("");

                        etSpk.setText("");
                        tvValueSpk.setText("");
                        lSPK.setVisibility(View.VISIBLE);
                    }
                }
            });

            etVehicle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(b)
                        etVehicle.showDropDown();
                }
            });
            etVehicle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etVehicle.setText("");
                    etVehicle.showDropDown();
                }
            });
            etVehicle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Vehicle vehicle = ((SelectVehicleAdapter) parent.getAdapter()).getItemAt(position);
                    tvValueUnit.setText(gson.toJson(vehicle));
                    etVehicle.setText(TransaksiAngkutHelper.showVehicle(vehicle));
                    updatePengirimanGanda();
                }
            });
            etVehicle.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (lsKendaraan.getText().toString().equals(TransaksiAngkut.LUAR)) {
                        Vehicle veh = new Vehicle();
                        veh.setVehicleCode(etVehicle.getText().toString());
                        tvValueUnit.setText(gson.toJson(veh));
                        updatePengirimanGanda();
                    }
                }
            });

            etDriver.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(b)
                        etDriver.showDropDown();
                }
            });
            etDriver.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etDriver.setText("");
                    etDriver.showDropDown();
                }
            });
            etDriver.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Operator operator = ((SelectOperatorAdapter) parent.getAdapter()).getItemAt(position);
                    tvValueOperator.setText(gson.toJson(operator));
                    etDriver.setText(operator.getNama());
                    updatePengirimanGanda();
                }
            });
            etDriver.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (lsKendaraan.getText().toString().equals(TransaksiAngkut.LUAR)) {
                        Operator ope = new Operator();
                        ope.setNama(etDriver.getText().toString());
                        tvValueOperator.setText(gson.toJson(ope));
                        updatePengirimanGanda();
                    }
                }
            });

            etSpk.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(b)
                        etSpk.showDropDown();
                }
            });

            etSpk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etSpk.setText("");
                    etSpk.showDropDown();
                }
            });

            etSpk.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    SPK spk = ((SelectSpkAdapter) parent.getAdapter()).getItemAt(position);
                    tvValueSpk.setText(gson.toJson(spk));
                    etSpk.setText(spk.getIdSPK());
                    updatePengirimanGanda();
                }
            });

            etSpk.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(s.toString().isEmpty()){
                        tvValueSpk.setText("");
                        updatePengirimanGanda();
                    }
                }
            });

            etLangsiran.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(b)
                        etLangsiran.showDropDown();
                }
            });

            etLangsiran.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etLangsiran.setText("");
                    tvValueLangsiran.setText("");
                    etLangsiran.showDropDown();
                }
            });

            etLangsiran.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Langsiran langsiran = ((SelectLangsiranAdapter) parent.getAdapter()).getItemAt(position);
                    tvValueLangsiran.setText(gson.toJson(langsiran));
                    etLangsiran.setText(langsiran.getNamaTujuan());
                    updatePengirimanGanda();
                }
            });

            etSpk.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(s.toString().isEmpty()){
                        tvValueSpk.setText("");
                        updatePengirimanGanda();
                    }
                }
            });
        }
    }
}
