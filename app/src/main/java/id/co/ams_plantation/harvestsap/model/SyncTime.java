package id.co.ams_plantation.harvestsap.model;

import id.co.ams_plantation.harvestsap.connection.Tag;

public class SyncTime {
    public static final String UPLOAD_TRANSAKSI = "UPLOAD_TRANSAKSI";
    public static final String SYNC_DATA_TRANSAKSI = "SYNC_DATA_TRANSAKSI";
    public static final String SYNC_DATA_MASTER = "SYNC_DATA_MASTER";

    String namaListCekBox;
    Long timeSync;
    boolean selected;
    Tag lastTag;
    boolean finish;

    public SyncTime(String namaListCekBox, Long timeSync, boolean selected, boolean finish) {
        this.namaListCekBox = namaListCekBox;
        this.timeSync = timeSync;
        this.selected = selected;
        this.finish = finish;
    }

    public SyncTime(String namaListCekBox, Long timeSync, boolean selected) {
        this.namaListCekBox = namaListCekBox;
        this.timeSync = timeSync;
        this.selected = selected;
        this.finish = false;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getNamaListCekBox() {
        return namaListCekBox;
    }

    public void setNamaListCekBox(String namaListCekBox) {
        this.namaListCekBox = namaListCekBox;
    }

    public Long getTimeSync() {
        return timeSync;
    }

    public void setTimeSync(Long timeSync) {
        this.timeSync = timeSync;
    }

    public Tag getLastTag() {
        return lastTag;
    }

    public void setLastTag(Tag lastTag) {
        this.lastTag = lastTag;
    }

    public boolean isFinish() {
        return finish;
    }

    public void setFinish(boolean finish) {
        this.finish = finish;
    }
}
