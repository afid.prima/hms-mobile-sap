package id.co.ams_plantation.harvestsap.util;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.ObjectRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import id.co.ams_plantation.harvestsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.Pemanen;
import id.co.ams_plantation.harvestsap.ui.PemanenActivity;
import jp.wasabeef.recyclerview.adapters.SlideInLeftAnimationAdapter;

/**
 * Created by user on 12/12/2018.
 */

public class PemanenHelper {

    AlertDialog listrefrence;
    ArrayList<Pemanen> filter;
    ArrayList<Pemanen> origin;
    FragmentActivity activity;

    AppCompatEditText etUrut;
    AppCompatEditText etKemandoran;
    AppCompatEditText etNama;
    AppCompatEditText etGang;
    Button btnCancel;
    Button btnSave;
    Pemanen selectedPemanen;

    public PemanenHelper(FragmentActivity activity) {
        this.activity = activity;
    }

    public ArrayList<Pemanen> getListPemanenByGang(HashMap<String,Pemanen> allPemanen,String kodeGang){
        ArrayList<Pemanen> pemanenArrayList = new ArrayList<>();
        for(HashMap.Entry<String,Pemanen> entry : allPemanen.entrySet()){
            if(entry.getValue().getGank().equals(kodeGang)){
                pemanenArrayList.add(entry.getValue());
            }
        }
        return pemanenArrayList;
    }

    public ArrayList<Pemanen> getListPemanenByGangWithCompare(HashMap<String,Pemanen> allPemanen,ArrayList<String> compareKodePemanen,String kodeGang){
        ArrayList<Pemanen> pemanenArrayList = new ArrayList<>();
        for(HashMap.Entry<String,Pemanen> entry : allPemanen.entrySet()){
            if(entry.getValue().getGank() != null) {
                if (entry.getValue().getGank().equals(kodeGang)) {
                    pemanenArrayList.add(entry.getValue());
                }
            }
        }
        return pemanenArrayList;
    }

    public void chosePemanen(String gang){
        origin = new ArrayList<>();
        filter = new ArrayList<>();

//        ArrayList<Pemanen> data = new ArrayList<>();
//
        Long start = System.currentTimeMillis();
//        WaspHash hash = GlobalHelper.getTableHash(GlobalHelper.TABEL_PEMANEN);
//        for (Object object : hash.getAllValues()){
//            data.add(object.toString());
//        }
//        Log.d("WaspDB read ", String.valueOf(System.currentTimeMillis() - start));
//
//        data = new ArrayList<>();
//
//        start = System.currentTimeMillis();
//        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_PEMANEN);
//        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
//        Iterable<DataNitrit> iterable = repository.find().project(DataNitrit.class);
//        for (Iterator iterator = iterable.iterator(); iterator.hasNext();) {
//            DataNitrit dataNitrit = (DataNitrit) iterator.next();
//            data.add(dataNitrit.getValueDataNitrit());
//        }
//        db.close();
//        Log.d("nitrit read ", String.valueOf(System.currentTimeMillis() - start));

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_PEMANEN );
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = iterable.iterator(); iterator.hasNext();) {
            Gson gson = new Gson();
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Pemanen pemanen = gson.fromJson(dataNitrit.getValueDataNitrit(), Pemanen.class);
            if (pemanen.getGank().equals(gang)) {
                origin.add(gson.fromJson(dataNitrit.getValueDataNitrit(), Pemanen.class));
                filter.add(gson.fromJson(dataNitrit.getValueDataNitrit(), Pemanen.class));
            }
        }
        db.close();
        Log.d("nitrit read ", String.valueOf(System.currentTimeMillis() - start));

        View view = LayoutInflater.from(activity).inflate(R.layout.list_refrensi_object,null);
        TextView idHeader = (TextView) view.findViewById(R.id.idHeader);
        EditText etSearch_lso = (EditText) view.findViewById(R.id.et_search_lso);
        RecyclerView rv_usage_lso = (RecyclerView) view.findViewById(R.id.rv_usage_lso);

        idHeader.setText(activity.getResources().getString(R.string.chose_pemanen));
        rv_usage_lso.setLayoutManager(new GridLayoutManager(activity,2));

        final ChoseTphAdapter adapter = new ChoseTphAdapter(activity);
        final SlideInLeftAnimationAdapter adapterLeft = new SlideInLeftAnimationAdapter(adapter);
        adapterLeft.setFirstOnly(false);
//       adapterLeft.setInterpolator(new OvershootInterpolator(1f));
        adapterLeft.setDuration(400);
        etSearch_lso.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s.toString());
            }
        });
        rv_usage_lso.setAdapter(adapterLeft);

        listrefrence = WidgetHelper.showListReference(listrefrence,view,activity);
    }

    public void showFormPemanen(Pemanen pemanen,View view){
        selectedPemanen = pemanen;
        if(etUrut == null) {
             etUrut = (AppCompatEditText) view.findViewById(R.id.etUrut);
             etKemandoran = (AppCompatEditText) view.findViewById(R.id.etKemandoran);
             etNama = (AppCompatEditText) view.findViewById(R.id.etNama);
             etGang = (AppCompatEditText) view.findViewById(R.id.etGang);
             btnCancel = (Button) view.findViewById(R.id.btnCancel);
             btnSave = (Button) view.findViewById(R.id.btnSave);
        }

        etKemandoran.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(activity instanceof PemanenActivity){
                    ((PemanenActivity) activity).closeForm(false);
                    GlobalHelper.hideKeyboard(activity);
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GlobalHelper.hideKeyboard(activity);
                new LongOperation().execute();
            }
        });

        etUrut.setText(String.valueOf(pemanen.getNoUrut()));
        etKemandoran.setText(pemanen.getKemandoran() == null ? "" : pemanen.getKemandoran());
        etNama.setText(pemanen.getNama());
        etGang.setText(pemanen.getGank());

//        listrefrence = WidgetHelper.showFormDialog(listrefrence,view,activity);
    }

    public boolean validationPemanen(){
        if(etKemandoran.getText().toString().isEmpty()){
            Toast.makeText(HarvestApp.getContext(),"Mohon Input Kemandoran",Toast.LENGTH_SHORT).show();
            etKemandoran.requestFocus();
            return false;
        }

        if(etUrut.getText().toString().isEmpty()){
            Toast.makeText(HarvestApp.getContext(),"Mohon Input No Urut",Toast.LENGTH_SHORT).show();
            etUrut.requestFocus();
            return false;
        }
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_PEMANEN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = iterable.iterator(); iterator.hasNext();) {
            Gson gson = new Gson();
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Pemanen pemanen = gson.fromJson(dataNitrit.getValueDataNitrit(), Pemanen.class);

            if(pemanen.getKemandoran() == null && pemanen.getNoUrut() == 0){

            }else if((pemanen.getGank().equals(selectedPemanen.getGank())) && (pemanen.getNoUrut() == Integer.parseInt(etUrut.getText().toString()))){
                Toast.makeText(HarvestApp.getContext(),"Nomor Sudah Ada Dipakai Oleh "+ pemanen.getNama(),Toast.LENGTH_SHORT).show();
                db.close();
                return false;
            }
        }
        db.close();

        selectedPemanen.setKemandoran(etKemandoran.getText().toString());
        selectedPemanen.setNoUrut(Integer.parseInt(etUrut.getText().toString()));
        selectedPemanen.setStatus(Pemanen.Pemanen_NotUplaod);
        return true;
    }

    public boolean savePemanen(){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_PEMANEN);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        DataNitrit dataNitrit = null;
        Gson gson = new Gson();
        dataNitrit = new DataNitrit(selectedPemanen.getKodePemanen(),gson.toJson(selectedPemanen));
        repository.update(dataNitrit);
        db.close();
        return true;
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        AlertDialog alertDialogAllpoin;
        boolean validasi;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(activity, activity.getResources().getString(R.string.wait));
            validasi = validationPemanen();
        }

        @Override
        protected String doInBackground(String... strings) {
            if(validasi){
                if(savePemanen()){
                    return "1";
                }
            }
            return "0";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(alertDialogAllpoin!=null){
                alertDialogAllpoin.dismiss();
            }
            switch (Integer.parseInt(result)){
                case 0:
                    if(validasi) {
                        Toast.makeText(HarvestApp.getContext(), "Gagal UpdatePemanen", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case 1:
//                    if(listrefrence != null){
//                        listrefrence.dismiss();
//                    }
                    if(activity instanceof PemanenActivity){
                        ((PemanenActivity) activity).closeForm(true);
                    }
                    Toast.makeText(HarvestApp.getContext(),"Berhasil UpdatePemanen", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

    class ChoseTphAdapter extends RecyclerView.Adapter<ChoseTphAdapter.Holder> {
        Context contextApdater;

        public ChoseTphAdapter(Context context) {
            this.contextApdater = context;
        }

        @Override
        public ChoseTphAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(contextApdater).inflate(R.layout.row_infowindow_chose,parent,false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(Holder holder, int position) {
            holder.setValue(filter.get(position));
        }

        @Override
        public int getItemCount() {
            return filter != null ? filter.size(): 0 ;
        }

        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults results = new FilterResults();
                    filter = new ArrayList<>();
                    String string = constraint.toString().toLowerCase();
                    for (Pemanen pemanen : origin) {
                        if(pemanen.getNama().toLowerCase().contains(string) || pemanen.getGank().toLowerCase().contains(string)){
                            filter.add(pemanen);
                        }
                    }
                    if (filter.size() > 0) {
                        results.count = filter.size();
                        results.values = filter;
                        return results;
                    } else {
                        return null;
                    }
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null) {
                        filter = (ArrayList<Pemanen>) results.values;
                    } else {
                        filter.clear();
                    }
                    notifyDataSetChanged();

                }
            };
        }

        public class Holder extends RecyclerView.ViewHolder {
            TextView tv_riw_chose;
            TextView tv_riw_chose1;
            CardView cv_riw_chose;
            public Holder(View itemView) {
                super(itemView);
                cv_riw_chose = (CardView) itemView.findViewById(R.id.cv_riw_chose);
                tv_riw_chose = (TextView) itemView.findViewById(R.id.tv_riw_chose);
                tv_riw_chose1 = (TextView) itemView.findViewById(R.id.tv_riw_chose1);
            }

            public void setValue(Pemanen value) {
                tv_riw_chose.setText(value.getNama());
                tv_riw_chose1.setText(value.getGank());
                cv_riw_chose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Fragment fragment = (Fragment) activity.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        if(fragment instanceof TphInputFragment){
//                            ((TphInputFragment) fragment).setPemanen(value);
                        }
                        listrefrence.dismiss();
                    }
                });
            }
        }
    }
}
