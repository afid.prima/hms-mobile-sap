package id.co.ams_plantation.harvestsap.TableView.view_model;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewColumnHeader;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestsap.model.Operator;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiSpbHelper;

/**
 * Created by user on 12/27/2018.
 */

public class TSpbTableViewModel {
    private final Context mContext;
    private final ArrayList<TransaksiSPB> transaksiSPBS;
    String [] HeaderColumn = {"Create","Approve","Operator","Tujuan","Waktu","Janjang","Brondolan"};
    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

    public TSpbTableViewModel(Context context, HashMap<String,TransaksiSPB> hTAngkut) {
        mContext = context;
        transaksiSPBS = new ArrayList<>();
        transaksiSPBS.addAll(hTAngkut.values());
    }

    private List<TableViewRowHeader> getSimpleRowHeaderList() {
        List<TableViewRowHeader> list = new ArrayList<>();
        //"angkut"
        for (int i = 0; i < transaksiSPBS.size(); i++) {
            String id = i + "-" + transaksiSPBS.get(i).getIdSPB()+ "-" + transaksiSPBS.get(i).getStatus() ;
            TableViewRowHeader header = new TableViewRowHeader(id, transaksiSPBS.get(i).getNoSpb() != null ? transaksiSPBS.get(i).getNoSpb() : TransaksiSpbHelper.converIdSPBToNoSPB(transaksiSPBS.get(i).getIdSPB()));
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<TableViewColumnHeader> getRandomColumnHeaderList() {
        List<TableViewColumnHeader> list = new ArrayList<>();

        for (int i = 0; i < HeaderColumn.length; i++) {
            TableViewColumnHeader header = new TableViewColumnHeader(String.valueOf(i), HeaderColumn[i]);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<List<TableViewCell>> getCellListForSortingTest() {
        List<List<TableViewCell>> list = new ArrayList<>();
        for (int i = 0; i < transaksiSPBS.size(); i++) {
            List<TableViewCell> cellList = new ArrayList<>();
            for (int j = 0; j < HeaderColumn.length; j++) {
                // Create dummy id.
                String id = j + "-" + transaksiSPBS.get(i).getIdSPB()+ "-" + transaksiSPBS.get(i).getStatus() ;

                TableViewCell cell = null;
                User user = null;
                switch (j){
                    case 0:{
                        if(mContext instanceof MainMenuActivity){
                            user = GlobalHelper.dataAllUser.get(transaksiSPBS.get(i).getCreateBy());
                            if(user != null){
                                cell = new TableViewCell(id, user.getUserFullName());
                            }else{
                                cell = new TableViewCell(id, transaksiSPBS.get(i).getCreateBy());
                            }
                        }else{
                            cell = new TableViewCell(id, transaksiSPBS.get(i).getCreateBy());
                        }
                        break;
                    }
                    case 1:{
                        if(mContext instanceof MainMenuActivity){
                            user = GlobalHelper.dataAllUser.get(transaksiSPBS.get(i).getApproveBy());
                            if(user != null){
                                cell = new TableViewCell(id, user.getUserFullName());
                            }else{
                                cell = new TableViewCell(id,transaksiSPBS.get(i).getApproveBy());
                            }
                        }else{
                            cell = new TableViewCell(id,transaksiSPBS.get(i).getApproveBy());
                        }
                        break;
                    }
                    case 2:{
                        Operator operator = transaksiSPBS.get(i).getTransaksiAngkut().getSupir();
                        cell = new TableViewCell(id, operator != null ? operator.getNama() :"");
                        break;
                    }
                    case 3:{
                        cell = new TableViewCell(id, transaksiSPBS.get(i).getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_KELUAR ? TransaksiAngkut.KELAUR :
                                transaksiSPBS.get(i).getTransaksiAngkut().getTujuan() == TransaksiAngkut.TUJUAN_PKS ? transaksiSPBS.get(i).getTransaksiAngkut().getPks().getNamaPKS() :
                                        "-");
                        break;
                    }
                    case 4:{
                        cell = new TableViewCell(id, dateFormat.format(transaksiSPBS.get(i).getCreateDate()));
                        break;
                    }
                    case 5:{
                        cell = new TableViewCell(id, transaksiSPBS.get(i).getTotalJanjang());
                        break;
                    }
                    case 6:{
                        cell = new TableViewCell(id,transaksiSPBS.get(i).getTotalBrondolan());
                        break;
                    }

                }
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }

    private static String getRandomString() {
        Random r = new Random();
        String str = " a ";
        for (int i = 0; i < r.nextInt(); i++) {
            str = str + " a ";
        }

        return str;
    }

    public List<List<TableViewCell>> getCellList() {
        return getCellListForSortingTest();
    }

    public List<TableViewRowHeader> getRowHeaderList() {
        return getSimpleRowHeaderList();
    }

    public List<TableViewColumnHeader> getColumnHeaderList() {
        return getRandomColumnHeaderList();
    }

}
