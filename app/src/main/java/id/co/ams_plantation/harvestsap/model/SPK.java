package id.co.ams_plantation.harvestsap.model;

import java.util.ArrayList;

public class SPK{

	public static final int descriptionAngkut = 0;
	public static final int descriptionPanen = 1;

	public static String [] descriptionName = {
			"ANGKUT",
			"PANEN"
	};

	private String companyCode;
	private String documentDate;
	private Object lastTime;
	private Object flag;
	private String idSPK;
	private String estCode;
	private String materialGroup;
	private String materialGroupDesc;
	private String lastUpdate;
	private String description;
	private Object isActive;
	private String vendorCode;
	private Object lastUpdateBy;
	private String shortText;
	private long validFrom;
	private long validTo;
	private ArrayList<String> block;

	public void setCompanyCode(String companyCode){
		this.companyCode = companyCode;
	}

	public String getCompanyCode(){
		return companyCode;
	}

	public void setDocumentDate(String documentDate){
		this.documentDate = documentDate;
	}

	public String getDocumentDate(){
		return documentDate;
	}

	public void setLastTime(Object lastTime){
		this.lastTime = lastTime;
	}

	public Object getLastTime(){
		return lastTime;
	}

	public void setFlag(Object flag){
		this.flag = flag;
	}

	public Object getFlag(){
		return flag;
	}

	public void setIdSPK(String idSPK){
		this.idSPK = idSPK;
	}

	public String getIdSPK(){
		return idSPK;
	}

	public void setEstCode(String estCode){
		this.estCode = estCode;
	}

	public String getEstCode(){
		return estCode;
	}

	public void setMaterialGroup(String materialGroup){
		this.materialGroup = materialGroup;
	}

	public String getMaterialGroup(){
		return materialGroup;
	}

	public void setLastUpdate(String lastUpdate){
		this.lastUpdate = lastUpdate;
	}

	public String getLastUpdate(){
		return lastUpdate;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setIsActive(Object isActive){
		this.isActive = isActive;
	}

	public Object getIsActive(){
		return isActive;
	}

	public void setVendorCode(String vendorCode){
		this.vendorCode = vendorCode;
	}

	public String getVendorCode(){
		return vendorCode;
	}

	public void setLastUpdateBy(Object lastUpdateBy){
		this.lastUpdateBy = lastUpdateBy;
	}

	public Object getLastUpdateBy(){
		return lastUpdateBy;
	}

	public String getMaterialGroupDesc() {
		return materialGroupDesc;
	}

	public void setMaterialGroupDesc(String materialGroupDesc) {
		this.materialGroupDesc = materialGroupDesc;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public ArrayList<String> getBlock() {
		return block;
	}

	public void setBlock(ArrayList<String> block) {
		this.block = block;
	}

	public Long getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Long validFrom) {
		this.validFrom = validFrom;
	}

	public Long getValidTo() {
		return validTo;
	}

	public void setValidTo(Long validTo) {
		this.validTo = validTo;
	}
}
