package id.co.ams_plantation.harvestsap.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.HashMap;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.connection.Tag;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.LastSyncTime;
import id.co.ams_plantation.harvestsap.model.SyncTime;

public class SyncTimeHelper {

    public static boolean cekSyncTime(Tag tag){

        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, Context.MODE_PRIVATE);
        String sync_time = prefer.getString(HarvestApp.SYNC_TIME,null);
        if(sync_time == null){
            return false;
        }
        Gson gson = new Gson();
        LastSyncTime lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
        HashMap<String,SyncTime> syncTimeHashMap=lastSyncTime.getSyncTimeHashMap();
        SyncTime syncTime;
        switch (tag){
            case InsertGerdangDetail:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case InsertOpnameNFC:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostPemanen:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostTphImage:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostTph:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostLangsiranImage:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostLangsiran:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostTransaksiPanenImage:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostTransaksiPanen:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostTransaksiPanenNFC:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostTransaksiAngkutImage:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostTransaksiAngkut:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostTransaksiAngkutNFC:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostTransaksiSPBImage:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostTransaksiSPB:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostSupervisionPanen:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostTransaksiCounter:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostQcBuahImages:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostQcBuah:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostSensusBjrImages:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostSensusBjr:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostQcAncakImages:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case PostQcAncak:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;

            case GetTransaksiPanenListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetTransaksiAngkutListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetTransaksiSpbListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetSensusBJRByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetQcBuahByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetQCMutuAncakByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetTPanenCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetSPBCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetTAngkutCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetTPanenSupervisionCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetQCMutuAncakHeaderCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;


            case GetTphListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetLangsiranListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetListPKSByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetMasterUserByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetOpnameNFCByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetKonfigurasiGerdang:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetTPHReasonUnharvestMaster:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetBlockMekanisasi:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetPemanenListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetOperatorListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetVehicleListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetAfdelingAssistantByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetApplicationConfiguration:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetSupervisionListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetBJRInformation:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetISCCInformation:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetMasterSPKByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetMasterCagesByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if (!syncTime.isSelected()) {
                return true;
                }
                break;
            case GetTPHCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
            case GetLangsiranCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                if(!syncTime.isSelected()){
                    return true;
                }
                break;
        }
        return false;
    }

    public static void updateSyncTime(Tag tag){
        SharedPreferences prefer = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME, Context.MODE_PRIVATE);
        String sync_time = prefer.getString(HarvestApp.SYNC_TIME,null);
        Gson gson = new Gson();
        LastSyncTime lastSyncTime = gson.fromJson(sync_time,LastSyncTime.class);
        if(sync_time == null){
            return;
        }

        HashMap<String,SyncTime> syncTimeHashMap = lastSyncTime.getSyncTimeHashMap();
        SyncTime syncTime;
        switch (tag){
            case InsertGerdangDetail:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case InsertOpnameNFC:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostPemanen:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostTphImage:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostTph:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostLangsiranImage:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostLangsiran:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostTransaksiPanenImage:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostTransaksiPanen:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostTransaksiPanenNFC:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostTransaksiAngkutImage:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostTransaksiAngkut:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostTransaksiAngkutNFC:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostTransaksiSPBImage:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostTransaksiSPB:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostSupervisionPanen:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostTransaksiCounter:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostQcBuahImages:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostQcBuah:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostSensusBjrImages:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostSensusBjr:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostQcAncakImages:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;
            case PostQcAncak:
                syncTime = syncTimeHashMap.get(SyncTime.UPLOAD_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.UPLOAD_TRANSAKSI,syncTime);
                break;


            case GetTransaksiPanenListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;
            case GetTransaksiAngkutListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;
            case GetTransaksiSpbListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;
            case GetSensusBJRByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;
            case GetQcBuahByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;
            case GetQCMutuAncakByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;
            case GetTPanenCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;
            case GetSPBCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;
            case GetTAngkutCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;
            case GetTPHCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;
            case GetLangsiranCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;
            case GetTPanenSupervisionCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;
            case GetQCMutuAncakHeaderCounterByUserID:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_TRANSAKSI);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_TRANSAKSI,syncTime);
                break;


            case GetTphListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetLangsiranListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetListPKSByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetMasterUserByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetOpnameNFCByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetKonfigurasiGerdang:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetTPHReasonUnharvestMaster:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetBlockMekanisasi:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetPemanenListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetOperatorListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetVehicleListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetAfdelingAssistantByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetApplicationConfiguration:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetSupervisionListByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetBJRInformation:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetISCCInformation:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetMasterSPKByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
            case GetMasterCagesByEstate:
                syncTime = syncTimeHashMap.get(SyncTime.SYNC_DATA_MASTER);
                syncTime.setTimeSync(System.currentTimeMillis());
                syncTime.setLastTag(tag);
                syncTimeHashMap.put(SyncTime.SYNC_DATA_MASTER,syncTime);
                break;
        }

        lastSyncTime.setSyncTimeHashMap(syncTimeHashMap);
        SharedPreferences.Editor editor = prefer.edit();
        editor.putString(HarvestApp.SYNC_TIME,gson.toJson(lastSyncTime));
        editor.apply();
    }

    public static boolean cekDbTranskasi(String dbName){
        boolean tblTransaksi = false;
        dbName = dbName.replace(".db","");
        dbName = Encrypts.decrypt(dbName);
        switch (dbName){
//            case GlobalHelper.TABEL_TPH:
//                tblTransaksi = true;
//                break;
//            case GlobalHelper.TABLE_LANGSIRAN:
//                tblTransaksi = true;
//                break;
            case GlobalHelper.TABLE_TRANSAKSI_TPH:
                tblTransaksi = true;
                break;
            case GlobalHelper.TABLE_ANGKUT:
                tblTransaksi = true;
                break;
            case GlobalHelper.TABLE_ANGKUT_SPB:
                tblTransaksi = true;
                break;
            case GlobalHelper.TABLE_TRANSAKSI_ANGKUT:
                tblTransaksi = true;
                break;
            case GlobalHelper.TABLE_TRANSAKSI_SPB:
                tblTransaksi = true;
                break;
            case GlobalHelper.TABLE_QC_ANCAK:
                tblTransaksi = true;
                break;
            case GlobalHelper.TABLE_QC_ANCAK_POHON:
                tblTransaksi = true;
                break;
            case GlobalHelper.TABLE_QC_BJR:
                tblTransaksi = true;
                break;
            case GlobalHelper.TABLE_QC_BUAH:
                tblTransaksi = true;
                break;
        }
        return tblTransaksi;
    }

    public static boolean cekDbMaster(String dbName){
        boolean tbl = false;
        dbName = dbName.replace(".db","");
        dbName = Encrypts.decrypt(dbName);
        switch (dbName){
            case GlobalHelper.TABEL_TPH:
                tbl = true;
                break;
            case GlobalHelper.TABEL_PEMANEN:
                tbl = true;
                break;
            case GlobalHelper.TABLE_LANGSIRAN:
                tbl = true;
                break;
            case GlobalHelper.TABLE_USER_HARVEST:
                tbl = true;
                break;
            case GlobalHelper.TABLE_PKS:
                tbl = true;
                break;
            case GlobalHelper.TABEL_OPERATOR:
                tbl = true;
                break;
            case GlobalHelper.TABLE_VEHICLE:
                tbl = true;
                break;
            case GlobalHelper.TABLE_AFDELING_ASSISTANT:
                tbl = true;
                break;
            case GlobalHelper.TABLE_APPLICATION_CONFIGURATION:
                tbl = true;
                break;
            case GlobalHelper.TABLE_TPANENGANGLIST:
                tbl = true;
                break;
            case GlobalHelper.TABLE_TPANENSUPERVISIONLIST:
                tbl = true;
                break;
            case GlobalHelper.TABLE_BJRINFORMATION:
                tbl = true;
                break;
            case GlobalHelper.TABLE_ISCCINFORMATION:
                tbl = true;
                break;
        }
        return tbl;
    }

    public static boolean cekFolderTransaksi(String folderName){
        boolean folder = false;
        if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TRANSAKSI_TPH])){
            folder = true;
        } else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_ANGKUT])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QC_SENSUS_BJR])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SPB])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_ANGKUT_SPB])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_SPB])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK_POHON])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QC_MUTU_BUAH])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QC_MUTU_ANCAK])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TRANSAKSI_TPH_NFC])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT_NFC])){
            folder = true;
        }
        return folder;
    }

    public static boolean cekFolderMaster(String folderName){
        boolean folder = false;
        if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QR])){
            folder = true;
        }if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TPH])){
            folder = true;
        }if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_LANGSIRAN])){
            folder = true;
        }if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_LANGSIRAN])){
            folder = true;
        }else if(folderName.equalsIgnoreCase(GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TPanenSupervision])){
            folder = true;
        }
        return folder;
    }

    public static void updateFinishSyncTime(String namaListCekBox){

        SharedPreferences pref = HarvestApp.getContext().getSharedPreferences(HarvestApp.SYNC_TIME,Context.MODE_PRIVATE);
        String sync_time = pref.getString(HarvestApp.SYNC_TIME,null);
        Gson gson = new Gson();
        LastSyncTime lastSyncTime = gson.fromJson(sync_time, LastSyncTime.class);
        HashMap<String, SyncTime> syncTimeHashMap = lastSyncTime.getSyncTimeHashMap();
        SyncTime syncTime = syncTimeHashMap.get(namaListCekBox);
        if(syncTime.isSelected()) {
            syncTime.setSelected(false);
            syncTime.setFinish(true);
            syncTime.setTimeSync(System.currentTimeMillis());
        }

        syncTimeHashMap.put(syncTime.getNamaListCekBox(),syncTime);
        lastSyncTime.setSyncTimeHashMap(syncTimeHashMap);
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(HarvestApp.SYNC_TIME,gson.toJson(lastSyncTime));
        edit.apply();
    }
}
