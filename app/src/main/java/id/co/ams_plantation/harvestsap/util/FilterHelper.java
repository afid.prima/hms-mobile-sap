package id.co.ams_plantation.harvestsap.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestsap.Fragment.AngkutFragment;
import id.co.ams_plantation.harvestsap.Fragment.RekonsilasiListFragment;
import id.co.ams_plantation.harvestsap.Fragment.SpbFragment;
import id.co.ams_plantation.harvestsap.Fragment.TphFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.FilterAfdeling;
import id.co.ams_plantation.harvestsap.model.FilterMenu;
import id.co.ams_plantation.harvestsap.model.FilterTransaction;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

public class FilterHelper {
    Context context;
    AlertDialog alertDialog;
    ArrayList<FilterAfdeling> listAfdeling;
    ArrayList<FilterMenu> listMenu;
    MainMenuActivity mainMenuActivity;

    public static final String No_Filter_Afdeling = "All Afdeling";
    public static final String No_Filter_User = "Semua Input User";

    public FilterHelper(Context context) {
        this.context = context;
    }

    public void setFilterView(Fragment fragment){
        if(context instanceof MainMenuActivity){
            mainMenuActivity = (MainMenuActivity) context;
        }
        SharedPreferences preferencesFILTER_TRANSAKSI = HarvestApp.getContext().getSharedPreferences(HarvestApp.FILTER_TRANSAKSI, Context.MODE_PRIVATE);
        String filterTransakti = preferencesFILTER_TRANSAKSI.getString(HarvestApp.FILTER_TRANSAKSI,null);
        Gson gson  = new Gson();
        FilterTransaction filterTransaction = gson.fromJson(filterTransakti,FilterTransaction.class);

        if(filterTransaction == null){
            mainMenuActivity.tvFilterAfdeling.setText(No_Filter_Afdeling);
            mainMenuActivity.tvFilterDataInputBy.setText(No_Filter_User);
        }else if (fragment instanceof TphFragment){
            String afdeling = null;
            boolean masuk = false;
            for (int i = 0 ; i < filterTransaction.getFilterMenus().size();i++){
                if(filterTransaction.getFilterMenus().get(i).getMenuName().equals("Panen") && filterTransaction.getFilterMenus().get(i).getSelected()) {
                    for (int j = 0 ; j < filterTransaction.getFilterAfdelings().size();j++) {
                        if(filterTransaction.getFilterAfdelings().get(j).getSelected()) {
                            if (afdeling == null) {
                                afdeling = filterTransaction.getFilterAfdelings().get(j).getAfdeling();
                            } else {
                                afdeling += "," + filterTransaction.getFilterAfdelings().get(j).getAfdeling();
                            }
                        }
                    }
                    mainMenuActivity.tvFilterDataInputBy.setText(filterTransaction.getShowInputBy() == 0  ? No_Filter_User : "Input Sendiri");
                    masuk = true;
                }
            }
            if(masuk) {
                if(afdeling == null){
                    mainMenuActivity.tvFilterAfdeling.setText(No_Filter_Afdeling);
                }else {
                    String[] safdeling = afdeling.split(",");
                    if (safdeling.length == filterTransaction.getFilterAfdelings().size()) {
                        mainMenuActivity.tvFilterAfdeling.setText(No_Filter_Afdeling);
                    } else {
                        mainMenuActivity.tvFilterAfdeling.setText(afdeling);
                    }
                }
            }else{
                mainMenuActivity.tvFilterAfdeling.setText(No_Filter_Afdeling);
                mainMenuActivity.tvFilterDataInputBy.setText(No_Filter_User);
            }
        }else if (fragment instanceof AngkutFragment){
            String afdeling = null;
            boolean masuk = false;
            for (int i = 0 ; i < filterTransaction.getFilterMenus().size();i++){
                if(filterTransaction.getFilterMenus().get(i).getMenuName().equals("Angkut") && filterTransaction.getFilterMenus().get(i).getSelected()) {
                    for (int j = 0 ; j < filterTransaction.getFilterAfdelings().size();j++) {
                        if(filterTransaction.getFilterAfdelings().get(j).getSelected()) {
                            if (afdeling == null) {
                                afdeling = filterTransaction.getFilterAfdelings().get(j).getAfdeling();
                            } else {
                                afdeling += "," + filterTransaction.getFilterAfdelings().get(j).getAfdeling();
                            }
                        }
                    }
                    mainMenuActivity.tvFilterDataInputBy.setText(filterTransaction.getShowInputBy() == 0  ? No_Filter_User : "Input Sendiri");
                    masuk = true;
                }
            }
            if(masuk) {
                if(afdeling == null){
                    mainMenuActivity.tvFilterAfdeling.setText(No_Filter_Afdeling);
                }else {
                    String[] safdeling = afdeling.split(",");
                    if (safdeling.length == filterTransaction.getFilterAfdelings().size()) {
                        mainMenuActivity.tvFilterAfdeling.setText(No_Filter_Afdeling);
                    } else {
                        mainMenuActivity.tvFilterAfdeling.setText(afdeling);
                    }
                }
            }else{
                mainMenuActivity.tvFilterAfdeling.setText(No_Filter_Afdeling);
                mainMenuActivity.tvFilterDataInputBy.setText(No_Filter_User);
            }
        }else if (fragment instanceof SpbFragment){
            String afdeling = null;
            boolean masuk = false;
            for (int i = 0 ; i < filterTransaction.getFilterMenus().size();i++){
                if(filterTransaction.getFilterMenus().get(i).getMenuName().equals("Spb") && filterTransaction.getFilterMenus().get(i).getSelected()) {
                    for (int j = 0 ; j < filterTransaction.getFilterAfdelings().size();j++) {
                        if(filterTransaction.getFilterAfdelings().get(j).getSelected()) {
                            if (afdeling == null) {
                                afdeling = filterTransaction.getFilterAfdelings().get(j).getAfdeling();
                            } else {
                                afdeling += "," + filterTransaction.getFilterAfdelings().get(j).getAfdeling();
                            }
                        }
                    }
                    mainMenuActivity.tvFilterDataInputBy.setText(filterTransaction.getShowInputBy() == 0  ? No_Filter_User : "Input Sendiri");
                    masuk = true;
                }
            }
            if(masuk) {
                if(afdeling == null){
                    mainMenuActivity.tvFilterAfdeling.setText(No_Filter_Afdeling);
                }else {
                    String[] safdeling = afdeling.split(",");
                    if (safdeling.length == filterTransaction.getFilterAfdelings().size()) {
                        mainMenuActivity.tvFilterAfdeling.setText(No_Filter_Afdeling);
                    } else {
                        mainMenuActivity.tvFilterAfdeling.setText(afdeling);
                    }
                }
            }else{
                mainMenuActivity.tvFilterAfdeling.setText(No_Filter_Afdeling);
                mainMenuActivity.tvFilterDataInputBy.setText(No_Filter_User);
            }
        }
    }

    public void showFilterAfdeling(ArrayList<Fragment> arrayListFragment){
        View baseView = LayoutInflater.from(context).inflate(R.layout.filter_layout,null);
        RecyclerView rvAfd = (RecyclerView) baseView.findViewById(R.id.rvAfd);
        RecyclerView rvMenu = (RecyclerView) baseView.findViewById(R.id.rvMenu);
        SegmentedButtonGroup segmentedInputData = (SegmentedButtonGroup) baseView.findViewById(R.id.segmentedInputData);
        LinearLayout lClose = (LinearLayout) baseView.findViewById(R.id.lClose);
        LinearLayout lsave = (LinearLayout) baseView.findViewById(R.id.lsave);

        listAfdeling = new ArrayList<>();
        listMenu = new ArrayList<>();
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.AFDELING_BLOCK, Context.MODE_PRIVATE);
        String afdelingObj = preferences.getString(HarvestApp.AFDELING_BLOCK,null);

        SharedPreferences preferencesFILTER_TRANSAKSI = HarvestApp.getContext().getSharedPreferences(HarvestApp.FILTER_TRANSAKSI, Context.MODE_PRIVATE);
        String filterTransakti = preferencesFILTER_TRANSAKSI.getString(HarvestApp.FILTER_TRANSAKSI,null);
        Gson gson  = new Gson();
        FilterTransaction filterTransaction = gson.fromJson(filterTransakti,FilterTransaction.class);

        try {
            JSONObject objAfdeling = new JSONObject(afdelingObj);
            for(int i = 0; i<objAfdeling.names().length(); i++){
                boolean selected = false;
                if(filterTransaction != null) {
                    for (int j = 0; j < filterTransaction.getFilterAfdelings().size(); j++) {
                        if (filterTransaction.getFilterAfdelings().get(j).getSelected()) {
                            if (objAfdeling.names().get(i).toString().equals(filterTransaction.getFilterAfdelings().get(j).getAfdeling())) {
                                selected = true;
                                break;
                            }
                        }
                    }
                }else{
                    selected = true;
                }
                listAfdeling.add(new FilterAfdeling(objAfdeling.names().get(i).toString(),selected));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < arrayListFragment.size();i++){

            String namaFragment = null;
            if(arrayListFragment.get(i) instanceof TphFragment) {
                namaFragment = "Panen";
            }else if(arrayListFragment.get(i) instanceof AngkutFragment) {
                namaFragment = "Angkut";
            }else if(arrayListFragment.get(i) instanceof SpbFragment) {
                namaFragment = "Spb";
            }else if(arrayListFragment.get(i) instanceof RekonsilasiListFragment){
                namaFragment = "Rekonsiliasi";
            }

            if(namaFragment != null) {
                boolean selected = false;
                if (filterTransaction != null) {
                    for (int j = 0; j < filterTransaction.getFilterMenus().size(); j++) {
                        if (filterTransaction.getFilterMenus().get(j).getSelected()) {
                            if (filterTransaction.getFilterMenus().get(j).getMenuName().equals(namaFragment)) {
                                selected = true;
                                break;
                            }
                        }
                    }
                }else{
                    selected = true;
                }

                listMenu.add(new FilterMenu(namaFragment, selected));
            }
        }

        if(filterTransaction != null) {
            segmentedInputData.setPosition(filterTransaction.getShowInputBy());
        }

        FilterAfdelingAdapter adapterAfdeling = new FilterAfdelingAdapter(context,listAfdeling);
        rvAfd.setLayoutManager(new GridLayoutManager(context,2));
        ScaleInAnimationAdapter scaleInAnimationAdapterAfdeling = new ScaleInAnimationAdapter(adapterAfdeling);
        rvAfd.setAdapter(scaleInAnimationAdapterAfdeling);

        FilterMenuAdapter adapterMenu = new FilterMenuAdapter(context,listMenu);
        rvMenu.setLayoutManager(new GridLayoutManager(context,2));
        ScaleInAnimationAdapter scaleInAnimationAdapterMenu = new ScaleInAnimationAdapter(adapterMenu);
        rvMenu.setAdapter(scaleInAnimationAdapterMenu);

        lsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterTransaction filterSimpan = new FilterTransaction(
                        segmentedInputData.getPosition(),
                        listMenu,
                        listAfdeling
                );
                Gson gson  = new Gson();
                SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.FILTER_TRANSAKSI, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(HarvestApp.FILTER_TRANSAKSI,gson.toJson(filterSimpan));
                editor.apply();

                if(context instanceof MainMenuActivity){
                    mainMenuActivity = (MainMenuActivity) context;
                    Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
                    setFilterView(fragment);
                    if(fragment instanceof TphFragment) {
                        ((TphFragment) fragment).startLongOperation(TphFragment.STATUS_LONG_OPERATION_NONE);
                    }else if (fragment instanceof AngkutFragment){
                        ((AngkutFragment) fragment).startLongOperation(TphFragment.STATUS_LONG_OPERATION_NONE);
                    }else if (fragment instanceof SpbFragment){
                        ((SpbFragment) fragment).startLongOperation(TphFragment.STATUS_LONG_OPERATION_NONE);
                    }
                }
                alertDialog.dismiss();
            }
        });

        lClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog = WidgetHelper.showListReference(alertDialog,baseView,context);
    }

    class FilterAfdelingAdapter extends RecyclerView.Adapter<FilterAfdelingAdapter.Holder>{
        Context context;
        ArrayList<FilterAfdeling> originLists;
        public FilterAfdelingAdapter(Context context, ArrayList<FilterAfdeling> filterAfdelings){
            this.context = context;
            originLists = filterAfdelings;
        }

        @NonNull
        @Override
        public FilterAfdelingAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.item_checkbox_filter,parent,false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull FilterAfdelingAdapter.Holder holder, int position) {
            holder.setValue(originLists.get(position));
        }

        @Override
        public int getItemCount() {
            return originLists!=null ? originLists.size() : 0;
        }

        public class Holder extends RecyclerView.ViewHolder {
            CheckBox cb;
            public Holder(View itemView) {
                super(itemView);
                cb = (CheckBox) itemView.findViewById(R.id.checkBox);
            }

            public void setValue(FilterAfdeling filterAfdeling) {
                cb.setText(filterAfdeling.getAfdeling());
                cb.setChecked(filterAfdeling.getSelected());

                cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        for(int i = 0 ; i < listAfdeling.size();i++){
                            if(cb.getText().toString().equals(listAfdeling.get(i).getAfdeling())){
                                listAfdeling.get(i).setSelected(isChecked);
                            }
                        }
                    }
                });
            }
        }
    }

    class FilterMenuAdapter extends RecyclerView.Adapter<FilterMenuAdapter.Holder>{
        Context context;
        ArrayList<FilterMenu> originLists;
        public FilterMenuAdapter(Context context, ArrayList<FilterMenu> filterMenu){
            this.context = context;
            originLists = filterMenu;
        }

        @NonNull
        @Override
        public FilterMenuAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(context).inflate(R.layout.item_checkbox_filter,parent,false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull FilterMenuAdapter.Holder holder, int position) {
            holder.setValue(originLists.get(position));
        }

        @Override
        public int getItemCount() {
            return originLists!=null ? originLists.size() : 0;
        }

        public class Holder extends RecyclerView.ViewHolder {
            CheckBox cb;
            public Holder(View itemView) {
                super(itemView);
                cb = (CheckBox) itemView.findViewById(R.id.checkBox);
            }

            public void setValue(FilterMenu filterMenu) {
                cb.setText(filterMenu.getMenuName());
                cb.setChecked(filterMenu.getSelected());

                cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        for(int i = 0 ; i < listMenu.size();i++){
                            if(cb.getText().toString().equals(listMenu.get(i).getMenuName())){
                                listMenu.get(i).setSelected(isChecked);
                            }
                        }
                    }
                });
            }
        }
    }
}
