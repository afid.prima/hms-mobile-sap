package id.co.ams_plantation.harvestsap.ui;

import static id.co.ams_plantation.harvestsap.Fragment.SettingFragment.ButtonGroup_Lokal;
import static id.co.ams_plantation.harvestsap.Fragment.SettingFragment.ButtonGroup_Public;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.CekCagesAdapter;
import id.co.ams_plantation.harvestsap.adapter.RefreshHeaderRecycleView;
import id.co.ams_plantation.harvestsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestsap.model.Cages;
import id.co.ams_plantation.harvestsap.presenter.ConnectionPresenter;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.SetUpDataSyncHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import id.co.ams_plantation.harvestsap.view.ApiView;

public class CagesActivity extends BaseActivity implements ApiView {

    CoordinatorLayout myCoordinatorLayout;
    SegmentedButtonGroup segmentedButtonGroup;
    LinearLayout lnoData;
    Button btnSearch;
    LinearLayout llSearch;
    CompleteTextViewHelper etSearch;
    RecyclerView rv;
    RefreshLayout refreshRv;

    HashMap<String, Cages> CagesInformationHashMap = new HashMap<>();
    CekCagesAdapter cekCagesAdapter;
    Set<String> listSearch;

    final int LongOperation_SetUpData= 0;
    final int LongOperation_SetUpDataDownload = 1;
    final int LongOperation_UpdateData = 2;

    ConnectionPresenter connectionPresenter;
    SetUpDataSyncHelper setUpDataSyncHelper;
    boolean awal;
    ServiceResponse responseApi;

    @Override
    protected void initPresenter() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cek_cages_list_layout);
        getSupportActionBar().hide();
        toolBarSetup();

        myCoordinatorLayout = findViewById(R.id.myCoordinatorLayout);
        segmentedButtonGroup = findViewById(R.id.segmentedButtonGroup);
        llSearch = findViewById(R.id.llSearch);
        lnoData = findViewById(R.id.lnoData);
        etSearch = findViewById(R.id.etSearch);
        btnSearch = findViewById(R.id.btnSearch);
        rv = findViewById(R.id.rv);
        refreshRv = findViewById(R.id.refreshRv);

        setUpDataSyncHelper = new SetUpDataSyncHelper(this);
        connectionPresenter = new ConnectionPresenter(this);
        awal = true;

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
        if(preferences.getBoolean(HarvestApp.CONNECTION_PREF,false)){
            segmentedButtonGroup.setPosition(ButtonGroup_Public);
        }else{
            segmentedButtonGroup.setPosition(ButtonGroup_Lokal);
        }

        rv.setLayoutManager(new GridLayoutManager(this,2));

        refreshRv.setEnableAutoLoadMore(true);
        refreshRv.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                refresh();
            }
        });
        refreshRv.setRefreshHeader(new RefreshHeaderRecycleView(this));
        refreshRv.setHeaderHeight(60);
        refreshRv.autoRefresh();

        segmentedButtonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(int position) {
                switch (position){
                    case 0: {
                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF, false);
                        editor.apply();
                        break;
                    }
                    case 1: {
                        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putBoolean(HarvestApp.CONNECTION_PREF, true);
                        editor.apply();
                        break;
                    }
                }
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String search = etSearch.getText().toString();
                cekCagesAdapter.getFilter().filter(search);
            }
        });
    }

    private void refresh() {
        lnoData.setVisibility(View.GONE);
        refreshRv.getLayout().postDelayed(() -> {
            if(awal) {
                new LongOperation().execute(String.valueOf(LongOperation_SetUpData));
            }else{
                new LongOperation().execute(String.valueOf(LongOperation_SetUpDataDownload));
            }
        }, 2000);
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        TextView textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backProses();
            }
        });
        textToolbar.setText(getResources().getString(R.string.cek_cages));
    }

    private void setupAdapter(){
        if(CagesInformationHashMap == null){
            llSearch.setVisibility(View.GONE);
            lnoData.setVisibility(View.VISIBLE);
            return;
        }else if (CagesInformationHashMap.values() == null){
            llSearch.setVisibility(View.GONE);
            lnoData.setVisibility(View.VISIBLE);
            return;
        }

        rv.setVisibility(View.VISIBLE);
        etSearch.setText("");
        Collection<Cages> values = CagesInformationHashMap.values();
        if(values.size() > 0) {
            cekCagesAdapter = new CekCagesAdapter(this, new ArrayList<>(values));
            rv.setAdapter(cekCagesAdapter);
            cekCagesAdapter.notifyDataSetChanged();
        }else{
            rv.setVisibility(View.GONE);
        }

        awal = false;

        if(listSearch != null) {
            if(listSearch.size() > 0) {
                List<String> lSearch = new ArrayList<>();
                lSearch.addAll(listSearch);
                lSearch = GlobalHelper.removeNullUsingIterator(lSearch);
                ArrayAdapter<String> adapterSearch = new ArrayAdapter<String>(this,
                        android.R.layout.simple_dropdown_item_1line, lSearch);
                etSearch.setAdapter(adapterSearch);
                etSearch.setThreshold(1);
                adapterSearch.notifyDataSetChanged();
            }
        }

        if(CagesInformationHashMap.size() > 0){
            llSearch.setVisibility(View.VISIBLE);
            lnoData.setVisibility(View.GONE);
        }else{
            llSearch.setVisibility(View.GONE);
            lnoData.setVisibility(View.VISIBLE);
        }
    }

    public void backProses() {
        setResult(GlobalHelper.RESULT_CAGESACTIVITY);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        backProses();
    }

    @Override
    public void successResponse(ServiceResponse serviceResponse) {
        try {
            switch (serviceResponse.getTag()) {
                case GetMasterCagesByEstate:
                    responseApi = serviceResponse;
                    new LongOperation().execute(String.valueOf(LongOperation_UpdateData));
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void badResponse(ServiceResponse serviceResponse) {
        switch (serviceResponse.getTag()) {
            case GetMasterCagesByEstate:
                WidgetHelper.showSnackBar(myCoordinatorLayout, "GET Gagal GetMasterCagesByEstate ");
                new LongOperation().execute(String.valueOf(LongOperation_SetUpData));
                break;
        }
    }

    public class LongOperation extends AsyncTask<String, String, String> {
        boolean skip = false;

        @Override
        protected String doInBackground(String... strings) {
            try {
                switch (Integer.parseInt(strings[0])) {
                    case LongOperation_SetUpData:
                        CagesInformationHashMap = GlobalHelper.getDataAllCages();
                        listSearch = new android.support.v4.util.ArraySet<>();

                        if(CagesInformationHashMap != null){
                            if(CagesInformationHashMap.size() > 0){
                                for(Cages Cages : CagesInformationHashMap.values()){
                                    try {
                                        listSearch.add(Cages.getAssetNo());
                                        listSearch.add(Cages.getAssetName());
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                        break;
                    case LongOperation_SetUpDataDownload:
                        connectionPresenter.GetMasterCagesByEstate2();
                        break;
                    case LongOperation_UpdateData:
                        skip= !setUpDataSyncHelper.downloadMasterCages(responseApi,this);
                        break;
                }
                return strings[0];
            } catch (Exception e) {
                return "Exception";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result.equals("Exception")) {
                WidgetHelper.showSnackBar(myCoordinatorLayout, "Gagal Prepare Data Long Operation");
                refreshRv.finishRefresh();
            }else{
                switch (Integer.parseInt(result)) {
                    case LongOperation_SetUpData:
                        setupAdapter();
                        refreshRv.finishRefresh();
                        break;
                    case LongOperation_UpdateData:
                        if(skip){
                            WidgetHelper.showSnackBar(myCoordinatorLayout, "Gagal Insert Cages Information");
                            refreshRv.finishRefresh();
                        } else {
                            new LongOperation().execute(String.valueOf(LongOperation_SetUpData));
                        }
                        break;
                    case LongOperation_SetUpDataDownload:
                        rv.setVisibility(View.GONE);
                        llSearch.setVisibility(View.GONE);
                        break;
                }
            }
        }
    }
}
