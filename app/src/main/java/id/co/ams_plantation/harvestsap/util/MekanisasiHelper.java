package id.co.ams_plantation.harvestsap.util;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.ArraySet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import id.co.ams_plantation.harvestsap.Fragment.MekanisasiPanenInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.SpbFragment;
import id.co.ams_plantation.harvestsap.Fragment.TphFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.CekSPKBlockAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectGangAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectPemanenAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectTphMekanisasiAdapter;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.BJRInformation;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.DynamicParameterPenghasilan;
import id.co.ams_plantation.harvestsap.model.Gerdang;
import id.co.ams_plantation.harvestsap.model.HasilPanen;
import id.co.ams_plantation.harvestsap.model.Kongsi;
import id.co.ams_plantation.harvestsap.model.MekanisasiOperator;
import id.co.ams_plantation.harvestsap.model.Pemanen;
import id.co.ams_plantation.harvestsap.model.SubPemanen;
import id.co.ams_plantation.harvestsap.model.TPH;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.model.Vehicle;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;
import id.co.ams_plantation.harvestsap.ui.MekanisasiActivity;

public class MekanisasiHelper {
    private static String TAG = "MekanisasiHelper";
    Context context;
    AlertDialog alertDialog;

    MekanisasiPanenInputFragment mekanisasiPanenInputFragment;
    SharedPrefHelper sharedPrefHelper;
    String gangSelected;
    SelectGangAdapter adapterGang;
    SelectPemanenAdapter adapterPemanen;
    SelectTphMekanisasiAdapter adapterTPH;
    TphHelper tphHelper;
    PemanenHelper pemanenHelper;
    TransaksiPanen transaksiPanen;
    int totalJjg;
    int totalJjgPendapatan;

    public MekanisasiHelper(Context context) {
        this.context = context;
        sharedPrefHelper = new SharedPrefHelper(context);
    }

    public void popupNewSPB(){
        View baseView = LayoutInflater.from(context).inflate(R.layout.popup_newspb_layout,null);
        LinearLayout lTapSPB = (LinearLayout) baseView.findViewById(R.id.lTapSPB);
        LinearLayout lTapMekanisasi = (LinearLayout) baseView.findViewById(R.id.lTapMekanisasi);

        lTapSPB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context instanceof MainMenuActivity){
                    Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
                    if (fragment instanceof SpbFragment){
                        ((SpbFragment) fragment).chosenSPB(1);
                    }
                }
                alertDialog.dismiss();
            }
        });

        lTapMekanisasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context instanceof MainMenuActivity){
                    Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
                    if (fragment instanceof SpbFragment){
                        ((SpbFragment) fragment).chosenSPB(0);
                    }
                }
                alertDialog.dismiss();
            }
        });

        alertDialog = WidgetHelper.showListReference(alertDialog,baseView,context);
    }

    public void popupNewPanen(){
        View baseView = LayoutInflater.from(context).inflate(R.layout.popup_newpanen_layout,null);
        LinearLayout lPanenManual = (LinearLayout) baseView.findViewById(R.id.lPanenManual);
        LinearLayout lPanenMekanisasi = (LinearLayout) baseView.findViewById(R.id.lPanenMekanisasi);
        LinearLayout lTapMekanisasi = (LinearLayout) baseView.findViewById(R.id.lTapMekanisasi);

        lPanenManual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context instanceof MainMenuActivity){
                    Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
                    if (fragment instanceof TphFragment){
                        ((TphFragment) fragment).chosenPanen(0);
                    }
                }
                alertDialog.dismiss();
            }
        });

        lPanenMekanisasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context instanceof MainMenuActivity){
                    Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
                    if (fragment instanceof TphFragment){
                        ((TphFragment) fragment).chosenPanen(2);
                    }
                }
                alertDialog.dismiss();
            }
        });

        lTapMekanisasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context instanceof MainMenuActivity){
                    Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
                    if (fragment instanceof TphFragment){
                        ((TphFragment) fragment).chosenSPB();
                    }
                }
                alertDialog.dismiss();
            }
        });

        alertDialog = WidgetHelper.showListReference(alertDialog,baseView,context);
    }

    public void setupFormMekansiasi(ArrayAdapter<String> adapterBlock,TphHelper tphHelper,PemanenHelper pemanenHelper){
        if (context instanceof MekanisasiActivity) {
            if (((MekanisasiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof MekanisasiPanenInputFragment) {
                mekanisasiPanenInputFragment = (MekanisasiPanenInputFragment) ((MekanisasiActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
            }
        }

        this.tphHelper = tphHelper;
        this.pemanenHelper = pemanenHelper;

        if(adapterBlock.getCount() > 0){
            mekanisasiPanenInputFragment.etBlock.setThreshold(1);
            mekanisasiPanenInputFragment.etBlock.setAdapter(adapterBlock);
        }

        mekanisasiPanenInputFragment.etBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mekanisasiPanenInputFragment.etBlock.showDropDown();
            }
        });

        mekanisasiPanenInputFragment.etBlock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mekanisasiPanenInputFragment.selectedBlock = mekanisasiPanenInputFragment.etBlock.getText().toString();
                setDropDownTPH(mekanisasiPanenInputFragment.etBlock.getText().toString());
            }
        });

        mekanisasiPanenInputFragment.etAncak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mekanisasiPanenInputFragment.etAncak.showDropDown();
            }
        });

        mekanisasiPanenInputFragment.etAncak.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                adapterTPH = (SelectTphMekanisasiAdapter) adapterView.getAdapter();
                TPH tph = adapterTPH.getItemAt(position);
                setTPH(tph);
            }
        });

        mekanisasiPanenInputFragment.etGang.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                adapterGang = (SelectGangAdapter) adapterView.getAdapter();
                gangSelected =  adapterGang.getItemAt(i).getGank();

                if(!gangSelected.contains("PN")){
                    AlertDialog alertDialog = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
                            .setTitle("Perhatian")
                            .setMessage("Apakah Anda Yakin Untuk Simpan Bukan Gang Panen")
                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    mekanisasiPanenInputFragment.etGang.setText("");
                                    mekanisasiPanenInputFragment.etPemanen.setText("");
                                }
                            })
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();

                                    setGang(gangSelected);
                                }
                            })
                            .create();
                    alertDialog.show();
                }else{
                    setGang(gangSelected);
                }
            }
        });

        mekanisasiPanenInputFragment.etGang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mekanisasiPanenInputFragment.etGang.showDropDown();
            }
        });

        mekanisasiPanenInputFragment.etGang.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if(gangSelected == null){
                        mekanisasiPanenInputFragment.etGang.setText("");
                    }else{
                        mekanisasiPanenInputFragment.etGang.setText(gangSelected);
                    }
                }
            }
        });

        mekanisasiPanenInputFragment.etPemanen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (context instanceof MekanisasiActivity) {
                    mekanisasiPanenInputFragment.etPemanen.showDropDown();
                }
            }
        });

        mekanisasiPanenInputFragment.etPemanen.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                adapterPemanen = (SelectPemanenAdapter) adapterView.getAdapter();
                Pemanen pemanen = adapterPemanen.getItemAt(position);
                if(pemanen.getGank() != null){
                    if(!pemanen.getGank().contains("PN")){
                        AlertDialog alertDialog = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
                                .setTitle("Perhatian")
                                .setMessage("Apakah Anda Yakin Untuk Simpan Pemanen Bukan Gang Panen")
                                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        mekanisasiPanenInputFragment.etPemanen.setText("");
                                    }
                                })
                                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        setPemanen(adapterPemanen.getItemAt(position));
                                    }
                                })
                                .create();
                        alertDialog.show();
                    }else{
                        setPemanen(adapterPemanen.getItemAt(position));
                    }
                }else {
                    setPemanen(adapterPemanen.getItemAt(position));
                }
            }
        });

        mekanisasiPanenInputFragment.etTotalJanjangSubPemanen.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setJjgNormal(true);
//                setTotalJjgPendapatan(true);
            }
        });

        mekanisasiPanenInputFragment.etBuahMentahSubPemanen.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setJjgNormal(true);
//                setTotalJjgPendapatan(true);
            }
        });

//        mekanisasiPanenInputFragment.etBusukSubPemanen.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                setJjgNormal(true);
////                setTotalJjgPendapatan(true);
//            }
//        });
//
//        mekanisasiPanenInputFragment.etLewatMatangSubPemanen.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                setJjgNormal(true);
////                setTotalJjgPendapatan(true);
//            }
//        });
//
//        mekanisasiPanenInputFragment.etAbnormalSubPemanen.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                setJjgNormal(true);
////                setTotalJjgPendapatan(true);
//            }
//        });
//
//        mekanisasiPanenInputFragment.etTangkaiPanjangSubPemanen.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                setJjgNormal(true);
////                setTotalJjgPendapatan(true);
//            }
//        });
//
//        mekanisasiPanenInputFragment.etBuahDimakanTikusSubPemanen.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                setJjgNormal(true);
////                setTotalJjgPendapatan(true);
//            }
//        });

        mekanisasiPanenInputFragment.lsaveFormMekanisasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HasilPanen hasilPanen = new HasilPanen(
                        Integer.parseInt(mekanisasiPanenInputFragment.etNormalSubPemanen.getText().toString().isEmpty() ? "0" : mekanisasiPanenInputFragment.etNormalSubPemanen.getText().toString())
                        ,Integer.parseInt(mekanisasiPanenInputFragment.etBuahMentahSubPemanen.getText().toString().isEmpty() ? "0" : mekanisasiPanenInputFragment.etBuahMentahSubPemanen.getText().toString())
                        ,0
                        ,0
                        ,0
                        ,0
                        ,0
                        ,Integer.parseInt(mekanisasiPanenInputFragment.etTotalBrondolanSubPemanen.getText().toString().isEmpty() ? "0" : mekanisasiPanenInputFragment.etTotalBrondolanSubPemanen.getText().toString())
                        ,0
                        ,Integer.parseInt(mekanisasiPanenInputFragment.etTotalJanjangSubPemanen.getText().toString().isEmpty() ? "0" : mekanisasiPanenInputFragment.etTotalJanjangSubPemanen.getText().toString())
                        ,Integer.parseInt(mekanisasiPanenInputFragment.etTotalJanjangPendapatanSubPemanen.getText().toString().isEmpty() ? "0" : mekanisasiPanenInputFragment.etTotalJanjangPendapatanSubPemanen.getText().toString())
                        ,System.currentTimeMillis()
                        ,false
                );
                hasilPanen.setTotalJanjangBuruk(hasilPanen.getBuahMentah());
                setHasilPanen(hasilPanen);

                if(transaksiPanen.getHasilPanen() == null){
                    Toast.makeText(HarvestApp.getContext(),"Mohon input janjang dahulu",Toast.LENGTH_SHORT).show();
                    return;
                }else if (transaksiPanen.getHasilPanen().getTotalJanjang() < 1){
                    Toast.makeText(HarvestApp.getContext(),"Total janjang harus lebih sama dengan 1",Toast.LENGTH_SHORT).show();
                    return;
                }else if (transaksiPanen.getHasilPanen().getJanjangNormal() < 0){
                    Toast.makeText(HarvestApp.getContext(),"Total Normal Tidak Boleh Dibawah 0",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(transaksiPanen.getPemanen() == null){
                    Toast.makeText(HarvestApp.getContext(),"Mohon input dahulu pemanen",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(transaksiPanen.getTph() == null){
                    Toast.makeText(HarvestApp.getContext(),"Mohon input dahulu TPH",Toast.LENGTH_SHORT).show();
                    return;
                }

                ArrayList<SubPemanen> subPemanens = new ArrayList<>();
                subPemanens.add(new SubPemanen(0,
                        GlobalHelper.getCharForNumber(0),
                        100,
                        transaksiPanen.getHasilPanen(),
                        SubPemanen.idPemnaen,
                        transaksiPanen.getPemanen())
                );

                int idKongsi = Kongsi.idNone;
                Kongsi kongsi = new Kongsi(idKongsi,subPemanens);
                transaksiPanen.setKongsi(kongsi);

                mekanisasiPanenInputFragment.updateUIRV();
                mekanisasiPanenInputFragment.mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            }
        });

        mekanisasiPanenInputFragment.lldeletedFormMekanisasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
                        .setTitle("Perhatian")
                        .setMessage("Apakah Anda Yakin Untuk Hapus Data Panen Ini")
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                if(transaksiPanen.getIdTPanen() != null){
                                    mekanisasiPanenInputFragment.mekanisasiPanens.remove(transaksiPanen.getIdTPanen());
                                }
                                mekanisasiPanenInputFragment.updateUIRV();
                                mekanisasiPanenInputFragment.mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                            }
                        })
                        .create();
                alertDialog.show();
            }
        });
    }

    private void setJjgNormal(boolean withEditText) {
        int jjgT = GlobalHelper.getTotalJanjang(
                new HasilPanen(
                        0,
                        Integer.parseInt(mekanisasiPanenInputFragment.etBuahMentahSubPemanen.getText().toString().isEmpty() ? "0" : mekanisasiPanenInputFragment.etBuahMentahSubPemanen.getText().toString()),
                        0,
                        0,
                        0,
                        0,
                        0,
                        Integer.parseInt(mekanisasiPanenInputFragment.etTotalJanjangSubPemanen.getText().toString().isEmpty() ? "0" : mekanisasiPanenInputFragment.etTotalJanjangSubPemanen.getText().toString()),
                        Integer.parseInt(mekanisasiPanenInputFragment.etTotalJanjangPendapatanSubPemanen.getText().toString().isEmpty() ? "0" : mekanisasiPanenInputFragment.etTotalJanjangPendapatanSubPemanen.getText().toString()),
                        Integer.parseInt(mekanisasiPanenInputFragment.etTotalBrondolanSubPemanen.getText().toString().isEmpty() ? "0" : mekanisasiPanenInputFragment.etTotalBrondolanSubPemanen.getText().toString()),
                        false
                )
        );
        totalJjg = Integer.parseInt(mekanisasiPanenInputFragment.etTotalJanjangSubPemanen.getText().toString().isEmpty() ? "0" : mekanisasiPanenInputFragment.etTotalJanjangSubPemanen.getText().toString());

        if (withEditText) {
            mekanisasiPanenInputFragment.etNormalSubPemanen.setText(String.valueOf(totalJjg - jjgT));
            mekanisasiPanenInputFragment.etTotalJanjangPendapatanSubPemanen.setText(String.valueOf(totalJjg - jjgT));
        }
    }

    public void showFormMekanisasi(TransaksiPanen value,boolean newForm){

        mekanisasiPanenInputFragment.mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd-MMMM-yyyy");
        gangSelected = sharedPrefHelper.getName(SharedPrefHelper.KEY_GENGSELECTED);
        this.transaksiPanen = value;

        if(newForm){
            mekanisasiPanenInputFragment.sheetHeader.setText("Panen Baru");
        }else{
            mekanisasiPanenInputFragment.sheetHeader.setText("Update Panen");
        }

        mekanisasiPanenInputFragment.tvDate.setText(sdf.format(value.getCreateDate()));

        if (value.getPemanen() != null) {

            if(value.getPemanen().getGank() != null) {
                Log.d(this.getClass() + " setValue 1",value.getPemanen().getGank());
                mekanisasiPanenInputFragment.etGang.setText(value.getPemanen().getGank());
            }else {
                if (gangSelected != null) {
                    mekanisasiPanenInputFragment.etGang.setText(gangSelected);
                }else {
                    mekanisasiPanenInputFragment.etGang.setText("");
                }
            }

            if(value.getPemanen().getNama() != null){
                Log.d(this.getClass() + " setValue 1",value.getPemanen().getNama());
                mekanisasiPanenInputFragment.etPemanen.setText(value.getPemanen().getNama());
                if(value.getPemanen().getGank() != null && value.getPemanen().getNik() != null && value.getPemanen().getNama() != null){
                    mekanisasiPanenInputFragment.tvPemanen.setText(value.getPemanen().getGank() + " - " + value.getPemanen().getNik() +" - "+ value.getPemanen().getNama());
                }
            }else{
                mekanisasiPanenInputFragment.etPemanen.setText("");
                mekanisasiPanenInputFragment.tvPemanen.setText("Gang - NIK - Nama Pemanen");
            }
        }else{
            mekanisasiPanenInputFragment.etPemanen.setText("");
            mekanisasiPanenInputFragment.tvPemanen.setText("Gang - NIK - Nama Pemanen");
        }

        if(value.getTph() != null){
            if(value.getTph().getBlock() != null && value.getTph().getAncak() != null && value.getTph().getNamaTph() != null) {
                mekanisasiPanenInputFragment.etBlock.setText(value.getTph().getBlock());
                mekanisasiPanenInputFragment.etAncak.setText(value.getTph().getAncak());
                mekanisasiPanenInputFragment.tvTPH.setText(value.getTph().getBlock() + " - " + value.getTph().getAncak() + " - " + value.getTph().getNamaTph());
            }else{
                mekanisasiPanenInputFragment.etBlock.setText("");
                mekanisasiPanenInputFragment.etAncak.setText("");
                mekanisasiPanenInputFragment.tvTPH.setText("Block - Ancak - TPH");
            }
        }else{
            mekanisasiPanenInputFragment.etBlock.setText("");
            mekanisasiPanenInputFragment.etAncak.setText("");
            mekanisasiPanenInputFragment.tvTPH.setText("Block - Ancak - TPH");
        }

        HashMap<String,Pemanen> sGang = new HashMap<>();
        if(GlobalHelper.dataPemanen.size() > 0) {
            for (HashMap.Entry<String, Pemanen> entry : GlobalHelper.dataPemanen.entrySet()) {
                sGang.put(entry.getValue().getGank(), entry.getValue());
            }
            adapterGang = new SelectGangAdapter(context, R.layout.row_setting, new ArrayList<Pemanen>(sGang.values()));
            mekanisasiPanenInputFragment.etGang.setThreshold(1);
            mekanisasiPanenInputFragment.etGang.setAdapter(adapterGang);
            adapterGang.notifyDataSetChanged();
        }

        if(value.getBlock()!= null){
            mekanisasiPanenInputFragment.etBlock.setText(value.getBlock());
            setDropDownTPH(mekanisasiPanenInputFragment.etBlock.getText().toString());
        }

        if(gangSelected != null){
            Pemanen adaPemanen = sGang.get(gangSelected);
            if (adaPemanen != null) {
                if(mekanisasiPanenInputFragment.etGang.getText().toString().isEmpty()) {
                    mekanisasiPanenInputFragment.etGang.setText(gangSelected);
                }
                adapterPemanen = new SelectPemanenAdapter(context, R.layout.row_record,
                        pemanenHelper.getListPemanenByGangWithCompare(GlobalHelper.dataPemanen,
                                null,
                                mekanisasiPanenInputFragment.etGang.getText().toString())
                );
                mekanisasiPanenInputFragment.etPemanen.setThreshold(1);
                mekanisasiPanenInputFragment.etPemanen.setAdapter(adapterPemanen);
                adapterPemanen.notifyDataSetChanged();
            }
        }

        if(value.getHasilPanen() != null){
            mekanisasiPanenInputFragment.etTotalJanjangSubPemanen.setText(String.valueOf(value.getHasilPanen().getTotalJanjang()));
            mekanisasiPanenInputFragment.etBuahMentahSubPemanen.setText(String.valueOf(value.getHasilPanen().getBuahMentah()));
//            mekanisasiPanenInputFragment.etBusukSubPemanen.setText(String.valueOf(value.getHasilPanen().getBusukNJangkos()));
//            mekanisasiPanenInputFragment.etLewatMatangSubPemanen.setText(String.valueOf(value.getHasilPanen().getBuahLewatMatang()));
//            mekanisasiPanenInputFragment.etAbnormalSubPemanen.setText(String.valueOf(value.getHasilPanen().getBuahAbnormal()));
//            mekanisasiPanenInputFragment.etTangkaiPanjangSubPemanen.setText(String.valueOf(value.getHasilPanen().getTangkaiPanjang()));
//            mekanisasiPanenInputFragment.etBuahDimakanTikusSubPemanen.setText(String.valueOf(value.getHasilPanen().getBuahDimakanTikus()));
            mekanisasiPanenInputFragment.etTotalBrondolanSubPemanen.setText(String.valueOf(value.getHasilPanen().getBrondolan()));
        }else{
            mekanisasiPanenInputFragment.etTotalJanjangSubPemanen.setText("");
            mekanisasiPanenInputFragment.etBuahMentahSubPemanen.setText("");
//            mekanisasiPanenInputFragment.etBusukSubPemanen.setText("");
//            mekanisasiPanenInputFragment.etLewatMatangSubPemanen.setText("");
//            mekanisasiPanenInputFragment.etAbnormalSubPemanen.setText("");
//            mekanisasiPanenInputFragment.etTangkaiPanjangSubPemanen.setText("");
//            mekanisasiPanenInputFragment.etBuahDimakanTikusSubPemanen.setText("");
            mekanisasiPanenInputFragment.etTotalBrondolanSubPemanen.setText("");
        }
    }


    private void setDropDownTPH(String block){
        adapterTPH = new SelectTphMekanisasiAdapter(context, R.layout.row_record,
                tphHelper.setDataTph(GlobalHelper.getEstate().getEstCode(), block)
        );
        mekanisasiPanenInputFragment.etAncak.setThreshold(1);
        mekanisasiPanenInputFragment.etAncak.setAdapter(adapterTPH);
        mekanisasiPanenInputFragment.etAncak.showDropDown();
    }

    private void setTPH(TPH tph){
        mekanisasiPanenInputFragment.etAncak.setText(tph.getAncak());
        mekanisasiPanenInputFragment.tvTPH.setText(tph.getBlock() + " - " + tph.getAncak() + " - " + tph.getNamaTph());

        transaksiPanen.setTph(tph);
        transaksiPanen.setBlock(tph.getBlock());
        transaksiPanen.setAncak(tph.getAncak());

        if(tph.getBlock() != null){
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(System.currentTimeMillis()));
            String idBJRInformasi = String.valueOf(cal.get(Calendar.YEAR)) + "-" + String.valueOf(cal.get(Calendar.MONTH) + 1) + "-" + transaksiPanen.getTph().getBlock() + "-" + transaksiPanen.getTph().getAfdeling();
            BJRInformation bjrInformation = mekanisasiPanenInputFragment.mekanisasiActivity.bjrInformationHashMap.get(idBJRInformasi);
            if(bjrInformation != null) {
                transaksiPanen.setBjrMekanisasi(bjrInformation.getBjrAdjustment());
            }else{
                transaksiPanen.setBjrMekanisasi(0);
            }
        }

        mekanisasiPanenInputFragment.mekanisasiPanens.put(transaksiPanen.getIdTPanen(),transaksiPanen);

        if(mekanisasiPanenInputFragment.etGang.getText().toString().isEmpty()){
            mekanisasiPanenInputFragment.etGang.requestFocus();
        }else if (mekanisasiPanenInputFragment.etPemanen.getText().toString().isEmpty()){
            mekanisasiPanenInputFragment.etPemanen.showDropDown();
        }

    }

    private void setGang(String Gang){
        mekanisasiPanenInputFragment.etGang.setText(Gang);
        sharedPrefHelper.commit(SharedPrefHelper.KEY_GENGSELECTED,Gang);
//                        File fileGangSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_PANEN] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_PANEN_GANG] );
//                        GlobalHelper.writeFileContent(fileGangSelected.getAbsolutePath(),etGang.getText().toString());

        adapterPemanen = new SelectPemanenAdapter(context, R.layout.row_record,
                pemanenHelper.getListPemanenByGangWithCompare(GlobalHelper.dataPemanen,
                        null,
                        mekanisasiPanenInputFragment.etGang.getText().toString())
        );
        mekanisasiPanenInputFragment.etPemanen.setThreshold(1);
        mekanisasiPanenInputFragment.etPemanen.setAdapter(adapterPemanen);
        adapterPemanen.notifyDataSetChanged();
        mekanisasiPanenInputFragment.etPemanen.requestFocus();
        mekanisasiPanenInputFragment.etPemanen.callOnClick();
    }

    private void setPemanen(Pemanen pemanen){
        mekanisasiPanenInputFragment.etPemanen.setText(pemanen.getNama());
        mekanisasiPanenInputFragment.tvPemanen.setText(pemanen.getGank() + " - " + pemanen.getNik() +" - "+ pemanen.getNama());

        transaksiPanen.setPemanen(pemanen);
        if(mekanisasiPanenInputFragment.mekanisasiPanens.size() > 0){
            for(TransaksiPanen panen : new ArrayList<>(mekanisasiPanenInputFragment.mekanisasiPanens.values())){
                if(transaksiPanen.getIdTPanen().toLowerCase().equals(panen.getIdTPanen())){
                    mekanisasiPanenInputFragment.mekanisasiPanens.put(panen.getIdTPanen(),transaksiPanen);
                    break;
                }
            }
        }

        mekanisasiPanenInputFragment.etTotalJanjangSubPemanen.requestFocus();
    }

    private void setHasilPanen(HasilPanen hasilPanen){
        transaksiPanen.setHasilPanen(hasilPanen);
        if(mekanisasiPanenInputFragment.mekanisasiPanens.size() > 0){
            for(TransaksiPanen panen : new ArrayList<>(mekanisasiPanenInputFragment.mekanisasiPanens.values())){
                if(transaksiPanen.getIdTPanen().toLowerCase().equals(panen.getIdTPanen())){
                    mekanisasiPanenInputFragment.mekanisasiPanens.put(panen.getIdTPanen(),transaksiPanen);
                    break;
                }
            }
        }
    }

    public Set<String> setBlockMekanisais(){
        Set<String> blocks = new android.support.v4.util.ArraySet<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_BLOCK_MEKANISASI);
        if(db != null) {
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                blocks.add(dataNitrit.getIdDataNitrit());
            }
            db.close();
        }
        return blocks;
    }

    public void showBlockMekanisasi(){
        Set<String> block =  setBlockMekanisais();
        View baseView = LayoutInflater.from(context).inflate(R.layout.popup_blockmekanisasi_layout,null);
        LinearLayout lSync = (LinearLayout) baseView.findViewById(R.id.lSync);
        LinearLayout lTutup = (LinearLayout) baseView.findViewById(R.id.lTutup);
        RecyclerView rvBlock = (RecyclerView) baseView.findViewById(R.id.rvBlock);

        rvBlock.setLayoutManager(new GridLayoutManager(context, 4));
        CekSPKBlockAdapter adapter = new CekSPKBlockAdapter(new ArrayList<>(block));
        rvBlock.setAdapter(adapter);

        lSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        lTutup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog = WidgetHelper.showListReference(alertDialog,baseView,context);
    }

    public HashMap<String, MekanisasiOperator> getCurrentMekanisasiOperator(Long currentDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String tgl = sdf.format(currentDate);
        HashMap<String,MekanisasiOperator> currentMekanisasiOperator = new HashMap<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_OPERATOR_MEKANISASI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("param1", tgl));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                MekanisasiOperator mekanisasiOperator  = gson.fromJson(dataNitrit.getValueDataNitrit(),MekanisasiOperator.class);
                currentMekanisasiOperator.put(mekanisasiOperator.getPemanen().getNik(),mekanisasiOperator);
                Log.d("MekanisaiHelper", "getCurrentMekanisasiOperator: "+gson.toJson(mekanisasiOperator));
            }
        }
        db.close();
        return  currentMekanisasiOperator;
    }
    public void saveMekanisasiOperator(TransaksiPanen transaksiPanen){
        if(transaksiPanen.getVehicleMekanisasi() == null){
            return;
        }

        if(transaksiPanen.getVehicleMekanisasi().getVehicleCode() == null){
            return;
        }

        Vehicle vehicle = transaksiPanen.getVehicleMekanisasi();

        if(transaksiPanen.getKongsi() == null){
            return;
        }

        Pemanen pemanen = null;
        if(transaksiPanen.getKongsi().getSubPemanens().size() > 1){
            for (int i = 0 ; i < transaksiPanen.getKongsi().getSubPemanens().size(); i++){
                SubPemanen subPemanen = transaksiPanen.getKongsi().getSubPemanens().get(i);
                if(subPemanen.getTipePemanen() == SubPemanen.idOperator || subPemanen.getTipePemanen() == SubPemanen.idOperatorTonase){
                    pemanen = subPemanen.getPemanen();
                    break;
                }
            }
        }

        if(pemanen == null){
            return;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat sdfId = new SimpleDateFormat("yyMMdd");
        String tglPanen = sdf.format(transaksiPanen.getCreateDate());
        String tglPanenId = sdfId.format(transaksiPanen.getCreateDate());
        String estCode = GlobalHelper.getEstate().getEstCode();
        if(pemanen.getEstate() != null){
            estCode = pemanen.getEstate();
        }

        MekanisasiOperator mekanisasiOperator = new MekanisasiOperator(
                estCode + "-"+tglPanenId+ "-"+pemanen.getNik(),
                estCode,
                tglPanen,
                vehicle,
                pemanen,
                transaksiPanen.getTypeVehicleMekanisasi(),
                MekanisasiOperator.MekanisasiOperator_NotUplaod,
                transaksiPanen.getCreateDate(),
                transaksiPanen.getCreateBy()
        );

        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(mekanisasiOperator.getIdMekanisasiOperator(),gson.toJson(mekanisasiOperator),tglPanen);

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_OPERATOR_MEKANISASI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", mekanisasiOperator.getIdMekanisasiOperator()));
        if(cursor.size() > 0) {
            repository.update(dataNitrit);
        }else{
            repository.insert(dataNitrit);
        }
        db.close();
    }
}
