package id.co.ams_plantation.harvestsap.ui;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.esri.android.map.MapView;
import com.esri.android.runtime.ArcGISRuntime;
import com.esri.core.runtime.LicenseLevel;
import com.esri.core.runtime.LicenseResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.util.Arrays;
import java.util.Date;

import butterknife.BindView;
import id.co.ams_plantation.harvestsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.Estate;
import id.co.ams_plantation.harvestsap.model.NearestBlock;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.presenter.InputTphPresenter;
import id.co.ams_plantation.harvestsap.service.BluetoothService;
import id.co.ams_plantation.harvestsap.util.ActivityLoggerHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.NfcHelper;
import id.co.ams_plantation.harvestsap.util.QrHelper;
import id.co.ams_plantation.harvestsap.util.SharedPrefHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import id.co.ams_plantation.harvestsap.view.TphView;

import static id.co.ams_plantation.harvestsap.util.GlobalHelper.REQUEST_RESOLVE_ERROR;

/**
 * Created by user on 12/4/2018.
 */

public class TphActivity extends BaseActivity implements TphView,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener ,com.google.android.gms.location.LocationListener {

    public InputTphPresenter presenter;
    public Estate estate;
    public TransaksiPanen selectedTransaksiPanen;
    public NearestBlock nearestBlock;
    public int TYPE_NFC;
    public String valueNFC;
    public int flgNewHarvest;
    Activity activity;
    //    public BluetoothHelper bluetoothHelper;
    public QrHelper qrHelper;

//
//    public LocationManager mLocationManager;
//    public Location currentlocation;

    RelativeLayout lShowHide;
    RelativeLayout lShow;
    RelativeLayout lHide;
    ImageView ivShow;
    ImageView ivHide;


    @BindView(R.id.myCoordinatorLayout)
    public CoordinatorLayout myCoordinatorLayout;

    public Snackbar searchingGPS;
    public boolean tulisUlangNFC;

    /****************************************** NFC ******************************************************************/
    public Tag myTag;
    public NfcAdapter nfcAdapter;
    public PendingIntent pendingIntent;
    public IntentFilter[] writeTagFilters;

    /****************************************************************************************************************/

    private static Handler handler;

    @Override
    protected void initPresenter() {
        presenter = new InputTphPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LicenseResult licenseResult = ArcGISRuntime.setClientId("uFfCzKhpVfi0ggjz");
        LicenseLevel licenseLevel = ArcGISRuntime.License.getLicenseLevel();
        Log.e("License Result", licenseResult.toString() + " | " + licenseLevel.toString());
        setContentView(R.layout.new_layout_tph);
        getSupportActionBar().hide();
        startTimeScreen = new Date();

        mapView = findViewById(R.id.map);
        zoomToLocation = findViewById(R.id.zoomToLocation);
        rlMap = findViewById(R.id.rlMap);
        lShowHide = findViewById(R.id.lShowHide);
        ivShow = findViewById(R.id.ivShow);
        ivHide = findViewById(R.id.ivHide);
        lShow = findViewById(R.id.lShow);
        lHide = findViewById(R.id.lHide);

        zoomToLocation.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_my_location)
                .sizeDp(24)
                .colorRes(R.color.Black));
        ivShow.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_down).sizeDp(24)
                .colorRes(R.color.Black));

        ivHide.setImageDrawable(new
                IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_chevron_up).sizeDp(24)
                .colorRes(R.color.Black));

        rlMap.setVisibility(View.GONE);
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            String transaksiTph = intent.getExtras().getString("transaksiTph");
            if (transaksiTph != null) {
                Gson gson = new Gson();
                selectedTransaksiPanen = gson.fromJson(transaksiTph, TransaksiPanen.class);
            }
            String nearBlock = intent.getExtras().getString("nearestBlock");
            if(nearBlock != null){
                Gson gson = new Gson();
                nearestBlock = gson.fromJson(nearBlock,NearestBlock.class);
            }
            String flgNewHarvestS = intent.getExtras().getString("flgNewHarvest");
            if(flgNewHarvestS != null){
                flgNewHarvest = Integer.parseInt(flgNewHarvestS);
            }
        }

        lShow.setVisibility(View.GONE);
        lShowHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lHide.getVisibility() == View.GONE){
                    rlMap.setVisibility(View.VISIBLE);
                    lHide.setVisibility(View.VISIBLE);
                    lShow.setVisibility(View.GONE);
                }else{
                    rlMap.setVisibility(View.GONE);
                    lHide.setVisibility(View.GONE);
                    lShow.setVisibility(View.VISIBLE);
                }
            }
        });

        qrHelper = new QrHelper(this);
        activity = this;

        String strEst = Encrypts.decrypt(GlobalHelper.getFileContent(Encrypts.encrypt(GlobalHelper.SELECTED_ESTATE)));
        Gson gson = new Gson();
        estate = (Estate) gson.fromJson(strEst, Estate.class);
////      set di baseActivity
//        createLocationListener();

//        setupGoogleAPI();
        toolBarSetup();

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
        }
        NfcHelper.readFromIntent(getIntent());
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[]{tagDetected};
//        dataAllUser = GlobalHelper.getAllUser();
        main();

    }


    private void setupGoogleAPI(){
        // initialize Google API Client
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public static Handler getHandler() {
        return handler;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void main() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_container, TphInputFragment.getInstance())
                .commit();
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = findViewById(R.id.toolbar_icon);
        TextView textToolbar = findViewById(R.id.toolbar_text);
        ((BaseActivity) this).viewIndicator = findViewById(R.id.viewIndicator);
        ((BaseActivity) this).viewIndicator.setVisibility(View.VISIBLE);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backProses();
            }
        });
        textToolbar.setText(getResources().getString(R.string.panen));
    }

    public void backProses() {
//        if (getSupportFragmentManager().findFragmentById(R.id.content_container)
//                instanceof TphNewInputFragment) {
//            getSupportFragmentManager().beginTransaction()
//                    .replace(R.id.content_container, TphInputFragment.getInstance())
//                    .commit();
//        } else {

        SharedPrefHelper.applySharepref(HarvestApp.TRANSAKSI_PANEN_PREF,null);
        SharedPrefHelper.applySharepref(HarvestApp.FOTO_PANEN_PREF,null);

        setResult(GlobalHelper.RESULT_TPHACTIVITY);
        finish();
//        }
    }

    private void createLocationListener() {
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

//        searchingGPS = Snackbar.make(myCoordinatorLayout,getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
//        searchingGPS.show();
        WidgetHelper.warningFindGps(this,myCoordinatorLayout);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                1f, mLocationListener);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if(GlobalHelper.isDoubleClick()){
            return;
        }
        setIntent(intent);
        String idNFC = null;
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            idNFC = GlobalHelper.bytesToHexString(myTag.getId());
            Log.i("tagNFC", idNFC);
            Log.i("tagNFC", Arrays.toString(myTag.getTechList()));
        }
        Toast.makeText(this, "Tap Kartu", Toast.LENGTH_SHORT).show();
        valueNFC = NfcHelper.readFromIntent(intent);
        int formatNFC = NfcHelper.cekFormatNFC(valueNFC);
        if (valueNFC != null) {
            if (formatNFC != GlobalHelper.TYPE_NFC_SALAH) {
                if (valueNFC.length() == 5) {
                    if (NfcHelper.stat != NfcHelper.NFC_TIDAK_BISA_WRITE) {
                        if (getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof TphInputFragment) {
                            TphInputFragment fragment = (TphInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                            fragment.startLongOperation(NfcHelper.stat);
                        }
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.nfc_empty) + " " + GlobalHelper.LIST_NFC[formatNFC], Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (NfcHelper.stat != NfcHelper.NFC_TIDAK_BISA_WRITE) {
                        if (getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof TphInputFragment) {
                            TphInputFragment fragment = (TphInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                            fragment.startLongOperation(NfcHelper.stat);
                        }
                    }
                }
                TYPE_NFC = formatNFC;
            } else {
                if (!tulisUlangNFC) {
                    //nfc yang ke write baru setengah
                    if(selectedTransaksiPanen == null) {
                        if (getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof TphInputFragment) {
                            valueNFC = GlobalHelper.FORMAT_NFC_PANEN;
                            TphInputFragment fragment = (TphInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                            fragment.startLongOperation(NfcHelper.stat);
                            Log.d("TPHActivity", "onNewIntent: NFC Baru ketulis sebagian "+idNFC);
                        } else {
                            NfcHelper.uiFailedTapNFC(this,HarvestApp.getContext().getResources().getString(R.string.nfc_not_empty));
                            Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_not_empty), Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        if (!selectedTransaksiPanen.getIdNfc().equals(idNFC)) {
                            NfcHelper.uiFailedTapNFC(this,"Kartu Panen Yang Berbeda Seharusnya "+ selectedTransaksiPanen.getIdNfc());
                            Toast.makeText(HarvestApp.getContext(), "Kartu Panen Yang Berbeda Seharusnya "+ selectedTransaksiPanen.getIdNfc(), Toast.LENGTH_SHORT).show();
                        } else {
                            NfcHelper.uiFailedTapNFC(this,HarvestApp.getContext().getResources().getString(R.string.nfc_not_empty));
                            Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_not_empty), Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    //kalau ada kartu yang nga ke tap sempurna coba di cek dlu apakah id nfcnya sama
                    if (selectedTransaksiPanen != null && idNFC != null) {
                        if (selectedTransaksiPanen.getIdNfc() != null) {
                            if (idNFC.equals(selectedTransaksiPanen.getIdNfc())) {
                                // kalau sama langsung update aja biar kartu bisa kepakai
                                if (getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof TphInputFragment) {
                                    valueNFC = GlobalHelper.FORMAT_NFC_PANEN;
                                    TphInputFragment fragment = (TphInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                    fragment.startLongOperation(NfcHelper.stat);
                                }else{
                                    NfcHelper.uiFailedTapNFC(this,HarvestApp.getContext().getResources().getString(R.string.format_not_valid) + " 999");
                                    Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.format_not_valid) + " 999", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                NfcHelper.uiFailedTapNFC(this,HarvestApp.getContext().getResources().getString(R.string.format_not_valid) + " 999");
                                Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.format_not_valid) + " 999", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            NfcHelper.uiFailedTapNFC(this,HarvestApp.getContext().getResources().getString(R.string.format_not_valid) + " 999");
                            Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.format_not_valid) + " 999", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        NfcHelper.uiFailedTapNFC(this,HarvestApp.getContext().getResources().getString(R.string.format_not_valid) + " 999");
                        Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.format_not_valid) + " 999", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } else {
            NfcHelper.uiFailedTapNFC(this,HarvestApp.getContext().getResources().getString(R.string.format_not_valid) + " 999");
            Toast.makeText(this, R.string.format_not_valid, Toast.LENGTH_SHORT).show();
        }
    }

//    /******************************************************************************
//     **********************************Read From NFC Tag***************************
//     ******************************************************************************/
//    private void readFromIntent(Intent intent) {
//        String action = intent.getAction();
//        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
//                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
//                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
//            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
//            NdefMessage[] msgs = null;
//            if (rawMsgs != null) {
//                msgs = new NdefMessage[rawMsgs.length];
//                for (int i = 0; i < rawMsgs.length; i++) {
//                    msgs[i] = (NdefMessage) rawMsgs[i];
//                }
//            }
//            buildTagViews(msgs);
//        }
//    }
//
//    private void buildTagViews(NdefMessage[] msgs) {
//        if (msgs == null || msgs.length == 0) return;
//
//        String text = "";
////        String tagId = new String(msgs[0].getRecords()[0].getType());
//        byte[] payload = msgs[0].getRecords()[0].getPayload();
//        String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16"; // Get the Text Encoding
//        int languageCodeLength = payload[0] & 0063; // Get the Language Code, e.g. "en"
//        // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");
//
//        try {
//            // Get the Text
//            text = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
//        } catch (UnsupportedEncodingException e) {
//            Log.e("UnsupportedEncoding", e.toString());
//        }
//    }

    @Override
    public void onStart() {
        super.onStart();
        if(mGoogleApiClient != null) {
            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            }
        }
        // If Bluetooth is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!bluetoothHelper.mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, GlobalHelper.REQUEST_ENABLE_BT);
            // Otherwise, setup the session
        } else {
            if (bluetoothHelper.mService == null)
                bluetoothHelper.mService = new BluetoothService(this, bluetoothHelper.mHandler);
        }
    }

    @Override
    public void onBackPressed() {
        backProses();
    }

    @Override
    protected void onResume() {
        super.onResume();

        startTimeScreen = new Date();
        if(mGoogleApiClient != null) {
            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.reconnect();
            }
        }
        NfcHelper.WriteModeOn(nfcAdapter, this, pendingIntent, writeTagFilters);
        if (bluetoothHelper != null) {
            if (bluetoothHelper.mService != null) {

                if (bluetoothHelper.mService.getState() == BluetoothService.STATE_NONE) {
                    // Start the Bluetooth services
                    bluetoothHelper.mService.start();
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
        }
        endTimeScreen = new Date();
        if(GlobalHelper.getUser()!=null){
            ActivityLoggerHelper.recordScreenTime(TphActivity.class.getSimpleName(),startTimeScreen, endTimeScreen);
        }
        NfcHelper.WriteModeOff(nfcAdapter, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        endTimeScreen = new Date();
        if(GlobalHelper.getUser()!=null){
            ActivityLoggerHelper.recordScreenTime(TphActivity.class.getSimpleName(),startTimeScreen, endTimeScreen);
        }
        // Stop the Bluetooth services
        if (bluetoothHelper != null) {
            if (bluetoothHelper.mService != null)
                bluetoothHelper.mService.stop();
        }
        if(mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
//        searchingGPS = Snackbar.make(myCoordinatorLayout,getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
//        searchingGPS.show();
        WidgetHelper.warningFindGps(this,myCoordinatorLayout);
        starLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TphActivity.class.getName(), "GoogleAPIClient connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // Error with resolution, try again
                mGoogleApiClient.connect();
            }
        }else{
            WidgetHelper.showSnackBar(myCoordinatorLayout,"onConnectionFailed");
//            searchingGPS = Snackbar.make(myCoordinatorLayout,"",Snackbar.LENGTH_INDEFINITE);
//            searchingGPS.show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        locationChangeListener(location);
    }
}
