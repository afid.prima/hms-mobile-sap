package id.co.ams_plantation.harvestsap.TableView.holder;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;

/**
 * Created by user on 12/24/2018.
 */

public class TPHRowHeaderViewHolder extends AbstractViewHolder {

    private final TextView row_header_textview;
    private final LinearLayout row_header_container;
    private TableViewCell cell;

    public TPHRowHeaderViewHolder(View layout) {
        super(layout);
        row_header_textview = (TextView) itemView.findViewById(R.id.row_header_textview);
        row_header_container = (LinearLayout) itemView.findViewById(R.id.row_header_container);
    }

    public void setCell(TableViewRowHeader cell) {
        String [] id = cell.getId().split("_");
        this.cell = cell;
        row_header_textview.setText(String.valueOf(cell.getData()));
        switch (Integer.parseInt(id[2])){
            case TransaksiPanen.SAVE_ONLY:
                row_header_container.setBackgroundColor(TransaksiPanen.COLOR_SAVE_ONLY);
                break;
            case TransaksiPanen.TAP:
                row_header_container.setBackgroundColor(TransaksiPanen.COLOR_TAP);
                break;
            case TransaksiPanen.UPLOAD:
                row_header_container.setBackgroundColor(TransaksiPanen.COLOR_UPLOAD);
                break;
            case TransaksiPanen.SUDAH_DIANGKUT:
                row_header_container.setBackgroundColor(TransaksiPanen.COLOR_SUDAH_DIANGKUT);
                break;
            case TransaksiPanen.SUDAH_DIPKS:
                row_header_container.setBackgroundColor(TransaksiPanen.COLOR_SUDAH_DIPKS);
                break;
        }
    }

    public String getCellId(){
        return this.cell.getId();
    }
}
