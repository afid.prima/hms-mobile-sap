package id.co.ams_plantation.harvestsap.presenter;

import com.alibaba.fastjson.JSONObject;

import id.co.ams_plantation.harvestsap.model.ExampleModel;
import id.co.ams_plantation.harvestsap.presenter.base.BasePresenter;
import id.co.ams_plantation.harvestsap.view.ExampleView;

/*
    RULES :
    1. Always Extend ApiPresenter
    2. Always Attach Corresponding View
    3. Create method / data transaction logic between
    4. You may use Model directly/indirectly from this class
    5. You may use another helper class as long as its logic only
    6. This is logic only class.

    DO NOT :
    1. Update UI / set UI on this class
    2. Calling any View class.
 */

public class ExamplePresenter extends BasePresenter {
    public ExamplePresenter(ExampleView view){attachView(view);}
    public void exampleLogic(){

    }

    //If your result want tobe presented as model, define on presenter.
    public ExampleModel parseResultModel(String resultJSON){
        ExampleModel model = (ExampleModel) JSONObject.parse(resultJSON);
        //do logic stuff
        return model;
    }


}
