package id.co.ams_plantation.harvestsap.util;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestsap.Fragment.SettingFragment;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;

/**
 * Created on : 19,May,2021
 * Author     : Afid
 */

public class QrDoubleSPBHelpper {

    public static final int SingelQr = 0;
    public static final int DoubleQr = 1;

    Context context;
    MainMenuActivity mainMenuActivity;
    SettingFragment settingFragment;
    android.support.v7.app.AlertDialog listrefrence;
    SharedPrefHelper sharedPrefHelper;

    public QrDoubleSPBHelpper(Context context) {
        this.context = context;
        sharedPrefHelper = new SharedPrefHelper(context);
    }

    public void choseQrSPB(){
        if (context instanceof MainMenuActivity){
            mainMenuActivity = ((MainMenuActivity) context);
            Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
            if(fragment instanceof SettingFragment) {
                settingFragment = ((SettingFragment) fragment);
            }
        }

        View view = LayoutInflater.from(context).inflate(R.layout.doube_qr_layout,null);
        SegmentedButtonGroup sbgDoubleQr = (SegmentedButtonGroup) view.findViewById(R.id.sbgDoubleQr);

        String sharePrefQr = sharedPrefHelper.getName(SharedPrefHelper.KEY_DoubleQrSPB);
        int position = DoubleQr;
        if(sharePrefQr != null){
            position = Integer.parseInt(sharePrefQr);
        }
        sbgDoubleQr.setPosition(position);
        sharedPrefHelper.commit(SharedPrefHelper.KEY_DoubleQrSPB,String.valueOf(position));

        sbgDoubleQr.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(int position) {
                sharedPrefHelper.commit(SharedPrefHelper.KEY_DoubleQrSPB,String.valueOf(position));
            }
        });

        listrefrence = WidgetHelper.showListReference(listrefrence,view,context);
    }
}
