package id.co.ams_plantation.harvestsap.model;

public class ISCCInformation {
    private int idCertificate;
    private String estCode;
    private double validFrom;
    private double validUntil;
    private String ghgValue;
    private String certificateNumber;
    private String certificateType;
    private String ghgType;
    private String ghgSatuan;
    private String estNewCode;

    public ISCCInformation() {
    }

    public ISCCInformation(int idCertificate, String estCode, double validFrom, double validUntil, String ghgValue, String certificateNumber, String certificateType, String ghgType, String ghgSatuan) {
        this.idCertificate = idCertificate;
        this.estCode = estCode;
        this.validFrom = validFrom;
        this.validUntil = validUntil;
        this.ghgValue = ghgValue;
        this.certificateNumber = certificateNumber;
        this.certificateType = certificateType;
        this.ghgType = ghgType;
        this.ghgSatuan = ghgSatuan;
    }

    public int getIdCertificate() {
        return idCertificate;
    }

    public void setIdCertificate(int idCertificate) {
        this.idCertificate = idCertificate;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public double getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(double validFrom) {
        this.validFrom = validFrom;
    }

    public double getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(double validUntil) {
        this.validUntil = validUntil;
    }

    public String getGhgValue() {
        return ghgValue;
    }

    public void setGhgValue(String ghgValue) {
        this.ghgValue = ghgValue;
    }

    public String getCertificateNumber() {
        return certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber) {
        this.certificateNumber = certificateNumber;
    }

    public String getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(String certificateType) {
        this.certificateType = certificateType;
    }

    public String getGhgType() {
        return ghgType;
    }

    public void setGhgType(String ghgType) {
        this.ghgType = ghgType;
    }

    public String getGhgSatuan() {
        return ghgSatuan;
    }

    public void setGhgSatuan(String ghgSatuan) {
        this.ghgSatuan = ghgSatuan;
    }

    public String getEstNewCode() {
        return estNewCode;
    }

    public void setEstNewCode(String estNewCode) {
        this.estNewCode = estNewCode;
    }
}
