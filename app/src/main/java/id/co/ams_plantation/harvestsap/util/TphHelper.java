package id.co.ams_plantation.harvestsap.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.ArraySet;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.esri.android.map.Callout;
import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.LocationDisplayManager;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISLocalTiledLayer;
import com.esri.android.map.event.OnPanListener;
import com.esri.android.map.event.OnStatusChangedListener;
import com.esri.android.map.event.OnZoomListener;
import com.esri.android.map.ogc.kml.KmlLayer;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.CompositeSymbol;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;
import com.google.gson.Gson;
import com.mikepenz.iconics.utils.Utils;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.Estate;
import id.co.ams_plantation.harvestsap.model.Langsiran;
import id.co.ams_plantation.harvestsap.model.NearestBlock;
import id.co.ams_plantation.harvestsap.model.ReasonUnharvestReturn;
import id.co.ams_plantation.harvestsap.model.ReasonUnharvestTPH;
import id.co.ams_plantation.harvestsap.model.StampImage;
import id.co.ams_plantation.harvestsap.model.TPH;
import id.co.ams_plantation.harvestsap.model.TPHnLangsiran;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.MapActivity;
import id.co.ams_plantation.harvestsap.ui.TphActivity;
import ng.max.slideview.SlideView;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by user on 12/12/2018.
 */

public class TphHelper {

    public AlertDialog listrefrence;
    FragmentActivity activity;

    SlidingUpPanelLayout mLayout;
    MapView map;
    LocationDisplayManager locationDisplayManager;
    FloatingActionButton fabZoomToLocation;
    FloatingActionButton fabZoomToEstate;
    SegmentedButtonGroup segmentedButtonGroup;
    RelativeLayout layoutBottomSheet;
    ImageView ivCrosshair;
    LinearLayout ll_latlon;
    TextView tv_lat_manual;
    TextView tv_lon_manual;
    TextView tv_dis_manual;
    TextView tv_deg_manual;
//    RadioGroup radioGroup;

    ImageView ivPhoto;
    ImageView imagecacnel;
    ImageView imagesview;
    RelativeLayout rl_ipf_takepicture;
    SliderLayout sliderLayout;

    LinearLayout lheader;
    LinearLayout btnShowForm;
    AppCompatEditText etNamaTph;
    EditText etEst;
    AppCompatEditText etAncak;
    CompleteTextViewHelper etBlock;
    CompleteTextViewHelper etAfd;
    //    ChipView cvAncak;
    Button btnSave;
//    BottomSheetBehavior sheetBehavior;
    View langsiran_new;
    View pks_new;

    GraphicsLayer graphicsLayer;
    GraphicsLayer graphicsLayerTPH;
    SpatialReference spatialReference;
    boolean isMapLoaded=false;

    ArrayList<File> ALselectedImage;

    ArcGISLocalTiledLayer tiledLayer;
    KmlLayer kmlLayer;
    String locKmlMap;

    boolean newData;
    BaseActivity baseActivity;
    TphActivity tphActivity;
    MapActivity mapActivity;
    int idgraptemp = 0;
    boolean manual;

    public double latawal = 0.0;
    public double longawal = 0.0;
    public double latitude = 0.0;
    public double longitude = 0.0;

    public HashMap<Integer,TPH> hashpoint;
    public Callout callout;

    TPH selectedTph;
    Boolean popUpForm = false;
    AlertDialog alertDialog2;
    boolean isNewData;
    LinearLayout lAncak;
//    boolean awal = true;
    HashMap<String,ArrayList<ReasonUnharvestTPH>> hashMapReasonUnharvestTPH;
    ReasonUnharvestReturn reasonUnharvestReturn;
    ReasonUnharvestTPH ruSelected;
    HashMap<Integer,ReasonUnharvestTPH> hashMapReasonUnharvestTPHRadioButton;

    public TphHelper(FragmentActivity activity) {
        this.activity = activity;
    }

    //unutk deklarias dari map activity
    public void setupFormInput(View view,TPH tph){
        mapActivity = (MapActivity) activity;
        locKmlMap = mapActivity.locKmlMap;
        ALselectedImage= new ArrayList<>();
        selectedTph = tph;
        if(ivPhoto == null) {
            ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
            imagecacnel = (ImageView) view.findViewById(R.id.imagecacnel);
            imagesview = (ImageView) view.findViewById(R.id.imagesview);
            rl_ipf_takepicture = (RelativeLayout) view.findViewById(R.id.rl_ipf_takepicture);
            lAncak = (LinearLayout) view.findViewById(R.id.lAncak);
            sliderLayout = (SliderLayout) view.findViewById(R.id.sliderLayout);
            etNamaTph = (AppCompatEditText) view.findViewById(R.id.etNamaTph);
            etEst = (EditText) view.findViewById(R.id.etEst);
            etAfd = (CompleteTextViewHelper) view.findViewById(R.id.etAfd);
            etAncak = (AppCompatEditText) view.findViewById(R.id.etAncak);
            etBlock = (CompleteTextViewHelper) view.findViewById(R.id.etBlock);
            btnSave = (Button) view.findViewById(R.id.btnSave);
        }else{
            if(tph == null) {
                setupimageslide();
                etNamaTph.setText("");
                etAncak.setText("");
            }
        }
        lAncak.setVisibility(View.GONE);
        btnSave.setVisibility(View.VISIBLE);

        if(tph != null) {
            isNewData = false;
            showValueForm(tph);
            if(GlobalHelper.distance(((BaseActivity) activity).currentlocation.getLatitude(),latitude,
                    ((BaseActivity) activity).currentlocation.getLongitude(),longitude) > GlobalHelper.RADIUS){
                btnSave.setVisibility(View.GONE);
            }
        }else{
            isNewData = true;
            Estate estate = tph == null ? GlobalHelper.getEstate() : GlobalHelper.getEstateByEstCode(tph.getEstCode());
            etEst.setText(estate.getEstName());
        }

        etBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etBlock.showDropDown();
            }
        });
        etBlock.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    etBlock.showDropDown();
                }
            }
        });

        etBlock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                etAfd.requestFocus();
            }
        });

        etAfd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etAfd.showDropDown();
            }
        });
        etAfd.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    etBlock.showDropDown();
                }
            }
        });
        etAfd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(ALselectedImage.size() == 0){
                    GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_NEW_TPH_MAP_ACTIVITY;
                    EasyImage.openCamera(activity,1);
                }
                GlobalHelper.hideKeyboard(activity);
            }
        });

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ALselectedImage.size() > 3) {
                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.warning_take_foto),Toast.LENGTH_LONG).show();
                }else {
                    if(etBlock.getText().toString().isEmpty()){
                        etBlock.requestFocus();
                        Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.please_chose_block),Toast.LENGTH_LONG).show();
                        return;
                    }

                    StampImage stampImage = new StampImage();
                    stampImage.setBlock(etBlock.getText().toString());

                    Gson gson  = new Gson();
                    SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(HarvestApp.STAMP_IMAGES,gson.toJson(stampImage));
                    editor.apply();

                    GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_NEW_TPH_MAP_ACTIVITY;
                    EasyImage.openCamera(activity,1);
                }
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
                String stringIsi = preferences.getString(HarvestApp.STAMP_IMAGES,null);
                Gson gson  = new Gson();
                StampImage stampImage = gson.fromJson(stringIsi,StampImage.class);
                if(stampImage != null) {
                    if (!stampImage.getBlock().equals(etBlock.getText().toString())) {
                        stampImage.setBlock(etBlock.getText().toString());
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(HarvestApp.STAMP_IMAGES, gson.toJson(stampImage));
                        editor.apply();

                        GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_NEW_TPH_MAP_ACTIVITY;
                        EasyImage.openCamera(activity, 1);
                        Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.warning_take_foto2), Toast.LENGTH_LONG).show();
                        return;
                    }
                }else if (!etBlock.getText().toString().isEmpty()){
                    stampImage = new StampImage();
                    stampImage.setBlock(etBlock.getText().toString());
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(HarvestApp.STAMP_IMAGES, gson.toJson(stampImage));
                    editor.apply();

                    GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_NEW_TPH_MAP_ACTIVITY;
                    EasyImage.openCamera(activity, 1);
                    return;
                }else{
                    Toast.makeText(HarvestApp.getContext(), "Pilih Block Dulu ya", Toast.LENGTH_LONG).show();
                    return;
                }

                mapActivity.mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                mapActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new LongOperation().execute();
                    }
                });
            }
        });
    }

    public ArrayList<TPH> setDataTph(String Estate,String Block){
        ArrayList<TPH> tphArrayList = new ArrayList<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> tphIterable = repository.find(ObjectFilters.eq("param1", Block));
        for (Iterator iterator = tphIterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            TPH tph = gson.fromJson(dataNitrit.getValueDataNitrit(),TPH.class);
//            double radius = GlobalHelper.distance(latawal,tph.getLatitude(),
//                    longawal,tph.getLongitude());
            if ((tph.getStatus() == TPH.STATUS_TPH_MASTER )
                    && (tph.getCreateBy() != TPH.USER_GIS_SYSTEM) &&
                    (tph.getBlock().equals(Block)) && (tph.getEstCode().equals(Estate))) {
                tphArrayList.add(tph);
            }
        }
        db.close();
        return tphArrayList;
    }

    public TPH getTphBayangan(String Estate,String Block){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> tphIterable = repository.find(ObjectFilters.eq("param1", Block));
        for (Iterator iterator = tphIterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            TPH tph = gson.fromJson(dataNitrit.getValueDataNitrit(),TPH.class);
//            double radius = GlobalHelper.distance(latawal,tph.getLatitude(),
//                    longawal,tph.getLongitude());
            if ((tph.getCreateBy() == TPH.USER_GIS_SYSTEM) &&
                    (tph.getBlock().equals(Block)) && (tph.getEstCode().equals(Estate))) {
                db.close();
                return tph;
            }
        }
        db.close();
        return null;
    }

    public ArrayList<TPH> cariTphTerdekat(ArrayList<TPH> alltph){
        double distance = 0.0;
        ArrayList<TPH> selectTph = new ArrayList<>();
        TPH tphX = null;
        baseActivity = (BaseActivity) activity;
        if(locationDisplayManager != null){
            baseActivity.currentlocation = locationDisplayManager.getLocation();
        }
        for(int i = 0; i < alltph.size(); i++){
            TPH tph = alltph.get(i);
            double dis = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(),tph.getLatitude(),
                    baseActivity.currentlocation.getLongitude(),tph.getLongitude());

            if(dis <= GlobalHelper.RADIUS){
                selectTph.add(tph);
            }
            if(i == 0){
                tphX = alltph.get(i);
                distance = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(),tph.getLatitude(),
                        baseActivity.currentlocation.getLongitude(),tph.getLongitude());
            }else{
                TPH selectTphX = alltph.get(i);
                double disX = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(),selectTphX.getLatitude(),
                        baseActivity.currentlocation.getLongitude(),selectTphX.getLongitude());
                if(disX <= distance){
                    distance = disX;
                    tphX = selectTphX;
                }

            }

        }
        //nanti ini di munculkan
//        if(selectTph.size() == 0){
//            if(tphX != null) {
//                Log.d(this.getClass().toString(), "cariTphTerdekat: distance "+distance );
//                selectTph.add(tphX);
//            }
//        }
        return selectTph;
    }

    public ArrayList<TPHnLangsiran> setDataTphnLangsiran(String Estate, String[] Block){
        ArrayList<TPHnLangsiran> tphArrayList = new ArrayList<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
        if(db != null) {
            for(int i = 0 ; i < Block.length; i++) {
                Log.d("TPHHelper", "setDataTphnLangsiran: "+ Block[i]);
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                Cursor<DataNitrit> tphIterable = repository.find(ObjectFilters.eq("param1", Block[i]));
                for (Iterator iterator = tphIterable.iterator(); iterator.hasNext(); ) {
                    DataNitrit dataNitrit = (DataNitrit) iterator.next();
                    Gson gson = new Gson();
                    TPH tph = gson.fromJson(dataNitrit.getValueDataNitrit(), TPH.class);
                    Log.d("TPHHelper", "setDataTphnLangsiran: "+ gson.toJson(tph));
                    if ((tph.getStatus() != TPH.STATUS_TPH_DELETED && tph.getStatus() != TPH.STATUS_TPH_INACTIVE_QC_MOBILE)
                            && (tph.getCreateBy() != TPH.USER_GIS_SYSTEM) &&
                            (tph.getBlock().equals(Block[i])) && (tph.getEstCode().equals(Estate))) {
                        tphArrayList.add(new TPHnLangsiran(tph.getNoTph(), tph));
                    }else if(tph.getReasonUnharvestTPH() != null){
                        tphArrayList.add(new TPHnLangsiran(tph.getNoTph(), tph));
                    }
                }
            }
            db.close();
        }

        db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_LANGSIRAN);
        if(db != null) {
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> tphIterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = tphIterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                Langsiran langsiran = gson.fromJson(dataNitrit.getValueDataNitrit(), Langsiran.class);
                for(int i = 0 ; i < Block.length; i++) {
                    if ((langsiran.getStatus() != Langsiran.STATUS_LANGSIRAN_DELETED && langsiran.getStatus() != Langsiran.STATUS_LANGSIRAN_INACTIVE_QC_MOBILE)
                        && (langsiran.getBlock().equals(Block[i]))) {
                        tphArrayList.add(new TPHnLangsiran(langsiran.getIdTujuan(), langsiran));
                    }
                }
            }
            db.close();
        }
        return tphArrayList;
    }

    public TPHnLangsiran cariTphnLangsiran(ArrayList<TPHnLangsiran> alltph) {
        double distance = 0.0;
        TPHnLangsiran selectTph = null;
        baseActivity = (BaseActivity) activity;
        for (int i = 0; i < alltph.size(); i++) {
            if (i == 0) {
                selectTph = alltph.get(i);
                if (selectTph.getTph() != null) {
                    distance = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(), selectTph.getTph().getLatitude(),
                            baseActivity.currentlocation.getLongitude(), selectTph.getTph().getLongitude());
                } else if (selectTph.getLangsiran() != null) {
                    distance = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(), selectTph.getLangsiran().getLatitude(),
                            baseActivity.currentlocation.getLongitude(), selectTph.getLangsiran().getLongitude());
                }
            } else {
                TPHnLangsiran selectTphX = alltph.get(i);
                double disX = 0.0;
                if (selectTphX.getTph() != null) {
                    disX = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(), selectTphX.getTph().getLatitude(),
                            baseActivity.currentlocation.getLongitude(), selectTphX.getTph().getLongitude());
                } else if (selectTphX.getLangsiran() != null) {
                    disX = GlobalHelper.distance(baseActivity.currentlocation.getLatitude(), selectTphX.getLangsiran().getLatitude(),
                            baseActivity.currentlocation.getLongitude(), selectTphX.getLangsiran().getLongitude());
                }
                if (disX <= distance) {
                    distance = disX;
                    selectTph = selectTphX;
                }

            }
        }
        if (selectTph == null) {
            if(alltph.size() > 0){
                return alltph.get(0);
            }else{
                return null;
            }
        } else {
            return selectTph;
        }
    }

    public Langsiran getLangsrian(String idLangsrian){
        Langsiran langsiran = null;
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_LANGSIRAN);
        if(db != null) {
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Cursor<DataNitrit> tphIterable = repository.find(ObjectFilters.eq("idDataNitrit", idLangsrian));
            for (Iterator iterator = tphIterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                langsiran = gson.fromJson(dataNitrit.getValueDataNitrit(), Langsiran.class);
                break;
            }
            db.close();
        }
        return langsiran;
    }

    public NearestBlock getNearBlock(Location location){
        File kml = new File(GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK));
        Set<String> allBlock = GlobalHelper.getNearBlock(kml, location.getLatitude(),location.getLongitude());
        Set<String> bloks = new ArraySet<>();
        String block = "";
        for (String est : allBlock) {
            try {
                String[] split = est.split(";");
                bloks.add(split[2]);
                block = split[2];
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            String [] estAfdBlock = GlobalHelper.getAndAssignBlock(kml,location.getLatitude(),location.getLongitude()).split(";");
            block = estAfdBlock[2];
        }catch (Exception e){
            e.printStackTrace();
        }

        return new NearestBlock(location,block,bloks);
    }

    public NearestBlock getNearBlock(double lat, double lon){
        File kml = new File(GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK));
        Set<String> allBlock = GlobalHelper.getNearBlock(kml, lat,lon);
        Set<String> bloks = new ArraySet<>();
        String block = "";
        for (String est : allBlock) {
            try {
                String[] split = est.split(";");
                bloks.add(split[2]);
                block = split[2];
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            String [] estAfdBlock = GlobalHelper.getAndAssignBlock(kml,lat,lon).split(";");
            block = estAfdBlock[2];
        }catch (Exception e){
            e.printStackTrace();
        }

        return new NearestBlock(null,block,bloks);
    }

    public static NearestBlock getNearBlockStatic(double lat, double lon){
        File kml = new File(GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK));
        Set<String> allBlock = GlobalHelper.getNearBlock(kml, lat,lon);
        Set<String> bloks = new ArraySet<>();
        String block = "";
        for (String est : allBlock) {
            try {
                String[] split = est.split(";");
                bloks.add(split[2]);
                block = split[2];
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            String [] estAfdBlock = GlobalHelper.getAndAssignBlock(kml,lat,lon).split(";");
            block = estAfdBlock[2];
        }catch (Exception e){
            e.printStackTrace();
        }

        return new NearestBlock(null,block,bloks);
    }

    //memunuclkan popupmap tph pada proses transaksi panen
//    public void choseTph(String block){
////        awal = true;
//        idgraptemp = 0;
//        manual = false;
//
//        if(activity instanceof TphActivity) {
//            tphActivity = ((TphActivity) activity);
//            if (((TphActivity) activity).searchingGPS != null) {
//                ((TphActivity) activity).searchingGPS.dismiss();
//            }
//        }
//
//        View view = LayoutInflater.from(activity).inflate(R.layout.tph_new,null);
//        LicenseResult licenseResult = ArcGISRuntime.setClientId("uFfCzKhpVfi0ggjz");
//        LicenseLevel licenseLevel = ArcGISRuntime.License.getLicenseLevel();
//
//        mLayout = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);
//        map = (MapView) view.findViewById(R.id.map);
//        fabZoomToLocation = (FloatingActionButton) view.findViewById(R.id.fabZoomToLocation);
//        fabZoomToEstate = (FloatingActionButton) view.findViewById(R.id.fabZoomToEstate);
//        segmentedButtonGroup = (SegmentedButtonGroup) view.findViewById(R.id.segmentedButtonGroup);
////        layoutBottomSheet = (RelativeLayout) view.findViewById(R.id.bottom_sheet);
//        btnShowForm = (LinearLayout) view.findViewById(R.id.btnShowForm);
//
//        langsiran_new = (View) view.findViewById(R.id.langsiran_new);
//        pks_new = (View) view.findViewById(R.id.pks_new);
//
//        ll_latlon = (LinearLayout) view.findViewById(R.id.ll_latlon);
//        ivCrosshair = (ImageView) view.findViewById(R.id.iv_crosshair);
//        tv_lat_manual = (TextView) view.findViewById(R.id.tv_lat_manual);
//        tv_lon_manual = (TextView) view.findViewById(R.id.tv_lon_manual);
//        tv_dis_manual = (TextView) view.findViewById(R.id.tv_dis_manual);
//        tv_deg_manual = (TextView) view.findViewById(R.id.tv_deg_manual);
//        ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
//        imagecacnel = (ImageView) view.findViewById(R.id.imagecacnel);
//        imagesview = (ImageView) view.findViewById(R.id.imagesview);
//        rl_ipf_takepicture = (RelativeLayout) view.findViewById(R.id.rl_ipf_takepicture);
//        sliderLayout = (SliderLayout) view.findViewById(R.id.sliderLayout);
//
////        lheader = (LinearLayout) view.findViewById(R.id.lheader);
//        etNamaTph = (AppCompatEditText) view.findViewById(R.id.etNamaTph);
//        etEst = (EditText) view.findViewById(R.id.etEst);
//        etAfd = (CompleteTextViewHelper) view.findViewById(R.id.etAfd);
//        etAncak = (AppCompatEditText) view.findViewById(R.id.etAncak);
//        etBlock = (CompleteTextViewHelper) view.findViewById(R.id.etBlock);
////        cvAncak = (ChipView) view.findViewById(R.id.cvAncak);
//        btnSave = (Button) view.findViewById(R.id.btnSave);
//
//        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
//            @Override
//            public void onPanelSlide(View panel, float slideOffset) {
//                Log.i("TPH helper", "onPanelSlide, offset " + slideOffset);
//            }
//
//            @Override
//            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
//                if(manual && newState ==  SlidingUpPanelLayout.PanelState.EXPANDED){
//                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.please_stop_manual_point),Toast.LENGTH_SHORT).show();
//                }
//                Log.i("TPH helper", "onPanelStateChanged " + newState);
//            }
//        });
//        mLayout.setFadeOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//            }
//        });
//
//        langsiran_new.setVisibility(View.GONE);
//        pks_new.setVisibility(View.GONE);
//
//        graphicsLayer = new GraphicsLayer();
//        graphicsLayerTPH = new GraphicsLayer();
//        baseActivity = (BaseActivity) activity;
//        ALselectedImage = new ArrayList<>();
//
//        latawal = baseActivity.currentlocation.getLatitude();
//        longawal = baseActivity.currentlocation.getLongitude();
//        latitude = baseActivity.currentlocation.getLatitude();
//        longitude = baseActivity.currentlocation.getLongitude();
//
//        fabZoomToEstate.setImageDrawable(new
//                IconicsDrawable(activity)
//                .icon(MaterialDesignIconic.Icon.gmi_landscape)
//                .sizeDp(24)
//                .colorRes(R.color.White));
//
//        fabZoomToLocation.setImageDrawable(new
//                IconicsDrawable(activity)
//                .icon(MaterialDesignIconic.Icon.gmi_pin)
//                .sizeDp(24)
//                .colorRes(R.color.White));
//
//        mapSetup();
//        switchCrossHairUI(false);
//        fabZoomToEstate.callOnClick();
//
//        setupBlokAfdeling(latitude,longitude);
//
////        List<String> listableObjects = getListEstate(GlobalHelper.getUser(),tphActivity.estate);
////        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
////                android.R.layout.simple_dropdown_item_1line, listableObjects);
////        etEst.setAdapter(adapter);
//        etEst.setText(tphActivity.estate.getEstName());
//
//        etBlock.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                etBlock.showDropDown();
//            }
//        });
//
//        etAfd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                etAfd.showDropDown();
//            }
//        });
//
//        ivPhoto.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(ALselectedImage.size() > 3) {
//                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.warning_take_foto),Toast.LENGTH_LONG).show();
//                }else {
//                    GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_NEW_TPH;
//                    EasyImage.openCamera(activity,1);
//                }
//            }
//        });
//        btnSave.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new LongOperation().execute();
//            }
//        });
//
//        if(selectedTph != null){
//            showValueForm(selectedTph);
//        }
//
//        fabZoomToEstate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(map.isLoaded()){
//                    if(tiledLayer!=null){
//                        map.setExtent(tiledLayer.getFullExtent());
//                    }
//                    map.invalidate();
//                    if(idgraptemp == 0) {
//                        idgraptemp = addGrapTemp(latawal, longawal, idgraptemp);
//                        pointTPHSetup(GlobalHelper.getEstate().getEstCode(),block);
//
//                        fabZoomToLocation.callOnClick();
//                        //awal = false;
//                    }
//
////                    if(awal){
////                        fabZoomToLocation.callOnClick();
////                        awal = false;
////                    }
//
//                    map.setOnSingleTapListener(new OnSingleTapListener() {
//                        @Override
//                        public void onSingleTap(float v, float v1) {
//                            if(map.isLoaded()){
//                                defineTapSetup(v,v1);
//                            }
//                        }
//                    });
//                }
//            }
//        });
//
//        fabZoomToLocation.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (tphActivity.currentLocationOK()) {
//                    latawal = tphActivity.currentlocation.getLatitude();
//                    longawal = tphActivity.currentlocation.getLongitude();
//                    latitude = latawal;
//                    longitude = longawal;
//                    zoomToLocation(latawal,longawal,spatialReference);
//                    idgraptemp = addGrapTemp(latawal,longawal,idgraptemp);
//                }else{
//                    tphActivity.searchingGPS = Snackbar.make(tphActivity.myCoordinatorLayout,activity.getResources().getString(R.string.search_gps),Snackbar.LENGTH_INDEFINITE);
//                    tphActivity.searchingGPS.show();
//                }
//            }
//        });
//
//        segmentedButtonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
//            @Override
//            public void onClickedButton(int position) {
//                switch (position){
//                    case 0:{
//                        manual = false;
//                        switchCrossHairUI(false);
//                        idgraptemp = addGrapTemp(latitude,longitude,idgraptemp);
//                        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
//                        break;
//                    }
//                    case 1:{
//                        manual = true;
//                        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
////                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                        setDefaultManual();
//                        break;
//                    }
//                }
//            }
//        });
//
////        btnShowForm.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                if(!manual){
////                    showFormDialog();
////                }else{
////                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.please_stop_manual_point),Toast.LENGTH_SHORT).show();
////                }
////            }
////        });
////        lheader.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View view) {
////                if (!manual) {
////                    switch (sheetBehavior.getState()) {
////                        case BottomSheetBehavior.STATE_COLLAPSED:
////                            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
////                            break;
////                        case BottomSheetBehavior.STATE_EXPANDED:
////                            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
////                            break;
////                    }
////                }else{
////                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.please_stop_manual_point),Toast.LENGTH_SHORT).show();
////                }
////            }
////        });
//
////        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
////        /**
////         * bottom sheet state change listener
////         * we are changing button text when sheet changed state
////         * */
////        // set the peek height
////        //sheetBehavior.setPeekHeight(190);
////        sheetBehavior.setHideable(false);
////        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
////            @Override
////            public void onStateChanged(@NonNull View bottomSheet, int newState) {
////                if (manual) {
////                    Toast.makeText(HarvestApp.getContext(),activity.getResources().getString(R.string.please_stop_manual_point),Toast.LENGTH_SHORT).show();
////                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
////                } else {
////                    switch (newState) {
////                        case BottomSheetBehavior.STATE_HIDDEN:
////                            break;
////                        case BottomSheetBehavior.STATE_EXPANDED:
////                            fabZoomToEstate.hide();
////                            fabZoomToLocation.hide();
////                            segmentedButtonGroup.setVisibility(View.GONE);
////                            Log.v("sheet", "STATE_EXPANDED");
////                            break;
////                        case BottomSheetBehavior.STATE_COLLAPSED:
////                            fabZoomToEstate.show();
////                            fabZoomToLocation.show();
////                            segmentedButtonGroup.setVisibility(View.VISIBLE);
////                            Log.v("sheet", "STATE_COLLAPSED");
////                            break;
////                        case BottomSheetBehavior.STATE_DRAGGING:
////                            break;
////                        case BottomSheetBehavior.STATE_SETTLING:
////                            break;
////                    }
////                }
////            }
////
////            @Override
////            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
////
////            }
////        });
//
//        listrefrence = WidgetHelper.showFormDialog(listrefrence,view,activity);
//    }

//    public void showFormDialog(){
//        popUpForm = true;
//        new LongOperation().execute();
//    }

//    private void setupFormDialog() {
//        View view = LayoutInflater.from(activity).inflate(R.layout.tph_new_input, null);
//
//
//
//        alertDialog2 = WidgetHelper.showFormDialog(alertDialog2,view,activity);
//    }

    private boolean savePointTph(){
//        HashSet<String> sFoto = new HashSet<>();
//        if(ALselectedImage.size() > 0){
//            for(File file : ALselectedImage){
//                sFoto.add(file.getAbsolutePath());
//            }
//        }
//
//        String estCode = "";
//        for(Estate estate : GlobalHelper.getUser().getEstates()){
//            if(estate.getEstName().equalsIgnoreCase(etEst.getText().toString())){
//                estCode = estate.getEstCode();
//            }
//        }
//
////                HashSet<String> ancak = new HashSet<>();
////                if(cvAncak.getChips().size() > 0 ){
////                    for(String s :cvAncak.getChips()){
////                        ancak.add(s);
////                    }
////                }
//        String idTph;
//        boolean newData = false;
//        if(selectedTph == null){
//            newData = true;
//            idTph = GlobalHelper.getUser().getUserID()
//                    +"_"+String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_TPH));
//        }else{
//            idTph = selectedTph.getNoTph();
//        }
//
//        TPH tph = new TPH(
//                idTph,
//                etNamaTph.getText().toString(),
//                etBlock.getText().toString(),
//                etAfd.getText().toString(),
//                estCode,
//                etAncak.getText().toString(),
//                longitude,
//                latitude,
//                Integer.parseInt(GlobalHelper.getUser().getUserID()),
//                System.currentTimeMillis(),
//                Integer.parseInt(GlobalHelper.getUser().getUserID()),
//                System.currentTimeMillis(),
//                TPH.STATUS_TPH_BELUM_UPLOAD,
//                sFoto
//        );
//
//        if(selectedTph != null){
//            tph.setUpdateBy(Integer.parseInt(GlobalHelper.getUser().getUserID()));
//            tph.setUpdateDate(System.currentTimeMillis());
//        }

        if(saveTPH(selectedTph,newData)){
            if(mapActivity != null) {
                mapActivity.removeResurvey(new TPHnLangsiran(selectedTph.getNoTph(),selectedTph));
                mapActivity.addGraphicTph(new TPHnLangsiran(selectedTph.getNoTph(),selectedTph));
            }else{
                addGraphicTph(selectedTph);
            }
            return true;
        }else{
            return false;
        }
    }

    public boolean saveTPH(TPH tph, boolean insert){
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(tph.getNoTph(),
                gson.toJson(tph),
                tph.getBlock(),
                tph.getAfdeling(),
                String.valueOf(tph.getCreateBy()));
        if(insert) {
            repository.insert(dataNitrit);
        }else{
            repository.update(dataNitrit);
        }


        db.close();
        return true;
    }


    public void yesnoNonAktifTph(TPH selectTph,boolean hapus){
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.yes_no_radiogroup, null);
        TextView tvDialogKet = (TextView) dialogView.findViewById(R.id.ket1);
        TextView tvWarningText = (TextView) dialogView.findViewById(R.id.tvWarningText);
        RadioGroup radioGroup = (RadioGroup) dialogView.findViewById(R.id.radio_group);
        SlideView btnCancelDialog = (SlideView) dialogView.findViewById(R.id.btnCancel);
        SlideView btnOkDialog = (SlideView) dialogView.findViewById(R.id.btnOK);

        final AlertDialog dialog = new AlertDialog.Builder(activity, R.style.MyAlertDialogStyle)
                .setView(dialogView)
                .setCancelable(false)
                .create();

        tvWarningText.setText("*TPH akan benar terupdate setelah sync sukses");
        tvDialogKet.setText("Pilih Alasan Tph "+ selectTph.getNamaTph());
        btnOkDialog.setText("Yes");
        btnCancelDialog.setText("No");

        hashMapReasonUnharvestTPHRadioButton = new HashMap<>();
        hashMapReasonUnharvestTPH = listReasonUnharvestTPH();
        reasonUnharvestReturn = null;
        ruSelected = null;

        if(hashMapReasonUnharvestTPH.size() > 0){
            String key = "Delete";
            if(!hapus){
                key = "TPH masih digunakan";
            }
            tvDialogKet.setText("Pilih Alasan "+key+" Untuk Tph "+ selectTph.getNamaTph());
            reasonUnharvestReturn = setAdapterReasonUnharvest(key,hashMapReasonUnharvestTPH,activity,selectTph);
            if(reasonUnharvestReturn.getListReasonUnharvestTPH().size() > 0) {
                String conditionName = "";
                for(int i = 0 ; i < reasonUnharvestReturn.getListReasonUnharvestTPH().size(); i++){
                    ReasonUnharvestTPH reasonUnharvestTPH = reasonUnharvestReturn.getListReasonUnharvestTPH().get(i);
                    RadioButton radioButton = new RadioButton(activity);
                    radioButton.setId(View.generateViewId());
                    radioButton.setText(reasonUnharvestTPH.getReasonName());
                    if(reasonUnharvestReturn.getReasonUnharvestTPH() != null){
                        if(reasonUnharvestTPH.getActionReasonId() == reasonUnharvestReturn.getReasonUnharvestTPH().getActionReasonId()){
                            radioButton.setChecked(true);
                            ruSelected = reasonUnharvestReturn.getReasonUnharvestTPH();
                        }
                    }
                    hashMapReasonUnharvestTPHRadioButton.put(radioButton.getId(),reasonUnharvestTPH);
                    if(!conditionName.equals(reasonUnharvestTPH.getConditionName())){
                        TextView textView = new TextView(activity);
                        textView.setId(View.generateViewId());
                        textView.setText(reasonUnharvestTPH.getConditionName());
                        conditionName = reasonUnharvestTPH.getConditionName();
                        radioGroup.addView(textView);
                    }
                    radioGroup.addView(radioButton);
                }
            }
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                ruSelected = hashMapReasonUnharvestTPHRadioButton.get(checkedId);
                Toast.makeText(activity,ruSelected.getReasonName(),Toast.LENGTH_SHORT).show();
            }
        });

        btnCancelDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                dialog.dismiss();
            }
        });
        btnOkDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                if(reasonUnharvestReturn == null){
                    Toast.makeText(activity,"action dan alasan tidak muncul mohon sync ulang",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(reasonUnharvestReturn.getListReasonUnharvestTPH().size() == 0) {
                    Toast.makeText(activity,"alasan tidak muncul mohon sync ulang",Toast.LENGTH_SHORT).show();
                    return;
                }

                if (ruSelected == null) {
                    Toast.makeText(activity,"Mohon Pilih Alasan Hapus Tph",Toast.LENGTH_SHORT).show();
                }else if(ruSelected.getActionReasonId() == 0){
                    Toast.makeText(activity,"Mohon Tentukan Alasan Hapus Tph",Toast.LENGTH_SHORT).show();
                }else {
                    selectTph.setUpdateDate(System.currentTimeMillis());
                    selectTph.setUpdateBy(Integer.parseInt(GlobalHelper.getUser().getUserID()));
                    if(ruSelected.getActionName().toLowerCase().equals("delete")){
                        selectTph.setStatus(TPH.STATUS_TPH_DELETED);
                    }
                    selectTph.setPanen3Bulan(1);
                    selectTph.setReasonUnharvestTPH(ruSelected);
                    if (saveTPH(selectTph, false)) {
                        Toast.makeText(activity,ruSelected.getNotification(),Toast.LENGTH_LONG).show();
                        //jika berhasil langsung update variabel yang ada di map main
                        int idgrap = ((MapActivity) activity).getIdGrapSelected(new TPHnLangsiran(selectTph.getNoTph(), selectTph));

                        ((MapActivity) activity).graphicsLayerTPH3Bulan.removeGraphic(idgrap);
                        ((MapActivity) activity).countTph3Bulan --;
                        ((MapActivity) activity).countHariIni ++;
                        ((MapActivity) activity).updateCounterKet();
                        dialog.dismiss();
                        ((MapActivity) activity).callout.hide();
                    }
                }
            }
        });
        dialog.show();
    }

    public boolean cekValidation(){
        HashSet<String> sFoto = new HashSet<>();
        if(ALselectedImage.size() > 0){
            for(File file : ALselectedImage){
                sFoto.add(file.getAbsolutePath());
            }
        }

        String estCode = "";
        for(Estate estate : GlobalHelper.getUser().getEstates()){
            if(estate.getEstName().equalsIgnoreCase(etEst.getText().toString())){
                estCode = estate.getEstCode();
            }
        }

//                HashSet<String> ancak = new HashSet<>();
//                if(cvAncak.getChips().size() > 0 ){
//                    for(String s :cvAncak.getChips()){
//                        ancak.add(s);
//                    }
//                }
        String idTph;
        newData = false;
        if(selectedTph == null){
            newData = true;
            SimpleDateFormat sdfD = new SimpleDateFormat("ddMMyy");
            idTph = "T"+String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_TPH))
                    + sdfD.format(System.currentTimeMillis())
                    +GlobalHelper.getUser().getUserID();
        }else{
            idTph = selectedTph.getNoTph();
        }

        TPH tph = new TPH(
                idTph,
                etNamaTph.getText().toString(),
                etBlock.getText().toString(),
                etAfd.getText().toString(),
                estCode,
                etNamaTph.getText().toString(),
//                etAncak.getText().toString(),
                longitude,
                latitude,
                Integer.parseInt(GlobalHelper.getUser().getUserID()),
                System.currentTimeMillis(),
                Integer.parseInt(GlobalHelper.getUser().getUserID()),
                System.currentTimeMillis(),
                TPH.STATUS_TPH_BELUM_UPLOAD,
                sFoto,
                0
        );

        if(selectedTph != null){
            tph.setUpdateBy(Integer.parseInt(GlobalHelper.getUser().getUserID()));
            tph.setUpdateDate(System.currentTimeMillis());
        }

        if(tph.getNamaTph().isEmpty()){
            Toast.makeText(HarvestApp.getContext(),HarvestApp.getContext().getResources().getString(R.string.please_input_tph_name),Toast.LENGTH_SHORT).show();
            return false;
        }else if (tph.getEstCode().isEmpty()){
            Toast.makeText(HarvestApp.getContext(),HarvestApp.getContext().getResources().getString(R.string.please_chose_estate),Toast.LENGTH_SHORT).show();
            return false;
        }else if (tph.getFoto().size() == 0){
            Toast.makeText(HarvestApp.getContext(),HarvestApp.getContext().getResources().getString(R.string.please_take_picture),Toast.LENGTH_SHORT).show();
            return false;
        }else if (tph.getNamaTph().length() >= 21){
            Toast.makeText(HarvestApp.getContext(),"Nama TPH Terlalu Panjang",Toast.LENGTH_SHORT).show();
            return false;
        }else if (tph.getBlock().length() >= 11){
            Toast.makeText(HarvestApp.getContext(),"Blok TPH Terlalu Panjang",Toast.LENGTH_SHORT).show();
            return false;
        }else if (tph.getAfdeling().length() >= 11){
            Toast.makeText(HarvestApp.getContext(),"Afdeling TPH Terlalu Panjang",Toast.LENGTH_SHORT).show();
            return false;
        }

        selectedTph = tph;
        return true;
    }

//    public void setValueForm(TPH tph){
//        selectedTph = tph;
//        showFormDialog();
//    }

    public void showValueForm(TPH tph){
        Gson gson = new Gson();
        Log.d(this.getClass()+" "+ "showValueForm", gson.toJson(tph));

        //unutk tampilanpeta sudah di handle di map activity
        if(mapActivity == null) {
            graphicsLayer.removeAll();
            switchCrossHairUI(false);
            zoomToLocation(latitude, longitude, spatialReference);
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
        }

        String est = "";
        for(Estate estate : GlobalHelper.getUser().getEstates()){
            if(estate.getEstCode().equalsIgnoreCase(tph.getEstCode())){
                est = estate.getEstName();
            }
        }

//        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        etNamaTph.setText(tph.getNamaTph());
        etEst.setText(est);
        etAfd.setText(tph.getAfdeling());
        etAncak.setText(tph.getAncak());
        etBlock.setText(tph.getBlock());

//nanti muncul
//        etEst.setFocusable(false);
//        etAfd.setFocusable(false);
//        etBlock.setFocusable(false);

        longitude = tph.getLongitude();
        latitude = tph.getLatitude();

        ALselectedImage.clear();
        if(tph.getFoto() != null) {
            if (tph.getFoto().size() > 0) {
                for (String s : tph.getFoto()) {
                    File file = new File(s);
                    ALselectedImage.add(file);
                }
            }
        }
        setupimageslide();

        double radius = GlobalHelper.distance(latitude,latawal,longitude,longawal);
        if(radius > GlobalHelper.RADIUS){
            btnSave.setVisibility(View.GONE);
        }else{
            btnSave.setVisibility(View.VISIBLE);
        }
    }

    private void addGraphicTph(TPH tph){
        Point fromPoint = new Point(tph.getLongitude(), tph.getLatitude());
        Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

        CompositeSymbol cms = new CompositeSymbol();
        SimpleMarkerSymbol sms = new SimpleMarkerSymbol(activity.getResources().getColor(R.color.Blue), Utils.convertDpToPx(activity, GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.CIRCLE);
        SimpleLineSymbol sls = new SimpleLineSymbol(activity.getResources().getColor(R.color.Blue), GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
        sms.setOutline(sls);
        cms.add(sms);
        Graphic pointGraphic = new Graphic(toPoint, cms);
        int idgrap = graphicsLayerTPH.addGraphic(pointGraphic);
        hashpoint.put(idgrap,tph);
    }

//    private void defineTapSetup(float x,float y){
//        List<TPH> tphList = new ArrayList<>();
//        int[] graphicIDs = graphicsLayerTPH.getGraphicIDs(x,y,Utils.convertDpToPx(activity,GlobalHelper.MARKER_TOUCH_SIZE));
//
//        if(graphicIDs.length > 0) {
//            for(int gid : graphicIDs) {
//                for (HashMap.Entry<Integer, TPH> entry : hashpoint.entrySet()) {
//                    if(gid == entry.getKey()){
//                        tphList.add(entry.getValue());
//                    }
//                }
//            }
//        }
//
//        calloutSetup();
//        if (callout.isShowing()) {
//            callout.hide();
//        } else {
//            if (graphicIDs.length == 1) {
//                //jika dalam satu tap hanya ada satu titik maka kesini
//                int idx = -1;
//                String graptype = "";
//                Graphic graphic = graphicsLayerTPH.getGraphic(graphicIDs[0]);
//                if (graphic.getGeometry() instanceof Point) {
//                    graptype = "point";
//                    idx = graphicIDs[0];
//                }
//                showInfoWindow(idx,tphList.get(0));
//            }else if (graphicIDs.length > 1){
//                showChoseInfoWindow(graphicIDs, graphicsLayerTPH, tphList);
//            }
//        }
//    }

    private void showChoseInfoWindow(int[] graphicIDs,GraphicsLayer gl,List<TPH> tphList){
        ArrayList<TPH> windowChoseArrayList = new ArrayList<>();

        for (int i = 0; i < graphicIDs.length; i++) {
            Graphic graphic = gl.getGraphic(graphicIDs[i]);
            int idx =0;
            if (graphic.getGeometry() instanceof Point) {
                idx = graphicIDs[i];
            }

            if(idx != 0){
                for (HashMap.Entry<Integer, TPH> entry : hashpoint.entrySet()) {
                    if(idx == entry.getKey()){
                        windowChoseArrayList.add(entry.getValue());
                    }
                }
            }
        }

        Collections.sort(windowChoseArrayList, new Comparator<TPH>() {
            @Override
            public int compare(TPH o1, TPH o2) {
                return o1.getNamaTph().compareTo(o2.getNamaTph());
            }
        });

        if(windowChoseArrayList.size()>1){
            ViewGroup content = InfoWindowChose.setupInfoWindowsChose(activity, windowChoseArrayList);
            callout.setContent(content);
            Graphic graphic = gl.getGraphic(graphicIDs[graphicIDs.length -1]);
            String graptype = "";
            int idx =0;
            if (graphic.getGeometry() instanceof Point) {
                graptype = "point";
                idx = graphicIDs[graphicIDs.length -1];
            }

            Point realpoin = (Point) gl.getGraphic(idx).getGeometry();

            Point wgs84poin = (Point) GeometryEngine.project(realpoin,
                    gl.getSpatialReference(),
                    SpatialReference.create(SpatialReference.WKID_WGS84));
            Point mapPoint = (Point) GeometryEngine.project(wgs84poin,
                    SpatialReference.create(SpatialReference.WKID_WGS84),
                    map.getSpatialReference());
            callout.setCoordinates(mapPoint);
            map.centerAt(mapPoint,true);
            callout.animatedShow(mapPoint,content);
        }else if (windowChoseArrayList.size() == 1){
            showInfoWindow(graphicIDs[0],windowChoseArrayList.get(0));
        }
    }


    private void calloutSetup() {
        this.callout = map.getCallout();
        this.callout.setStyle(R.xml.statisticspop);
        if (this.callout.isShowing()) {
            this.callout.hide();
        }
    }

    public void showInfoWindow(int idx,TPH tph){
        ViewGroup content = InfoWindow.setupInfoWindows(activity, tph);
        callout.setContent(content);
        Point realpoin = (Point) graphicsLayerTPH.getGraphic(idx).getGeometry();

        if(realpoin!= null) {
            Point wgs84poin = (Point) GeometryEngine.project(realpoin,
                    graphicsLayerTPH.getSpatialReference(),
                    SpatialReference.create(SpatialReference.WKID_WGS84));
            Point mapPoint = (Point) GeometryEngine.project(wgs84poin,
                    SpatialReference.create(SpatialReference.WKID_WGS84),
                    map.getSpatialReference());
            callout.setCoordinates(mapPoint);
            map.centerAt(mapPoint,true);
            callout.animatedShow(mapPoint,content);
        }
    }

    public void setImages(File images,Context context){
//        Glide.with(context)
//                .load(images)
//                .asBitmap()
//                .error(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_camera_alt, null))
//                .centerCrop();
        ALselectedImage.add(images);
        setupimageslide();
    }

    private void setupimageslide(){
        Set<File> sf = new HashSet<>();
        sf.addAll(ALselectedImage);
        ALselectedImage.clear();
        ALselectedImage.addAll(sf);

        if(ALselectedImage.size() > 3){
            ivPhoto.setVisibility(View.GONE);
        }else{
            ivPhoto.setVisibility(View.VISIBLE);
        }
        if(ALselectedImage.size() != 0){
            imagecacnel.setVisibility(View.VISIBLE);
            imagesview.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.VISIBLE);
        }else{
            imagecacnel.setVisibility(View.GONE);
            imagesview.setVisibility(View.GONE);
            sliderLayout.setVisibility(View.GONE);
        }
        sliderLayout.removeAllSliders();
        for(File file : ALselectedImage){
            TextSliderView textSliderView = new TextSliderView(activity);
            // initialize a SliderLayout
            if(file.toString().startsWith("http:")){
                String surl = file.toString();
                if(file.toString().startsWith("http://")){

                }else if(file.toString().startsWith("http:/")){
                    surl =surl.replace("http:/","http://");
                }

                textSliderView
                        .image(GlobalHelper.setUrlFoto(surl))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }else {
                textSliderView
                        .image(file)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        imagecacnel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
                try {
                    if (sliderLayout.getCurrentPosition() == 0 && ALselectedImage.size() == 1) {
                        ALselectedImage.clear();
                        sliderLayout.removeAllSliders();
                        sliderLayout.setVisibility(View.GONE);
                        imagecacnel.setVisibility(View.GONE);
                        imagesview.setVisibility(View.GONE);
                    } else {
                        ALselectedImage.remove(sliderLayout.getCurrentPosition());
                        sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
                    }
                }catch (Exception e){
                    Log.e("sliderLayout", e.toString());
                }
            }
        });
        imagesview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WidgetHelper.showImagesZoom(activity,ALselectedImage);
            }
        });
    }

    private ArrayList<String> getListEstate(User user, Estate estate){
        ArrayList<String> estateArrayList = new ArrayList<>();
        for(Estate estate1:user.getEstates()) {
            if(estate1.getCompanyShortName().equalsIgnoreCase(estate.getCompanyShortName())){
                estateArrayList.add(estate1.getEstName());
            }
        }
        return estateArrayList;
    }

    private void mapSetup(){
        //
        map.removeAll();
        graphicsLayer.removeAll();
        String locTiledMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_VKM);
        tiledLayer = new ArcGISLocalTiledLayer(locTiledMap,true);
        tiledLayer.setRenderNativeResolution(true);
        tiledLayer.setMinScale(70000d);
        tiledLayer.setMaxScale(1000d);
        map.addLayer(tiledLayer);

        locKmlMap = GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK);

        kmlLayer = new KmlLayer(locKmlMap);
        kmlLayer.setOpacity(0.3f);
        map.addLayer(kmlLayer);

        map.setOnStatusChangedListener(new OnStatusChangedListener() {
            @Override
            public void onStatusChanged(Object o, STATUS status) {
                if(o==map && status==STATUS.INITIALIZED){
                    spatialReference = map.getSpatialReference();
                    isMapLoaded = true;
                    fabZoomToEstate.callOnClick();
                }
            }
        });
        map.setOnPanListener(new OnPanListener() {
            @Override
            public void prePointerMove(float v, float v1, float v2, float v3) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Point prePoint = map.getCenter();
                        Point point = (Point) GeometryEngine.project(prePoint,map.getSpatialReference(), SpatialReference.create(SpatialReference.WKID_WGS84));
                    }
                }
            }

            @Override
            public void postPointerMove(float v, float v1, float v2, float v3) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Point prePoint = map.getCenter();
                        Point point = (Point) GeometryEngine.project(prePoint,map.getSpatialReference(),SpatialReference.create(SpatialReference.WKID_WGS84));
                        double imeter = GlobalHelper.distance(latawal,point.getY(),longawal,point.getX());
                        double bearing = GlobalHelper.bearing(latawal,point.getY(),longawal,point.getX());
                        tv_lat_manual.setText(String.format("%.5f",point.getY()));
                        tv_lon_manual.setText(String.format("%.5f",point.getX()));
                        tv_dis_manual.setText(String.format("%.0f m",imeter));
                        tv_deg_manual.setText(String.format("%.0f",bearing));
                        if(point.getY() != 0.0 && point.getX() != 0.0) {
                            if (imeter <= GlobalHelper.RADIUS) {
                                latitude = point.getY();
                                longitude = point.getX();
                                ll_latlon.setBackgroundColor(Color.WHITE);
                            } else {
                                ll_latlon.setBackgroundColor(Color.RED);
                            }
                        }
                    }

                }
            }

            @Override
            public void prePointerUp(float v, float v1, float v2, float v3) {

            }

            @Override
            public void postPointerUp(float v, float v1, float v2, float v3) {

            }
        });
        map.setOnZoomListener(new OnZoomListener() {
            @Override
            public void preAction(float v, float v1, double v2) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Log.e("zoom preAction",v+ " ; "+ v1+" ; "+v2);
                        Log.e("zoom scale",String.format("%.0f",map.getScale()));
                        int scale = (int) map.getScale();
                        int zoomlvl = GlobalHelper.getZoomlvl(map.getScale());
                        Log.v("zoom lvl",String.format(" 1 : %,dm Zl : %,d",scale,zoomlvl));
                        map.getSpatialReference();
                    }

                }
            }

            @Override
            public void postAction(float v, float v1, double v2) {
                if(map.isLoaded()){
                    if(map.getSpatialReference()!=null){
                        Log.e("zoom postAction",v+ " ; "+ v1+" ; "+v2);
                        int scale = (int) map.getScale();
                        int zoomlvl = GlobalHelper.getZoomlvl(map.getScale());
                        Log.v("zoom lvl",String.format(" 1 : %,dm Zl : %,d",scale,zoomlvl));
                    }

                }
            }
        });

        graphicsLayer.setMinScale(70000d);
        graphicsLayer.setMaxScale(1000d);
        graphicsLayerTPH.setMinScale(70000d);
        graphicsLayerTPH.setMaxScale(1000d);
        map.addLayer(graphicsLayer);
        map.addLayer(graphicsLayerTPH);
        map.invalidate();
    }

    public void pointTPHSetup(String Estate,String Block){
        graphicsLayerTPH.removeAll();
        hashpoint = new HashMap<>();

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> tphIterable = repository.find(ObjectFilters.eq("param1", Block));
        for (Iterator iterator = tphIterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            TPH tph = gson.fromJson(dataNitrit.getValueDataNitrit(),TPH.class);
//            double radius = GlobalHelper.distance(latawal,tph.getLatitude(),
//                    longawal,tph.getLongitude());
            if ((tph.getStatus() != TPH.STATUS_TPH_DELETED && tph.getStatus() != TPH.STATUS_TPH_INACTIVE_QC_MOBILE)
                    && (tph.getCreateBy() != TPH.USER_GIS_SYSTEM)) {
                addGraphicTph(tph);
            }
        }

//        ObjectRepository<TPH> repository = db.getRepository(TPH.class);
//        Iterable<TPH> tphIterable = repository.find().project(TPH.class);
//        for (Iterator iterator = tphIterable.iterator(); iterator.hasNext();) {
//            TPH tph = (TPH) iterator.next();
//
//            if (tph.getStatus() != TPH.STATUS_TPH_DELETED) {
//                addGraphicTph(tph);
//            }
//        }

//        NitriteCollection collection = db.getCollection(Encrypts.encrypt(GlobalHelper.TABEL_TPH));
//        for(Document document : collection.find()){
//            Gson gson = new Gson();
//            TPH tph = gson.fromJson(document.get("valueDataNitrit").toString(),TPH.class);
//            if(tph.getStatus() != TPH.STATUS_TPH_DELETED) {
//                addGraphicTph(tph);
//            }
//        }

        db.close();
//        WaspHash tblTPH = GlobalHelper.getTableHash(GlobalHelper.TABEL_TPH);
//        Gson gson = new Gson();
//        for (int i = 0 ; i < tblTPH.getAllValues().size();i++){
//            TPH tph = gson.fromJson(tblTPH.getAllValues().get(i).toString(),TPH.class);
//            if(tph.getStatus() != TPH.STATUS_TPH_DELETED) {
//                addGraphicTph(tph);
//            }
//        }
    }

    private int addGrapTemp(Double lat, Double lon, int idlama){
        if(map!=null && map.isLoaded()){
            Point fromPoint = new Point(lon, lat);
            Point toPoint = (Point) GeometryEngine.project(fromPoint, SpatialReference.create(SpatialReference.WKID_WGS84), spatialReference);

            CompositeSymbol cms = new CompositeSymbol();
            SimpleMarkerSymbol sms = new SimpleMarkerSymbol(Color.GREEN, Utils.convertDpToPx(activity, GlobalHelper.MARKER_SIZE), SimpleMarkerSymbol.STYLE.X);
            SimpleLineSymbol sls = new SimpleLineSymbol(Color.GREEN, GlobalHelper.GARISTEPI_SIZE, SimpleLineSymbol.STYLE.SOLID);
            sms.setOutline(sls);
            cms.add(sms);
            Graphic pointGraphic = new Graphic(toPoint, cms);
            setupBlokAfdeling(lat,lon);

            if(idlama!= 0){
                removeGrapTemp(idlama);
            }

            return graphicsLayer.addGraphic(pointGraphic);
        }else{
            return 0;
        }
    }

    private void removeGrapTemp(int id){
        graphicsLayer.removeGraphic(id);
    }

    public void setupBlokAfdeling(Double latitude,Double longitude){
        MapActivity.LogIntervalStep("addGrapTemp 1 start");
        Set<String> allBlockAfdeling = GlobalHelper.getNearBlock(new File(locKmlMap),latitude,longitude);
        MapActivity.LogIntervalStep("addGrapTemp 1 getNearBlock");

        Set<String> allBlock = new ArraySet<>();
        Set<String> allAfdeling = new ArraySet<>();

        MapActivity.LogIntervalStep("addGrapTemp 1 getDataBlockAfd");
        for(String s : allBlockAfdeling){
            try {
                String [] split = s.split(";");
                allAfdeling.add(split[1]);
                allBlock.add(split[2]);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        MapActivity.LogIntervalStep("addGrapTemp 1 setUpBlockAfd");
        ArrayAdapter<String> adapterBlock = new ArrayAdapter<String>
                (activity,android.R.layout.select_dialog_item, allBlock.toArray(new String[allBlock.size()]));
        etBlock.setThreshold(1);
        etBlock.setAdapter(adapterBlock);

        ArrayAdapter<String> adapterAfdeling = new ArrayAdapter<String>
                (activity,android.R.layout.select_dialog_item, allAfdeling.toArray(new String[allAfdeling.size()]));
        adapterAfdeling.notifyDataSetChanged();
        etAfd.setThreshold(1);
        etAfd.setAdapter(adapterAfdeling);

        MapActivity.LogIntervalStep("addGrapTemp 1 setUpBlockAfdFinish");
        try {

            MapActivity.LogIntervalStep("addGrapTemp 1 setValue");
            String [] blockAfdeling = GlobalHelper.getAndAssignBlock(new File(locKmlMap),latitude,longitude).split(";");
            if(selectedTph == null){
                etBlock.setText(blockAfdeling[2]);
                etAfd.setText(blockAfdeling[1]);
            }

            MapActivity.LogIntervalStep("addGrapTemp 1 finish Value");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setDefaultManual(){

        if(selectedTph != null){
            longitude = longawal;
            latitude = latawal;
            selectedTph = null;

            ALselectedImage.clear();
            setupimageslide();
        }

        Point mapPoint = GeometryEngine.project(longitude,latitude,spatialReference);
        map.centerAt(mapPoint,true);
        tv_lat_manual.setText(String.format("%.5f",longitude));
        tv_lon_manual.setText(String.format("%.5f",latitude));
        tv_deg_manual.setText("0");
        tv_dis_manual.setText("0 m");
        ll_latlon.setBackgroundColor(Color.WHITE);
        zoomToLocation(latitude,longitude,spatialReference);
        GlobalHelper.hideKeyboard(activity);

        switchCrossHairUI(true);
//        btnSave.setVisibility(View.VISIBLE);
    }

    private void switchCrossHairUI(boolean visible){
        if(visible){
            ivCrosshair.setVisibility(View.VISIBLE);
            ll_latlon.setVisibility(View.VISIBLE);

            RelativeLayout.LayoutParams params= (RelativeLayout.LayoutParams) ll_latlon.getLayoutParams();
            params.addRule(RelativeLayout.BELOW, R.id.iv_crosshair);
            //ll_latlon.setLayoutParams(params);
        }else{
            ivCrosshair.setVisibility(View.GONE);
            ll_latlon.setVisibility(View.GONE);
        }
    }

    private void zoomToLocation(double lat, double lon, SpatialReference spatialReference) {
        Point mapPoint = GeometryEngine.project(lon,lat,spatialReference);
        map.centerAt(mapPoint,true);
        map.zoomTo(mapPoint,15000f);
    }

    public static String getAfdeling(String Estate,String Block){
        String afdeling = "";
        ArrayList<TPH> tphArrayList = new ArrayList<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_TPH);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> tphIterable = repository.find(ObjectFilters.eq("param1", Block));
        for (Iterator iterator = tphIterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            TPH tph = gson.fromJson(dataNitrit.getValueDataNitrit(),TPH.class);
//            double radius = GlobalHelper.distance(latawal,tph.getLatitude(),
//                    longawal,tph.getLongitude());
            if ((tph.getStatus() != TPH.STATUS_TPH_DELETED && tph.getStatus() != TPH.STATUS_TPH_INACTIVE_QC_MOBILE)
                    && (tph.getCreateBy() != TPH.USER_GIS_SYSTEM) &&
                    (tph.getBlock().equals(Block)) && (tph.getEstCode().equals(Estate))) {
                if(tph.getAfdeling() != null){
                    afdeling = tph.getAfdeling();
                    break;
                }
            }
        }
        db.close();
        return afdeling;
    }

    public static HashMap<String,ArrayList<ReasonUnharvestTPH>> listReasonUnharvestTPH(){
        HashMap<String,ArrayList<ReasonUnharvestTPH>> hashMap = new HashMap<>();
        ArrayList<ReasonUnharvestTPH> reasonUnharvestTPHS = new ArrayList<>();
//        reasonUnharvestTPHS.add(new ReasonUnharvestTPH(0,"Pilih Action","Pilih Reason",0,""));

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_REASON_UNHARVEST);
        if(db != null) {
            ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
            Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
            for (Iterator iterator = Iterable.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                ReasonUnharvestTPH reasonUnharvestTPH = gson.fromJson(dataNitrit.getValueDataNitrit(), ReasonUnharvestTPH.class);
                if(reasonUnharvestTPH != null){
                    reasonUnharvestTPHS.add(reasonUnharvestTPH);
                }
            }
            db.close();
        }
//        reasonUnharvestTPHS.add(new ReasonUnharvestTPH(1,"Delete","TPH tidak di gunakan / Salah pemetaan",1,"Delete"));
//        reasonUnharvestTPHS.add(new ReasonUnharvestTPH(2,"Delete","Terdapat TPH lain yang lebih sesuai dengan kondisi lapangan",1,"Delete"));
//        reasonUnharvestTPHS.add(new ReasonUnharvestTPH(3,"TPH masih digunakan","Akses ke TPH rusak",2,"Perbaikan"));
//        reasonUnharvestTPHS.add(new ReasonUnharvestTPH(4,"TPH masih digunakan","Kondisi TPH tidak standar",2,"Perbaikan"));
//        reasonUnharvestTPHS.add(new ReasonUnharvestTPH(5,"TPH masih digunakan","Dipanen warga / pencurian",2,"Perbaikan"));
//        reasonUnharvestTPHS.add(new ReasonUnharvestTPH(6,"TPH masih digunakan","Masalah sosial",3,"Konsolidasi"));
//        reasonUnharvestTPHS.add(new ReasonUnharvestTPH(7,"TPH masih digunakan","Kondisi tanaman tidak homogen",3,"Konsolidasi"));
//        reasonUnharvestTPHS.add(new ReasonUnharvestTPH(8,"TPH masih digunakan","Banjir",3,"Konsolidasi"));

        for(int i = 0 ; i < reasonUnharvestTPHS.size();i++){
            ReasonUnharvestTPH reasonUnharvestTPH = reasonUnharvestTPHS.get(i);
            ArrayList<ReasonUnharvestTPH> arrayList = hashMap.get(reasonUnharvestTPH.getActionName());
            if(arrayList == null){
                arrayList = new ArrayList<>();
            }
            arrayList.add(reasonUnharvestTPH);
            hashMap.put(reasonUnharvestTPH.getActionName(),arrayList);
        }

        return hashMap;
    }

    public static ReasonUnharvestReturn setAdapterReasonUnharvest(String parma, HashMap<String,ArrayList<ReasonUnharvestTPH>> hashMapReasonUnharvestTPH, FragmentActivity activity,TPH tph){
        ArrayList<ReasonUnharvestTPH> strings2 = new ArrayList<>();
        ReasonUnharvestTPH reasonUnharvestTPHSelected = null;
        for(int i = 0 ; i < hashMapReasonUnharvestTPH.get(parma).size(); i++){
            ReasonUnharvestTPH reasonUnharvestTPH = hashMapReasonUnharvestTPH.get(parma).get(i);
            strings2.add(reasonUnharvestTPH);
            if(tph.getReasonUnharvestTPH() != null){
                if(tph.getReasonUnharvestTPH().getConditionId() == reasonUnharvestTPH.getConditionId()){
                    reasonUnharvestTPHSelected = reasonUnharvestTPH;
                }
            }
        }
        ReasonUnharvestReturn reasonUnharvestReturn = new ReasonUnharvestReturn(reasonUnharvestTPHSelected,strings2);
        return reasonUnharvestReturn;
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        private AlertDialog alertDialogAllpoin ;
        private View view;
        private Boolean validasi;
//
//        public LongOperation(View v) {
//            view = v;
//        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(activity, activity.getResources().getString(R.string.wait));
            if (!popUpForm) {
                validasi = cekValidation();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            if(popUpForm){
                popUpForm = false;
                return "popup";
            }else {
                if(validasi) {
                    if (savePointTph()) {
//                pointTPHSetup(selectedTph.getEstCode(),selectedTph.getBlock());
                        return String.valueOf(1);
                    } else {
                        return String.valueOf(0);
                    }
                }else{
                    return String.valueOf(0);
                }
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result.equals("popup")){
                //setupFormDialog();
                if (alertDialogAllpoin != null) {
                    alertDialogAllpoin.cancel();
                }
            }else {
                if (alertDialogAllpoin != null) {
                    alertDialogAllpoin.cancel();
                }
                switch (Integer.parseInt(result)) {
                    case 0: {
                        Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.save_failed), Toast.LENGTH_SHORT).show();
                        break;
                    }
                    case 1: {
                        GlobalHelper.setCountNumberPlusOne(GlobalHelper.LIST_FOLDER_TPH);
                        Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.save_done), Toast.LENGTH_SHORT).show();
//                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        if(activity instanceof MapActivity){
                            int stat = 0;
                            if (isNewData) {
                                stat = MapActivity.Status_Close_Form_Save;
                            }else{
                                stat = MapActivity.Status_Close_Form_Update;
                            }
                            mapActivity.closeForm(stat);
                        }else {
                            Fragment fragment = (Fragment) activity.getSupportFragmentManager().findFragmentById(R.id.content_container);
                            if (fragment instanceof TphInputFragment) {
                                ((TphInputFragment) fragment).setTph(selectedTph);
                                ((TphInputFragment) fragment).tphHelper.listrefrence.dismiss();
                            }
                        }
                        break;
                    }
                }
            }
        }
    }
}
