package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;


/**
 * Created by user on 12/11/2017.
 */

public class ImageViewPagerPinchToZoomAdapter extends PagerAdapter {
    private final List<File> drawables;
    public ImageViewPagerPinchToZoomAdapter(List<File> drawables) {
        this.drawables = drawables;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Context context = container.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.page_image, null);
        container.addView(view);

        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        if(drawables.get(position).toString().startsWith("http:")){
            String surl = drawables.get(position).toString();
            if(drawables.get(position).toString().startsWith("http://")){

            }else if(drawables.get(position).toString().startsWith("http:/")){
                surl =surl.replace("http:/","http://");
            }
            try {

                Picasso.with(HarvestApp.getContext()).load(GlobalHelper.setUrlFoto(surl)).into(imageView);
//                URL urlConnection = new URL(surl);
//                HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
//                connection.setDoInput(true);
//                connection.connect();
//                InputStream input = connection.getInputStream();
//                myBitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            Bitmap myBitmap = BitmapFactory.decodeFile(drawables.get(position).getAbsolutePath());
            if(myBitmap != null) {
                imageView.setImageBitmap(myBitmap);

                ImageMatrixTouchHandler imageMatrixTouchHandler = new ImageMatrixTouchHandler(context);
                imageView.setOnTouchListener(imageMatrixTouchHandler);
            }else{
                Toast.makeText(HarvestApp.getContext(),"Gagal Load Foto",Toast.LENGTH_SHORT);
            }
        }

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;

        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        imageView.setImageResource(0);

        container.removeView(view);
    }

    @Override
    public int getCount() {
        return drawables.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition (Object object) {
        return POSITION_NONE;
    }
}

