package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;

import id.co.ams_plantation.harvestsap.Fragment.LangsiranInputFragment;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.ui.AngkutActivity;

public class AngkutInputAdapter extends RecyclerView.Adapter<AngkutInputAdapter.Holder>{
    Context context;
    ArrayList<Angkut> angkuts;
    ArrayList<Angkut> origin;
    HashSet<String> angkutHashSet;
    Boolean checkedAll;

    public AngkutInputAdapter(Context context, ArrayList<Angkut> angkuts,boolean checkedAll) {
        this.context = context;
        this.origin = angkuts;
        this.angkuts = angkuts;
        this.checkedAll = checkedAll;
        angkutHashSet = new HashSet<>();
    }

    @NonNull
    @Override
    public AngkutInputAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.angkut_detail_input, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AngkutInputAdapter.Holder holder, int position) {
        holder.setValue(angkuts.get(position));
    }

    @Override
    public int getItemCount() {
        return angkuts != null ? angkuts.size() : 0;
    }

    @Override
    public long getItemId(int position) {
//        return super.getItemId(position);
        return position;
    }

    @Override
    public int getItemViewType(int position) {
//        return super.getItemViewType(position);
        return position;
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                angkuts = new ArrayList<>();
                String string = constraint.toString().toLowerCase();
                if(origin.size() > 0) {
                    for (Angkut angkut : origin) {
                        if (angkut.getOph().toLowerCase().contains(string.toLowerCase()) ||
                                angkut.getBlock().toLowerCase().contains(string.toLowerCase()) ||
                                String.valueOf(angkut.getJanjang()).equals(string) ||
                                String.valueOf(angkut.getBrondolan()).equals(string) ||
                                String.valueOf(angkut.getBerat()).equals(string)) {
                            angkuts.add(angkut);
                        }
                    }
                }
                if (angkuts.size() > 0) {
                    results.count = angkuts.size();
                    results.values = angkuts;
                    return results;
                } else {
                    return null;
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null) {
                    angkuts = (ArrayList<Angkut>) results.values;
                } else {
                    angkuts.clear();
                }
                notifyDataSetChanged();

            }
        };
    }

    public class Holder extends RecyclerView.ViewHolder {

        LinearLayout llAngkut;
        CheckBox cbAngkut;
        TextView tvOph;
        TextView tvBlock;
        TextView tvJJg;
        TextView tvBrdl;
        TextView tvBerat;

        public Holder(View itemView) {
            super(itemView);
            llAngkut = itemView.findViewById(R.id.llAngkut);
            cbAngkut = itemView.findViewById(R.id.cbAngkut);
            tvOph = itemView.findViewById(R.id.tvOph);
            tvBlock = itemView.findViewById(R.id.tvBlock);
            tvJJg = itemView.findViewById(R.id.tvJJg);
            tvBrdl = itemView.findViewById(R.id.tvBrdl);
            tvBerat = itemView.findViewById(R.id.tvBerat);

            cbAngkut.setChecked(checkedAll);

            llAngkut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cbAngkut.setChecked(!cbAngkut.isChecked());
                }
            });
        }

        public void setValue(Angkut value){
            tvOph.setText(value.getOph());
            tvBlock.setText("Block : "+value.getBlock());
            tvJJg.setText(String.format("Janjang : %,d Jjg",value.getJanjang()));
            tvBrdl.setText(String.format("Brondolan : %,d Kg",value.getBrondolan()));
            tvBerat.setText(String.format("Berat : %.0f Kg",value.getBerat()));

            if(context instanceof AngkutActivity){
                if (((AngkutActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof LangsiranInputFragment) {
                    LangsiranInputFragment fragment = (LangsiranInputFragment) ((AngkutActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
                    if(!angkutHashSet.contains(value.getOph())){
                        angkutHashSet.add(value.getOph());

                        //cek apakah sudah diangkut base on selectedAngkut
                        cbAngkut.setChecked(fragment.cekSudahAngkut(value,checkedAll));
                        fragment.updateDataSelected(value,cbAngkut.isChecked());
                    }
                }
            }

            cbAngkut.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(context instanceof AngkutActivity){
                        if (((AngkutActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof LangsiranInputFragment) {
                            LangsiranInputFragment fragment = (LangsiranInputFragment) ((AngkutActivity) context).getSupportFragmentManager().findFragmentById(R.id.content_container);
                            fragment.updateDataSelected(value,cbAngkut.isChecked());
                        }
                    }
                }
            });
        }
    }
}
