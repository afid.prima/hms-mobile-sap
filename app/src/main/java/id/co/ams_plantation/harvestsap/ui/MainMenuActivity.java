package id.co.ams_plantation.harvestsap.ui;

import android.app.Activity;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;

import butterknife.BindView;
import cat.ereza.customactivityoncrash.util.ServerConnectionListener;
import devlight.io.library.ntb.NavigationTabBar;
import id.co.ams_plantation.harvestsap.BuildConfig;
import id.co.ams_plantation.harvestsap.Fragment.AngkutFragment;
import id.co.ams_plantation.harvestsap.Fragment.QcFragment;
import id.co.ams_plantation.harvestsap.Fragment.QcMutuAncakFragmet;
import id.co.ams_plantation.harvestsap.Fragment.QcMutuBuahFragment;
import id.co.ams_plantation.harvestsap.Fragment.QcSensusBjrFragment;
import id.co.ams_plantation.harvestsap.Fragment.RekonsilasiListFragment;
import id.co.ams_plantation.harvestsap.Fragment.SettingFragment;
import id.co.ams_plantation.harvestsap.Fragment.SpbFragment;
import id.co.ams_plantation.harvestsap.Fragment.TphFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.QcMutuBuah;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.presenter.ConnectionPresenter;
import id.co.ams_plantation.harvestsap.presenter.MainMenuPresenter;
import id.co.ams_plantation.harvestsap.service.BluetoothService;
import id.co.ams_plantation.harvestsap.util.ActivityLoggerHelper;
import id.co.ams_plantation.harvestsap.util.FilterHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.MutuAncakHelper;
import id.co.ams_plantation.harvestsap.util.NfcHelper;
import id.co.ams_plantation.harvestsap.util.QrHelper;
import id.co.ams_plantation.harvestsap.util.SharedPrefHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiSpbHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import id.co.ams_plantation.harvestsap.view.MainMenuView;

import static id.co.ams_plantation.harvestsap.util.NfcHelper.readFromIntent;

/**
 * Created by user on 11/21/2018.
 */

public class MainMenuActivity extends BaseActivity implements MainMenuView {

    @BindView(R.id.ntb_horizontal)
    NavigationTabBar navigationTabBar;
    @BindView(R.id.vp_horizontal_ntb)
    public ViewPager viewPager;
    @BindView(R.id.footerUser)
    TextView footerUser;
    @BindView(R.id.footerEstate)
    TextView footerEstate;
    public @BindView(R.id.btnFilter)
    LinearLayout btnFilter;
    public @BindView(R.id.tvFilterAfdeling)
    TextView tvFilterAfdeling;
    public @BindView(R.id.tvFilterDataInputBy)
    TextView tvFilterDataInputBy;
    public @BindView(R.id.ivLogo)
    ImageView ivLogo;
    @BindView(R.id.cvToken)
    CardView cvToken;
    @BindView(R.id.tvToken)
    TextView tvToken;

    MainMenuPresenter presenter;

    Activity activity;
//    QrHelper qrHelper;

    ArrayList<Fragment> arrayListFragment;

    public ConnectionPresenter connectionPresenter;
    public FilterHelper filterHelper;

/****************************************** NFC ******************************************************************/
    public Tag myTag;
    NfcAdapter nfcAdapter;
    PendingIntent pendingIntent;
    IntentFilter[] writeTagFilters;

    //tambahan crash
    ServerConnectionListener serverConnectionListener;
    String value;
    public Long timeScreen;
//    public MutuAncakHelper mutuAncakHelper;

    @Override
    protected void initPresenter() {
        presenter = new MainMenuPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);
        getSupportActionBar().hide();
        startTimeScreen = new Date();
        timeScreen = System.currentTimeMillis();
        Log.d("MainMenuActivity.java", "1. timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
        timeScreen = System.currentTimeMillis();

//        qrHelper = new QrHelper(this);
        filterHelper = new FilterHelper(this);
//        mutuAncakHelper = new MutuAncakHelper(this);

        Log.d("MainMenuActivity.java", "2. timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
        timeScreen = System.currentTimeMillis();

        activity = this;
        main();

        Log.d("MainMenuActivity.java", "3. timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
        timeScreen = System.currentTimeMillis();

        SharedPrefHelper.applySharepref(HarvestApp.TRANSAKSI_PANEN_PREF,null);
        SharedPrefHelper.applySharepref(HarvestApp.FOTO_PANEN_PREF,null);

        Log.d("MainMenuActivity.java", "4. timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
        timeScreen = System.currentTimeMillis();

//        if(cekAngkutOn()){
//            AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
//                    .setTitle("Perhatian")
//                    .setCancelable(false)
//                    .setMessage("Apakah Anda Ingin Melanjutkan Proses Angkut")
//                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            TransaksiAngkutHelper.hapusFileAngkut(true);
//                            dialog.dismiss();
//                        }
//                    })
//                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.dismiss();
//                            Intent intent = new Intent(MainMenuActivity.this, AngkutActivity.class);
//                            if(value != null) {
//                                Gson gson = new Gson();
//                                TransaksiAngkut parsingTAngkut = gson.fromJson(value,TransaksiAngkut.class);
//                                parsingTAngkut.setAngkuts(null);
//                                intent.putExtra("transaksiAngkut", gson.toJson(parsingTAngkut));
//                            }
//                            startActivityForResult(intent, GlobalHelper.RESULT_ANGKUTACTIVITY);
//                        }
//                    })
//                    .create();
//            alertDialog.show();
//        }else
        if (spbOn()){
            popupSPBOn();
        }

        Log.d("MainMenuActivity.java", "5. timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
        timeScreen = System.currentTimeMillis();

//        else if (qcAncakOn()){
//            AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
//                    .setTitle("Perhatian")
//                    .setMessage("Apakah Anda Inggin Melanjutkan Proses Qc Ancak")
//                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            mutuAncakHelper.hapusFileQcAncak(true);
//                            dialog.dismiss();
//                        }
//                    })
//                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.dismiss();
//                            Intent intent = new Intent(MainMenuActivity.this,QcMutuAncakActivity.class);
//                            if(value != null) {
//                                intent.putExtra("qcAncak", value);
//                            }
//                            startActivityForResult(intent, GlobalHelper.RESULT_QC_MUTU_ANCAK);
//                        }
//                    })
//                    .create();
//            alertDialog.show();
//        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Long exptDate = sdf.parse(GlobalHelper.getUser().getExpirationDate()).getTime();
            long sisaHari = exptDate - System.currentTimeMillis();
            sisaHari = sisaHari / (24 * 60 * 60 * 1000);
            if(sisaHari <= GlobalHelper.NOTIF_TOKEN_DAYS){
                tvToken.setText(sisaHari+" "+getResources().getString(R.string.days_left));
            }else{
                cvToken.setVisibility(View.GONE);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.d("MainMenuActivity.java", "6. timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
        timeScreen = System.currentTimeMillis();

        ivLogo.setVisibility(View.GONE);
        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterHelper.showFilterAfdeling(arrayListFragment);
            }
        });

        Log.d("MainMenuActivity.java", "7. timeScreen: " + String.valueOf(System.currentTimeMillis() - timeScreen)  );
        timeScreen = System.currentTimeMillis();
    }

    private boolean cekAngkutOn(){
        //cek apakah transaksi angkut sudah di create
        File db = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT) + "/" +Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT)+ ".db");
        File file = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
        value = null;
        if(file.exists()){
            value = GlobalHelper.readFileContent(file.getAbsolutePath());
//            Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
//            try {
//                String value = null;
//                JSONObject objTAngkut = new JSONObject(GlobalHelper.readFileContent(file.getAbsolutePath()));
//                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
//                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", objTAngkut.getString("idTransaksi")));
//                for (Iterator iterator = cursor.iterator(); iterator.hasNext();) {
//                    DataNitrit dataNitrit = (DataNitrit) iterator.next();
//                    value = dataNitrit.getValueDataNitrit();
//                    break;
//                }
//                Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.stop_angkut),Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(this, AngkutActivity.class);
//                if(value != null) {
//                    intent.putExtra("transaksiAngkut", value);
//                }
//                startActivityForResult(intent, GlobalHelper.RESULT_ANGKUTACTIVITY);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            db.close();
        }

        return db.exists();
    }

    private boolean spbOn(){
        File db = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT_SPB) + "/" +Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT_SPB)+ ".db");
        File dbMekanisasi = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI) + "/" +Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI)+ ".db");
        File fileSPBSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_SPB] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_SPB] );
        value = null;

        //dari spb yang sudah pernah di save atau ada di db
        if(fileSPBSelected.exists()){
            value = GlobalHelper.readFileContent(fileSPBSelected.getAbsolutePath());
        }

        if(db.exists()){
            return true;
        }else if (dbMekanisasi.exists()){
            return true;
        }else {
            return false;
        }
    }

    private boolean qcAncakOn(){
        File db = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_QC_ANCAK_POHON) + "/" +Encrypts.encrypt(GlobalHelper.TABLE_QC_ANCAK_POHON)+ ".db");
        File fileQcAncakSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECTED_QC_ANCAK] );
        value = null;

        //dari spb yang sudah pernah di save atau ada di db
        if(fileQcAncakSelected.exists()){
            value = GlobalHelper.readFileContent(fileQcAncakSelected.getAbsolutePath());
        }

        return db.exists();
    }

    private void main(){
        try {
            JSONObject jModule = GlobalHelper.getModule();

            if(jModule == null){
                Toast.makeText(HarvestApp.getContext(),getResources().getString(R.string.cannot_acses),Toast.LENGTH_LONG).show();
                Intent intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps");
//                if(BuildConfig.BUILD_VARIANT.equals("dev")){
//                    intent = getPackageManager().getLaunchIntentForPackage("id.co.ams_plantation.amsadminapps.dev");
//                }
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return;
            }

//            dataAllUser = GlobalHelper.getAllUser();
//            dataOperator = GlobalHelper.getAllOperator();
//            dataVehicle = GlobalHelper.getAllVehicle();

//            NavigationTabBar.Model panenHa = new NavigationTabBar.Model.Builder(
//                    ContextCompat.getDrawable(this, R.drawable.ic_farmeregret_bw),
//                    ContextCompat.getColor(this, R.color.light_green_color)
//            ).title(getResources().getString(R.string.panenHaCoverage)).build();

            NavigationTabBar.Model panen = new NavigationTabBar.Model.Builder(
                    ContextCompat.getDrawable(this, R.drawable.ic_panen_bw),
                    ContextCompat.getColor(this, R.color.OrangeRed)
            ).title(getResources().getString(R.string.panen)).build();

//            NavigationTabBar.Model angkut = new NavigationTabBar.Model.Builder(
//                    getResources().getDrawable(R.drawable.ic_trucking_bw),
//                    getResources().getColor(R.color.Gold)
//            ).title(getString(R.string.angkut)).build();

            NavigationTabBar.Model spb = new NavigationTabBar.Model.Builder(
                    getResources().getDrawable(R.drawable.ic_spb_bw),
                    getResources().getColor(R.color.DeepSkyBlue)
            ).title(getString(R.string.spb)).build();


//            NavigationTabBar.Model rekonsiliasi = new NavigationTabBar.Model.Builder(
//                    getResources().getDrawable(R.drawable.ic_rekonsilasi),
//                    getResources().getColor(R.color.Gold)
//            ).title(getString(R.string.rekonsiliasi)).build();
//
//
//            NavigationTabBar.Model qcAncak = new NavigationTabBar.Model.Builder(
//                    getResources().getDrawable(R.drawable.ic_palm_bw),
//                    getResources().getColor(R.color.light_green_color)
//            ).title(getString(R.string.qc_mutu_ancak)).build();
//
//            NavigationTabBar.Model qcMutuBuah = new NavigationTabBar.Model.Builder(
//                    getResources().getDrawable(R.drawable.ic_qc_bw),
//                    getResources().getColor(R.color.md_light_green_800)
//            ).title(getString(R.string.qc_mutu_buah)).build();
//
//            NavigationTabBar.Model qcBjr = new NavigationTabBar.Model.Builder(
//                    getResources().getDrawable(R.drawable.ic_sensus_bjr_bw),
//                    getResources().getColor(R.color.green_color)
//            ).title(getString(R.string.sensus_bjr)).build();

            NavigationTabBar.Model setting = new NavigationTabBar.Model.Builder(
                    ContextCompat.getDrawable(this, R.drawable.ic_settings),
                    ContextCompat.getColor(this, R.color.Blue)
            ).title(getString(R.string.action_settings)).build();

            ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
            arrayListFragment = new ArrayList<>();
            arrayListFragment.clear();
            if (jModule.getString("mdlAccCode").equals("HMS2")){
//                models.add(panenHa);
                models.add(panen);
//                models.add(angkut);
                models.add(spb);
//                models.add(rekonsiliasi);
                models.add(setting);

//                arrayListFragment.add(PemanenCoverageFragment.getInstance());
                arrayListFragment.add(TphFragment.getInstance());
//                arrayListFragment.add(AngkutFragment.getInstance());
                arrayListFragment.add(SpbFragment.getInstance());
//                arrayListFragment.add(RekonsilasiListFragment.getInstance());
                arrayListFragment.add(SettingFragment.getInstance());
            }else if (jModule.getString("mdlAccCode").equals("HMS3")){
                if(jModule.getString("subMdlAccCode").equals("P1")){
//                    models.add(panenHa);
                    models.add(panen);
                    models.add(setting);

//                    arrayListFragment.add(PemanenCoverageFragment.getInstance());
                    arrayListFragment.add(TphFragment.getInstance());
                    arrayListFragment.add(SettingFragment.getInstance());
                }else if(jModule.getString("subMdlAccCode").equals("P2")){
//                    models.add(angkut);
                    // test
//                    models.add(rekonsiliasi);
                    models.add(spb);
                    models.add(setting);

//                    arrayListFragment.add(AngkutFragment.getInstance());
//                    arrayListFragment.add(RekonsilasiListFragment.getInstance());
                    arrayListFragment.add(SpbFragment.getInstance());
                    arrayListFragment.add(SettingFragment.getInstance());
                }else if(jModule.getString("subMdlAccCode").equals("P3")){
                    models.add(spb);
                    models.add(setting);

                    arrayListFragment.add(SpbFragment.getInstance());
                    arrayListFragment.add(SettingFragment.getInstance());
                }else if(jModule.getString("subMdlAccCode").equals("P4")){
//                    models.add(angkut);
                    models.add(spb);
//                    models.add(rekonsiliasi);
                    models.add(setting);

//                    arrayListFragment.add(AngkutFragment.getInstance());
                    arrayListFragment.add(SpbFragment.getInstance());
//                    arrayListFragment.add(RekonsilasiListFragment.getInstance());
                    arrayListFragment.add(SettingFragment.getInstance());
                }else if(jModule.getString("subMdlAccCode").equals("P6")){
//                    models.add(panenHa);
                    models.add(panen);
//                    models.add(angkut);
                    models.add(spb);
//                    models.add(rekonsiliasi);
                    models.add(setting);

//                    arrayListFragment.add(PemanenCoverageFragment.getInstance());
                    arrayListFragment.add(TphFragment.getInstance());
//                    arrayListFragment.add(AngkutFragment.getInstance());
                    arrayListFragment.add(SpbFragment.getInstance());
//                    arrayListFragment.add(RekonsilasiListFragment.getInstance());
                    arrayListFragment.add(SettingFragment.getInstance());
                }else{
                    models.add(setting);
                    arrayListFragment.add(SettingFragment.getInstance());
                }
            }else if (jModule.getString("mdlAccCode").equals("HMS4")){
//                models.add(panenHa);
                models.add(panen);
//                models.add(angkut);
                models.add(spb);
//                models.add(rekonsiliasi);
//                models.add(qcAncak);
//                models.add(qcMutuBuah);
//                models.add(qcBjr);
                models.add(setting);

//                arrayListFragment.add(PemanenCoverageFragment.getInstance());
                arrayListFragment.add(TphFragment.getInstance());
//                arrayListFragment.add(AngkutFragment.getInstance());
                arrayListFragment.add(SpbFragment.getInstance());
//                arrayListFragment.add(RekonsilasiListFragment.getInstance());
//                arrayListFragment.add(QcMutuAncakFragmet.getInstance());
//                arrayListFragment.add(QcMutuBuahFragment.getInstance());
//                arrayListFragment.add(QcSensusBjrFragment.getInstance());
                arrayListFragment.add(SettingFragment.getInstance());
            }else if (jModule.getString("mdlAccCode").equals("HMS5")){
//                models.add(panenHa);
                models.add(panen);
//                models.add(angkut);
                models.add(spb);
//                models.add(rekonsiliasi);
//                models.add(qcAncak);
//                models.add(qcMutuBuah);
//                models.add(qcBjr);
                models.add(setting);

//                arrayListFragment.add(PemanenCoverageFragment.getInstance());
                arrayListFragment.add(TphFragment.getInstance());
//                arrayListFragment.add(AngkutFragment.getInstance());
                arrayListFragment.add(SpbFragment.getInstance());
//                arrayListFragment.add(RekonsilasiListFragment.getInstance());
//                arrayListFragment.add(QcMutuAncakFragmet.getInstance());
//                arrayListFragment.add(QcMutuBuahFragment.getInstance());
//                arrayListFragment.add(QcSensusBjrFragment.getInstance());
                arrayListFragment.add(SettingFragment.getInstance());
            }else{
                models.add(setting);
                arrayListFragment.add(SettingFragment.getInstance());
            }

            viewPager.setAdapter(new TabPagerAdapter(getSupportFragmentManager(), arrayListFragment));

//        models.add(
//                new NavigationTabBar.Model.Builder(
//                        getResources().getDrawable(R.drawable.ic_data_source),
//                        getResources().getColor(R.color.LightBlue)
//                ).title(getString(R.string.qc)).build()
//        );

            navigationTabBar.setModels(models);
            navigationTabBar.setViewPager(viewPager);

            navigationTabBar.post(new Runnable() {
                @Override
                public void run() {
                    final View bgNavigationTabBar = (View) findViewById(R.id.bg_ntb_horizontal);
                    bgNavigationTabBar.getLayoutParams().height = (int) navigationTabBar.getBarHeight();
                    bgNavigationTabBar.requestLayout();

                    final View viewPager = findViewById(R.id.vp_horizontal_ntb);
                    ((ViewGroup.MarginLayoutParams) viewPager.getLayoutParams()).topMargin =
                            (int) -navigationTabBar.getBadgeMargin();
                    viewPager.requestLayout();
                }
            });
//            viewPager.setOffscreenPageLimit(0);
            navigationTabBar.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
                @Override
                public void onStartTabSelected(final NavigationTabBar.Model model, final int index) {

                }

                @Override
                public void onEndTabSelected(final NavigationTabBar.Model model, final int index) {
                    model.hideBadge();
                }
            });
            viewPager.setOffscreenPageLimit(1);

            User user = GlobalHelper.getUser();
            footerUser.setText(user.getUserFullName());
            footerEstate.setText(GlobalHelper.getEstate().getEstCode() + " - " +GlobalHelper.getEstate().getEstName());

            nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            if (nfcAdapter == null) {
                // Stop here, we definitely need NFC
                Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            }

            pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
            tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
            writeTagFilters = new IntentFilter[]{tagDetected};
//
//        btnWrite.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (myTag == null) {
//                    Toast.makeText(getContext(), getResources().getString(R.string.nfc_error), Toast.LENGTH_LONG).show();
//                } else {
//                    presenter.prepareWriteNFC(editText.getText().toString(),myTag);
//                }
//            }
//        });
//
//        btnQr.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                presenter.createQr(qrHelper,ivQr,editText.getText().toString());
//                tvIsiQr.setText(editText.getText().toString());
//            }
//        });
//
//        btnPrintnQr.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String msg = null;
//                try {
//                    msg = GlobalHelper.compress(editText.getText().toString());
//                    bluetoothHelper.printPanen(msg,tvIsiQr.getText().toString());
//                } catch (IOException e) {
//                    Toast.makeText(HarvestApp.getContext(),"Tidak Bisa Create Qr",Toast.LENGTH_SHORT).show();
//                    e.printStackTrace();
//                }
//            }
//        });
//
//        btnScanQr.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getContext(), QRScan.class);
//                startActivityForResult(intent,  REQUEST_SCAN_QR);
//            }
//        });
//
//        btnShareQR.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                presenter.shareQr(activity,qrHelper,drawCanvas);
//            }
//        });
//
//
//        btnSelectPrint.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                bluetoothHelper.showListBluetoothPrinter();
//            }
//        });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void popupSPBOn(){
        AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                .setCancelable(false)
                .setTitle("Perhatian")
                .setMessage("Apakah Anda ingin melanjutkan proses SPB sebelumnya yang belum selesai ?")
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        popupSPBOn2();
//                        TransaksiSpbHelper.hapusFileAngkut(true);
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        File dbMekanisasi = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT_SPB) + "/" +Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT_SPB)+ ".db");

                        if(dbMekanisasi.exists()) {
                            Intent intent = new Intent(MainMenuActivity.this, SpbActivity.class);
                            if (value != null) {
                                if (!value.isEmpty()) {
                                    Gson gson = new Gson();
                                    TransaksiSPB parsingTSpb = gson.fromJson(value, TransaksiSPB.class);
                                    parsingTSpb.getTransaksiAngkut().setAngkuts(null);
                                    intent.putExtra("transaksiSpb", gson.toJson(parsingTSpb));
                                }
                            }
                            startActivityForResult(intent, GlobalHelper.RESULT_SPBACTIVITY);
                        }else{

                            Intent intent = new Intent(MainMenuActivity.this, MekanisasiActivity.class);
                            if (value != null) {
                                if (!value.isEmpty()) {
                                    Gson gson = new Gson();
                                    TransaksiSPB parsingTSpb = gson.fromJson(value, TransaksiSPB.class);
                                    parsingTSpb.getTransaksiAngkut().setAngkuts(null);
                                    intent.putExtra("transaksiSpb", gson.toJson(parsingTSpb));
                                }
                            }
                            startActivityForResult(intent, GlobalHelper.RESULT_MEKANISASIACTIVITY);
                        }
                    }
                })
                .create();
        alertDialog.show();
    }

    private void popupSPBOn2(){
        AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle)
                .setCancelable(false)
                .setTitle("Perhatian")
                .setMessage("Apakah Anda yakin untuk mengakhiri proses SPB sebelumnya yang belum selesai ? ")
                .setNegativeButton("Lanjut Proses SPB", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        File dbMekanisasi = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT_SPB) + "/" +Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT_SPB)+ ".db");

                        if(dbMekanisasi.exists()){
                            Intent intent = new Intent(MainMenuActivity.this,SpbActivity.class);
                            if(value != null) {
                                if(!value.isEmpty()) {
                                    Gson gson = new Gson();
                                    TransaksiSPB parsingTSpb = gson.fromJson(value, TransaksiSPB.class);
                                    parsingTSpb.getTransaksiAngkut().setAngkuts(null);
                                    intent.putExtra("transaksiSpb", gson.toJson(parsingTSpb));
                                }
                            }
                            startActivityForResult(intent, GlobalHelper.RESULT_SPBACTIVITY);
                        }else{
                            Intent intent = new Intent(MainMenuActivity.this,MekanisasiActivity.class);
                            if(value != null) {
                                if(!value.isEmpty()) {
                                    Gson gson = new Gson();
                                    TransaksiSPB parsingTSpb = gson.fromJson(value, TransaksiSPB.class);
                                    parsingTSpb.getTransaksiAngkut().setAngkuts(null);
                                    intent.putExtra("transaksiSpb", gson.toJson(parsingTSpb));
                                }
                            }
                            startActivityForResult(intent, GlobalHelper.RESULT_MEKANISASIACTIVITY);
                        }
                    }
                })
                .setPositiveButton("Hapus SPB", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        TransaksiSpbHelper.hapusFileAngkut(true);
                        dialog.dismiss();
                    }
                })
                .create();
        alertDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        // If Bluetooth is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!bluetoothHelper.mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, GlobalHelper.REQUEST_ENABLE_BT);
            // Otherwise, setup the session
        } else {
            if (bluetoothHelper.mService == null)
                bluetoothHelper.mService = new BluetoothService(this, bluetoothHelper.mHandler);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            Log.i("tagNFC", GlobalHelper.bytesToHexString(myTag.getId()));
            Log.i("tagNFC", Arrays.toString(myTag.getTechList()));
        }
        value = readFromIntent(intent);
        if (value != null) {
            int formatNFC = NfcHelper.cekFormatNFC(value);
            if(formatNFC != GlobalHelper.TYPE_NFC_SALAH && value.length() == 5){
                Toast.makeText(this,getResources().getString(R.string.nfc_empty) +" "+ GlobalHelper.LIST_NFC[formatNFC],Toast.LENGTH_SHORT).show();
            }else {
                cekDataPassing(value, this);
            }
        }else {
            Toast.makeText(this,R.string.format_not_valid,Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
//        unRegister();
        NfcHelper.WriteModeOff(nfcAdapter,this);
        endTimeScreen = new Date();
        if(GlobalHelper.getUser()!=null){
            ActivityLoggerHelper.recordScreenTime(MainMenuActivity.class.getSimpleName(),startTimeScreen, endTimeScreen);
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        startTimeScreen = new Date();
//        registerCrashReceiver();
        NfcHelper.WriteModeOn(nfcAdapter,this,pendingIntent,writeTagFilters);
        if(bluetoothHelper != null) {
            if (bluetoothHelper.mService != null) {

                if (bluetoothHelper.mService.getState() == BluetoothService.STATE_NONE) {
                    // Start the Bluetooth services
                    bluetoothHelper.mService.start();
                }
            }
        }


        if(!GlobalHelper.isTimeAutomatic(MainMenuActivity.this)){
            if(!BuildConfig.BUILD_VARIANT.equals("dev")) {
                AlertDialog alertDialog = WidgetHelper.showOKCancelDialog(MainMenuActivity.this,
                        "Aktifkan Tanggal & Waktu Otomatis",
                        "Mohon aktifkan konfigurasi tanggal & waktu otomatis untuk melanjutkan menggunakan aplikasi",
                        "Tutup",
                        "Buka Pengaturan",
                        false,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivityForResult(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS), 0);
                                finish();
                            }
                        }, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(MainMenuActivity.this, "Anda harus mengaktifkan tanggal otomatis", Toast.LENGTH_SHORT).show();
                                MainMenuActivity.this.finish();
                            }
                        });

                alertDialog.show();
                return;
            }
        }

//        if(!GlobalVars.checkCoreFileExists()){
//            Bundle bundle = new Bundle();
//            bundle.putString("message","Anda tidak memiliki akses/akses anda habis. Silahkan aktivasi ulang melalui Admin Apps!");
//            Intent intent = new Intent(this,EntryActivity.class);
//            intent.putExtras(bundle);
//            startActivity(intent);
//        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        endTimeScreen = new Date();
        if(GlobalHelper.getUser()!=null){
            ActivityLoggerHelper.recordScreenTime(MainMenuActivity.class.getSimpleName(),startTimeScreen, endTimeScreen);
        }
        // Stop the Bluetooth services
        if (bluetoothHelper != null) {
            if (bluetoothHelper.mService != null)
                bluetoothHelper.mService.stop();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            switch (resultCode){
                case GlobalHelper.RESULT_TPHACTIVITY: {
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof TphFragment){
                        ((TphFragment) fragment).startLongOperation(TphFragment.STATUS_LONG_OPERATION_NONE);
                    }

                    break;
                }
                case GlobalHelper.RESULT_ANGKUTACTIVITY: {
                    cekAngkutOn();
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof AngkutFragment){
                        ((AngkutFragment) fragment).startLongOperation(AngkutFragment.STATUS_LONG_OPERATION_NONE);
                    }
                    if(fragment instanceof SpbFragment){
                        ((SpbFragment) fragment).startLongOperation(SpbFragment.STATUS_LONG_OPERATION_NONE);
                    }
                    break;
                }
                case GlobalHelper.RESULT_SPBACTIVITY: {
                    spbOn();
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof SpbFragment){
                        ((SpbFragment) fragment).startLongOperation(SpbFragment.STATUS_LONG_OPERATION_NONE);
                    }
                    break;
                }
                case GlobalHelper.RESULT_MEKANISASIACTIVITY: {
                    spbOn();
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof SpbFragment){
                        ((SpbFragment) fragment).startLongOperation(SpbFragment.STATUS_LONG_OPERATION_NONE);
                    }else if (fragment instanceof TphFragment){
                        ((TphFragment) fragment).startLongOperation(TphFragment.STATUS_LONG_OPERATION_NONE);
                    }
                    break;
                }
                case GlobalHelper.RESULT_SCAN_QR:{
                    String isiQr = data.getExtras().getString(QRScan.NILAI_QR_SCAN);
                    cekDataPassing(isiQr,this);
                    break;
                }
                case GlobalHelper.RESULT_MAPACTIVITY:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof SettingFragment){
                        if (((BaseActivity) this).alertDialogBase != null) {
                            ((BaseActivity) this).alertDialogBase.cancel();
                        }
                    }
                    break;
                }
                case GlobalHelper.RESULT_PEMANENACTIVITY:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof SettingFragment){
                        if (((BaseActivity) this).alertDialogBase != null) {
                            ((BaseActivity) this).alertDialogBase.cancel();
                        }
                    }
                    break;
                }
                case GlobalHelper.RESULT_FORMATNFCACTIVITY:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof SettingFragment){
                        if (((BaseActivity) this).alertDialogBase != null) {
                            ((BaseActivity) this).alertDialogBase.cancel();
                        }
                    }
                    break;
                }
                case GlobalHelper.RESULT_CEKBJRACTIVITY:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof SettingFragment){
                        if (((BaseActivity) this).alertDialogBase != null) {
                            ((BaseActivity) this).alertDialogBase.cancel();
                        }
                    }
                    break;
                }
                case GlobalHelper.RESULT_SPKACTIVITY:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof SettingFragment){
                        if (((BaseActivity) this).alertDialogBase != null) {
                            ((BaseActivity) this).alertDialogBase.cancel();
                        }
                    }
                    break;
                }
                case GlobalHelper.RESULT_CAGESACTIVITY:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof SettingFragment){
                        if (((BaseActivity) this).alertDialogBase != null) {
                            ((BaseActivity) this).alertDialogBase.cancel();
                        }
                    }
                    break;
                }
                case GlobalHelper.RESULT_MEKANISASIBLOCKACTIVITY:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof SettingFragment){
                        if (((BaseActivity) this).alertDialogBase != null) {
                            ((BaseActivity) this).alertDialogBase.cancel();
                        }
                    }
                    break;
                }
                case GlobalHelper.RESULT_OPNAMENFCACTIVITY:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof SettingFragment){
                        if (((BaseActivity) this).alertDialogBase != null) {
                            ((BaseActivity) this).alertDialogBase.cancel();
                        }
                    }
                    break;
                }
                case GlobalHelper.RESULT_COUNTNFCACTIVITY:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof SettingFragment){
                        if (((BaseActivity) this).alertDialogBase != null) {
                            ((BaseActivity) this).alertDialogBase.cancel();
                        }
                    }
                    break;
                }
                case GlobalHelper.RESULT_QC_MUTU_BUAH:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof QcMutuBuahFragment){
                        ((QcMutuBuahFragment) fragment).startLongOperation(QcMutuBuahFragment.STATUS_LONG_OPERATION_NONE);
                    }
                    break;
                }
                case GlobalHelper.RESULT_QC_SENSUS_BJR:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof QcSensusBjrFragment){
                        ((QcSensusBjrFragment) fragment).startLongOperation(QcSensusBjrFragment.STATUS_LONG_OPERATION_NONE);
                    }
                    break;
                }
                case GlobalHelper.RESULT_QC_MUTU_ANCAK:{
                    Fragment fragment = ((TabPagerAdapter)viewPager.getAdapter()).getItem(viewPager.getCurrentItem());
                    if(fragment instanceof QcMutuAncakFragmet){
                        ((QcMutuAncakFragmet) fragment).startLongOperation(QcMutuAncakFragmet.STATUS_LONG_OPERATION_NONE);
                    }
                    break;
                }
            }
    }

    @Override
    public void onBackPressed() {
        AlertDialog alertDialog = new AlertDialog.Builder(this,R.style.MyAlertDialogStyle)
                .setTitle("Perhatian")
                .setMessage("Yakin keluar aplikasi?")
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .create();
        alertDialog.show();
    }
    public class TabPagerAdapter extends FragmentStatePagerAdapter {
        ArrayList<Fragment> arrayListFragment;
        public TabPagerAdapter(FragmentManager fm, ArrayList<Fragment> arrayListFragment) {
            super(fm);
            this.arrayListFragment = arrayListFragment;
        }
        @Override
        public Fragment getItem(int position) {
            return arrayListFragment.get(position);
        }

        @Override
        public int getCount() {
            return arrayListFragment.size();
        }

    }
    //tambahan broadcast crash
    private void registerCrashReceiver(){
        serverConnectionListener = new ServerConnectionListener();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(serverConnectionListener,intentFilter);
    }
    private void unRegister(){
        if(serverConnectionListener!=null){
            unregisterReceiver(serverConnectionListener);
        }
    }

}
