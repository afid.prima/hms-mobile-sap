package id.co.ams_plantation.harvestsap.model;

public class ManualSPB {

    public final static int TYPE_KARTU_HILANG  = 1;
    public final static int TYPE_KARTU_RUSAK  = 2;
    public final static int TYPE_KARTU_MAX_RESTAN  = 3;
    public final static String TYPE_KARTU_HILANG_NAME = "Kartu Hilang";
    public final static String TYPE_KARTU_RUSAK_NAME = "Kartu Rusak";
    public final static String TYPE_KARTU_MAX_RESTAN_NAME = "Kartu Max Restan";


    private String id;
    private String userId;
    private String kebun;
    private int tipeId;
    private String tipe;
    private String block;
    private TPH tph;
    private long createDate;
    private long updateDate;
    private String cardId;
    private TransaksiPanen transaksiPanen;

    public ManualSPB(String id,String userId, String kebun, int tipeId,String tipe, String block, TPH tph, long createDate, long updateDate){
        this.id = id;
        this.userId = userId;
        this.kebun = kebun;
        this.tipeId = tipeId;
        this.tipe = tipe;
        this.block = block;
        this.tph = tph;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public ManualSPB(String id,String userId, String kebun, int tipeId, String tipe, String block, TPH tph, long createDate, long updateDate, String cardId){
        this.id = id;
        this.userId = userId;
        this.kebun = kebun;
        this.tipeId = tipeId;
        this.tipe = tipe;
        this.block = block;
        this.tph = tph;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.cardId = cardId;
    }

    public String getId(){
        return this.id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getKebun() {
        return kebun;
    }

    public void setKebun(String kebun) {
        this.kebun = kebun;
    }

    public int getTipeId() {
        return tipeId;
    }

    public void setTipeId(int tipeId) {
        this.tipeId = tipeId;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public TPH getTph() {
        return tph;
    }

    public void setTph(TPH tph) {
        this.tph = tph;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(long updateDate) {
        this.updateDate = updateDate;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public TransaksiPanen getTransaksiPanen() {
        return transaksiPanen;
    }

    public void setTransaksiPanen(TransaksiPanen transaksiPanen) {
        this.transaksiPanen = transaksiPanen;
    }
}
