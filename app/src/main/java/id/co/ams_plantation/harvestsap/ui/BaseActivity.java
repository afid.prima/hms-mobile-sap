package id.co.ams_plantation.harvestsap.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.esri.android.map.MapView;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import butterknife.ButterKnife;
import id.co.ams_plantation.harvestsap.Fragment.AngkutFragment;
import id.co.ams_plantation.harvestsap.Fragment.AngkutInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.CountNfcInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.LangsiranInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.MekanisasiInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.MekanisasiPanenInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.QcMutuBuahFragment;
import id.co.ams_plantation.harvestsap.Fragment.QcSensusBjrFragment;
import id.co.ams_plantation.harvestsap.Fragment.QcSensusBjrInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.SpbInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.TphFragment;
import id.co.ams_plantation.harvestsap.Fragment.TphInputFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.QcMutuBuah;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.util.BluetoothHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.NfcHelper;
import id.co.ams_plantation.harvestsap.util.ResizeImage;
import id.co.ams_plantation.harvestsap.util.SharedPrefHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiPanenHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiSpbHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import id.zelory.compressor.Compressor;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

import static id.co.ams_plantation.harvestsap.util.GlobalHelper.FASTESTINTERVAL;
import static id.co.ams_plantation.harvestsap.util.GlobalHelper.INTERVAL;
import static id.co.ams_plantation.harvestsap.util.GlobalHelper.MAX_ACCURACY;
import static id.co.ams_plantation.harvestsap.util.GlobalHelper.MAX_ACCURACY_CALIBRATION;
import static id.co.ams_plantation.harvestsap.util.GlobalHelper.MAX_ACCURACY_MAPMENU;
import static id.co.ams_plantation.harvestsap.util.GlobalHelper.TAG_CAMERA;


public abstract class BaseActivity extends AppCompatActivity {
    public BluetoothHelper bluetoothHelper;
    public LocationManager mLocationManager;
    public Location currentlocation;
    public GoogleApiClient mGoogleApiClient;
    public LocationRequest mLocationRequest;

    public RelativeLayout rlMap;
    public MapView mapView;
    public ImageView zoomToLocation;

    final int LongOperation_CekDataPassing_Biru = 0;
    final int LongOperation_CekDataPassing_Hijau = 1;
    final int LongOperation_CekDataPassing_Red = 2;

    Activity activitySelected;
    public JSONObject objSeledted = new JSONObject();

    public AlertDialog alertDialogBase;
    public int idxUpload;
    public View viewIndicator;

    protected abstract void initPresenter();
    public Date startTimeScreen;
    public Date endTimeScreen;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        bluetoothHelper = new BluetoothHelper(this);
        initPresenter();

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1,
                1f, mLocationListener);
    }

    public void setNavigationTitle(TextView toolbar, @StringRes int title) {
        toolbar.setTextColor(Color.parseColor("#FFFFFF"));
        toolbar.setText(title);
    }

    public void setNavigationBack(TextView toolbar, ImageView backImg, @StringRes int title) {
        backImg.setVisibility(View.VISIBLE);
        backImg.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        toolbar.setTextColor(Color.parseColor("#FFFFFF"));
        toolbar.setText(title);
        toolbar.setOnClickListener(view -> finish());
    }

    public void setNavigationBackWithMenu(TextView toolbar, ImageView backImg, ImageView menuImg, @StringRes int title) {
        backImg.setVisibility(View.VISIBLE);
        menuImg.setVisibility(View.VISIBLE);
        backImg.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        menuImg.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_menu)
                .colorRes(R.color.White)
                .sizeDp(24));
        toolbar.setTextColor(Color.parseColor("#FFFFFF"));
        toolbar.setText(title);
        toolbar.setOnClickListener(view -> finish());
    }

    public void cekDataPassing(String value, Activity activity) {
        try {
            activitySelected = activity;
            objSeledted = new JSONObject(value);
            switch (objSeledted.getInt("z")) {
                case GlobalHelper.TYPE_NFC_BIRU: {
//                    addFlagInNFC(value,activity,GlobalHelper.TYPE_NFC_BIRU);
                    new LongOperation().execute(String.valueOf(LongOperation_CekDataPassing_Biru));
                    break;
                }
                case GlobalHelper.TYPE_NFC_HIJAU: {
                    new LongOperation().execute(String.valueOf(LongOperation_CekDataPassing_Hijau));
                    break;
                }
                case GlobalHelper.TYPE_NFC_RED: {
                    new LongOperation().execute(String.valueOf(LongOperation_CekDataPassing_Red));
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.data_not_valid), Toast.LENGTH_SHORT).show();
        }
    }


    public static boolean addFlagInNFC(String valueNFC, Activity activity,int typeNfc){
        JSONObject jNFC = null;
        try {
            jNFC = new JSONObject(valueNFC);
            jNFC.put("y", GlobalHelper.getUser().getUserID());
            if(!jNFC.has("x")) {
                jNFC.put("x", System.currentTimeMillis());
            }
            if(jNFC.has("w")){
                int w = jNFC.getInt("w");
                jNFC.put("w", w + 1);
            }else {
                jNFC.put("w", 1);
            }
            String compres = jNFC.toString();

            if(activity instanceof  AngkutActivity) {
                if (((AngkutActivity) activity).TYPE_NFC == typeNfc) {
                    return NfcHelper.write(compres, ((AngkutActivity) activity).myTag, false);
                }
            }else if (activity instanceof  SpbActivity){
                if (((SpbActivity) activity).TYPE_NFC == typeNfc) {
                    return NfcHelper.write(compres, ((SpbActivity) activity).myTag, false);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean formatNfcPanen(Activity activity,int typeNfc){
        boolean formatReturn = false;
        if(activitySelected instanceof  AngkutActivity) {
            if (((AngkutActivity) activity).TYPE_NFC == typeNfc) {
                formatReturn = NfcHelper.writeNewFormat(GlobalHelper.FORMAT_NFC_PANEN, ((AngkutActivity) activitySelected).myTag);
                ((AngkutActivity) activitySelected).myTag = null;
            }
        }else if (activity instanceof  SpbActivity){
            if (((SpbActivity) activity).TYPE_NFC == typeNfc) {
                formatReturn =NfcHelper.writeNewFormat(GlobalHelper.FORMAT_NFC_PANEN, ((SpbActivity) activitySelected).myTag);
                ((SpbActivity) activitySelected).myTag = null;
            }
        }else if (activity instanceof  MekanisasiActivity){
            if (((MekanisasiActivity) activity).TYPE_NFC == typeNfc) {
                formatReturn =NfcHelper.writeNewFormat(GlobalHelper.FORMAT_NFC_PANEN, ((MekanisasiActivity) activitySelected).myTag);
                ((MekanisasiActivity) activitySelected).myTag = null;
            }
        }

        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_TPH_NFC);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        try {
            DataNitrit dataNitrit = new DataNitrit(objSeledted.getString("a"), objSeledted.toString());
            Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", dataNitrit.getIdDataNitrit()));
            if (cursor.size() > 0) {
                repository.update(dataNitrit);
            } else {
                repository.insert(dataNitrit);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();
        return  formatReturn;
    }

    public void setIntent(Context activity, Class<?> aClass) {
        Intent intent = new Intent(activity, aClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                if(TAG_CAMERA == GlobalHelper.TAG_CAMERA_TRANSAKSI_SPB_MEKANISASI ||
                        TAG_CAMERA == GlobalHelper.TAG_CAMERA_TRANSAKSI_ANGKUT ||
                        TAG_CAMERA == GlobalHelper.TAG_CAMERA_TRANSAKSI_SPB ){
                    if(currentlocation != null){

                        File picture = imageFiles.get(0);
                        try {
                            picture = new Compressor(BaseActivity.this)
                                    .setMaxWidth((int) GlobalHelper.maxWidthImages)
                                    .setMaxHeight((int) GlobalHelper.maxHeightImages)
                                    .compressToFile(imageFiles.get(0));

                            // Mendapatkan dimensi tanpa memuat seluruh gambar ke memori
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            BitmapFactory.decodeFile(picture.getAbsolutePath(), options);

                            // Mendapatkan lebar dan tinggi
                            int width = options.outWidth;
                            int height = options.outHeight;
                            Log.d("BA onActivityResult", "onImagesPicked: kompres berhasil SPB " + String.valueOf(width) + "x" + String.valueOf(height));
                            Log.d("BA onActivityResult", "onImagesPicked: kompres berhasil SPB " + picture.getAbsolutePath());

                            if(width > GlobalHelper.maxWidthImages || height > GlobalHelper.maxHeightImages){
                                File newFileTemp = new File(picture.getParent() + "/" + picture.getName() + "X.JPG");

                                Bitmap resizedBitmap = ResizeImage.resizeImageSafely(picture.getAbsolutePath(),newFileTemp.getAbsolutePath());
                                if(resizedBitmap != null){
                                    Log.d("BA onActivityResult", "onImagesPicked: SPB Sukses");
                                    picture = newFileTemp;
                                }
                            }
                        } catch (IOException e) {
                            Log.d("BA onActivityResult", "onImagesPicked: SPB "+ e.toString());
                            throw new RuntimeException(e);
                        }
//                        Compressor compressor = compressorBuilder.build();
//                        File picture = compressor;
//                        File picture = Compressor.getDefault(BaseActivity.this).compressToFile(imageFiles.get(0));
                        Location location = currentlocation;

                        String descExift = GlobalHelper.exiftDescAngkut();
                        picture = GlobalHelper.addExif(picture, location,descExift);

//                        picture = GlobalHelper.photoAddStampAngkut(picture);
//                        picture = GlobalHelper.photoAddStamp(picture, location,"");
                        if (picture != null)
                            if (picture.exists()) {
                                for (File file : imageFiles) file.delete();
                            }
                        String namaFile = System.currentTimeMillis() + "_" + String.format("%.5f", location.getLongitude()) + "_" + String.format("%.5f", location.getLatitude());
                        try {
                            File newdir, selectedImage;
                            newdir = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT]);
                            selectedImage = GlobalHelper.moveFile(picture, newdir, namaFile);
                            if(TAG_CAMERA == GlobalHelper.TAG_CAMERA_TRANSAKSI_ANGKUT) {
                                AngkutInputFragment fragment = (AngkutInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                ((AngkutInputFragment) fragment).addImages(selectedImage);
                            }else if (TAG_CAMERA == GlobalHelper.TAG_CAMERA_TRANSAKSI_SPB){
                                SpbInputFragment fragment = (SpbInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                ((SpbInputFragment) fragment).addImages(selectedImage);
                            }else if (TAG_CAMERA == GlobalHelper.TAG_CAMERA_TRANSAKSI_SPB_MEKANISASI){
                                MekanisasiInputFragment fragment = (MekanisasiInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                ((MekanisasiInputFragment) fragment).addImages(selectedImage);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }else{
                        Toast.makeText(HarvestApp.getContext(), "Mencari Gps", Toast.LENGTH_SHORT).show();
                    }
                }else if (TAG_CAMERA == GlobalHelper.TAG_CAMERA_TRANSAKSI_TPH_MEKANISASI) {
                    //tidak dipakai
                    File picture = imageFiles.get(0);
                    try {
                        picture = new Compressor(BaseActivity.this)
                                .setMaxWidth((int) GlobalHelper.maxWidthImages)
                                .setMaxHeight((int) GlobalHelper.maxHeightImages)
                                .compressToFile(imageFiles.get(0));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    Location location = currentlocation;

                    picture = GlobalHelper.photoAddStampPanenMekanisasi(picture);
                    picture = GlobalHelper.photoAddStampPanenTotalJanJangMekanisasi(picture);
                    picture = GlobalHelper.photoAddStampPanenAtasMekanisais(picture, location);

                    if (picture != null)
                        if (picture.exists()) {
                            for (File file : imageFiles) file.delete();
                        }
                    String namaFile = String.valueOf(System.currentTimeMillis());

                    if(location != null){
                        namaFile += "_" + String.format("%.5f", location.getLongitude()) + "_" + String.format("%.5f", location.getLatitude());
                    }

                    try {
                        File newdir, selectedImage;
                        newdir = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TRANSAKSI_TPH]);
                        selectedImage = GlobalHelper.moveFile(picture, newdir, namaFile);
                        MekanisasiPanenInputFragment fragment = (MekanisasiPanenInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                        ((MekanisasiPanenInputFragment) fragment).addImages(selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else {
                    if (currentLocationOK()) {
                        // Buat objek Compressor.Builder
                        File picture = imageFiles.get(0);
                        try {
                            picture = new Compressor(BaseActivity.this)
                                    .setMaxWidth((int) GlobalHelper.maxWidthImages)
                                    .setMaxHeight((int) GlobalHelper.maxHeightImages)
                                    .compressToFile(imageFiles.get(0));

                            // Mendapatkan dimensi tanpa memuat seluruh gambar ke memori
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            BitmapFactory.decodeFile(picture.getAbsolutePath(), options);

                            // Mendapatkan lebar dan tinggi
                            int width = options.outWidth;
                            int height = options.outHeight;
                            Log.d("BA onActivityResult", "onImagesPicked: kompres berhasil " + String.valueOf(width) + "x" + String.valueOf(height));
                            Log.d("BA onActivityResult", "onImagesPicked: kompres berhasil " + picture.getAbsolutePath());

                            if(width > GlobalHelper.maxWidthImages || height > GlobalHelper.maxHeightImages){
                                File newFileTemp = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TPH] + "/" + picture.getName() + ".JPG");

                                Bitmap resizedBitmap = ResizeImage.resizeImageSafely(picture.getAbsolutePath(),newFileTemp.getAbsolutePath());
                                if(resizedBitmap != null){
                                    Log.d("BA onActivityResult", "onImagesPicked: Sukses");
                                    picture = newFileTemp;
                                }
                            }

                        } catch (IOException e) {
                            Log.d("BA onActivityResult", "onImagesPicked: "+ e.toString());
                            throw new RuntimeException(e);
                        }

//                        File picture = Compressor.getDefault(BaseActivity.this).compressToFile(imageFiles.get(0));
                        Location location = currentlocation;
                        if (TAG_CAMERA == GlobalHelper.TAG_CAMERA_NEW_TPH_MAP_ACTIVITY || TAG_CAMERA == GlobalHelper.TAG_CAMERA_NEW_LANGSIRAN_MAP_ACTIVITY) {
                            try {
                                SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.LATLON_PENDAFTARAN, Context.MODE_PRIVATE);
                                String objLatlon = preferences.getString(HarvestApp.LATLON_PENDAFTARAN, null);

                                JSONObject obj = new JSONObject(objLatlon);
                                location.setLongitude(obj.getDouble("lon"));
                                location.setLatitude(obj.getDouble("lat"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (TAG_CAMERA == GlobalHelper.TAG_CAMERA_TRANSAKSI_TPH) {
                            if(location != null){
                                if(location.getLatitude() == 0.00 && location.getLongitude() == 0.00){
                                    TransaksiPanen transaksiPanen = SharedPrefHelper.getSharePrefTransaksiPanen();
                                    if(transaksiPanen.getTph() != null){
                                        if(transaksiPanen.getTph().getNoTph() != null){
                                            location.setLatitude(transaksiPanen.getLatitude());
                                            location.setLongitude(transaksiPanen.getLongitude());
                                        }
                                    }
                                }
                            }
                            String descExift = GlobalHelper.exiftDescPanen();
                            picture = GlobalHelper.addExif(picture, location,descExift);
//                            picture = GlobalHelper.photoAddStampPanen(picture);
//                            picture = GlobalHelper.photoAddStampPanenTotalJanJang(picture);
//                            picture = GlobalHelper.photoAddStampPanenAtas(picture, location);
                        } else {
//                            picture = GlobalHelper.photoAddStamp(picture, location,"");
                            picture = GlobalHelper.addExif(picture, location,"");
                        }
                        if (picture != null)
                            if (picture.exists()) {
                                for (File file : imageFiles) file.delete();
                            }
                        String namaFile = System.currentTimeMillis() + "_" + String.format("%.5f", location.getLongitude()) + "_" + String.format("%.5f", location.getLatitude());
                        try {
                            File newdir, selectedImage;
                            switch (TAG_CAMERA) {
                                case GlobalHelper.TAG_CAMERA_NEW_TPH: {
                                    newdir = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TPH]);
                                    selectedImage = GlobalHelper.moveFile(picture, newdir, namaFile);
                                    TphInputFragment fragment = (TphInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                    ((TphInputFragment) fragment).tphHelper.setImages(selectedImage, BaseActivity.this);
                                    break;
                                }
                                case GlobalHelper.TAG_CAMERA_NEW_TPH_MAP_ACTIVITY: {
                                    if (currentlocation.getAccuracy() < MAX_ACCURACY_MAPMENU) {
                                        newdir = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TPH]);
                                        selectedImage = GlobalHelper.moveFile(picture, newdir, namaFile);
                                        ((MapActivity) BaseActivity.this).tphHelper.setImages(selectedImage, BaseActivity.this);
                                    } else {
                                        Toast.makeText(HarvestApp.getContext(), "GPS Tidak Akurat", Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                                }
                                case GlobalHelper.TAG_CAMERA_TRANSAKSI_TPH: {
                                    newdir = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TRANSAKSI_TPH]);
                                    selectedImage = GlobalHelper.moveFile(picture, newdir, namaFile);
                                    TphInputFragment fragment = (TphInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                    ((TphInputFragment) fragment).setImages(selectedImage);
                                    break;
                                }
                                case GlobalHelper.TAG_CAMERA_NEW_LANGSIRAN: {
                                    newdir = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_LANGSIRAN]);
                                    selectedImage = GlobalHelper.moveFile(picture, newdir, namaFile);
                                    AngkutInputFragment fragment = (AngkutInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                    ((AngkutInputFragment) fragment).tujuanHelper.setImages(selectedImage, BaseActivity.this);
                                    break;
                                }
                                case GlobalHelper.TAG_CAMERA_NEW_LANGSIRAN_MAP_ACTIVITY: {
                                    if (currentlocation.getAccuracy() < MAX_ACCURACY_MAPMENU) {
                                        newdir = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_TPH]);
                                        selectedImage = GlobalHelper.moveFile(picture, newdir, namaFile);
                                        ((MapActivity) BaseActivity.this).tujuanHelper.setImages(selectedImage, BaseActivity.this);
                                    } else {
                                        Toast.makeText(HarvestApp.getContext(), "GPS Tidak Akurat", Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                                }
                                case GlobalHelper.TAG_CAMERA_QC_SENSUSBJR: {
                                    newdir = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 + "/" + GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_QC_SENSUS_BJR]);
                                    selectedImage = GlobalHelper.moveFile(picture, newdir, namaFile);
                                    QcSensusBjrInputFragment fragment = (QcSensusBjrInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                    ((QcSensusBjrInputFragment) fragment).setImages(selectedImage);
                                    break;
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(HarvestApp.getContext(), "GPS Tidak Akurat", Toast.LENGTH_SHORT).show();
                    }
                }
                return;
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(BaseActivity.this);
                    if (photoFile != null) photoFile.delete();
                }
                return;
            }
        });
        switch (requestCode) {
            case GlobalHelper.REQUEST_ENABLE_BT: {
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a session
                    Toast.makeText(this, "Bluetooth Aktif", Toast.LENGTH_SHORT).show();
                } else {
                    // User did not enable Bluetooth or an error occured
                    Toast.makeText(this, R.string.bt_not_enabled,
                            Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    public final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            currentlocation = location;
            if (currentLocationOK()) {
                if (BaseActivity.this instanceof TphActivity) {
                    if (((TphActivity) BaseActivity.this).searchingGPS != null) {
                        if (((TphActivity) BaseActivity.this).searchingGPS.isShown()) {
                            ((TphActivity) BaseActivity.this).searchingGPS.dismiss();
                        }
                    }
                } else if (BaseActivity.this instanceof AngkutActivity) {
                    if (((AngkutActivity) BaseActivity.this).searchingGPS != null) {
                        if (((AngkutActivity) BaseActivity.this).searchingGPS.isShown()) {
                            ((AngkutActivity) BaseActivity.this).searchingGPS.dismiss();
                        }
                    }
                } else if (BaseActivity.this instanceof SpbActivity) {
                    if (((SpbActivity) BaseActivity.this).searchingGPS != null) {
                        if (((SpbActivity) BaseActivity.this).searchingGPS.isShown()) {
                            ((SpbActivity) BaseActivity.this).searchingGPS.dismiss();
                        }
                    }
                } else if (BaseActivity.this instanceof QcMutuAncakActivity) {
                    if (((QcMutuAncakActivity) BaseActivity.this).searchingGPS != null) {
                        if (((QcMutuAncakActivity) BaseActivity.this).searchingGPS.isShown()) {
                            ((QcMutuAncakActivity) BaseActivity.this).searchingGPS.dismiss();
                        }
                    }
                } else if (BaseActivity.this instanceof QcMutuBuahActivity) {
                    if (((QcMutuBuahActivity) BaseActivity.this).searchingGPS != null) {
                        if (((QcMutuBuahActivity) BaseActivity.this).searchingGPS.isShown()) {
                            ((QcMutuBuahActivity) BaseActivity.this).searchingGPS.dismiss();
                        }
                    }
                } else if (BaseActivity.this instanceof QcSensusBjrActivity) {
                    if (((QcSensusBjrActivity) BaseActivity.this).searchingGPS != null) {
                        if (((QcSensusBjrActivity) BaseActivity.this).searchingGPS.isShown()) {
                            ((QcSensusBjrActivity) BaseActivity.this).searchingGPS.dismiss();
                        }
                    }
                }
            }
//            if(searchingGPS!=null){
//                if(currentlocation.getAccuracy()>MAX_ACCURACY){
//                    if (!searchingGPS.isShown()) {
//                        searchingGPS = Snackbar.make(myCoordinatorLayout, getResources().getString(R.string.gps_not_accurate), Snackbar.LENGTH_INDEFINITE).setActionTextColor(Color.YELLOW);
//                        //Log.e("curr acc",location.getAccuracy()+" m");
//                        searchingGPS.show();
//                    }
//                }else{
//                    if (searchingGPS.isShown()) {
//                        searchingGPS.dismiss();
//                    }
//                }
//            }
            Log.v("change Location", currentlocation.getLongitude() + "," + currentlocation.getLatitude());
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
            Log.v("onStatusChanged L", s);
        }

        @Override
        public void onProviderEnabled(String s) {
            Log.v("onProviderEnabled L", s);
        }

        @Override
        public void onProviderDisabled(String s) {
            Log.v("onProviderDisabled L", s);
        }
    };

    public void starLocationUpdate() {
        // Request location updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTESTINTERVAL);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        currentlocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    }

    public void locationChangeListener(Location location){
        currentlocation = location;
        if (currentLocationOK()) {
            if (BaseActivity.this instanceof TphActivity) {
                if (((TphActivity) BaseActivity.this).searchingGPS != null) {
                    if (((TphActivity) BaseActivity.this).searchingGPS.isShown()) {
                        ((TphActivity) BaseActivity.this).searchingGPS.dismiss();
                    }
                }
            }
        }
        Log.v("change Location", currentlocation.getLongitude() + "," + currentlocation.getLatitude());
    }

    public boolean currentLocationOK(){
        if(currentlocation != null){
            if(currentlocation.getAccuracy() >= MAX_ACCURACY_CALIBRATION ){
                if(this instanceof EntryActivity){

                }else if(this instanceof ActiveActivity){

                }else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                seIndicatorAccuracy();
                                alertDialogBase = WidgetHelper.showCalibrationGpsDialog(BaseActivity.this);
                            } catch (WindowManager.BadTokenException e) {
                                Log.e("WindowManagerBad ", e.toString());
                            }
                        }
                    });
                }
                return false;
            }else if(currentlocation.getAccuracy() < MAX_ACCURACY){
                seIndicatorAccuracy();
                return true;
            }else if (this instanceof  SpbActivity && currentlocation.getAccuracy() < MAX_ACCURACY_CALIBRATION){
                return true;
            }
        }
        seIndicatorAccuracy();
        return false;
    }

    private void seIndicatorAccuracy(){
        if(viewIndicator != null){
            if(currentlocation == null){
                viewIndicator.setBackgroundColor(getResources().getColor(R.color.Red));
            }else if (currentlocation.getAccuracy() >= MAX_ACCURACY + 30) {
                viewIndicator.setBackgroundColor(getResources().getColor(R.color.Red));
            }else if (currentlocation.getAccuracy() >= MAX_ACCURACY) {
                viewIndicator.setBackgroundColor(getResources().getColor(R.color.Yellow));
            }else if (currentlocation.getAccuracy() < MAX_ACCURACY) {
                viewIndicator.setBackgroundColor(getResources().getColor(R.color.Green));
            }
        }
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        private AlertDialog alertDialogAllpoin ;
        TransaksiPanen transaksiPanen;
        TransaksiAngkut transaksiAngkut;
        TransaksiSPB transaksiSPB;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            transaksiPanen = null;
            transaksiAngkut = null;
            transaksiSPB = null;
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(BaseActivity.this,getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... strings) {
            switch (Integer.parseInt(strings[0])) {
                case LongOperation_CekDataPassing_Biru: {
                    transaksiPanen = TransaksiPanenHelper.toDecompre(objSeledted);
                    break;
                }
                case LongOperation_CekDataPassing_Hijau:{
                    transaksiAngkut = TransaksiAngkutHelper.toDecompre(objSeledted);
                    break;
                }
                case LongOperation_CekDataPassing_Red:{
                    transaksiSPB = TransaksiSpbHelper.toDecompre(objSeledted);
                    break;
                }
            }
            return strings[0];
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            switch (Integer.parseInt(result)) {
                case LongOperation_CekDataPassing_Biru:{
                    if(transaksiPanen != null) {
                        if (activitySelected instanceof  SpbActivity){
                            if(((SpbActivity) activitySelected).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof SpbInputFragment){
                                SpbInputFragment fragment = (SpbInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                fragment.addAngkut(transaksiPanen);
                            }
                        }else if (activitySelected instanceof  MekanisasiActivity){
                            if(((MekanisasiActivity) activitySelected).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof MekanisasiInputFragment){
                                MekanisasiInputFragment fragment = (MekanisasiInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                fragment.addAngkut(transaksiPanen);
                            }else {
                                Toast.makeText(HarvestApp.getContext(),"tap kartu bukan pada halaman ini",Toast.LENGTH_SHORT).show();
                            }
                        }else if (activitySelected instanceof  CountNfcActivity){
                            if(((CountNfcActivity) activitySelected).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof CountNfcInputFragment){
                                CountNfcInputFragment fragment = (CountNfcInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                fragment.addAngkut(transaksiPanen);
                            }
                        }
                        else if (activitySelected instanceof AngkutActivity) {
                            ((AngkutActivity) activitySelected).selectedTransaksiPanen = transaksiPanen;
                            ((AngkutActivity) activitySelected).getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_container, TphInputFragment.getInstance())
                                    .commit();
                        } else if (activitySelected instanceof MainMenuActivity){
                            Fragment fragment = ((MainMenuActivity.TabPagerAdapter)((MainMenuActivity) activitySelected).viewPager.getAdapter()).getItem(((MainMenuActivity) activitySelected).viewPager.getCurrentItem());
                            if (fragment instanceof TphFragment) {
                                String nfcId = GlobalHelper.bytesToHexString(((MainMenuActivity) activitySelected).myTag.getId());
                                if(nfcId != null){
                                    transaksiPanen.setIdNfc(nfcId);
                                }
                                Gson gson = new Gson();
                                Intent intent = new Intent(activitySelected, TphActivity.class);
                                intent.putExtra("transaksiTph", gson.toJson(transaksiPanen));
                                startActivityForResult(intent, GlobalHelper.RESULT_TPHACTIVITY);
                            }else if(fragment instanceof QcMutuBuahFragment){
                                QcMutuBuah qcMutuBuah = new QcMutuBuah();
                                qcMutuBuah.setIdTPanen(transaksiPanen.getIdTPanen());
                                qcMutuBuah.setTransaksiPanenKrani(transaksiPanen);

                                Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_QC_BUAH);
                                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("param1", transaksiPanen.getIdTPanen()));
                                if(cursor.size() > 0) {
                                    for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                                        DataNitrit dataNitrit = (DataNitrit) iterator.next();
                                        Gson gson = new Gson();
                                        qcMutuBuah = gson.fromJson(dataNitrit.getValueDataNitrit(),QcMutuBuah.class);
                                        break;
                                    }
                                }
                                db.close();

                                Gson gson = new Gson();
                                Intent intent = new Intent(activitySelected, QcMutuBuahActivity.class);
                                intent.putExtra("qcMutuBuah", gson.toJson(qcMutuBuah));
                                startActivityForResult(intent, GlobalHelper.RESULT_QC_MUTU_BUAH);
                            }else if (fragment instanceof QcSensusBjrFragment){
                                Gson gson = new Gson();
                                Intent intent = new Intent(activitySelected, QcSensusBjrActivity.class);
                                intent.putExtra("transaksiPanen", gson.toJson(transaksiPanen));
                                startActivityForResult(intent, GlobalHelper.RESULT_QC_SENSUS_BJR);
                            }else{
                                Toast.makeText(HarvestApp.getContext(),"Dimenu Ini Tidak Terima Kartu Panen",Toast.LENGTH_SHORT).show();
                            }


                        }else if (activitySelected instanceof QcMutuBuahActivity){



                        }else if (activitySelected instanceof QcSensusBjrActivity){
                            ((QcSensusBjrActivity) activitySelected).selectedTransaksiPanen = transaksiPanen;
                            ((QcSensusBjrActivity) activitySelected).getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_container, QcSensusBjrInputFragment.getInstance())
                                    .commit();
                        }
                    }else {
                        Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.data_not_valid) + " Transaksi", Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                case LongOperation_CekDataPassing_Hijau:{
                    if(transaksiAngkut != null) {
                        if (activitySelected instanceof  SpbActivity){
                            if(((SpbActivity) activitySelected).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof SpbInputFragment){
                                SpbInputFragment fragment = (SpbInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                fragment.addAngkut(transaksiAngkut);
                            }
                        }if (activitySelected instanceof  CountNfcActivity){
                            if(((CountNfcActivity) activitySelected).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof CountNfcInputFragment){
                                CountNfcInputFragment fragment = (CountNfcInputFragment) getSupportFragmentManager().findFragmentById(R.id.content_container);
                                fragment.addAngkut(transaksiAngkut);
                            }
                        }else if (activitySelected instanceof AngkutActivity) {
                            if(((AngkutActivity) activitySelected).selectedTransaksiAngkut != null){
                                if(((AngkutActivity) activitySelected).selectedTransaksiAngkut.getIdTAngkut().equals(transaksiAngkut.getIdTAngkut()) ){
                                    Toast.makeText(HarvestApp.getContext(),"Ini Kartu Yang Sama",Toast.LENGTH_SHORT).show();
                                }else{
                                    ((AngkutActivity) activitySelected).transaksiAngkutFromNFC = transaksiAngkut;
                                    ((AngkutActivity) activitySelected).getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.content_container, LangsiranInputFragment.getInstance())
                                            .commit();
                                }
                            }else{
                                ((AngkutActivity) activitySelected).transaksiAngkutFromNFC = transaksiAngkut;
                                ((AngkutActivity) activitySelected).getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_container, LangsiranInputFragment.getInstance())
                                        .commit();
                            }
                        } else if (activitySelected instanceof MainMenuActivity) {
                            Fragment fragment = ((MainMenuActivity.TabPagerAdapter)((MainMenuActivity) activitySelected).viewPager.getAdapter()).getItem(((MainMenuActivity) activitySelected).viewPager.getCurrentItem());
                            if (fragment instanceof AngkutFragment) {
                                Gson gson = new Gson();
                                Intent intent = new Intent(activitySelected, AngkutActivity.class);
                                transaksiAngkut.setStatus(TransaksiAngkut.UPLOAD);
                                intent.putExtra("transaksiAngkut", gson.toJson(transaksiAngkut));
                                startActivityForResult(intent, GlobalHelper.RESULT_ANGKUTACTIVITY);
                            }else{
                                Toast.makeText(HarvestApp.getContext(),"Dimenu Ini Tidak Terima Kartu Angkut",Toast.LENGTH_SHORT).show();
                            }
                        }else if (activitySelected instanceof QcMutuBuahActivity){
                            Toast.makeText(HarvestApp.getContext(),"NFC Hijau Tidak Bisa Di Scan Pada Tahap Ini",Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.data_not_valid) + " Transaksi", Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                case LongOperation_CekDataPassing_Red:{
                    if(transaksiSPB != null){
                        if (activitySelected instanceof  SpbActivity){
                            Toast.makeText(HarvestApp.getContext(),"SPB Tidak Bisa di Scan Pada Tahap Ini",Toast.LENGTH_SHORT).show();
                        }else if (activitySelected instanceof AngkutActivity) {
                            Toast.makeText(HarvestApp.getContext(),"SPB Tidak Bisa di Scan Pada Tahap Ini",Toast.LENGTH_SHORT).show();
                        }else if (activitySelected instanceof MainMenuActivity){
                            Gson gson = new Gson();
                            Intent intent = new Intent(activitySelected, SpbActivity.class);
                            transaksiSPB.setStatus(TransaksiSPB.UPLOAD);
                            intent.putExtra("transaksiSpb", gson.toJson(transaksiSPB));
                            startActivityForResult(intent,GlobalHelper.RESULT_SPBACTIVITY);
                        }else if (activitySelected instanceof QcMutuBuahActivity){
                            Toast.makeText(HarvestApp.getContext(),"NFC Hijau Tidak Bisa Di Scan Pada Tahap Ini",Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                }
            }

            if (alertDialogAllpoin != null) {
                alertDialogAllpoin.cancel();
            }
        }
    }
}
