package id.co.ams_plantation.harvestsap.ui;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.OpnameNfcAdapter;
import id.co.ams_plantation.harvestsap.model.OpnameNFC;
import id.co.ams_plantation.harvestsap.util.ActivityLoggerHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.NfcHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import jp.wasabeef.recyclerview.adapters.SlideInLeftAnimationAdapter;

/**
 * Created on : 24,October,2022
 * Author     : Afid
 */

public class OpnameNfcActivity extends BaseActivity {
    SegmentedButtonGroup segmentedButtonGroup;
    BetterSpinner bsAfdeling;
    ImageView iv_nfc_panen;
    ImageView iv_nfc_angkut;
    LinearLayout lSummary;
    TextView tvTotalOpname;
    TextView tvOpnameBelumUpload;
    String format;

    boolean bisaDaftar;
    AlertDialog alertDialog;
    String idNfc;
    android.support.v7.app.AlertDialog listrefrence;

    OpnameNFC opnameNFC;
    HashMap<String,OpnameNFC> allNfcOpname;
    int belumUpload = 0;
    int belumOpname = 0;

    public static final int LongOperation_InsertOpname = 1;
    public static final int LongOperation_GetAllOpname = 2;

    /****************************************** NFC ******************************************************************/
    Tag myTag;
    NfcAdapter nfcAdapter;
    PendingIntent pendingIntent;
    IntentFilter[] writeTagFilters;

    @Override
    protected void initPresenter() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.opnamenfc_layout);
        getSupportActionBar().hide();
        toolBarSetup();
        startTimeScreen = new Date();
//        segmentedButtonGroup = (SegmentedButtonGroup) findViewById(R.id.segmentedButtonGroup);
        lSummary = (LinearLayout) findViewById(R.id.lSummary);
        tvTotalOpname = (TextView) findViewById(R.id.tvTotalOpname);
        tvOpnameBelumUpload = (TextView) findViewById(R.id.tvOpnameBelumUpload);
        bsAfdeling = (BetterSpinner) findViewById(R.id.bsAfdeling);
        iv_nfc_panen = (ImageView) findViewById(R.id.iv_nfc_panen);
        iv_nfc_angkut = (ImageView) findViewById(R.id.iv_nfc_angkut);

        iv_nfc_angkut.setVisibility(View.GONE);
        format = GlobalHelper.FORMAT_NFC_PANEN;
        allNfcOpname = new HashMap<>();

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
        }
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[]{tagDetected};
        bisaDaftar = true;

        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.AFDELING_BLOCK, Context.MODE_PRIVATE);
        String afdelingObj = preferences.getString(HarvestApp.AFDELING_BLOCK,null);
        List<String> lAfd = new ArrayList<>();
        lAfd.add("Pilih Afdeling");
        try {
            JSONObject objAfdeling = new JSONObject(afdelingObj);
            if(objAfdeling.names() != null) {
                for (int i = 0; i < objAfdeling.names().length(); i++) {
                    lAfd.add(objAfdeling.names().get(i).toString());
                }
            }else{
                lAfd.add("AFD-1");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, lAfd);
        bsAfdeling.setAdapter(adapter);
        bsAfdeling.setText(lAfd.get(0));

        lSummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAllOpnameNfc();
            }
        });

        new LongOperation().execute(String.valueOf(LongOperation_GetAllOpname));
    }

    public void showAllOpnameNfc(){

        View view = LayoutInflater.from(this).inflate(R.layout.list_refrensi_object,null);
        TextView idHeader = (TextView) view.findViewById(R.id.idHeader);
        EditText etSearch_lso = (EditText) view.findViewById(R.id.et_search_lso);
        RecyclerView rv_usage_lso = (RecyclerView) view.findViewById(R.id.rv_usage_lso);

        idHeader.setText(this.getResources().getString(R.string.list_opname_nfc));
        rv_usage_lso.setLayoutManager(new LinearLayoutManager(this));

        OpnameNfcAdapter adapter = new OpnameNfcAdapter(this,new ArrayList<>(allNfcOpname.values()));
        SlideInLeftAnimationAdapter adapterLeft = new SlideInLeftAnimationAdapter(adapter);
        adapterLeft.setFirstOnly(false);

        adapterLeft.setDuration(400);
        etSearch_lso.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s.toString());
            }
        });

        rv_usage_lso.setAdapter(adapterLeft);
        listrefrence = WidgetHelper.showListReference(listrefrence,view,this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            Log.i("tagNFC", GlobalHelper.bytesToHexString(myTag.getId()));
            Log.i("tagNFC", Arrays.toString(myTag.getTechList()));
        }

        if(bsAfdeling.getText().toString().toLowerCase().equals("pilih afdeling")){
            Toast.makeText(HarvestApp.getContext(), "Mohon Pilih Afdeling Dahulu", Toast.LENGTH_SHORT).show();
            return;
        }

        String valueNFC = NfcHelper.readFromIntent(intent);
        String idX = GlobalHelper.bytesToHexString(myTag.getId());

        opnameNFC = new OpnameNFC(
                idX,
                GlobalHelper.getEstate().getEstCode(),
                bsAfdeling.getText().toString(),
                GlobalHelper.getUser().getUserID(),
                System.currentTimeMillis(),
                GlobalHelper.getUser().getUserID(),
                System.currentTimeMillis(),
                "Panen",
                OpnameNFC.SAVE
        );

        if(!bisaDaftar){
//            String idX = GlobalHelper.bytesToHexString(myTag.getId());
            if(!idNfc.equals(idX)){
                return;
            }else{
                bisaDaftar = true;
                alertDialog.dismiss();
            }
        }

        if(opnameNFC != null) {
            new LongOperation().execute(String.valueOf(LongOperation_InsertOpname));
        }
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        TextView textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backProses();
            }
        });
        textToolbar.setText(getResources().getString(R.string.format_nfc_opname));
    }

    public void backProses() {
        setResult(GlobalHelper.RESULT_OPNAMENFCACTIVITY);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause(){
        super.onPause();
        NfcHelper.WriteModeOff(nfcAdapter,this);
        endTimeScreen = new Date();
        if(GlobalHelper.getUser()!=null){
            ActivityLoggerHelper.recordScreenTime(OpnameNfcActivity.class.getSimpleName(),startTimeScreen, endTimeScreen);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        startTimeScreen = new Date();
        NfcHelper.WriteModeOn(nfcAdapter,this,pendingIntent,writeTagFilters);
    }

    @Override
    public void onBackPressed() {
        backProses();
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        private AlertDialog alertDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialog = WidgetHelper.showWaitingDialog(OpnameNfcActivity.this, getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... params) {
            switch (Integer.parseInt(params[0])) {
                case LongOperation_InsertOpname:
                    if (opnameNFC != null) {
                        int sebelum = allNfcOpname.size();
                        NfcHelper.insertOpnameNFC(opnameNFC);
                        allNfcOpname.put(opnameNFC.getUid(),opnameNFC);
                        if(sebelum < allNfcOpname.size()){
                            belumUpload++;
                        }
                        return String.valueOf(params[0]);
                    }
                    break;

                case LongOperation_GetAllOpname:
                    allNfcOpname = NfcHelper.showAllOpnameNfc();
                    belumUpload = 0 ;
                    if(allNfcOpname.size() > 0){
                        for (OpnameNFC opnameNFC : allNfcOpname.values()){
                            if(opnameNFC.getStatus() == OpnameNFC.SAVE){
                                belumUpload++;
                            }
                        }
                    }
                    break;

            }
            return String.valueOf(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            alertDialog.dismiss();
            switch (Integer.parseInt(result)) {
                case LongOperation_InsertOpname:
                    tvTotalOpname.setText(String.valueOf(allNfcOpname.size()) + " NFC");
                    tvOpnameBelumUpload.setText(String.valueOf(belumUpload) + " NFC");
                    Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_opname_success), Toast.LENGTH_SHORT).show();
                    break;
                case LongOperation_GetAllOpname:
                    tvOpnameBelumUpload.setText(String.valueOf(belumUpload) + " NFC");
                    tvTotalOpname.setText(String.valueOf(allNfcOpname.size()) + " NFC");
                    break;
            }
        }
    }
}
