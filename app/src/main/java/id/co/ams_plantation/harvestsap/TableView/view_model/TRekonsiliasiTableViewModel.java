package id.co.ams_plantation.harvestsap.TableView.view_model;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewColumnHeader;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;

public class TRekonsiliasiTableViewModel {
    private final Context mContext;
    private ArrayList<TransaksiAngkut> transaksiAngkuts;
    String [] HeaderColumn = {"Kendaraan","Tujuan","Dari","Sampai","Angkut","Kartu Rusak","Kartu Hilang"};
    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

    public TRekonsiliasiTableViewModel(Context context, ArrayList<TransaksiAngkut> hTAngkut) {
        mContext = context;
        transaksiAngkuts = new ArrayList<>();
        transaksiAngkuts = hTAngkut;
    }

    private List<TableViewRowHeader> getSimpleRowHeaderList() {
        List<TableViewRowHeader> list = new ArrayList<>();
        //"angkut"
        for (int i = 0; i < transaksiAngkuts.size(); i++) {
            String id = i + "-" + transaksiAngkuts.get(i).getIdTAngkut() + "-" + transaksiAngkuts.get(i).getStatus();
            TableViewRowHeader header = new TableViewRowHeader(id, transaksiAngkuts.get(i).getIdTAngkut());
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<TableViewColumnHeader> getRandomColumnHeaderList() {
        List<TableViewColumnHeader> list = new ArrayList<>();

        for (int i = 0; i < HeaderColumn.length; i++) {
            TableViewColumnHeader header = new TableViewColumnHeader(String.valueOf(i), HeaderColumn[i]);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<List<TableViewCell>> getCellListForSortingTest() {
        List<List<TableViewCell>> list = new ArrayList<>();
        for (int i = 0; i < transaksiAngkuts.size(); i++) {
            List<TableViewCell> cellList = new ArrayList<>();
            for (int j = 0; j < HeaderColumn.length; j++) {
                // Create dummy id.
                String id = j + "-" + transaksiAngkuts.get(i).getIdTAngkut() + "-" + transaksiAngkuts.get(i).getStatus();

                TableViewCell cell = null;
                switch (j){
                    case 0:{
//                        Vehicle vehicle = GlobalHelper.dataVehicle.get(transaksiAngkuts.get(i).getVehicle());
//                        cell = new TableViewCell(id, vehicle != null ? vehicle.getVehicleName() : transaksiAngkuts.get(i).getVehicle());
                        cell = new TableViewCell(id, transaksiAngkuts.get(i).getVehicle().getVehicleCode());
                        break;
                    }
                    case 1:{
//                        String s = transaksiAngkuts.get(i).getTujuan() == TransaksiAngkut.TUJUAN_PKS ? TransaksiAngkut.PKS :
//                                transaksiAngkuts.get(i).getTujuan() == TransaksiAngkut.TUJUAN_KELUAR ? TransaksiAngkut.KELAUR : TransaksiAngkut.LANGSIR;
                        String Tujuan = "";
                        if(transaksiAngkuts.get(i).getTujuan() == TransaksiAngkut.TUJUAN_PKS){
                            Tujuan = transaksiAngkuts.get(i).getPks().getNamaPKS();
                        }else if (transaksiAngkuts.get(i).getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR){
                            Tujuan = transaksiAngkuts.get(i).getLangsiran().getNamaTujuan();
                        }else if (transaksiAngkuts.get(i).getTujuan() == TransaksiAngkut.TUJUAN_KELUAR){
                            Tujuan = TransaksiAngkut.KELAUR;
                        }
                        cell = new TableViewCell(id,Tujuan);
                        break;
                    }
                    case 2:{
                        cell = new TableViewCell(id, dateFormat.format(transaksiAngkuts.get(i).getStartDate()));
                        break;
                    }
                    case 3:{
                        cell = new TableViewCell(id,  dateFormat.format(transaksiAngkuts.get(i).getEndDate()));
                        break;
                    }
                    case 4:{
//                        cell = new TableViewCell(id, String.valueOf(transaksiAngkuts.get(i).getAngkuts().size()));
//                        int angkutManual=0;
//                        for(Angkut angkut : transaksiAngkuts.get(i).getAngkuts()){
//                            if(angkut.getManualSPBData()!=null){
//                                angkutManual++;
//                            }
//                        }
                        cell = new TableViewCell(id, String.valueOf(transaksiAngkuts.get(i).getAngkuts().size()));
                        break;
                    }
                    case 5:{
//                        cell = new TableViewCell(id, String.format("%,d",transaksiAngkuts.get(i).getTotalJanjang()));
                        int kartuRusak=0;
                        for(Angkut angkut : transaksiAngkuts.get(i).getAngkuts()){
                           if(angkut.getManualSPBData()!=null){
                               if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                                   kartuRusak++;
                               }
                           }
                        }
                        cell = new TableViewCell(id, String.valueOf(kartuRusak));
                        break;
                    }
                    case 6:{
//                        cell = new TableViewCell(id, String.format("%,d",transaksiAngkuts.get(i).getTotalBrondolan()));
                        int kartuHilang=0;
                        for(Angkut angkut : transaksiAngkuts.get(i).getAngkuts()){
                            if(angkut.getManualSPBData()!=null){
                                if(angkut.getManualSPBData().getTipe().equals("Kartu Hilang")){
                                    kartuHilang++;
                                }
                            }
                        }
                        cell = new TableViewCell(id, String.valueOf(kartuHilang));
                        break;
                    }
                }
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }

    private static String getRandomString() {
        Random r = new Random();
        String str = " a ";
        for (int i = 0; i < r.nextInt(); i++) {
            str = str + " a ";
        }

        return str;
    }

    public List<List<TableViewCell>> getCellList() {
        return getCellListForSortingTest();
    }

    public List<TableViewRowHeader> getRowHeaderList() {
        return getSimpleRowHeaderList();
    }

    public List<TableViewColumnHeader> getColumnHeaderList() {
        return getRandomColumnHeaderList();
    }

}
