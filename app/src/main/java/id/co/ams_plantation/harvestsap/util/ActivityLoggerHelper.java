package id.co.ams_plantation.harvestsap.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.UserAppUsage;

public class ActivityLoggerHelper {
    private static final String TAG = ActivityLoggerHelper.class.getSimpleName();

    private String className="";
    private String elementId = "";

    public  static void recordScreenTime(String className, Date startTime, Date endTime){
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.ms");
            String connectionInfo = "";

            if(startTime == null || endTime == null){
                return;
            }

            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
            if(preferences.getBoolean(HarvestApp.CONNECTION_PREF,false)){
                connectionInfo = "Public";
            }else{
                connectionInfo = "Lokal";
            }

            UserAppUsage usage = SharedPrefHelper.getSharePrefUserAppUsage();

            if(GlobalHelper.getUser()!=null){
                UserAppUsage userAppUsage = new UserAppUsage(Integer.parseInt(GlobalHelper.getUser().getUserID()),
                        "HMS",
                        HarvestApp.getContext().getPackageName(),
                        UserAppUsage.SCREEN_TIME,
                        "Android",
                        className,
                        "",
                        connectionInfo,
                        GlobalHelper.getIPAddress(true),
                        sdf.format(startTime),
                        sdf.format(endTime),
                        "",
                        new Date(),
                        usage.getAndroidId(),
                        usage.getVersionApp(),
                        usage.getIdAndroid()
                );
                insertToLocal(userAppUsage);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void recordHitTime(String className, String elementId, Date startTime, Date endTime){
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.ms");
            String connectionInfo = "";
            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
            if(preferences.getBoolean(HarvestApp.CONNECTION_PREF,false)){
                connectionInfo = "Public";
            }else{
                connectionInfo = "Lokal";
            }

            UserAppUsage usage = SharedPrefHelper.getSharePrefUserAppUsage();

            if(GlobalHelper.getUser()!=null) {
                UserAppUsage userAppUsage = new UserAppUsage(Integer.parseInt(GlobalHelper.getUser().getUserID()),
                        "HMS",
                        HarvestApp.getContext().getPackageName(),
                        UserAppUsage.HIT,
                        "Android",
                        className,
                        elementId,
                        connectionInfo,
                        GlobalHelper.getIPAddress(true),
                        sdf.format(startTime),
                        sdf.format(endTime),
                        "",
                        new Date(),
                        usage.getAndroidId(),
                        usage.getVersionApp(),
                        usage.getIdAndroid()
                );
                insertToLocal(userAppUsage);
            }
        }catch (Exception e){

        }

    }

    public static void recordRequestData(String className, String elementId, Date startTime, Date endTime){
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.ms");
            String connectionInfo = "";
            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.CONNECTION_PREF, Context.MODE_PRIVATE);
            if(preferences.getBoolean(HarvestApp.CONNECTION_PREF,false)){
                connectionInfo = "Public";
            }else{
                connectionInfo = "Lokal";
            }

            UserAppUsage usage = SharedPrefHelper.getSharePrefUserAppUsage();

            if(GlobalHelper.getUser()!=null) {
                UserAppUsage userAppUsage = new UserAppUsage(Integer.parseInt(GlobalHelper.getUser().getUserID()),
                        "HMS",
                        HarvestApp.getContext().getPackageName(),
                        UserAppUsage.REQUEST_DATA,
                        "Android",
                        className,
                        elementId,
                        connectionInfo,
                        GlobalHelper.getIPAddress(true),
                        sdf.format(startTime),
                        sdf.format(endTime),
                        "",
                        new Date(),
                        usage.getAndroidId(),
                        usage.getVersionApp(),
                        usage.getIdAndroid()
                );
                insertToLocal(userAppUsage);
            }
        }catch (Exception e){

        }

    }

    private static void insertToLocal(UserAppUsage userAppUsage){
        Gson gson = new Gson();
        DataNitrit dataNitrit = new DataNitrit(String.valueOf(userAppUsage.getCreateDate()), gson.toJson(userAppUsage));
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ContainerLogActivity);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", dataNitrit.getIdDataNitrit()));
        if(cursor.size() > 0) {
            repository.update(dataNitrit);
            Log.d(TAG, "insertToLocalUAL: "+gson.toJson(userAppUsage));
        }else{
            repository.insert(dataNitrit);
            Log.d(TAG, "insertToLocalUAL: "+gson.toJson(userAppUsage));
        }
        db.close();
    }

    public static void clearDB(){
        File file = GlobalHelper.fileDbNitrit(GlobalHelper.TABLE_ContainerLogActivity);
        if(file.exists()){
            Log.d(TAG, "clearDB: berhasil hapus log activity");
            file.delete();
        }
    }
}
