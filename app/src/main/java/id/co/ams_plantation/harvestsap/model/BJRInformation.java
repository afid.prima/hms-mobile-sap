package id.co.ams_plantation.harvestsap.model;

public class BJRInformation {
    private String idBJRInformation;
    private String block;
    private String afdeling;
    private int tahunTanam;
    private double bjrAdjustment;
    private int bulan;
    private int tahun;

    public BJRInformation() {
    }

    public BJRInformation(String idBJRInformation, String block, String afdeling, int tahunTanam, double bjrAdjustment, int bulan, int tahun) {
        this.idBJRInformation = idBJRInformation;
        this.block = block;
        this.afdeling = afdeling;
        this.tahunTanam = tahunTanam;
        this.bjrAdjustment = bjrAdjustment;
        this.bulan = bulan;
        this.tahun = tahun;
    }

    public BJRInformation(String block, String afdeling, int tahunTanam, double bjrAdjustment, int bulan, int tahun) {
        this.idBJRInformation = tahun + "-"+ bulan+ "-"+ block+ "-"+ afdeling;
        this.block = block;
        this.afdeling = afdeling;
        this.tahunTanam = tahunTanam;
        this.bjrAdjustment = bjrAdjustment;
        this.bulan = bulan;
        this.tahun = tahun;
    }

    public String getIdBJRInformation() {
        return idBJRInformation;
    }

    public void setIdBJRInformation(String idBJRInformation) {
        this.idBJRInformation = idBJRInformation;
    }


    public void setIdBJRInformation(int tahun,int bulan,String block) {
        this.idBJRInformation = tahun + "-"+ bulan+ "-"+ block;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }

    public int getTahunTanam() {
        return tahunTanam;
    }

    public void setTahunTanam(int tahunTanam) {
        this.tahunTanam = tahunTanam;
    }

    public double getBjrAdjustment() {
        return bjrAdjustment;
    }

    public void setBjrAdjustment(double bjrAdjustment) {
        this.bjrAdjustment = bjrAdjustment;
    }

    public int getBulan() {
        return bulan;
    }

    public void setBulan(int bulan) {
        this.bulan = bulan;
    }

    public int getTahun() {
        return tahun;
    }

    public void setTahun(int tahun) {
        this.tahun = tahun;
    }
}
