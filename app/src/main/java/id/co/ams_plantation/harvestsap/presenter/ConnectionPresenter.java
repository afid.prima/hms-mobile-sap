package id.co.ams_plantation.harvestsap.presenter;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.connection.Tag;
import id.co.ams_plantation.harvestsap.model.LinkReport;
import id.co.ams_plantation.harvestsap.model.SyncTransaksi;
import id.co.ams_plantation.harvestsap.presenter.base.ApiPresenter;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.PathUrl;
import id.co.ams_plantation.harvestsap.util.SyncHistoryHelper;
import id.co.ams_plantation.harvestsap.view.ApiView;

public class ConnectionPresenter extends ApiPresenter {

    public ConnectionPresenter(ApiView view){attachView(view);}

    public void GetOpnameNFCByEstate(){
        get(PathUrl.GetOpnameNFCByEstate + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetOpnameNFCByEstate);
    }

    public void GetMasterUserByEstate(){
        get(PathUrl.GetMasterUserByEstate + GlobalHelper.getEstate().getEstCode()+ "&userID="+ GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetMasterUserByEstate);
    }

    public void GetPemanenListByEstate(){
        get(PathUrl.GetPemanenListByEstate + GlobalHelper.getEstate().getEstCode() + "&userID="+ GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetPemanenListByEstate);
    }
    public void GetOperatorListByEstate(){
        get(PathUrl.GetOperatorListByEstate + GlobalHelper.getEstate().getEstCode() + "&userID="+ GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetOperatorListByEstate);
    }

    public void GetVehicleListByEstate(){
        get(PathUrl.GetVehicleListByEstate + GlobalHelper.getEstate().getEstCode() + "&userID="+ GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetVehicleListByEstate);
    }

    public void GetPksListByEstate(){
        get(PathUrl.GetListPKSByEstate + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetListPKSByEstate);
    }

    public void GetTphListByEstate(){
        SyncTransaksi syncTransaksi = SyncHistoryHelper.getSyncTransaksi();
        if (syncTransaksi == null) {
            get(PathUrl.GetTphListByEstate + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetTphListByEstate);
        }else {
            get(PathUrl.GetTphListByEstate + GlobalHelper.getEstate().getEstCode() + "&afd=" + syncTransaksi.getAfdeling(), new HashMap<>(), Tag.GetTphListByEstate);
        }
    }

    public void GetLangsiranListByEstate(){
        get(PathUrl.GetLangsiranListByEstate + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetLangsiranListByEstate);
    }

    public void GetTransaksiPanenListByEstate() {
        SyncTransaksi syncTransaksi = SyncHistoryHelper.getSyncTransaksi();
        if (syncTransaksi == null) {
            get(PathUrl.GetTransaksiPanenListByEstate + GlobalHelper.getEstate().getEstCode(), new HashMap<>(), Tag.GetTransaksiPanenListByEstate);
        }else{
            get(PathUrl.GetTransaksiPanenListByEstate + GlobalHelper.getEstate().getEstCode() + "&afd="+syncTransaksi.getAfdeling(), new HashMap<>(), Tag.GetTransaksiPanenListByEstate);
        }
    }

    public void GetTransaksiAngkutListByEstate() {
        SyncTransaksi syncTransaksi = SyncHistoryHelper.getSyncTransaksi();
        if (syncTransaksi == null) {
            get(PathUrl.GetTransaksiAngkutListByEstate + GlobalHelper.getEstate().getEstCode(), new HashMap<>(), Tag.GetTransaksiAngkutListByEstate);
        } else {
            if (syncTransaksi.getUserId() == null) {
                get(PathUrl.GetTransaksiAngkutListByEstate + GlobalHelper.getEstate().getEstCode(), new HashMap<>(), Tag.GetTransaksiAngkutListByEstate);
            }else{
                get(PathUrl.GetTransaksiAngkutListByEstate + GlobalHelper.getEstate().getEstCode() + "&afd=" + syncTransaksi.getAfdeling() + "&userId=" + syncTransaksi.getUserId(), new HashMap<>(), Tag.GetTransaksiAngkutListByEstate);
            }
        }
    }

    public void GetTransaksiSpbListByEstate() {
        SyncTransaksi syncTransaksi = SyncHistoryHelper.getSyncTransaksi();
        if (syncTransaksi == null) {
            get(PathUrl.GetTransaksiSpbListByEstate + GlobalHelper.getEstate().getEstCode(), new HashMap<>(), Tag.GetTransaksiSpbListByEstate);
        }else {
            if (syncTransaksi.getUserId() == null) {
                get(PathUrl.GetTransaksiSpbListByEstate + GlobalHelper.getEstate().getEstCode(), new HashMap<>(), Tag.GetTransaksiSpbListByEstate);
            } else {
                get(PathUrl.GetTransaksiSpbListByEstate + GlobalHelper.getEstate().getEstCode() + "&afd=" + syncTransaksi.getAfdeling() + "&userId=" + syncTransaksi.getUserId(), new HashMap<>(), Tag.GetTransaksiSpbListByEstate);
            }
        }
    }

    public void GetQcBuahByEstate(){
        get(PathUrl.GetQCMutuBuahByEstate + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetQcBuahByEstate);
    }

    public void GetSensusBJRByEstate(){
        get(PathUrl.GetSensusBJRByEstate + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetSensusBJRByEstate);
    }

    public void GetQCMutuAncakByEstate(){
        get(PathUrl.GetQCMutuAncakByEstate + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetQCMutuAncakByEstate);
    }

    public void GetEstateAndAssistanceList(){
        get(PathUrl.GetEstateAndAssistanceList ,new HashMap<>(), Tag.GetEstateAndAssistanceList);
    }

    public void GetLatestRequestAssistensiByRequestUserId(){
        get(PathUrl.GetLatestRequestAssistensiByRequestUserId+ GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetLatestRequestAssistensiByRequestUserId);
    }

    public void GetLinkReport(String dateParam){
        get(PathUrl.GetLinkReport+ GlobalHelper.getEstate().getEstCode()+ "&dateParam="+ dateParam,new HashMap<>(), Tag.GetLinkReport);
    }

    public void UploadOpnameNFC(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.InsertOpnameNFC + GlobalHelper.getEstate().getEstCode(),bodyParameter,Tag.InsertOpnameNFC);
    }

    public void UploadPemanen(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostPemanen + GlobalHelper.getEstate().getEstCode(),bodyParameter,Tag.PostPemanen);
    }

    public void UploadImagesTPH(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostTphImage + GlobalHelper.getEstate().getEstCode(),bodyParameter,Tag.PostTphImage);
    }

    public void UploadTPH(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostTph + GlobalHelper.getEstate().getEstCode(),bodyParameter,Tag.PostTph);
    }

    public void UploadImagesLangsiran(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostLangsiranImage + GlobalHelper.getEstate().getEstCode(),bodyParameter,Tag.PostLangsiranImage);
    }

    public void UploadLangsiran(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostLangsiran + GlobalHelper.getEstate().getEstCode(),bodyParameter,Tag.PostLangsiran);
    }

    public void UploadImagesPanen(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostTransaksiPanenImage + GlobalHelper.getEstate().getEstCode(),bodyParameter,Tag.PostTransaksiPanenImage);
    }

    public void UploadTransaksiPanen(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostTransaksiPanen + GlobalHelper.getEstate().getEstCode(),bodyParameter,Tag.PostTransaksiPanen);
    }

    public void UploadTransaksiPanenWithSyncMaster(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostTransaksiPanen + GlobalHelper.getEstate().getEstCode()+ "&userId="+GlobalHelper.getUser().getUserID()+"&syncmaster=1",bodyParameter,Tag.PostTransaksiPanen);
    }

    public void UploadTransaksiPanenNFC(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostTransaksiPanenNFC + GlobalHelper.getEstate().getEstCode(),bodyParameter,Tag.PostTransaksiPanenNFC);
    }

    public void UploadImagesTransaksiAngkut(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostTransaksiAngkutImage + GlobalHelper.getEstate().getEstCode(),bodyParameter,Tag.PostTransaksiAngkutImage);
    }

    public void UploadTransaksiAngkut(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostTransaksiAngkut + GlobalHelper.getEstate().getEstCode(),bodyParameter,Tag.PostTransaksiAngkut);
    }

    public void UploadTransaksiAngkutNFC(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostTransaksiAngkutNFC + GlobalHelper.getEstate().getEstCode(),bodyParameter,Tag.PostTransaksiAngkutNFC);
    }

    public void UploadImagesSPB(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostTransaksiSPBImage + GlobalHelper.getEstate().getEstCode(),bodyParameter,Tag.PostTransaksiSPBImage);
    }

    public void UploadTransaksiSPB(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostTransaksiSPB + GlobalHelper.getEstate().getEstCode(),bodyParameter,Tag.PostTransaksiSPB);
    }

    public void UploadTransaksiSPBWithSyncMaster(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostTransaksiSPB + GlobalHelper.getEstate().getEstCode()+ "&userId="+GlobalHelper.getUser().getUserID()+"&syncmaster=1",bodyParameter,Tag.PostTransaksiSPB);
    }

    public void UploadQcBuahImages(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostQcBuahImages,bodyParameter,Tag.PostQcBuahImages);
    }

    public void UploadQcBuah(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostQcBuah ,bodyParameter,Tag.PostQcBuah);
    }

    public void UploadSensusBJRImages(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostSensusBJRImages,bodyParameter,Tag.PostSensusBjrImages);
    }

    public void UploadSensusBjr(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostSensusBJR ,bodyParameter,Tag.PostSensusBjr);
    }

    public void UploadQcAncak(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.PostQcAncak ,bodyParameter,Tag.PostQcAncak);
    }

    public void UploadAssistanceRequest(JSONArray bodyParameter) {
        postBodyRawArray(PathUrl.InsertAssistanceRequest + GlobalHelper.getUser().getUserID(),bodyParameter,Tag.InsertAssistanceRequest);
    }

    public void Upload_Supervision(JSONObject bodyParameter){
        postBodyRaw(PathUrl.PostSupervision ,bodyParameter,Tag.PostSupervisionPanen);
    }

    public void InsertGerdangDetail(JSONArray bodyParameter){
        postBodyRawArray(PathUrl.InsertGerdangDetail + GlobalHelper.getEstate().getEstCode() ,bodyParameter,Tag.InsertGerdangDetail);
    }

    public void UploadLogSPB(JSONArray bodyParameter){
        postBodyRawArray(PathUrl.UploadLogSPB + GlobalHelper.getEstate().getEstCode(),bodyParameter,Tag.UploadLogSPB);
    }

    public void UploadCrashReport(JSONArray bodyParameter){
        postBodyRawArrayAdmin(PathUrl.CrashReportList ,bodyParameter,Tag.CrashReportList);
    }

    public void UploadLogActivity(JSONArray bodyParameter){
        postBodyRawArrayAdmin(PathUrl.LogActivity ,bodyParameter,Tag.LogActivity);
    }

    public void GetTPanenCounterByUserID(){
        get(PathUrl.GetTPanenCounterByUserID + GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetTPanenCounterByUserID);
    }

    public void GetAfdelingAssistantByEstate(){
        get(PathUrl.GetAfdelingAssistantByEstate + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetAfdelingAssistantByEstate);
    }

    public void GetApplicationConfiguration(){
        get(PathUrl.GetApplicationConfiguration + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetApplicationConfiguration);
    }

    public void GetApplicationConfiguration2(){
        get2(PathUrl.GetApplicationConfiguration + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetApplicationConfiguration);
    }

    public void GetBJRInformation(){
        get(PathUrl.GetBJRInformation + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetBJRInformation);
    }

    public void GetBJRInformation2(){
        get2(PathUrl.GetBJRInformation + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetBJRInformation);
    }

    public void GetISCCInformation(){
        get(PathUrl.GetISCCInformation + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetISCCInformation);
    }

    public void GetConfigGerdangByEstate(){
        get(PathUrl.GetKonfigurasiGerdang + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetKonfigurasiGerdang);
    }

    public void GetTPHReasonUnharvestMaster(){
        get(PathUrl.GetTPHReasonUnharvestMaster + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetTPHReasonUnharvestMaster);
    }

    public void GetBlockMekanisasi(){
        get(PathUrl.GetBlockMekanisasi + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetBlockMekanisasi);
    }

    public void GetMasterSPKByEstate(){
        get(PathUrl.GetMasterSPKByEstate + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetMasterSPKByEstate);
    }

    public void GetMasterSPKByEstate2(){
        get2(PathUrl.GetMasterSPKByEstate + GlobalHelper.getEstate().getEstCode(),new HashMap<>(), Tag.GetMasterSPKByEstate);
    }

    public void GetMasterCagesByEstate(){
        get(PathUrl.GetMasterCagesByEstate + GlobalHelper.getEstate().getEstCode(), new HashMap<>(), Tag.GetMasterCagesByEstate);
    }

    public void GetMasterCagesByEstate2(){
        get2(PathUrl.GetMasterCagesByEstate + GlobalHelper.getEstate().getEstCode(), new HashMap<>(), Tag.GetMasterCagesByEstate);
    }

    public void GetSupervisionListByEstate(){
        get(PathUrl.GetSupervisionListByEstate + GlobalHelper.getEstate().getEstCode() + "&userId="+GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetSupervisionListByEstate);
    }

    public void GetSPBCounterByUserID(){
        get(PathUrl.GetSPBCounterByUserID + GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetSPBCounterByUserID);
    }


    public void GetTAngkutCounterByUserID(){
        get(PathUrl.GetTAngkutCounterByUserID + GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetTAngkutCounterByUserID);
    }

    public void GetTPHCounterByUserID(){
        get(PathUrl.GetTPHCounterByUserID + GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetTPHCounterByUserID);
    }

    public void GetLangsiranCounterByUserID(){
        get(PathUrl.GetLangsiranCounterByUserID + GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetLangsiranCounterByUserID);
    }

    public void GetTPanenSupervisionCounterByUserID(){
        get(PathUrl.GetTPanenSupervisionCounterByUserID + GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetTPanenSupervisionCounterByUserID);
    }

    public void GetQCMutuAncakHeaderCounterByUserID(){
        get(PathUrl.GetQCMutuAncakHeaderCounterByUserID + GlobalHelper.getUser().getUserID(),new HashMap<>(), Tag.GetQCMutuAncakHeaderCounterByUserID);
    }

    public String downloadReport(LinkReport filePdf){
        try {

            URL url = new URL(filePdf.getUrl());
            HttpURLConnection urlConnection = (HttpURLConnection) url
                    .openConnection();

            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);

            // connect
            urlConnection.connect();


            FileOutputStream fileOutput = new FileOutputStream(filePdf.getFullPath());

            // Stream used for reading the data from the internet
            InputStream inputStream = urlConnection.getInputStream();

            // this is the total size of the file which we are
            // downloading
//            totalsize = urlConnection.getContentLength();
//            setText("Starting PDF download...");

            // create a buffer...
            byte[] buffer = new byte[1024 * 1024];
            int bufferLength = 0;

            while ((bufferLength = inputStream.read(buffer)) > 0) {
                fileOutput.write(buffer, 0, bufferLength);
//                downloadedSize += bufferLength;
//                per = ((float) downloadedSize / totalsize) * 100;
//                setText("Total PDF File size  : "
//                        + (totalsize / 1024)
//                        + " KB\n\nDownloading PDF " + (int) per
//                        + "% complete");
            }
            // close the output stream when complete //
            fileOutput.close();
            return "ok";

        } catch (final MalformedURLException e) {
            return "Report Gagal Di Downlaod Coba Akses "+filePdf.getUrl() + " Dari Hp Anda";
        } catch (final IOException e) {
            return "Report Gagal Di Downlaod Coba Akses "+filePdf.getUrl() + " Dari Hp Anda";
        } catch (final Exception e) {
            return "Report Gagal Di Downlaod Coba Akses "+filePdf.getUrl() + " Dari Hp Anda";
        }
    }
}
