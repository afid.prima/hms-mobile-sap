package id.co.ams_plantation.harvestsap.model;

import java.util.ArrayList;

public class ReasonUnharvestReturn {
    ReasonUnharvestTPH reasonUnharvestTPH;
    ArrayList<ReasonUnharvestTPH> listReasonUnharvestTPH;

    public ReasonUnharvestReturn(ReasonUnharvestTPH reasonUnharvestTPH, ArrayList<ReasonUnharvestTPH> listReasonUnharvestTPH) {
        this.reasonUnharvestTPH = reasonUnharvestTPH;
        this.listReasonUnharvestTPH = listReasonUnharvestTPH;
    }

    public ReasonUnharvestTPH getReasonUnharvestTPH() {
        return reasonUnharvestTPH;
    }

    public void setReasonUnharvestTPH(ReasonUnharvestTPH reasonUnharvestTPH) {
        this.reasonUnharvestTPH = reasonUnharvestTPH;
    }

    public ArrayList<ReasonUnharvestTPH> getListReasonUnharvestTPH() {
        return listReasonUnharvestTPH;
    }

    public void setListReasonUnharvestTPH(ArrayList<ReasonUnharvestTPH> listReasonUnharvestTPH) {
        this.listReasonUnharvestTPH = listReasonUnharvestTPH;
    }
}
