package id.co.ams_plantation.harvestsap.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by user on 12/20/2018.
 */

public class Angkut {
    public static final String[] LIST_STATUS_KARTU = {"Kartu Rusak","Kartu Hilang"};
    public static final String MANUAL = "MANUAL";


    String idAngkut;
    long waktuAngkut;
    TransaksiPanen transaksiPanen;
    TransaksiAngkut transaksiAngkut;
    MekanisasiPanen mekanisasiPanen;
    double latitude;
    double longitude;
    int janjang;
    int brondolan;
    double berat;
    double bjr;
    String block;
    TPH tph;
    Langsiran langsiran;
    HashSet<String> foto;
    String oph;
    int idxAngkut;
    String idAngkutRef;
    ManualSPB manualSPBData;
    Set<String> idNfc;
    boolean hasBeenAngkut;
    int ophCount;

    public Angkut() {}

    public Angkut(String oph,String block,int janjang,int brondolan,double berat,int idxAngkut){
        this.oph = oph;
        this.block = block;
        this.janjang = janjang;
        this.brondolan = brondolan;
        this.berat = berat;
        this.idxAngkut = idxAngkut;
    }

    public Angkut(String idAngkut, long waktuAngkut) {
        this.idAngkut = idAngkut;
        this.waktuAngkut = waktuAngkut;
    }

    public Angkut(int janjang, int brondolan, String block,double berat) {
        this.janjang = janjang;
        this.brondolan = brondolan;
        this.block = block;
        this.berat = berat;
    }

    public Angkut(int janjang, int brondolan, double berat) {
        this.janjang = janjang;
        this.brondolan = brondolan;
        this.berat = berat;
    }

    public Angkut(String idAngkut, long waktuAngkut, TransaksiPanen transaksiPanen, TransaksiAngkut transaksiAngkut, double latitude, double longitude, int janjang, int brondolan, double bjr, double berat, String block, String oph, int idxAngkut, TPH tph, Langsiran langsiran, HashSet<String> foto) {
        this.idAngkut = idAngkut;
        this.waktuAngkut = waktuAngkut;
        this.transaksiPanen = transaksiPanen;
        this.transaksiAngkut = transaksiAngkut;
        this.latitude = latitude;
        this.longitude = longitude;
        this.janjang = janjang;
        this.brondolan = brondolan;
        this.bjr = bjr;
        this.berat = berat;
        this.block = block;
        this.oph = oph;
        this.idxAngkut = idxAngkut;
        this.tph = tph;
        this.langsiran = langsiran;
        this.foto = foto;
    }

    //tambahan zendi, angkut hanya input block dan tph
    public Angkut(String idAngkut, long waktuAngkut, TransaksiPanen transaksiPanen, TransaksiAngkut transaksiAngkut, double latitude, double longitude, String block, TPH tph, Langsiran langsiran, HashSet<String> foto) {
        this.idAngkut = idAngkut;
        this.waktuAngkut = waktuAngkut;
        this.transaksiPanen = transaksiPanen;
        this.transaksiAngkut = transaksiAngkut;
        this.latitude = latitude;
        this.longitude = longitude;
        this.block = block;
        this.tph = tph;
        this.langsiran = langsiran;
        this.foto = foto;
    }

    public Angkut(String idAngkut, long waktuAngkut, TransaksiPanen transaksiPanen, TransaksiAngkut transaksiAngkut, double latitude, double longitude, String block, TPH tph, Langsiran langsiran, HashSet<String> foto, ManualSPB manualSPB) {
        this.idAngkut = idAngkut;
        this.waktuAngkut = waktuAngkut;
        this.transaksiPanen = transaksiPanen;
        this.transaksiAngkut = transaksiAngkut;
        this.latitude = latitude;
        this.longitude = longitude;
        this.block = block;
        this.tph = tph;
        this.langsiran = langsiran;
        this.foto = foto;
        this.manualSPBData = manualSPB;
        this.oph = MANUAL;
    }

    public Angkut(String idAngkut, long waktuAngkut, TransaksiPanen transaksiPanen, TransaksiAngkut transaksiAngkut, double latitude, double longitude,String block, TPH tph, Langsiran langsiran, int janjangNormal, int brondolan,double bjr,double berat, HashSet<String> foto) {
        this.idAngkut = idAngkut;
        this.waktuAngkut = waktuAngkut;
        this.transaksiPanen = transaksiPanen;
        this.transaksiAngkut = transaksiAngkut;
        this.latitude = latitude;
        this.longitude = longitude;
        this.block = block;
        this.tph = tph;
        this.langsiran = langsiran;
        this.janjang = janjangNormal;
        this.brondolan = brondolan;
        this.bjr = bjr;
        this.berat = berat;
        this.foto = foto;
    }

    public Angkut(String idAngkut,long waktuAngkut, int janjang, int brondolan, double berat, double bjr, String block, String idAngkutRef,int ophCount, Set<String> idNfc,TransaksiPanen transaksiPanen,TransaksiAngkut transaksiAngkut,TPH tph,Langsiran langsiran,ManualSPB manualSPB) {
        this.idAngkut = idAngkut;
        this.waktuAngkut = waktuAngkut;
        this.janjang = janjang;
        this.brondolan = brondolan;
        this.berat = berat;
        this.bjr = bjr;
        this.block = block;
        this.idAngkutRef = idAngkutRef;
        this.ophCount = ophCount;
        this.idNfc = idNfc;
        this.transaksiPanen = transaksiPanen;
        this.transaksiAngkut = transaksiAngkut;
        this.tph = tph;
        this.langsiran = langsiran;
        this.manualSPBData = manualSPB;
    }

    public TransaksiAngkut getTransaksiAngkut() {
        return transaksiAngkut;
    }

    public void setTransaksiAngkut(TransaksiAngkut transaksiAngkut) {
        this.transaksiAngkut = transaksiAngkut;
    }

    public String getIdAngkut() {
        return idAngkut;
    }

    public void setIdAngkut(String idAngkut) {
        this.idAngkut = idAngkut;
    }

    public long getWaktuAngkut() {
        return waktuAngkut;
    }

    public void setWaktuAngkut(long waktuAngkut) {
        this.waktuAngkut = waktuAngkut;
    }

    public TransaksiPanen getTransaksiPanen() {
        return transaksiPanen;
    }

    public void setTransaksiPanen(TransaksiPanen transaksiPanen) {
        this.transaksiPanen = transaksiPanen;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getJanjang() {
        return janjang;
    }

    public void setJanjang(int janjang) {
        this.janjang = janjang;
    }

    public int getBrondolan() {
        return brondolan;
    }

    public void setBrondolan(int brondolan) {
        this.brondolan = brondolan;
    }

    public double getBerat() {
        return berat;
    }

    public void setBerat(double berat) {
        this.berat = berat;
    }

    public double getBjr() {
        return bjr;
    }

    public void setBjr(double bjr) {
        this.bjr = bjr;
    }

    public HashSet<String> getFoto() {
        return foto;
    }

    public void setFoto(HashSet<String> foto) {
        this.foto = foto;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public TPH getTph() {
        return tph;
    }

    public void setTph(TPH tph) {
        this.tph = tph;
    }

    public Langsiran getLangsiran() {
        return langsiran;
    }

    public void setLangsiran(Langsiran langsiran) {
        this.langsiran = langsiran;
    }

    public ManualSPB getManualSPBData() {
        return manualSPBData;
    }

    public void setManualSPBData(ManualSPB manualSPBData) {
        this.manualSPBData = manualSPBData;
    }

    public String getIdAngkutRef() {
        return idAngkutRef;
    }

    public void setIdAngkutRef(String idAngkutRef) {
        this.idAngkutRef = idAngkutRef;
    }

    public String getOph() {
        return oph;
    }

    public void setOph(String oph) {
        this.oph = oph;
    }

    public int getIdxAngkut() {
        return idxAngkut;
    }

    public void setIdxAngkut(int idxAngkut) {
        this.idxAngkut = idxAngkut;
    }

    public Set<String> getIdNfc() {
        return idNfc;
    }

    public void setIdNfc(Set<String> idNfc) {
        this.idNfc = idNfc;
    }

    public boolean isHasBeenAngkut() {
        return hasBeenAngkut;
    }

    public void setHasBeenAngkut(boolean hasBeenAngkut) {
        this.hasBeenAngkut = hasBeenAngkut;
    }

    public int getOphCount() {
        return ophCount;
    }

    public void setOphCount(int ophCount) {
        this.ophCount = ophCount;
    }
}
