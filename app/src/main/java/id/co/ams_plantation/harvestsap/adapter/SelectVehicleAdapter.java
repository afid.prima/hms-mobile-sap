package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.Vehicle;

public class SelectVehicleAdapter extends ArrayAdapter<Vehicle> {
    public ArrayList<Vehicle> items;
    public ArrayList<Vehicle> itemsAll;
    public ArrayList<Vehicle> suggestions;
    private final int viewResourceId;

    public SelectVehicleAdapter(Context context, int viewResourceId, ArrayList<Vehicle> data){
        super(context, viewResourceId, data);
        this.items = new ArrayList<>();
        this.items.addAll(data);
        this.itemsAll = (ArrayList<Vehicle>)this.items.clone();
        this.suggestions = new ArrayList<>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        if(items.size()>position){
            Vehicle vehicle = items.get(position);
            if (vehicle != null) {
                ImageView iv = (ImageView) v.findViewById(R.id.iv);
                TextView tv = (TextView) v.findViewById(R.id.tv);
                TextView tv1 = (TextView) v.findViewById(R.id.tv1);
                iv.setVisibility(View.GONE);
                tv.setText(vehicle.getVraOrderNumber() != null ? vehicle.getVraOrderNumber() : "" );
                String ket = vehicle.getVehicleName() != null ? vehicle.getVehicleName() + "\n" : "";
                ket += vehicle.getEstCodeSAP() != null ? vehicle.getEstCodeSAP().length() > 4 ? vehicle.getEstCodeSAP().substring(vehicle.getEstCodeSAP().length() - 4) : "" : "";
                tv1.setText(ket);
            }
        }
        return v;
    }

    public Vehicle getItemAt(int position){
        return items.get(position);
    }

    @Override
    public int getCount(){
        return items!=null?items.size():0;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((Vehicle)(resultValue)).getVehicleCode();
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (Vehicle Vehicle : itemsAll) {
                    boolean masuk = false;
                    if(Vehicle.getVehicleName() != null){
                        if(Vehicle.getVehicleName().toLowerCase().contains(constraint.toString().toLowerCase())){
                            masuk = true;
                        }
                    }
                    if(Vehicle.getVraOrderNumber() != null){
                        if (Vehicle.getVraOrderNumber().toLowerCase().contains(constraint.toString().toLowerCase())){
                            masuk = true;
                        }
                    }

                    if(masuk){
                        suggestions.add(Vehicle);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Vehicle> filteredList = (ArrayList<Vehicle>) results.values;
//            clear();
            items.clear();
//            itemsAll.clear();
            if(results != null && results.count > 0) {
//                addAll(filteredList);
//                itemsAll.addAll(filteredList);
//                for (Anggota anggota : filteredList){
//                    items.add(anggota);
//                }
                items.addAll(filteredList);
            }else{
//                clear();
                items.clear();
//                itemsAll.clear();
            }
            notifyDataSetChanged();
        }
    };
}
