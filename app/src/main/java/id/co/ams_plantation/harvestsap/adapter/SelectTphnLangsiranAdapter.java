package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.TPHnLangsiran;

public class SelectTphnLangsiranAdapter extends ArrayAdapter<TPHnLangsiran> {
    public ArrayList<TPHnLangsiran> items;
    public ArrayList<TPHnLangsiran> itemsAll;
    public ArrayList<TPHnLangsiran> suggestions;
    private final int viewResourceId;

    public SelectTphnLangsiranAdapter(Context context, int viewResourceId, ArrayList<TPHnLangsiran> data){
        super(context, viewResourceId, data);
        this.items = new ArrayList<TPHnLangsiran>();
        this.items.addAll(data);
        this.itemsAll = (ArrayList<TPHnLangsiran>)this.items.clone();
        this.suggestions = new ArrayList<>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        if(items.size()>position){
            TPHnLangsiran tph = items.get(position);
            if (tph != null) {
                View llStatus = (View) v.findViewById(R.id.llStatus);
                RelativeLayout rlImage = (RelativeLayout) v.findViewById(R.id.rlImage);
                TextView item_ket = (TextView) v.findViewById(R.id.item_ket);
                TextView item_ket2 = (TextView) v.findViewById(R.id.item_ket2);
                TextView item_ket1 = (TextView) v.findViewById(R.id.item_ket1);
                TextView item_ket3 = (TextView) v.findViewById(R.id.item_ket3);
                rlImage.setVisibility(View.GONE);
                if(tph.getTph() != null){
                    llStatus.setBackgroundColor(TPHnLangsiran.COLOR_TPH);
                    item_ket.setText(tph.getTph().getNamaTph());
                    item_ket2.setText(tph.getTph().getAncak());
                    item_ket1.setText(tph.getTph().getBlock());
                    item_ket3.setText("TPH");
                }else if (tph.getLangsiran() != null){
                    llStatus.setBackgroundColor(TPHnLangsiran.COLOR_LANGSIRAN);
                    item_ket.setText(tph.getLangsiran().getNamaTujuan());
                    item_ket2.setVisibility(View.GONE);
                    item_ket1.setText(tph.getLangsiran().getBlock());
                    item_ket3.setText("Langsiran");
                }
            }
        }
        return v;
    }

    public TPHnLangsiran getItemAt(int position){
        return items.get(position);
    }

    @Override
    public int getCount(){
        return items!=null?items.size():0;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            TPHnLangsiran tpHnLangsiran = ((TPHnLangsiran)(resultValue));
            String str = "";
            if(tpHnLangsiran.getTph() != null){
                str = tpHnLangsiran.getTph().getNamaTph();
            }else if (tpHnLangsiran.getLangsiran() != null){
                str = tpHnLangsiran.getLangsiran().getNamaTujuan();
            }
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (TPHnLangsiran tph : itemsAll) {
                    if(tph.getTph() != null){
                        if(tph.getTph().getNamaTph().toLowerCase().contains(constraint.toString().toLowerCase())){
                            suggestions.add(tph);
                        }
                    }else if (tph.getLangsiran() != null){
                        if(tph.getLangsiran().getNamaTujuan().toLowerCase().contains(constraint.toString().toLowerCase())){
                            suggestions.add(tph);
                        }
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<TPHnLangsiran> filteredList = (ArrayList<TPHnLangsiran>) results.values;
//            clear();
            items.clear();
//            itemsAll.clear();
            if(results != null && results.count > 0) {
//                addAll(filteredList);
//                itemsAll.addAll(filteredList);
//                for (Anggota anggota : filteredList){
//                    items.add(anggota);
//                }
                items.addAll(filteredList);
            }else{
//                clear();
                items.clear();
//                itemsAll.clear();
            }
            notifyDataSetChanged();
        }
    };
}
