package id.co.ams_plantation.harvestsap.model;

import java.util.Date;

/**
 * Created on : 05,December,2022
 * Author     : Afid
 */

public class GerdangConfig {
    String estCode;
    long dateStart;
    long dateEnd;

    public GerdangConfig() {
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public long getDateStart() {
        return dateStart;
    }

    public void setDateStart(long dateStart) {
        this.dateStart = dateStart;
    }

    public long getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(long dateEnd) {
        this.dateEnd = dateEnd;
    }
}
