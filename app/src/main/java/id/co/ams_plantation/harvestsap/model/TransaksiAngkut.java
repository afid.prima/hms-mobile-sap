package id.co.ams_plantation.harvestsap.model;

import java.util.ArrayList;
import java.util.HashSet;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;

/**
 * Created by user on 12/3/2018.
 */

public class TransaksiAngkut {

    // note transaksi angkut create by adalah supir atau pic yang angkut

    public static final int COLOR_FROM_ANGKUT = HarvestApp.getContext().getResources().getColor(R.color.DeepSkyBlue);
    public static final int COLOR_FROM_TRANSIT = HarvestApp.getContext().getResources().getColor(R.color.DarkSeaGreen);
    public static final int COLOR_FROM_MANUAL = HarvestApp.getContext().getResources().getColor(R.color.Yellow);
    public static final int COLOR_FROM_MANUAL_V2 = HarvestApp.getContext().getResources().getColor(R.color.Red);

    public static final int TUJUAN_PKS= 0;
    public static final int TUJUAN_LANGSIR= 1;
    public static final int TUJUAN_KELUAR = 2;

    public static final String PKS = "PKS";
    public static final String LANGSIR = "LANGSIRAN";
    public static final String KELAUR = "EXTERNAL";

    public static final int KENDARAAN_KEBUN = 0;
    public static final int KENDARAAN_LUAR = 1;

    public static final String KEBUN = "SENDIRI";
    public static final String LUAR = "KONTRAK";

    public static final String MASTER = "MASTER";
    public static final String FREE_TEXT = "FREE TEXT";

    public static final String KENDARAAN_CODE_KEBUN = "INT";
    public static final String KENDARAAN_CODE_LUAR = "EXT";

    public static final String [] KENDARAAN_CODE = {"I","E"};

    public static final int TAP = 0;
    public static final int UPLOAD = 1;
    public static final int HAPUS = 2;
    public static final int SPB = 3;
    public static final int UPLOAD_HAPUS = -2;

    public static final int COLOR_TAP = HarvestApp.getContext().getResources().getColor(R.color.DeepSkyBlue);
    public static final int COLOR_UPLOAD = HarvestApp.getContext().getResources().getColor(R.color.Yellow);
    public static final int COLOR_SPB = HarvestApp.getContext().getResources().getColor(R.color.Green);

    String idTAngkut;
    int kendaraanDari;
    Operator supir;
    String createBy;
    Long startDate;
    Long endDate;
    int totalJanjang;
    int totalBrondolan;
    double totalBeratEstimasi;
    double latitudeTujuan;
    double longitudeTujuan;
    String estCode;
    Vehicle vehicle;
    int tujuan;
    PKS pks;
    Langsiran langsiran;
    String bin;
    int status;
    ArrayList<Angkut> angkuts;
    String idSPB;
    String approveBy;
    String idNfc;
    SupervisionList substitute;
    SPK spk;
    boolean isManual=false;
    HashSet<String> foto;
    boolean fullAngkut;
    ArrayList<Operator> loader;
    ArrayList<Cages> cages;
    ArrayList<PengirimanGanda> pengirimanGandas;
    ArrayList<SubTotalAngkut> subTotalAngkuts;
    ArrayList<TranskasiAngkutVersion> transkasiAngkutVersions;
    ArrayList<MekanisasiPanen> mekanisasiPanens;
    boolean revisi;

    public TransaksiAngkut(){}

    public TransaksiAngkut(String idTAngkut, String createBy, Long startDate, Long endDate, int totalJanjang, int totalBrondolan, double totalBeratEstimasi, double latitudeTujuan, double longitudeTujuan, String estCode, int kendaraanDari, Operator supir, Vehicle vehicleCode, int tujuan, PKS pks, Langsiran langsiran, SPK spk, String bin, int status, ArrayList<Angkut> angkuts, ArrayList<Operator> loader, ArrayList<Cages> cages, ArrayList<PengirimanGanda> pengirimanGandas, HashSet<String> foto) {
        this.idTAngkut = idTAngkut;
        this.createBy = createBy;
        this.startDate = startDate;
        this.endDate = endDate;
        this.totalJanjang = totalJanjang;
        this.totalBrondolan = totalBrondolan;
        this.totalBeratEstimasi = totalBeratEstimasi;
        this.latitudeTujuan = latitudeTujuan;
        this.longitudeTujuan = longitudeTujuan;
        this.estCode = estCode;
        this.kendaraanDari = kendaraanDari;
        this.supir = supir;
        this.vehicle = vehicleCode;
        this.tujuan = tujuan;
        this.pks= pks;
        this.langsiran = langsiran;
        this.spk = spk;
        this.bin = bin;
        this.status = status;
        this.loader  = loader;
        this.cages = cages;
        this.pengirimanGandas = pengirimanGandas;
        this.angkuts = angkuts;
        this.foto = foto;
    }

    public TransaksiAngkut(int totalJanjang, int totalBrondolan, double totalBeratEstimasi, ArrayList<Angkut> angkuts) {
        this.totalJanjang = totalJanjang;
        this.totalBrondolan = totalBrondolan;
        this.totalBeratEstimasi = totalBeratEstimasi;
        this.angkuts = angkuts;
    }

    public ArrayList<SubTotalAngkut> getSubTotalAngkuts() {
        return subTotalAngkuts;
    }

    public void setSubTotalAngkuts(ArrayList<SubTotalAngkut> subTotalAngkuts) {
        this.subTotalAngkuts = subTotalAngkuts;
    }

    public ArrayList<TranskasiAngkutVersion> getTranskasiAngkutVersions() {
        return transkasiAngkutVersions;
    }

    public void setTranskasiAngkutVersions(ArrayList<TranskasiAngkutVersion> transkasiAngkutVersions) {
        this.transkasiAngkutVersions = transkasiAngkutVersions;
    }

    public PKS getPks() {
        return pks;
    }

    public void setPks(PKS pks) {
        this.pks = pks;
    }

    public Langsiran getLangsiran() {
        return langsiran;
    }

    public void setLangsiran(Langsiran langsiran) {
        this.langsiran = langsiran;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getEstCode() {
        return estCode;
    }

    public void setEstCode(String estCode) {
        this.estCode = estCode;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicleCode) {
        this.vehicle = vehicleCode;
    }

    public int getTotalJanjang() {
        return totalJanjang;
    }

    public void setTotalJanjang(int totalJanjang) {
        this.totalJanjang = totalJanjang;
    }

    public int getTotalBrondolan() {
        return totalBrondolan;
    }

    public void setTotalBrondolan(int totalBrondolan) {
        this.totalBrondolan = totalBrondolan;
    }

    public String getIdTAngkut() {
        return idTAngkut;
    }

    public void setIdTAngkut(String idTAngkut) {
        this.idTAngkut = idTAngkut;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public double getLatitudeTujuan() {
        return latitudeTujuan;
    }

    public void setLatitudeTujuan(double latitudeTujuan) {
        this.latitudeTujuan = latitudeTujuan;
    }

    public double getLongitudeTujuan() {
        return longitudeTujuan;
    }

    public void setLongitudeTujuan(double longitudeTujuan) {
        this.longitudeTujuan = longitudeTujuan;
    }

    public int getTujuan() {
        return tujuan;
    }

    public void setTujuan(int tujuan) {
        this.tujuan = tujuan;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public ArrayList<Angkut> getAngkuts() {
        return angkuts;
    }

    public void setAngkuts(ArrayList<Angkut> angkuts) {
        this.angkuts = angkuts;
    }

    public String getIdSPB() {
        return idSPB;
    }

    public void setIdSPB(String idSPB) {
        this.idSPB = idSPB;
    }

    public String getApproveBy() {
        return approveBy;
    }

    public void setApproveBy(String approveBy) {
        this.approveBy = approveBy;
    }

    public int getKendaraanDari() {
        return kendaraanDari;
    }

    public void setKendaraanDari(int kendaraanDari) {
        this.kendaraanDari = kendaraanDari;
    }

    public Operator getSupir() {
        return supir;
    }

    public void setSupir(Operator supir) {
        this.supir = supir;
    }

    public String getIdNfc() {
        return idNfc;
    }

    public void setIdNfc(String idNfc) {
        this.idNfc = idNfc;
    }

    public double getTotalBeratEstimasi() {
        return totalBeratEstimasi;
    }

    public void setTotalBeratEstimasi(double totalBeratEstimasi) {
        this.totalBeratEstimasi = totalBeratEstimasi;
    }

    public boolean isManual() {
        return isManual;
    }

    public void setManual(boolean manual) {
        isManual = manual;
    }

    public SupervisionList getSubstitute() {
        return substitute;
    }

    public void setSubstitute(SupervisionList substitute) {
        this.substitute = substitute;
    }

    public ArrayList<Operator> getLoader() {
        return loader;
    }

    public ArrayList<Cages> getCages() {
        return cages;
    }

    public void setCages(ArrayList<Cages> cages) {
        this.cages = cages;
    }

    public void setLoader(ArrayList<Operator> loader) {
        this.loader = loader;
    }

    public ArrayList<PengirimanGanda> getPengirimanGandas() {
        return pengirimanGandas;
    }

    public void setPengirimanGandas(ArrayList<PengirimanGanda> pengirimanGandas) {
        this.pengirimanGandas = pengirimanGandas;
    }

    public HashSet<String> getFoto() {
        return foto;
    }

    public void setFoto(HashSet<String> foto) {
        this.foto = foto;
    }

    public boolean isFullAngkut() {
        return fullAngkut;
    }

    public void setFullAngkut(boolean fullAngkut) {
        this.fullAngkut = fullAngkut;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public SPK getSpk() {
        return spk;
    }

    public void setSpk(SPK spk) {
        this.spk = spk;
    }

    public ArrayList<MekanisasiPanen> getMekanisasiPanens() {
        return mekanisasiPanens;
    }

    public void setMekanisasiPanens(ArrayList<MekanisasiPanen> mekanisasiPanens) {
        this.mekanisasiPanens = mekanisasiPanens;
    }

    public boolean isRevisi() {
        return revisi;
    }

    public void setRevisi(boolean revisi) {
        this.revisi = revisi;
    }
}
