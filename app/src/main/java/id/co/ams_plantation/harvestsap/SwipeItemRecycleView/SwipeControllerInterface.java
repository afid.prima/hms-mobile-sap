package id.co.ams_plantation.harvestsap.SwipeItemRecycleView;

public interface SwipeControllerInterface {
    void onItemMove(int fromPosition, int toPosition);
}
