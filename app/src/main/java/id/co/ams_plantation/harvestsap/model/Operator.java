package id.co.ams_plantation.harvestsap.model;

public class Operator {
    private String kodeOperator;
    private String nik;
    private String nama;
    private String gank;
    private String area;
    private String estate;
    private String afdeling;
    private String estCodeSAP;

    public String getKodeOperator() {
        return kodeOperator;
    }

    public void setKodeOperator(String kodeOperator) {
        this.kodeOperator = kodeOperator;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGank() {
        return gank;
    }

    public void setGank(String gank) {
        this.gank = gank;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }

    public String getEstCodeSAP() {
        return estCodeSAP;
    }

    public void setEstCodeSAP(String estCodeSAP) {
        this.estCodeSAP = estCodeSAP;
    }
}


