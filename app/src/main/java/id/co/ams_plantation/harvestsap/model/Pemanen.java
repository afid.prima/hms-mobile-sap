package id.co.ams_plantation.harvestsap.model;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;

/**
 * Created by user on 12/3/2018.
 */

public class Pemanen {
    String kodePemanen;
    String nik;
    String nama;
    String gank;
    String area;
    String estate;
    String afdeling;
    String kemandoran;
    String estCodeSAP;
    int noUrut;
    int status;

    public static int Pemanen_Upload = 0;
    public static int Pemanen_NotUplaod = 1;

    public static final int COLOR_DEFIND = HarvestApp.getContext().getResources().getColor(R.color.Red);
    public static final int COLOR_UNDEFIND = HarvestApp.getContext().getResources().getColor(R.color.Yellow);

    public Pemanen() {
    }

    public Pemanen(String kodePemanen, String nama) {
        this.kodePemanen = kodePemanen;
        this.nama = nama;
    }

    public Pemanen(String kodePemanen, String nik, String nama, String gank, String area, String estate, String afdeling) {
        this.kodePemanen = kodePemanen;
        this.nik = nik;
        this.nama = nama;
        this.gank = gank;
        this.area = area;
        this.estate = estate;
        this.afdeling = afdeling;
    }

    public Pemanen(String kemandoran, int noUrut) {
        this.kemandoran = kemandoran;
        this.noUrut = noUrut;
    }

    public String getKodePemanen() {
        return kodePemanen;
    }

    public void setKodePemanen(String kodePemanen) {
        this.kodePemanen = kodePemanen;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGank() {
        return gank;
    }

    public void setGank(String gank) {
        this.gank = gank;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEstate() {
        return estate;
    }

    public void setEstate(String estate) {
        this.estate = estate;
    }

    public String getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(String afdeling) {
        this.afdeling = afdeling;
    }

    public String getKemandoran() {
        return kemandoran;
    }

    public void setKemandoran(String kemandoran) {
        this.kemandoran = kemandoran;
    }

    public int getNoUrut() {
        return noUrut;
    }

    public void setNoUrut(int noUrut) {
        this.noUrut = noUrut;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getEstCodeSAP() {
        return estCodeSAP;
    }

    public void setEstCodeSAP(String estCodeSAP) {
        this.estCodeSAP = estCodeSAP;
    }
}
