package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.TPH;
import id.co.ams_plantation.harvestsap.model.TPHnLangsiran;

public class SelectTphMekanisasiAdapter extends ArrayAdapter<TPH> {
    public ArrayList<TPH> items;
    public ArrayList<TPH> itemsAll;
    public ArrayList<TPH> suggestions;
    private final int viewResourceId;

    public SelectTphMekanisasiAdapter(Context context, int viewResourceId, ArrayList<TPH> data) {
        super(context, viewResourceId, data);
        this.items = new ArrayList<TPH>();
        this.items.addAll(data);
        this.itemsAll = (ArrayList<TPH>) this.items.clone();
        this.suggestions = new ArrayList<>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        if (items.size() > position) {
            TPH tph = items.get(position);
            if (tph != null) {
                View llStatus = (View) v.findViewById(R.id.llStatus);
                RelativeLayout rlImage = (RelativeLayout) v.findViewById(R.id.rlImage);
                TextView item_ket = (TextView) v.findViewById(R.id.item_ket);
                TextView item_ket2 = (TextView) v.findViewById(R.id.item_ket2);
                TextView item_ket1 = (TextView) v.findViewById(R.id.item_ket1);
                TextView item_ket3 = (TextView) v.findViewById(R.id.item_ket3);
                rlImage.setVisibility(View.GONE);

                llStatus.setBackgroundColor(TPHnLangsiran.COLOR_TPH);
                item_ket.setText(tph.getNamaTph());
                item_ket2.setText(tph.getAncak());
                item_ket1.setText(tph.getBlock());
                item_ket3.setText("TPH");
            }
        }
        return v;
    }

    public TPH getItemAt(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            TPH tph = ((TPH) (resultValue));
            String str = "";
            if (tph != null) {
                str = tph.getNamaTph();
            }
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (TPH tph : itemsAll) {
                    if (tph != null) {
                        if (tph.getNamaTph().toLowerCase().contains(constraint.toString().toLowerCase())) {
                            suggestions.add(tph);
                        } else if (tph.getAncak().toLowerCase().contains(constraint.toString().toLowerCase())) {
                            suggestions.add(tph);
                        }
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<TPH> filteredList = (ArrayList<TPH>) results.values;
            items.clear();
            if (results != null && results.count > 0) {
                items.addAll(filteredList);
            } else {
                items.clear();
            }
            notifyDataSetChanged();
        }
    };
}