package id.co.ams_plantation.harvestsap.model;

import java.util.ArrayList;

public class Kongsi {
    public static final int idNone = 0;
    public static final int idKelompok = 1;
    public static final int idMekanis = 2;

    public static final String [] arrayTipeKongsi = {
            "Tidak Kongsi",
            "Kelompok",
            "Mekanis"
    };

    int tipeKongsi;
    ArrayList<SubPemanen> subPemanens;
    Pemanen pemanenGerdang;

    public Kongsi(int tipeKongsi, ArrayList<SubPemanen> subPemanens) {
        this.tipeKongsi = tipeKongsi;
        this.subPemanens = subPemanens;
    }

    public int getTipeKongsi() {
        return tipeKongsi;
    }

    public void setTipeKongsi(int tipeKongsi) {
        this.tipeKongsi = tipeKongsi;
    }

    public ArrayList<SubPemanen> getSubPemanens() {
        return subPemanens;
    }

    public void setSubPemanens(ArrayList<SubPemanen> subPemanens) {
        this.subPemanens = subPemanens;
    }

    public Pemanen getPemanenGerdang() {
        return pemanenGerdang;
    }

    public void setPemanenGerdang(Pemanen pemanenGerdang) {
        this.pemanenGerdang = pemanenGerdang;
    }
}
