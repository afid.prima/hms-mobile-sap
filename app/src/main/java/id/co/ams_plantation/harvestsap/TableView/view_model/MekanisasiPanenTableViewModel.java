package id.co.ams_plantation.harvestsap.TableView.view_model;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import id.co.ams_plantation.harvestsap.TableView.model.TableViewCell;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewColumnHeader;
import id.co.ams_plantation.harvestsap.TableView.model.TableViewRowHeader;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;

public class MekanisasiPanenTableViewModel {
    private final Context mContext;
    private final ArrayList<TransaksiPanen> panens;
    private final ArrayList<Angkut> angkuts;
    String [] HeaderColumn = {"Block","Ancak","Pemanen","Janjang","Berat Estimasi","BJR","ID NFC","KET"};

    public MekanisasiPanenTableViewModel(Context context, HashMap<String,TransaksiPanen> hPanen,HashMap<String,Angkut> hAngkut) {
        mContext = context;
        panens = new ArrayList<>();
        angkuts = new ArrayList<>();
        panens.addAll(hPanen.values());
        angkuts.addAll(hAngkut.values());
        Collections.sort(panens, new Comparator<TransaksiPanen>() {
            @Override
            public int compare(TransaksiPanen t0, TransaksiPanen t1) {
                return t0.getIdTPanen().compareTo(t1.getIdTPanen());
            }
        });
        Collections.sort(angkuts, new Comparator<Angkut>() {
            @Override
            public int compare(Angkut t0, Angkut t1) {
                return t0.getIdAngkut().compareTo(t1.getIdAngkut());
            }
        });
    }

    private List<TableViewRowHeader> getSimpleRowHeaderList() {
        List<TableViewRowHeader> list = new ArrayList<>();
        int no = 0;
        //"mekanisais"
        for (int i = 0; i < panens.size(); i++) {
            String id = i + ";" + panens.get(i).getIdTPanen();
            no = i + 1;
            TableViewRowHeader header = new TableViewRowHeader(id, String.valueOf(no));
            header.setColor(panens.get(i).getColorBackground());
            list.add(header);
        }
        // tap kartu
        for (int i = 0; i < angkuts.size(); i++) {
            String nama = ";manual";
            if(angkuts.get(i).getTransaksiPanen() != null){
                nama = ";angkut";
            }else if (angkuts.get(i).getTransaksiAngkut() != null){
                nama = ";transit";
            }
            String id = i + ";" + angkuts.get(i).getIdAngkut() + nama;
            no++;
            TableViewRowHeader header = new TableViewRowHeader(id, String.valueOf(no));
            list.add(header);
        }

        return list;
    }
    /**
     * This is a dummy model list test some cases.
     */
    private List<TableViewColumnHeader> getRandomColumnHeaderList() {
        List<TableViewColumnHeader> list = new ArrayList<>();

        for (int i = 0; i < HeaderColumn.length; i++) {
            TableViewColumnHeader header = new TableViewColumnHeader(String.valueOf(i), HeaderColumn[i]);
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    private List<List<TableViewCell>> getCellListForSortingTest() {
        List<List<TableViewCell>> list = new ArrayList<>();
        for (int i = 0; i < panens.size(); i++) {
            List<TableViewCell> cellList = new ArrayList<>();
            for (int j = 0; j < HeaderColumn.length; j++) {
                // Create dummy id.
                String id = j + ";" + panens.get(i).getIdTPanen();
                int color = panens.get(i).getColorBackground();
                TableViewCell cell = null;
                switch (j){
                    case 0: {
                        String nama = "";
                        if (panens.get(i).getTph() != null) {
                            nama = panens.get(i).getTph().getBlock();
                        }
                        cell = new TableViewCell(id, nama);
                        break;
                    }
                    case 1: {
                        String nama = "";
                        if (panens.get(i).getTph() != null) {
                            nama = panens.get(i).getTph().getAncak();
                        }
                        cell = new TableViewCell(id, nama);
                        break;
                    }
                    case 2: {
                        String nama = "";
                        if (panens.get(i).getPemanen() != null) {
                            nama = panens.get(i).getPemanen().getKodePemanen() + "-" + panens.get(i).getPemanen().getNama();
                        }
                        cell = new TableViewCell(id, nama);
                        break;
                    }
                    case 3: {
                        if (panens.get(i).getHasilPanen() != null) {
                            cell = new TableViewCell(id, panens.get(i).getHasilPanen().getTotalJanjang());
                        }else {
                            cell = new TableViewCell(id, "");
                        }
                        break;
                    }
                    case 4: {
                        if (panens.get(i).getHasilPanen() != null && panens.get(i).getBjrMekanisasi() > 0.0) {
                            int beratEstimasi = (int) (panens.get(i).getHasilPanen().getTotalJanjang() * panens.get(i).getBjrMekanisasi());
                            cell = new TableViewCell(id,  beratEstimasi);
                        }else {
                            cell = new TableViewCell(id, "");
                        }
                        break;
                    }
                    case 5: {
                        cell = new TableViewCell(id, String.format("%.1f", panens.get(i).getBjrMekanisasi()));
                        break;
                    }
                    case 6: {
                        cell = new TableViewCell(id, "");
                        break;
                    }case 7: {
                        cell = new TableViewCell(id, "Mekanisasi");
                        break;
                    }
                }
                cell.setColor(color);
                cellList.add(cell);
            }
            list.add(cellList);
        }

        for (int i = 0; i < angkuts.size(); i++) {
            List<TableViewCell> cellList = new ArrayList<>();
            for (int j = 0; j < HeaderColumn.length; j++) {
                // Create dummy id.
                String id = j + "; " + angkuts.get(i).getIdAngkut() + "-Angkut";

                TableViewCell cell = null;
                switch (j){
                    case 0: {
                        String nama = "-";
                        if (angkuts.get(i).getTransaksiPanen() != null) {
                            if (angkuts.get(i).getTransaksiPanen().getTph() != null) {
                                if (angkuts.get(i).getTransaksiPanen().getTph().getBlock() != null) {
                                    nama = angkuts.get(i).getTransaksiPanen().getTph().getBlock();
                                }
                            }
                        } else if (angkuts.get(i).getTph() != null) {
                            if (angkuts.get(i).getTph().getBlock() != null) {
                                nama = angkuts.get(i).getTph().getBlock();
                            }
                        }
                        cell = new TableViewCell(id, nama);
                        break;
                    }
                    case 1: {
                        String nama = "Manual";
                        if (angkuts.get(i).getTransaksiPanen() != null) {
                            nama = angkuts.get(i).getTransaksiPanen().getTph().getAncak();
                            if (nama == null) {
                                nama = "TPH";
                            }
                        } else if (angkuts.get(i).getTph() != null) {
                            if (angkuts.get(i).getTph().getAncak() != null) {
                                nama = angkuts.get(i).getTph().getAncak();
                            }
                        }
                        cell = new TableViewCell(id, nama);
                        break;
                    }
                    case 2: {
                        String nama = "";
                        if(angkuts.get(i).getTransaksiPanen() != null){
                            if(angkuts.get(i).getTransaksiPanen().getPemanen() != null){
                                nama = angkuts.get(i).getTransaksiPanen().getPemanen().getKodePemanen() + "-" + angkuts.get(i).getTransaksiPanen().getPemanen().getNama();
                            }
                        }
                        cell = new TableViewCell(id, nama);
                        break;
                    }
                    case 3: {
                        cell = new TableViewCell(id, angkuts.get(i).getJanjang());
                        break;
                    }
                    case 4: {
                        cell = new TableViewCell(id, String.format("%.1f", angkuts.get(i).getBerat()));
                        break;
                    }
                    case 5: {
                        cell = new TableViewCell(id, String.format("%.1f", angkuts.get(i).getBjr()));
                        break;
                    }
                    case 6: {
                        String idNfc = "-";
                        if(angkuts.get(i).getIdNfc() != null) {
                            if (angkuts.get(i).getIdNfc().size() > 0) {
                                for (String s : angkuts.get(i).getIdNfc()) {
                                    idNfc = s;
                                }
                            }
                        }
                        cell = new TableViewCell(id, idNfc);
                        break;
                    }
                    case 7: {
                        String nama = "";
                        if(angkuts.get(i).getTransaksiPanen() != null){
                            nama = "TAP";
                        }else if (angkuts.get(i).getManualSPBData() != null){
                            nama = angkuts.get(i).getManualSPBData().getTipe();
                        }
                        cell = new TableViewCell(id, nama);
                        break;
                    }
                }
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }

    private static String getRandomString() {
        Random r = new Random();
        String str = " a ";
        for (int i = 0; i < r.nextInt(); i++) {
            str = str + " a ";
        }

        return str;
    }

    public List<List<TableViewCell>> getCellList() {
        return getCellListForSortingTest();
    }

    public List<TableViewRowHeader> getRowHeaderList() {
        return getSimpleRowHeaderList();
    }

    public List<TableViewColumnHeader> getColumnHeaderList() {
        return getRandomColumnHeaderList();
    }

}
