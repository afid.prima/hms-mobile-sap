package id.co.ams_plantation.harvestsap.util;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import id.co.ams_plantation.harvestsap.Fragment.AngkutFragment;
import id.co.ams_plantation.harvestsap.Fragment.SpbFragment;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiSPB;
import id.co.ams_plantation.harvestsap.ui.MainMenuActivity;
import ng.max.slideview.SlideView;

public class MainMenuHelper {
    Context context;
    SpbFragment spbFragment;
    AngkutFragment angkutFragment;

    public MainMenuHelper(Context context) {
        this.context = context;
    }

    public void showYesNoQuestion(Object object){
        if(context instanceof MainMenuActivity){
            android.support.v4.app.Fragment fragment = ((MainMenuActivity.TabPagerAdapter) ((MainMenuActivity)context).viewPager.getAdapter()).getItem(((MainMenuActivity)context).viewPager.getCurrentItem());
            if(fragment instanceof SpbFragment){
                spbFragment = ((SpbFragment) fragment);
            }else if (fragment instanceof AngkutFragment){
                angkutFragment = ((AngkutFragment) fragment);
            }
        }

        View dialogView = LayoutInflater.from(context).inflate(R.layout.yes_no, null);
        TextView tvDialogKet = (TextView) dialogView.findViewById(R.id.ket1);
        SlideView btnCancelDialog = (SlideView) dialogView.findViewById(R.id.btnCancel);
        SlideView btnOkDialog = (SlideView) dialogView.findViewById(R.id.btnOK);

        final AlertDialog dialog = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
                .setView(dialogView)
                .setCancelable(false)
                .create();

        final ForegroundColorSpan fcs = new ForegroundColorSpan(context.getResources().getColor(R.color.Red));
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

        String text = "";
        if(spbFragment != null){
            TransaksiSPB transaksiSPB = (TransaksiSPB) object;
            text = "Apakah Anda Yakin Untuk Hapus SPB "+transaksiSPB.getNoSpb();
        }else if (angkutFragment != null){
            TransaksiAngkut transaksiAngkut = (TransaksiAngkut) object;
            text = "Apakah Anda Yakin Untuk Hapus Angkut "+transaksiAngkut.getIdTAngkut();
        }
        tvDialogKet.setText(text);
        btnOkDialog.setText("Yes");
        btnCancelDialog.setText("No");

        btnCancelDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                dialog.dismiss();
            }
        });
        btnOkDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                dialog.dismiss();
                if (spbFragment != null) {
                    spbFragment.startLongOperation(SpbFragment.STATUS_LONG_OPERATION_DELETED);
                }else if (angkutFragment != null){
                    angkutFragment.startLongOperation(AngkutFragment.STATUS_LONG_OPERATION_DELETED);
                }
            }
        });
        dialog.show();
    }
}
