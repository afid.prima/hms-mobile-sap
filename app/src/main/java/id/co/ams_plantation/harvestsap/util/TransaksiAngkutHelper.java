package id.co.ams_plantation.harvestsap.util;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import id.co.ams_plantation.harvestsap.Fragment.AngkutInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.LangsiranInputFragment;
import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.adapter.QrItemAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectedTransaksiAngkutVersion;
import id.co.ams_plantation.harvestsap.encryptor.Encrypts;
import id.co.ams_plantation.harvestsap.model.Angkut;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.Estate;
import id.co.ams_plantation.harvestsap.model.Langsiran;
import id.co.ams_plantation.harvestsap.model.MekanisasiPanen;
import id.co.ams_plantation.harvestsap.model.ModelRecyclerView;
import id.co.ams_plantation.harvestsap.model.Operator;
import id.co.ams_plantation.harvestsap.model.PengirimanGanda;
import id.co.ams_plantation.harvestsap.model.SubTotalAngkut;
import id.co.ams_plantation.harvestsap.model.SubTotalAngkutMekanisasi;
import id.co.ams_plantation.harvestsap.model.SupervisionList;
import id.co.ams_plantation.harvestsap.model.TransaksiAngkut;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.model.TranskasiAngkutVersion;
import id.co.ams_plantation.harvestsap.model.User;
import id.co.ams_plantation.harvestsap.model.Vehicle;
import id.co.ams_plantation.harvestsap.service.BluetoothService;
import id.co.ams_plantation.harvestsap.ui.AngkutActivity;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import jp.wasabeef.recyclerview.adapters.SlideInLeftAnimationAdapter;

/**
 * Created by user on 12/28/2018.
 */

public class TransaksiAngkutHelper {

    FragmentActivity activity;
    public AlertDialog listrefrence;
    String TAG = this.getClass().getName();

    public TransaksiAngkutHelper(FragmentActivity activity) {
        this.activity = activity;
    }

//    public static void createNewAngkut(){
//        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.SUBSTITUTE_KRANI_ANGKUT, Context.MODE_PRIVATE);
//        String stringSubstituteKrani = preferences.getString(HarvestApp.SUBSTITUTE_KRANI_ANGKUT, null);
//
//        SimpleDateFormat sdfD = new SimpleDateFormat("ddMMyy");
//        try {
//            JSONObject object = new JSONObject();
//            object.put("idTransaksi","D" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_TRANSAKSI_ANGKUT))
//            + sdfD.format(System.currentTimeMillis())
//            + GlobalHelper.getUser().getUserID());
//            object.put("idSpb","S" + String.format("%04d", GlobalHelper.getCountNumber(GlobalHelper.LIST_FOLDER_SPB))
//                    + sdfD.format(System.currentTimeMillis())
//                    + GlobalHelper.getUser().getUserID());
//            object.put("approveBy", GlobalHelper.getUser().getUserID());
//            object.put("createDate",System.currentTimeMillis());
//            object.put("tujuan",TransaksiAngkut.TUJUAN_KELUAR);
////            object.put("vehicle","DT01");
//            if(GlobalHelper.getAllOperator().values().size() > 0 && GlobalHelper.getAllVehicle().values().size() > 0){
//                object.put("kendaraanDari", TransaksiAngkut.KENDARAAN_KEBUN);
//            }else {
//                object.put("kendaraanDari", TransaksiAngkut.KENDARAAN_LUAR);
//            }
//            if(stringSubstituteKrani != null){
//                object.put("subtitute", stringSubstituteKrani);
//            }
//            File file = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
//            GlobalHelper.writeFileContent(file.getAbsolutePath(),object.toString());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    public static void createNewAngkut(TransaksiAngkut transaksiAngkut){

        Gson gson = new Gson();
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.SUBSTITUTE_KRANI_ANGKUT, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        if(transaksiAngkut.getSubstitute() != null){
            editor.putString(HarvestApp.SUBSTITUTE_KRANI_ANGKUT,gson.toJson(transaksiAngkut.getSubstitute()));
            editor.apply();
        }else{
            editor.remove(HarvestApp.SUBSTITUTE_KRANI_ANGKUT);
            editor.apply();
        }

        String object = gson.toJson(transaksiAngkut);
        File file = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
        GlobalHelper.writeFileContent(file.getAbsolutePath(),object);
    }

//    public static void createNewAngkut(JSONObject jsonObject){;
//        try {
//            SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.SUBSTITUTE_KRANI_ANGKUT, Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = preferences.edit();
//
//            JSONObject object = new JSONObject();
//            object.put("idTransaksi",jsonObject.getString("idTransaksi"));
//            object.put("createDate",jsonObject.getLong("createDate"));
//            object.put("tujuan",jsonObject.getInt("tujuan"));
//            if(jsonObject.has("vehicle")) {
//                object.put("vehicle",jsonObject.getString("vehicle"));
//            }
//            object.put("kendaraanDari",jsonObject.getInt("kendaraanDari"));
//            object.put("supir",jsonObject.getString("supir"));
//            if(jsonObject.has("foto")) {
//                object.put("foto", jsonObject.getJSONArray("foto"));
//            }
//
//            if(jsonObject.has("subtitute")){
//                object.put("subtitute",jsonObject.getString("subtitute"));
//                editor.putString(HarvestApp.SUBSTITUTE_KRANI_ANGKUT,jsonObject.getString("subtitute"));
//                editor.apply();
//            }else{
//                editor.remove(HarvestApp.SUBSTITUTE_KRANI_ANGKUT);
//                editor.apply();
//            }
//
//            if(jsonObject.getInt("tujuan") == TransaksiAngkut.TUJUAN_LANGSIR) {
//                object.put("langsiran", jsonObject.getString("langsiran"));
//            }else if (jsonObject.getInt("tujuan") == TransaksiAngkut.TUJUAN_PKS) {
//                object.put("idSpb", jsonObject.getString("idSpb"));
//                object.put("pks", jsonObject.getString("pks"));
//                object.put("approveBy", jsonObject.getString("approveBy"));
//            }else if (jsonObject.getInt("tujuan") == TransaksiAngkut.TUJUAN_KELUAR){
//                object.put("idSpb", jsonObject.getString("idSpb"));
//                object.put("approveBy", jsonObject.getString("approveBy"));
//            }
//
//            if(jsonObject.has("bin")){
//                object.put("bin",jsonObject.getString("bin"));
//            }else{
//                object.remove("bin");
//            }
//
//            if(jsonObject.has("loader")){
//                object.put("loader", jsonObject.getJSONArray("loader"));
//            }
//
//            if(jsonObject.has("pengirimanGandas")){
//                object.put("pengirimanGandas", jsonObject.getJSONArray("pengirimanGandas"));
//            }
//
//            File file = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
//            GlobalHelper.writeFileContent(file.getAbsolutePath(),object.toString());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    public static void hapusFileAngkut(Boolean withDB){
        File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
        File fileAngkutCounter = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+  GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_ANGKUT] + "/" + "COUNT",GlobalHelper.getUser().getUserID() );
        File fileDBAngkut = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT) + "/" +Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT)+ ".db");
        if(fileAngkutSelected.exists()){
            fileAngkutSelected.delete();
        }
        if(fileAngkutCounter.exists()){
            fileAngkutCounter.delete();
        }
        if(fileDBAngkut.exists() && withDB){
            fileDBAngkut.delete();
        }
    }

    public static String readAngkutSelected(){
        File fileAngkutSelected = new File(Environment.getExternalStorageDirectory() + GlobalHelper.EXTERNAL_DIR_FILES_HMS_DB2 +"/"+ GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] ,GlobalHelper.LIST_FOLDER[GlobalHelper.LIST_FOLDER_SELECT_ANGKUT] );
        if(fileAngkutSelected.exists()){
            return GlobalHelper.readFileContent(fileAngkutSelected.getAbsolutePath());
        }else{
            return null;
        }
    }

    public static boolean cekDataLangsiranUpdate(Context context, TransaksiAngkut transaksiAngkut){
        AngkutActivity activity = null;
        if(context instanceof AngkutActivity){
            activity = (AngkutActivity) context;
        }else{
            return false;
        }
        return activity.selectedTransaksiAngkut.getIdTAngkut().equalsIgnoreCase(transaksiAngkut.getIdTAngkut());
//        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
//        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
//        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", transaksiAngkut.getIdTAngkut()));
//        if(cursor.size() > 0) {
//            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
//                DataNitrit dataNitrit = (DataNitrit) iterator.next();
//                Gson gson = new Gson();
//                TransaksiAngkut tAngkut = gson.fromJson(dataNitrit.getValueDataNitrit(),TransaksiAngkut.class);
//                if(transaksiAngkut.getLangsiran() != null && tAngkut.getLangsiran() != null) {
//                    if (transaksiAngkut.getLangsiran().getIdTujuan().equalsIgnoreCase(tAngkut.getLangsiran().getIdTujuan())) {
//                        db.close();
//                        return true;
//                    }
//                }
//            }
//        }
//        db.close();
    }

    public ArrayList<TransaksiAngkut> setupBatchKartuAngkut(TransaksiAngkut transaksiAngkut){
        ArrayList<TransaksiAngkut> transaksiAngkuts = new ArrayList<>();
        Collections.sort(transaksiAngkut.getAngkuts(), new Comparator<Angkut>() {
            @Override
            public int compare(Angkut t0, Angkut t1) {
                return  t0.getOph().compareTo(t1.getOph());
            }
        });

        Gson gson = new Gson();
        String sAngkut = gson.toJson(transaksiAngkut);

        int count = 0;
        ArrayList<Angkut> angkuts = new ArrayList<>();
        ArrayList<Angkut> data = transaksiAngkut.getAngkuts();
        for (Angkut angkut : data){
            if(!angkut.getOph().equalsIgnoreCase(Angkut.MANUAL)) {
                angkuts.add(angkut);
                if (count == GlobalHelper.MAX_ANGKUT_IN_TRANSAKSI) {
                    TransaksiAngkut headerTAngkut = gson.fromJson(sAngkut,TransaksiAngkut.class);
                    headerTAngkut.setAngkuts(angkuts);
                    transaksiAngkuts.add(headerTAngkut);

                    angkuts = new ArrayList<>();
                    count = 0;
                } else {
                    count++;
                }
            }
        }

        if(angkuts.size() > 0 ){
            TransaksiAngkut headerTAngkut = gson.fromJson(sAngkut,TransaksiAngkut.class);
            headerTAngkut.setAngkuts(angkuts);
            transaksiAngkuts.add(headerTAngkut);
        }
        return transaksiAngkuts;
    }

    public void tapNFC(TransaksiAngkut transaksiAngkut) {
        Log.d(TAG, "tapNFC: "+transaksiAngkut.getAngkuts().size() );
        /// pastikan nfc hijau
        if (((AngkutActivity) activity).myTag == null) {
            Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.nfc_error), Toast.LENGTH_LONG).show();
        } else {
            if (((AngkutActivity) activity).TYPE_NFC == GlobalHelper.TYPE_NFC_HIJAU) {
                if (((AngkutActivity) activity).valueNFC != null) {
                    if (((AngkutActivity) activity).valueNFC.length() == 5) {
                        AngkutActivity angkutActivity = ((AngkutActivity) activity);
                        String compres = toCompres(transaksiAngkut);
                        if (angkutActivity.getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof LangsiranInputFragment) {
                            if (NfcHelper.write(compres, ((AngkutActivity) activity).myTag,true)) {
                                updateDataTranskasiWithNFCId(transaksiAngkut);
                                angkutActivity.backProses();
                                Log.d(this.getClass() + " backProses", "Dari LangsiranInputFragment");
                            } else {
                                Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_write_error), Toast.LENGTH_LONG).show();
                            }
                        }else if (angkutActivity.getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof  AngkutInputFragment) {
                            if (NfcHelper.write(compres, ((AngkutActivity) activity).myTag,false)) {
                                updateDataTranskasiWithNFCId(transaksiAngkut);

                                AngkutInputFragment fragment = (AngkutInputFragment) angkutActivity.getSupportFragmentManager().findFragmentById(R.id.content_container);
                                Log.d(TAG, "tapNFC: idxTap "+angkutActivity.idxTap );
                                angkutActivity.idxTap ++;
                                if (angkutActivity.idxTap < angkutActivity.batchKartuAngkut.size()) {
                                    fragment.nextNfc();
                                    WidgetHelper.showSnackBar(angkutActivity.myCoordinatorLayout,String.format("Tap [ %,d / %,d]",angkutActivity.idxTap,angkutActivity.batchKartuAngkut.size()));
                                    WidgetHelper.showOKDialog(this.activity,
                                            "Warning",
                                            "Tap Kartu NFC Selanjutnya Unutk Simpan Data Selanjutnya",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    fragment.nextNfc();
                                                    dialog.dismiss();
                                                }
                                            }
                                    );
                                } else {
                                    Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_write_success), Toast.LENGTH_LONG).show();
                                    Log.d(this.getClass() + " tapNFC", "Close1");
                                    ((AngkutActivity) activity).closeActivity();
                                    Log.d(this.getClass() + " backProses", "Dari AngkutInputFragment");
                                }
                            } else {
                                Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_write_error), Toast.LENGTH_LONG).show();
                            }

                        }
                    } else {
                        if (((AngkutActivity) activity).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof LangsiranInputFragment){
                            String compres = toCompres(transaksiAngkut);
                            if (NfcHelper.write(compres, ((AngkutActivity) activity).myTag,true)) {
                                Log.d(this.getClass() + " tapNFC", "Close2");
                                Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_write_success), Toast.LENGTH_LONG).show();
                                ((AngkutActivity) activity).backProses();
                                Log.d(this.getClass() + " backProses", "Dari LangsiranInputFragment");
                            } else {
                                Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_write_error), Toast.LENGTH_LONG).show();
                            }
                        }else if (((AngkutActivity) activity).getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof AngkutInputFragment) {
                            if (transaksiAngkut.getIdTAngkut().equalsIgnoreCase(((AngkutActivity) activity).selectedTransaksiAngkut.getIdTAngkut())) {
                                String compres = toCompres(transaksiAngkut);
                                if (NfcHelper.write(compres, ((AngkutActivity) activity).myTag,false)) {
                                    updateDataTranskasiWithNFCId(transaksiAngkut);
                                    Log.d(this.getClass() + " tapNFC", "Close3");
                                    AngkutActivity angkutActivity = ((AngkutActivity) activity);
                                    AngkutInputFragment fragment = (AngkutInputFragment) ((AngkutActivity) activity).getSupportFragmentManager().findFragmentById(R.id.content_container);
                                    angkutActivity.idxTap ++;
                                    if (angkutActivity.idxTap < angkutActivity.batchKartuAngkut.size()) {
                                        fragment.nextNfc();
                                        WidgetHelper.showSnackBar(angkutActivity.myCoordinatorLayout,String.format("Tap [ %,d / %,d]",angkutActivity.idxTap,angkutActivity.batchKartuAngkut.size()));
                                        WidgetHelper.showOKDialog(this.activity,
                                                "Warning",
                                                "Tap Kartu NFC Selanjutnya Unutk Simpan Data Selanjutnya",
                                                new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        fragment.nextNfc();
                                                        dialog.dismiss();
                                                    }
                                                }
                                        );
                                    } else {
                                        Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_write_success), Toast.LENGTH_LONG).show();
                                        Log.d(this.getClass() + " tapNFC", "Close1");
                                        ((AngkutActivity) activity).closeActivity();
                                        Log.d(this.getClass() + " backProses", "Dari AngkutInputFragment");
                                    }
                                } else {
                                    Toast.makeText(HarvestApp.getContext(), HarvestApp.getContext().getResources().getString(R.string.nfc_write_error), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Log.d(this.getClass() + " tapNFC", "not empty 1");
                                Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.nfc_not_empty), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } else {
                    Log.d(this.getClass() + " tapNFC", "not empty 2");
                    Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.nfc_not_empty), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(HarvestApp.getContext(), activity.getResources().getString(R.string.not_nfc_green), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void updateDataTranskasiWithNFCId(TransaksiAngkut transaksiAngkut){
        HashMap<String ,Angkut> angkutHashMap = new HashMap<>();
        for(Angkut angkut : transaksiAngkut.getAngkuts()){
            angkutHashMap.put(angkut.getOph(),angkut);
        }
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", transaksiAngkut.getIdTAngkut()));
        if(cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                TransaksiAngkut tAngkut = gson.fromJson(dataNitrit.getValueDataNitrit(),TransaksiAngkut.class);
                if(tAngkut.getAngkuts().size() > 0){
                    ArrayList<Angkut> angkuts = new ArrayList<>();
                    for (Angkut angkut : tAngkut.getAngkuts()){
                        if(angkutHashMap.get(angkut.getOph()) != null){
                            Set<String> idNfc = new android.support.v4.util.ArraySet<>();
                            if(angkut.getIdNfc() != null){
                                idNfc = angkut.getIdNfc();
                            }
                            idNfc.add(GlobalHelper.bytesToHexString(((AngkutActivity) activity).myTag.getId()));
                            angkut.setIdNfc(idNfc);
                        }
                        angkuts.add(angkut);
                    }
                    tAngkut.setAngkuts(angkuts);
                    dataNitrit = new DataNitrit(tAngkut.getIdTAngkut(), gson.toJson(tAngkut), ""+tAngkut.isManual());
                    repository.update(dataNitrit);
                }
                break;
            }
        }
        db.close();
    }

    public void printTransaksiAngkut(TransaksiAngkut transaksiAngkut,ArrayList<Angkut> angkutView){
        if (((BaseActivity) activity).bluetoothHelper.mService.getState() != BluetoothService.STATE_CONNECTED) {
            ((BaseActivity) activity).bluetoothHelper.showListBluetoothPrinter();
        }else {
            String compres = toCompres(transaksiAngkut);
            ((BaseActivity) activity).bluetoothHelper.printTransaksiAngkut(compres, transaksiAngkut,angkutView);
            if(activity instanceof  AngkutActivity){
                if(activity.getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof AngkutInputFragment) {
                    ((AngkutActivity) activity).closeActivity();
                }
            }
        }
    }

    public void printRekonsilasi(TransaksiAngkut transaksiAngkut, int kartuRusak, int kartuHilang,ArrayList<Angkut> angkutView){
        if (((BaseActivity) activity).bluetoothHelper.mService.getState() != BluetoothService.STATE_CONNECTED) {
            ((BaseActivity) activity).bluetoothHelper.showListBluetoothPrinter();
        }else {
            String compres = toCompres(transaksiAngkut);
            ((BaseActivity) activity).bluetoothHelper.printRekonsilasi(compres, transaksiAngkut, kartuRusak, kartuHilang,angkutView);
            if(activity instanceof  AngkutActivity){
                if(activity.getSupportFragmentManager().findFragmentById(R.id.content_container) instanceof AngkutInputFragment) {
                    ((AngkutActivity) activity).closeActivity();
                }
            }
        }
    }

    public void showQr(TransaksiAngkut transaksiAngkut,QrHelper qrHelper,ArrayList<Angkut> angkutView){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyy HH:mm");
        String tujuan = TransaksiAngkut.PKS;
        if(transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR){
            tujuan = TransaksiAngkut.LANGSIR;
            if(transaksiAngkut.getLangsiran() != null){
                tujuan = transaksiAngkut.getLangsiran().getNamaTujuan();
            }
        }else if (transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_PKS){
            tujuan = TransaksiAngkut.PKS;
            if(transaksiAngkut.getPks() != null){
                tujuan = transaksiAngkut.getPks().getNamaPKS();
            }
        }else{
            tujuan = "KELUAR";
        }
        String compres = toCompres(transaksiAngkut);
        View view = LayoutInflater.from(activity).inflate(R.layout.show_qr,null);
        RelativeLayout isi = (RelativeLayout) view.findViewById(R.id.isi);
//        ImageView iv_qrcode = (ImageView) view.findViewById(R.id.iv_qrcode);
//        TextView tvHeaderQr = (TextView) view.findViewById(R.id.tvHeaderQr);
        Button shareqr = (Button) view.findViewById(R.id.btn_shareqr);
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);

//        tvHeaderQr.setText(activity.getResources().getString(R.string.transaksi_angkut));
//        try {
//            iv_qrcode.setImageBitmap(qrHelper.encodeAsBitmap(compres));
//        } catch (WriterException e) {
//            e.printStackTrace();
//        }

        int angkutTph = 0;
        int angkutTransit = 0;
        int angkutManual = 0;

        for (int i = 0 ; i < angkutView.size(); i++){
            if(angkutView.get(i).getTransaksiPanen() != null){
                angkutTph ++;
            }else if (angkutView.get(i).getTransaksiAngkut() != null){
                angkutTransit++;
            }else{
                angkutManual ++;
            }
        }

        Estate estate = GlobalHelper.getEstateByEstCode(transaksiAngkut.getEstCode());

        ArrayList<ModelRecyclerView> arrayList = new ArrayList<>();
        arrayList.add(new ModelRecyclerView("headerQr",activity.getResources().getString(R.string.surat_langsiran),"",""));
        arrayList.add(new ModelRecyclerView("Qr",compres,"",""));
        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_transaksi),"",""));
        if(estate != null) {
            arrayList.add(new ModelRecyclerView("detail", "PT", estate.getCompanyShortName(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Estate", estate.getEstName(), ""));
        }else{
            arrayList.add(new ModelRecyclerView("detail", "PT", activity.getResources().getString(R.string.dont_have_estate) + transaksiAngkut.getEstCode(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Estate", activity.getResources().getString(R.string.dont_have_estate) + transaksiAngkut.getEstCode(), ""));
        }

        User userSelected = null;
        if(qrHelper.context instanceof BaseActivity){
            userSelected = GlobalHelper.dataAllUser.get(transaksiAngkut.getCreateBy());
            arrayList.add(new ModelRecyclerView("detail","Input Oleh" ,userSelected.getUserFullName(),""));
        }else{
            arrayList.add(new ModelRecyclerView("detail","Input Oleh" ,transaksiAngkut.getCreateBy(),""));
        }

        if(transaksiAngkut.getSubstitute() != null){
            arrayList.add(new ModelRecyclerView("detail","Krani Penganti" ,transaksiAngkut.getSubstitute().getNama(),""));
        }

        arrayList.add(new ModelRecyclerView("detail","Waktu Input" ,dateFormat.format(transaksiAngkut.getEndDate()),""));
        arrayList.add(new ModelRecyclerView("detail","ID Transaksi" ,transaksiAngkut.getIdTAngkut(),""));
        arrayList.add(new ModelRecyclerView("detail","Tujuan" ,tujuan,""));

        if(transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR){
            if(transaksiAngkut.getLangsiran() != null){
                if(transaksiAngkut.getLangsiran().getNamaTujuan() != null){
                    arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_langsiran),"",""));

                    Estate estateLangsiran = GlobalHelper.getEstateByEstCode(transaksiAngkut.getLangsiran().getEstCode());
                    arrayList.add(new ModelRecyclerView("detail","PT" ,estateLangsiran.getCompanyShortName(),""));
                    arrayList.add(new ModelRecyclerView("detail","Estate" ,estateLangsiran.getEstName(),""));
                    arrayList.add(new ModelRecyclerView("detail","Afdeling" ,transaksiAngkut.getLangsiran().getAfdeling(),""));
                    arrayList.add(new ModelRecyclerView("detail","Block" ,transaksiAngkut.getLangsiran().getBlock(),""));
                    arrayList.add(new ModelRecyclerView("detail","Nama Langsiran" ,transaksiAngkut.getLangsiran().getNamaTujuan(),""));
                    arrayList.add(new ModelRecyclerView("detail","Type Langsiran" ,Langsiran.LIST_TIPE_LANGSIRAN[transaksiAngkut.getLangsiran().getTypeTujuan()],""));
                }
            }
        }else if (transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_PKS) {
            if(transaksiAngkut.getPks() != null){
                if(transaksiAngkut.getPks().getNamaPKS() != null){
                    arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_pks),"",""));

                    Estate estateLangsiran = GlobalHelper.getEstateByEstCode(transaksiAngkut.getPks().getEstCode());
                    arrayList.add(new ModelRecyclerView("detail","PT" ,estateLangsiran.getCompanyShortName(),""));
                    arrayList.add(new ModelRecyclerView("detail","Estate" ,estateLangsiran.getEstName(),""));
                    arrayList.add(new ModelRecyclerView("detail","Afdeling" ,transaksiAngkut.getPks().getAfdeling(),""));
                    arrayList.add(new ModelRecyclerView("detail","Block" ,transaksiAngkut.getPks().getBlock(),""));
                    arrayList.add(new ModelRecyclerView("detail","Nama PKS" ,transaksiAngkut.getPks().getNamaPKS(),""));

                }
            }
        }

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_vehicle),"",""));
        arrayList.add(new ModelRecyclerView("detail","Kendaraan Dari" ,transaksiAngkut.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN ?
                TransaksiAngkut.KEBUN : TransaksiAngkut.LUAR,""));
        if(qrHelper.context instanceof BaseActivity){
            Operator operator = transaksiAngkut.getSupir();
            Vehicle vehicle = transaksiAngkut.getVehicle();
            arrayList.add(new ModelRecyclerView("detail","Supir" ,operator != null ? operator.getNama(): "",""));
            arrayList.add(new ModelRecyclerView("detail","Kendaraan" ,vehicle != null ? vehicle.getVehicleCode():"",""));
        }else{
            arrayList.add(new ModelRecyclerView("detail","Supir" ,transaksiAngkut.getSupir() != null ? transaksiAngkut.getSupir().getNama() : "",""));
            arrayList.add(new ModelRecyclerView("detail","Kendaraan" ,transaksiAngkut.getVehicle() != null ? transaksiAngkut.getVehicle().getVehicleCode() : "" ,""));
        }

        if(transaksiAngkut.getBin() != null){
            arrayList.add(new ModelRecyclerView("detail","Bin" ,transaksiAngkut.getBin(),""));
        }

        if(transaksiAngkut.getPengirimanGandas() != null) {
            Collections.sort(transaksiAngkut.getPengirimanGandas(), new Comparator<PengirimanGanda>() {
                @Override
                public int compare(PengirimanGanda o1, PengirimanGanda o2) {
                    return o1.getIdx() < o2.getIdx() ? -1 : (o1.getIdx() > o2.getIdx()) ? 1 : 0;
                }
            });

            arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_pengiriman_lanjut),"",""));
            arrayList.add(new ModelRecyclerView("detailgandaheader",
                    "Unit Ke",
                    "Kepemilikan",
                    "Unit",
                    "Operator"
            ));
            for (int i = 0; i < transaksiAngkut.getPengirimanGandas().size(); i++) {
                PengirimanGanda ganda = transaksiAngkut.getPengirimanGandas().get(i);
                int ke = i + 2;
                arrayList.add(new ModelRecyclerView("detailganda",
                        String.valueOf(ke) ,
                        ganda.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN ? TransaksiAngkut.KEBUN : TransaksiAngkut.LUAR,
                        ganda.getVehicle().getVehicleCode(),
                        ganda.getOperator().getNama()
                ));
            }
        }

        if(transaksiAngkut.getLoader() != null){
            arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_loader),"",""));
            arrayList.add(new ModelRecyclerView("detailloaderheader","Nama" ,"NIK","Estate"));
            for (int i = 0; i < transaksiAngkut.getLoader().size(); i++) {
                Operator operator = transaksiAngkut.getLoader().get(i);
                arrayList.add(new ModelRecyclerView("detailloader",operator.getNama() ,operator.getNik(),operator.getArea()));
            }
        }

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_angkut),"",""));
        arrayList.add(new ModelRecyclerView("detail","Σ Angkut TPH" ,String.valueOf(angkutTph),""));
        arrayList.add(new ModelRecyclerView("detail","Σ Angkut Langsiran" ,String.valueOf(angkutTransit),""));
        arrayList.add(new ModelRecyclerView("detail","Σ Angkut Manual" ,String.valueOf(angkutManual),""));
        arrayList.add(new ModelRecyclerView("detail","Σ Angkut" ,String.valueOf(angkutView.size()),""));
        arrayList.add(new ModelRecyclerView("detail","Mulai Angkut" ,dateFormat.format(transaksiAngkut.getStartDate()),""));
        arrayList.add(new ModelRecyclerView("detail","Selesai Angkut" ,dateFormat.format(transaksiAngkut.getEndDate()),""));

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.tbs_angkut),"",""));
        arrayList.add(new ModelRecyclerView("detail","Janjang" ,String.format("%,d",transaksiAngkut.getTotalJanjang()),"Jjg"));
        arrayList.add(new ModelRecyclerView("detail","Brondolan" ,String.format("%,d",transaksiAngkut.getTotalBrondolan()),"Kg"));
        arrayList.add(new ModelRecyclerView("detail","Berat" ,String.format("%.0f",transaksiAngkut.getTotalBeratEstimasi()),"Kg"));

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.detail_angkut) ,"",""));
        arrayList.add(new ModelRecyclerView("subTotal","Angkut" ,
                "Janjang",
                "Brdl",
                "Berat"
        ));

        Collections.sort(angkutView, new Comparator<Angkut>() {
            @Override
            public int compare(Angkut t0, Angkut t1) {
                String a = t0.getBlock() ;
                String b = t1.getBlock() ;
                return (a.compareTo(b));
            }
        });
        Angkut subTotalAngkutBlock = null;
        int count = 0;
        boolean angkutLangsiran = true;
        for (int i = 0 ; i < angkutView.size(); i++) {
            Angkut angkut = angkutView.get(i);
            count = count + 1;
            String nama = "Manual";
            String nama1 = "0 Jjg";
            String nama2 = "0 Kg";
            String nama3 = "0 Kg";
            String block = "";
            if (angkut.getTransaksiPanen() != null) {
                nama = "TPH";
                if (angkut.getTransaksiPanen().getTph() != null) {
                    nama = angkut.getTransaksiPanen().getTph().getNamaTph() != null ?
                            angkut.getTransaksiPanen().getTph().getNamaTph() : "TPH";
                    nama += angkut.getTransaksiPanen().getTph().getBlock() != null ?
                            "/" + angkut.getTransaksiPanen().getTph().getBlock() : "-";
                    block = angkut.getTransaksiPanen().getTph().getBlock() != null ?
                            angkut.getTransaksiPanen().getTph().getBlock() : "-";
                }
            } else if (angkut.getTph() != null) {
                nama = angkut.getTph().getNamaTph() != null ?
                        angkut.getTph().getNamaTph() : "TPH";
                nama += angkut.getTph().getBlock() != null ?
                        "/" + angkut.getTph().getBlock() : "-";
                block = angkut.getTph().getBlock() != null ?
                        angkut.getTph().getBlock() : "-";
                nama += " Manual";
            } else if (angkut.getTransaksiAngkut() != null) {
                nama = "Langsiran";
                if (angkut.getTransaksiAngkut().getLangsiran() != null) {
                    nama = angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() != null ?
                            angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() : "Langsiran";
                    nama += angkut.getTransaksiAngkut().getLangsiran().getBlock() != null ?
                            "-" + angkut.getTransaksiAngkut().getLangsiran().getBlock() : "-";
                    block = angkut.getBlock() != null ?
                            angkut.getBlock() : "-";
                    nama += "/" + block;
                }
            } else if (angkut.getLangsiran() != null) {
                nama = angkut.getLangsiran().getNamaTujuan() != null ?
                        angkut.getLangsiran().getNamaTujuan() : "Langsiran";
                nama += angkut.getLangsiran().getBlock() != null ?
                        "/" + angkut.getLangsiran().getBlock() : "-";
                block = angkut.getLangsiran().getBlock() != null ?
                        angkut.getLangsiran().getBlock() : "-";
                nama += " Manual";
            }

            if (subTotalAngkutBlock == null) {
                subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
                        angkut.getBrondolan(),
                        block,
                        angkut.getBerat());
            } else if (subTotalAngkutBlock.getBlock().equals(block)) {
                subTotalAngkutBlock.setJanjang(subTotalAngkutBlock.getJanjang() + angkut.getJanjang());
                subTotalAngkutBlock.setBrondolan(subTotalAngkutBlock.getBrondolan() + angkut.getBrondolan());
                subTotalAngkutBlock.setBerat(subTotalAngkutBlock.getBerat() + angkut.getBerat());
            } else if (!subTotalAngkutBlock.getBlock().equals(block)) {
                arrayList.add(new ModelRecyclerView("subTotal", "Block " + subTotalAngkutBlock.getBlock(),
                        String.format("%,d Jjg", subTotalAngkutBlock.getJanjang()),
                        String.format("%,d Kg", subTotalAngkutBlock.getBrondolan()),
                        String.format("%.0f Kg", subTotalAngkutBlock.getBerat())
                ));
                subTotalAngkutBlock = new Angkut(angkut.getJanjang(),
                        angkut.getBrondolan(),
                        block,
                        angkut.getBerat());
                count = 1;
            }

            nama1 = String.format("%,d", angkut.getJanjang()) + "Jjg";
            nama2 = String.format("%,d", angkut.getBrondolan()) + "Kg";
            nama3 = String.format("%.0f", angkut.getBerat()) + "Kg";
            if (!nama.equalsIgnoreCase("manual") && !nama1.equalsIgnoreCase("0Jjg")) {
                angkutLangsiran = false;
                arrayList.add(new ModelRecyclerView("detailisi", count + ". " + nama, nama1, nama2, nama3));
            }
        }

        if(angkutLangsiran){
            for (int i = 0 ; i < transaksiAngkut.getSubTotalAngkuts().size();i++){
                SubTotalAngkut subTotalAngkut = transaksiAngkut.getSubTotalAngkuts().get(i);
                arrayList.add(new ModelRecyclerView("subTotal", "Block " + subTotalAngkut.getBlock(),
                        String.format("%,d Jjg", subTotalAngkut.getJanjang()),
                        String.format("%,d Kg", subTotalAngkut.getBrondolan()),
                        String.format("%.0f Kg", subTotalAngkut.getBerat())
                ));
            }
        }else {
            arrayList.add(new ModelRecyclerView("subTotal", "Block " + subTotalAngkutBlock.getBlock(),
                    String.format("%,d Jjg", subTotalAngkutBlock.getJanjang()),
                    String.format("%,d Kg", subTotalAngkutBlock.getBrondolan()),
                    String.format("%.0f Kg", subTotalAngkutBlock.getBerat())
            ));
        }

        rv.setLayoutManager(new LinearLayoutManager(activity));
        QrItemAdapter adapter = new QrItemAdapter(activity,arrayList);
        rv.setAdapter(adapter);

        shareqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qrHelper.share(activity,rv);
                listrefrence.dismiss();
            }
        });
        listrefrence = WidgetHelper.showListReference(listrefrence,view,activity);
    }

    public void showQrRekonsilasi(TransaksiAngkut transaksiAngkut, QrHelper qrHelper,ArrayList<Angkut> angkutView){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyy HH:mm");
        String tujuan = TransaksiAngkut.PKS;
        if(transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR){
            tujuan = TransaksiAngkut.LANGSIR;
            if(transaksiAngkut.getLangsiran() != null){
                tujuan = transaksiAngkut.getLangsiran().getNamaTujuan();
            }
        }else if (transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_PKS){
            tujuan = TransaksiAngkut.PKS;
            if(transaksiAngkut.getPks() != null){
                tujuan = transaksiAngkut.getPks().getNamaPKS();
            }
        }else{
            tujuan = "KELUAR";
        }
        String compres = toCompres(transaksiAngkut);
        View view = LayoutInflater.from(activity).inflate(R.layout.show_qr,null);
        RelativeLayout isi = (RelativeLayout) view.findViewById(R.id.isi);
//        ImageView iv_qrcode = (ImageView) view.findViewById(R.id.iv_qrcode);
//        TextView tvHeaderQr = (TextView) view.findViewById(R.id.tvHeaderQr);
        Button shareqr = (Button) view.findViewById(R.id.btn_shareqr);
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);

//        tvHeaderQr.setText(activity.getResources().getString(R.string.transaksi_angkut));
//        try {
//            iv_qrcode.setImageBitmap(qrHelper.encodeAsBitmap(compres));
//        } catch (WriterException e) {
//            e.printStackTrace();
//        }

        int angkutTph = 0;
        int angkutTransit = 0;
        int angkutManual = 0;

        for (int i = 0 ; i < angkutView.size(); i++){
            if(angkutView.get(i).getTransaksiPanen() != null){
                angkutTph ++;
            }else if (angkutView.get(i).getTransaksiAngkut() != null){
                angkutTransit++;
            }else{
                angkutManual ++;
            }
        }

        Estate estate = GlobalHelper.getEstateByEstCode(transaksiAngkut.getEstCode());

        ArrayList<ModelRecyclerView> arrayList = new ArrayList<>();
        arrayList.add(new ModelRecyclerView("headerQr",activity.getResources().getString(R.string.rekonsiliasi),"",""));
        arrayList.add(new ModelRecyclerView("Qr",compres,"",""));
        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_rekonsilasi),"",""));
        if(estate != null) {
            arrayList.add(new ModelRecyclerView("detail", "PT", estate.getCompanyShortName(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Estate", estate.getEstName(), ""));
        }else{
            arrayList.add(new ModelRecyclerView("detail", "PT", activity.getResources().getString(R.string.dont_have_estate) + transaksiAngkut.getEstCode(), ""));
            arrayList.add(new ModelRecyclerView("detail", "Estate", activity.getResources().getString(R.string.dont_have_estate) + transaksiAngkut.getEstCode(), ""));
        }

        User userSelected = null;
        if(qrHelper.context instanceof BaseActivity){
            userSelected = GlobalHelper.dataAllUser.get(transaksiAngkut.getCreateBy());
            arrayList.add(new ModelRecyclerView("detail","Input Oleh" ,userSelected.getUserFullName(),""));
        }else{
            arrayList.add(new ModelRecyclerView("detail","Input Oleh" ,transaksiAngkut.getCreateBy(),""));
        }

        if(transaksiAngkut.getSubstitute() != null){
            arrayList.add(new ModelRecyclerView("detail","Krani Penganti" ,transaksiAngkut.getSubstitute().getNama(),""));
        }

        arrayList.add(new ModelRecyclerView("detail","Waktu Input" ,dateFormat.format(transaksiAngkut.getEndDate()),""));
        arrayList.add(new ModelRecyclerView("detail","ID Transaksi" ,transaksiAngkut.getIdTAngkut(),""));
        arrayList.add(new ModelRecyclerView("detail","Tujuan" ,tujuan,""));

        if(transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR){
            if(transaksiAngkut.getLangsiran() != null){
                if(transaksiAngkut.getLangsiran().getNamaTujuan() != null){
                    arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_langsiran),"",""));

                    Estate estateLangsiran = GlobalHelper.getEstateByEstCode(transaksiAngkut.getLangsiran().getEstCode());
                    arrayList.add(new ModelRecyclerView("detail","PT" ,estateLangsiran.getCompanyShortName(),""));
                    arrayList.add(new ModelRecyclerView("detail","Estate" ,estateLangsiran.getEstName(),""));
                    arrayList.add(new ModelRecyclerView("detail","Afdeling" ,transaksiAngkut.getLangsiran().getAfdeling(),""));
                    arrayList.add(new ModelRecyclerView("detail","Block" ,transaksiAngkut.getLangsiran().getBlock(),""));
                    arrayList.add(new ModelRecyclerView("detail","Nama Langsiran" ,transaksiAngkut.getLangsiran().getNamaTujuan(),""));
                    arrayList.add(new ModelRecyclerView("detail","Type Langsiran" ,Langsiran.LIST_TIPE_LANGSIRAN[transaksiAngkut.getLangsiran().getTypeTujuan()],""));
                }
            }
        }else if (transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_PKS) {
            if(transaksiAngkut.getPks() != null){
                if(transaksiAngkut.getPks().getNamaPKS() != null){
                    arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_pks),"",""));

                    Estate estateLangsiran = GlobalHelper.getEstateByEstCode(transaksiAngkut.getPks().getEstCode());
                    arrayList.add(new ModelRecyclerView("detail","PT" ,estateLangsiran.getCompanyShortName(),""));
                    arrayList.add(new ModelRecyclerView("detail","Estate" ,estateLangsiran.getEstName(),""));
                    arrayList.add(new ModelRecyclerView("detail","Afdeling" ,transaksiAngkut.getPks().getAfdeling(),""));
                    arrayList.add(new ModelRecyclerView("detail","Block" ,transaksiAngkut.getPks().getBlock(),""));
                    arrayList.add(new ModelRecyclerView("detail","Nama PKS" ,transaksiAngkut.getPks().getNamaPKS(),""));

                }
            }
        }

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_vehicle),"",""));
        arrayList.add(new ModelRecyclerView("detail","Kendaraan Dari" ,transaksiAngkut.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN ?
                TransaksiAngkut.KEBUN : TransaksiAngkut.LUAR,""));

        Operator operator = transaksiAngkut.getSupir();
        Vehicle vehicle = transaksiAngkut.getVehicle();
        arrayList.add(new ModelRecyclerView("detail","Supir" ,operator != null ? operator.getNama() : "",""));
        arrayList.add(new ModelRecyclerView("detail","Kendaraan" ,vehicle != null ? vehicle.getVehicleCode(): "" ,""));

        if(transaksiAngkut.getBin() != null){
            arrayList.add(new ModelRecyclerView("detail","Bin" ,transaksiAngkut.getBin(),""));
        }

        if(transaksiAngkut.getPengirimanGandas() != null) {
            Collections.sort(transaksiAngkut.getPengirimanGandas(), new Comparator<PengirimanGanda>() {
                @Override
                public int compare(PengirimanGanda o1, PengirimanGanda o2) {
                    return o1.getIdx() < o2.getIdx() ? -1 : (o1.getIdx() > o2.getIdx()) ? 1 : 0;
                }
            });

            arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_pengiriman_lanjut),"",""));
            arrayList.add(new ModelRecyclerView("detailgandaheader",
                    "Unit Ke",
                    "Kepemilikan",
                    "Unit",
                    "Operator"
            ));
            for (int i = 0; i < transaksiAngkut.getPengirimanGandas().size(); i++) {
                PengirimanGanda ganda = transaksiAngkut.getPengirimanGandas().get(i);
                int ke = i + 2;
                arrayList.add(new ModelRecyclerView("detailganda",
                        String.valueOf(ke) ,
                        ganda.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN ? TransaksiAngkut.KEBUN : TransaksiAngkut.LUAR,
                        ganda.getVehicle().getVehicleCode(),
                        ganda.getOperator().getNama()
                ));
            }
        }

        if(transaksiAngkut.getLoader() != null){
            arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_loader),"",""));
            arrayList.add(new ModelRecyclerView("detailloaderheader","Nama" ,"NIK","Estate"));
            for (int i = 0; i < transaksiAngkut.getLoader().size(); i++) {
                operator = transaksiAngkut.getLoader().get(i);
                arrayList.add(new ModelRecyclerView("detailloader",operator.getNama() ,operator.getNik(),operator.getArea()));
            }
        }

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.info_angkut),"",""));
//        arrayList.add(new ModelRecyclerView("detail","Σ Angkut TPH" ,String.valueOf(angkutTph)+" TPH",""));
//        arrayList.add(new ModelRecyclerView("detail","Σ Angkut Langsiran" ,String.valueOf(angkutTransit)+" Langsiran",""));
//        arrayList.add(new ModelRecyclerView("detail","Σ Angkut Manual" ,String.valueOf(angkutManual) +" Manual",""));
        arrayList.add(new ModelRecyclerView("detail","Σ Manual Data" , transaksiAngkut.getAngkuts().size() + " Pengangkutan",""));
        arrayList.add(new ModelRecyclerView("detail","Mulai Angkut" ,dateFormat.format(transaksiAngkut.getStartDate()),""));
        arrayList.add(new ModelRecyclerView("detail","Selesai Angkut" ,dateFormat.format(transaksiAngkut.getEndDate()),""));

        arrayList.add(new ModelRecyclerView("header",activity.getResources().getString(R.string.tbs_tipe).toUpperCase(),"",""));
        int totalKartuRusak = 0;
        int totalKartuHilang = 0;

        for(Angkut angkut : angkutView){
            if(angkut.getManualSPBData() != null){
                if(angkut.getManualSPBData().getTipe() != null){
                    if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                        totalKartuRusak++;
                    }else if(angkut.getManualSPBData().getTipe().equals("Kartu Hilang")){
                        totalKartuHilang++;
                    }
                }
            }
        }
        arrayList.add(new ModelRecyclerView("detail","Kartu Rusak" ,String.format("%,d",totalKartuRusak),""));
        arrayList.add(new ModelRecyclerView("detail","Kartu Hilang" ,String.format("%,d",totalKartuHilang),""));

        arrayList.add(new ModelRecyclerView("header","DETAIL ANGKUT" ,"",""));
        arrayList.add(new ModelRecyclerView("subTotal","TPH" ,
                "Kartu Rusak",
                "Kartu Hilang"
        ));

        Collections.sort(angkutView, new Comparator<Angkut>() {
            @Override
            public int compare(Angkut t0, Angkut t1) {
                String a = t0.getBlock();
                String b = t1.getBlock();
                return (a.compareTo(b));
            }
        });
        Angkut subTotalAngkutBlock = null;
        int count = 0;
        int totalBlockKartuRusak = 0;
        int totalBlockKartuHilang = 0;
        for (int i = 0 ; i < angkutView.size(); i++){
            Angkut angkut = angkutView.get(i);
            if(angkut.getManualSPBData()!=null){
                count = count + 1;
                String nama = "Manual";
                String nama1 = "0";
                String nama2 = "0";
                String block = "";

                int subKartuHilang=0;
                int subKartuRusak=0;
                if(angkut.getTransaksiPanen() != null){
                    nama = "TPH";
                    if(angkut.getTransaksiPanen().getTph() != null){
                        nama = angkut.getTransaksiPanen().getTph().getNamaTph() != null ?
                                angkut.getTransaksiPanen().getTph().getNamaTph() : "TPH";
                        nama += angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                "/" +angkut.getTransaksiPanen().getTph().getBlock() : "-";
                        block = angkut.getTransaksiPanen().getTph().getBlock() != null ?
                                angkut.getTransaksiPanen().getTph().getBlock() : "-";
                    }
                }else if (angkut.getTph() != null) {
                    nama = angkut.getTph().getNamaTph() != null ?
                            angkut.getTph().getNamaTph() : "TPH";
                    nama += angkut.getTph().getBlock() != null ?
                            "/" +angkut.getTph().getBlock() : "-";
                    block = angkut.getTph().getBlock() != null ?
                            angkut.getTph().getBlock() : "-";
//                nama += " Manual";
                }else if (angkut.getTransaksiAngkut() != null){
                    nama = "Langsiran";
                    if(angkut.getTransaksiAngkut().getLangsiran() != null){
                        nama = angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() != null ?
                                angkut.getTransaksiAngkut().getLangsiran().getNamaTujuan() : "Langsiran";
                        nama += angkut.getTransaksiAngkut().getLangsiran().getBlock() != null ?
                                "/" +angkut.getTransaksiAngkut().getLangsiran().getBlock() : "-";
                        block = angkut.getTransaksiAngkut().getLangsiran().getBlock() != null ?
                                angkut.getTransaksiAngkut().getLangsiran().getBlock() : "-";
                    }
                }else if (angkut.getLangsiran() != null) {
                    nama = angkut.getLangsiran().getNamaTujuan() != null ?
                            angkut.getLangsiran().getNamaTujuan(): "Langsiran";
                    nama += angkut.getLangsiran().getBlock() != null ?
                            "/" +angkut.getLangsiran().getBlock(): "-";
                    block = angkut.getLangsiran().getBlock() != null ?
                            angkut.getLangsiran().getBlock(): "-";
//                nama += " Manual";
                }



                if(subTotalAngkutBlock == null){
                    subTotalAngkutBlock = angkut;
                    if(angkut.getManualSPBData()!=null){
                        if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                            totalBlockKartuRusak++;
                        }else{
                            totalBlockKartuHilang++;
                        }
                    }
                }else if(subTotalAngkutBlock.getBlock().equals(block)){
                    if(angkut.getManualSPBData()!=null){
                        if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                            totalBlockKartuRusak++;
                        }else{
                            totalBlockKartuHilang++;
                        }
                    }
                }else if(!subTotalAngkutBlock.getBlock().equals(block)){
                    arrayList.add(new ModelRecyclerView("subTotal","Block "+subTotalAngkutBlock.getBlock() ,
                            String.format("%,d", totalBlockKartuRusak),
                            String.format("%,d", totalBlockKartuHilang)));
                    Log.i("getManualSPBData", "totalBlockKartuRusak: "+totalBlockKartuRusak+" totalBlockKartuHilang: "+totalBlockKartuHilang);
                    totalBlockKartuRusak = 0;
                    totalBlockKartuHilang = 0;
                    subTotalAngkutBlock = angkut;
                    if(subTotalAngkutBlock.getManualSPBData().getTipe().equals("Kartu Rusak")){
                        totalBlockKartuRusak++;
                    }else{
                        totalBlockKartuHilang++;
                    }
                    count = 1;
                }

                if(angkut.getManualSPBData().getTipe().equals("Kartu Rusak")){
                    subKartuRusak = 1;
                    subKartuHilang = 0;
                }else{
                    subKartuRusak = 0;
                    subKartuHilang = 1;
                }

                nama1 = String.format("%,d",subKartuRusak) ;
                nama2 = String.format("%,d",subKartuHilang) ;
                arrayList.add(new ModelRecyclerView("detailisi", count + ". "+nama,nama1,nama2));
                if(i == transaksiAngkut.getAngkuts().size()-1){
                    arrayList.add(new ModelRecyclerView("subTotal","Block "+subTotalAngkutBlock.getBlock() ,
                            String.format("%,d", totalBlockKartuRusak),
                            String.format("%,d", totalBlockKartuHilang)));
                    Log.i("getManualSPBData", "totalBlockKartuRusak: "+totalBlockKartuRusak+" totalBlockKartuHilang: "+totalBlockKartuHilang);
                }
            }

        }

        arrayList.add(new ModelRecyclerView("subTotal","Grand Total" ,
                String.format("%,d",totalKartuRusak),
                String.format("%,d",totalKartuHilang)
        ));

        rv.setLayoutManager(new LinearLayoutManager(activity));
        QrItemAdapter adapter = new QrItemAdapter(activity,arrayList);
        rv.setAdapter(adapter);

        shareqr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                qrHelper.share(activity,rv);
                listrefrence.dismiss();
            }
        });
        listrefrence = WidgetHelper.showListReference(listrefrence,view,activity);
    }

    public String toCompres(TransaksiAngkut transaksiAngkut){
        SimpleDateFormat sdfT = new SimpleDateFormat("HHmm");
        try {

            JSONArray array = new JSONArray();
            for(int i = 0 ; i < transaksiAngkut.getAngkuts().size(); i++){
//                array.put(transaksiAngkut.getAngkuts().get(i).getIdAngkut());
                if(!transaksiAngkut.getAngkuts().get(i).getOph().equalsIgnoreCase(Angkut.MANUAL)) {
                    array.put(transaksiAngkut.getAngkuts().get(i).getOph() + ";"
                            + transaksiAngkut.getAngkuts().get(i).getBlock() + ";"
                            + Integer.toHexString(transaksiAngkut.getAngkuts().get(i).getJanjang()) + ";"
                            + Integer.toHexString(transaksiAngkut.getAngkuts().get(i).getBrondolan()) + ";"
                            + Integer.toHexString((int) Math.round(transaksiAngkut.getAngkuts().get(i).getBerat())) + ";"
                            + Integer.toHexString(transaksiAngkut.getAngkuts().get(i).getIdxAngkut())
                    );
                }
            }

            JSONArray latlon = new JSONArray();
            latlon.put(String.format("%.5f",transaksiAngkut.getLatitudeTujuan()));
            latlon.put(String.format("%.5f",transaksiAngkut.getLongitudeTujuan()));

            String  tujuan = String.valueOf(transaksiAngkut.getTujuan());
            if(transaksiAngkut.getTujuan() == TransaksiAngkut.TUJUAN_LANGSIR) {
                if (transaksiAngkut.getLangsiran() != null) {
                    tujuan = tujuan + "_" + transaksiAngkut.getLangsiran().getIdTujuan();
                }
            }

            if(transaksiAngkut.getSubTotalAngkuts() == null){
                transaksiAngkut.setSubTotalAngkuts(getSubTotalAngkut(transaksiAngkut.getAngkuts()));
            }
            JSONArray arraySubTotal = new JSONArray();
            for(int i = 0; i < transaksiAngkut.getSubTotalAngkuts().size();i++){
                SubTotalAngkut subTotalAngkut = transaksiAngkut.getSubTotalAngkuts().get(i);
                int berat = (int) Math.round(subTotalAngkut.getBerat());
                arraySubTotal.put(
                        subTotalAngkut.getBlock()+";"+
                        Integer.toHexString(subTotalAngkut.getJanjang())+";"+
                        Integer.toHexString(subTotalAngkut.getBrondolan())+";"+
                        Integer.toHexString(berat)
                );
            }
            int berat = (int) Math.round(transaksiAngkut.getTotalBeratEstimasi());
            JSONObject object = new JSONObject();
            object.put("a",transaksiAngkut.getIdTAngkut());
            object.put("b",Integer.toHexString(transaksiAngkut.getTotalJanjang()) +
                    "_"+ Integer.toHexString(transaksiAngkut.getTotalBrondolan()) +
                    "_"+ Integer.toHexString(berat)
            );

            String isiC = "";
            if(transaksiAngkut.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN && transaksiAngkut.getVehicle() != null && transaksiAngkut.getSupir() != null){
                isiC = transaksiAngkut.getEstCode() +
                        ";"+ transaksiAngkut.getVehicle().getVraOrderNumber() +
                        ";"+ tujuan +
                        ";"+ transaksiAngkut.getKendaraanDari()+
                        ";"+ transaksiAngkut.getSupir().getKodeOperator();
            }else{
                isiC = transaksiAngkut.getEstCode() +
                        ";"+ transaksiAngkut.getVehicle().getVehicleCode() +
                        ";"+ tujuan +
                        ";"+ transaksiAngkut.getKendaraanDari()+
                        ";"+ transaksiAngkut.getSupir().getNama();
            }
            object.put("c",isiC);
            object.put("d",Integer.toHexString(Integer.parseInt(sdfT.format(transaksiAngkut.getEndDate()))));
            object.put("e",array);
            object.put("f",latlon);
            object.put("g",arraySubTotal);
            if(transaksiAngkut.getSubstitute() != null){
                object.put("h",transaksiAngkut.getSubstitute().getNik() + "_"+ transaksiAngkut.getSubstitute().getEstate());
            }

            if(transaksiAngkut.getLoader() != null){
                String iString = "";
                for (int i = 0 ; i < transaksiAngkut.getLoader().size();i++){
                    iString += transaksiAngkut.getLoader().get(i).getKodeOperator() + ";";
                }
                object.put("i",iString);
            }

            if(transaksiAngkut.getPengirimanGandas() != null){
                JSONArray jsonArray = new JSONArray();
                Collections.sort(transaksiAngkut.getPengirimanGandas(), new Comparator<PengirimanGanda>() {
                    @Override
                    public int compare(PengirimanGanda o1, PengirimanGanda o2) {
                        return o1.getIdx() < o2.getIdx() ? -1 : (o1.getIdx() > o2.getIdx()) ? 1 : 0;
                    }
                });
                for (int i = 0 ; i < transaksiAngkut.getPengirimanGandas().size();i++){
                    PengirimanGanda ganda = transaksiAngkut.getPengirimanGandas().get(i);
                    String isiGanda = ganda.getIdx()+ ";"+
                            ganda.getKendaraanDari() + ";" +
                            ganda.getVehicle().getVehicleCode() + ";";

                    if(ganda.getKendaraanDari() == TransaksiAngkut.KENDARAAN_KEBUN){
                        isiGanda += ganda.getOperator().getKodeOperator();
                    }else {
                        isiGanda += ganda.getOperator().getNama();
                    }
                    jsonArray.put(isiGanda);
                }
                object.put("j",jsonArray);
            }

            object.put("z",GlobalHelper.TYPE_NFC_HIJAU);

            Log.d("json object",object.toString());

            return GlobalHelper.compress(object.toString());

        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static TransaksiAngkut toDecompre(JSONObject value){
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmm");
        SimpleDateFormat sdfD = new SimpleDateFormat("ddMMyy");
        SimpleDateFormat sdfT = new SimpleDateFormat("HHmm");
        try {
            TransaksiAngkut transaksiAngkut = new TransaksiAngkut();
            ArrayList<Angkut> angkuts = new ArrayList<>();

            JSONArray eObject = (JSONArray) value.get("e");
            JSONArray fObject = (JSONArray) value.get("f");
            for (int i = 0 ; i < eObject.length(); i++){
//                angkuts.add(new Angkut(eObject.get(i).toString(),
//                          sdf.parse(eObject.get(i).toString().substring(5)).getTime()));
                String [] aAngkut = eObject.get(i).toString().split(";");
                angkuts.add(new Angkut(aAngkut[0],
                        aAngkut[1],
                        (int) Long.parseLong(aAngkut[2], 16),
                        (int) Long.parseLong(aAngkut[3], 16),
                        Double.parseDouble(String.valueOf((int) Long.parseLong(aAngkut[4], 16))),
                        (int) Long.parseLong(aAngkut[5], 16)
                ));
            }
            transaksiAngkut.setLatitudeTujuan(Double.parseDouble(fObject.getString(0).replace(",",".")));
            transaksiAngkut.setLongitudeTujuan(Double.parseDouble(fObject.getString(1).replace(",",".")));
            transaksiAngkut.setAngkuts(angkuts);
            transaksiAngkut.setIdTAngkut(value.getString("a"));
            transaksiAngkut.setCreateBy(value.getString("a").substring(11));
            transaksiAngkut.setStartDate(sdfD.parse(value.getString("a").substring(5,11)).getTime());
            transaksiAngkut.setEndDate(sdf.parse(value.getString("a").substring(5,11) + String.format("%04d", (int) Long.parseLong(value.getString("d"), 16))).getTime());

            String [] janjangBrondolan = value.getString("b").split("_");
            transaksiAngkut.setTotalJanjang((int) Long.parseLong(janjangBrondolan[0], 16));
            transaksiAngkut.setTotalBrondolan((int) Long.parseLong(janjangBrondolan[1], 16));
            if(janjangBrondolan.length > 2) {
                int berat = (int) Long.parseLong(janjangBrondolan[2], 16);
                transaksiAngkut.setTotalBeratEstimasi((double) berat);
            }

            String [] estVehicleTujuan = value.getString("c").split(";");

            transaksiAngkut.setEstCode(estVehicleTujuan[0]);
//            transaksiAngkut.setVehicle(estVehicleTujuan[1]);

            String [] tujuan = estVehicleTujuan[2].split("_");
            transaksiAngkut.setTujuan(Integer.parseInt(tujuan[0]));

            if(tujuan[0].equals(String.valueOf(TransaksiAngkut.TUJUAN_LANGSIR))){
                Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_LANGSIRAN);
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", tujuan[1]));
                for(DataNitrit dataNitrit : cursor){
                    Gson gson = new Gson();
                    Langsiran langsiran = gson.fromJson(dataNitrit.getValueDataNitrit(),Langsiran.class);
                    transaksiAngkut.setLangsiran(langsiran);
                    break;
                }
                db.close();
            }

            transaksiAngkut.setKendaraanDari(Integer.parseInt(estVehicleTujuan[3]));
            if(Integer.parseInt(estVehicleTujuan[3]) == TransaksiAngkut.KENDARAAN_KEBUN){
                Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_OPERATOR);
                ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", estVehicleTujuan[4]));
                for(DataNitrit dataNitrit : cursor){
                    Gson gson = new Gson();
                    transaksiAngkut.setSupir(gson.fromJson(dataNitrit.getValueDataNitrit(),Operator.class));
                    break;
                }
                db.close();

                db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_VEHICLE);
                repository = db.getRepository(DataNitrit.class);
                cursor = repository.find(ObjectFilters.eq("idDataNitrit", estVehicleTujuan[1]));
                for(DataNitrit dataNitrit : cursor){
                    Gson gson = new Gson();
                    transaksiAngkut.setVehicle(gson.fromJson(dataNitrit.getValueDataNitrit(),Vehicle.class));
                    break;
                }
                db.close();
            }else{
                Operator operator = new Operator();
                Vehicle vehicle = new Vehicle();

                operator.setNama(estVehicleTujuan[4]);
                vehicle.setVehicleCode(estVehicleTujuan[1]);

                transaksiAngkut.setSupir(operator);
                transaksiAngkut.setVehicle(vehicle);
            }

            if(value.has("g")){
                ArrayList<SubTotalAngkut> subTotalAngkuts = new ArrayList<>();
                JSONArray gObject = (JSONArray) value.get("g");
                for (int i = 0 ; i < gObject.length(); i++){
                    String [] gSplit = String.valueOf(gObject.get(i)).split(";");
                    if(gSplit.length == 4) {
                        int berat = (int) Long.parseLong(gSplit[3], 16);
                        subTotalAngkuts.add(new SubTotalAngkut(
                                gSplit[0],
                                (int) Long.parseLong(gSplit[1], 16),
                                (int) Long.parseLong(gSplit[2], 16),
                                (double) berat
                        ));
                    }
                }
                transaksiAngkut.setSubTotalAngkuts(subTotalAngkuts);
            }

            SupervisionList substitute = null;
            if(value.has("h")){
                String [] hArray = value.get("h").toString().split("_");
                if(hArray.length == 2) {
                    Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TPANENSUPERVISIONLIST);
                    ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                    Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", hArray[0]));
                    for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                        DataNitrit dataNitrit = (DataNitrit) iterator.next();
                        Gson gson = new Gson();
                        substitute = gson.fromJson(dataNitrit.getValueDataNitrit(), SupervisionList.class);
                    }
                    db.close();
                }
                transaksiAngkut.setSubstitute(substitute);
            }

            if(value.has("i")){
                ArrayList<Operator> loader = new ArrayList<>();
                String [] iArray = value.get("i").toString().split(";");
                for (int i = 0 ; i < iArray.length; i++){
                    Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_OPERATOR);
                    ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                    Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", iArray[i]));
                    for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                        DataNitrit dataNitrit = (DataNitrit) iterator.next();
                        Gson gson = new Gson();
                        loader.add(gson.fromJson(dataNitrit.getValueDataNitrit(), Operator.class));
                    }
                    db.close();
                }
                transaksiAngkut.setLoader(loader);
            }

            if(value.has("j")){
                ArrayList<PengirimanGanda> pengirimanGandas = new ArrayList<>();
                JSONArray jArray = value.getJSONArray("j");
                for (int i = 0 ; i < jArray.length(); i++){
                    String [] jSplit = jArray.get(i).toString().split(";");
                    Operator operator = new Operator();
                    Vehicle vehicle = new Vehicle();
                    if(Integer.parseInt(jSplit[1]) == TransaksiAngkut.KENDARAAN_KEBUN){
                        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABEL_OPERATOR);
                        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
                        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", jSplit[3]));
                        for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                            DataNitrit dataNitrit = (DataNitrit) iterator.next();
                            Gson gson = new Gson();
                            operator = gson.fromJson(dataNitrit.getValueDataNitrit(), Operator.class);
                        }
                        db.close();

                        vehicle.setVehicleCode(jSplit[2]);
                        pengirimanGandas.add(new PengirimanGanda(
                                Integer.parseInt(jSplit[0]),
                                Integer.parseInt(jSplit[1]),
                                operator,
                                vehicle
                        ));
                    }else{
                        operator.setNama(jSplit[3]);
                        vehicle.setVehicleCode(jSplit[2]);
                        pengirimanGandas.add(new PengirimanGanda(
                                Integer.parseInt(jSplit[0]),
                                Integer.parseInt(jSplit[1]),
                                operator,
                                vehicle
                        ));
                    }
                }
                transaksiAngkut.setPengirimanGandas(pengirimanGandas);
            }

            return transaksiAngkut;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void showHistoryTransaksi(TransaksiAngkut transaksiAngkut){
        TransaksiAngkut transaksiAngkut1 = null;
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_TRANSAKSI_ANGKUT);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", transaksiAngkut.getIdTAngkut()));
        if (cursor.size() > 0) {
            for (Iterator iterator = cursor.iterator(); iterator.hasNext(); ) {
                DataNitrit dataNitrit = (DataNitrit) iterator.next();
                Gson gson = new Gson();
                transaksiAngkut1 = gson.fromJson(dataNitrit.getValueDataNitrit(), TransaksiAngkut.class);
                break;
            }
        }
        db.close();

        View view = LayoutInflater.from(activity).inflate(R.layout.list_refrensi_object,null);
        TextView idHeader = (TextView) view.findViewById(R.id.idHeader);
        EditText etSearch_lso = (EditText) view.findViewById(R.id.et_search_lso);
        RecyclerView rv_usage_lso = (RecyclerView) view.findViewById(R.id.rv_usage_lso);

        idHeader.setText(activity.getResources().getString(R.string.chose_transaksi_version));
        rv_usage_lso.setLayoutManager(new LinearLayoutManager(activity));

        ArrayList<TranskasiAngkutVersion> transkasiAngkutVersions = new ArrayList<>();
        TranskasiAngkutVersion angkutVersionX = null;
        if(transaksiAngkut1!= null){
            transkasiAngkutVersions.add(new TranskasiAngkutVersion(0,transaksiAngkut1));
        }
        for(int i = 0 ;i< transaksiAngkut.getTranskasiAngkutVersions().size();i++) {
            TranskasiAngkutVersion angkutVersion = transaksiAngkut.getTranskasiAngkutVersions().get(i);

            if(angkutVersionX != null){
                if(angkutVersionX.getNextIdTransaksi() != null && angkutVersion.getNextIdTransaksi() == null){
                    transkasiAngkutVersions.add(angkutVersionX);
                }
            }

            if (angkutVersion.getNextIdTransaksi() == null) {
                transkasiAngkutVersions.add(transaksiAngkut.getTranskasiAngkutVersions().get(i));
            }
            angkutVersionX = angkutVersion;
        }

        final SelectedTransaksiAngkutVersion adapter = new SelectedTransaksiAngkutVersion(activity,transkasiAngkutVersions);
        final SlideInLeftAnimationAdapter adapterLeft = new SlideInLeftAnimationAdapter(adapter);
        adapterLeft.setFirstOnly(false);
//       adapterLeft.setInterpolator(new OvershootInterpolator(1f));
        adapterLeft.setDuration(400);
        etSearch_lso.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                adapter.getFilter().filter(s.toString());
            }
        });
        rv_usage_lso.setAdapter(adapterLeft);

        listrefrence = WidgetHelper.showListReference(listrefrence,view,activity);
    }

    public boolean updateDataNFC(TransaksiAngkut transaksiAngkutFromNFC,String valueNFC){
        try {
            if(valueNFC != null){
                if(valueNFC.equals(GlobalHelper.FORMAT_NFC_ANGKUT)){
                    return true;
                }else{
                    TransaksiAngkut transaksiAngkut = toDecompre(new JSONObject(valueNFC));
                    if(transaksiAngkut.getIdTAngkut().equals(transaksiAngkutFromNFC.getIdTAngkut())){
                        NfcHelper.write(GlobalHelper.FORMAT_NFC_ANGKUT, ((AngkutActivity) activity).myTag,true);
                        return true;
                    }else{
                        Toast.makeText(HarvestApp.getContext(),"Kartu Angkut Yang Anda Tap Bukan Karu Yang Sama",Toast.LENGTH_LONG).show();
                        return false;
                    }
                }
            }else{
                Toast.makeText(HarvestApp.getContext(),"ISI NFC Tidak TerBaca",Toast.LENGTH_LONG);
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public HashMap<String,Angkut> setDataView(ArrayList<Angkut> angkuts){
        HashMap<String,Angkut> angkutView = new HashMap<>();
        if(angkuts.size() > 0) {
            angkutView = new HashMap<>();
            Collections.sort(angkuts, new Comparator<Angkut>() {
                @Override
                public int compare(Angkut o1, Angkut o2) {
                    String so1 = o1.getIdAngkutRef() != null ? o1.getIdAngkutRef() : o1.getIdAngkut();
                    String so2 = o2.getIdAngkutRef() != null ? o2.getIdAngkutRef() : o2.getIdAngkut();
                    return so1.compareTo(so2);
                }
            });

            Angkut angkutX = null;
            int ophCount = 0;
            for(Angkut angkut : angkuts){
                ophCount ++;
                if(angkut.getIdAngkutRef() == null){
                    angkut.setIdAngkutRef(angkut.getIdAngkut());
                }
                if(angkut.getBlock() == null){
                    angkut.setBlock("");
                }
                if(angkutX == null){
                    angkutX = new Angkut(
                            angkut.getIdAngkut(),
                            angkut.getWaktuAngkut(),
                            angkut.getJanjang(),
                            angkut.getBrondolan(),
                            angkut.getBerat(),
                            angkut.getBjr(),
                            angkut.getBlock(),
                            angkut.getIdAngkutRef(),
                            ophCount,
                            angkut.getIdNfc(),
                            angkut.getTransaksiPanen(),
                            angkut.getTransaksiAngkut(),
                            angkut.getTph(),
                            angkut.getLangsiran(),
                            angkut.getManualSPBData()
                    );
                }else if(!angkutX.getIdAngkutRef().equals(angkut.getIdAngkutRef())){
                    angkutView.put(angkutX.getIdAngkut(),angkutX);
                    ophCount = 1;
                    angkutX = new Angkut(
                            angkut.getIdAngkut(),
                            angkut.getWaktuAngkut(),
                            angkut.getJanjang(),
                            angkut.getBrondolan(),
                            angkut.getBerat(),
                            angkut.getBjr(),
                            angkut.getBlock(),
                            angkut.getIdAngkutRef(),
                            ophCount,
                            angkut.getIdNfc(),
                            angkut.getTransaksiPanen(),
                            angkut.getTransaksiAngkut(),
                            angkut.getTph(),
                            angkut.getLangsiran(),
                            angkut.getManualSPBData()
                    );
                }else{
                    angkutX = new Angkut(
                            angkutX.getIdAngkut(),
                            angkutX.getWaktuAngkut(),
                            angkut.getJanjang() + angkutX.getJanjang(),
                            angkut.getBrondolan() + angkutX.getBrondolan(),
                            angkut.getBerat() + angkutX.getBerat(),
                            angkut.getBjr(),
                            angkut.getBlock(),
                            angkut.getIdAngkutRef(),
                            ophCount,
                            angkut.getIdNfc(),
                            angkut.getTransaksiPanen(),
                            angkut.getTransaksiAngkut(),
                            angkut.getTph(),
                            angkut.getLangsiran(),
                            angkut.getManualSPBData()
                    );
                }
            }

            if(!angkutX.getIdAngkut().isEmpty()){
                angkutView.put(angkutX.getIdAngkut(),angkutX);
            }
        }
        return angkutView;
    }

    public static ArrayList<SubTotalAngkut> getSubTotalAngkut(ArrayList<Angkut> angkuts){
        Collections.sort(angkuts, new Comparator<Angkut>() {
            @Override
            public int compare(Angkut t0, Angkut t1) {
                String block0 = "";
                String block1 = "";
                if(t0.getTransaksiPanen() != null){
                    block0 = t0.getTransaksiPanen().getTph().getBlock() != null ?
                            t0.getTransaksiPanen().getTph().getBlock() : "-";
                }else if (t0.getTph() != null) {
                    block0 = t0.getTph().getBlock() != null ?
                            t0.getTph().getBlock() : "-";
                }else if (t0.getTransaksiAngkut() != null){
                    block0 = t0.getBlock() != null ?
                            t0.getBlock() : "-";
                }else if (t0.getLangsiran() != null) {
                    block0 = t0.getLangsiran().getBlock() != null ?
                            t0.getLangsiran().getBlock(): "-";
                }

                if(t1.getTransaksiPanen() != null){
                    block1 = t1.getTransaksiPanen().getTph().getBlock() != null ?
                            t1.getTransaksiPanen().getTph().getBlock() : "-";
                }else if (t1.getTph() != null) {
                    block1 = t1.getTph().getBlock() != null ?
                            t1.getTph().getBlock() : "-";
                }else if (t1.getTransaksiAngkut() != null){
                    block1 = t1.getBlock() != null ?
                            t1.getBlock() : "-";
                }else if (t1.getLangsiran() != null) {
                    block1 = t1.getLangsiran().getBlock() != null ?
                            t1.getLangsiran().getBlock(): "-";
                }
                return block0.compareTo(block1);
            }
        });

        ArrayList<SubTotalAngkut> subTotalAngkuts = new ArrayList<>();
        SubTotalAngkut subTotalAngkut = null;

        for (int i = 0 ; i < angkuts.size(); i++){
            String block = "";
            if(angkuts.get(i).getTransaksiPanen() != null){
                if(angkuts.get(i).getTransaksiPanen().getTph() != null){
                    block = angkuts.get(i).getTransaksiPanen().getTph().getBlock() != null ?
                            angkuts.get(i).getTransaksiPanen().getTph().getBlock() : "-";
                }
            }else if (angkuts.get(i).getTph() != null) {
                block = angkuts.get(i).getTph().getBlock() != null ?
                        angkuts.get(i).getTph().getBlock() : "-";
            }else if (angkuts.get(i).getTransaksiAngkut() != null){
                block = angkuts.get(i).getBlock() != null ?
                        angkuts.get(i).getBlock() : "-";
            }else if (angkuts.get(i).getLangsiran() != null) {
                block = angkuts.get(i).getLangsiran().getBlock() != null ?
                        angkuts.get(i).getLangsiran().getBlock(): "-";
            }

            if(block != "") {
                if (subTotalAngkut == null) {
                    subTotalAngkut = new SubTotalAngkut(block,
                            angkuts.get(i).getJanjang(),
                            angkuts.get(i).getBrondolan(),
                            angkuts.get(i).getBerat());
                } else if (subTotalAngkut.getBlock().equals(block)) {
                    subTotalAngkut.setJanjang(subTotalAngkut.getJanjang() + angkuts.get(i).getJanjang());
                    subTotalAngkut.setBrondolan(subTotalAngkut.getBrondolan() + angkuts.get(i).getBrondolan());
                    subTotalAngkut.setBerat(subTotalAngkut.getBerat() + angkuts.get(i).getBerat());
                } else if (!subTotalAngkut.getBlock().equals(block)) {
                    subTotalAngkuts.add(subTotalAngkut);
                    subTotalAngkut = new SubTotalAngkut(block,
                            angkuts.get(i).getJanjang(),
                            angkuts.get(i).getBrondolan(),
                            angkuts.get(i).getBerat());
                }
            }
        }
        subTotalAngkuts.add(subTotalAngkut);
        return subTotalAngkuts;
    }

    public static ArrayList<SubTotalAngkut> getSubTotalMekanisasi(ArrayList<SubTotalAngkutMekanisasi> angkuts){
        Collections.sort(angkuts, new Comparator<SubTotalAngkutMekanisasi>() {
            @Override
            public int compare(SubTotalAngkutMekanisasi t0, SubTotalAngkutMekanisasi t1) {
                String block0 = "";
                String block1 = "";
                if(t0.getTransaksiPanen() != null) {
                    if (t0.getTransaksiPanen().getTph() != null) {
                        block0 = t0.getTransaksiPanen().getTph().getBlock() != null ?
                                t0.getTransaksiPanen().getTph().getBlock() : "-";
                    }
                }else if(t0.getAngkut() != null) {
                    if (t0.getAngkut().getTransaksiPanen() != null) {
                        block0 = t0.getAngkut().getTransaksiPanen().getTph().getBlock() != null ?
                                t0.getAngkut().getTransaksiPanen().getTph().getBlock() : "-";
                    } else if (t0.getAngkut().getTph() != null) {
                        block0 = t0.getAngkut().getTph().getBlock() != null ?
                                t0.getAngkut().getTph().getBlock() : "-";
                    } else if (t0.getAngkut().getTransaksiAngkut() != null) {
                        block0 = t0.getAngkut().getBlock() != null ?
                                t0.getAngkut().getBlock() : "-";
                    } else if (t0.getAngkut().getLangsiran() != null) {
                        block0 = t0.getAngkut().getLangsiran().getBlock() != null ?
                                t0.getAngkut().getLangsiran().getBlock() : "-";
                    }
                }

                if(t1.getTransaksiPanen() != null) {
                    if (t1.getTransaksiPanen().getTph() != null) {
                        block1 = t1.getTransaksiPanen().getTph().getBlock() != null ?
                                t1.getTransaksiPanen().getTph().getBlock() : "-";
                    }
                }else if(t1.getAngkut() != null) {
                    if (t1.getAngkut().getTransaksiPanen() != null) {
                        block1 = t1.getAngkut().getTransaksiPanen().getTph().getBlock() != null ?
                                t1.getAngkut().getTransaksiPanen().getTph().getBlock() : "-";
                    } else if (t1.getAngkut().getTph() != null) {
                        block1 = t1.getAngkut().getTph().getBlock() != null ?
                                t1.getAngkut().getTph().getBlock() : "-";
                    } else if (t1.getAngkut().getTransaksiAngkut() != null) {
                        block1 = t1.getAngkut().getBlock() != null ?
                                t1.getAngkut().getBlock() : "-";
                    } else if (t1.getAngkut().getLangsiran() != null) {
                        block1 = t1.getAngkut().getLangsiran().getBlock() != null ?
                                t1.getAngkut().getLangsiran().getBlock() : "-";
                    }
                }
                return block0.compareTo(block1);
            }
        });

        ArrayList<SubTotalAngkut> subTotalAngkuts = new ArrayList<>();
        SubTotalAngkut subTotalAngkut = null;

        for (int i = 0 ; i < angkuts.size(); i++){
            String block = "";
            if(angkuts.get(i).getTransaksiPanen() != null) {
                if (angkuts.get(i).getTransaksiPanen().getTph() != null) {
                    block = angkuts.get(i).getTransaksiPanen().getTph().getBlock() != null ?
                            angkuts.get(i).getTransaksiPanen().getTph().getBlock() : "-";
                }
            }else if (angkuts.get(i).getAngkut() != null){
                if(angkuts.get(i).getAngkut().getTransaksiPanen() != null){
                    if(angkuts.get(i).getAngkut().getTransaksiPanen().getTph() != null){
                        block = angkuts.get(i).getAngkut().getTransaksiPanen().getTph().getBlock() != null ?
                                angkuts.get(i).getAngkut().getTransaksiPanen().getTph().getBlock() : "-";
                    }
                }else if (angkuts.get(i).getAngkut().getTph() != null) {
                    block = angkuts.get(i).getAngkut().getTph().getBlock() != null ?
                            angkuts.get(i).getAngkut().getTph().getBlock() : "-";
                }else if (angkuts.get(i).getAngkut().getTransaksiAngkut() != null){
                    block = angkuts.get(i).getAngkut().getBlock() != null ?
                            angkuts.get(i).getAngkut().getBlock() : "-";
                }else if (angkuts.get(i).getAngkut().getLangsiran() != null) {
                    block = angkuts.get(i).getAngkut().getLangsiran().getBlock() != null ?
                            angkuts.get(i).getAngkut().getLangsiran().getBlock(): "-";
                }
            }

            if(block != "") {
                double beratEstimasi = 0;
                // mekanisasi
                if(angkuts.get(i).getTransaksiPanen() != null) {
                    if (angkuts.get(i).getTransaksiPanen().getHasilPanen() != null) {
                        beratEstimasi = angkuts.get(i).getTransaksiPanen().getHasilPanen().getTotalJanjang() * angkuts.get(i).getTransaksiPanen().getBjrMekanisasi();

                        if (subTotalAngkut == null) {
                            subTotalAngkut = new SubTotalAngkut(block,
                                    angkuts.get(i).getTransaksiPanen().getHasilPanen().getTotalJanjang(),
                                    angkuts.get(i).getTransaksiPanen().getHasilPanen().getBrondolan(),
                                    beratEstimasi);
                        } else if (subTotalAngkut.getBlock().equals(block)) {
                            subTotalAngkut.setJanjang(subTotalAngkut.getJanjang() + angkuts.get(i).getTransaksiPanen().getHasilPanen().getTotalJanjang());
                            subTotalAngkut.setBrondolan(subTotalAngkut.getBrondolan() + angkuts.get(i).getTransaksiPanen().getHasilPanen().getBrondolan());
                            subTotalAngkut.setBerat(subTotalAngkut.getBerat() + beratEstimasi);
                        } else if (!subTotalAngkut.getBlock().equals(block)) {
                            subTotalAngkuts.add(subTotalAngkut);
                            subTotalAngkut = new SubTotalAngkut(block,
                                    angkuts.get(i).getTransaksiPanen().getHasilPanen().getTotalJanjang(),
                                    angkuts.get(i).getTransaksiPanen().getHasilPanen().getBrondolan(),
                                    beratEstimasi);
                        }
                    }
                }else if (angkuts.get(i).getAngkut() != null){
                    //tap lama
                    if (subTotalAngkut == null) {
                        subTotalAngkut = new SubTotalAngkut(block,
                                angkuts.get(i).getAngkut().getJanjang(),
                                angkuts.get(i).getAngkut().getBrondolan(),
                                angkuts.get(i).getAngkut().getBerat());
                    } else if (subTotalAngkut.getBlock().equals(block)) {
                        subTotalAngkut.setJanjang(subTotalAngkut.getJanjang() + angkuts.get(i).getAngkut().getJanjang());
                        subTotalAngkut.setBrondolan(subTotalAngkut.getBrondolan() + angkuts.get(i).getAngkut().getBrondolan());
                        subTotalAngkut.setBerat(subTotalAngkut.getBerat() + angkuts.get(i).getAngkut().getBerat());
                    } else if (!subTotalAngkut.getBlock().equals(block)) {
                        subTotalAngkuts.add(subTotalAngkut);
                        subTotalAngkut = new SubTotalAngkut(block,
                                angkuts.get(i).getAngkut().getJanjang(),
                                angkuts.get(i).getAngkut().getBrondolan(),
                                angkuts.get(i).getAngkut().getBerat());
                    }
                }
            }
        }
        subTotalAngkuts.add(subTotalAngkut);
        return subTotalAngkuts;
    }

    public static ArrayList<MekanisasiPanen> getMekanisasiPanenFromAngkuts(HashMap<String,TransaksiPanen> originAngkut){
        ArrayList<MekanisasiPanen> mekanisasiPanens = new ArrayList<>();
        if(originAngkut.size() > 0){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            MekanisasiPanen mekanisasiPanen = new MekanisasiPanen();
            ArrayList<TransaksiPanen> arrayList = new ArrayList<>(originAngkut.values());
            mekanisasiPanen.setIdMekanisasi("MP-"+System.currentTimeMillis()+"-"+GlobalHelper.getUser().getUserID());
            mekanisasiPanen.setDate(sdf.format(System.currentTimeMillis()));
            mekanisasiPanen.setCreateBy(GlobalHelper.getUser().getUserID());
            mekanisasiPanen.setEstCode(GlobalHelper.getEstate().getEstCode());
            mekanisasiPanen.setTransaksiPanens(arrayList);
            mekanisasiPanens.add(mekanisasiPanen);
        }
        return mekanisasiPanens;
    }

    public static ArrayList<MekanisasiPanen> getMekanisaiPanen(){

        ArrayList<MekanisasiPanen> mekanisasiPanens = new ArrayList<>();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitrit = (DataNitrit) iterator.next();
            Gson gson = new Gson();
            try {
                MekanisasiPanen mekanisasiPanen = gson.fromJson(dataNitrit.getValueDataNitrit(), MekanisasiPanen.class);
                if (mekanisasiPanen.getIdMekanisasi() != null) {
                    mekanisasiPanens.add(mekanisasiPanen);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        db.close();
        return mekanisasiPanens;
    }

    public static TransaksiPanen cekIdTPanenHasBeenTap(String idTpanen){
        File file = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT) ,"PANEN_TAP");
        String sData = GlobalHelper.readFileContent(file.getAbsolutePath());
        if(sData.isEmpty()){
            return null;
        }
        Gson gson = new Gson();
        HashMap<String, TransaksiPanen> hashMapTPanen = gson.fromJson(sData, HashMap.class);
        return gson.fromJson(gson.toJson(hashMapTPanen.get(idTpanen),Map.class),TransaksiPanen.class);
    }

    public static TransaksiAngkut cekIdTAngkutHasBeenTap(TransaksiAngkut transaksiAngkut){
        File file = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT) ,"ANGKUT_TAP");
        String sData = GlobalHelper.readFileContent(file.getAbsolutePath());
        if(sData.isEmpty()){
            return null;
        }
        Gson gson = new Gson();
        HashMap<String, String> hashMapTPanen = gson.fromJson(sData, HashMap.class);
        String sTangkut = hashMapTPanen.get(transaksiAngkut.getIdTAngkut());
        if(sTangkut != null){
            TransaksiAngkut transaksiAngkut1 = gson.fromJson(sTangkut,TransaksiAngkut.class);
            ArrayList<Angkut> angkuts = new ArrayList<>();
            if(transaksiAngkut1 != null ) {
                if(transaksiAngkut1.getAngkuts() != null) {
                    if (transaksiAngkut1.getAngkuts().size() > 0 && transaksiAngkut.getAngkuts().size() > 0) {
                        for (Angkut angkut : transaksiAngkut.getAngkuts()) {
                            boolean hasbeenTap = false;
                            for (Angkut angkut1 : transaksiAngkut1.getAngkuts()) {
                                if(angkut.getOph() != null && angkut1.getOph() != null) {
                                    if (angkut.getOph().equalsIgnoreCase(angkut1.getOph())) {
                                        hasbeenTap = true;
                                        break;
                                    }
                                }
                            }
                            if (!hasbeenTap) {
                                angkuts.add(angkut);
                            }
                        }
                        transaksiAngkut.setAngkuts(angkuts);

                    }
                }
            }
        }
        return  transaksiAngkut;
//        return gson.fromJson(gson.toJson(hashMapTPanen.get(idTAngkut),Map.class),TransaksiAngkut.class);
    }

    public static void  addIdTPanenHasBeenTap(TransaksiAngkut transaksiAngkut){
        try {
            File file = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT), "PANEN_TAP");
            String sData = GlobalHelper.readFileContent(file.getAbsolutePath());
            Gson gson = new Gson();
            HashMap<String, TransaksiPanen> hashMapTPanen = new HashMap<>();
            if (!sData.isEmpty()) {
                hashMapTPanen = gson.fromJson(sData, HashMap.class);
            }

            File fileAngkut = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT), "ANGKUT_TAP");
            String sDataAngkut = GlobalHelper.readFileContent(fileAngkut.getAbsolutePath());
            HashMap<String, String> hashMapTAngkut = new HashMap<>();
            if (!sDataAngkut.isEmpty()) {
                hashMapTAngkut = gson.fromJson(sDataAngkut, HashMap.class);
            }

            for (int i = 0; i < transaksiAngkut.getAngkuts().size(); i++) {
                Angkut angkut = transaksiAngkut.getAngkuts().get(i);
                if (angkut.getTransaksiPanen() != null) {
                    TransaksiPanen transaksiPanen = angkut.getTransaksiPanen();
                    if (transaksiPanen != null) {
                        hashMapTPanen.put(transaksiPanen.getIdTPanen(), transaksiPanen);
                    }
                } else if (angkut.getTransaksiAngkut() != null) {
                    TransaksiAngkut angkutTransaksiAngkut = angkut.getTransaksiAngkut();
                    if (angkutTransaksiAngkut != null) {

                        ArrayList<Angkut> angkuts = new ArrayList<>();
                        Angkut angkut1 = new Angkut();
                        angkut1.setOph(angkut.getOph());
                        String sTangkut = hashMapTAngkut.get(angkutTransaksiAngkut.getIdTAngkut());

                        if (sTangkut != null) {
                            TransaksiAngkut tAngkutHastMap = gson.fromJson(sTangkut, TransaksiAngkut.class);
                            if (tAngkutHastMap != null) {
                                if (tAngkutHastMap.getAngkuts() != null) {
                                    if (tAngkutHastMap.getAngkuts().size() > 0) {
                                        angkuts = tAngkutHastMap.getAngkuts();
                                    }
                                }
                            }
                        }

                        angkuts.add(angkut1);
                        angkutTransaksiAngkut.setAngkuts(angkuts);
                        hashMapTAngkut.put(angkutTransaksiAngkut.getIdTAngkut(), gson.toJson(angkutTransaksiAngkut));
                    }
                }
            }

            GlobalHelper.writeFileContent(file.getAbsolutePath(), gson.toJson(hashMapTPanen));
            GlobalHelper.writeFileContent(fileAngkut.getAbsolutePath(), gson.toJson(hashMapTAngkut));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void removeIdTPanenHasBeenTap(String idTpanen){
        File file = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT) ,"PANEN_TAP");
        String sData = GlobalHelper.readFileContent(file.getAbsolutePath());
        Gson gson = new Gson();
        if(!sData.isEmpty()){
            HashMap<String, TransaksiPanen> hashMapTPanen = gson.fromJson(sData, HashMap.class);
            hashMapTPanen.remove(idTpanen);
            GlobalHelper.writeFileContent(file.getAbsolutePath(),gson.toJson(hashMapTPanen));
        }
    }

    public static void removeIdTAngkutHasBeenTap(String idTAngkut){
        File file = new File(GlobalHelper.getDatabasePathHMS() + "/" + Encrypts.encrypt(GlobalHelper.TABLE_ANGKUT) ,"ANGKUT_TAP");
        String sData = GlobalHelper.readFileContent(file.getAbsolutePath());
        Gson gson = new Gson();
        if(!sData.isEmpty()){
            HashMap<String, String> hashMapTAngkut = gson.fromJson(sData, HashMap.class);
            hashMapTAngkut.remove(idTAngkut);
            GlobalHelper.writeFileContent(file.getAbsolutePath(),gson.toJson(hashMapTAngkut));
        }
    }

    public static ArrayList<ModelRecyclerView> showHasBeenTap(TransaksiPanen transaksiPanen,Context context){
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd-MMMM-yyyy HH:mm");
        ArrayList<ModelRecyclerView> showHasBeenList = new ArrayList<>();

        showHasBeenList.add(new ModelRecyclerView("headerQr","Sudah Di Angkut","",""));
        showHasBeenList.add(new ModelRecyclerView("detail",
                "Id Panen",
                transaksiPanen.getIdTPanen(),
                ""));
        showHasBeenList.add(new ModelRecyclerView("detail",
                "Tanggal Panen",
                sdf.format(transaksiPanen.getCreateDate()),
                ""));
        showHasBeenList.add(new ModelRecyclerView("detail",
                "Tph",
                transaksiPanen.getTph().getNamaTph(),
                ""));
        showHasBeenList.add(new ModelRecyclerView("detail",
                "Block",
                transaksiPanen.getTph().getBlock(),
                ""));
        showHasBeenList.add(new ModelRecyclerView("detail",
                "Pemanen",
                transaksiPanen.getPemanen().getNama(),
                ""));
        return showHasBeenList;
    }

    public static ArrayList<ModelRecyclerView> showHasBeenTap(TransaksiAngkut transaksiAngkut,Context context){
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, dd-MMMM-yyyy HH:mm");
        ArrayList<ModelRecyclerView> showHasBeenList = new ArrayList<>();

        showHasBeenList.add(new ModelRecyclerView("headerQr","Sudah Di Angkut","",""));
        showHasBeenList.add(new ModelRecyclerView("detail",
                "Id Angkut",
                transaksiAngkut.getIdTAngkut(),
                ""));
        showHasBeenList.add(new ModelRecyclerView("detail",
                "Start Date",
                sdf.format(transaksiAngkut.getStartDate()),
                ""));
        showHasBeenList.add(new ModelRecyclerView("detail",
                "Finish Date",
                sdf.format(transaksiAngkut.getEndDate()),
                ""));
        showHasBeenList.add(new ModelRecyclerView("detail",
                "Supir",
                transaksiAngkut.getSupir() != null ? transaksiAngkut.getSupir().getNama() : "-",
                ""));
        showHasBeenList.add(new ModelRecyclerView("detail",
                "Kendaraan",
                transaksiAngkut.getVehicle() != null ? transaksiAngkut.getVehicle().getVehicleCode() : "-",
                ""));
        return showHasBeenList;
    }

    public static ArrayList<Angkut> cekAngkutUniq(ArrayList<Angkut> angkuts){
        HashMap<String,Angkut> cekAngkut = new HashMap<>();
        for(int i = 0 ; i < angkuts.size(); i++){
            Angkut angkut = angkuts.get(i);
            cekAngkut.put(angkut.getIdAngkut(),angkut);
        }
        return new ArrayList<>(cekAngkut.values());
    }

    public static String showVehicle(Vehicle vehicle){
        if(vehicle != null){
            if(vehicle.getVehicleName() != null){
                String [] splitName = vehicle.getVehicleName().split("-");
                String stringReturn = splitName[0].trim();
                if(splitName.length > 2){
                    stringReturn += "-" + splitName[1].trim();
                }
                return stringReturn;
            }else if(vehicle.getVraOrderNumber() != null){
                return vehicle.getVraOrderNumber();
            }else if(vehicle.getVehicleCode() != null){
                return vehicle.getVehicleCode();
            }
        }
        return "";
    }

    public static Boolean cekPlatno(String str){

        String input = str.replaceAll("\\s+", "").toUpperCase();

        //^: Mencocokkan awal dari string.
        //([A-Z]{1,2}): Grup pertama yang mencocokkan 1 hingga 2 huruf kapital (A-Z).
        //(\\s|-)*: Grup kedua yang mencocokkan nol atau lebih spasi (\\s) atau tanda hubung (-).
        //([1-9][0-9]{0,3}): Grup ketiga yang mencocokkan angka dari 1 hingga 9 diikuti oleh nol hingga tiga angka tambahan.
        //(\\s|-)*: Grup keempat yang mencocokkan nol atau lebih spasi (\\s) atau tanda hubung (-).
        //([A-Z]{0,3}|[1-9][0-9]{1,2}): Grup kelima yang mencocokkan huruf kapital dari 0 hingga 3 atau angka dari 1 hingga 9 diikuti oleh 1 hingga 2 angka tambahan.
        //$: Mencocokkan akhir dari string.
        String pattern = "^([A-Z]{1,2})(\\s|-)*([1-9][0-9]{0,3})(\\s|-)*([A-Z]{0,3}|[1-9][0-9]{1,2})$";

        Pattern regexPattern = Pattern.compile(pattern);
        Matcher matcher = regexPattern.matcher(input);

        return matcher.matches();
    }
}
