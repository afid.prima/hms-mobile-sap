package id.co.ams_plantation.harvestsap.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.ArraySet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.listener.ITableViewListener;
import com.google.gson.Gson;
import com.labo.kaji.fragmentanimations.MoveAnimation;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.utils.Utils;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.dizitart.no2.Nitrite;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import id.co.ams_plantation.harvestsap.HarvestApp;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.SwipeItemRecycleView.SwipeController;
import id.co.ams_plantation.harvestsap.SwipeItemRecycleView.SwipeControllerActions;
import id.co.ams_plantation.harvestsap.TableView.adapter.MekanisasiPanenDetailAdapter;
import id.co.ams_plantation.harvestsap.TableView.holder.ColumnHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.holder.MekanisasiPanenRowHeaderViewHolder;
import id.co.ams_plantation.harvestsap.TableView.popup.ColumnHeaderPopup;
import id.co.ams_plantation.harvestsap.TableView.popup.RowHeaderLongPressPopup;
import id.co.ams_plantation.harvestsap.TableView.view_model.MekanisasiPanenDetailTableViewModel;
import id.co.ams_plantation.harvestsap.adapter.SelectGangAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectOperatorAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectPemanenAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectSupervisionAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectTphMekanisasiAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectTphnLangsiranAdapter;
import id.co.ams_plantation.harvestsap.adapter.SelectVehicleAdapter;
import id.co.ams_plantation.harvestsap.adapter.SubPemanenMekanisasiAdapter;
import id.co.ams_plantation.harvestsap.model.BJRInformation;
import id.co.ams_plantation.harvestsap.model.DataNitrit;
import id.co.ams_plantation.harvestsap.model.DynamicParameterPenghasilan;
import id.co.ams_plantation.harvestsap.model.HasilPanen;
import id.co.ams_plantation.harvestsap.model.MekanisasiOperator;
import id.co.ams_plantation.harvestsap.model.MekanisasiPanen;
import id.co.ams_plantation.harvestsap.model.Operator;
import id.co.ams_plantation.harvestsap.model.Pemanen;
import id.co.ams_plantation.harvestsap.model.StampImage;
import id.co.ams_plantation.harvestsap.model.SupervisionList;
import id.co.ams_plantation.harvestsap.model.TPH;
import id.co.ams_plantation.harvestsap.model.TransaksiPanen;
import id.co.ams_plantation.harvestsap.model.Vehicle;
import id.co.ams_plantation.harvestsap.ui.BaseActivity;
import id.co.ams_plantation.harvestsap.ui.MekanisasiActivity;
import id.co.ams_plantation.harvestsap.util.BjrHelper;
import id.co.ams_plantation.harvestsap.util.CompleteTextViewHelper;
import id.co.ams_plantation.harvestsap.util.GlobalHelper;
import id.co.ams_plantation.harvestsap.util.MekanisasiHelper;
import id.co.ams_plantation.harvestsap.util.PemanenHelper;
import id.co.ams_plantation.harvestsap.util.SharedPrefHelper;
import id.co.ams_plantation.harvestsap.util.TphHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiAngkutHelper;
import id.co.ams_plantation.harvestsap.util.TransaksiPanenHelper;
import id.co.ams_plantation.harvestsap.util.WidgetHelper;
import ng.max.slideview.SlideView;
import pl.aprilapps.easyphotopicker.EasyImage;

import static id.co.ams_plantation.harvestsap.util.GlobalHelper.isDoubleClick;

public class MekanisasiPanenInputFragment extends Fragment {

    final int LongOperation_Save = 1;
    final int LongOperation_Hapus = 2;

    final int YesNo_Simpan = 1;
    final int YesNo_Hapus = 2;

    private static final String TAG = MekanisasiPanenInputFragment.class.getSimpleName();
    boolean saveBtn = false;

    BaseActivity baseActivity;
    public MekanisasiActivity mekanisasiActivity;
    TphHelper tphHelper;
    TransaksiPanenHelper transaksiPanenHelper;
    public MekanisasiHelper mekanisasiHelper;
    PemanenHelper pemanenHelper;

    CompleteTextViewHelper etKraniPanen;
    CompleteTextViewHelper etOperatorQt;
    CompleteTextViewHelper etLoaderQt;
    CompleteTextViewHelper etQt;
    AppCompatEditText etBin;
    RelativeLayout btnAddMekanisasi;
    ImageView ivNodata;
    TableView mTableView;
    TextView tvTotalOPH;
    TextView tvTotalJanjang;
    TextView tvBrondolan;
    TextView tvTotalBeratEstimasi;

    public SlidingUpPanelLayout mLayout;
    RelativeLayout btnShowForm;
    LinearLayout llheadumano;
    public TextView sheetHeader;
    public TextView tvDate;
    ImageView ivchevronup;
    ImageView ivchevrondown;

    View tphphoto;
    ImageView ivPhoto;
    ImageView imagecacnel;
    ImageView imagesview;
    SliderLayout sliderLayout;
    RelativeLayout rl_ipf_takepicture;

    LinearLayout lsave;
    LinearLayout lldeleted;
    LinearLayout lcancel;

    public CompleteTextViewHelper etBlock;
    public CompleteTextViewHelper etAncak;
    public CompleteTextViewHelper etGang;
    public CompleteTextViewHelper etPemanen;
    public AppCompatEditText etTotalJanjangPendapatanSubPemanen;
    public AppCompatEditText etTotalJanjangSubPemanen;
    public AppCompatEditText etNormalSubPemanen;
    public AppCompatEditText etBuahMentahSubPemanen;
//    public AppCompatEditText etBusukSubPemanen;
//    public AppCompatEditText etLewatMatangSubPemanen;
//    public AppCompatEditText etAbnormalSubPemanen;
//    public AppCompatEditText etTangkaiPanjangSubPemanen;
//    public AppCompatEditText etBuahDimakanTikusSubPemanen;
    public AppCompatEditText etTotalBrondolanSubPemanen;
    public TextView tvTPH;
    public TextView tvPemanen;
    public LinearLayout lsaveFormMekanisasi;
    public LinearLayout lldeletedFormMekanisasi;

    SelectOperatorAdapter adapterOperator;
    SelectVehicleAdapter adapterVehicle;
    SelectSupervisionAdapter adapterKrani;
    ArrayAdapter<String> adapterBlock;

    public Pemanen pemanenSelected;
    MekanisasiPanen mekanisasiPanenSelected;

    public DynamicParameterPenghasilan dynamicParameterPenghasilan;
    public HashMap<String,TransaksiPanen> mekanisasiPanens;

    Operator selectedLoader;
    Operator selectedOperator;
    Vehicle vehicleUse;
    ArrayList<File> ALselectedImage;
    HashMap<Long,SupervisionList> sSupervisionList;
    HashMap<String, MekanisasiOperator> currentMekanisasiOperator;

    MekanisasiPanenDetailTableViewModel mTableViewModel;
    MekanisasiPanenDetailAdapter mekanisasiPanenDetailAdapter;
//    SubPemanenMekanisasiAdapter subPemanenMekanisaisAdapter;
//    SwipeController swipeController = null;
    File kml;

    SupervisionList kraniPanenSelected;
    public String selectedBlock;
    HasilPanen hasilPanenStamp;
    StampImage stampImage;

    public static MekanisasiPanenInputFragment getInstance() {
        Log.i(TAG, "getInstance: no bundle");
        MekanisasiPanenInputFragment mekanisasiPanenInputFragment = new MekanisasiPanenInputFragment();
        return mekanisasiPanenInputFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "arguments: ");
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return MoveAnimation.create(MoveAnimation.UP,enter,600);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mekanisasi_panen_input, null, false);
        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: ");

        etKraniPanen = (CompleteTextViewHelper) view.findViewById(R.id.etKraniPanen);
        etOperatorQt = (CompleteTextViewHelper) view.findViewById(R.id.etOperatorQt);
        etLoaderQt = (CompleteTextViewHelper) view.findViewById(R.id.etLoader);
        etQt = (CompleteTextViewHelper) view.findViewById(R.id.etQt);
        etBin = (AppCompatEditText) view.findViewById(R.id.etBin);
        btnAddMekanisasi = (RelativeLayout) view.findViewById(R.id.btnAddMekanisasi);
        ivNodata = (ImageView) view.findViewById(R.id.ivNodata);
        mTableView = (TableView) view.findViewById(R.id.tableview);
        tvTotalJanjang = (TextView) view.findViewById(R.id.tvTotalJanjang);
        tvTotalOPH = (TextView) view.findViewById(R.id.tvTotalOPH);
        tvBrondolan = (TextView) view.findViewById(R.id.tvBrondolan);
        tvTotalBeratEstimasi = (TextView) view.findViewById(R.id.tvTotalBeratEstimasi);

//        tphphoto = (View) view.findViewById(R.id.tphphoto);
//        ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
//        imagecacnel = (ImageView) view.findViewById(R.id.imagecacnel);
//        imagesview = (ImageView) view.findViewById(R.id.imagesview);
//        rl_ipf_takepicture = (RelativeLayout) view.findViewById(R.id.rl_ipf_takepicture);
//        sliderLayout = (SliderLayout) view.findViewById(R.id.sliderLayout);

        mLayout = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);
        llheadumano = (LinearLayout) view.findViewById(R.id.llheadumano);
        btnShowForm = (RelativeLayout) view.findViewById(R.id.btnShowForm);
        sheetHeader = (TextView) view.findViewById(R.id.sheetHeader);
        tvDate = (TextView) view.findViewById(R.id.tvDate);
        ivchevronup = (ImageView) view.findViewById(R.id.ivchevronup);
        ivchevrondown = (ImageView) view.findViewById(R.id.ivchevrondown);

        lsave = (LinearLayout) view.findViewById(R.id.lsave);
        lldeleted = (LinearLayout) view.findViewById(R.id.lldeleted);
        lcancel = (LinearLayout) view.findViewById(R.id.lcancel);

        etBlock = view.findViewById(R.id.etBlock);
        etAncak = view.findViewById(R.id.etAncak);
        etGang = view.findViewById(R.id.etGang);
        etPemanen = view.findViewById(R.id.etPemanen);
        etTotalJanjangPendapatanSubPemanen = view.findViewById(R.id.etTotalJanjangPendapatanSubPemanen);
        etTotalJanjangSubPemanen = view.findViewById(R.id.etTotalJanjangSubPemanen);
        etNormalSubPemanen = view.findViewById(R.id.etNormalSubPemanen);
        etBuahMentahSubPemanen = view.findViewById(R.id.etBuahMentahSubPemanen);
//        etBusukSubPemanen = view.findViewById(R.id.etBusukSubPemanen);
//        etLewatMatangSubPemanen = view.findViewById(R.id.etLewatMatangSubPemanen);
//        etAbnormalSubPemanen = view.findViewById(R.id.etAbnormalSubPemanen);
//        etTangkaiPanjangSubPemanen = view.findViewById(R.id.etTangkaiPanjangSubPemanen);
//        etBuahDimakanTikusSubPemanen = view.findViewById(R.id.etBuahDimakanTikusSubPemanen);
        etTotalBrondolanSubPemanen = view.findViewById(R.id.etTotalBrondolanSubPemanen);
        tvTPH = view.findViewById(R.id.tvTPH);
        tvPemanen = view.findViewById(R.id.tvPemanen);
        lsaveFormMekanisasi = view.findViewById(R.id.lsaveFormMekansasi);
        lldeletedFormMekanisasi = view.findViewById(R.id.lldeletedFormMekansasi);

        baseActivity = ((BaseActivity) getActivity());
        mekanisasiActivity = ((MekanisasiActivity) getActivity());
        tphHelper = new TphHelper(getActivity());
        transaksiPanenHelper = new TransaksiPanenHelper(getActivity());
        mekanisasiHelper = new MekanisasiHelper(getActivity());
        pemanenHelper = new PemanenHelper(getActivity());

        ALselectedImage = new ArrayList<>();
        mekanisasiPanens = new HashMap<>();

        currentMekanisasiOperator = new HashMap<>();
        currentMekanisasiOperator = mekanisasiHelper.getCurrentMekanisasiOperator(System.currentTimeMillis());

        mLayout.setTouchEnabled(false);
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        ivchevronup.setVisibility(View.GONE);
        ivchevrondown.setVisibility(View.GONE);
//        llheadumano.setVisibility(View.GONE);

//        ivchevronup.setImageDrawable(new
//                IconicsDrawable(mekanisasiActivity)
//                .icon(MaterialDesignIconic.Icon.gmi_chevron_up)
//                .sizeDp(18)
//                .colorRes(R.color.Gray));
//        ivchevrondown.setImageDrawable(new
//                IconicsDrawable(mekanisasiActivity)
//                .icon(MaterialDesignIconic.Icon.gmi_chevron_down)
//                .sizeDp(18)
//                .colorRes(R.color.Gray));

        tvTotalJanjang.setText("");
        tvBrondolan.setText("");
        tvTotalOPH.setText("");
        saveBtn = false;

        HashMap<String,DynamicParameterPenghasilan> appConfig = GlobalHelper.getAppConfig();
        dynamicParameterPenghasilan = appConfig.get(GlobalHelper.getEstate().getEstCode());
        Collection<Operator> operators = GlobalHelper.dataOperator.values();
        Collection<Vehicle> vehicles = GlobalHelper.dataVehicle.values();

        sSupervisionList = new HashMap<>();
        SupervisionList supervisionList = new SupervisionList();
        supervisionList.setUserID(Long.parseLong(GlobalHelper.getUser().getUserID()));
        supervisionList.setNama(GlobalHelper.getUser().getUserFullName());
        supervisionList.setNik("-");
        supervisionList.setAfdeling("-");
        supervisionList.setGank("-");
        supervisionList.setEstate(GlobalHelper.getEstate().getEstCode());
        supervisionList.setEstCodeSAP("-");
        sSupervisionList.put(supervisionList.getUserID(),supervisionList);

        if(GlobalHelper.dataAllSupervision != null) {
            if (GlobalHelper.dataAllSupervision.size() > 0) {
                for (HashMap.Entry<String, SupervisionList> entry : GlobalHelper.dataAllSupervision.entrySet()) {
                    if(entry.getValue().getUserID() >= 1) {
                        sSupervisionList.put(entry.getValue().getUserID(), entry.getValue());
                    }
                }
            }
        }
        adapterKrani = new SelectSupervisionAdapter(getActivity(), R.layout.row_setting, new ArrayList<SupervisionList>(sSupervisionList.values()));
        etKraniPanen.setThreshold(1);
        etKraniPanen.setAdapter(adapterKrani);
        adapterKrani.notifyDataSetChanged();

        setKraniPanen(sSupervisionList.get(Long.parseLong(GlobalHelper.getUser().getUserID())));

//        kml = new File(GlobalHelper.decryptFiles(GlobalHelper.TYPE_KMLBLOCK));
//        Set<String> allBlock = GlobalHelper.getAllBlock(kml);

        Set<String> allBlock = mekanisasiHelper.setBlockMekanisais();

        adapterBlock = new ArrayAdapter<String>(getActivity(),android.R.layout.select_dialog_item, allBlock.toArray(new String[allBlock.size()]));
        adapterOperator = new SelectOperatorAdapter(getActivity(),R.layout.row_setting, new ArrayList<Operator>(operators));
        adapterVehicle= new SelectVehicleAdapter(getActivity(),R.layout.row_setting, new ArrayList<Vehicle>(vehicles));

        etQt.setAdapter(adapterVehicle);
        etQt.setThreshold(1);
        etOperatorQt.setAdapter(adapterOperator);
        etOperatorQt.setThreshold(1);
        etLoaderQt.setAdapter(adapterOperator);
        etLoaderQt.setThreshold(1);

        mekanisasiHelper.setupFormMekansiasi(adapterBlock,tphHelper,pemanenHelper);

        etNormalSubPemanen.setFocusable(false);
        etTotalJanjangPendapatanSubPemanen.setFocusable(false);

        if(mekanisasiActivity.mekanisasiPanenSelected != null){
            if(mekanisasiActivity.mekanisasiPanenSelected.getIdMekanisasi() != null){
                lldeleted.setVisibility(View.VISIBLE);
                mekanisasiPanenSelected = mekanisasiActivity.mekanisasiPanenSelected;
                setLoader(mekanisasiPanenSelected.getLoader());
                setOperator(mekanisasiPanenSelected.getOperator());
                setVehicle(mekanisasiPanenSelected.getVehicle());
                setKraniPanen(sSupervisionList.get(Long.parseLong(mekanisasiPanenSelected.getCreateBy())));

                if(mekanisasiPanenSelected.getTransaksiPanens().size() > 0){
                    Collections.sort(mekanisasiPanenSelected.getTransaksiPanens(), new Comparator<TransaksiPanen>() {
                        @Override
                        public int compare(TransaksiPanen o1, TransaksiPanen o2) {
                            return o1.getCreateDate() > o2.getCreateDate() ? -1 : (o1.getCreateDate() < o2.getCreateDate()) ? 1 : 0;
                        }
                    });

                    if(mekanisasiPanenSelected.getTransaksiPanens().get(mekanisasiPanenSelected.getTransaksiPanens().size() - 1) != null){
                        if(mekanisasiPanenSelected.getTransaksiPanens().get(mekanisasiPanenSelected.getTransaksiPanens().size() - 1).getTph() != null){
                            selectedBlock = mekanisasiPanenSelected.getTransaksiPanens().get(mekanisasiPanenSelected.getTransaksiPanens().size() - 1).getTph().getBlock();
                        }
                    }

                    mekanisasiPanens = new HashMap<>();
                    for(TransaksiPanen panen : mekanisasiPanenSelected.getTransaksiPanens()){
                        mekanisasiPanens.put(panen.getIdTPanen(),panen);
                    }

                }

                updateUIRV();

//                subPemanenMekanisaisAdapter = new SubPemanenMekanisasiAdapter(getActivity(),mekanisasiPanenSelected.getTransaksiPanens(),adapterBlock);
//                rvPemanen.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//                rvPemanen.setAdapter(subPemanenMekanisaisAdapter);
            }
        }else{
            lldeleted.setVisibility(View.GONE);
            updateUIRV();
//            TransaksiPanen transaksiPanen = new TransaksiPanen();
//            transaksiPanen.setIdTPanen("M-"+System.currentTimeMillis()+"-"+ GlobalHelper.getUser().getUserID());
//            transaksiPanen.setCreateDate(System.currentTimeMillis());
//            ArrayList<TransaksiPanen> mekanisasiPanens = new ArrayList<>();
//            mekanisasiPanens.add(transaksiPanen);
//            subPemanenMekanisaisAdapter = new SubPemanenMekanisasiAdapter(getActivity(),mekanisasiPanens,adapterBlock);
//            rvPemanen.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//            rvPemanen.setAdapter(subPemanenMekanisaisAdapter);
        }

//        swipeController = new SwipeController(new SwipeControllerActions() {
//            @Override
//            public void onRightClicked(int position) {
//                if(subPemanenMekanisaisAdapter.mekanisasiPanens.size() > 1) {
//                    subPemanenMekanisaisAdapter.mekanisasiPanens.remove(position);
//                    subPemanenMekanisaisAdapter.notifyItemRemoved(position);
//                    updateTotalJanjang();
//                }
//            }
//
//            @Override
//            public void onLeftClicked(int position) {
//                if (subPemanenMekanisaisAdapter.mekanisasiPanens.size() >= GlobalHelper.MAX_MEKANISASI_PANEN) {
//                    Toast.makeText(HarvestApp.getContext(), subPemanenMekanisaisAdapter.mekanisasiPanens.size() + " " + getActivity().getResources().getString(R.string.max_pemanen), Toast.LENGTH_SHORT).show();
//                    return;
//                }
//
//
//                TransaksiPanen transaksiPanen = new TransaksiPanen();
//                transaksiPanen.setIdTPanen("M-"+System.currentTimeMillis()+"-"+ GlobalHelper.getUser().getUserID());
//                transaksiPanen.setCreateDate(System.currentTimeMillis());
//                transaksiPanen.setBlock(selectedBlock);
//                subPemanenMekanisaisAdapter.mekanisasiPanens.add(transaksiPanen);
//                subPemanenMekanisaisAdapter.notifyItemInserted(subPemanenMekanisaisAdapter.mekanisasiPanens.size());
//
//                rvPemanen.scrollToPosition(subPemanenMekanisaisAdapter.getItemCount() - 1);
//                GlobalHelper.hideKeyboard(mekanisasiActivity);
//            }
//        },subPemanenMekanisaisAdapter);
//
//        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
//        itemTouchhelper.attachToRecyclerView(rvPemanen);

        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
//                if(slideOffset == 0){
//                    mLayout.setTouchEnabled(true);
//                }
//                Log.i("MekanisasiPanenInput", "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
//                if(newState ==  SlidingUpPanelLayout.PanelState.EXPANDED){
//                    ivchevronup.setVisibility(View.GONE);
//                    ivchevrondown.setVisibility(View.VISIBLE);
//                }else if (newState ==  SlidingUpPanelLayout.PanelState.ANCHORED){
//                    ivchevronup.setVisibility(View.GONE);
//                    ivchevrondown.setVisibility(View.VISIBLE);
//                }else if (newState ==  SlidingUpPanelLayout.PanelState.COLLAPSED){
//                    ivchevronup.setVisibility(View.VISIBLE);
//                    ivchevrondown.setVisibility(View.GONE);
//                }
            }
        });

        btnShowForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED){
//                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//                }else if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED){
//                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//                }else if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED){
//                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
//                }
            }
        });

        btnAddMekanisasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mekanisasiPanens.size() >= GlobalHelper.MAX_MEKANISASI_PANEN) {
                    Toast.makeText(HarvestApp.getContext(), mekanisasiPanens.size() + " " + getActivity().getResources().getString(R.string.max_pemanen), Toast.LENGTH_SHORT).show();
                    return;
                }

                TransaksiPanen transaksiPanen = new TransaksiPanen();
                transaksiPanen.setIdTPanen("M-"+System.currentTimeMillis()+"-"+ String.valueOf(kraniPanenSelected.getUserID()));
                transaksiPanen.setCreateDate(System.currentTimeMillis());
                transaksiPanen.setCreateBy(String.valueOf(kraniPanenSelected.getUserID()));
                transaksiPanen.setEstCode(GlobalHelper.getEstate().getEstCode());
                transaksiPanen.setBlock(selectedBlock);
                if(baseActivity.currentlocation != null){
                    transaksiPanen.setLongitude(baseActivity.currentlocation.getLongitude());
                    transaksiPanen.setLatitude(baseActivity.currentlocation.getLatitude());
                }else{
                    transaksiPanen.setLongitude(0);
                    transaksiPanen.setLatitude(0);
                }
                mekanisasiPanens.put(transaksiPanen.getIdTPanen(),transaksiPanen);

//                llheadumano.setVisibility(View.VISIBLE);
//                mLayout.setTouchEnabled(true);

                mekanisasiHelper.showFormMekanisasi(transaksiPanen,true);
            }
        });

        etQt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etQt.showDropDown();
            }
        });

        etQt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etQt.setText("");
                etQt.showDropDown();
            }
        });

        etQt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setVehicle(((SelectVehicleAdapter) parent.getAdapter()).getItemAt(position));
            }
        });

        etOperatorQt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etOperatorQt.showDropDown();
            }
        });

        etOperatorQt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etOperatorQt.setText("");
                etOperatorQt.showDropDown();
            }
        });

        etOperatorQt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Operator operator = ((SelectOperatorAdapter) parent.getAdapter()).getItemAt(position);
                setOperator(operator);
                MekanisasiOperator mekanisasiOperator = currentMekanisasiOperator.get(operator.getKodeOperator());
                if (mekanisasiOperator != null) {
                    setVehicle(mekanisasiOperator.getVehicle());
                }
            }
        });

        etLoaderQt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etLoaderQt.showDropDown();
            }
        });

        etLoaderQt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etLoaderQt.setText("");
                etLoaderQt.showDropDown();
            }
        });

        etKraniPanen.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                    etKraniPanen.showDropDown();
            }
        });

        etKraniPanen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etKraniPanen.setText("");
                etKraniPanen.showDropDown();
            }
        });

        etKraniPanen.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SupervisionList kraniPanen = ((SelectSupervisionAdapter) parent.getAdapter()).getItemAt(position);
                setKraniPanen(kraniPanen);
            }
        });

        etLoaderQt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Operator operator = ((SelectOperatorAdapter) parent.getAdapter()).getItemAt(position);
                setLoader(operator);
            }
        });

//        ivPhoto.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(ALselectedImage.size() > 3) {
//                    Toast.makeText(HarvestApp.getContext(),getActivity().getResources().getString(R.string.warning_take_foto),Toast.LENGTH_LONG).show();
//                }else {
//                    takePicture();
//                }
//            }
//        });

        lsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isDoubleClick()) {
                    return;
                }

                if(validasi()){
                    showYesNo("Apakah Anda Yakin Untuk Simpan Data Ini",YesNo_Simpan);
                }
            }
        });

        lldeleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isDoubleClick()) {
                    return;
                }

                if (mekanisasiPanenSelected != null) {
                    showYesNo("Apakah Anda Yakin Untuk Hapus " + mekanisasiPanens.size() + " Data Ini", YesNo_Hapus);
                }
            }
        });

        lcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isDoubleClick()) {
                    return;
                }
                mekanisasiActivity.backProses();
            }
        });
    }

    public void updateUIRV(){

        if(mekanisasiPanens.size() > 0 ) {

            ivNodata.setVisibility(View.GONE);
            mTableView.setVisibility(View.VISIBLE);

            updateTotalJanjang();

            mTableViewModel = new MekanisasiPanenDetailTableViewModel(getContext(), mekanisasiPanens);
            // Create TableView Adapter
            mekanisasiPanenDetailAdapter = new MekanisasiPanenDetailAdapter(getContext(), mTableViewModel);
            mTableView.setAdapter(mekanisasiPanenDetailAdapter);
            mTableView.setRowHeaderWidth(Utils.convertDpToPx(getActivity(),100));
            mTableView.setTableViewListener(new ITableViewListener() {
                @Override
                public void onCellClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {
                    Log.d(TAG, "onCellClicked: column" + String.valueOf(column) +" row" + String.valueOf(row));
                }

                @Override
                public void onCellDoubleClicked(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {
                    Log.d(TAG, "onCellDoubleClicked: column" + String.valueOf(column) +" row" + String.valueOf(row));

                }

                @Override
                public void onCellLongPressed(@NonNull RecyclerView.ViewHolder cellView, int column, int row) {
                    Log.d(TAG, "onCellLongPressed: column" + String.valueOf(column) +" row" + String.valueOf(row));

                }

                @Override
                public void onColumnHeaderClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    Log.d(TAG, "onColumnHeaderClicked: column" + String.valueOf(column));
                    if (columnHeaderView != null && columnHeaderView instanceof ColumnHeaderViewHolder) {
                        // Create Long Press Popup
                        ColumnHeaderPopup popup = new ColumnHeaderPopup(
                                (ColumnHeaderViewHolder) columnHeaderView, mTableView);
                        // Show
                        popup.show();
                    }
                }

                @Override
                public void onColumnHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    Log.d(TAG, "onColumnHeaderDoubleClicked: column" + String.valueOf(column) );

                }

                @Override
                public void onColumnHeaderLongPressed(@NonNull RecyclerView.ViewHolder columnHeaderView, int column) {
                    Log.d(TAG, "onColumnHeaderLongPressed: column" + String.valueOf(column) );
                }

                @Override
                public void onRowHeaderClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    Log.d(TAG, "onRowHeaderClicked: column" + String.valueOf(row) );
                }

                @Override
                public void onRowHeaderDoubleClicked(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    Log.d(TAG, "onRowHeaderDoubleClicked: column" + String.valueOf(row) );
                }

                @Override
                public void onRowHeaderLongPressed(@NonNull RecyclerView.ViewHolder rowHeaderView, int row) {
                    Log.d(TAG, "onRowHeaderLongPressed: column" + String.valueOf(row) );
                    if (rowHeaderView != null && rowHeaderView instanceof MekanisasiPanenRowHeaderViewHolder) {

                        RowHeaderLongPressPopup popup = new RowHeaderLongPressPopup(rowHeaderView, mTableView,mekanisasiActivity);
                        popup.show();
                    }
                }
            });
            mekanisasiPanenDetailAdapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel
                    .getRowHeaderList(), mTableViewModel.getCellList());


//            mTableFilter = new Filter(mTableView);
        }else{
            ivNodata.setVisibility(View.VISIBLE);
            mTableView.setVisibility(View.GONE);
        }
    }

    /*
     * list param
     * stat :
     *  1. YesNo_Simpan = 1
     */
    public void showYesNo(String text,int stat){
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.yes_no, null);
        TextView tvDialogKet = (TextView) dialogView.findViewById(R.id.ket1);
        SlideView btnCancelDialog = (SlideView) dialogView.findViewById(R.id.btnCancel);
        SlideView btnOkDialog = (SlideView) dialogView.findViewById(R.id.btnOK);

        final AlertDialog dialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle)
                .setView(dialogView)
                .setCancelable(false)
                .create();

        final ForegroundColorSpan fcs = new ForegroundColorSpan(getActivity().getResources().getColor(R.color.Red));
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

        tvDialogKet.setText(text);
        btnOkDialog.setText("Yes");
        btnCancelDialog.setText("No");

        btnCancelDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                switch (stat) {
                    default:
                        dialog.dismiss();
                        break;
                }
            }
        });
        btnOkDialog.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                switch (stat){
                    case YesNo_Simpan:{
                        new LongOperation().execute(String.valueOf(LongOperation_Save));
                        break;
                    }
                    case YesNo_Hapus:{
                        new LongOperation().execute(String.valueOf(LongOperation_Hapus));
                        break;
                    }
                }

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public boolean validasi(){
        if(selectedOperator == null){
            Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Operator Quick Traktor",Toast.LENGTH_SHORT).show();
            return false;
        }

        if(vehicleUse == null){
            Toast.makeText(HarvestApp.getContext(),"Mohon Pilih Quick Traktor",Toast.LENGTH_SHORT).show();
            return false;
        }

        //ini dilepas karena load dan operator bisa orang yang sama
//        if(selectedLoader != null){
//            if(selectedLoader.getNik().equals(selectedOperator.getNik())){
//                Toast.makeText(HarvestApp.getContext(),"Loader dan Operator tidak boleh sama",Toast.LENGTH_SHORT).show();
//                return false;
//            }
//        }

        Set<String> idUniq = new HashSet<>();
        if(mekanisasiPanens.size() > 0){
            for(TransaksiPanen transaksiPanen : new ArrayList<>(mekanisasiPanens.values())) {

                Pemanen pemanen = transaksiPanen.getPemanen();
                TPH tph = transaksiPanen.getTph();
                HasilPanen hasilPanen = transaksiPanen.getHasilPanen();

                if(hasilPanen == null){
                    Toast.makeText(HarvestApp.getContext(),"Mohon input janjang dahulu",Toast.LENGTH_SHORT).show();
                    return false;
                }

                if(pemanen == null){
                    Toast.makeText(HarvestApp.getContext(),"Mohon input dahulu pemanen",Toast.LENGTH_SHORT).show();
                    return false;
                }

                if(pemanen.getKodePemanen().equals(selectedOperator.getKodeOperator())){
                    Toast.makeText(HarvestApp.getContext(),"Nama pemanen dan operator tidak boleh sama",Toast.LENGTH_SHORT).show();
                    return false;
                }

                if(tph == null){
                    Toast.makeText(HarvestApp.getContext(),"Mohon input dahulu TPH",Toast.LENGTH_SHORT).show();
                    return false;
                }

                idUniq.add(tph.getNoTph() + "-"+ pemanen.getKodePemanen());
            }
        } else {
            Toast.makeText(HarvestApp.getContext(),"Mohon Isi Pemanen Dan TPH dahulu",Toast.LENGTH_SHORT).show();
            return false;
        }

        if(idUniq.size() != mekanisasiPanens.size()){
            Toast.makeText(HarvestApp.getContext(),"Tidak boleh ada TPH dan Pemanen yang sama",Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public void setKraniPanen(SupervisionList kraniPanen){
        if(kraniPanen !=null) {
            etKraniPanen.setText(kraniPanen.getNama());
            kraniPanenSelected = kraniPanen;
        }else{
            SupervisionList supervisionList = new SupervisionList();
            supervisionList.setUserID(Long.parseLong(GlobalHelper.getUser().getUserID()));
            supervisionList.setNama(GlobalHelper.getUser().getUserFullName());
            supervisionList.setNik("-");
            supervisionList.setAfdeling("-");
            supervisionList.setGank("-");
            supervisionList.setEstate(GlobalHelper.getEstate().getEstCode());
            supervisionList.setEstCodeSAP("-");

            etKraniPanen.setText(supervisionList.getNama());
            kraniPanenSelected = supervisionList;
        }
    }

    public void setVehicle(Vehicle vehicle){
        if(vehicle !=null) {
            etQt.setText(TransaksiAngkutHelper.showVehicle(vehicle));
            vehicleUse = vehicle;
        }else{
            etQt.setText("");
            vehicleUse = new Vehicle();
        }

        //updateHeaderTransaksiSpb();
    }

    public void setOperator(Operator operator){
        if(operator != null) {
            etOperatorQt.setText(operator.getNama());
            selectedOperator = operator;
        }else{
            etOperatorQt.setText("");
            selectedOperator = new Operator();
        }

        //updateHeaderTransaksiSpb();
    }

    public void setLoader(Operator operator){
        if(operator != null) {
            etLoaderQt.setText(operator.getNama());
            selectedLoader = operator;
        }else{
            etLoaderQt.setText("");
            selectedLoader = null;
        }

        //updateHeaderTransaksiSpb();
    }

    private void takePicture(){
        if(stampImage == null){
            stampImage = new StampImage();
        }

        if (selectedOperator != null) {
            stampImage.setOperator(selectedOperator);
        } else {
            Operator X = new Operator();
            X.setNama(etOperatorQt.getText().toString());
            stampImage.setOperator(X);
        }

        if (vehicleUse != null) {
            stampImage.setVehicle(vehicleUse);
        } else {
            Vehicle X = new Vehicle();
            X.setVehicleName(etQt.getText().toString());
            stampImage.setVehicle(X);
        }

        if(pemanenSelected != null){
            stampImage.setPemanen(pemanenSelected);
        }

        Gson gson  = new Gson();
        SharedPreferences preferences = HarvestApp.getContext().getSharedPreferences(HarvestApp.STAMP_IMAGES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(HarvestApp.STAMP_IMAGES,gson.toJson(stampImage));
        editor.apply();

        GlobalHelper.TAG_CAMERA = GlobalHelper.TAG_CAMERA_TRANSAKSI_TPH_MEKANISASI;
        EasyImage.openCamera(getActivity(),1);
    }

    public void addImages(File newImages){
        ALselectedImage.add(newImages);
        setupimageslide();
    }

    private void setupimageslide(){
        Set<File> sf = new HashSet<>();
        sf.addAll(ALselectedImage);
        ALselectedImage.clear();
        ALselectedImage.addAll(sf);

        if(ALselectedImage.size() > 3){
            ivPhoto.setVisibility(View.GONE);
        }else{
            ivPhoto.setVisibility(View.VISIBLE);
        }
        if(ALselectedImage.size() != 0){
            imagecacnel.setVisibility(View.VISIBLE);
            imagesview.setVisibility(View.VISIBLE);
            sliderLayout.setVisibility(View.VISIBLE);
        }else{
            imagecacnel.setVisibility(View.GONE);
            imagesview.setVisibility(View.GONE);
            sliderLayout.setVisibility(View.GONE);
        }
        sliderLayout.removeAllSliders();
        for(File file : ALselectedImage){
            TextSliderView textSliderView = new TextSliderView(mekanisasiActivity);
            // initialize a SliderLayout
            if(file.toString().startsWith("http:")){
                String surl = file.toString();
                if(file.toString().startsWith("http://")){

                }else if(file.toString().startsWith("http:/")){
                    surl =surl.replace("http:/","http://");
                }

                textSliderView
                        .image(GlobalHelper.setUrlFoto(surl))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }else {
                textSliderView
                        .image(file)
                        .setScaleType(BaseSliderView.ScaleType.Fit);
            }
            sliderLayout.addSlider(textSliderView);
        }
        sliderLayout.stopAutoCycle();
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.FlipHorizontal);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        imagecacnel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(GSApp.getContext(),"slider getCurrentPosition " + sliderLayout.getCurrentPosition(),Toast.LENGTH_SHORT).show();
                try {
                    if (sliderLayout.getCurrentPosition() == 0 && ALselectedImage.size() == 1) {
                        ALselectedImage.clear();
                        sliderLayout.removeAllSliders();
                        sliderLayout.setVisibility(View.GONE);
                        imagecacnel.setVisibility(View.GONE);
                        imagesview.setVisibility(View.GONE);
                    } else {
                        ALselectedImage.remove(sliderLayout.getCurrentPosition());
                        sliderLayout.removeSliderAt(sliderLayout.getCurrentPosition());
                    }
                }catch (Exception e){
                    Log.e("sliderLayout", e.toString());
                }
            }
        });
        imagesview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WidgetHelper.showImagesZoom(mekanisasiActivity,ALselectedImage);
            }
        });
    }

    public void updateTotalJanjang(){
        int tJJG = 0;
        int tBrdl = 0;
        int tBerat = 0;
        if(mekanisasiPanens.size() > 0){
            for(TransaksiPanen transaksiPanen : new ArrayList<>(mekanisasiPanens.values())) {
                if(transaksiPanen.getHasilPanen() != null) {
                    tJJG += transaksiPanen.getHasilPanen().getTotalJanjang();
                    tBrdl += transaksiPanen.getHasilPanen().getBrondolan();

                    if (transaksiPanen.getTph() != null) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(new Date(System.currentTimeMillis()));
                        String idBJRInformasi = String.valueOf(cal.get(Calendar.YEAR)) + "-" + String.valueOf(cal.get(Calendar.MONTH) + 1) + "-" + transaksiPanen.getTph().getBlock() +"-"+  transaksiPanen.getTph().getAfdeling();
                        BJRInformation bjrInformation = mekanisasiActivity.bjrInformationHashMap.get(idBJRInformasi);
                        if (bjrInformation != null) {
                            tBerat += (int) transaksiPanen.getHasilPanen().getTotalJanjang() * bjrInformation.getBjrAdjustment();
                        }
                    }
                }

            }
        }

        tvTotalOPH.setText(String.valueOf(mekanisasiPanens.size()));
        tvTotalJanjang.setText(String.valueOf(tJJG));
        tvBrondolan.setText(String.valueOf(tBrdl));
        tvTotalBeratEstimasi.setText(String.valueOf(tBerat));
    }

    public void setPemanen(Pemanen pemanen){
        pemanenSelected = pemanen;
        if(stampImage == null){
            stampImage = new StampImage();
        }
        stampImage.setPemanen(pemanen);
    }

    private void saveTransaksiPanen(){

//        HashMap<String,TransaksiPanen> panenHashMap = new HashMap<>();
//        if(mekanisasiPanens.size() > 0) {
//            for (TransaksiPanen transaksiPanen : new ArrayList<>(mekanisasiPanens.values())) {
//                Calendar cal = Calendar.getInstance();
//                cal.setTime(new Date(System.currentTimeMillis()));
//                String idBJRInformasi = String.valueOf(cal.get(Calendar.YEAR)) + "-" + String.valueOf(cal.get(Calendar.MONTH) + 1) + "-" + transaksiPanen.getTph().getBlock() + "-" + transaksiPanen.getTph().getAfdeling();
//                BJRInformation bjrInformation = mekanisasiActivity.bjrInformationHashMap.get(idBJRInformasi);
//                if(bjrInformation != null) {
//                    transaksiPanen.setBjrMekanisasi(bjrInformation.getBjrAdjustment());
//                }else{
//                    transaksiPanen.setBjrMekanisasi(0);
//                }
//                panenHashMap.put(transaksiPanen.getIdTPanen(), transaksiPanen);
//            }
//        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        MekanisasiPanen mekanisasiPanen = new MekanisasiPanen(
                "MP-"+System.currentTimeMillis()+"-"+String.valueOf(kraniPanenSelected.getUserID())
                ,selectedOperator
                ,selectedLoader
                ,vehicleUse
                ,etBin.getText().toString()
                ,sdf.format(System.currentTimeMillis())
                ,String.valueOf(kraniPanenSelected.getUserID())
                ,GlobalHelper.getEstate().getEstCode()
                ,Integer.parseInt(tvTotalJanjang.getText().toString())
                ,Integer.parseInt(tvBrondolan.getText().toString())
                ,Integer.parseInt(tvTotalBeratEstimasi.getText().toString())
                ,new ArrayList<>(mekanisasiPanens.values())
        );

        if(mekanisasiPanenSelected != null){
            mekanisasiPanen.setIdMekanisasi(mekanisasiPanenSelected.getIdMekanisasi());
        }

        Gson gson = new Gson();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);

        DataNitrit dataNitrit = new DataNitrit(mekanisasiPanen.getIdMekanisasi(), gson.toJson(mekanisasiPanen));
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", mekanisasiPanen.getIdMekanisasi()));
        if(cursor.size() > 0) {
            repository.update(dataNitrit);
        }else{
            repository.insert(dataNitrit);
        }

        // cek apakah ada id panen yang sama ikut ke dalam mekanisasi yang beda
        HashMap<String,TransaksiPanen> panenHashMap = new HashMap<>();
        Iterable<DataNitrit> Iterable = repository.find().project(DataNitrit.class);
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitritX = (DataNitrit) iterator.next();
            try {
                MekanisasiPanen mekanisasiPanenX = gson.fromJson(dataNitritX.getValueDataNitrit(), MekanisasiPanen.class);
                if (mekanisasiPanen.getIdMekanisasi() != null) {
                    for(int i = 0 ; i< mekanisasiPanenX.getTransaksiPanens().size(); i++){
                        TransaksiPanen panen = mekanisasiPanenX.getTransaksiPanens().get(i);
                        panen.setRemarks(mekanisasiPanenX.getIdMekanisasi());
                        panenHashMap.put(panen.getIdTPanen(),panen);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        //ambil id mekanisasinya
        Set<String> idMekanisasi = new HashSet<>();
        if(panenHashMap.size() > 0) {
            for (TransaksiPanen panen : panenHashMap.values()) {
                idMekanisasi.add(panen.getRemarks());
            }
        }

        Set<String> idMekanisasiHapus = new HashSet<>();
        for (Iterator iterator = Iterable.iterator(); iterator.hasNext();) {
            DataNitrit dataNitritX = (DataNitrit) iterator.next();
            try {
                MekanisasiPanen mekanisasiPanenX = gson.fromJson(dataNitritX.getValueDataNitrit(), MekanisasiPanen.class);
                boolean masuk = false;
                if(idMekanisasi.size() > 0) {
                    for (String s : idMekanisasi) {
                        if (mekanisasiPanenX.getIdMekanisasi().toUpperCase().contains(s)){
                            masuk = true;
                            break;
                        }
                    }
                }

                if(!masuk){
                    idMekanisasiHapus.add(mekanisasiPanenX.getIdMekanisasi());
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        //hapus id mekanisais yang kemungkinan duplicate panenya
        if(idMekanisasiHapus.size() > 0){
            for(String s : idMekanisasiHapus) {
                Cursor<DataNitrit> cursorX = repository.find(ObjectFilters.eq("idDataNitrit", s));
                if (cursorX.size() > 0) {
                    for (DataNitrit dataNitrit1 : cursor) {
                        repository.remove(dataNitrit1);
                    }
                }
            }
        }

        db.close();
    }

    private void hapusTranskasiPanen(){

        MekanisasiPanen mekanisasiPanen = new MekanisasiPanen();
        if(mekanisasiPanenSelected != null){
            mekanisasiPanen.setIdMekanisasi(mekanisasiPanenSelected.getIdMekanisasi());
        }

        Gson gson = new Gson();
        Nitrite db = GlobalHelper.getTableNitrit(GlobalHelper.TABLE_ANGKUT_SPB_MEKANISASI);
        ObjectRepository<DataNitrit> repository = db.getRepository(DataNitrit.class);
        DataNitrit dataNitrit = new DataNitrit(mekanisasiPanen.getIdMekanisasi(), gson.toJson(mekanisasiPanen));
        Cursor<DataNitrit> cursor = repository.find(ObjectFilters.eq("idDataNitrit", mekanisasiPanen.getIdMekanisasi()));
        if(cursor.size() > 0) {
            repository.remove(dataNitrit);
        }
        db.close();
    }

    public boolean cekBackProses(){
        if(mekanisasiPanens.size() == 0){
            return true;
        }else if(!saveBtn && mekanisasiPanens.size() > 0 ){
            return false;
        }
        return saveBtn;
    }

    private class LongOperation extends AsyncTask<String, String, String> {
        private AlertDialog alertDialogAllpoin ;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alertDialogAllpoin = WidgetHelper.showWaitingDialog(getActivity(), getResources().getString(R.string.wait));
        }

        @Override
        protected String doInBackground(String... params) {
            switch (Integer.parseInt(params[0])){
                case LongOperation_Save: {
                    saveTransaksiPanen();
                    saveBtn = true;
                    break;
                }
                case LongOperation_Hapus:{
                    saveBtn = true;
                    hapusTranskasiPanen();
                    break;
                }
            }
            return String.valueOf(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (alertDialogAllpoin != null) {
                alertDialogAllpoin.cancel();
            }

            switch (Integer.parseInt(result)) {
                case LongOperation_Save: {
                    mekanisasiActivity.backProses();
                    break;
                }
                case LongOperation_Hapus: {
                    mekanisasiActivity.backProses();
                    break;
                }
            }
        }
    }
}
