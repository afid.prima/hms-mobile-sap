package id.co.ams_plantation.harvestsap.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.io.File;
import java.util.Date;

import id.co.ams_plantation.harvestsap.Fragment.AssistensiInputFragment;
import id.co.ams_plantation.harvestsap.Fragment.AssistensiListFragment;
import id.co.ams_plantation.harvestsap.Fragment.ReportListFragment;
import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.connection.ServiceResponse;
import id.co.ams_plantation.harvestsap.model.LinkReport;
import id.co.ams_plantation.harvestsap.presenter.ConnectionPresenter;
import id.co.ams_plantation.harvestsap.presenter.ReportPresenter;
import id.co.ams_plantation.harvestsap.view.ReportView;

public class ReportActivity extends BaseActivity implements ReportView {
    public ReportPresenter presenter;
    public ConnectionPresenter connectionPresenter;
    public LinkReport selectedLinkReport;

    TextView textToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_layout);
        toolBarSetup();
        main();
    }

    @Override
    protected void initPresenter() {
        presenter = new ReportPresenter(this);
        connectionPresenter = new ConnectionPresenter(this);
    }



    public void openPDF (){
        File file = selectedLinkReport.getFullPath();

        Intent intent = new Intent(Intent.ACTION_VIEW);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Uri apkUri = FileProvider.getUriForFile(this,
                    this.getApplicationContext().getPackageName() + ".provider",
                    file);

            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            Log.e("Location",apkUri.toString());
            intent.setDataAndType(apkUri, "application/pdf");
        }else{
            intent.setDataAndType(Uri.fromFile(file), "application/pdf");
        }
        //intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
       onBackProses();
    }

    public void onBackProses(){
        finish();
    }

    private void toolBarSetup() {
        getSupportActionBar().hide();
        ImageView imgBack = (ImageView) findViewById(R.id.toolbar_icon);
        textToolbar = (TextView) findViewById(R.id.toolbar_text);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageDrawable(new IconicsDrawable(this)
                .icon(MaterialDesignIconic.Icon.gmi_arrow_left)
                .colorRes(R.color.White)
                .sizeDp(24));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackProses();
            }
        });
        textToolbar.setText(getResources().getString(R.string.report));
    }
    public void setTextToolbar(String title){
        textToolbar.setText(title);
    }

    private void main(){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_container, ReportListFragment.getInstance())
                .commit();
    }

    @Override
    public void successResponse(ServiceResponse serviceResponse) {
        try {
            switch (serviceResponse.getTag()) {
                case GetLinkReport:
                    if (getSupportFragmentManager().findFragmentById(R.id.content_container)
                            instanceof ReportListFragment) {
                        ReportListFragment fragment = (ReportListFragment) this.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        fragment.onSuccesRespon(serviceResponse);
                    }
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void badResponse(ServiceResponse serviceResponse) {
        try {
            switch (serviceResponse.getTag()) {
                case GetLinkReport:
                    if (getSupportFragmentManager().findFragmentById(R.id.content_container)
                            instanceof ReportListFragment) {
                        ReportListFragment fragment = (ReportListFragment) this.getSupportFragmentManager().findFragmentById(R.id.content_container);
                        fragment.onBadResponse(serviceResponse);
                    }
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
