package id.co.ams_plantation.harvestsap.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import id.co.ams_plantation.harvestsap.R;
import id.co.ams_plantation.harvestsap.model.SPK;
import id.co.ams_plantation.harvestsap.model.TPH;
import id.co.ams_plantation.harvestsap.util.TransaksiPanenHelper;

public class SelectSpkAdapter extends ArrayAdapter<SPK> {

    public ArrayList<SPK> items;
    public ArrayList<SPK> itemsAll;
    public ArrayList<SPK> suggestions;
    private final int viewResourceId;
    private TPH tph;

    public SelectSpkAdapter(@NonNull Context context, int resource, @NonNull ArrayList<SPK> data, TPH tph) {
        super(context, resource, data);
        this.items = new ArrayList<>();
        this.items.addAll(data);
        this.itemsAll = (ArrayList<SPK>) this.items.clone();
        this.suggestions = new ArrayList<>();
        this.viewResourceId = resource;
        this.tph = tph;
    }

    public SelectSpkAdapter(@NonNull Context context, int resource, @NonNull ArrayList<SPK> data) {
        super(context, resource, data);
        this.items = new ArrayList<>();
        this.items.addAll(data);
        this.itemsAll = (ArrayList<SPK>) this.items.clone();
        this.suggestions = new ArrayList<>();
        this.viewResourceId = resource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        if (items.size() > position) {
            SPK spk = items.get(position);
            if (spk != null) {
                ImageView iv = (ImageView) v.findViewById(R.id.iv);
                TextView tv = (TextView) v.findViewById(R.id.tv);
                TextView tv1 = (TextView) v.findViewById(R.id.tv1);
                iv.setVisibility(View.GONE);
                tv.setText(TransaksiPanenHelper.showSPK(spk));
                tv1.setText(spk.getShortText());
            }
        }
        return v;
    }

    public SPK getItemAt(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((SPK) (resultValue)).getIdSPK();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (SPK pks : itemsAll) {
                    if (pks.getIdSPK().toLowerCase().contains(constraint.toString().toLowerCase()) || pks.getMaterialGroupDesc().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        boolean add = true;
//                        if (tph != null) {
//                            if (tph.getBlock() != null) {
//                                add = false;
//                                for (int i = 0; i < pks.getBlock().size(); i++) {
//                                    if (pks.getBlock().get(i).toUpperCase().contains(tph.getBlock().toUpperCase())) {
//                                        add = true;
//                                        break;
//                                    }
//                                }
//                            }
//                        }
//
//                        if(System.currentTimeMillis() >= pks.getValidFrom() && System.currentTimeMillis() <= pks.getValidTo()) {
//                            if (add) {
                                suggestions.add(pks);
//                            }
//                        }
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<SPK> filteredList = (ArrayList<SPK>) results.values;
//            clear();
            items.clear();
//            itemsAll.clear();
            if (results != null && results.count > 0) {
//                addAll(filteredList);
//                itemsAll.addAll(filteredList);
//                for (Anggota anggota : filteredList){
//                    items.add(anggota);
//                }
                items.addAll(filteredList);
            } else {
//                clear();
                items.clear();
//                itemsAll.clear();
            }
            notifyDataSetChanged();
        }
    };
}