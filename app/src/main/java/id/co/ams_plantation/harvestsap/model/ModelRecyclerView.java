package id.co.ams_plantation.harvestsap.model;

/**
 * Created by user on 12/4/2018.
 */

public class ModelRecyclerView {
    String nama;
    String nama1;
    String nama2;
    String nama3;
    String nama4;
    String nama5;
    String layout;

    public ModelRecyclerView(String layout,String nama, String nama1, String nama2) {
        this.nama = nama;
        this.nama1 = nama1;
        this.nama2 = nama2;
        this.layout = layout;
    }

    public ModelRecyclerView(String layout,String nama, String nama1, String nama2, String nama3) {
        this.nama = nama;
        this.nama1 = nama1;
        this.nama2 = nama2;
        this.nama3 = nama3;
        this.layout = layout;
    }

    public ModelRecyclerView(String layout,String nama, String nama1, String nama2, String nama3, String nama4, String nama5) {
        this.nama = nama;
        this.nama1 = nama1;
        this.nama2 = nama2;
        this.nama3 = nama3;
        this.nama4 = nama4;
        this.nama5 = nama5;
        this.layout = layout;
    }

    public String getNama1() {
        return nama1;
    }

    public void setNama1(String nama1) {
        this.nama1 = nama1;
    }

    public String getNama2() {
        return nama2;
    }

    public void setNama2(String nama2) {
        this.nama2 = nama2;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public String getNama3() {
        return nama3;
    }

    public void setNama3(String nama3) {
        this.nama3 = nama3;
    }

    public String getNama4() {
        return nama4;
    }

    public void setNama4(String nama4) {
        this.nama4 = nama4;
    }

    public String getNama5() {
        return nama5;
    }

    public void setNama5(String nama5) {
        this.nama5 = nama5;
    }
}
