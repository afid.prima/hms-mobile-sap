package id.co.ams_plantation.harvestsap.model;

public class QcMutuBuah {
    String idQCMutuBuah;
    String idTPanen;
    TransaksiPanen transaksiPanenKrani;
    TransaksiPanen transaksiPanenQc;

    public QcMutuBuah() {
    }

    public QcMutuBuah(String idQCMutuBuah, String idTPanen, TransaksiPanen transaksiPanenKrani, TransaksiPanen transaksiPanenQc) {
        this.idQCMutuBuah = idQCMutuBuah;
        this.idTPanen = idTPanen;
        this.transaksiPanenKrani = transaksiPanenKrani;
        this.transaksiPanenQc = transaksiPanenQc;
    }

    public String getIdQCMutuBuah() {
        return idQCMutuBuah;
    }

    public void setIdQCMutuBuah(String idQCMutuBuah) {
        this.idQCMutuBuah = idQCMutuBuah;
    }

    public String getIdTPanen() {
        return idTPanen;
    }

    public void setIdTPanen(String idTPanen) {
        this.idTPanen = idTPanen;
    }

    public TransaksiPanen getTransaksiPanenKrani() {
        return transaksiPanenKrani;
    }

    public void setTransaksiPanenKrani(TransaksiPanen transaksiPanenKrani) {
        this.transaksiPanenKrani = transaksiPanenKrani;
    }

    public TransaksiPanen getTransaksiPanenQc() {
        return transaksiPanenQc;
    }

    public void setTransaksiPanenQc(TransaksiPanen transaksiPanenQc) {
        this.transaksiPanenQc = transaksiPanenQc;
    }
}
