package cat.ereza.customactivityoncrash.activity;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import net.rehacktive.waspdb.WaspDb;
import net.rehacktive.waspdb.WaspFactory;
import net.rehacktive.waspdb.WaspHash;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import cat.ereza.customactivityoncrash.R;
import cat.ereza.customactivityoncrash.config.CaocConfig;
import cat.ereza.customactivityoncrash.helper.GlobalHelper;
import cat.ereza.customactivityoncrash.helper.HTTPRequest;
import cat.ereza.customactivityoncrash.model.CrashDetailAnalyticInfo;
import cat.ereza.customactivityoncrash.model.User;
import cat.ereza.customactivityoncrash.util.CrashUtil;
import cat.ereza.customactivityoncrash.util.ServerConnectionListener;
import cat.ereza.customactivityoncrash.util.WidgetHelper;
import cz.msebera.android.httpclient.Header;

public class CrashReportActivity extends AppCompatActivity {
    Button detailBtn;
    Button reportBtn;
    Button restartBtn;
    ImageView backImg;
    CaocConfig config;
    User user;
    CrashDetailAnalyticInfo crashDetail;
    private AlertDialog loadingDialog;
    ServerConnectionListener serverConnectionListener;

    @SuppressLint("PrivateResource")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customactivityoncrash_activity_crash_report);
//        registerReceiver();
        noSqlInit();
//        Button restartButton = findViewById(R.id.customactivityoncrash_error_activity_restart_button);
        restartBtn = findViewById(R.id.restartBtn);
        reportBtn = findViewById(R.id.reportBtn);
        detailBtn = findViewById(R.id.detailBtn);
        backImg = findViewById(R.id.back);

        config = CustomActivityOnCrash.getConfigFromIntent(getIntent());
        reportBtn.setVisibility(View.GONE);
        if (config == null) {
            //This should never happen - Just finish the activity to avoid a recursive crash.
            finish();
            return;
        }

        if (config.isShowRestartButton() && config.getRestartActivityClass() != null) {
            restartBtn.setText(R.string.customactivityoncrash_error_activity_restart_app);
            restartBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CustomActivityOnCrash.restartApplication(CrashReportActivity.this, config);
                }
            });
        } else {
            restartBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CustomActivityOnCrash.closeApplication(CrashReportActivity.this, config);
                }
            });
        }

//        Button moreInfoButton = findViewById(R.id.customactivityoncrash_error_activity_more_info_button);

        if (config.isShowErrorDetails()) {
            detailBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //We retrieve all the error data and show it

                    AlertDialog dialog = new AlertDialog.Builder(CrashReportActivity.this)
                            .setTitle(R.string.customactivityoncrash_error_activity_error_details_title)
                            .setMessage(CustomActivityOnCrash.getAllErrorDetailsFromIntent(CrashReportActivity.this, getIntent()))
                            .setPositiveButton(R.string.customactivityoncrash_error_activity_error_details_close, null)
                            .setNeutralButton(R.string.customactivityoncrash_error_activity_error_details_copy,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            copyErrorToClipboard();
                                        }
                                    })
                            .show();
                    TextView textView = dialog.findViewById(android.R.id.message);
                    if (textView != null) {
                        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.customactivityoncrash_error_activity_error_details_text_size));
                    }
                }
            });
        } else {
            detailBtn.setVisibility(View.GONE);
        }

        Integer defaultErrorActivityDrawableId = config.getErrorDrawable();
        ImageView errorImageView = findViewById(R.id.customactivityoncrash_error_activity_image);

        if (defaultErrorActivityDrawableId != null) {
            errorImageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), defaultErrorActivityDrawableId, getTheme()));
        }

        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                unRegister();
                finish();
            }
        });

        reportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingDialog = WidgetHelper.showWaitingDialog(CrashReportActivity.this, "Tunggu Sebentar!");
                getData();

            }
        });

        isOffline(CrashUtil.getCrashDetail(CustomActivityOnCrash.getContext(),Integer.parseInt(user.getUserID()),
                CrashUtil.getPackageName(CustomActivityOnCrash.getContext()),CustomActivityOnCrash.getAllErrorDetailsFromIntentV2(CrashReportActivity.this, getIntent())));
    }

    private void copyErrorToClipboard() {
        String errorInformation = CustomActivityOnCrash.getAllErrorDetailsFromIntent(CrashReportActivity.this, getIntent());

        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        //Are there any devices without clipboard...?
        if (clipboard != null) {
            ClipData clip = ClipData.newPlainText(getString(R.string.customactivityoncrash_error_activity_error_details_clipboard_label), errorInformation);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(CrashReportActivity.this, R.string.customactivityoncrash_error_activity_error_details_copied, Toast.LENGTH_SHORT).show();
        }
    }

    private void getData(){
        crashDetail = CrashUtil.getCrashDetail(CustomActivityOnCrash.getContext(),Integer.parseInt(user.getUserID()),
                CrashUtil.getPackageName(CustomActivityOnCrash.getContext()),CustomActivityOnCrash.getAllErrorDetailsFromIntentV2(CrashReportActivity.this, getIntent()));

        Log.i("CrashData", "crashDetail: "+crashDetail.toString());
        checkServerConnection(CrashReportActivity.this, crashDetail);
//        if(loadingDialog.isShowing()){
//            loadingDialog.dismiss();
//        }

    }

    private void reportCrash(final CrashDetailAnalyticInfo model){
        AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try{
                    String response = new String(responseBody, StandardCharsets.UTF_8);
                    final JSONObject object = new JSONObject(response);
                    if(object.getString("code").equals("200")) {
                        WaspDb db = WaspFactory.openOrCreateDatabase(GlobalHelper.getDatabasePath(),
                                GlobalHelper.DB_MASTER,
                                GlobalHelper.getDatabasePass());
                        final WaspHash crashTbl = db.openOrCreateHash(GlobalHelper.CrashReport);
                        crashTbl.remove(model);
                        Log.i("Sukses Insert", "onSuccess: Berhasil insert");
                    }else{
                        Log.i("Gagal Insert", "onSuccess: Gagal insert "+response);
                        isOffline(model);
                    }
                    loadingDialog.dismiss();
                }catch (JSONException e){
                    Log.i("Sukses Insert", "onSuccess: Gagal insert JSONException : "+e.toString() );
                    isOffline(model);
                    loadingDialog.dismiss();
                }
                Toast.makeText(CrashReportActivity.this, "Crash has succesfully reported!", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i("Gagal Insert", "onFailure: Gagal insert : "+error.toString());
                Toast.makeText(CrashReportActivity.this, "Crash has succesfully reported!", Toast.LENGTH_SHORT).show();
                isOffline(model);
                loadingDialog.dismiss();
            }
        };


        HTTPRequest.reportCrash(CustomActivityOnCrash.getContext(),model,handler);
    }

    private void noSqlInit(){
        user = GlobalHelper.getUser();
    }

    private void isOffline( CrashDetailAnalyticInfo crashDetail){
        WaspDb db = WaspFactory.openOrCreateDatabase(GlobalHelper.getDatabasePath(),
                GlobalHelper.DB_MASTER,
                GlobalHelper.getDatabasePass());
        final WaspHash crashTbl = db.openOrCreateHash(GlobalHelper.CrashReport);
        int i=0;


        if(crashTbl.getAllValues().size()==0){
           i=1;
            crashTbl.put(i,crashDetail);

            Log.i("Offline Add", "berhasil simpan log crash :1 "+crashTbl.getAllValues().size());
        }else{
            boolean notFound = false;
            for(Object obj : crashTbl.getAllValues()){
                CrashDetailAnalyticInfo model = (CrashDetailAnalyticInfo) obj;
                if(model.getCrashDetail().equals(crashDetail.getCrashDetail())){
                    notFound = true;
                    break;
                }
            }
            if(notFound){
                i = crashTbl.getAllValues().size()+1;
                crashTbl.put(i,crashDetail);

                Log.i("Offline Add", "berhasil simpan log crash : "+crashTbl.getAllValues().size());
            }
        }

        if(loadingDialog != null) {
            if (loadingDialog.isShowing()) {
                loadingDialog.dismiss();
            }
        }

//        Log.i("Offline Add", "berhasil simpan log crash : "+crashTbl.getAllValues().size());
    }

    private void checkServerConnection(Context context,final CrashDetailAnalyticInfo crashDetailAnalyticInfo){
        AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                try {
                    String response = new String(responseBody, StandardCharsets.UTF_8);
                    JSONObject responseObject = new JSONObject(response);
                    if(responseObject.get("code").equals("200")){
                        //todo update help description admin apps
                        reportCrash(crashDetailAnalyticInfo);
                    }else{
//                        isOffline(crashDetailAnalyticInfo);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
//                    isOffline(crashDetailAnalyticInfo);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                isOffline(crashDetailAnalyticInfo);
            }
        };
        // HTTPRequest.checkConnection(handler);
        HTTPRequest.getHelpDescription(context,handler);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        registerReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        unRegister();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        unRegister();
    }

    private void registerReceiver(){
        serverConnectionListener = new ServerConnectionListener();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(serverConnectionListener,intentFilter);
    }

    private void unRegister(){
        if(serverConnectionListener!=null){
            unregisterReceiver(serverConnectionListener);
        }
    }

}
