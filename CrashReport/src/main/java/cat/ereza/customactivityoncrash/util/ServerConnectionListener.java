package cat.ereza.customactivityoncrash.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;

import net.rehacktive.waspdb.WaspDb;
import net.rehacktive.waspdb.WaspFactory;
import net.rehacktive.waspdb.WaspHash;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;
import cat.ereza.customactivityoncrash.helper.GlobalHelper;
import cat.ereza.customactivityoncrash.helper.HTTPRequest;
import cat.ereza.customactivityoncrash.model.CrashDetailAnalyticInfo;
import cz.msebera.android.httpclient.Header;

public class ServerConnectionListener extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        String action = intent.getAction();
        Log.d("TEMP", action);
        if(action.equals(ConnectivityManager.CONNECTIVITY_ACTION)){
            Log.i("Fire here", "onReceive: ");
            WaspDb db = WaspFactory.openOrCreateDatabase(GlobalHelper.getDatabasePath(),
                    GlobalHelper.DB_MASTER,
                    GlobalHelper.getDatabasePass());
            final WaspHash crashTbl = db.openOrCreateHash(GlobalHelper.CrashReport);
            if(crashTbl.getAllValues().size()>0){
                Log.i("Check Coonection", "onReceive: ");
                checkServerConnection(context);
            }//
        }
    }

    private void checkServerConnection(Context context){
        AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                try {
                    String response = new String(responseBody, StandardCharsets.UTF_8);
                    JSONObject responseObject = new JSONObject(response);
                    if(responseObject.get("code").equals("200")){
                        //todo update help description admin apps
                        WaspDb db = WaspFactory.openOrCreateDatabase(GlobalHelper.getDatabasePath(),
                                GlobalHelper.DB_MASTER,
                                GlobalHelper.getDatabasePass());
                        final WaspHash crashTbl = db.openOrCreateHash(GlobalHelper.CrashReport);
                        int id = 1;
                        for(Object obj : crashTbl.getAllValues()){
                            CrashDetailAnalyticInfo model = (CrashDetailAnalyticInfo) obj;
                            reportCrash(id,model);
                            id++;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        };
        HTTPRequest.getHelpDescription(context,handler);
    }

    public static void reportCrash(final int id,CrashDetailAnalyticInfo model){
        AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                WaspDb db = WaspFactory.openOrCreateDatabase(GlobalHelper.getDatabasePath(),
                        GlobalHelper.DB_MASTER,
                        GlobalHelper.getDatabasePass());
                final WaspHash crashTbl = db.openOrCreateHash(GlobalHelper.CrashReport);
                try{
                    String response = new String(responseBody, StandardCharsets.UTF_8);
                    final JSONObject object = new JSONObject(response);
                    if(object.getString("code").equals("200")) {


                        Log.i("Sukses Insert", "onSuccess: Berhasil insert");
                    }else{
                        Log.i("Gagal Insert", "onSuccess: Gagal insert "+response);
                    }
                    crashTbl.remove(id);
//                    loadingDialog.dismiss();
                }catch (JSONException e){
                    Log.i("Sukses Insert", "onSuccess: Gagal insert JSONException : "+e.toString() );
//                    loadingDialog.dismiss();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i("Gagal Insert", "onFailure: Gagal insert : "+error.toString());
//                loadingDialog.dismiss();
            }
        };


        HTTPRequest.reportCrash(CustomActivityOnCrash.getContext(),model,handler);
    }
}
